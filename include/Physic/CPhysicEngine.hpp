/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CPhysicEngine.hpp
 * \date       2008 Création de la classe CPhysicEngine.
 * \date 17/07/2010 Création de la méthode RemoveModel.
 * \date 14/12/2010 Ajout de la liste des triggers.
 * \date 12/04/2011 Ajout d'un début de code pour utiliser le moteur physique Bullet.
 * \date 12/12/2011 Ajout d'une liste de volumes statiques convexes.
 *                  Ajout d'un débugger pour Bullet.
 */

#ifndef T_FILE_PHYSIC_CPHYSICENGINE_HPP_
#define T_FILE_PHYSIC_CPHYSICENGINE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#ifdef T_USING_PHYSIC_BULLET
#  include <bullet/btBulletDynamicsCommon.h>
#endif

#include "Physic/Export.hpp"
#include "Physic/CRay.hpp"


namespace Ted
{

/**
 * \enum    TContentType
 * \ingroup Physic
 * \brief   Types de contenu.
 ******************************/

enum TContentType
{
    Empty,          ///< Vide, les objets peuvent s'y déplacer.
    Solid,          ///< Solide, les objets ne peuvent pas s'y trouver.
    Water,          ///< Eau, les objets peuvent s'y trouver. Les personnages ne peuvent rester sous l'eau que pendant un certain temps.
    Trigger,        ///< Trigger (déclencheur). Les objets qui le traversent peuvent déclencher une action.
    WaterAndTrigger ///< Eau et trigger.
};

/*
#ifdef T_USE_MODELS_V2
class IModel;
#else
class IModel;
class IModelV2;
#endif
*/
class ICollisionVolume;
//class IBaseTrigger;
class CBSPTree;
class CStaticVolume;
class CPhysicEngine;
//class CBuffer;


namespace Game
{
    extern T_PHYSIC_API CPhysicEngine * physicEngine; // Défini dans CPhysicEngine.cpp
}


/**
 * \class   CPhysicEngine
 * \ingroup Physic
 * \brief   Cette classe coordonne l'ensemble de la physique utilisée dans le jeu.
 *
 * Elle tient à jour une liste des modèles physiques et une liste des triggers, et
 * fournit des méthodes pour gérer les collisions.
 ******************************/

class T_PHYSIC_API CPhysicEngine
{
public:

    // Constructeur et destructeur
    CPhysicEngine();
    ~CPhysicEngine();

    // Accesseurs
    TVector3F getGravity() const;
    CBSPTree * getBSPTree() const;
    unsigned int getNumBodys() const;
    //unsigned int getNumTriggers() const;
    //CBuffer * getDebugBuffer() const;

    // Mutateurs
    void setGravity(const TVector3F gravity);
    void setBSPTree(CBSPTree * bsp);

    // Méthodes publiques

    void addRigidBody(btRigidBody * body);
    void removeRigidBody(btRigidBody * body);
/*
#ifdef T_USE_MODELS_V2
    void addModel(IModel * model);
    void removeModel(IModel * model);
#else
    void addModel(IModel * model);
    void removeModel(IModel * model);
    void addModel(IModelV2 * model);
    void removeModel(IModelV2 * model);
#endif
*/
    void addStaticVolume(CStaticVolume * volume);
    void removeStaticVolume(CStaticVolume * volume);
    //void addTrigger(IBaseTrigger * trigger);
    //void removeTrigger(IBaseTrigger * trigger);
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectPosition(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    bool isUnderWater(const TVector3F& position) const;
    TVector3F getCorrectPosition(const TVector3F& position);
    TVector3F getCorrectDeplacement(const TVector3F& from, const TVector3F& to);
    CRayImpact getRayImpact(const CRay& ray) const;
    bool slide(ICollisionVolume& volume, const TVector3F& movement) const;
    //void checkTriggers(IBaseAnimating * entity) const;
    void setFrameTime(unsigned int frameTime);

#ifdef T_USING_PHYSIC_BULLET
    inline btDiscreteDynamicsWorld * getWorld() const
    {
        return m_world;
    }
#endif

protected:

    // Données protégées

#ifdef T_USING_PHYSIC_BULLET
    btDiscreteDynamicsWorld * m_world;
    btBroadphaseInterface * m_broadphase;
    btCollisionDispatcher * m_dispatcher;
    btDefaultCollisionConfiguration * m_collision_configuration;
    btSequentialImpulseConstraintSolver * m_solver;
    //CBuffer * m_buffer;                     ///< Buffer graphique utilisé pour l'affichage des données physiques.
#endif

    TVector3F m_gravity;                    ///< Vecteur d'accélération de la pesanteur.
    CBSPTree * m_bsp;                       ///< Arbre BSP principal.
    std::vector<btRigidBody *> m_bodys;     ///< Tableau contenant tous les modèles de la map.

/*
#ifdef T_USE_MODELS_V2
    std::vector<IModel *> m_models;         ///< Tableau contenant tous les modèles de la map.
#else
    std::vector<IModel *> m_models;         ///< Tableau contenant tous les modèles de la map.
    std::vector<IModelV2 *> m_modelsV2;     ///< Tableau contenant tous les modèles de la map.
#endif
*/

    //std::vector<IBaseTrigger *> m_triggers; ///< Tableau contenant tous les triggers de la map.
    std::vector<CStaticVolume *> m_volumes; ///< Tableau contenant tous les volumes statiques convexes de la map.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CPHYSICENGINE_HPP_

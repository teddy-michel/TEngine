/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBSPTree.hpp
 * \date       2008 Création de la classe CBSPTree.
 */

#ifndef T_FILE_PHYSIC_CBPSTREE_HPP_
#define T_FILE_PHYSIC_CBPSTREE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "CPhysicEngine.hpp"


namespace Ted
{

class CBSPTreeNode;
class Volume;
class Impact;
class ICollisionVolume;


/**
 * \class   CBSPTree
 * \ingroup Physic
 * \brief   Gestion des arbres BSP.
 *
 * Cette classe est similaire à CBSPTreeNode, mise à part qu'elle fournit une
 * méthode pour gérer le glissement des formes englobantes sur l'arbre BSP.
 ******************************/

class T_PHYSIC_API CBSPTree
{
public:

    // Constructeur et destructeur
    CBSPTree(CBSPTreeNode * node = nullptr);
    ~CBSPTree();

    // Accesseur
    CBSPTreeNode * getHeadNode() const;

    // Mutateur
    void setHeadNode(CBSPTreeNode * node);

    // Méthodes publiques
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    bool Slide(ICollisionVolume& volume, const TVector3F& movement) const;
    void getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const;
    bool hasImpact(const CRay& ray) const;
    CRayImpact getRayImpact(const CRay& ray) const;
    void CheckTree();

#ifdef T_DEBUG
    void Debug_LogTree() const;
#endif

protected:

    // Donnée protégée
    CBSPTreeNode * m_node; ///< Premier nœud de l'arbre BSP.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CBPSTREE_HPP_

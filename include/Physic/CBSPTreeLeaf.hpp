/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBSPTreeLeaf.hpp
 * \date 09/05/2009 Création de la classe CBSPTreeLeaf.
 */

#ifndef T_FILE_PHYSIC_CBPSTREELEAF_HPP_
#define T_FILE_PHYSIC_CBPSTREELEAF_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "IBSPTreeElement.hpp"


namespace Ted
{

/**
 * \class   CBSPTreeLeaf
 * \ingroup Physic
 * \brief   Feuille d'un arbre BSP.
 ******************************/

class T_PHYSIC_API CBSPTreeLeaf : public IBSPTreeElement
{
public:

    // Constructeur et destructeur
    CBSPTreeLeaf(TContentType type = Empty);
    virtual ~CBSPTreeLeaf();

    // Accesseurs
    bool isLeaf() const;
    bool isNode() const;
    TContentType getType() const;

    // Mutateur
    void setType(TContentType type);

    // Méthodes publiques
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    void getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const;
    bool hasImpact(const CRay& ray) const;
    bool getRayImpact(const CRay& ray, CRayImpact& impact) const;
    bool CheckTree();

#ifdef T_DEBUG
    void Debug_LogTree(unsigned int decalage = 0) const;
#endif

protected:

    // Donnée protégée
    TContentType m_type; ///< Type de feuilles de l'arbre BSP.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CBPSTREELEAF_HPP_

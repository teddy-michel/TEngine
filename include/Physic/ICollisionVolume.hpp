/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/ICollisionVolume.hpp
 * \date 11/02/2010 Création de la classe ICollisionVolume.
 */

#ifndef T_FILE_PHYSIC_ICOLLISIONVOLUME_HPP_
#define T_FILE_PHYSIC_ICOLLISIONVOLUME_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "CPhysicEngine.hpp"
#include "CPlane.hpp"


namespace Ted
{

/**
 * \class   ICollisionVolume
 * \ingroup Physic
 * \brief   Classe de base des volumes permettant de gérer les collisions par le moteur physique.
 ******************************/

class T_PHYSIC_API ICollisionVolume
{
public:

    // Constructeur et destructeur
    ICollisionVolume();
    virtual ~ICollisionVolume();

    // Position
    virtual TVector3F getPosition() const = 0;
    virtual void setPosition(const TVector3F& position) = 0;
    virtual void translate(const TVector3F& v) = 0;

    // Méthodes publiques
    virtual float getOffset(const CPlane& plane) const = 0;
    virtual TContentType getContentType(const TVector3F& position) const = 0;
    virtual bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const = 0;
    virtual bool hasImpact(const CRay& ray) const = 0;
    virtual CRayImpact getRayImpact(const CRay& ray) const = 0;
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_ICOLLISIONVOLUME_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBoundingBox.hpp
 * \date 02/02/2010 Création de la classe CBoundingBox.
 * \date 11/07/2010 Test d'intersection avec une CSphereBox ou un CTriangle.
 * \date 16/12/2010 Ajout de la méthode isInside.
 */

#ifndef T_FILE_PHYSIC_CBOUNDINGBOX_HPP_
#define T_FILE_PHYSIC_CBOUNDINGBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "ICollisionVolume.hpp"


namespace Ted
{

class CBoundingSphere;
class CHitBox;
class CTriangle;


/**
 * \class   CBoundingBox
 * \ingroup Physic
 * \brief   Une bounding box est une boite non orientée.
 *
 * Une bounding box, ou boite AABB, est le plus petit volume aligné sur l'axe
 * englobant entièrement un modèle. Elle permet de déterminer rapidement si deux
 * modèles peuvent être en collision.
 ******************************/

class T_PHYSIC_API CBoundingBox : public ICollisionVolume
{
public:

    // Constructeurs
    CBoundingBox();
    CBoundingBox(const TVector3F& min, const TVector3F& max);

    // Accesseurs
    TVector3F getPosition() const;
    TVector3F getMin() const;
    TVector3F getMax() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& v);
    void setMin(const TVector3F& min);
    void setMax(const TVector3F& max);
    void addPoint(const TVector3F& point);

    // Méthodes publiques
    float getOffset(const CPlane& plane) const;
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    bool hasImpact(const CRay& ray) const;
    CRayImpact getRayImpact(const CRay& ray) const;
    bool isInside(const CBoundingBox& box) const;
    bool isIntersecting(const CBoundingBox& box) const;
    bool isIntersecting(const CBoundingSphere& sphere) const;
    bool isIntersecting(const CHitBox& box) const;
    bool isIntersecting(const CTriangle& triangle) const;

#ifdef T_DEBUG
    void Debug_Draw() const;
#endif

protected:

    // Données protégées
    TVector3F m_min;  ///< Point minimal.
    TVector3F m_max;  ///< Point maximal.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CBOUNDINGBOX_HPP_

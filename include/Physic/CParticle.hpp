/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CParticle.hpp
 * \date       2008 Création de la classe CParticule.
 * \date 16/07/2010 Modification de la gestion de la durée de vie.
 * \date 08/05/2011 La classe est renommée en CParticle.
 */

#ifndef T_FILE_PHYSIC_CPARTICLE_HPP_
#define T_FILE_PHYSIC_CPARTICLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Graphic/CColor.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   CParticle
 * \ingroup Physic
 * \brief   Description d'une particule.
 ******************************/

class T_PHYSIC_API CParticle
{
public:

    // Constructeur
    explicit CParticle(const TVector3F& position = Origin3F, const TVector3F& vitesse = Origin3F, const CColor& color = CColor::White, unsigned int life = static_cast<unsigned int>(-1));

    // Accesseurs
    float getMass() const;
    TVector3F getPosition() const;
    TVector3F getVitesse() const;
    CColor getColor() const;
    unsigned int getLife() const;
    bool isDead() const;

    // Mutateurs
    void setMass(float mass);
    void setPosition(const TVector3F& position);
    void setVitesse(const TVector3F& vitesse);
    void setColor(const CColor& color);
    void setLife(unsigned int life);

    // Méthode publique
    void update(unsigned int frameTime);

protected:

    // Données protégées
    float m_mass;         ///< Masse de la particule (inutile pour le moment car pas de frottements).
    TVector3F m_position; ///< Position de la particule.
    TVector3F m_vitesse;  ///< Vitesse de la particule.
    CColor m_color;       ///< Couleur de la particule.
    unsigned int m_life;  ///< Durée de vie restante à la particule en millisecondes.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CPARTICLE_HPP_

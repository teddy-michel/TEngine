/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CStaticVolume.hpp
 * \date 12/12/2011 Création de la classe CStaticVolume.
 */

#ifndef T_FILE_PHYSIC_CSTATICVOLUME_HPP_
#define T_FILE_PHYSIC_CSTATICVOLUME_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#ifdef T_USING_PHYSIC_BULLET
#  include <bullet/btBulletDynamicsCommon.h>
#endif

#include "Physic/Export.hpp"


namespace Ted
{

/**
 * \class   CStaticVolume
 * \ingroup Game
 * \brief   La classe CStaticVolume permet de gérer un volume statique convexe.
 ******************************/

class T_PHYSIC_API CStaticVolume
{
public:

    // Constructeur et destructeur
    CStaticVolume(const btAlignedObjectArray<btVector3>& vertices);
    ~CStaticVolume();

#ifdef T_USING_PHYSIC_BULLET
    btRigidBody * getRigidBody() const;
    btCollisionShape * getShape() const;
#endif

private:

    // Données privées
#ifdef T_USING_PHYSIC_BULLET
    btRigidBody * m_body;       ///< Pointeur sur l'objet rigide.
    btCollisionShape * m_shape; ///< Volume englobant l'objet.
#endif

};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CSTATICVOLUME_HPP_

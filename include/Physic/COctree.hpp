/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/COctree.hpp
 * \date       2008 Création de la classe COctree.
 * \date 27/07/2010 Ajout du paramètre child.
 * \date 10/04/2011 Ajout de plusieurs attributs et méthodes.
 */

#ifndef T_FILE_PHYSIC_COCTREE_HPP_
#define T_FILE_PHYSIC_COCTREE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   COctree
 * \ingroup Physic
 * \brief   Gestion des arbres octaires (Octree).
 * \todo    Créer les méthodes pour tester les collisions.
 ******************************/

class T_PHYSIC_API COctree
{
public:

    // Constructeur et destructeur
    COctree();
    ~COctree();

    // Accesseur
    TVector3F getCenter() const;
    TContentType getType() const;
    bool isLeaf() const;

    // Mutateur
    void setType(TContentType type);

    // Méthodes publiques
    TContentType getContentType(const TVector3F& position) const;
    void Divide(const TVector3F& center);

protected:

    // Donnée protégée
    TVector3F m_center;  ///< Coordonnées du centre du nœud.
    TContentType m_type; ///< Type de contenu du nœud.

    /**
     * Nœuds enfants. Si le nœud est une feuille, les 8 pointeurs valent nullptr.
     * Représentation spatiale des nœuds :
     *
     * \code
     * Partie supérieure :
     *
     * +---+---+
     * | 0 | 1 |
     * +---+---+   Y
     * | 2 | 3 |   |
     * +---+---+   +---X
     *
     * Partie inférieure :
     *
     * +---+---+
     * | 4 | 5 |
     * +---+---+   Y
     * | 6 | 7 |   |
     * +---+---+   +---X
     * \endcode
     ******************************/
    COctree * m_child[8];
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_COCTREE_HPP_

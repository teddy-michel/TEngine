/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/IBSPTreeElement.hpp
 * \date 09/05/2009 Création de la classe IBSPTreeElement.
 */

#ifndef T_FILE_PHYSIC_IBPSTREEELEMENT_HPP_
#define T_FILE_PHYSIC_IBPSTREEELEMENT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "CPhysicEngine.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class Impact;
class ICollisionVolume;


/**
 * \class   IBSPTreeElement
 * \ingroup Physic
 * \brief   Élément (nœud ou feuille) d'un arbre BSP.
 ******************************/

class T_PHYSIC_API IBSPTreeElement
{
public:

    // Constructeur et destructeur
    IBSPTreeElement();
    virtual ~IBSPTreeElement() = 0;

    // Accesseurs
    virtual bool isLeaf() const = 0;
    virtual bool isNode() const = 0;

    // Méthodes publiques
    virtual TContentType getContentType(const TVector3F& position) const = 0;
    virtual bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const = 0;
    virtual void getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const = 0;
    virtual bool hasImpact(const CRay& ray) const = 0;
    virtual bool getRayImpact(const CRay& ray, CRayImpact& impact) const = 0;
    virtual bool CheckTree() = 0;

#ifdef T_DEBUG
    virtual void Debug_LogTree(unsigned int offset = 0) const = 0;
#endif
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_IBPSTREEELEMENT_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBoundingSphere.hpp
 * \date 02/02/2010 Création de la classe CBoundingSphere.
 * \date 11/07/2010 La classe CSphereBox s'appelle désormais CBoundingSphere.
 *                  Test d'intersection avec une CBoundingBox ou un CTriangle.
 */

#ifndef T_FILE_PHYSIC_CBOUNDINGSPHERE_HPP_
#define T_FILE_PHYSIC_CBOUNDINGSPHERE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "ICollisionVolume.hpp"


namespace Ted
{

class CBoundingBox;
class CHitBox;
class CTriangle;


/**
 * \class   CBoundingSphere
 * \ingroup Physic
 * \brief   Volume englobant sphérique.
 *
 * Une sphère englobante est le volume englobant le plus simple à mettre en
 * place, et le plus rapide dans la détection des collisions. En contre-partie,
 * il est moins précis qu'une boite orientée ou alignée sur les axes.
 ******************************/

class T_PHYSIC_API CBoundingSphere : public ICollisionVolume
{
public:

    // Constructeur
    CBoundingSphere(const TVector3F& center, float radius);

    // Accesseurs
    TVector3F getPosition() const;
    float getRadius() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& v);
    void setRadius(float radius);

    // Méthodes publiques
    float getOffset(const CPlane& plane) const;
    TContentType getContentType (const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    bool hasImpact(const CRay& ray) const;
    CRayImpact getRayImpact(const CRay& ray) const;
    bool isIntersecting(const CBoundingBox& box) const;
    bool isIntersecting(const CBoundingSphere& sphere) const;
    bool isIntersecting(const CHitBox& box) const;
    bool isIntersecting(const CTriangle& triangle) const;

protected:

    // Données protégées
    TVector3F m_center; ///< Centre de la sphère.
    float m_radius;     ///< Rayon de la sphère.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CBOUNDINGSPHERE_HPP_

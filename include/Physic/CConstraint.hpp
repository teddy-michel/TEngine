/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CConstraint.hpp
 * \date 05/07/2010 Création de la classe CConstraint.
 */

#ifndef T_USING_PHYSIC_BULLET

#ifndef T_FILE_PHYSIC_CCONSTRAINT_HPP_
#define T_FILE_PHYSIC_CCONSTRAINT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"


namespace Ted
{

class IModel;


/**
 * \class   CConstraint
 * \ingroup Physic
 * \brief   Une contrainte (ou joint) permet de relier deux modèles entre eux.
 ******************************/

class T_PHYSIC_API CConstraint
{
public:

    // Constructeur
    CConstraint();

    // Accesseurs
    IModel * getModel1() const;
    IModel * getModel2() const;

protected:

    // Données protégées
    IModel * m_model1; ///< Pointeur sur le premier modèle.
    IModel * m_model2; ///< Pointeur sur le second modèle.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CCONSTRAINT_HPP_

#endif

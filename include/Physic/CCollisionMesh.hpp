/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CCollisionMesh.hpp
 * \date 28/06/2010 Création de la classe CCollisionMesh.
 * \date 05/07/2010 Les triangles contiennent des indices à la place de vecteurs.
 * \date 11/07/2010 Création de la structure CTriangle.
 */

#ifndef T_FILE_PHYSIC_CCOLLISIONMESH_HPP_
#define T_FILE_PHYSIC_CCOLLISIONMESH_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "ICollisionVolume.hpp"


namespace Ted
{

/**
 * \struct  CTriangle
 * \ingroup Physic
 * \brief   Triangle représentant une face d'un maillage.
 ******************************/

struct T_PHYSIC_API CTriangle
{
    TVector3F s1; ///< Premier sommet.
    TVector3F s2; ///< Deuxième sommet.
    TVector3F s3; ///< Troisième sommet.

    /// Constructeur par défaut.
    CTriangle(const TVector3F& p_s1, const TVector3F& p_s2, const TVector3F& p_s3) :
        s1 (p_s1), s2 (p_s2), s3 (p_s3) { }
};


/**
 * \class   CCollisionMesh
 * \ingroup Physic
 * \brief   Gestion d'un maillage utilisé pour les collisions (TriMesh).
 ******************************/

class T_PHYSIC_API CCollisionMesh : public ICollisionVolume
{
public:

    // Constructeur
    CCollisionMesh();

    // Position
    TVector3F getPosition() const;
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& v);

    // Méthodes publiques
    float getOffset(const CPlane& plane) const;
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;

protected:

    /// Un triangle est une face du maillage.
    struct TTriangle
    {
        unsigned int s1; ///< Premier sommet.
        unsigned int s2; ///< Deuxième sommet.
        unsigned int s3; ///< Troisième sommet.
    };

    // Donnée protégée
    std::vector<TVector3F> m_vertices;   ///< Tableau contenant les sommets du maillage.
    std::vector<TTriangle> m_triangles; ///< Tableau contenant les faces du maillage.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CCOLLISIONMESH_HPP_

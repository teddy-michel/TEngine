/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CRay.hpp
 * \date 04/07/2010 Création de la classe CRay.
 * \date 15/07/2010 Ajout des opérateurs de comparaison pour les impacts.
 */

#ifndef T_FILE_PHYSIC_CRAY_HPP_
#define T_FILE_PHYSIC_CRAY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

const float RAY_MAX_LENGTH = 10000.0f; ///< Longueur maximale d'un rayon.


/**
 * \class   CRay
 * \ingroup Physic
 * \brief   Représente un rayon utilisé pour les intersections.
 ******************************/

class T_PHYSIC_API CRay
{
public:

    TVector3F origin;    ///< Début du rayon.
    TVector3F direction; ///< Direction du rayon (vecteur unitaire de préférence).

    /**
     * Constructeur.
     *
     * \param p_origin Début du rayon.
     * \param p_direction Direction du rayon.
     ******************************/

    CRay(const TVector3F& p_origin, const TVector3F& p_direction) :
        origin (p_origin), direction (p_direction) {}
};


class IBaseAnimating;


/**
 * \class   CRayImpact
 * \ingroup Physic
 * \brief   Description de l'intersection d'un rayon avec l'environnement.
 ******************************/

class T_PHYSIC_API CRayImpact
{
public:

    TVector3F normale;       ///< Normale de l'impact.
    TVector3F point;         ///< Point d'impact.
    IBaseAnimating * entity; ///< Pointeur sur l'entité percutée (nul si l'impact se produit sur le monde).
    float delta;             ///< Paramètre de l'équation (ray.origin + delta * ray.direction = point).

    /**
     * Constructeur par défaut.
     *
     * \param p_normale Normale de l'impact.
     * \param p_point Point d'impact.
     * \param p_entity Pointeur sur l'entité percutée.
     * \param p_delta Paramètre.
     ******************************/

    CRayImpact (const TVector3F& p_normale = Origin3F, const TVector3F& p_point = Origin3F, IBaseAnimating * p_entity = nullptr, float p_delta = std::numeric_limits<float>::max()) :
        normale(p_normale), point(p_point), entity(p_entity), delta (p_delta) {}

    /**
     * Indique s'il y a impact.
     *
     * \return Booléen.
     ******************************/

    inline bool isImpact() const
    {
        return (entity || normale != Origin3F);
    }

    /**
     * Opérateur de comparaison (<).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator<(const CRayImpact& impact)
    {
        return (delta < impact.delta);
    }

    /**
     * Opérateur de comparaison (>).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator>(const CRayImpact& impact)
    {
        return (delta > impact.delta);
    }

    /**
     * Opérateur de comparaison (<=).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator<=(const CRayImpact& impact)
    {
        return (delta < impact.delta);
    }

    /**
     * Opérateur de comparaison (>=).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator>=(const CRayImpact& impact)
    {
        return (delta > impact.delta);
    }

    /**
     * Opérateur de comparaison (==).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator==(const CRayImpact& impact)
    {
        return (std::abs(delta - impact.delta) < std::numeric_limits<float>::epsilon());
    }

    /**
     * Opérateur de comparaison (!=).
     * Remarque : Les rayons à l'origine des deux impacts doivent être les mêmes,
     * sans quoi cet opérateur n'aurait aucun sens.
     *
     * \param impact Impact à comparer.
     * \return Booléen.
     ******************************/

    inline bool operator!=(const CRayImpact& impact)
    {
        return (std::abs(delta - impact.delta) > std::numeric_limits<float>::epsilon());
    }
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CRAY_HPP_

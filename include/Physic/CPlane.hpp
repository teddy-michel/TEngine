/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CPlane.hpp
 * \date 08/02/2010 Création de la classe CPlane.
 * \date 14/07/2010 Constructeur à partir de trois points.
 */

#ifndef T_FILE_PHYSIC_CPLANE_HPP_
#define T_FILE_PHYSIC_CPLANE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   CPlane
 * \ingroup Physic
 * \brief   Description d'un plan dans l'espace.
 ******************************/

class T_PHYSIC_API CPlane
{
public:

    // Constructeurs
    CPlane(const TVector3F& normale = TVector3F(0.0f, 0.0f, 1.0f), float distance = 0.0f);
    CPlane(const TVector3F& pt1, const TVector3F& pt2, const TVector3F& pt3);

    // Accesseurs
    TVector3F getNormale() const;
    float getDistance() const;

    // Mutateurs
    void setNormale(const TVector3F& normale);
    void setDistance(float distance);

    // Méthode publique
    float getDistance(const TVector3F& point) const;

protected:

    // Données protégées
    TVector3F m_normale; ///< Vecteur normale.
    float m_distance;    ///< Distance à l'origine.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CPLANE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/Impact.hpp
 * \date 11/05/2009 Création de la classe Impact.
 */

#ifndef T_FILE_PHYSIC_IMPACT_HPP_
#define T_FILE_PHYSIC_IMPACT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   Impact
 * \ingroup Physic
 * \brief   Gestion d'une collision.
 ******************************/

class T_PHYSIC_API Impact
{
public:

    // Constructeur
    Impact();

    // Accesseurs
    TVector3F getPointFrom() const;
    TVector3F getPointTo() const;
    TVector3F getNormale() const;
    float getFracImpact() const;
    float getFracReelle() const;

    // Mutateurs
    void setPointFrom(const TVector3F& from);
    void setPointTo(const TVector3F& to);
    void setNormale(const TVector3F& normale);
    void setFracImpact(float frac_impact);
    void setFracReelle(float frac_reelle);

    // Méthodes publiques
    void Reset(const TVector3F& position_debut, const TVector3F& position_fin);
    void clearImpact();
    bool isImpact() const;
    void setImpact(const TVector3F& normal_impact, float fraction_impact, float fraction_reelle);
    void setNearerImpact(const TVector3F& normal_impact, float fraction_impact, float fraction_reelle);

protected:

    // Données protégées
    TVector3F m_point_from; ///< Début du segment.
    TVector3F m_point_to;   ///< Fin du segment.
    TVector3F m_normale;    ///< Normale au point d'impact.
    float m_frac_impact;    ///< Fraction d'impact.
    float m_frac_reelle;    ///< Fraction réelle d'impact.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_IMPACT_HPP_

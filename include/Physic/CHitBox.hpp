/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CHitBox.hpp
 * \date 12/02/2010 Création de la classe CHitBox.
 * \date 12/07/2010 L'orientation de la boite est représentée par un quaternion.
 *                  Ajout des tests d'intersection avec les autres volumes englobants.
 */

#ifndef T_FILE_PHYSIC_CHITBOX_HPP_
#define T_FILE_PHYSIC_CHITBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Export.hpp"
#include "Physic/ICollisionVolume.hpp"
#include "Core/Maths/CQuaternion.hpp"


namespace Ted
{

class CBoundingBox;
class CBoundingSphere;
class CTriangle;


/**
 * \class   CHitBox
 * \ingroup Physic
 * \brief   Une hit box est une boite orientée.
 *
 * Une hit box est similaire à une bounding box, mais elle peut en plus subir
 * des rotations.
 * Ce volume est principalement utilisée pour déterminer l'intersection entre un
 * rayon et un modèle. Après avoir déterminer l'intersection entre la boite non
 * orientée du modèle et le rayon, on affine la recherche en testant chacune
 * des boites orientées du modèle.
 ******************************/

class T_PHYSIC_API CHitBox : public ICollisionVolume
{
public:

    // Constructeur et destructeur
    CHitBox();
    virtual ~CHitBox();

    // Accesseurs
    TVector3F getPosition() const;
    CQuaternion getAngles() const;
    float getWidth() const;
    float getLength() const;
    float getHeight() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& v);
    void setAngles(const CQuaternion& angles);
    void setWidth(float width);
    void setLength(float length);
    void setHeight(float height);
    void setSize(float width, float length, float height);

    // Méthodes publiques
    float getOffset(const CPlane& plane) const;
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    bool hasImpact(const CRay& ray) const;
    CRayImpact getRayImpact(const CRay& ray) const;
    bool isIntersecting(const CBoundingBox& box) const;
    bool isIntersecting(const CBoundingSphere& sphere) const;
    bool isIntersecting(const CHitBox& box) const;
    bool isIntersecting(const CTriangle& triangle) const;

protected:

    // Données protégées
    TVector3F m_position; ///< Centre de la boite.
    CQuaternion m_angles; ///< Orientation de la boite.
    float m_width;        ///< Largeur de la boite.
    float m_length;       ///< Longueur de la boite.
    float m_height;       ///< Hauteur de la boite.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CHITBOX_HPP_

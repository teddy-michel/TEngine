/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/HTTP.hpp
 * \date 25/05/2009 Création de la classe HTTP.
 */

#ifndef T_FILE_NETWORK_HTTP_HPP_
#define T_FILE_NETWORK_HTTP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>
#include <string>

#include "Network/Export.hpp"
#include "Network/CIPAddress.hpp"
#include "Network/CSocketTCP.hpp"


namespace Ted
{

/**
 * \class   HTTP
 * \ingroup Network
 *
 *  This class provides methods for manipulating the HTTP protocol (described in RFC 1945).
 *  It can connect to a website, get its files, send requests, etc.
 ******************************/

class T_NETWORK_API HTTP
{
public:

    /**
     * \class   Request
     * \ingroup Network
     *
     *  This class wraps an HTTP request, which is basically :
     *  - a header with a method, a target URI, and a set of field/value pairs
     *  - an optional body (for POST requests)
     ******************************/

    class Request
    {
    public:

        /// Enumerate the available HTTP methods for a request.
        enum TMethod
        {
            Get,  ///< Request in get mode, standard method to retrieve a page.
            Post, ///< Request in post mode, usually to send data to a page.
            Head  ///< Request a page's header only.
        };

        // Constructor
        Request(TMethod method = Get, const CString& URI = "/", const CString& body = CString());

        // Public methods
        void setField(const CString& field, const CString& value);
        void setMethod(TMethod method);
        void setURI(const CString& URI);
        void setHTTPVersion(unsigned int major, unsigned int minor);
        void setBody(const CString& body);

    private:

        friend class HTTP;

        // Private methods
        CString toString() const;
        bool hasField(const CString& field) const;

        // Private members
        std::map<CString, CString> m_fields; ///< Fields of the header.
        TMethod m_method;             ///< Method to use for the request.
        CString m_URI;                ///< Target URI of the request.
        unsigned int m_major_version; ///< Major HTTP version.
        unsigned int m_minor_version; ///< Minor HTTP version.
        CString m_body;               ///< Body of the request.
    };


    /**
     * \class   Response
     * \ingroup Network
     *
     *  This class wraps an HTTP response, which is basically :
     *  - a header with a status code and a set of field/value pairs
     *  - a body (the content of the requested resource)
     ******************************/

    class Response
    {
    public :

        /// Enumerate all the valid status codes returned in a HTTP response
        enum Status
        {
            // 2xx: success
            OK                  = 200, ///< Most common code returned when operation was successful
            Created             = 201, ///< The resource has successfully been created
            Accepted            = 202, ///< The request has been accepted, but will be processed later by the server
            NoContent           = 204, ///< Sent when the server didn't send any data in return

            // 3xx: redirection
            MultipleChoices     = 300, ///< The requested page can be accessed from several locations
            MovedPermanently    = 301, ///< The requested page has permanently moved to a new location
            MovedTemporarily    = 302, ///< The requested page has temporarily moved to a new location
            NotModified         = 304, ///< For conditionnal requests, means the requested page hasn't changed and doesn't need to be refreshed

            // 4xx: client error
            BadRequest          = 400, ///< The server couldn't understand the request (syntax error)
            Unauthorized        = 401, ///< The requested page needs an authentification to be accessed
            Forbidden           = 403, ///< The requested page cannot be accessed at all, even with authentification
            NotFound            = 404, ///< The requested page doesn't exist

            // 5xx: server error
            InternalServerError = 500, ///< The server encountered an unexpected error
            NotImplemented      = 501, ///< The server doesn't implement a requested feature
            BadGateway          = 502, ///< The gateway server has received an error from the source server
            ServiceNotAvailable = 503, ///< The server is temporarily unavailable (overloaded, in maintenance, ...)

            // 10xx: SFML custom codes
            InvalidResponse     = 1000,///< Response is not a valid HTTP one
            ConnectionFailed    = 1001 ///< Connection with server failed.
        };

        // Constructor
        Response();

        // Public methods
        const CString& getField(const CString& field) const;
        Status getStatus() const;
        unsigned int getMajorHTTPVersion() const;
        unsigned int getMinorHTTPVersion() const;
        const CString& getBody() const;

    private :

        friend class HTTP;

        // Private method
        void FromString(const CString& data);

        // Private members
        std::map<CString, CString> m_fields; ///< Fields of the header.
        Status m_status;              ///< Status code.
        unsigned int m_major_version; ///< Major HTTP version.
        unsigned int m_minor_version; ///< Minor HTTP version.
        CString m_body;               ///< Body of the response.
    };

    // Constructors
    HTTP();
    HTTP(const CString& host, uint16_t port = 0);

    // Public methods
    void setHost(const CString& host, uint16_t port = 0);
    Response SendRequest(const Request& req);

private:

    // Private members
    CSocketTCP m_connection; ///< Connection to the host.
    CIPAddress m_host;       ///< Web host address.
    CString m_host_name;     ///< Web host name.
    uint16_t m_port;         ///< Port used for connection with host.
};

} // Namespace Ted

#endif // T_FILE_NETWORK_HTTP_HPP_

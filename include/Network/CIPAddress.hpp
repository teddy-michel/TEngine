/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/CIPAddress.hpp
 * \date 25/05/2009 Création de la classe CIPAdress.
 * \date 14/03/2011 Inclusion de stdint.h.
 */

#ifndef T_FILE_NETWORK_CIPADDRESS_HPP_
#define T_FILE_NETWORK_CIPADDRESS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <istream>
#include <ostream>
#include <string>
#include <stdint.h>

#include "Network/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CIPAddress
 * \ingroup Network
 * \brief   Permet de manipuler les adresses IP v4.
 ******************************/

class T_NETWORK_API CIPAddress
{
public :

    // Constructeurs
    CIPAddress();
    CIPAddress(const CString& addr);
    CIPAddress(const char * addr);
    CIPAddress(unsigned char byte0, unsigned char byte1, unsigned char byte2, unsigned char byte3);
    CIPAddress(uint32_t addr);

    // Public methods
    bool isValid() const;
    CString toString() const;
    uint32_t toInteger() const;
    static CIPAddress getLocalAddress();
    static CIPAddress getPublicAddress();

    // Operators
    bool operator==(const CIPAddress& addr) const;
    bool operator!=(const CIPAddress& addr) const;
    bool operator<(const CIPAddress& addr) const;
    bool operator>(const CIPAddress& addr) const;
    bool operator<=(const CIPAddress& addr) const;
    bool operator>=(const CIPAddress& addr) const;

    // Static member
    static const CIPAddress LocalHost; ///< Local host address (to connect to the same computer).

protected:

    // Protected member
    uint32_t m_addr; ///< Address stored as an unsigned 32 bits integer.
};

std::istream& operator>>(std::istream& stream, CIPAddress& addr);
std::ostream& operator<<(std::ostream& stream, const CIPAddress& addr);

} // Namespace Ted

#endif // T_FILE_NETWORK_CIPADDRESS_HPP_

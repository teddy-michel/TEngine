/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CClient.hpp
 * \date 24/05/2009 Création de la classe CClient.
 */

#ifndef T_FILE_NETWORK_CCLIENT_HPP_
#define T_FILE_NETWORK_CCLIENT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Network/Export.hpp"


namespace Ted
{

class CServer;

/**
 * \class   CClient
 * \ingroup Network
 * \brief   Gestion d'un client connecté à un serveur.
 ******************************/

class T_NETWORK_API CClient
{
public:

    // Constructeur et destructeur
    CClient();
    ~CClient();

    // Méthodes publiques
    CServer * getServer() const;
    void setServer(CServer * server);

protected:

    // Donnée protégée
    CServer * m_server; ///< Pointeur sur le serveur auquel le client est connecté.
};

} // Namespace Ted

#endif // T_FILE_NETWORK_CCLIENT_HPP_

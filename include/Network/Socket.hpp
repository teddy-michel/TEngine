/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/Socket.hpp
 * \date       2008 Création des fonctions pour manipuler les sockets.
 */

#ifndef T_FILE_NETWORK_SOCKET_HPP_
#define T_FILE_NETWORK_SOCKET_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "os.h"

#if defined T_SYSTEM_WINDOWS
#  include <winsock2.h>
#elif defined T_SYSTEM_LINUX
#  include <sys/types.h>
#  include <sys/socket.h>
#  include <netinet/in.h>
#  include <arpa/inet.h>
#  include <unistd.h>
#endif


/*-----------------------------------------------------*
 *   Définitions de constantes et de types pour Linux  *
 *-----------------------------------------------------*/

#if defined T_SYSTEM_LINUX
#  define INVALID_SOCKET -1
#  define SOCKET_ERROR   -1
   typedef int SOCKET;
   typedef struct sockaddr_in SOCKADDR_IN;
   typedef struct sockaddr SOCKADDR;
#endif


namespace Ted
{

/**
 * \namespace Socket
 * \ingroup   Network
 * \brief     Fonctions utiles à la gestion des sockets.
 * \todo      Supprimer et utiliser la SFML.
 ******************************/

namespace Socket
{
    /// Enumeration of status returned by socket functions.
    enum Status
    {
        Done,         ///< The socket has sent / received the data.
        NotReady,     ///< The socket is not ready to send / receive data yet.
        Disconnected, ///< The TCP socket has been disconnected.
        Error         ///< An unexpected error happened.
    };

    // Méthodes statiques
    void initialise();
    void clean();
    Status getErrorStatus();
}

} // Namespace Ted

#endif // T_FILE_NETWORK_SOCKET_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file CSocketTCP.hpp
 * \date       2008 Création de la classe CSocketTCP.
 */

#ifndef T_FILE_NETWORK_CSOCKETTCP_HPP_
#define T_FILE_NETWORK_CSOCKETTCP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Network/Export.hpp"
#include "Network/Socket.hpp"


namespace Ted
{

class CPacket;
class CIPAddress;


/**
 * \class   CSocketTCP
 * \ingroup Network
 * \brief   Utilisation des sockets TCP.
 ******************************/

class T_NETWORK_API CSocketTCP
{
public:

    // Constructeur
    CSocketTCP();

    // Méthodes publiques
    void setBlocking(bool blocking = true);
    Socket::Status connect(uint16_t port, const CIPAddress& host, float timeout = 0.0f);
    bool listen(uint16_t port);
    Socket::Status accept(CSocketTCP& connected, CIPAddress * addr = nullptr);
    Socket::Status send(const char * data, std::size_t size);
    Socket::Status receive(char * data, std::size_t max_size, std::size_t& size_received);
    Socket::Status send(CPacket& packet);
    Socket::Status receive(CPacket& packet);
    bool close();
    bool isValid() const;

    // Opérateurs
    bool operator==(const CSocketTCP& socket) const;
    bool operator!=(const CSocketTCP& socket) const;
    bool operator<(const CSocketTCP& socket) const;

private:

    // Constructeur privée
    CSocketTCP(SOCKET sock);

    // Méthode privée
    void create(SOCKET sock = INVALID_SOCKET);

    // Données privées
    SOCKET m_socket;                    ///< Socket descriptor.
    std::vector<char> m_pending_packet; ///< Data of the current pending packet, if any (in non-blocking mode)
    int32_t m_pending_packet_size;      ///< Size of the current pending packet, if any (in non-blocking mode)
    bool m_blocking;                    ///< Is the socket blocking or non-blocking ?
};

} // Namespace Ted

#endif // T_FILE_NETWORK_CSOCKETTCP_HPP_

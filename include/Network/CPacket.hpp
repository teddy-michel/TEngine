/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/CPacket.hpp
 * \date 25/05/2009 Création de la classe CPacket.
 */

#ifndef T_FILE_NETWORK_CPACKET_HPP_
#define T_FILE_NETWORK_CPACKET_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <string>
#include <stdint.h>

#include "Network/Export.hpp"


namespace Ted
{

/**
 * \class   CPacket
 * \ingroup Network
 * \brief   Packet wraps data to send / to receive through the network.
 ******************************/

class T_NETWORK_API CPacket
{
public:

    // Constructeur et destructeur
    CPacket();
    virtual ~CPacket();

    // Méthodes publiques
    void append(const void * data, std::size_t sizeInBytes);
    void clear();
    const char * getData() const;
    std::size_t getDataSize() const;
    bool endOfPacket() const;

    // Opérateurs
    operator bool() const;

    CPacket& operator>>(bool& data);
    CPacket& operator>>(int8_t& data);
    CPacket& operator>>(uint8_t& data);
    CPacket& operator>>(int16_t& data);
    CPacket& operator>>(uint16_t& data);
    CPacket& operator>>(int32_t& data);
    CPacket& operator>>(uint32_t& data);
    CPacket& operator>>(float& data);
    CPacket& operator>>(double& data);
    CPacket& operator>>(char * data);
    CPacket& operator>>(std::string& data);
    CPacket& operator>>(wchar_t * data);
    CPacket& operator>>(std::wstring& data);

    CPacket& operator<<(bool data);
    CPacket& operator<<(int8_t data);
    CPacket& operator<<(uint8_t data);
    CPacket& operator<<(int16_t data);
    CPacket& operator<<(uint16_t data);
    CPacket& operator<<(int32_t data);
    CPacket& operator<<(uint32_t data);
    CPacket& operator<<(float data);
    CPacket& operator<<(double data);
    CPacket& operator<<(const char * data);
    CPacket& operator<<(const std::string& data);
    CPacket& operator<<(const wchar_t * data);
    CPacket& operator<<(const std::wstring& data);

protected:

    friend class CSocketTCP;
    friend class CSocketUDP;

    // Méthodes protégées
    bool checkSize(std::size_t size);
    virtual const char * onSend(std::size_t& dataSize);
    virtual void onReceive(const char * data, std::size_t dataSize);

    // Données protégées
    std::vector<char> m_data;   ///< Data stored in the packet
    std::size_t       m_cursor; ///< Current reading position in the packet
    bool              m_valid;  ///< Reading state of the packet
};

} // Namespace Ted

#endif // T_FILE_NETWORK_CPACKET_HPP_

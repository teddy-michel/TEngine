/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CSocketUDP.hpp
 * \date       2008 Création de la classe CSocketUDP.
 */

#ifndef T_FILE_NETWORK_CSOCKETUDP_HPP_
#define T_FILE_NETWORK_CSOCKETUDP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Network/Export.hpp"
#include "Network/Socket.hpp"


namespace Ted
{

class CPacket;
class CIPAddress;

/**
 * \class   CSocketUDP
 * \ingroup Network
 * \brief   Utilisation des sockets UDP.
 ******************************/

class T_NETWORK_API CSocketUDP
{
public:

    // Constructeur
    CSocketUDP();

    // Accesseur
    uint16_t getPort() const;

    // Méthodes publiques
    void setBlocking(bool blocking = true);
    bool bind(uint16_t port);
    bool unbind();
    Socket::Status send(const char * data, std::size_t size, const CIPAddress& addr, uint16_t port);
    Socket::Status send(CPacket& paquet, CIPAddress& addr, uint16_t port);
    Socket::Status receive(char * data, std::size_t maxsize, std::size_t& sizereceived, CIPAddress& addr);
    Socket::Status receive(CPacket& paquet, CIPAddress& addr);
    bool close();
    bool isValid() const;

protected:

    // Méthode protégée
    void create(SOCKET socket = INVALID_SOCKET);

    // Protected members
    SOCKET m_socket;                    ///< Socket
    uint16_t m_port;                    ///< Port to which the socket is bound
    std::vector<char> m_pending_packet; ///< Data of the current pending packet, if any (in non-blocking mode)
    int32_t m_pending_packet_size;      ///< Size of the current pending packet, if any (in non-blocking mode)
    bool m_blocking;                    ///< Is the socket blocking or non-blocking ?
};

} // Namespace Ted

#endif // T_FILE_NETWORK_CSOCKETUDP_HPP_

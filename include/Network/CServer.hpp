/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CServer.hpp
 * \date 20/05/2009 Création de la classe CServer.
 */

#ifndef T_FILE_NETWORK_CSERVER_HPP_
#define T_FILE_NETWORK_CSERVER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Network/Export.hpp"


namespace Ted
{

/**
 * \class   CServer
 * \ingroup Network
 * \brief   Gestion d'un serveur.
 ******************************/

class T_NETWORK_API CServer
{
public:

    // Constructeur et destructeur
    CServer();
    ~CServer();

    // Méthodes publique
    unsigned int getNumClients() const;
    void addClient(int client);
    void deleteClient( int client);
    void clearClients();

protected:

    // Donnée protégée
    std::vector<int> m_clients; ///< Liste des clients connectés au serveur
};

} // Namespace Ted

#endif // T_FILE_NETWORK_CSERVER_HPP_

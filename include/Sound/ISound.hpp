/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/ISound.hpp
 * \date 03/12/2010 Création de la classe ISound.
 */

#ifndef T_FILE_SOUND_ISOUND_HPP_
#define T_FILE_SOUND_ISOUND_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <AL/al.h>

#include "Sound/Export.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   ISound
 * \ingroup Sound
 * \brief   Classe générale pour gérer les ressources sonores.
 ******************************/

class T_SOUND_API ISound
{
public:

    // Constructeur et destructeur
    ISound();
    virtual ~ISound();

    // Accesseurs
    CString getFileName() const;
    ALuint getSource() const;
    ALsizei getNumSamples() const;
    ALsizei getSampleRate() const;
    bool isPlaying() const;
    bool isPaused() const;
    bool isStopping() const;
    bool isLoop() const;
    float getPitch() const;
    float getVolume() const;
    TVector3F getPosition() const;
    float getMinimaleDistance() const;
    float getAttenuation() const;
    float getPlayingOffset() const;

    // Mutateurs
    void setLoop(bool loop = true);
    void setPitch(float pitch);
    void setVolume(float volume = 100.0f);
    void setPosition(const TVector3F &position);
    void setMinimaleDistance(float distance);
    void setAttenuation(float attenuation);
    void setPlayingOffset(float offset);

    // Méthodes publiques
    virtual bool loadFromFile(const CString& fileName) = 0;
    bool play();
    bool pause();
    bool stop();

    /// Conversion en booléen.
    inline operator bool() const
    {
        return m_loaded;
    }

protected:

    // Données protégées
    CString m_fileName;   ///< Adresse du fichier contenant le son.
    bool m_loop;          ///< Indique si le son doit être joué en boucle.
    bool m_loaded;        ///< Indique si le son est correctement chargé.
    bool m_play;          ///< Indique si le son est en cours de lecture.
    ALuint m_source;      ///< Source.
    ALsizei m_nbrSamples; ///< Nombre d'échantillons.
    ALsizei m_sampleRate; ///< Taux d'échantillonage.
};

} // Namespace Ted

#endif // T_FILE_SOUND_ISOUND_HPP_

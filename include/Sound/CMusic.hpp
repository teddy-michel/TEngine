/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CMusic.hpp
 * \date 25/04/2009 Création de la classe CMusic.
 * \date 11/07/2010 On peut modifier le nom du fichier.
 * \date 16/07/2010 Conversion en booléen.
 * \date 03/12/2010 CSound hérite de la classe ISound.
 * \date 04/12/2010 On peut charger une musique depuis un fichier ou depuis une vidéo.
 */

#ifndef T_FILE_SOUND_CMUSIC_HPP_
#define T_FILE_SOUND_CMUSIC_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sndfile.h>
#include <string>

#include "Sound/Export.hpp"
#include "os.h"

#ifdef T_SYSTEM_WINDOWS
#  include <vfw.h>
#endif

#include "Sound/ISound.hpp"


namespace Ted
{

/**
 * \class   CMusic
 * \ingroup Sound
 * \brief   Classe permettant de gérer une musique.
 ******************************/

class T_SOUND_API CMusic : public ISound
{
public:

    // Constructeur et destructeur
    CMusic();
    virtual ~CMusic();

    // Méthodes publiques
    const ALuint * getBuffers() const;
    void update();
    bool loadFromFile(const CString& fileName);
    bool loadFromVideo(const CString& fileName);

protected:

    // Données protégées
    ALuint m_buffer[2];     ///< Buffers contenant le son.
    ALenum m_format;        ///< Format du son.
    SNDFILE * m_file;       ///< Fichier contenant la musique.
    bool m_fromVideo;
#ifdef T_SYSTEM_WINDOWS
    PAVISTREAM m_aviStream; ///< Flux destiné à la lecture du son.
    LONG m_sampleCount;     ///< Nombre de samples déjà lu (pour les vidéos).
#endif

    // Méthode protégée
    bool readData(ALuint buffer, ALsizei nbr_samples);
};

} // Namespace Ted

#endif // T_FILE_SOUND_CMUSIC_HPP_

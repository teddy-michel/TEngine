/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CCapture.hpp
 * \date 24/05/2009 Création de la classe CCapture.
 */

#ifndef T_FILE_SOUND_CCAPTURE_HPP_
#define T_FILE_SOUND_CCAPTURE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>
#include <vector>
#include <AL/al.h>
#include <AL/alc.h>

#include "Sound/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CCapture
 * \ingroup Sound
 * \brief   Classe représentant une capture audio.
 ******************************/

class T_SOUND_API CCapture
{
public:

    // Constructeur et destructeur
    CCapture();
    ~CCapture();

    // Accesseurs
    bool isStarted() const;
    bool isStoped() const;

    // Méthodes publiques
    bool initCapture(ALCdevice * device);
    void start();
    void update();
    void stop();
    void saveToFile(const CString& fileName);

protected:

    // Donnée protégée
    ALCdevice * m_device;           ///< Pointeur sur le device de capture.
    bool m_started;                 ///< Booléen indiquant si la capture est en cours.
    std::vector<ALshort> m_samples; ///< Liste des échantillons.
};

} // Namespace Ted

#endif // T_FILE_SOUND_CCAPTURE_HPP_

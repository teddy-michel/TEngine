/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CSoundEngine.hpp
 * \date       2008 Création de la classe CSoundEngine.
 * \date 11/07/2010 On peut ajouter et supprimer un son ou une musique.
 * \date 03/12/2010 PlaySound et PlayMusic retourne le pointeur sur la ressource.
 * \date 02/03/2011 Création de la méthode SaveCapture.
 */

#ifndef T_FILE_SOUND_CSOUNDENGINE_HPP_
#define T_FILE_SOUND_CSOUNDENGINE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>
#include <AL/alc.h>

#include "Sound/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class CSoundEngine;
class CMusic;
class CSound;
class CCapture;


namespace Game
{
    extern T_SOUND_API CSoundEngine * soundEngine;
}


/**
 * \class   CSoundEngine
 * \ingroup Sound
 * \brief   Moteur de son.
 ******************************/

class T_SOUND_API CSoundEngine
{
public:

    // Constructeur et destructeur
    CSoundEngine();
    ~CSoundEngine();

    // Mutateurs
    static void setVolume(float volume);
    static void setPosition(const TVector3F& position);
    static void setDirection(const TVector3F& direction);
    static void setVelocity(const TVector3F& velocity);

    // Méthodes publiques
    bool initOpenAL();
    void addSound(CSound * sound);
    void addMusic(CMusic * music);
    void removeSound(CSound * sound);
    void removeMusic(CMusic * music);
    CSound * playSound(const CString& fileName);
    CMusic * playMusic(const CString& fileName);
    void frame();
    void deleteSounds();
    void deleteMusics();
    void startCapture();
    void stopCapture();
    void saveCapture(const CString& fileName);

    // Méthodes statiques
    static void getDevices(std::vector<CString>& devices);
    static void getCaptureDevices(std::vector<CString>& devices);
    static void shutdownOpenAL();
    static bool isOALExtension(const CString& extension);
    static void beep(unsigned short frequency, unsigned short duration);
    static void displayOpenALError(int error, const CString& msg = CString(), unsigned int line = 0);

protected:

    // Données protégées
    CCapture * m_capture;         ///< Pointeur sur la capture audio.
    ALCdevice * m_device;         ///< Pointeur sur le device de lecture.
    std::list<CSound *> m_sounds; ///< Sons en cours de lecture.
    std::list<CMusic *> m_musics; ///< Musiques en cours de lecture.
};

} // Namespace Ted

#endif // T_FILE_SOUND_CSOUNDENGINE_HPP_

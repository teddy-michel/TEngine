/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CSound.hpp
 * \date       2008 Création de la classe CSound.
 * \date 11/07/2010 On peut modifier le nom du fichier.
 * \date 16/07/2010 Conversion en booléen.
 * \date 03/12/2010 CSound hérite de la classe ISound.
 */

#ifndef T_FILE_SOUND_CSOUND_HPP_
#define T_FILE_SOUND_CSOUND_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>

#include "Sound/Export.hpp"
#include "Sound/ISound.hpp"


namespace Ted
{

/**
 * \class   CSound
 * \ingroup Sound
 * \brief   Classe permettant de gérer un son.
 *
 * Un son est entièrement chargé en mémoire, contrairement à une musique.
 ******************************/

class T_SOUND_API CSound : public ISound
{
public:

    // Constructeur et destructeur
    CSound();
    ~CSound();

    // Méthodes publiques
    ALuint getBuffer() const;
    void setLoop(bool loop = true);
    bool loadFromFile(const CString& fileName);

protected:

    // Donnée protégée
    ALuint m_buffer; ///< Buffer contenant le son.
};

} // Namespace Ted

#endif // T_FILE_SOUND_CSOUND_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/ILoaderSound.hpp
 * \date       2008 Création de la classe ILoaderSound.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_SOUND_ILOADERSOUND_HPP_
#define T_FILE_SOUND_ILOADERSOUND_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Sound/Export.hpp"
#include "Core/ILoader.hpp"


namespace Ted
{

/**
 * \class   ILoaderSound
 * \ingroup Sound
 * \brief   Classe de base pour le chargement des sons.
 *
 * \todo Supprimer cette classe.
 ******************************/

class T_SOUND_API ILoaderSound : public ILoader
{
public:

    // Constructeur
    ILoaderSound();

    // Accesseur
    unsigned int getDuration() const;

protected:

    // Donnée protégée
    unsigned int m_duration; ///< Durée du son en millisecondes.
};

} // Namespace Ted

#endif // T_FILE_SOUND_ILOADERSOUND_HPP_


/**
 * \mainpage
 *
 * \section intro Introduction
 * TedEngine est un moteur de jeu utilisant l'API OpenGL, écrit en C++, et sous
 * license GPL.
 *
 * \section doc Documentation
 * Cette documentation a été générée par
 * <a href="http://www.doxygen.org">DOxygen</a> et permet d'avoir un aperçu
 * complet et détaillé des classes composant le moteur.
 *
 * \section copyright Copyright
 * Copyright (C) 2008-2015 Teddy Michel
 *
 * This file is part of TEngine.
 *
 * TEngine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TEngine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TEngine. If not, see <http://www.gnu.org/licenses/>.
 *
 * \section compil Options de compilation
 *
 * \li T_DEBUG : Pour compiler en mode débuguage (assertions vérifiées, logs,
 *     méthodes supplémentaires, etc.). Pour compiler avec les symboles de débuguage,
 *     il faut modifier les options du compilateur (-g pour GCC).
 * \li T_BIG_ENDIAN : si le système est en big endian.
 * \li T_PERFORMANCE : Active les tests de performance, qui mesurent le temps
 *     écoulé dans certaines sections critiques.
 * \li T_USING_MEMORY_MANAGER : Pour utiliser le gestionnaire de mémoire.
 * \li T_NO_COVARIANT_RETURN : Si votre compilateur ne supporte pas les types
 *     de retour covariants.
 * \li T_USING_PHYSIC_BULLET : Utilisation du moteur physique Bullet.
 * \li T_ACTIVE_DEBUG_MODE : Activer l'interface de debug dans le jeu. Cela permet
 *     d'afficher la liste des textures et des entités, et certaines mesures (FPS, mémoire).
 * \li T_NO_DEFAULT_LOGGER : Ne pas créer de logger par défaut lors du lancement de l'application.
 * \li T_TIME_64BITS : Utiliser un entier de 64 bits pour stocker les temps (32 bits par défaut).
 * \li T_CAMERA_FREEFLY_COLLISION : Activer les collisions avec la caméra FreeFly.
 * \li T_TEXTURES_KEEP_TIME : mémoriser le moment d'utilisation de chaque texture.
 * \li T_USE_FREETYPE : utiliser la bibliothèque FreeType pour charger les polices de caractères.
 * \li T_USE_PIXMAP : utiliser des pixmaps dans l'interface graphique.
 */

/**
 * \defgroup Core     Base du moteur et outils divers.
 * \defgroup Game     Données du jeu (maps et modèles).
 * \defgroup Graphic  Moteur graphique.
 * \defgroup Gui      Interface graphique.
 * \defgroup Physic   Moteur physique.
 * \defgroup Network  Gestion du réseau.
 * \defgroup Sound    Gestion des sons et des musiques.
 */

/**
 * \namespace Ted
 * \brief     Espace de nom principal du moteur.
 */

/**
 * \namespace Ted::Gui
 * \brief     Espace de nom des objets de l'interface graphique.
 */

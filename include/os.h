/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file os.h
 * \date 08/05/2009 Création du fichier.
 */

#ifndef T_FILE_OS_H_
#define T_FILE_OS_H_


/*--------------------------------------------*
 *   Identification du système d'exploitation *
 *--------------------------------------------*/

#if defined(_WIN32) || defined(__WIN32__)
#  define T_SYSTEM_WINDOWS
#elif defined(linux) || defined(__linux)
#  define T_SYSTEM_LINUX
#elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)
#  define T_SYSTEM_MAC
#else
#  error This operating system is not supported.
#endif

#endif // T_FILE_OS_H_

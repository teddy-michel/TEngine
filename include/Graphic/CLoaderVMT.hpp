/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderVMT.hpp
 * \date 10/01/2010 Création de la classe CLoaderVMT.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_GRAPHIC_CLOADERVMT_HPP_
#define T_FILE_GRAPHIC_CLOADERVMT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/Export.hpp"
#include "Core/ILoader.hpp"


namespace Ted
{

/**
 * \class   CLoaderVMT
 * \ingroup Graphic
 * \brief   Chargement des fichiers de matériaux VMT.
 ******************************/

class T_GRAPHIC_API CLoaderVMT : public ILoader
{
public:

    // Constructeur
    CLoaderVMT();

    // Accesseur
    CString getTexture() const;

    // Méthode publique
    bool loadFromFile(const CString& fileName);

private:

    /**
     * \enum    TShaderType
     * \ingroup Loaders
     * \brief   Types de fichiers VMT.
     ******************************/

    enum TShaderType
    {
        UNKNOWN,                ///< Type inconnu.
        CABLE,                  ///< Texture d'un cable.
        LIGHT_MAPPED_GENERIC,   ///< LightmappedGeneric.
        PATCH,                  ///< Patch.
        UNLIT_GENERIC,          ///< UnlitGeneric.
        UNLIT_TWO_TEXTURE,      ///< UnlitTwoTexture.
        VERTEXLIT_GENERIC,      ///< VertexlitGeneric.
        WATER,                  ///< Water.
        WORLD_VERTEX_TRANSITION ///< WorldVertexTransition.
    };

protected:

    // Donnée protégée
    CString m_texture; ///< Nom de la texture.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CLOADERVMT_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/IHUD.hpp
 * \date 13/02/2010 Création de la classe CHUD.
 * \date 25/01/2011 Transformation de la classe CHUD en IHUD virtuelle.
 */

#ifndef T_FILE_GRAPHIC_IHUD_HPP_
#define T_FILE_GRAPHIC_IHUD_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/Export.hpp"
#include "Core/INonCopyable.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

class CBuffer2D;


/**
 * \class   IHUD
 * \ingroup Graphic
 * \brief   Gestion d'un HUD (Head Up Display).
 *
 * Un HUD permet de donner des informations au joueur au cours du jeu. Il est
 * utilisé par exemple pour afficher la santé, le nombre de munitions, la
 * position du joueur sur une carte ou un radar, etc.
 *
 * Chaque nouveau composant doit être défini dans une classe dérivée de IHUD.
 * La méthode Update est appelée chaque fois que le composant a besoin d'être
 * modifié, typiquement à chaque nouvelle frame. La méthode Paint affiche le
 * buffer graphique du composant.
 ******************************/

class T_GRAPHIC_API IHUD : public INonCopyable
{
public:

    // Constructeur et destructeur
    IHUD();
    virtual ~IHUD();

    // Accesseurs
    CBuffer2D * getBuffer() const;
    CRectangle getRec() const;

    // Mutateurs
    void setX(int x);
    void setY(int y);
    void setPosition(int x, int y);
    void setWidth(int width);
    void setHeight(int height);
    void setSize(int width, int height);

    // Méthodes publiques
    virtual void update() = 0;
    virtual void paint() = 0;

protected:

    // Données protégées
    CBuffer2D * m_buffer; ///< Pointeur sur le buffer graphique.
    CRectangle m_rec;     ///< Dimensions du rectangle pour afficher le HUD.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_IHUD_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/ICamera.hpp
 * \date       2008 Création de la classe ICamera.
 */

#ifndef T_FILE_GRAPHIC_ICAMERA_HPP_
#define T_FILE_GRAPHIC_ICAMERA_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/Export.hpp"
#include "Core/IEventReceiver.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/*-------------------------------*
 *   Paramètres divers           *
 *-------------------------------*/

const float MOUSE_MOTION_SPEED = 0.2f;  ///< Vitesse de déplacement du curseur.
const unsigned char ZOOM_ANGLE = 20;    ///< Angle de vue minimal en zoom.
const unsigned char VIEW_ANGLE = 70;    ///< Angle de vue normal.
const unsigned int ZOOM_TIME = 500;     ///< Durée avant que le zoom soit complet.


/**
 * \class   ICamera
 * \ingroup Graphic
 * \brief   Classe de base des caméras.
 ******************************/

class T_GRAPHIC_API ICamera : public IEventReceiver
{
public:

    // Constructeur et destructeur
    ICamera(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));
    virtual ~ICamera() = 0;

    // Accesseurs
    TVector3F getPosition() const;
    TVector3F getDirection() const;
    float getZoom() const;
    unsigned char getAngle() const;
    unsigned char getDefaultAngle() const;

    // Mutateurs
    virtual void setPosition(const TVector3F& position);
    virtual void setDirection(const TVector3F& direction);
    void setZoom(float pourcentage);
    void setAngle(unsigned char angle);

    // Méthodes publiques
    virtual void look() const;
    virtual void animate(unsigned int frameTime) = 0;
    virtual void initialize(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));

protected:

    // Données protégées
    TVector3F m_position;  ///< Position de la caméra.
    TVector3F m_direction; ///< Direction de la caméra.
    unsigned short m_zoom; ///< Durée avant que le zoom soit complet en millisecondes.
    unsigned char m_angle; ///< Angle de vue.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_ICAMERA_HPP_

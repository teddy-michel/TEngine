/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CShaderLoader.hpp
 * \date 10/05/2009 Création de la classe CShaderLoader.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_LOADERS_CLOADERSHADER_HPP_
#define T_FILE_LOADERS_CLOADERSHADER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>
#include <Cg/cg.h>

#include "Graphic/Export.hpp"
#include "Core/ILoader.hpp"
#include "Graphic/CShader.hpp"


namespace Ted
{

/**
 * \class   CShaderLoader
 * \ingroup Graphic
 * \brief   Chargement des shaders.
 ******************************/

class T_GRAPHIC_API CShaderLoader : public ILoader
{
public:

    // Constructeur et destructeur
    CShaderLoader(TShaderType type);
    virtual ~CShaderLoader();

    inline CShader * getShader() const;
    bool loadFromFile(const CString& fileName);

private :

    CGcontext m_context; ///< Context Cg.
    TShaderType m_type;  ///< Type des shaders gérés.
    CShader * m_shader;  ///< Pointeur sur le shader chargé.
};


/**
 * Retourne le shader chargé.
 *
 * \return Pointeur sur le shader.
 ******************************/

inline CShader * CShaderLoader::getShader() const
{
    return m_shader;
}

} // Namespace Ted

#endif // T_FILE_LOADERS_CLOADERSHADER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CMetaTexture.hpp
 * \date 02/02/2010 Création de la classe CMetaTexture.
 * \date 12/12/2011 Chaque méta-texture porte un numéro unique.
 * \date 04/04/2012 Ajout d'un paramètre pour rendre les méta-textures statiques.
 */

#ifndef T_FILE_GRAPHIC_CMETATEXTURE_HPP_
#define T_FILE_GRAPHIC_CMETATEXTURE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CTextureManager.hpp"


namespace Ted
{

/**
 * \class   CMetaTexture
 * \ingroup Graphic
 * \brief   Une meta texture regroupe plusieurs images.
 *
 * Une meta texture permet de regrouper plusieurs images en une seule texture, afin de réduire
 * les temps d'accès à la mémoire de la carte graphique. Une meta texture peut être utilisée
 * pour stocker toutes les lightmaps, ce qui évite d'avoir des milliers de petites textures à
 * gérer. La texture est générée lors du premier appel à la méthode Update(). Une fois
 * créée, il est impossible de modifier ses dimensions.
 *
 * Plus d'informations : http://www.blackpawn.com/texts/lightmaps/
 ******************************/

class T_GRAPHIC_API CMetaTexture
{
public:

    static const unsigned int InvalidImageNumber = static_cast<unsigned int>(-1);

    // Constructeurs et destructeur
    CMetaTexture();
    CMetaTexture(unsigned int width, unsigned int height);
    ~CMetaTexture();

    // Accesseurs
    TTextureId getTextureId() const;
    bool isStatic() const;
    unsigned int getWidth() const;
    unsigned int getHeight() const;
    unsigned int getX(unsigned int num) const;
    unsigned int getY(unsigned int num) const;

    // Méthodes publiques
    unsigned int getNumImages() const;
    unsigned int addImage(const CImage& img);
    void deleteImage(unsigned int num);
    void update();

private:

    void createTexture();

    /// Nœud de l'arbre permettant de placer les textures.
    struct TTextureNode
    {
        TTextureNode * child[2]; ///< Nœuds fils.
        unsigned int x;          ///< Abcisse du rectangle.
        unsigned int y;          ///< Ordonnée du rectangle.
        unsigned int width;      ///< Largeur du rectangle.
        unsigned int height;     ///< Hauteur du rectangle.
        unsigned int num_image;  ///< Identifiant de l'image dans la texture.

        TTextureNode();
        ~TTextureNode();
        TTextureNode * insertImage(const CImage& img, unsigned int num = 0);
        unsigned int getX(unsigned int num);
        unsigned int getY(unsigned int num);
        void deleteImage(unsigned int num);
    };

private:

    // Données privées
    static unsigned int NumMetaTextures; ///< Nombre de méta-textures.
    const unsigned int m_metaId;         ///< Numéro de la méta-texture.

protected:

    // Données protégées
    TTextureId m_texture;       ///< Identifiant de la texture.
    bool m_static;              /**< Indique si la texture est statique (elle a été chargée depuis
                                     un fichier), ou si elle est dynamique (on peut ajouter ou
                                     enlever des images). */
    unsigned int m_width;       ///< Largeur de la texture.
    unsigned int m_height;      ///< Hauteur de la texture.
    unsigned int m_imagesCount; ///< Nombre d'images dans la texture.
    TTextureNode * m_root;      ///< Pointeur sur la racine de l'arbre.
    unsigned char * m_pixels;   ///< Tableau des pixels de la texture.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CMETATEXTURE_HPP_

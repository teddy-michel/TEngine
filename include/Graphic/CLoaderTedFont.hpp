/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderTedFont.hpp
 * \date 18/02/2011 Création de la classe CLoaderTedFont.
 * \date 08/04/2012 Nouvelle version du format de fichier.
 */

#ifndef T_FILE_GRAPHIC_CLOADERTEDFONT_HPP_
#define T_FILE_GRAPHIC_CLOADERTEDFONT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdint.h>

#include "Graphic/Export.hpp"
#include "Core/ILoader.hpp"
#include "Core/CString.hpp"
#include "Graphic/CImage.hpp"


namespace Ted
{

/**
 * \class   CLoaderTedFont
 * \ingroup Graphic
 * \brief   Chargement des fichiers de police du moteur.
 ******************************/

class T_GRAPHIC_API CLoaderTedFont : public ILoader
{
public:

#include "Core/struct_alignment_start.h"

    /// Dimensions d'un caractère.
    struct TCharSize
    {
        uint8_t width;  ///< Largeur du caractère.
        uint8_t height; ///< Hauteur du caractère.
    };

#include "Core/struct_alignment_end.h"

    // Constructeur
    CLoaderTedFont();

    // Accesseur
    unsigned char getSize() const;
    const CImage& getImage() const;
    void getCharSizes(TCharSize sizes[256]) const;

    // Méthode publique
    bool loadFromFile(const CString& fileName);

private:

/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

    static const uint16_t FileIdent = ('T' + ('F' << 8)); ///< Identifiant du format de fichier.

    /// Liste des versions du format de fichier.
#if __cplusplus < 201103L
    enum TVersion
#else
    enum TVersion : uint16_t
#endif
    {
        Version_1_0 = ('1' + ('0' << 8)) ///< Version 1.0 (18/02/2011).
    };

#include "Core/struct_alignment_start.h"

    /// En-tête des fichiers.
    struct THeader
    {
        uint16_t ident;           ///< Identifiant du fichier.
        uint16_t version;         ///< Version du format.
        char name[31];            ///< Nom de la police.
        uint8_t size;             ///< Taille d'un caractère dans l'image.
        TCharSize char_size[256]; ///< Dimensions de chaque caractère.
        uint32_t offset;          ///< Offset de l'image dans le fichier.
    };

#include "Core/struct_alignment_end.h"

private:

    // Données privées
    unsigned char m_size;         ///< Largeur d'un caractère dans l'image.
    char m_name[31];              ///< Nom de la police.
    TCharSize m_char_size[256];   ///< Dimensions de chaque caractère.
    CImage m_image;               ///< Image contenant chaque caractère.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CLOADERTEDFONT_HPP_

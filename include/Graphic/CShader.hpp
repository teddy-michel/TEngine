/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CShader.hpp
 * \date       2008 Création de la classe CShader.
 * \date 08/07/2010 La méthode LoadFromFile retourne un booléen.
 */

#ifndef T_FILE_GRAPHIC_CSHADER_HPP_
#define T_FILE_GRAPHIC_CSHADER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>
#include <Cg/cg.h>

#include "Graphic/Export.hpp"
#include "Graphic/CColor.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/Maths/CVector4.hpp"
#include "Core/Maths/CMatrix4.hpp"


namespace Ted
{

/**
 * \enum    TShaderType
 * \ingroup Graphic
 * \brief   Types de shaders.
 ******************************/

enum TShaderType
{
    SHADER_VERTEX, ///< Vertex shaders.
    SHADER_PIXEL   ///< Pixel shaders.
};


/**
 * \class   CShader
 * \ingroup Graphic
 * \brief   Gestion des shaders.
 ******************************/

class T_GRAPHIC_API CShader
{
public:

    // Constructeur et destructeur
    CShader(CGprogram program, TShaderType type);
    ~CShader();

    // Accesseur
    inline CGprogram getProgram() const;
    inline TShaderType getType() const;

    // Méthodes publiques
    bool loadFromFile(const CString& fileName);
    void setParameter(const CString& name, float value );
    void setParameter(const CString& name, const TVector2F& value);
    void setParameter(const CString& name, const TVector3F& value);
    void setParameter(const CString& name, const TVector4F& value);
    void setParameter(const CString& name, const TMatrix4F& value);
    void setParameter(const CString& name, const CColor& value);

protected :

    // Méthode protégée
    CGparameter getParameter(const CString& name) const;

    // Données protégés
    CGprogram m_program; ///< Programme Cg associé au shader.
    TShaderType m_type;  ///< Type du shader.
};


/**
 * Accesseur pour program.
 *
 * \return Programme Cg associé au shader.
 ******************************/

inline CGprogram CShader::getProgram() const
{
    return m_program;
}


/**
 * Accesseur pour type.
 *
 * \return Type de shader.
 ******************************/

inline TShaderType CShader::getType() const
{
    return m_type;
}

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CSHADER_HPP_

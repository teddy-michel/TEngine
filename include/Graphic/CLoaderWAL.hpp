/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderWAL.hpp
 * \date       2008 Création de la classe CLoaderWAL.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_GRAPHIC_CLOADERWAL_HPP_
#define T_FILE_GRAPHIC_CLOADERWAL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdint.h>

#include "Graphic/Export.hpp"
#include "Graphic/ILoaderImage.hpp"


namespace Ted
{

/**
 * \class   CLoaderWAL
 * \ingroup Graphic
 * \brief   Chargement des images WAL (Quake 2).
 ******************************/

class T_GRAPHIC_API CLoaderWAL : public ILoaderImage
{
public:

    // Constructeur
    CLoaderWAL();

    // Méthode publique
    bool loadFromFile(const CString& fileName);

private:

/*-------------------------------*
 *   Structures                  *
 *-------------------------------*/

#include "Core/struct_alignment_start.h"

    /// En-tête des fichiers WAL
    struct THeader
    {
        char name[32];      ///< Nom de la texture.
        uint32_t width;     ///< Largeur du plus grand mipmap.
        uint32_t height;    ///< Hauteur du plus grand mipmap.
        int32_t offset[4];  ///< Byte offset of the start of each of the 4 mipmap levels.
        char next_name[32]; ///< Nom de la texture suivante dans l'animation.
        uint32_t flags;     ///< Flags.
        uint32_t contents;
        uint32_t value;
    };

#include "Core/struct_alignment_end.h"

};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CLOADERWAL_HPP_

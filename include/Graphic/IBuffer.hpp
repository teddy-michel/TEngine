/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/IBuffer.hpp
 * \date 11/02/2010 Création de la classe IBuffer.
 * \date 07/07/2010 Ajout du constructeur par défaut pour la structure TBufferTexture.
 * \date 23/11/2010 On peut utiliser plus de deux unités de texture.
 * \date 23/01/2011 Ajout du type PrimTriangleStrip à l'énumération TPrimitiveType.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */

#ifndef T_FILE_GRAPHIC_IBUFFER_HPP_
#define T_FILE_GRAPHIC_IBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/INonCopyable.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

const unsigned int NumUTUsed = 4; ///< Nombre d'unités de texture utilisées par les buffers.


/**
 * \enum    TPrimitiveType
 * \ingroup Graphic
 * \brief   Types de primitives à afficher.
 ******************************/

enum TPrimitiveType
{
    PrimTriangle      = GL_TRIANGLES,      ///< Chaque triplet de sommets constitue un triangle.
    PrimTriangleFan   = GL_TRIANGLE_FAN,   ///< Le premier sommet est commun à tous les triangles.
    PrimTriangleStrip = GL_TRIANGLE_STRIP, ///< Chaque nouveau sommet forme un triangle avec les deux précédents sommets.
    PrimLine          = GL_LINES,          ///< Chaque couple de sommets forme une ligne.
    PrimPoint         = GL_POINTS          ///< Dessine un point pour chaque sommet transmis.
};


/**
 * \struct  TBufferTexture
 * \ingroup Graphic
 * \brief   Liste des textures utilisées dans un buffer.
 ******************************/

struct T_GRAPHIC_API TBufferTexture
{
    TTextureId texture[NumUTUsed]; ///< Identifiant de la texture pour chaque unité.
    unsigned int nbr;              ///< Nombre d'indices concernés.

    /// Constructeur par défaut.
    TBufferTexture() : nbr (0)
    {
        for (unsigned int i = 0; i < NumUTUsed; ++i)
        {
            texture[i] = 0;
        }
    }
};

typedef std::vector<TBufferTexture> TBufferTextureVector;


/**
 * \class   IBuffer
 * \ingroup Graphic
 * \brief   Classe de base des buffers graphiques.
 ******************************/

class T_GRAPHIC_API IBuffer : public INonCopyable
{
public:

    // Constructeur et destructeur
    IBuffer();
    virtual ~IBuffer();

    // Accesseurs
    unsigned int getVertexBuffer() const;
    unsigned int getIndexBuffer() const;

    // Méthodes publiques
    virtual void clear() = 0;
    virtual unsigned int draw() const = 0;
    void requestUpdate();
    virtual void update() = 0;

protected:

    // Données protégées
    unsigned int m_vertexBuffer; ///< Identifiant du Vertex Buffer Object.
    unsigned int m_indexBuffer;  ///< Identifiant du Index Buffer Object.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_IBUFFER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CMultiBuffer.hpp
 * \date 11/02/2010 Création de la classe CMultiBuffer.
 * \date 09/12/2010 Suppression des méthodes pour ajouter une seule donnée aux tableaux.
 * \date 15/12/2010 Suppression des méthodes pour modifier ou accéder aux tableaux.
 * \date 15/12/2010 Création des méthodes qui retournent une référence sur un tableau.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 */

#ifndef T_FILE_GRAPHIC_CMULTIBUFFER_HPP_
#define T_FILE_GRAPHIC_CMULTIBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>

#include "Graphic/Export.hpp"
#include "Graphic/IBuffer.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CSubBuffer.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class CColor;


/**
 * \class   CMultiBuffer
 * \ingroup Graphic
 * \brief   Gestion des buffers pouvant contenir plusieurs sous buffers.
 *
 * Un multibuffer fonctionne de la même manière qu'un buffer simple, mais il
 * permet en plus de séparer les données à afficher en plusieurs groupes, qui
 * peuvent être affichés ou masqués. Cela permet de n'afficher que les zones de
 * la map qui sont visibles depuis la position du joueur, ou de gérer les
 * niveaux de détails (LOD) d'un modèle.
 ******************************/

class T_GRAPHIC_API CMultiBuffer : public IBuffer
{
public:

    // Constructeur
    CMultiBuffer();

    // Accesseurs
    TPrimitiveType getPrimitiveType() const;

    std::vector<unsigned int>& getIndices(unsigned short sub);
    std::vector<TVector3F>& getVertices(unsigned short sub);
    std::vector<TVector3F>& getNormales(unsigned short sub);
    std::vector<TVector2F>& getTextCoords(unsigned short sub, unsigned short unit = 0);
    std::vector<float>& getColors(unsigned short sub);
    TBufferTextureVector& getTextures(unsigned short sub);

    const std::vector<unsigned int>& getIndices(unsigned short sub) const;
    const std::vector<TVector3F>& getVertices(unsigned short sub) const;
    const std::vector<TVector3F>& getNormales(unsigned short sub) const;
    const std::vector<TVector2F>& getTextCoords(unsigned short sub, unsigned short unit = 0) const;
    const std::vector<float>& getColors(unsigned short sub) const;
    const TBufferTextureVector& getTextures(unsigned short sub) const;

    // Mutateur
    void setPrimitiveType(TPrimitiveType primitive);

    // Méthodes publiques
    void enableSubBuffer(unsigned short sub);
    void disableSubBuffer(unsigned short sub);
    void enableAllSubBuffer();
    void translate(const TVector3F& vector);
    void scale(float scale);
    void clear();
    unsigned int draw() const;
    void update();

private:

    // On interdit la copie des buffers
    CMultiBuffer(const CMultiBuffer&);
    CMultiBuffer& operator=(const CMultiBuffer&);

protected :

    /// Informations pour chaque SubBuffer.
    struct TBufferData
    {
        bool enable;       ///< Indique si le buffer doit être affiché.
        unsigned int nbr;  ///< Nombre d'indices.
        CSubBuffer buffer; ///< Données du buffer.
    };

    typedef std::list<TBufferData> TBufferDataList;


    // Données protégés
    TPrimitiveType m_primitive;          ///< Type de primitive à afficher (par défaut PrimTriangle).
    unsigned int m_numIndices;           ///< Nombre d'indices.
    unsigned int m_numVertices;          ///< Nombre de vertices.
    unsigned int m_numNormales;          ///< Nombre de normales.
    unsigned int m_numCoords[NumUTUsed]; ///< Nombre de coordonnées de texture pour chaque unité.
    unsigned int m_numColors;            ///< Nombre de couleurs.
    unsigned int m_numTextures;          ///< Nombre de textures.
    TBufferDataList m_data;              ///< Liste des données du buffer.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CMULTIBUFFER_HPP_

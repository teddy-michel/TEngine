/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderICO.hpp
 * \date 16/01/2010 Création de la classe CLoaderICO.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_GRAPHIC_CLOADERICO_HPP_
#define T_FILE_GRAPHIC_CLOADERICO_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdint.h>

#include "Graphic/Export.hpp"
#include "Graphic/Export.hpp"
#include "Graphic/ILoaderImage.hpp"


namespace Ted
{


/**
 * \class   CLoaderICO
 * \ingroup Graphic
 * \brief   Chargement des icônes de Microsoft Windows.
 *
 * \todo Supprimer cette classe.
 ******************************/

class T_GRAPHIC_API CLoaderICO : public ILoaderImage
{
public:

    // Constructeur
    CLoaderICO();

    // Méthode publique
    bool loadFromFile(const CString& fileName);

private:

/*-------------------------------*
 *   Structures                  *
 *-------------------------------*/

#include "Core/struct_alignment_start.h"

    /// Icon directory.
    struct TIconDir
    {
        uint16_t reserved; ///< Reserved (must be 0)
        uint16_t type;     ///< Resource Type (1 for icons)
        uint16_t count;    ///< How many images?
    };

    /// Icon directory entry.
    struct TIconDirEntry
    {
        uint8_t width;          ///< Width, in pixels, of the image
        uint8_t height;         ///< Height, in pixels, of the image
        uint8_t bColorCount;    ///< Number of colors in image (0 if >=8bpp)
        uint8_t reserved;       ///< Reserved (must be 0)
        uint16_t planes;        ///< Color Planes
        uint16_t wBitCount;     ///< Bits per pixel
        uint32_t dwBytesInRes;  ///< How many bytes in this resource?
        uint32_t dwImageOffset; ///< Where in the file is this image?
    };

    /// Bitmap info header (Windows library).
    struct TBitmapInfoHeader
    {
        uint32_t size;           ///< The number of bytes required by the structure
        int32_t width;           ///< The width of the bitmap in pixels
        int32_t height;          ///< The height of the bitmap in pixels
        uint16_t planes;         ///< The number of planes for the target device. This value must be set to 1.
        uint16_t biBitCount;     ///< The number of bits-per-pixel. This member must be one of the following values : 0, 1, 4, 8, 16, 24, 32.
        uint32_t biCompression;  ///< The type of compression for a compressed bottom-up bitmap (top-down DIBs cannot be compressed).
        uint32_t biSizeImage;    ///< The size of the image in bytes.
        int32_t biXPelsPerMeter; ///< The horizontal resolution in pixels-per-meter.
        int32_t biYPelsPerMeter; ///< The vertical resolution in pixels-per-meter.
        uint32_t biClrUsed;      ///< The number of color indexes in the color table that are actually used by the bitmap.
        uint32_t biClrImportant; ///< The number of color indexes that are required for displaying the bitmap.
    };

    /// RGBQuad (Windows library). This structure describes a color consisting of relative intensities of red, green, and blue.
    struct TRGBQuad
    {
        uint8_t rgbBlue;     ///< Bleu.
        uint8_t rgbGreen;    ///< Vert.
        uint8_t rgbRed;      ///< Rouge.
        uint8_t rgbReserved; ///< Réservé, set to zero.
    };

    /// Icon image.
    struct TIconImage
    {
       TBitmapInfoHeader icHeader; ///< DIB header.
       TRGBQuad icColors[1];       ///< Color table.
       uint8_t icXOR[1];           ///< DIB bits for XOR mask.
       uint8_t icAND[1];           ///< DIB bits for AND mask.
    };

#include "Core/struct_alignment_end.h"

};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CLOADERICO_HPP_

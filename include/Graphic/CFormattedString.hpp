/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFormattedString.hpp
 * \date 11/05/2012 Création de la classe CFormattedString.
 * \date 08/04/2013 Modification complète de la classe.
 */

#ifndef T_FILE_GRAPHIC_CFORMATTEDSTRING_HPP_
#define T_FILE_GRAPHIC_CFORMATTEDSTRING_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Graphic/Export.hpp"
#include "Core/CString.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CFormattedString
 * \ingroup Graphic
 * \brief   Gestion d'une chaine de caractères avec un formatage spécifique à chaque caractère.
 ******************************/

class T_GRAPHIC_API CFormattedString
{
public:

    /// Formattage du texte.
    struct TFormat
    {
        int offset;                   ///< Indice du premier caractère du bloc.
        int length;                   ///< Nombre de caractères du bloc.

        CColor textColor;             ///< Couleur du texte.
        CColor backgroundColor;       ///< Couleur de fond.
        uint8_t underline:1;          ///< Indique si le texte est souligné.

        uint8_t useTextColor:1;       ///< Indique si on modifie la couleur du texte.
        uint8_t useBackgroundColor:1; ///< Indique si on modifie la couleur de fond.
        uint8_t useUnderline:1;       ///< Indique si on modifie le soulignement.
      //uint8_t bold:1;               ///< Indique si le texte est en gras. => Police différente
      //uint8_t italic:1;             ///< Indique si le texte est en italique. => Police différente
        uint8_t padding:4;

        /// Constructeur par défaut.
        inline TFormat() :
            offset             (0),
            length             (0),
            textColor          (CColor::Black),
            backgroundColor    (0, 0, 0, 0),
            underline          (0),
            useTextColor       (1),
            useBackgroundColor (1),
            useUnderline       (1)
        { }
    };

    typedef std::list<TFormat> TFormatList;


    // Constructeur
    CFormattedString(const CString& str = CString(), const TFormat& defaultFormat = TFormat());

    // Méthodes publiques
    inline CString getString() const;
    void setString(const CString& str);
    inline TFormat getDefaultFormat() const;
    void setDefaultFormat(const TFormat& format);
    TFormat getFormat(int offset) const;
    void setPartFormat(const TFormat& format);
    void setPartDefaultFormat(int offset, int length);
    inline TFormatList getParts() const;
    TFormat getNextFormat(int offset) const;
    void clearFormat();

private:

    void setPartFormat(int offset, int length, const TFormat& format);

    // Attributs privés
    CString m_string;         ///< Texte à formatter.
    TFormat m_defaultFormat;  ///< Formattage par défaut.
    TFormatList m_blocks;     ///< Liste des blocs avec un format spécifique.
};


/**
 * Retourne la chaine de caractère à formatter.
 *
 * \return Chaine de caractère.
 ******************************/

inline CString CFormattedString::getString() const
{
    return m_string;
}


inline CFormattedString::TFormat CFormattedString::getDefaultFormat() const
{
    return m_defaultFormat;
}


inline CFormattedString::TFormatList CFormattedString::getParts() const
{
    return m_blocks;
}

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CFORMATTEDSTRING_HPP_

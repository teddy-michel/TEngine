/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CHUD_FPS.hpp
 * \date 25/01/2011 Création de la classe CHUD_FPS.
 */

#ifndef T_FILE_GRAPHIC_CHUD_FPS_HPP_
#define T_FILE_GRAPHIC_CHUD_FPS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/Export.hpp"
#include "Graphic/IHUD.hpp"


namespace Ted
{

/**
 * \class   CHUD_FPS
 * \ingroup Graphic
 * \brief   HUD affichant le nombre de frames par seconde (FPS).
 ******************************/

class T_GRAPHIC_API CHUD_FPS : public IHUD
{
public:

    // Constructeur
    CHUD_FPS();

    // Méthodes publiques
    void update();
    void paint();
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CHUD_FPS_HPP_

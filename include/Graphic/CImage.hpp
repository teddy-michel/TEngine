/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CImage.hpp
 * \date       2008 Création de la classe CImage.
 * \date 10/04/2013 Ajout de la méthode insertImage.
 */

#ifndef T_FILE_GRAPHIC_CIMAGE_HPP_
#define T_FILE_GRAPHIC_CIMAGE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Core/CString.hpp"
#include "Graphic/Export.hpp"
#include "Graphic/CColor.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

/**
 * \class   CImage
 * \ingroup Graphic
 * \brief   Classe pour gérer les images.
 ******************************/

class T_GRAPHIC_API CImage
{
public:

    // Constructeurs
    CImage(const CImage& image);
    explicit CImage();
    explicit CImage(int width, int height);
    CImage(int width, int height, const CColor * pixels);

    // Accesseurs
    int getWidth() const;
    int getHeight() const;
    const CColor * getPixels() const;

    // Méthodes publiques
    bool loadFromFile(const CString& file);
    bool saveToFile(const CString& file) const;
    void fill(const CColor& color);
    void setPixel(int x, int y, const CColor& color);
    CColor getPixel(int x, int y) const;
    void flip();
    void mirror();
    void changeColor(const CColor& from, const CColor& to);
    void insertImage(const CImage& image, const CRectangle& rec);
    //void resize(int width, int height);
    CImage& operator=(const CImage& image);

private:

    // Données privées
    int m_width;                  ///< Largeur de l'image en pixels.
    int m_height;                 ///< Hauteur de l'image en pixels.
    std::vector<CColor> m_pixels; ///< Tableau de pixels de l'image.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CIMAGE_HPP_

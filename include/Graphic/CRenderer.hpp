/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CRenderer.hpp
 * \date       2008 Création de la classe CRenderer.
 * \date 11/07/2010 On peut supprimer un buffer de la mémoire ou uniquement du gestionnaire.
 * \date 15/07/2010 Création de la méthode ScreenShot_Thread.
 * \date 09/12/2010 Utilisation de l'accumulation buffer.
 * \date 26/12/2010 Création de la méthode ChangeFogParams.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 11/04/2013 Recherche de la version du langage de shading.
 * \date 06/09/2015 Ajout du mode d'affichage des couleurs uniquement.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */

#ifndef T_FILE_GRAPHIC_CRENDERER_HPP_
#define T_FILE_GRAPHIC_CRENDERER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <string>
#include <GL/glew.h>
#include <Cg/cgGL.h>
#include <SFML/System.hpp>

#include "Graphic/Export.hpp"
#include "Core/ISingleton.hpp"
#include "Graphic/CShader.hpp"
#include "Graphic/CColor.hpp"


/**
 * Macro servant à calculer la position dans le buffer
 ******************************/
#define T_BUFFER_OFFSET(n)  ((char *)NULL + (n))


namespace Ted
{


/**
 * \enum    TDisplayMode
 * \ingroup Graphic
 * \brief   Modes d'affichages.
 ******************************/

enum TDisplayMode
{
    Normal,          ///< Affichage normal.
    Textured,        ///< Affichage des textures sans les lightmaps.
    Lightmaps,       ///< Affichage des lightmaps uniquement.
    Colored,         ///< Affichage des surfaces colorées.
    Grey,            ///< Les surfaces sont affichées en gris transparent.
    Wireframe,       ///< Fil de fer.
    WireframeCulling ///< Fil de fer avec culling activé.
};


/**
 * \enum    TMatrixType
 * \ingroup Graphic
 * \brief   Types de matrices de transformation.
 ******************************/

enum TMatrixType
{
    MatrixModelView  = GL_MODELVIEW,  ///< Matrice de vue.
    MatrixProjection = GL_PROJECTION, ///< Matrice de projection.
    MatrixTexture    = GL_TEXTURE     ///< Matrice de texture.
};


class ICamera;
class IBuffer;
class CSkyBox;
class CRenderer;
class CTextureManager;
class CFontManager;


namespace Game
{
    extern T_GRAPHIC_API CRenderer * renderer;             // Défini dans CRenderer.cpp
    extern T_GRAPHIC_API CTextureManager * textureManager; // Défini dans CTextureManager.cpp
    extern T_GRAPHIC_API CFontManager * fontManager;       // Défini dans CFontManager.cpp
}


/**
 * \class   CRenderer
 * \ingroup Graphic
 * \brief   Gère les objets 3D à afficher.
 * \todo    Dériver cette classe pour OpenGL et DirectX.
 ******************************/

class T_GRAPHIC_API CRenderer
{
    friend class IBuffer;

public:

    /// Versions du langage de shading de OpenGL.
    enum TShaderVersion
    {
        GLSLVersionUnknown, ///< Version inconnue.
        GLSLVersion_1_10,   ///< Version 1.10.59 (OpenGL 2.0).
        GLSLVersion_1_20,   ///< Version 1.20.8 (OpenGL 2.1).
        GLSLVersion_1_30,   ///< Version 1.30.10 (OpenGL 3.0).
        GLSLVersion_1_40,   ///< Version 1.40.08 (OpenGL 3.1).
        GLSLVersion_1_50,   ///< Version 1.50.11 (OpenGL 3.2).
        GLSLVersion_3_30,   ///< Version 3.30.6 (OpenGL 3.3).
        GLSLVersion_4_00,   ///< Version 4.00.9 (OpenGL 4.0).
        GLSLVersion_4_10,   ///< Version 4.10.6 (OpenGL 4.1).
        GLSLVersion_4_20,   ///< Version 4.20.11 (OpenGL 4.2).
        GLSLVersion_4_30,   ///< Version 4.30.8 (OpenGL 4.3).
        GLSLVersion_4_40,   ///< Version 4.40 (OpenGL 4.4).
        GLSLVersion_4_50    ///< Version 4.50 (OpenGL 4.5).
    };


    // Constructeur et destructeur
    CRenderer();
    virtual ~CRenderer();

    // Accesseurs
    inline float getNear() const;
    inline float getFar() const;
    inline TDisplayMode getMode() const;
    inline ICamera * getCamera() const;
    inline CSkyBox * getSkybox() const;
    inline float getAnisotropic() const;

    // Mutateurs
    void setNear(float znear);
    void setFar(float zfar);
    void setMode(TDisplayMode mode);
    void setCamera(ICamera * camera);
    void setSkybox(CSkyBox * skybox);
    float setAnisotropic(float anisotropic = 1.0f);

    // Méthodes publiques
    unsigned int getNumSurfaces() const;
    void initOpenGL();
    void resize(unsigned int width, unsigned int height);
    static void screenShot();
    static bool isOGLExtension(const char * extension);

    virtual void beginScene();
    virtual void displayScene();
    virtual void endScene() const;

    void doPendingTasks();

    CGprofile getShaderProfile(TShaderType type) const;
    const char * const * getShaderOptions(TShaderType type) const;
    CShader * createShader(CGprogram program, TShaderType type) const;
    void setVertexShader(const CShader * shader);
    void setPixelShader(const CShader * shader);
    void perpective() const;
    void useAccumBuffer(float mult, float accum);
    void changeFogParams(float start, float end, float density) const;
    virtual void popMatrix(TMatrixType t);
    virtual void pushMatrix(TMatrixType t);
    virtual void loadMatrix(TMatrixType t, const TMatrix4F& m);
    virtual void multMatrix(TMatrixType t, const TMatrix4F& m);
    virtual void getMatrix(TMatrixType t, TMatrix4F& m);

    // Remplacement des fonctions de Glu
    static void _gluLookAt(const TVector3F& position, const TVector3F& view);
    static void _gluPerspective(double fovy, double aspect, double znear, double zfar);

    friend class CBuffer;

private:

    // Méthodes privées
    void setRatio(double ratio);
    static void screenShot_Thread(void * buffer);

    // Actions asynchrones sur les buffers graphiques
    void requestBufferUpdate(IBuffer * buffer);
    void requestBufferDelete(IBuffer * buffer);

    // Données privées
    double m_ratio;                 ///< Ratio de la fenêtre (largeur / hauteur).
    ICamera * m_camera;             ///< Pointeur sur la caméra.
    CSkyBox * m_skybox;             ///< Pointeur sur la skybox.
    float m_near;                   ///< Distance minimale visible.
    float m_far;                    ///< Distance maximale visible.
    TDisplayMode m_mode;            ///< Mode d'affichage.
    unsigned int m_numSurfaces;     ///< Nombre de surfaces dessinées.
    unsigned int m_numSurfacesOld;  ///< Nombre de surfaces dessinées durant la dernière frame.
    float m_anisotropic;            ///< Valeur du filtrage anisotropique.

    // Shaders
    CGprofile m_VSProfile;          ///< Profil utilisé pour créer les vertex shaders.
    CGprofile m_PSProfile;          ///< Profil utilisé pour créer les pixel shaders.
    const char * m_VSOptions[2];    ///< Options utilisées pour créer les vertex shaders.
    const char * m_PSOptions[2];    ///< Options utilisées pour créer les pixel shaders.
    TShaderVersion m_shaderVersion; ///< Version du langage de shading supportée par la carte graphique.

    // Buffers
    enum TBufferAction
    {
        BufferActionUpdate = 1, ///< Mise à jour du buffer.
        BufferActionDelete = 2  ///< Suppression du buffer.
    };

    struct TBufferTask
    {
        IBuffer * buffer;     ///< Pointeur sur le buffer graphique.
        unsigned int vertexBuffer;
        unsigned int indexBuffer;
        TBufferAction action; ///< Action à effectuer.
    };

    mutable sf::Mutex m_mutex;                ///< Mutex pour permettre l'utilisation dans plusieurs threads.
    std::list<TBufferTask> m_buffersToUpdate; ///< Liste des buffers graphiques à mettre à jour.
};


/**
 * Donne la distance minimale visible
 *
 * \return Distance minimale visible.
 *
 * \sa CRenderer::setNear
 ******************************/

inline float CRenderer::getNear() const
{
    return m_near;
}


/**
 * Donne la distance maximale visible.
 *
 * \return Distance maximale visible.
 *
 * \sa CRenderer::setFar
 ******************************/

inline float CRenderer::getFar() const
{
    return m_far;
}


/**
 * Accesseur pour Mode.
 *
 * \return Mode d'affichage.
 *
 * \sa CRenderer::setMode
 ******************************/

inline TDisplayMode CRenderer::getMode() const
{
    return m_mode;
}


/**
 * Accesseur pour camera.
 *
 * \return Pointeur sur la caméra utilisée par le moteur graphique.
 *
 * \sa CRenderer::setCamera
 ******************************/

inline ICamera * CRenderer::getCamera() const
{
    return m_camera;
}


/**
 * Retourne la skybox utilisée.
 *
 * \return Pointeur sur la skybox utilisée par le moteur graphique.
 *
 * \sa CRenderer::setSkybox
 ******************************/

inline CSkyBox * CRenderer::getSkybox() const
{
    return m_skybox;
}


/**
 * Donne la valeur du filtrage anisotropique (1 par défaut).
 * Si cette valeur est strictement supérieure à 1, alors le filtrage anisotropique
 * peut être utilisé.
 *
 * \return Valeur du filtrage anisotropique.
 *
 * \sa CRenderer::setAnisotropic
 ******************************/

inline float CRenderer::getAnisotropic() const
{
    return m_anisotropic;
}

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CRENDERER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/ILoaderImage.hpp
 * \date       2008 Création de la classe ILoaderImage.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_GRAPHIC_ILOADERIMAGE_HPP_
#define T_FILE_GRAPHIC_ILOADERIMAGE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Core/ILoader.hpp"
#include "Graphic/CImage.hpp"


namespace Ted
{

/**
 * \class   ILoaderImage
 * \ingroup Graphic
 * \brief   Classe de base des loaders d'images.
 *
 * \todo Supprimer cette classe.
 ******************************/

class T_GRAPHIC_API ILoaderImage : public ILoader
{
public:

    // Constructeur
    ILoaderImage();

    // Accesseurs
    unsigned int getWidth() const;
    unsigned int getHeight() const;

    // Méthode publique
    CImage getImage();

protected:

    // Données protégées
    unsigned int m_width;                ///< Largeur de l'image en pixels.
    unsigned int m_height;               ///< Hauteur de l'image en pixels.
    std::vector<unsigned char> m_pixels; ///< Tableau des pixels.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_ILOADERIMAGE_HPP_

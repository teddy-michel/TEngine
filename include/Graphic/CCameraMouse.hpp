/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CCameraMouse.hpp
 * \date 21/02/2010 Création de la classe CCameraMouse.
 * \date 19/10/2013 Ajout de la méthode anglesFromVectors.
 */

#ifndef T_FILE_GRAPHIC_CCAMERAMOUSE_HPP_
#define T_FILE_GRAPHIC_CCAMERAMOUSE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>
#include <string>

#include "Graphic/Export.hpp"
#include "Graphic/ICamera.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   CCameraMouse
 * \ingroup Graphic
 * \brief   Caméra commandée uniquement par la souris.
 *
 * Cette caméra est similaire à celles des modeleurs 3D, comme Maya ou Valve
 * Hammer Editor par exemple. Le bouton gauche de la souris permet de se
 * déplacer, et le bouton droit permet de tourner.
 ******************************/

class T_GRAPHIC_API CCameraMouse : public ICamera
{
public:

    // Constructeur et destructeur
    CCameraMouse(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));
    virtual ~CCameraMouse();

    // Accesseurs
    float getTheta() const;
    float getPhi() const;

    // Mutateur
    void setDirection(const TVector3F& direction);

    // Méthodes publiques
    void animate(unsigned int frameTime);
    virtual void onEvent(const CMouseEvent& event);
    void initialize(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));

private:

    // Méthodes privées
    void vectorsFromAngles();
    void anglesFromVectors();

protected:

    // Données protégées
    TVector3F m_forward;  ///< Déplacement avant ou arrière.
    float m_theta;        ///< Angle Theta.
    float m_phi;          ///< Angle Phi.
    bool m_button_left;   ///< Indique si le bouton gauche de la souris est enfoncé.
    bool m_button_right;  ///< Indique si le bouton droit de la souris est enfoncé.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CCAMERAMOUSE_HPP_

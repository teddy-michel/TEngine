/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CSkyBox.hpp
 * \date       2008 Création de la classe CSkyBox.
 * \date 28/02/2011 Ajout d'un paramètre pour faire tourner la boite.
 */

#ifndef T_FILE_GRAPHIC_CSKYBOX_HPP_
#define T_FILE_GRAPHIC_CSKYBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>

#include "Graphic/Export.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   CSkyBox
 * \ingroup Graphic
 * \brief   Gestion d'une skybox.
 *
 * La skybox est un cube texturé dont le centre coïncide toujours avec la position
 * de la caméra. L'écriture dans le depth buffer est désactivé lors de l'affichage de
 * la skybox, le paramètre size peut donc à priori prendre n'importe quelle valeur
 * comprise dans la zone de vue (entre near et far) sans changer quoi que ce soit.
 *
 * À l'avenir, une skybox pourra être définie à partir d'un certain nombre de faces.
 * Il faudra alors utiliser le stencil buffer pour déterminer quelles partie de de
 * l'images doivent afficher la skybox.
 ******************************/

class T_GRAPHIC_API CSkyBox
{
public:

    // Constructeur et destructeur
    explicit CSkyBox(float size = 32.0f);
    ~CSkyBox();

    // Mutateurs
    void setSize(float size);
    void setTextureDown(const CString& fileName);
    void setTextureNorth(const CString& fileName);
    void setTextureEast(const CString& fileName);
    void setTextureSouth(const CString& fileName);
    void setTextureWest(const CString& fileName);
    void setTextureUp(const CString& fileName);

    // Accesseur
    float getSize() const;

    // Méthode publique
    void draw(TDisplayMode mode = Normal) const;

protected:

    // Méthode protégée
    void setTexture(unsigned int id, const CString& fileName);

    // Données protégées
    float m_size;                ///< Largeur de la skybox.
    TTextureId m_textures[6];    ///< Identifiants des textures de la skybox.
    unsigned int m_vertexBuffer; ///< Identifiant du VBO de la skybox.
    unsigned int m_indexBuffer;  ///< Identifiant du IBO de la skybox.
    TVector3F m_rotation;        ///< Rotation de la boite avant l'affichage.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CSKYBOX_HPP_

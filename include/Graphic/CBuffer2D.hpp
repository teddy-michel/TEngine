/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CBuffer2D.hpp
 * \date 05/02/2010 Création de la classe CBuffer2D.
 * \date 12/11/2010 La méthode addText prend un TTextParams en argument.
 * \date 23/01/2011 Les buffers 2D ont un fonctionnement similaire aux buffers 3D.
 * \date 27/02/2011 Création de méthodes pour ajouter des formes simples.
 * \date 15/06/2014 Utilisation de flottants pour les coordonnées.
 */

#ifndef T_FILE_GRAPHIC_CBUFFER2D_HPP_
#define T_FILE_GRAPHIC_CBUFFER2D_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/IBuffer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Maths/CVector2.hpp"


namespace Ted
{

class CRectangle;
class TTextParams;


/**
 * \class   CBuffer2D
 * \ingroup Graphic
 * \brief   Gestion d'un buffer graphique 2D.
 *
 * Un buffer 2D permet d'afficher des triangles, des lignes et des points. Les
 * coordonnées des sommets sont des entiers qui correspondent aux coordonnées
 * sur l'écran.
 *
 * Chaque sommet est associé à une couleur et à une coordonnée de texture. Chaque
 * triangle est associé à un identifiant de texture.
 *
 * Attention, si vous utilisez les primitives d'affichage PrimTriangleFan ou
 * PrimTriangleStrip, seul un triangle est affiché pour le moment.
 ******************************/

class T_GRAPHIC_API CBuffer2D : public IBuffer
{
public:

    // Constructeur
    CBuffer2D();

    // Méthodes publiques
    std::vector<TPrimitiveType>& getElements();
    std::vector<unsigned int>& getIndices();
    std::vector<TVector2F>& getVertices();
    std::vector<float>& getTextCoords();
    std::vector<float>& getColors();
    std::vector<TTextureId>& getTextures();

    const std::vector<TPrimitiveType>& getElements() const;
    const std::vector<unsigned int>& getIndices() const;
    const std::vector<TVector2F>& getVertices() const;
    const std::vector<float>& getTextCoords() const;
    const std::vector<float>& getColors() const;
    const std::vector<TTextureId>& getTextures() const;

    void clear();
    unsigned int draw() const;
    void update();

    void addRectangle(const CRectangle& rec, const CColor& color);
    void addRectangleRounded(const CRectangle& rec, const CColor& color);
    void addBorder(const CRectangle& rec, const CColor& color, int epaisseur = 1);
    void addBorderRounded(const CRectangle& rec, const CColor& color, int epaisseur = 1);
    void addDisc(int x, int y, int radius, const CColor& color);
    void addCircle(int x, int y, int radius, const CColor& color);
    void addText(const TTextParams& params);
    void addImage(const CRectangle& rec, const TTextureId texture);
    void addLine(const TVector2F& from, const TVector2F& to, const CColor& color);
    void addPoint(const TVector2F& pos, const CColor& color);

protected:

    // Données protégés
    std::vector<TPrimitiveType> m_elements; ///< Tableau des éléments à afficher.
    std::vector<unsigned int> m_indices;    ///< Tableau des indices.
    std::vector<TVector2F> m_vertices;      ///< Tableau des vertices.
    std::vector<float> m_coords;            ///< Tableau des coordonnées de texture.
    std::vector<float> m_colors;            ///< Tableau des couleurs.
    std::vector<TTextureId> m_textures;     ///< Tableau des identifiants de texture pour chaque triangle.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CBUFFER2D_HPP_

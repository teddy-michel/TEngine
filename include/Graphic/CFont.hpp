/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFont.hpp
 * \date 13/04/2013 Création de la classe CFont.
 */

#ifndef T_FILE_GRAPHIC_CFONT_HPP_
#define T_FILE_GRAPHIC_CFONT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>

#include "Graphic/Export.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/CString.hpp"
#include "Core/Maths/CRectangle.hpp"
#include "Core/Maths/CVector2.hpp"

#ifdef T_USE_FREETYPE
#include <ft2build.h>
#include FT_FREETYPE_H
#endif


namespace Ted
{

class CBuffer2D;
class TTextParams;
class TTextLine;


/**
 * \class   CFont
 * \ingroup Graphic
 * \brief   Gestion d'un fichier de police de caractère.
 *
 * Cette classe est utilisée en interne par le gestionnaire de police.
 ******************************/

class T_GRAPHIC_API CFont
{
    //friend class CFontManager;

public:

    struct TCharParams
    {
        CImage pixmap;   ///< Image du caractère.
        CRectangle rect; ///< Dimensions du caractère.
#ifdef T_USE_FREETYPE
        FT_UInt glyphIndex;
#endif
        TTextureId texture; ///< Identifiant de la texture.

#ifdef T_USE_FREETYPE
        inline TCharParams() : glyphIndex(0), texture(CTextureManager::NoTexture) { }
#else
        inline TCharParams() : texture(CTextureManager::NoTexture) { }
#endif
    };


    // Constructeur de destructeur
    CFont();
    ~CFont();

    bool loadFromFile(const CString& fileName);

    CString getFamilyName() const;
    CString getFontName() const;

    void drawText(CBuffer2D * buffer, const TTextParams& params);
    TVector2UI getTextSize(const TTextParams& params);
    void getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines);
    void getTextLineSize(const TTextParams& params, TTextLine& line);
    TCharParams getCharParams(int size, const CChar& character);

private:

    void setFontSize(int size) const;
    TCharParams loadChar(uint32_t charCode);

    // Données privées
#ifdef T_USE_FREETYPE
    FT_Face m_face;
#endif
    bool m_hasKerning;
    mutable int m_currentSize;
    std::map<int, std::map<CChar, TCharParams> > m_chars;
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CFONT_HPP_

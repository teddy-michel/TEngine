/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CSubBuffer.hpp
 * \date 09/02/2010 Création de la classe CSubBuffer.
 * \date 07/12/2010 Généralisation des méthodes pour modifier les coordonnées de texture.
 * \date 09/12/2010 Suppression des méthodes pour ajouter une seule donnée aux tableaux.
 * \date 15/12/2010 Suppression des méthodes pour modifier ou accéder aux tableaux.
 * \date 15/12/2010 Création des méthodes qui retournent une référence sur un tableau.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 */

#ifndef T_FILE_GRAPHIC_CSUBBUFFER_HPP_
#define T_FILE_GRAPHIC_CSUBBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/IBuffer.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class CColor;

/**
 * \class   CSubBuffer
 * \ingroup Graphic
 * \brief   Gestion des subbuffers graphiques.
 *
 * Un subbuffer est une partie représente une partie d'un buffer graphique.
 * Il contient différentes données comme des vertices, référencés
 * par des indices, des couleurs, des normales, ou des coordonnées de textures.
 * Par ailleurs, il contient une liste de textures à utiliser pour chaque indice.
 ******************************/

class T_GRAPHIC_API CSubBuffer
{
public:

    // Constructeurs
    CSubBuffer();
    CSubBuffer(const CSubBuffer& buffer);

    // Accesseurs
    std::vector<unsigned int>& getIndices();
    std::vector<TVector3F>& getVertices();
    std::vector<TVector3F>& getNormales();
    std::vector<TVector2F>& getTextCoords(unsigned short unit = 0);
    std::vector<float>& getColors();
    TBufferTextureVector& getTextures();

    const std::vector<unsigned int>& getIndices() const;
    const std::vector<TVector3F>& getVertices() const;
    const std::vector<TVector3F>& getNormales() const;
    const std::vector<TVector2F>& getTextCoords(unsigned short unit = 0) const;
    const std::vector<float>& getColors() const;
    const TBufferTextureVector& getTextures() const;

    // Méthodes publiques
    void translate(const TVector3F& vector);
    void scale(float scale);
    void clear();

protected:

    // Données protégés
    std::vector<unsigned int> m_indices;        ///< Tableau des indices.
    std::vector<TVector3F> m_vertices;          ///< Tableau des vertices.
    std::vector<TVector3F> m_normales;          ///< Tableau des normales.
    std::vector<TVector2F> m_coords[NumUTUsed]; ///< Tableaux des coordonnées de texture de chaque unité.
    std::vector<float> m_colors;                ///< Tableau des couleurs.
    TBufferTextureVector m_textures;            ///< Tableau des textures.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CSUBBUFFER_HPP_

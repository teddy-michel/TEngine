/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CBuffer.hpp
 * \date 17/05/2009 Création de la classe CBuffer.
 * \date 23/11/2010 On peut utiliser plus de deux unités de texture.
 * \date 07/12/2010 Suppression des méthodes pour ajouter une seule donnée à un tableau.
 *                  Généralisation des méthodes pour modifier les coordonnées de texture.
 * \date 14/12/2010 Suppression des méthodes permettant de modifier ou d'obtenir
 *                  un des tableaux. À la place, on doit utiliser une méthode qui
 *                  retourne une référence sur le tableau.
 * \date 15/12/2010 Ajout des méthodes qui retournent une référence constante sur un tableau.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 */

#ifndef T_FILE_GRAPHIC_CBUFFER_HPP_
#define T_FILE_GRAPHIC_CBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/IBuffer.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class CColor;


/**
 * \class   CBuffer
 * \ingroup Graphic
 * \brief   Gestion des buffers de la carte graphique.
 *
 * Un buffer est une partie de la mémoire de la carte graphique qui permet de
 * stocker des données nécessaire à l'affichage. Ces données peuvent être des
 * vertices, référencés par des indices, des couleurs, des normales, ou des
 * coordonnées de textures.
 * Par ailleurs, un buffer contient une liste de textures à utiliser pour
 * chaque indices.
 ******************************/

class T_GRAPHIC_API CBuffer : public IBuffer
{
public:

    // Constructeur
    CBuffer();

    // Accesseurs
    TPrimitiveType getPrimitiveType() const;

    std::vector<unsigned int>& getIndices();
    std::vector<TVector3F>& getVertices();
    std::vector<TVector3F>& getNormales();
    std::vector<TVector2F>& getTextCoords(unsigned short unit = 0);
    std::vector<float>& getColors();
    TBufferTextureVector& getTextures();

    const std::vector<unsigned int>& getIndices() const;
    const std::vector<TVector3F>& getVertices() const;
    const std::vector<TVector3F>& getNormales() const;
    const std::vector<TVector2F>& getTextCoords(unsigned short unit = 0) const;
    const std::vector<float>& getColors() const;
    const TBufferTextureVector& getTextures() const;

    // Mutateur
    void setPrimitiveType(TPrimitiveType primitive);

    // Méthodes publiques
    void translate(const TVector3F& vector);
    void scale(float scale);
    void clear();
    unsigned int draw() const;
    void update();

private:

    // Données privées
    unsigned int m_numIndices;           ///< Nombre d'indices.
    unsigned int m_numVertices;          ///< Nombre de vertices.
    unsigned int m_numNormales;          ///< Nombre de normales.
    unsigned int m_numCoords[NumUTUsed]; ///< Nombre de coordonnées de texture pour chaque unité.
    unsigned int m_numColors;            ///< Nombre de couleurs.
    unsigned int m_numTextures;          ///< Nombre de textures.

protected :

    // Données protégés
    TPrimitiveType m_primitive;                 ///< Type de primitive à afficher (par défaut PrimTriangle).
    std::vector<unsigned int> m_indices;        ///< Tableau des indices.
    std::vector<TVector3F> m_vertices;          ///< Tableau des vertices.
    std::vector<TVector3F> m_normales;          ///< Tableau des normales.
    std::vector<TVector2F> m_coords[NumUTUsed]; ///< Tableaux des coordonnées de texture de chaque unité.
    std::vector<float> m_colors;                ///< Tableau des couleurs.
    TBufferTextureVector m_textures;            ///< Tableau des textures.
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CBUFFER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFontManager.hpp
 * \date       2008 Création de la classe CPoliceManager.
 * \date 08/07/2010 La classe CPoliceManager s'appelle désormais CFontManager.
 * \date 22/07/2010 La méthode DrawText prend un rectangle comme argument.
 * \date 22/07/2010 Création de la méthode LogFontList.
 * \date 11/01/2011 Création de la structure TTextParams.
 * \date 12/11/2011 Les méthodes DrawText prennent un TTextParams en argument.
 * \date 18/01/2011 Création de la structure TTextLine.
 * \date 18/01/2011 Création des méthodes getTextLinesSize et getTextLineSize.
 * \date 19/02/2011 Les polices sont chargées depuis un fichier de police TEF.
 * \date 14/04/2012 Utilisation de CString à la place de std::string.
 * \date 19/04/2012 Les polices sont manipulées via un identifiant.
 * \date 13/04/2013 Utilisation possible de FreeType.
 */

#ifndef T_FILE_GRAPHIC_CFONTMANAGER_HPP_
#define T_FILE_GRAPHIC_CFONTMANAGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Graphic/Export.hpp"
#include "Graphic/CColor.hpp"

#ifdef T_USE_FREETYPE
#include <ft2build.h>
#include FT_FREETYPE_H
#else
#include "Graphic/CTextureManager.hpp"
#endif

#include "Core/Maths/CRectangle.hpp"
#include "Core/Maths/CVector2.hpp"

// #include "Core/ISingleton.hpp"
// #include "Core/CString.hpp"


namespace Ted
{

/// Type des identifiants de polices utilisés par le moteur.
typedef unsigned int TFontId;


/**
 * \struct  TTextLine
 * \ingroup Graphic
 * \brief   Une ligne de texte, avec la largeur de chaque caractère.
 ******************************/

struct T_GRAPHIC_API TTextLine
{
    unsigned int offset;    ///< Numéro du premier caractère par rapport au texte entier.
    std::vector<float> len; ///< Largeur de chaque caractère en pixels.

    /// Constructeur par défaut
    inline TTextLine() : offset (0) { }
};


class CBuffer2D;
#ifdef T_USE_FREETYPE
class CFont;
#endif
class TTextParams;


/**
 * \class   CFontManager
 * \ingroup Graphic
 * \brief   Gestion des polices de caractère.
 ******************************/

class T_GRAPHIC_API CFontManager
{
    friend class CFont;

public:

    static const int minFontSize = 8;
    static const int maxFontSize = 128;


    // Constructeur et destructeur
    CFontManager();
    ~CFontManager();

    // Accesseurs
    TFontId getFontId(const CString& name);
    CString getFontName(const TFontId id) const;

    // Méthodes publiques
    TFontId loadFont(const CString& name, const CString& fileName);
    void unloadFonts();
    void drawText(CBuffer2D * buffer, const TTextParams& params);
    TVector2UI getTextSize(const TTextParams& params);
    void getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines);
    TTextLine getTextLineSize(const TTextParams& params);
    void logFontList() const;

    static const TFontId InvalidFont = static_cast<TFontId>(-1);

private:

    TFontId privLoadFont(const CString& name, const CString& fileName);

#ifndef T_USE_FREETYPE

    /**
     * \struct  TFont
     * \ingroup Graphic
     * \brief   Structure d'une police de caractère.
     ******************************/

    struct TFont
    {
        CString name;         ///< Nom de la police.
        TTextureId texture;   ///< Identifiant de la texture.
        unsigned int width;   ///< Largeur de la texture en pixels.
        unsigned int height;  ///< Hauteur de la texture en pixels.
        TVector2US size[256]; ///< Dimensions de chaque caractère.
    };

    typedef std::vector<TFont> TFontVector;
#else
    typedef std::vector<CFont *> TFontVector;
#endif

    // Données privées

#ifdef T_USE_FREETYPE
    FT_Library m_library; ///< Bibliothèque FreeType.
#endif

    TFontVector m_fonts;  ///< Liste des polices chargées.
};


/**
 * \struct  TTextParams
 * \ingroup Graphic
 * \brief   Paramètres du texte à afficher.
 ******************************/

struct T_GRAPHIC_API TTextParams
{
  //CFormattedString text; ///< Texte à afficher.
    CString text;          ///< Texte à afficher.
    CColor color;          ///< Couleur du texte.

    CRectangle rec;        ///< Rectangle dans lequel afficher le texte.
    TFontId font;          ///< Police à utiliser.

    uint16_t size:10;      ///< Taille du texte.
    uint16_t newline:1;    ///< Indique si les retours à la ligne sont automatiques.
    uint16_t kerning:1;    ///< Indique si on utilise le kerning (rapprochement de certaines paires de caractères).
    uint16_t padding:4;

    uint16_t left;         ///< Décalage à gauche en pixels.
    uint16_t top;          ///< Décalage en haut en pixels.

    /// Constructeur par défaut.
    inline TTextParams() :
        text    (),
        color   (CColor::Black),
        rec     (),
        font    (CFontManager::InvalidFont),
        size    (16),
        newline (0),
        kerning (0),
        left    (0),
        top     (0)
    { }
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CFONTMANAGER_HPP_

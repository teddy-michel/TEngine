/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CCameraFreeFly.hpp
 * \date       2008 Création de la classe CCameraFreeFly.
 * \date 11/07/2010 On peut modifier le rayon de la sphère englobante.
 * \date 17/03/2011 Création de la méthode setKey.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 19/10/2013 Ajout de la méthode anglesFromVectors.
 */

#ifndef T_FILE_GRAPHIC_CCAMERAFREEFLY_HPP_
#define T_FILE_GRAPHIC_CCAMERAFREEFLY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/Export.hpp"
#include "Graphic/ICamera.hpp"
#include "Core/Events.hpp"


namespace Ted
{

class CBoundingSphere;


/**
 * \class   CCameraFreeFly
 * \ingroup Graphic
 * \brief   Caméra de type FreeFly.
 *
 * Cette caméra permet de se déplacer dans la scène dans tous les sens, avec pour seule
 * contraite les collisions avec l'environnement et les objets (désactivable).
 ******************************/

class T_GRAPHIC_API CCameraFreeFly : public ICamera
{
public:

    // Constructeur et destructeur
    CCameraFreeFly(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));
    virtual ~CCameraFreeFly();

    // Accesseurs
    float getTheta() const;
    float getPhi() const;

    // Mutateur
    void setDirection(const TVector3F& direction);

#ifdef T_CAMERA_FREEFLY_COLLISION

    bool isCollisions() const;

    // Mutateurs
    void setCollisions(bool collisions = true);
    void setSphereRadius(float radius);

#endif

    // Méthodes publiques
    void animate(unsigned int frameTime);
    virtual void onEvent(const CMouseEvent& event);
    void initialize(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));

private:

    // Méthodes privées
    void vectorsFromAngles();
    void anglesFromVectors();

protected:

    // Données protégées
    TVector3F m_forward;               ///< Déplacement avant ou arrière.
    TVector3F m_left;                  ///< Déplacement sur le côté.
    float m_theta;                     ///< Angle Theta.
    float m_phi;                       ///< Angle Phi.
    unsigned int m_timeVerticalMotion; ///< Durée du déplacement vertical.
    int m_verticalMotionDirection;     ///< Indique la direction du déplacement vertical.

#ifdef T_CAMERA_FREEFLY_COLLISION
    bool m_collisions;                 ///< La caméra ne peut pas traverser les murs.
    CBoundingSphere * m_sphere;        ///< Sphère englobante de la caméra.
#endif
};

} // Namespace Ted

#endif // T_FILE_GRAPHIC_CCAMERAFREEFLY_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CButtonGroup.hpp
 * \date 12/03/2010 Création de la classe GuiButtonGroup.
 * \date 29/05/2011 La classe est renommée en CButtonGroup.
 */

#ifndef T_FILE_GUI_CBUTTONGROUP_HPP_
#define T_FILE_GUI_CBUTTONGROUP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"


namespace Ted
{

class IButton;


/**
 * \class   CButtonGroup
 * \ingroup Gui
 * \brief   Conteneur pour organiser un groupe de boutons.
 *
 * Ce widget sert à gérer l'état de plusieurs boutons liés entre eux. Il ne
 * gère pas l'affichage des boutons, qui doivent donc être également insérés
 * dans un layout.
 * Si la propriété exclusive est vraie, un seul bouton peut être enfoncé en
 * même temps. Si l'utilisateur clique sur un bouton déjà enfoncé, son état ne
 * change pas. Si le bouton n'est pas enfoncé, son état change, et celui du
 * bouton précédent enfoncé aussi.
 * Si le groupe n'est apparenté à aucun widget, il faudra le supprimer
 * manuellement, c'est pourquoi il est préférable de l'ajouter au layout
 * contenant les boutons.
 ******************************/

class T_GUI_API CButtonGroup : public IWidget
{
public:

    static const int LastPosition = -1;

    // Constructeur
    explicit CButtonGroup(IWidget * parent = nullptr);

    // Accesseurs
    bool isExclusive() const;
    IButton * getButton(int pos) const;
    int getNumButtons() const;

    // Mutateur
    void setExclusive(bool exclusive = true);

    // Méthodes publiques
    void addButton(IButton * button, int pos = LastPosition);
    void removeButton(IButton * button);
    void removeButton(int pos);
    void clearButtons();
    void buttonState(IButton * button);
    virtual void draw();

private:

    // Données protégées
    bool m_exclusive; ///< Indique si un seul bouton peut être enfoncé à la fois.
    std::list<IButton *> m_buttons; ///< Liste des boutons.
};

} // Namespace Ted

#endif // T_FILE_GUI_CBUTTONGROUP_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CMenuItem.hpp
 * \date 06/05/2009 Création de la classe GuiMenuItem.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CMenuItem.
 */

#ifndef T_FILE_GUI_CMENUITEM_HPP_
#define T_FILE_GUI_CMENUITEM_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/IWidget.hpp"
#include "Gui/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class CMenu;


/**
 * \class   CMenuItem
 * \ingroup Gui
 * \brief   Item d'un menu.
 *
 * \section Signaux
 * \li onClick
 ******************************/

class T_GUI_API CMenuItem : public IWidget
{
public:

    // Constructeurs
    explicit CMenuItem(const CString& name, IWidget * parent = nullptr);
    explicit CMenuItem(bool separator, IWidget * parent = nullptr);
    explicit CMenuItem(IWidget * parent = nullptr);

    // Accesseurs
    CString getName() const;
    bool isSeparator() const;

    // Mutateurs
    void setName(const CString& name);
    void setSeparator(bool separator = true);

    inline CMenu * getMenu() const;
    void setMenu(CMenu * menu);

    // Méthodes publiques
    virtual void onEvent(const CMouseEvent& event);
    virtual void draw();

    // Signaux
    sigc::signal<void> onClick; ///< Signal émis lorsqu'on clique sur l'item.

private:

    // Données privées
    CMenu * m_menu;           ///< Pointeur sur le menu qui contient cet élément.
    CString m_name;           ///< Nom de l'item.
    bool m_separator;         ///< Indique si l'item est un séparateur.
    unsigned int m_time;      ///< Instant du dernier affichage.
    unsigned int m_timeFocus; ///< Durée en millisecondes pout passer d'une couleur à une autre.
};


inline CMenu * CMenuItem::getMenu() const
{
    return m_menu;
}

} // Namespace Ted

#endif // T_FILE_GUI_CMENUITEM_HPP_

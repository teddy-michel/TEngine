/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IButton.hpp
 * \date       2008 Création de la classe GuiBaseButton.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en IButton.
 */

#ifndef T_FILE_GUI_IBUTTON_HPP_
#define T_FILE_GUI_IBUTTON_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class CButtonGroup;


/**
 * \class   IButton
 * \ingroup Gui
 * \brief   Classe de base des boutons.
 *
 * \section Signaux
 * \li onClicked
 * \li onPressed
 * \li onReleased
 ******************************/

class T_GUI_API IButton : public IWidget
{
public:

    // Constructeurs et destructeur
    explicit IButton(const CString& text = CString(), IWidget * parent = nullptr);
    explicit IButton(IWidget * parent);
    virtual ~IButton();

    // Accesseurs
    bool isDown() const;
    CString getText() const;
    CButtonGroup * getGroup() const;

    // Mutateurs
    void setDown(bool down = true);
    void toggleDown();
    virtual void setText(const CString& text);
    void setGroup(CButtonGroup * group);

    // Signaux
    sigc::signal<void> onClicked;  ///< Signal émis lorsqu'on clique sur le bouton.
    sigc::signal<void> onPressed;  ///< Signal émis lorsque le bouton est enfoncé.
    sigc::signal<void> onReleased; ///< Signal émis lorsque le bouton est relaché.

protected:

    // Données protégées
    bool m_down;            ///< Indique si le bouton est enfoncé ou pas.
    CString m_text;         ///< Texte du bouton.
    CButtonGroup * m_group; ///< Groupe contenant le bouton.
};

} // Namespace Ted

#endif // T_FILE_GUI_IBUTTON_HPP_

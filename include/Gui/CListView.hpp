/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CListView.hpp
 * \date 24/02/2010 Création de la classe GuiListView.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 04/01/2011 Améliorations.
 * \date 14/01/2011 Améliorations diverses, utilisation des signaux.
 * \date 15/01/2011 Création des méthodes SelectItem et ToggleItem.
 * \date 17/01/2011 Héritage de GuiFrame à la place de GuiWidget.
 * \date 29/05/2011 La classe est renommée en CListView.
 */

#ifndef T_FILE_GUI_CLISTVIEW_HPP_
#define T_FILE_GUI_CLISTVIEW_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IScrollArea.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CListView
 * \ingroup Gui
 * \brief   Ce widget représente une liste d'éléments (pour le moment des chaines de caractère).
 *
 * \section Signaux
 * \li onSelectionChange
 * \li onCurrentItemChange
 * \li onItemSelected
 ******************************/

class T_GUI_API CListView : public IScrollArea
{
public:

    /**
     * \enum    TSelectionMode
     * \ingroup Gui
     * \brief   Modes de sélection des items d'une liste.
     ******************************/

    enum TSelectionMode
    {
        Single,       ///< Un seul item peut être sélectionné, et on ne peut pas désélectionner un item sélectionné.
        SingleOrNone, ///< Un seul item peut être sélectionné, et on peut désélectionner un item sélectionné.
        Multiple,     ///< On peut sélectionner plusieurs items en cliquant sur chacun d'eux (le clic inverse la sélection).
        Extended,     ///< On peut sélectionner plusieurs items en utilisant les touches Ctrl et Shift.
        None          ///< On ne peut sélectionner aucun item.
    };

    // Constructeur
    explicit CListView(IWidget * parent = nullptr);

    // Accesseurs
    int getCurrentItem() const;
    TSelectionMode getSelectionMode() const;
    int getPadding() const;
    CString getItem(int pos) const;

    // Mutateurs
    void setCurrentItem(int select);
    void setSelectionMode(TSelectionMode mode);
    void setPadding(int padding);

    // Méthodes publiques
    int getNumItems() const;
    int getLineHeight() const;
    void addItem(const CString& text, int pos = -1);
    void removeItem(int pos);
    void selectItem(int pos, bool select = true);
    void toggleItem(int pos);
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    void clear();
    void clearSelection();
    void invertSelection();
    void selectAll();

    // Signaux
    sigc::signal<void> onSelectionChange;        ///< Signal émis lorsque la sélection change.
    sigc::signal<void, int> onCurrentItemChange; ///< Signal émis lorsque l'item actif change.
    sigc::signal<void, int> onItemSelected;      ///< Signal émis lorsque un item est sélectionné.

private:

    /// Représente une ligne de la liste.
    struct TListItem
    {
        bool selected; ///< Indique si la ligne est sélectionnée.
        CString text;  ///< Texte de la ligne.
    };

    typedef std::list<TListItem> TListItemList;

    // Données protégées
    int m_current_item;    ///< Numéro de la ligne sélectionnée.
    int m_line_top;        ///< Numéro de la première ligne à afficher.
    int m_num_items;       ///< Nombre d'items dans la liste.
    TSelectionMode m_mode; ///< Mode de sélection.
    int m_padding;         ///< Espacement au-dessus et en-dessous d'un item en pixels (0 par défaut).
    TListItemList m_items; ///< Liste des items.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLISTVIEW_HPP_

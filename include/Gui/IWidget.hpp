/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IWidget.hpp
 * \date       2008 Création de la classe GuiWidget.
 * \date 17/07/2010 Suppression des fichiers GuiOutput.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 19/11/2010 Ajout de l'énumération TAlignment.
 * \date 06/03/2011 Déplacement de TAlignment. Création de la méthode getCursorType.
 * \date 11/05/2011 On peut activer et désactiver un objet.
 * \date 29/05/2011 La classe est renommée en IWidget.
 * \date 20/04/2013 Les widgets possèdent une liste d'objets enfants.
 */

#ifndef T_FILE_GUI_IWIDGET_HPP_
#define T_FILE_GUI_IWIDGET_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/Enums.hpp"
#include "Graphic/CColor.hpp"
#include "Core/IEventReceiver.hpp"


namespace Ted
{

/**
 * \class   IWidget
 * \ingroup Gui
 * \brief   Classe de base des objets de l'interface graphique.
 *
 * Un objet (ou widget) est défini par sa position et ses dimensions (réelles,
 * minimales, et maximales), et peut recevoir des évènements venant du clavier
 * ou de la souris.
 *
 * Les objets sont placés dans une hiérarchie. Chaque objet peut avoir un parent,
 * et une liste d'enfants. Lorsqu'un objet est détruit, ses descendants sont détruits
 * récursivement. Seuls les fenêtres et les conteneurs devraient avoir des enfants.
 *
 * Un objet peut être activé ou désactivé. Un objet désactivé ne s'occupe pas
 * des évènements qu'il reçoit, et dans certains cas s'affiche différemment.
 * Par ailleurs, si un objet a un l'un de ses ancêtre désactivé, il est lui-même
 * obligatoirement désactivé.
 ******************************/

class T_GUI_API IWidget : public IEventReceiver
{
public:

    // Constructeur et destructeur
    explicit IWidget(IWidget * parent = nullptr);
    virtual ~IWidget();

    // Position
    virtual int getX() const;
    virtual void setX(int x);
    virtual int getY() const;
    virtual void setY(int y);
    virtual void setPosition(int x, int y);

    // Dimensions
    virtual inline int getWidth() const;
    virtual void setWidth(int width);
    virtual inline int getHeight() const;
    virtual void setHeight(int height);
    virtual void setSize(int width, int height);
    virtual inline int getMinWidth() const;
    virtual void setMinWidth(int width);
    virtual inline int getMinHeight() const;
    virtual void setMinHeight(int height);
    virtual void setMinSize(int width, int height);
    virtual inline int getMaxWidth() const;
    virtual void setMaxWidth(int width);
    virtual inline int getMaxHeight() const;
    virtual void setMaxHeight(int height);
    virtual void setMaxSize(int width, int height);

    // Activation
    inline bool isEnable() const;
    bool isHierarchyEnable() const;
    void setEnable(bool enable = true);

    // Hiérarchie
    inline IWidget * getParent() const;
    void setParent(IWidget * const parent = nullptr);
    inline std::list<IWidget *> getChildren() const;
    void addChild(IWidget * const widget);
    void removeChild(IWidget * const widget);
    bool isAncestorOf(IWidget * const widget) const;
    bool hasChild(IWidget * const widget) const;

    // Affichage
#ifdef T_USE_PIXMAP
    virtual void getPixmap(CImage& pixmap) const;
    virtual void paint(CImage& pixmap);
    virtual void update(unsigned int frameTime);
    void repaint();
    bool needRepaint() const;
#else
    virtual void draw() = 0;
#endif

    virtual TCursor getCursorType(int x, int y) const;

    // Évènements
    virtual void onEvent(const CMouseEvent& event)    { T_UNUSED(event); } // MinGW ne trouve pas la méthode virtuelle...
    virtual void onEvent(const CTextEvent& event)     { T_UNUSED(event); } // MinGW ne trouve pas la méthode virtuelle...
    virtual void onEvent(const CKeyboardEvent& event) { T_UNUSED(event); } // MinGW ne trouve pas la méthode virtuelle...
    virtual void onEvent(const CResizeEvent& event);

private:

    // Attribut privé
    bool m_enable;      ///< Indique si le widget est actif ou pas.

#ifdef T_USE_PIXMAP
    bool m_needRepaint; ///< Indique si le widget doit être redessiné.
#endif

protected:

    // Attributs protégés
    int m_x;                         ///< Coordonnée horizontale.
    int m_y;                         ///< Coordonnée verticale.
    int m_width;                     ///< Largeur de l'objet en pixels.
    int m_height;                    ///< Hauteur de l'objet en pixels.
    int m_minWidth;                  ///< Largeur minimale de l'objet en pixels.
    int m_minHeight;                 ///< Hauteur minimale de l'objet en pixels.
    int m_maxWidth;                  ///< Largeur maximale de l'objet en pixels.
    int m_maxHeight;                 ///< Hauteur maximale de l'objet en pixels.
    IWidget * m_parent;              ///< Pointeur sur l'objet parent.
    std::list<IWidget *> m_children; ///< Liste des objets enfants.
};


/**
 * Donne la largeur de l'objet.
 *
 * \return Largeur de l'objet en pixels.
 *
 * \sa IWidget::setWidth
 ******************************/

inline int IWidget::getWidth() const
{
    return m_width;
}


/**
 * Donne la hauteur de l'objet.
 *
 * \return Hauteur de l'objet en pixels.
 *
 * \sa IWidget::setHeight
 ******************************/

inline int IWidget::getHeight() const
{
    return m_height;
}


/**
 * Donne la largeur minimale de l'objet.
 *
 * \return Largeur minimale du widget en pixels.
 *
 * \sa IWidget::setMinWidth
 ******************************/

inline int IWidget::getMinWidth() const
{
    return m_minWidth;
}


/**
 * Donne la hauteur minimale de l'objet.
 *
 * \return Hauteur minimale du widget en pixels.
 *
 * \sa IWidget::setMinHeight
 ******************************/

inline int IWidget::getMinHeight() const
{
    return m_minHeight;
}


/**
 * Donne la largeur maximale du widget.
 *
 * \return Largeur maximale du widget en pixels.
 *
 * \sa IWidget::setMaxWidth
 ******************************/

inline int IWidget::getMaxWidth() const
{
    return m_maxWidth;
}


/**
 * Donne la hauteur maximale du widget.
 *
 * \return Hauteur maximale du widget en pixels.
 *
 * \sa IWidget::setMaxHeight
 ******************************/

inline int IWidget::getMaxHeight() const
{
    return m_maxHeight;
}


/**
 * Indique si le widget est actif ou pas.
 *
 * \return Booléen.
 *
 * \sa IWidget::setEnable
 * \sa IWidget::isHierarchyEnable
 ******************************/

inline bool IWidget::isEnable() const
{
    return m_enable;
}


/**
 * Donne le pointeur sur l'objet parent.
 *
 * \return Pointeur sur l'objet parent, ou un pointeur invalide.
 *
 * \sa IWidget::setParent
 ******************************/

inline IWidget * IWidget::getParent() const
{
    return m_parent;
}


/**
 * Donne la liste des objets enfants.
 *
 * \return Liste des objets enfants.
 ******************************/

inline std::list<IWidget *> IWidget::getChildren() const
{
    return m_children;
}

} // Namespace Ted

#endif // T_FILE_GUI_IWIDGET_HPP_

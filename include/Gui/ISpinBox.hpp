/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ISpinBox.hpp
 * \date 24/02/2010 Création de la classe GuiBaseSpinBox.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en ISpinBox.
 * \date 01/05/2012 La valeur est modifiée lorsqu'un bouton reste enfoncé.
 */

#ifndef T_FILE_GUI_ISPINBOX_HPP_
#define T_FILE_GUI_ISPINBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class CLineEdit;
class CPushButton;


/**
 * \class   ISpinBox
 * \ingroup Gui
 * \brief   Classe de base des spin box.
 *
 * Une spin box est un champ texte possédant deux boutons pour augmenter ou diminuer
 * la valeur du champ.
 ******************************/

class T_GUI_API ISpinBox : public IWidget
{
public:

    // Constructeur et destructeur
    explicit ISpinBox(IWidget * parent = nullptr);
    virtual ~ISpinBox();

    // Accesseurs
    inline bool getWrapping() const;
    inline CString getPrefix() const;
    inline CString getSuffix() const;

    // Mutateurs
    void setWrapping(bool wrapping = true);
    void setPrefix(const CString& prefix);
    void setSuffix(const CString& suffix);
    void setWidth(int width);

    // Méthodes publiques
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    virtual void stepUp() = 0;
    virtual void stepDown() = 0;

private:

    // Méthode privée
    virtual void updateValue() = 0;

    unsigned int m_timeLast;   ///< Instant du dernier affichage du widget.
    unsigned int m_timeRepeat; ///< Compteur de temps utilisé pour la répétition lorsqu'un bouton est enfoncé.

protected:

    // Données protégées
    bool m_wrapping;            ///< Indique si la spinbox est circulaire.
    CString m_prefix;           ///< Préfixe.
    CString m_suffix;           ///< Suffixe.
    CLineEdit * m_line;         ///< Ligne de texte.
    CPushButton * m_buttonUp;   ///< Bouton pour incrémenter.
    CPushButton * m_buttonDown; ///< Bouton pour décrémenter.
};


/**
 * Indique si la spinbox est circulaire.
 *
 * \return Booléen.
 *
 * \sa ISpinBox::setWrapping
 ******************************/

inline bool ISpinBox::getWrapping() const
{
    return m_wrapping;
}


/**
 * Donne le préfixe utilisé dans le champ texte.
 *
 * \return Préfixe.
 *
 * \sa ISpinBox::setPrefix
 ******************************/

inline CString ISpinBox::getPrefix() const
{
    return m_prefix;
}


/**
 * Donne le suffixe utilisé dans le champ texte.
 *
 * \return Suffixe.
 *
 * \sa ISpinBox::setSuffix
 ******************************/

inline CString ISpinBox::getSuffix() const
{
    return m_suffix;
}

} // Namespace Ted

#endif // T_FILE_GUI_ISPINBOX_HPP_

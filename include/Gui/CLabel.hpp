/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLabel.hpp
 * \date 18/01/2010 Création de la classe GuiLabel.
 * \date 23/01/2011 Ajout d'un attribut pour stocker les paramètres du texte.
 * \date 08/05/2011 On peut modifier la taille du texte.
 * \date 29/05/2011 La classe est renommée en CLabel.
 * \date 03/04/2012 On peut modifier la police de caractère.
 */

#ifndef T_FILE_GUI_CLABEL_HPP_
#define T_FILE_GUI_CLABEL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Graphic/CFontManager.hpp"


namespace Ted
{

/**
 * \class   CLabel
 * \ingroup Gui
 * \brief   Un label permet d'écrire du texte dans une fenêtre.
 ******************************/

class T_GUI_API CLabel : public IWidget
{
public:

    // Constructeurs
    explicit CLabel(const CString& text, IWidget * parent = nullptr);
    explicit CLabel(IWidget * parent = nullptr);

    // Accesseurs
    CString getText() const;
    THorizontalAlignment getTextAlignment() const;
    CColor getTextColor() const;
    CString getTextFont() const;
    int getFontSize() const;

    // Mutateurs
    void setText(const CString& text);
    void setTextAlignment(THorizontalAlignment align);
    void setTextColor(const CColor& color);
    void setTextFont(const CString& font);
    void setFontSize(int fontSize);

    // Méthode publique
    virtual void draw();

private:

    // Données protégées
    THorizontalAlignment m_align; ///< Alignement du texte (à gauche par défaut).
    TTextParams m_params;         ///< Paramètres du texte.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLABEL_HPP_

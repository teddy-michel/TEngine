/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ILayout.hpp
 * \date 29/05/2009 Création de la classe GuiBaseLayout.
 * \date 25/07/2010 L'énumération LayoutAlignement est sortie de la classe GuiBaseLayout.
 * \date 29/05/2011 La classe est renommée en ILayout.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 * \date 14/06/2014 Déplacement de l'énumération TLayoutAlignment.
 */

#ifndef T_FILE_GUI_ILAYOUT_HPP_
#define T_FILE_GUI_ILAYOUT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Gui/CMargins.hpp"


namespace Ted
{

/**
 * \class   ILayout
 * \ingroup Gui
 * \brief   Classe de base des layouts, qui permettent d'organiser les widgets.
 ******************************/

class T_GUI_API ILayout : public IWidget
{
    friend class CWindow; // Pour appeler la méthode ComputeSize

public:

    // Constructeur et destructeur
    explicit ILayout(IWidget * parent = nullptr);
    virtual ~ILayout();

    // Accesseurs
    CMargins getPadding() const;
    CMargins getMargin() const;

    // Mutateurs
    void setPadding(const CMargins& padding);
    void setMargin(const CMargins& margin);

protected:

    // Méthode privée
    virtual void computeSize() = 0;

    // Données protégées
    CMargins m_padding; ///< Marges internes de chaque case en pixels.
    CMargins m_margin;  ///< Marges extérieures en pixels.
};

} // Namespace Ted

#endif // T_FILE_GUI_ILAYOUT_HPP_

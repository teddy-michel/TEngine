/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CComboBox.hpp
 * \date 02/03/2010 Création de la classe GuiComboBox.
 * \date 04/01/2011 Utilisation d'une ListView.
 * \date 15/01/2011 Création de la méthode SelectItem.
 * \date 29/05/2011 La classe est renommée en CComboBox.
 */

#ifndef T_FILE_GUI_CCOMBOBOX_HPP_
#define T_FILE_GUI_CCOMBOBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"


namespace Ted
{

class CLineEdit;
class CPushButton;
class CListView;


/**
 * \class   CComboBox
 * \ingroup Gui
 * \brief   Affichage d'une liste déroulante.
 *
 * \todo    L'utilisateur doit pouvoir ajouter manuellement des entrées à la liste.
 *
 * \section Signaux
 * \li onSelectedChange
 * \li onValueChange
 ******************************/

class T_GUI_API CComboBox : public IWidget
{
public:

    static const int LastPosition = -1;

    // Constructeur et destructeur
    explicit CComboBox(IWidget * parent = nullptr);
    virtual ~CComboBox();

    // Accesseurs
    int getNumItems() const;
    int getSelect() const;
    CString getItemSelected() const;

    // Mutateurs
    void setSelect(int select);
    void setWidth(int width);
    void setEditable(bool editable = true);

    // Méthodes publiques
    void addItem(const CString& item, int pos = LastPosition);
    void removeItem(int pos);
    void selectItem(int pos);
    void clearItems();
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);

    // Signaux
    sigc::signal<void, int> onSelectedChange; ///< Signal émis lorsque l'item sélectionné change.
    sigc::signal<void, const CString&> onValueChange;  ///< Signal émis lorsque la valeur change.

private:

    // Méthode privée
    void toggleList();

private:

    // Données protégées
    bool m_show_list;       ///< Indique si la liste doit être affichée.
    CLineEdit * m_line;     ///< Pointeur sur la ligne de texte.
    CPushButton * m_button; ///< Pointeur sur le bouton contenant la flèche.
    CListView * m_list;     ///< Pointeur sur la liste.
};

} // Namespace Ted

#endif // T_FILE_GUI_CCOMBOBOX_HPP_

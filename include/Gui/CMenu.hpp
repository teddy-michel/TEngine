/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CMenu.hpp
 * \date 06/05/2009 Création de la classe GuiMenu.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CMenu.
 * \date 04/04/2013 Nouveaux paramètres : itemHeight, itemMargin, padding, fontId, backgroundColor.
 * \date 06/04/2013 Nouveaux paramètres : fontSize, itemColorActive, itemColorInactive, fontColorActive, fontColorInactive.
 * \date 13/06/2014 Nouveau paramètre : itemPadding.
 * \date 14/06/2014 Ajout d'un paramètre pour aligner le texte.
 */

#ifndef T_FILE_GUI_CMENU_HPP_
#define T_FILE_GUI_CMENU_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <sigc++/sigc++.h>

#include "Gui/IWidget.hpp"
#include "Gui/CMargins.hpp"
#include "Gui/ILayout.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class CMenuItem;


/**
 * \class   CMenu
 * \ingroup Gui
 * \brief   Gestion d'un menu contenant des éléments cliquables.
 *
 * Pour le moment les menus ne sont utilisés que dans l'interface principale du jeu.
 ******************************/

class T_GUI_API CMenu : public IWidget
{
public:

    // Constructeur et destructeur
    explicit CMenu(IWidget * parent = nullptr);
    virtual ~CMenu();

    // Modification des éléments
    int getNumItems() const;
    void addItem(CMenuItem * item);
    void addItem(const CString& name, const sigc::slot<void>& slot);
    void addSeparator();
    void removeItem(CMenuItem * item);
    void removeItem(int item);
    void clearItems();

    // Paramètres graphiques
    virtual void setWidth(int width);
    inline int getItemHeight() const;
    void setItemHeight(int height);
    inline CMargins getPadding() const;
    void setPadding(const CMargins& margin);
    inline CMargins getItemMargin() const;
    void setItemMargin(const CMargins& margin);
    inline CMargins getItemPadding() const;
    void setItemPadding(const CMargins& margin);
    inline TFontId getFontId() const;
    void setFontId(TFontId font);
    inline int getFontSize() const;
    void setFontSize(int fontSize);
    inline CColor getBackgroundColor() const;
    void setBackgroundColor(const CColor& color);
    inline CColor getItemColorActive() const;
    void setItemColorActive(const CColor& color);
    inline CColor getItemColorInactive() const;
    void setItemColorInactive(const CColor& color);
    inline CColor getFontColorActive() const;
    void setFontColorActive(const CColor& color);
    inline CColor getFontColorInactive() const;
    void setFontColorInactive(const CColor& color);
    inline TAlignment getTextAlignment() const;
    void setTextAlignment(TAlignment align);

    virtual void onEvent(const CResizeEvent& event) { T_UNUSED(event); } // MinGW ne trouve pas la méthode virtuelle...
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void draw();

private:

    void addItemPrivate(CMenuItem * item);
    void updateItemsSize();

    // Données privées
    int m_itemHeight;               ///< Hauteur de chaque élément en pixels.
    CMargins m_padding;             ///< Marges internes du menu.
    CMargins m_itemMargin;          ///< Marges externes des éléments du menu.
    CMargins m_itemPadding;         ///< Marges internes des éléments du menu.
    TFontId m_fontId;               ///< Police utilisée pour le texte des éléments.
    int m_fontSize;                 ///< Taille du texte.
    CColor m_backgroundColor;       ///< Couleur de fond du menu.
    CColor m_itemColorActive;       ///< Couleur de fond des éléments actifs.
    CColor m_itemColorInactive;     ///< Couleur de fond des éléments inactifs.
    CColor m_fontColorActive;       ///< Couleur du texte des éléments actifs.
    CColor m_fontColorInactive;     ///< Couleur du texte des éléments inactifs.
    TAlignment m_textAlign;   ///< Alignement du texte des éléments.
    int m_currentItem;              ///< Élément actuellement sélectionné pour la navigation au clavier.
    std::list<CMenuItem *> m_items; ///< Liste des items du menu.
};


/**
 * Retourne la hauteur de chaque élément du menu.
 *
 * \return Hauteur d'un élément en pixels.
 ******************************/

inline int CMenu::getItemHeight() const
{
    return m_itemHeight;
}


/**
 * Retourne la valeur des marges internes du menu.
 *
 * \return Marges internes du menu.
 ******************************/

inline CMargins CMenu::getPadding() const
{
    return m_padding;
}


/**
 * Retourne la valeur des marges externes de chaque élément du menu.
 *
 * \return Marges externes des éléments du menu.
 ******************************/

inline CMargins CMenu::getItemMargin() const
{
    return m_itemMargin;
}


/**
 * Retourne la valeur des marges integers de chaque élément du menu.
 *
 * \return Marges internes des éléments du menu.
 ******************************/

inline CMargins CMenu::getItemPadding() const
{
    return m_itemPadding;
}


/**
 * Retourne l'identifiant de la police utilisée pour les éléments du menu.
 *
 * \return Identifiant de la police.
 ******************************/

inline TFontId CMenu::getFontId() const
{
    return m_fontId;
}


/**
 * Retourne la taille du texte des éléments du menu.
 *
 * \return Taille du texte.
 ******************************/

inline int CMenu::getFontSize() const
{
    return m_fontSize;
}


/**
 * Retourne la couleur de fond du menu.
 *
 * \return Couleur de fond du menu.
 ******************************/

inline CColor CMenu::getBackgroundColor() const
{
    return m_backgroundColor;
}


/**
 * Retourne la couleur de fond des éléments actifs du menu.
 *
 * \return Couleur de fond quand l'élément est actif.
 ******************************/

inline CColor CMenu::getItemColorActive() const
{
    return m_itemColorActive;
}


/**
 * Retourne la couleur de fond des éléments inactifs du menu.
 *
 * \return Couleur de fond quand l'élément est inactif.
 ******************************/

inline CColor CMenu::getItemColorInactive() const
{
    return m_itemColorInactive;
}


/**
 * Retourne la couleur du texte des éléments actifs du menu.
 *
 * \return Couleur du texte quand l'élément est actif.
 ******************************/

inline CColor CMenu::getFontColorActive() const
{
    return m_fontColorActive;
}


/**
 * Retourne la couleur du texte des éléments inactifs du menu.
 *
 * \return Couleur du texte quand l'élément est inactif.
 ******************************/

inline CColor CMenu::getFontColorInactive() const
{
    return m_fontColorInactive;
}


inline TAlignment CMenu::getTextAlignment() const
{
    return m_textAlign;
}

} // Namespace Ted

#endif // T_FILE_GUI_CMENU_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutForm.hpp
 * \date 15/07/2010 Création de la classe GuiLayoutForm.
 * \date 29/05/2011 La classe est renommée en CLayoutForm.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 */

#ifndef T_FILE_GUI_CLAYOUTFORM_HPP_
#define T_FILE_GUI_CLAYOUTFORM_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>

#include "Gui/Export.hpp"
#include "Gui/ILayout.hpp"


namespace Ted
{

class CLayoutGrid;
class CLabel;


/**
 * \class   CLayoutForm
 * \ingroup Gui
 * \brief   Layout permettant de gérer des champs d'un formulaire avec leur label.
 *
 * Ce layout utilise un layout grille qui comporte deux colonnes. La première
 * colonne est destinée aux labels, et la seconde aux champs du formulaire.
 ******************************/

class T_GUI_API CLayoutForm : public ILayout
{
public:

    // Constructeur et destructeur
    explicit CLayoutForm(IWidget * parent = nullptr);
    virtual ~CLayoutForm();

    // Accesseur
    TAlignment getLabelAlignement() const;
    CMargins getPadding() const;
    CMargins getMargin() const;

    // Mutateurs
    void setLabelAlignement(TAlignment align);
    void setPadding(const CMargins& padding);
    void setMargin(const CMargins& margin);

    // Méthodes publiques
    void addRow(const CString& label, IWidget * widget, int position = -1);
    void addRow(CLabel * label, IWidget * widget, int position = -1);
    int getNumRows() const;
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);

private:

    // Méthode privée
    virtual void computeSize();

    // Données protégées
    CLayoutGrid * m_layout;  ///< Pointeur sur le layout grille.
    TAlignment m_labelAlign; ///< Alignement des labels.
    int m_nbr_rows;          ///< Nombre de lignes du layout.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLAYOUTFORM_HPP_

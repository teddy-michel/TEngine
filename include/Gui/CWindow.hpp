/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow.hpp
 * \date    04/2009 Création de la classe GuiWindow.
 * \date 08/07/2010 On peut choisir d'afficher ou pas la barre de titre et la bordure.
 * \date 10/07/2010 Modification des paramètres pour afficher ou pas la bordure.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 29/05/2011 La classe est renommée en CWindow.
 * \date 03/04/2013 Mémorisation des paramètres du texte pour le titre.
 * \date 03/04/2013 Ajout d'attributs pour définir les couleurs de la fenêtre.
 */

#ifndef T_FILE_GUI_CWINDOW_HPP_
#define T_FILE_GUI_CWINDOW_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <sigc++/sigc++.h>

#include "Gui/IWidget.hpp"
#include "Core/CFlags.hpp"
#include "Core/CString.hpp"
#include "Graphic/CFontManager.hpp"


namespace Ted
{

class ILayout;


/**
 * \class   CWindow
 * \ingroup Gui
 * \brief   Affichage d'une fenêtre.
 *
 * Une fenêtre comporte éventuellement une bordure, une barre de titre, et un
 * widget central.
 *
 * \section Signaux
 * \li onClose
 * \li onOpen
 * \li onMove
 * \li onResize
 ******************************/

class T_GUI_API CWindow : public IWidget
{
public:

    /**
     * \enum    TWindowFlag
     * \ingroup Gui
     * \brief   Flags utilisés lors de la création d'une fenêtre.
     ******************************/

    enum TWindowFlag
    {
        WindowCenterX       = 0x01, ///< Fenêtre centrée horizontalement.
        WindowCenterY       = 0x02, ///< Fenêtre centrée verticalement.
        WindowCenter        = WindowCenterX | WindowCenterY,
        WindowNotResizable  = 0x04, ///< Fenêtre non redimensionnable.
        WindowNotMovable    = 0x08, ///< Fenêtre non déplaçable.
        WindowNoButtonClose = 0x10, ///< Enlève la croix pour fermer la fenêtre.
        WindowNoBorder      = 0x20 | WindowNoButtonClose ///< Enlève la bordure et la barre de titre.
    };

    T_DECLARE_FLAGS(TWindowFlags, TWindowFlag)


    // Constructeurs et destructeur
    explicit CWindow(const CString& title, TWindowFlags flags = 0, IWidget * parent = nullptr);
    explicit CWindow(IWidget * parent = nullptr);
    virtual ~CWindow();

    // Accesseurs
    inline CString getTitle() const;
    inline bool isOpen() const;
    inline bool isClose() const;
    inline bool isCenterX() const;
    inline bool isCenterY() const;
    inline bool isResizable() const;
    inline bool isMovable() const;
    inline bool hasButtonClose() const;
    inline bool hasBorder() const;
    inline bool hasFocus() const;
    inline IWidget * getChild() const;
    int getRealX() const;
    int getRealY() const;
    int getRealWidth() const;
    int getRealHeight() const;

    // Couleurs de la fenêtre
    CColor getBackgroundColorActive() const;
    void setBackgroundColorActive(const CColor& color);
    CColor getBackgroundColorInactive() const;
    void setBackgroundColorInactive(const CColor& color);
    CColor getBorderColorActive() const;
    void setBorderColorActive(const CColor& color);
    CColor getBorderColorInactive() const;
    void setBorderColorInactive(const CColor& color);

    // Mutateurs
    void setTitle(const CString& title);
    void setMinWidth(int width);
    void setMinHeight(int height);
    void setCenterX(bool centerX = true);
    void setCenterY(bool centerY = true);
    void setResizable( bool resizable = true);
    void setMovable(bool movable = true);
    void setButtonClose(bool close = true);
    void setBorder(bool border = true);
    void setPosition(int x, int y);
    virtual void setX(int x);
    virtual void setY(int y);
    virtual void setWidth(int width);
    virtual void setHeight(int height);
    void setFocus(bool focus = true);
    void setChild(IWidget * child);
    void setLayout(ILayout * layout);

    // Méthodes publiques
    virtual void onEvent(const CResizeEvent& event);
    virtual void onEvent(const CMouseEvent& event);
#ifdef T_USE_PIXMAP
    virtual void getPixmap(CImage& pixmap) const;
    virtual void paint(CImage& pixmap);
    virtual void update(unsigned int frameTime);
#else
    virtual void draw();
#endif
    virtual TCursor getCursorType(int x, int y) const;
    void removeChild();
    void open();
    void close();

    // Signaux
    sigc::signal<void> onClose;  ///< Signal émis lorsque la fenêtre est fermée.
    sigc::signal<void> onOpen;   ///< Signal émis lorsque la fenêtre est ouverte.
    sigc::signal<void> onMove;   ///< Signal émis lorsque la fenêtre est déplacée.
    sigc::signal<void> onResize; ///< Signal émis lorsque la fenêtre est redimensionnée.

private:

    void initWindow();

    // Données privées
    int m_moveX;              ///< Déplacement relatif de la fenêtre par rapport au curseur en cas de déplacement.
    int m_moveY;              ///< Déplacement relatif de la fenêtre par rapport au curseur en cas de déplacement.

    int m_resizeLeft;         ///< Redimensionnement à gauche.
    int m_resizeRight;        ///< Redimensionnement à droite.
    int m_resizeTop;          ///< Redimensionnement en haut.
    int m_resizeBottom;       ///< Redimensionnement en bas.

    TTextParams m_textParams; ///< Paramètres du texte pour le titre.

    // Couleurs de la fenêtre
    CColor m_backgroundColorActive;   ///< Couleur de fond quand la fenêtre est active.
    CColor m_backgroundColorInactive; ///< Couleur de fond quand la fenêtre est inactive.
    CColor m_borderColorActive;       ///< Couleur de la bordure quand la fenêtre est active.
    CColor m_borderColorInactive;     ///< Couleur de la bordure quand la fenêtre est inactive.

    uint32_t m_closed:1;      ///< Indique si la fenêtre est fermée.
    uint32_t m_centerX:1;     ///< Fenêtre centrée horizontalement.
    uint32_t m_centerY:1;     ///< Fenêtre centrée verticalement.
    uint32_t m_resizable:1;   ///< Indique si la fenêtre est redimensionnable.
    uint32_t m_movable:1;     ///< Indique si la fenêtre est déplaçable.
    uint32_t m_buttonClose:1; ///< Indique si la fenêtre possède une croix pour être fermée.
    uint32_t m_border:1;      ///< Indique si on doit afficher la bordure.
    uint32_t m_focus:1;       ///< Indique si la fenêtre a le focus.
    uint32_t m_padding:24;

    unsigned int m_time;      ///< Instant du dernier affichage.
    float m_transparency;     ///< Pourcentage de transparence.

    // Méthode protégée
    void computeSize();

protected:

    // Données protégées
    IWidget * m_child;        ///< Pointeur sur l'objet enfant.
    ILayout * m_layout;       ///< Pointeur sur le layout.
};


T_DECLARE_OPERATORS_FOR_FLAGS(CWindow::TWindowFlags)


/**
 * Donne le titre de la fenêtre.
 *
 * \return Titre de la fenêtre.
 *
 * \sa CWindow::setTitle
 ******************************/

inline CString CWindow::getTitle() const
{
    return m_textParams.text;
}


/**
 * Indique si la fenêtre est ouverte.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::isOpen() const
{
    return !m_closed;
}


/**
 * Accesseur pour closed.
 *
 * \return Indique si la fenêtre est fermée.
 ******************************/

inline bool CWindow::isClose() const
{
    return m_closed;
}


/**
 * Accesseur pour center_x.
 *
 * \return Booléen indiquant si la fenêtre est centré horizontalement.
 ******************************/

inline bool CWindow::isCenterX() const
{
    return m_centerX;
}


/**
 * Accesseur pour center_y.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::isCenterY() const
{
    return m_centerY;
}


/**
 * Indique si la fenêtre est redimensionnable.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::isResizable() const
{
    return m_resizable;
}


/**
 * Indique si la fenêtre est déplaçable.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::isMovable() const
{
    return m_movable;
}


/**
 * Indique si la fenêtre dispose d'un bouton pour la fermer.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::hasButtonClose() const
{
    return m_buttonClose;
}


/**
 * Indique si la bordure est affichée.
 *
 * \return Booléen.
 ******************************/

inline bool CWindow::hasBorder() const
{
    return m_border;
}


/**
 * Accesseur pour focus.
 *
 * \return Indique si la fenêtre a le focus.
 *
 * \sa CWindow::setFocus
 ******************************/

inline bool CWindow::hasFocus() const
{
    return m_focus;
}


/**
 * Accesseur pour child.
 *
 * \return Pointeur sur l'objet enfant.
 ******************************/

inline IWidget * CWindow::getChild() const
{
    return m_child;
}

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOW_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow_Load.hpp
 * \date 28/05/2009 Création de la classe GuiWindow_Load.
 * \date 29/05/2011 La classe est renommée en CWindow_Load.
 */

#ifndef T_FILE_GUI_CWINDOW_LOAD_HPP_
#define T_FILE_GUI_CWINDOW_LOAD_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/CWindow.hpp"


namespace Ted
{

class CPushButton;


/**
 * \class   CWindow_Load
 * \ingroup Gui
 * \brief   Classe permettant de gérer la fenêtre Charger une partie.
 ******************************/

class T_GUI_API CWindow_Load : public CWindow
{
public:

    // Constructeur
    explicit CWindow_Load(IWidget * parent = nullptr);

private:

    // Donnée privée
    CPushButton * m_button_close; ///< Bouton pour fermer la fenêtre.
};

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOW_LOAD_HPP_

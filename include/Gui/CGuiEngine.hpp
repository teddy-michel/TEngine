/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGuiEngine.hpp
 * \date       2008 Création de la classe GuiEngine.
 * \date 19/07/2010 La méthode ShowCursor est déplacée vers la classe CApplication.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 30/10/2010 Gestion des éléments au premier-plan (menus, infobulles, et combobox).
 * \date 23/01/2011 Les méthodes d'affichage prennent un CRectangle en paramètre à la place de quatre.
 * \date 29/01/2011 Ajout d'une liste de HUD à afficher.
 * \date 14/02/2011 Utilisation de la couleur ou de l'image de fond.
 * \date 02/03/2011 Création de l'énumération TCursor, et des méthodes getCursor, setCursor.
 * \date 06/03/2011 Déplacement de TCursor.
 * \date 29/05/2011 La classe est renommée en CGuiEngine.
 * \date 10/12/2011 Gestion des curseurs de la souris.
 * \date 07/06/2014 La classe CActivity s'occupe de la gestion des objets de l'interface.
 */

#ifndef T_FILE_GUI_CGUIENGINE_HPP_
#define T_FILE_GUI_CGUIENGINE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/CMenu.hpp"
#include "Gui/Enums.hpp"
#include "Gui/CCursor.hpp"
#include "Core/IEventReceiver.hpp"
#include "Graphic/CColor.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/CApplication.hpp"

#include "os.h"


namespace Ted
{

class CBuffer2D;
class CRectangle;
class TTextParams;
class IHUD;
class CGuiEngine;
class CWindow;
class CActivity;


namespace Game
{
    extern T_GUI_API CGuiEngine * guiEngine; // Défini dans CGuiEngine.cpp
}


/**
 * \class   CGuiEngine
 * \ingroup Gui
 * \brief   Moteur de l'interface graphique.
 ******************************/

class T_GUI_API CGuiEngine : public IEventReceiver
{
    friend class CActivity;

public:

    // Constructeur et destructeur
    CGuiEngine();
    virtual ~CGuiEngine();

    // Accesseurs
    bool hasFocus(const IWidget * objet) const;
    IWidget * getFocus() const;
    bool isPointed(const IWidget * objet) const;
    IWidget * getPointed() const;
    bool isSelected(const IWidget * objet) const;
    IWidget * getSelected() const;
    bool isForeground(const IWidget * objet) const;
    IWidget * getForeground() const;
    CColor getBackgroundColor() const;
    TTextureId getBackgroundImage() const;
    TCursor getCursor() const;

    // Mutateurs
    void setFocus(IWidget * widget = nullptr);
    void setPointed(IWidget * widget = nullptr);
    void setSelected(IWidget * widget = nullptr);
    void setForeground(IWidget * widget = nullptr);
    void setBackgroundColor(const CColor& color);
    void setBackgroundImage(const TTextureId texture);
    void setCursor(TCursor cursor);

    // Méthodes publiques
    virtual void init();

    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event);

    void beginFrame();
    void displayFrame();
    void endFrame();

    void onWindowOpen(CWindow * window);
    void onWindowClose(CWindow * window);

    void addHUD(IHUD * hud);
    void removeHUD(IHUD * hud);
    unsigned int getNumHUD() const;

    void drawRectangle(const CRectangle& rec, const CColor& color) const;
    void drawRectangleRounded(const CRectangle& rec, const CColor& color) const;
    void drawBorder(const CRectangle& rec, const CColor& color, unsigned int thickness = 1) const;
    void drawBorderRounded(const CRectangle& rec, const CColor& color, unsigned int thickness = 1) const;
    void drawDisc(int x, int y, unsigned int radius, const CColor& color) const;
    void drawCircle(int x, int y, unsigned int radius, const CColor& color) const;
    void drawText(const TTextParams& params) const;
    void drawImage(const CRectangle& rec, TTextureId texture) const;
    void drawLine(const TVector2F& from, const TVector2F& to, const CColor& color) const;
    void drawPoint(const TVector2F& pos, const CColor& color) const;

    // Slots publics
    void inputExit();
    void inputResume();

    // Activités
    CActivity * getActivity(const CString& activityName) const;
    CActivity * getCurrentActivity() const;
    void setCurrentActivity(const CString& activityName);
    void setCurrentActivity(CActivity * activity);

private:

    // Activités
    void addActivity(CActivity * activity);
    void removeActivity(CActivity * activity);

    // Méthodes privées
    void frameGame();


    // Curseurs
    CCursor m_cursorArrow;
    CCursor m_cursorHand;
    CCursor m_cursorText;
    CCursor m_cursorResizeNE_SW;
    CCursor m_cursorResizeN_S;
    CCursor m_cursorResizeNW_SE;
    CCursor m_cursorResizeW_E;

    CColor m_backgroundColor;           ///< Couleur de fond (gris transparent par défaut).
    TTextureId m_backgroundImage;       ///< Identifiant de la texture contenant l'image de fond (aucune image par défaut).
    TCursor m_cursor;                   ///< Curseur à utiliser.
    CBuffer2D * m_buffer;               ///< Buffer graphique pour l'affichage des widgets.

    std::map<CString, CActivity *> m_activities; ///< Liste des activités de l'application.
    CActivity * m_currentActivity;               ///< Activité actuellement utilisée.

    std::list<IHUD *> m_hud;            ///< Liste des HUD à afficher dans une partie.
};

} // Namespace Ted

#endif // T_FILE_GUI_CGUIENGINE_HPP_

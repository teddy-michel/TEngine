/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ICurve.hpp
 * \date 19/04/2013 Création de la classe ICurve.
 */

#ifndef T_FILE_GUI_ICURVE_HPP_
#define T_FILE_GUI_ICURVE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IWidget.hpp"
#include "Graphic/CColor.hpp"

#ifdef T_USE_PIXMAP
#  include "Graphic/CImage.hpp"
#else
#  include "Core/Maths/CRectangle.hpp"
#endif


namespace Ted
{

/**
 * \class   ICurve
 * \ingroup Gui
 * \brief   Classe de base des courbes affichables dans un graphique.
 ******************************/

class T_GUI_API ICurve : public IWidget
{
public:

    /// Mode d'affichage de la courbe.
    enum TMode
    {
        Point,        ///< Affichage de points.
        JoinedPoints, ///< Points reliés deux à deux.
        VLine         ///< Lignes verticales.
    };


    // Constructeur et destructeur
    explicit ICurve(IWidget * parent = nullptr);
    virtual ~ICurve();

    inline CColor getColor() const;
    void setColor(const CColor& color);

    inline TMode getMode() const;
    void setMode(TMode mode);

    virtual float getCurveYFromX(float x) const = 0;
    virtual float getCurveMinY(float minX, float maxX) const = 0;
    virtual float getCurveMaxY(float minX, float maxX) const = 0;
    virtual float getCurveMinX() const = 0;
    virtual float getCurveMaxX() const = 0;

    // Affichage
    void setCurveRect(float minX, float maxX, float minY, float maxY);
    virtual void draw();

private:

    // Attributs privés
    CColor m_color; ///< Couleur de la courbe.
    TMode m_mode;   ///< Mode d'affichage.
    float m_minX;   ///< Valeur minimale affichée en abscisse.
    float m_maxX;   ///< Valeur maximale affichée en abscisse.
    float m_minY;   ///< Valeur minimale affichée en ordonnée.
    float m_maxY;   ///< Valeur maximale affichée en ordonnée.
};


/**
 * Retourne la couleur de la courbe.
 *
 * \return Couleur de la courbe.
 ******************************/

inline CColor ICurve::getColor() const
{
    return m_color;
}


/**
 * Retourne le mode d'affichage de la courbe.
 *
 * \return Mode d'affichage de la courbe.
 ******************************/

inline ICurve::TMode ICurve::getMode() const
{
    return m_mode;
}

} // Namespace Ted

#endif // T_FILE_GUI_ICURVE_HPP_

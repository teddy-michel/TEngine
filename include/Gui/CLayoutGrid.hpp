/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutGrid.hpp
 * \date 18/01/2010 Création de la classe GuiLayoutGrid.
 * \date 03/07/2010 Modification de la gestion des évènements.
 * \date 15/07/2010 Ajout de l'alignement des widgets.
 * \date 29/05/2011 La classe est renommée en CLayoutGrid.
 */

#ifndef T_FILE_GUI_CLAYOUTGRID_HPP_
#define T_FILE_GUI_CLAYOUTGRID_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>

#include "Gui/Export.hpp"
#include "Gui/ILayout.hpp"


namespace Ted
{

class CLayoutForm;


/**
 * \class   CLayoutGrid
 * \ingroup Gui
 * \brief   Layout contenant des objets disposés dans une grille.
 ******************************/

class T_GUI_API CLayoutGrid : public ILayout
{
    friend class CLayoutForm; // Un CLayoutForm utilise un CLayoutGrid

public:

    // Constructeur
    explicit CLayoutGrid(IWidget * parent = nullptr);

    // Accesseurs
    int getNumRows() const;
    int getNumCols() const;
    int getRowMinHeight(int row) const;
    int getColumnMinWidth(int col) const;

    // Mutateurs
    void setRowMinHeight(int row, int min);
    void setColMinWidth(int col, int min);

    // Méthodes publiques
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    void addChild(IWidget * widget, int row, int col, int rowspan = 1, int colspan = 1, TAlignment align = AlignMiddleCenter);
    void addChild(IWidget * widget, int row, int col, TAlignment align);
    void removeChild(IWidget * widget);
    void removeChild(int row, int col);

protected:

    // Méthode privée
    virtual void computeSize();

private:

    /// Une case du tableau
    struct TLayoutItem
    {
        IWidget * widget; ///< Pointeur sur l'objet.
        int row;          ///< Numéro de la ligne contenant l'objet.
        int col;          ///< Numéro de la colonne contenant l'objet.
        int rowspan;      ///< Nombre de lignes occupées par l'objet.
        int colspan;      ///< Nombre de colonnes occupées par l'objet.
        TAlignment align; ///< Alignement de l'objet dans sa case.

        /// Constructeur par défaut.
        TLayoutItem() : widget(nullptr), row(0), col(0), rowspan(0), colspan(0), align(AlignMiddleCenter) { }
    };

    // Donnée protégée
    int m_nbr_rows;                     ///< Nombre de lignes.
    int m_nbr_cols;                     ///< Nombre de colonnes.
    std::list<TLayoutItem> m_children;  ///< Liste des objets enfants.
    std::vector<int> m_rows_min_height; ///< Hauteur minimale de chaque ligne.
    std::vector<int> m_cols_min_width;  ///< Largeur minimale de chaque colonne.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLAYOUTGRID_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGraph.hpp
 * \date 19/04/2013 Création de la classe CGraph.
 * \date 20/04/2013 Améliorations.
 */

#ifndef T_FILE_GUI_CGRAPH_HPP_
#define T_FILE_GUI_CGRAPH_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/IWidget.hpp"
#include "Core/CString.hpp"
#include "Core/Time.hpp"
#include "Graphic/CColor.hpp"
#include "Gui/CMargins.hpp"


namespace Ted
{

class ICurve;


/**
 * \class   CGraph
 * \ingroup Gui
 * \brief   Widget permettant d'afficher un graphique avec plusieurs courbes.
 ******************************/

class T_GUI_API CGraph : public IWidget
{
public:

	// Constructeurs et destructeur
	explicit CGraph(IWidget * parent = nullptr);
	explicit CGraph(const CString& title, IWidget * parent = nullptr);
	virtual ~CGraph();

    // Courbes
    void addCurve(ICurve * curve);
    void removeCurve(ICurve * curve);
    std::list<ICurve *> getCurves() const;
    void clearCurves();

    unsigned int getUpdateDelay() const;
    void setUpdateDelay(unsigned int delay);

    CString getTitle() const;
    void setTitle(const CString& title);

    // Légendes
    CString getHorizontalLegend() const;
	void setHorizontalLegend(const CString& legend);
	CString getVerticalLegend() const;
	void setVerticalLegend(const CString& legend);

    // Grille
	int getHorizontalGridSize() const;
	void setHorizontalGridSize(int size);
	int getVerticalGridSize() const;
	void setVerticalGridSize(int size);
	void setGridSize(int size);
	bool isHorizontalGridEnabled() const;
	void setHorizontalGridEnabled(bool enabled);
	bool isVerticalGridEnabled() const;
	void setVerticalGridEnabled(bool enabled);

	float getChartMinX();
	void setChartMinX(float minX);
	float getChartMaxX();
	void setChartMaxX(float maxX);
	float getChartMinY();
	void setChartMinY(float minY);
	float getChartMaxY();
	void setChartMaxY(float maxY);
	bool isAutoX() const;
	void setAutoX(bool autoX);
	bool isAutoY() const;
	void setAutoY(bool autoY);

	virtual void draw();

private:

    void computeSize();

    // Attributs privés
	std::list<ICurve *> m_curves;  ///< Liste des courbes du graphe.
	unsigned int m_updateDelay;    ///< Durée entre deux mises à jour du graphe (en millisecondes).
	TTime m_lastUpdateTime;        ///< Instant de la dernière mise à jour du graphe.

	// Légendes
	CString m_title;               ///< Titre du graphe. => CLabel ?
	CString m_horizontalLegend;    ///< Légende horizontale. => CLabel ?
	CString m_verticalLegend;      ///< Légende verticale. => CLabel ?

	// Grille
	uint32_t m_horizontalGridSize:15;   ///< Largeur de la grille.
	uint32_t m_horizontalGridEnabled:1; ///< Indique si la grille horizontale est affichée.
	uint32_t m_verticalGridSize:15;     ///< Hauteur de la grille.
	uint32_t m_verticalGridEnabled:1;   ///< Indique si la grille verticale est affichée.

	// Fenêtre affichée
	float m_chartMinX;
	float m_chartMaxX;
	float m_chartMinY;
	float m_chartMaxY;
	bool m_autoX;
	bool m_autoY;

	// Couleurs
	CColor m_backgroundColor;
	CColor m_gridColor;

	CMargins m_graphMargins;
};

} // Namespace Ted

#endif // T_FILE_GUI_CGRAPH_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDialog.hpp
 * \date       2008 Création de la classe GuiDialog.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CDialog.
 */

#ifndef T_FILE_GUI_CDIALOG_HPP_
#define T_FILE_GUI_CDIALOG_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>
#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/CWindow.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class CLabel;


/**
 * \class   CDialog
 * \ingroup Gui
 * \brief   Affichage d'une boite de dialogue.
 *
 * Une boite de dialogue est une fenêtre avec des paramètres spéciaux :
 * non redimensionnable, un texte (CLabel), et des boutons. Il est bien
 * entendu possible de traiter un objet CDialog comme un objet CWindow.
 *
 * \section Signaux
 * \li onButtonYes
 * \li onButtonNo
 * \li onButtonOK
 * \li onButtonCancel
 ******************************/

class T_GUI_API CDialog : public CWindow
{
public:

    /**
     * \enum    TDialogButton
     * \ingroup Gui
     * \brief   Flags pour définir les boutons des boites de dialogues.
     ******************************/

    enum TDialogButton
    {
        ButtonYes    = 0x01, ///< Bouton Oui.
        ButtonNo     = 0x02, ///< Bouton Non.
        ButtonOK     = 0x04, ///< Bouton OK.
        ButtonCancel = 0x08  ///< Bouton Annuler.
    };

    T_DECLARE_FLAGS(TDialogButtons, TDialogButton)


    // Constructeurs
    CDialog(const CString& title, const CString& text, TDialogButtons flags = 0, IWidget * parent = nullptr);
    explicit CDialog(IWidget * parent = nullptr);

    // Méthodes publiques
    CString getText() const;
    void setText(const CString& text);

    // Signaux
    sigc::signal<void> onButtonYes;    ///< Signal émis lorsque le bouton Oui est enfoncé.
    sigc::signal<void> onButtonNo;     ///< Signal émis lorsque le bouton Non est enfoncé.
    sigc::signal<void> onButtonOK;     ///< Signal émis lorsque le bouton OK est enfoncé.
    sigc::signal<void> onButtonCancel; ///< Signal émis lorsque le bouton Annuler est enfoncé.

private:

    // Donnée protégée
    CLabel * m_label;
    CString m_text; ///< Texte de la boite de dialogue.
};


T_DECLARE_OPERATORS_FOR_FLAGS(CDialog::TDialogButtons)

} // Namespace Ted

#endif // T_FILE_GUI_CDIALOG_HPP_

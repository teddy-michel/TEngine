/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CPushButton.hpp
 * \date       2008 Création de la classe GuiPushButton.
 * \date 24/07/2010 Redéfinition de la méthode setText et suppression de ComputeSize.
 * \date 12/05/2011 Les paramètres du texte sont conservés par la classe.
 * \date 29/05/2011 La classe est renommée en CPushButton.
 */

#ifndef T_FILE_GUI_CPUSHBUTTON_HPP_
#define T_FILE_GUI_CPUSHBUTTON_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IButton.hpp"
#include "Graphic/CFontManager.hpp"


namespace Ted
{

/**
 * \class   CPushButton
 * \ingroup Gui
 * \brief   Affichage d'un bouton enfonçable.
 ******************************/

class T_GUI_API CPushButton : public IButton
{
public:

    // Constructeur
    explicit CPushButton(const CString& text, IWidget * parent = nullptr);

    // Mutateur
    virtual void setText(const CString& text);

    // Méthodes publiques
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    virtual void setWidth(int width) override;
    virtual void setHeight(int height) override;

private:

    // Données protégées
    unsigned int m_time;      ///< Instant du dernier affichage.
    unsigned int m_timeFocus; ///< Durée en millisecondes pout passer d'une couleur à une autre.
    TTextParams m_textParams; ///< Paramètres du texte du bouton.
};

} // Namespace Ted

#endif // T_FILE_GUI_CPUSHBUTTON_HPP_

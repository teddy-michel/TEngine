/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLoaderAVI.hpp
 * \date 26/04/2009 Création de la classe CLoaderAVI.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 11/07/2010 Chargement d'une vidéo et création de la texture.
 * \date 15/07/2010 Ajout du flux audio et d'un paramètre indiquant si la lecture est en cours.
 * \date 04/12/2010 Ajout des méthodes Play et Stop.
 */

#ifndef T_FILE_GUI_CLOADERAVI_HPP_
#define T_FILE_GUI_CLOADERAVI_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "os.h"

#ifdef T_SYSTEM_WINDOWS
#  include <vfw.h>
#endif

#include "Gui/Export.hpp"
#include "Core/ILoader.hpp"


namespace Ted
{

class CMusic;


/**
 * \class   CLoaderAVI
 * \ingroup Gui
 * \brief   Chargement des vidéos AVI.
 ******************************/

class T_GUI_API CLoaderAVI : public ILoader
{
public:

    // Constructeur et destructeur
    CLoaderAVI();
    virtual ~CLoaderAVI();

    // Accesseurs
    bool isLoop() const;
    bool isPlaying() const;
    unsigned int getTextureId() const;
    unsigned int getFrameTime() const;
    unsigned int getTime() const;
    int getWidth() const;
    int getHeight() const;

    // Mutateurs
    void setLoop(bool loop = true);
    void setTime(unsigned int time);

    // Méthodes publiques
    bool loadFromFile(const CString& fileName);
    void update(unsigned int frameTime);
    void play();
    void stop();

private:

    // Données protégées
    bool m_loop;                ///< Indique si on doit jouer la vidéo en boucle (false par défaut).
    bool m_play;                ///< Indique si la vidéo est en cours de lecture.
    unsigned int m_texture;     ///< Identifiant de la texture.
    unsigned int m_frameTime;   ///< Durée d'une frame en millisecondes.
    unsigned int m_time;        ///< Durée déjà écoulée en millisecondes.
    int m_width;                ///< Largeur de l'image en pixels.
    int m_height;               ///< Hauteur de l'image en pixels.
    unsigned int m_frame;       ///< Numéro de la dernière frame affichée.
    unsigned int m_lastFrame;   ///< Numéro de la dernière frame de la vidéo.
#ifdef T_SYSTEM_WINDOWS
    PAVISTREAM m_video;         ///< Flux destiné à la lecture de la vidéo.
    PGETFRAME m_pgf;            ///< Frame buffer.
#endif
    CMusic * m_audio;           ///< Pointeur sur la ressource audio de la vidéo.
    std::vector<unsigned char> m_pixels; ///< Tableau des pixels de l'image.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLOADERAVI_HPP_

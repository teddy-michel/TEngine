/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CMargins.hpp
 * \date 11/04/2012 Création de la classe CMargins.
 */

#ifndef T_FILE_GUI_CMARGINS_HPP_
#define T_FILE_GUI_CMARGINS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Graphic/CFontManager.hpp"


namespace Ted
{

/**
 * \class   CMargins
 * \ingroup Gui
 * \brief   Cette classe permet de gérer les marges d'un rectangle.
 ******************************/

class T_GUI_API CMargins
{
public:

    // Constructeurs
    inline CMargins(int margin = 0);
    inline CMargins(int left, int top, int right, int bottom);

    // Accesseurs
    inline int getLeft() const;
    inline int getTop() const;
    inline int getRight() const;
    inline int getBottom() const;
    inline bool isNull() const;

    // Mutateurs
    inline void setLeft(int left);
    inline void setTop(int top);
    inline void setRight(int right);
    inline void setBottom(int bottom);

    // Opérateurs
    inline bool operator==(const CMargins& other);
    inline bool operator!=(const CMargins& other);

private:

    // Données protégées
    int m_left;
    int m_top;
    int m_right;
    int m_bottom;
};


/**
 * Construit un objet CMargins avec toutes les marges identiques.
 *
 * \param margin Valeur des marges.
 ******************************/

inline CMargins::CMargins(int margin) :
m_left   (0),
m_top    (0),
m_right  (0),
m_bottom (0)
{
    if (margin >= 0)
    {
        m_left = m_top = m_right = m_bottom = margin;
    }
}


/**
 * Construit un objet CMargins.
 *
 * \param left   Marge gauche.
 * \param top    Marge supérieure.
 * \param right  Marge droite.
 * \param bottom Marge inférieure.
 ******************************/

inline CMargins::CMargins(int left, int top, int right, int bottom) :
m_left   (left   < 0 ? 0 : left  ),
m_top    (top    < 0 ? 0 : top   ),
m_right  (right  < 0 ? 0 : right ),
m_bottom (bottom < 0 ? 0 : bottom)
{ }


/**
 * Donne la marge gauche
 *
 * \return Marge gauche.
 *
 * \sa CMargins::setLeft
 ******************************/

inline int CMargins::getLeft() const
{
    return m_left;
}


/**
 * Donne la marge supérieure.
 *
 * \return Marge supérieure.
 *
 * \sa CMargins::setTop
 ******************************/

inline int CMargins::getTop() const
{
    return m_top;
}


/**
 * Donne la marge droite
 *
 * \return Marge droite.
 *
 * \sa CMargins::Right
 ******************************/

inline int CMargins::getRight() const
{
    return m_right;
}


/**
 * Donne la marge inférieure.
 *
 * \return Marge inférieure.
 *
 * \sa CMargins::setBottom
 ******************************/

inline int CMargins::getBottom() const
{
    return m_bottom;
}


/**
 * Indique si toutes les marges sont nulles.
 *
 * \return Booléen.
 ******************************/

inline bool CMargins::isNull() const
{
    return (m_left == 0 && m_top == 0 && m_right == 0 && m_bottom == 0);
}


/**
 * Modifie la marge gauche.
 *
 * \param left Marge gauche.
 *
 * \sa CMargins::getLeft
 ******************************/

inline void CMargins::setLeft(int left)
{
    if (left < 0)
        m_left = 0;
    else
        m_left = left;
}


/**
 * Modifie la marge supérieure.
 *
 * \param top Marge supérieure.
 *
 * \sa CMargins::getTop
 ******************************/

inline void CMargins::setTop(int top)
{
    if (top < 0)
        m_top = 0;
    else
        m_top = top;
}


/**
 * Modifie la marge droite.
 *
 * \param right Marge droite.
 *
 * \sa CMargins::getRight
 ******************************/

inline void CMargins::setRight(int right)
{
    if (right < 0)
        m_right = 0;
    else
        m_right = right;
}


/**
 * Modifie la marge inférieure.
 *
 * \param bottom Marge inférieure.
 *
 * \sa CMargins::getBottom
 ******************************/

inline void CMargins::setBottom(int bottom)
{
    if (bottom < 0)
        m_bottom = 0;
    else
        m_bottom = bottom;
}


/**
 * Opérateur de comparaison ==.
 *
 * \param other Marge à comparer.
 * \return Booléen.
 ******************************/

inline bool CMargins::operator==(const CMargins& other)
{
    return (m_left   == other.m_left  &&
            m_top    == other.m_top   &&
            m_right  == other.m_right &&
            m_bottom == other.m_bottom);
}


/**
 * Opérateur de comparaison !=.
 *
 * \param other Marge à comparer.
 * \return Booléen.
 ******************************/

inline bool CMargins::operator!=(const CMargins& other)
{
    return (m_left   != other.m_left  ||
            m_top    != other.m_top   ||
            m_right  != other.m_right ||
            m_bottom != other.m_bottom);
}

} // Namespace Ted

#endif // T_FILE_GUI_CMARGINS_HPP_

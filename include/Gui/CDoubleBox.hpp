/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDoubleBox.hpp
 * \date 28/05/2009 Création de la classe GuiDoubleBox.
 * \date 14/08/2010 Précision du nombre de décimales à afficher.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en CDoubleBox.
 */

#ifndef T_FILE_GUI_CDOUBLEBOX_HPP_
#define T_FILE_GUI_CDOUBLEBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/ISpinBox.hpp"


namespace Ted
{

/**
 * \class   CDoubleBox
 * \ingroup Gui
 * \brief   Objet contenant un nombre décimal pouvant être incrémenté et décrémenté.
 *
 * \section Signaux
 * \li onChange
 ******************************/

class T_GUI_API CDoubleBox : public ISpinBox
{
public:

    // Constructeurs
    explicit CDoubleBox(double value, IWidget * parent = nullptr);
    explicit CDoubleBox(IWidget * parent = nullptr);

    // Accesseurs
    double getValue() const;
    double getMinimum() const;
    double getMaximum() const;
    double getStep() const;
    unsigned int getDecimals() const;

    // Mutateurs
    void setValue(double value);
    void setMinimum(double minimum);
    void setMaximum(double maximum);
    void setRange(double minimum, double maximum);
    void setStep(double step);
    void setDecimals(unsigned int decimals);

    // Méthodes publiques
    void stepUp();
    void stepDown();

    // Signaux
    sigc::signal<void, double> onChange; ///< Signal émis lorsque la valeur change.

private:

    // Méthode privée
    void updateValue();

    // Données protégées
    double m_value;          ///< Valeur de la doublebox.
    double m_min;            ///< Valeur minimale.
    double m_max;            ///< Valeur maximale.
    double m_step;           ///< Valeur à ajouter à chaque incrémentation.
    unsigned int m_decimals; ///< Nombre de décimales à afficher.
};

} // Namespace Ted

#endif // T_FILE_GUI_CDOUBLEBOX_HPP_

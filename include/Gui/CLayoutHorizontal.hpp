/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutHorizontal.hpp
 * \date 18/01/2010 Création de la classe GuiLayoutHorizontal.
 * \date 03/07/2010 Modification de la gestion des évènements.
 * \date 29/05/2011 La classe est renommée en CLayoutHorizontal.
 * \date 14/06/2014 Dérivation de la classe ILayoutBox.
 */

#ifndef T_FILE_GUI_CLAYOUTHORIZONTAL_HPP_
#define T_FILE_GUI_CLAYOUTHORIZONTAL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/ILayoutBox.hpp"


namespace Ted
{

/**
 * \class   CLayoutHorizontal
 * \ingroup Gui
 * \brief   Layout horizontal pour disposer des objets les uns à côté des autres.
 ******************************/

class T_GUI_API CLayoutHorizontal : public ILayoutBox
{
public:

    // Constructeur
    explicit CLayoutHorizontal(IWidget * parent = nullptr);

    // Méthode publique
    virtual void addChild(IWidget * widget, TAlignment align = AlignMiddleCenter, int pos = -1, int stretch = 0);

protected:

    // Méthode privée
    virtual void computeSize();
};

} // Namespace Ted

#endif // T_FILE_GUI_CLAYOUTHORIZONTAL_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CSlider.hpp
 * \date 12/12/2010 Création de la classe GuiSlider.
 * \date 06/03/2011 Création du signal onValueChange.
 * \date 06/05/2011 Implémentation en utilisant le code des scrollbars.
 * \date 28/05/2011 Amélioration du déplacement du curseur à la souris.
 * \date 29/05/2011 La classe est renommée en CSlider.
 */

#ifndef T_FILE_GUI_CSLIDER_HPP_
#define T_FILE_GUI_CSLIDER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"


namespace Ted
{

/**
 * \class   CSlider
 * \ingroup Gui
 * \brief   Un slider est une barre avec un curseur qu'il est possible de déplacer.
 *
 * \todo    Utiliser une classe de base commune entre CSlider et CScrollBar.
 *
 * \section Signaux
 * \li onValueChange
 ******************************/

class T_GUI_API CSlider : public IWidget
{
public:

    // Constructeur
    explicit CSlider(IWidget * parent = nullptr);

    // Accesseurs
    inline int getValue() const;
    inline int getTotal() const;
    inline int getStep() const;
    inline bool isVertical() const;
    inline bool isHorizontal() const;

    // Mutateurs
    void setValue(int value);
    void setTotal(int total);
    void setStep(int step );
    void setVertical(bool vertical = true);
    void setHorizontal(bool horizontal = true);

    // Méthodes publiques
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    void pageUp();
    void pageDown();

    // Signaux
    sigc::signal<void, unsigned int> onValueChange; ///< Signal émis lorsque la valeur change.

private:

    // Données privées
    int m_value;     ///< Valeur actuelle.
    int m_valueTmp;  ///< Valeur temporaire (pour le déplacement à la souris).
    int m_total;     ///< Valeur totale.
    int m_step;      ///< Nombre de lignes dans une page.
    int m_clic;      ///< Donne la position du clic sur la barre.

    // Donnée protégée
    bool m_vertical; ///< Indique si la barre est verticale (par défaut).
};


/**
 * Donne le numéro de la ligne affichée.
 *
 * \return Numéro de la ligne affichée.
 *
 * \sa CSlider::setValue
 ******************************/

inline int CSlider::getValue() const
{
    return m_value;
}


/**
 * Donne la valeur totale.
 *
 * \return Valeur totale.
 *
 * \sa CSlider::setTotal
 ******************************/

inline int CSlider::getTotal() const
{
    return m_total;
}


/**
 * Donne le nombre de lignes d'une page.
 *
 * \return Nombre de lignes.
 *
 * \sa CSlider::setStep
 ******************************/

inline int CSlider::getStep() const
{
    return m_step;
}


/**
 * Indique si la barre est verticale.
 *
 * \return Booléen.
 *
 * \sa CSlider::isHorizontal
 * \sa CSlider::setVertical
 ******************************/

inline bool CSlider::isVertical() const
{
    return m_vertical;
}


/**
 * Indique si la barre est horizontale.
 *
 * \return Booléen.
 *
 * \sa CSlider::isVertical
 * \sa CSlider::setHorizontal
 ******************************/

inline bool CSlider::isHorizontal() const
{
    return !m_vertical;
}

} // Namespace Ted

#endif // T_FILE_GUI_CSLIDER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutTab.hpp
 * \date 27/05/2009 Création de la classe GuiTab.
 * \date 16/07/2010 Utilisation possible des signaux, création du signal onTabChange.
 * \date 29/05/2011 La classe est renommée en CTab.
 * \date 04/04/2012 La classe est renommée en CLayoutTab.
 * \date 04/04/2012 On peut modifier la position de la barre d'onglets.
 */

#ifndef T_FILE_GUI_CLAYOUTTAB_HPP_
#define T_FILE_GUI_CLAYOUTTAB_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/ILayout.hpp"


namespace Ted
{

/**
 * \class   CLayoutTab
 * \ingroup Gui
 * \brief   Gestion d'une boite d'onglets.
 *
 * Chaque onglet se comporte comme un layout pouvant contenir un seul objet.
 *
 * \todo Pouvoir modifier la position de la barre d'onglets.
 *
 * \section Signaux
 * \li onTabChange
 ******************************/

class T_GUI_API CLayoutTab : public ILayout
{
public:

    /**
     * \enum    TTabPosition
     * \ingroup Gui
     * \brief   Position de la barre d'onglets.
     ******************************/

    enum TTabPosition
    {
        Top,      ///< Au-dessus (par défaut).
        Bottom,   ///< En-dessous.
        Left,     ///< À gauche.
        Right     ///< À droite.
    };


    // Constructeur et destructeur
    explicit CLayoutTab(IWidget * parent = nullptr);
    virtual ~CLayoutTab();

    // Accesseurs
    int getOpenTab() const;
    TTabPosition getTabPosition() const;
    int getNumTabs() const;
    IWidget * getTab(int pos) const;

    // Mutateurs
    void setOpenTab(int pos);
    void setTabPosition(TTabPosition position);
    void setWidth(int width);
    void setHeight(int height);

    // Méthodes publiques
    void addTab(const CString& name, IWidget * tab, int pos = -1, TAlignment align = AlignMiddleCenter);
    void addTab(const CString& name, IWidget * tab, TAlignment align);
    void removeTab(IWidget * tab);
    void removeTab(int pos);
    void clearTabs();
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);

    // Signaux
    sigc::signal<void> onTabChange; ///< Signal émis lorsque l'onglet actif change.

protected:

    // Méthode privée
    virtual void computeSize();

private:

    /**
     * \struct  TabItem
     * \ingroup Gui
     * \brief   Représente un onglet, son nom et l'objet qu'il contient.
     ******************************/

    struct TabItem
    {
        CString name;     ///< Nom de l'onglet.
        IWidget * tab;    ///< Pointeur sur l'objet enfant.
        TAlignment align; ///< Alignement de l'objet.
    };

    // Données protégées
    int m_open;                ///< Numéro de l'onglet ouvert.
    TTabPosition m_position;   ///< Position de la barre d'onglet.
    std::list<TabItem> m_tabs; ///< Liste des onglets.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLAYOUTTAB_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWidgetDebugTextures.hpp
 * \date 13/06/2014 Création de la classe CWidgetDebugTextures.
 */

#ifndef T_FILE_GUI_CWIDGETDEBUGTEXTURES_HPP_
#define T_FILE_GUI_CWIDGETDEBUGTEXTURES_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IWidget.hpp"


namespace Ted
{

class CGraph;


/**
 * \class   CWidgetDebugTextures
 * \ingroup Gui
 * \brief   Widget qui affiche la liste des textures chargées en mémoire.
 ******************************/

class T_GUI_API CWidgetDebugTextures : public IWidget
{
public:

    // Constructeurs
    explicit CWidgetDebugTextures(IWidget * parent = nullptr);

    // Méthode publique
    virtual void draw();
};

} // Namespace Ted

#endif // T_FILE_GUI_CWIDGETDEBUGTEXTURES_HPP_

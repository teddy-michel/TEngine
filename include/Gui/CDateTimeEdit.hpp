/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDateTimeEdit.hpp
 * \date 14/03/2010 Création de la classe GuiDateTimeEdit.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en CDateTimeEdit.
 */

#ifndef T_FILE_GUI_CDATETIMEEDIT_HPP_
#define T_FILE_GUI_CDATETIMEEDIT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/ISpinBox.hpp"
#include "Core/CDateTime.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CDateTimeEdit
 * \ingroup Gui
 * \brief   Widget permettant d'éditer des dates et des heures.
 *
 * \section Signaux
 * \li onChange
 ******************************/

class T_GUI_API CDateTimeEdit : public ISpinBox
{
public:

    enum TFieldType
    {
        Second,
        Minute,
        Hour,
        Day,
        Week,
        Month,
        Year
    };

    // Constructeurs
    explicit CDateTimeEdit(IWidget * parent = nullptr);
    CDateTimeEdit(const CDateTime& datetime, IWidget * parent = nullptr);

    // Accesseurs
    CDateTime getDateTime() const;
    CDateTime getMinDateTime() const;
    CDateTime getMaxDateTime() const;
    CString getFormat() const;

    // Mutateurs
    void setDateTime(const CDateTime& datetime);
    void setMinDateTime(const CDateTime& datetime);
    void setMaxDateTime(const CDateTime& datetime);
    void setFormat(const CString& format);

    // Méthodes publiques
    virtual void stepUp();
    virtual void stepDown();

    // Signaux
    sigc::signal<void, CDateTime> onChange; ///< Signal émis lorsque la valeur change.


private:

    // Méthode privée
    virtual void updateValue();

private:

    // Données protégées
    CDateTime m_datetime; ///< Date et heure.
    CDateTime m_min;      ///< Date et heure minimale.
    CDateTime m_max;      ///< Date et heure maximale.
    CString m_format;     ///< Format d'affichage de la date.
    TFieldType m_select;  ///< Numéro de l'élément sélectionné.
};

} // Namespace Ted

#endif // T_FILE_GUI_CDATETIMEEDIT_HPP_

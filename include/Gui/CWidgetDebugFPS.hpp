/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWidgetDebugFPS.hpp
 * \date 01/04/2013 Création de la classe CWidgetDebugFPS.
 * \date 19/04/2013 Utilisation de la classe CGraph.
 */

#ifndef T_FILE_GUI_CWIDGETDEBUGFPS_HPP_
#define T_FILE_GUI_CWIDGETDEBUGFPS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Core/CCircularBuffer.hpp"


namespace Ted
{

class CGraph;


/**
 * \class   CWidgetDebugFPS
 * \ingroup Gui
 * \brief   Widget qui affiche l'historique de FPS.
 ******************************/

class T_GUI_API CWidgetDebugFPS : public IWidget
{
public:

    // Constructeurs
    explicit CWidgetDebugFPS(IWidget * parent = nullptr);

    // Méthode publique
    virtual void draw();

private:

    CGraph * m_graphFPS;
    CCircularBuffer<float> m_bufferFPS;
};

} // Namespace Ted

#endif // T_FILE_GUI_CWIDGETDEBUGFPS_HPP_

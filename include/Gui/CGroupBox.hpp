/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGroupBox.hpp
 * \date 19/11/2010 Création de la classe GuiGroupBox.
 * \date 19/01/2011 Surcharge des méthodes setWidth, setHeight, et onEvent.
 * \date 29/05/2011 La classe est renommée en CGroupBox.
 */

#ifndef T_FILE_GUI_CGROUPBOX_HPP_
#define T_FILE_GUI_CGROUPBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CGroupBox
 * \ingroup Gui
 * \brief   Ce widget permet d'afficher un cadre qui contient un autre widget,
 *          et d'afficher un titre au-dessus de cette zone.
 ******************************/

class T_GUI_API CGroupBox : public IWidget
{
public:

    // Constructeurs et destructeur
    explicit CGroupBox(const CString& title = CString(), IWidget * parent = nullptr);
    explicit CGroupBox(IWidget * parent);
    virtual ~CGroupBox();

    // Accesseurs
    CString getTitle() const;
    THorizontalAlignment getAlignment() const;
    bool isCheckable() const;
    IWidget * getChild() const;

    // Mutateurs
    virtual void setWidth(int width);
    virtual void setHeight(int height);
    void setTitle(const CString& title);
    void setAlignment(THorizontalAlignment align);
    void setCheckable(bool checkable = true);
    void setChild(IWidget * child);

    // Méthodes publiques
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);

private:

    // Données protégées
    CString m_title;              ///< Titre du groupe.
    THorizontalAlignment m_align; ///< Alignement du titre (à gauche par défaut).
    bool m_checkable;             ///< Indique si le groupe possède une case à cocher.
    bool m_checked;               ///< Indique si le groupe est actif ou pas.
    IWidget * m_child;            ///< Widget à l'intérieur du groupe.
};

} // Namespace Ted

#endif // T_FILE_GUI_CGROUPBOX_HPP_

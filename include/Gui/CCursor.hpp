/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CCursor.hpp
 * \date 09/12/2011 Création de la classe CCursor.
 */

#ifndef T_FILE_GUI_CCURSOR_HPP_
#define T_FILE_GUI_CCURSOR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Graphic/CTextureManager.hpp"


namespace Ted
{

/**
 * \class   CCursor
 * \ingroup Gui
 * \brief   Gestion d'un curseur de la souris.
 ******************************/

class T_GUI_API CCursor
{
public:

    static const unsigned int CursorSize = 24;

    // Constructeur et destructeur
    CCursor(const char pixels[CursorSize*CursorSize] = nullptr);
    CCursor(const char pixels[CursorSize*CursorSize], const unsigned int px, const unsigned int py);
    ~CCursor();

    // Accesseurs
    char const * getPixels() const;
    unsigned int getPointerX() const;
    unsigned int getPointerY() const;
    TTextureId getTextureId();

    // Mutateurs
    void setPointerX(unsigned int px);
    void setPointerY(unsigned int py);

private:

    // Méthode privée
    void loadTexture();

    /**
     * \enum    TCursorPixels
     * \ingroup Gui
     * \brief   Liste des couleurs disponibles pour les curseurs.
     ******************************/

#if __cplusplus < 201103L
    enum TCursorPixels
#else
    enum TCursorPixels : uint8_t
#endif
    {
        PixelBlack       = 'X', ///< Pixel de couleur noire.
        PixelWhite       = '.', ///< Pixel de couleur blanche.
        PixelTransparent = ' ', ///< Pixel transparent.
        PixelInverted    = '0'  ///< Couleur inversée (ou noir si indisponible).
    };

protected:

    // Donnée protégée
    char m_pixels[CursorSize*CursorSize]; ///< Pixels composant le curseur.
    unsigned int m_px;    ///< Coordonnée horizontale du pointeur.
    unsigned int m_py;    ///< Coordonnée verticale du pointeur.
    TTextureId m_texture; ///< Identifiant de la texture du curseur.
};

} // Namespace Ted

#endif // T_FILE_GUI_CCURSOR_HPP_

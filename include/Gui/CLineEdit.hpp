/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLineEdit.hpp
 * \date 26/04/2009 Création de la classe GuiLineEdit.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 30/10/2010 On peut annuler la dernière modification.
 * \date 04/11/2010 Héritage de la classe GuiBaseTextEdit.
 * \date 17/01/2011 Héritage de la classe GuiWidget (car GuiBaseTextEdit n'en hérite plus).
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 29/05/2011 La classe est renommée en CLineEdit.
 */

#ifndef T_FILE_GUI_CLINEEDIT_HPP_
#define T_FILE_GUI_CLINEEDIT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Gui/ITextEdit.hpp"


namespace Ted
{

/**
 * \class   CLineEdit
 * \ingroup Gui
 * \brief   Champ de texte à une seule ligne.
 ******************************/

class T_GUI_API CLineEdit : public IWidget, public ITextEdit
{
public:

    // Constructeurs
    explicit CLineEdit(const CString& text = CString(), IWidget * parent = nullptr);
    explicit CLineEdit(IWidget * parent);

    // Accesseur
    bool isPassword() const;

    // Mutateur
    void setPassword(bool password = true);

    // Méthodes publiques
    virtual void draw();
    virtual TCursor getCursorType(int x, int y) const;
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CMouseEvent& event);

private:

    // Données protégées
    bool m_password; ///< Indique si le texte doit être masqué à l'affichage (mot de passe).
    int m_left;      ///< Nombre de pixels de décalage à gauche.
};

} // Namespace Ted

#endif // T_FILE_GUI_CLINEEDIT_HPP_

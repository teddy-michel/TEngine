/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CCurveCircularBuffer.hpp
 * \date 19/04/2013 Création de la classe CCurveCircularBuffer.
 */

#ifndef T_FILE_GUI_CCURVECIRCULARBUFFER_HPP_
#define T_FILE_GUI_CCURVECIRCULARBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IDiscreteCurve.hpp"
#include "Core/CCircularBuffer.hpp"


namespace Ted
{

/**
 * \class   CCurveCircularBuffer
 * \ingroup Gui
 * \brief   Courbe prenant ses valeurs dans un buffer circulaire.
 ******************************/

class T_GUI_API CCurveCircularBuffer : public IDiscreteCurve
{
public:

    // Constructeur et destructeur
	explicit CCurveCircularBuffer(CCircularBuffer<float> * buffer, IWidget * parent = nullptr);
	~CCurveCircularBuffer();

	CCircularBuffer<float> * getBuffer() const;
	void setBuffer(CCircularBuffer<float> * buffer);

    virtual float getCurveYFromX(int x) const;
	virtual float getCurveMinY(int minX, int maxX) const;
	virtual float getCurveMaxY(int minX, int maxX) const;
	virtual float getCurveMinX() const;
	virtual float getCurveMaxX() const;

private:

	CCircularBuffer<float> * m_data;
};

} // Namespace Ted

#endif // T_FILE_GUI_CCURVECIRCULARBUFFER_HPP_

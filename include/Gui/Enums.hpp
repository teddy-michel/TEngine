/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/Enums.hpp
 * \date 06/03/2011 Création du fichier à partir des énumérations définies dans GuiEngine.hpp et GuiWidget.hpp.
 */

#ifndef T_FILE_GUI_ENUMS_HPP_
#define T_FILE_GUI_ENUMS_HPP_


namespace Ted
{

/**
 * \enum    THorizontalAlignment
 * \ingroup Gui
 * \brief   Alignements possibles.
 ******************************/

enum THorizontalAlignment
{
    AlignLeft,    ///< Aligné à gauche.
    AlignCenter,  ///< Aligné au centre.
    AlignRight    ///< Aligné à droite.
};


/**
 * \enum    TAlignment
 * \ingroup Gui
 * \brief   Alignement des widgets dans les layouts.
 ******************************/

enum TAlignment
{
    AlignTopLeft,      ///< En haut à gauche.
    AlignTopCenter,    ///< En haut au centre.
    AlignTopRight,     ///< En haut à droite.
    AlignMiddleLeft,   ///< Au milieu à gauche.
    AlignMiddleCenter, ///< Au milieu au centre.
    AlignMiddleRight,  ///< Au milieu à droite.
    AlignBottomLeft,   ///< En bas à gauche.
    AlignBottomCenter, ///< En bas au centre.
    AlignBottomRight   ///< En bas à droite.
};


/**
 * \enum    TCursor
 * \ingroup Gui
 * \brief   Liste des curseurs de souris.
 ******************************/

enum TCursor
{
    CursorArrow,      ///< Curseur en forme de flèche (par défaut).
    CursorHand,       ///< Curseur en forme de main.
    CursorText,       ///< Curseur d'édition de texte.
    CursorResizeNESW, ///< Curseur de redimensionnement pointant Nord-Est - Sud-Ouest.
    CursorResizeNS,   ///< Curseur de redimensionnement pointant Nord - Sud.
    CursorResizeNWSE, ///< Curseur de redimensionnement pointant Nord-Ouest - Sud-Est.
    CursorResizeWE,   ///< Curseur de redimensionnement pointant Ouest - Est.
    CursorEmpty       ///< Curseur invisible. (Sa valeur donne le nombre de curseurs disponibles.)
};

} // Namespace Ted

#endif // T_FILE_GUI_ENUMS_HPP_

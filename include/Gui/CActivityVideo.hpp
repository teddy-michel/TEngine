/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CActivityVideo.hpp
 * \date 02/06/2014 Création de la classe CActivityVideo.
 */

#ifndef T_FILE_GUI_CACTIVITYVIDEO_HPP_
#define T_FILE_GUI_CACTIVITYVIDEO_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CActivity.hpp"
#include "Core/CFlags.hpp"


namespace Ted
{

class CLoaderAVI;


/**
 * Activité pour lire une vidéo automatiquement, le joueur pouvant éventuellement l'interrompre.
 * Une fois que la lecture est terminée, une autre activité est démarrée.
 */

class T_GUI_API CActivityVideo : public CActivity
{
public:

    /// Comportement de la vidéo.
    enum TPlayMode
    {
        AlwaysPlay,        ///< Joue la vidéo à chaque fois sans que le joueur puisse l'interrompre.
        PlayOnlyFirstTime, ///< Joue la vidéo uniquement la première fois.
        PlayAtLeastOnce,   ///< Joue la vidéo à chaque fois, le joueur pouvant l'interrompre sauf la première fois.
        PlayEachTime       ///< Joue la vidéo à chaque fois, le joueur pouvant l'interrompre.
    };

    /// Liste des actions permettant l'interruption de la vidéo.
    enum TInterruptAction
    {
        NoActions    = 0x00,
        KeySpace     = 0x01,
        KeyEnter     = 0x02,
        KeyEscape    = 0x04,
        MouseButtons = 0x08,
        AllKeys      = 0x10
    };

    T_DECLARE_FLAGS(TInterruptActions, TInterruptAction)


    CActivityVideo(const CString& activityName);
    ~CActivityVideo();

    void setVideoFileName(const CString& fileName);
    CString getVideoFileName() const;

    void setPlayMode(TPlayMode playMode);
    TPlayMode getPlayMode() const;

    void setInterruptActions(TInterruptActions actions);
    TInterruptActions getInterruptActions() const;

    void setNextActivityName(const CString& activityName);
    CString getNextActivityName() const;

    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);

    void start();
    void stop();

protected:

    void update();

private:

    void nextActivity();


    CLoaderAVI * m_video;
    CString m_videoFileName;
    TPlayMode m_playMode;
    TInterruptActions m_interruptActions;
    CString m_nextActivity;
    unsigned int m_timeLast;
};

T_DECLARE_OPERATORS_FOR_FLAGS(CActivityVideo::TInterruptActions)

} // Namespace Ted

#endif // T_FILE_GUI_CACTIVITYVIDEO_HPP_

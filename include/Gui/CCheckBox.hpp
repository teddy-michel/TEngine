/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CCheckBox.hpp
 * \date       2008 Création de la classe GuiCheckBox.
 * \date 29/05/2011 La classe est renommée en CCheckBox.
 */

#ifndef T_FILE_GUI_CCHECKBOX_HPP_
#define T_FILE_GUI_CCHECKBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IButton.hpp"


namespace Ted
{

class CLabel;


/**
 * \class   CCheckBox
 * \ingroup Gui
 * \brief   Affichage d'une case à cocher.
 ******************************/

class T_GUI_API CCheckBox : public IButton
{
public:

    // Constructeurs et destructeur
    explicit CCheckBox(const CString& text = CString(), IWidget * parent = nullptr);
    explicit CCheckBox(IWidget * parent);
    virtual ~CCheckBox();

    // Méthodes publiques
    CString getText() const;
    void setText(const CString& text);
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);

private:

    // Donnée protégée
    CLabel * m_label; ///< Pointeur sur le label.
};

} // Namespace Ted

#endif // T_FILE_GUI_CCHECKBOX_HPP_

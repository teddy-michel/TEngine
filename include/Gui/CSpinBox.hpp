/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CSpinBox.hpp
 * \date 28/05/2009 Création de la classe GuiSpinBox.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en CSpinBox.
 */

#ifndef T_FILE_GUI_CSPINBOX_HPP_
#define T_FILE_GUI_CSPINBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/ISpinBox.hpp"


namespace Ted
{

/**
 * \class   CSpinBox
 * \ingroup Gui
 * \brief   Objet contenant un nombre entier pouvant être incrémenté et décrémenté.
 *
 * \section Signaux
 * \li onChange
 ******************************/

class T_GUI_API CSpinBox : public ISpinBox
{
public:

    // Constructeurs
    explicit CSpinBox(int value = 0, IWidget * parent = nullptr);
    explicit CSpinBox(IWidget * parent);

    // Accesseurs
    int getValue() const;
    int getMinimum() const;
    int getMaximum() const;
    int getStep() const;

    // Mutateurs
    void setValue(int value);
    void setMinimum(int minimum);
    void setMaximum(int maximum);
    void setRange(int minimum, int maximum);
    void setStep(int step);

    // Méthodes publiques
    void stepUp();
    void stepDown();

    // Signaux
    sigc::signal<void, int> onChange; ///< Signal émis lorsque la valeur change.

private:

    // Méthode privée
    void updateValue();

protected:

    // Donnée protégée
    int m_value; ///< Valeur de la spinbox.
    int m_min;   ///< Valeur minimale.
    int m_max;   ///< Valeur maximale.
    int m_step;  ///< Valeur à ajouter à chaque incrémentation.
};

} // Namespace Ted

#endif // T_FILE_GUI_CSPINBOX_HPP_

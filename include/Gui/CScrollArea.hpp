/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CScrollArea.hpp
 * \date 27/06/2014 Création de la classe CScrollArea.
 */

#ifndef T_FILE_GUI_CSCROLLAREA_HPP_
#define T_FILE_GUI_CSCROLLAREA_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IScrollArea.hpp"


namespace Ted
{

/**
 * \class   CScrollArea
 * \ingroup Gui
 * \brief   Conteneur affichant une partie d'un widget.
 ******************************/

class T_GUI_API CScrollArea : public IScrollArea
{
public:

    // Constructeur
    explicit CScrollArea(IWidget * parent);

    // Widget interne
    void setWidget(IWidget * widget);
    IWidget * getWidget() const;

    // Méthodes publiques
    virtual void draw();
    virtual TCursor getCursorType(int x, int y) const;
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CMouseEvent& event);

protected:

    IWidget * m_widget; ///< Widget interne.
};

} // Namespace Ted

#endif // T_FILE_GUI_CSCROLLAREA_HPP_

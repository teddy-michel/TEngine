/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindowDebug.hpp
 * \date 01/04/2013 Création de la classe CWindowDebug.
 * \date 13/06/2014 Ajout du widget pour afficher les textures.
 */

#ifndef T_FILE_GUI_CWINDOWDEBUG_HPP_
#define T_FILE_GUI_CWINDOWDEBUG_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/CWindow.hpp"


namespace Ted
{

//class CWidgetDebugMemory;
class CWidgetDebugTextures;
class CWidgetDebugFPS;
class CLayoutHorizontal;


/**
 * \class   CWindowDebug
 * \ingroup Gui
 * \brief   Classe permettant de gérer la fenêtre de debug.
 ******************************/

class T_GUI_API CWindowDebug : public CWindow
{
public:

    // Constructeur et destructeur
    explicit CWindowDebug(IWidget * parent = nullptr);
    ~CWindowDebug();

    virtual void onEvent(const CResizeEvent& event);

private:

    //void displayMemory();
    void displayTextures();
    void displayFPS();

    //CWidgetDebugMemory * m_widgetMemory;
    CWidgetDebugTextures * m_widgetTextures;
    CWidgetDebugFPS * m_widgetFPS;
    CLayoutHorizontal * m_layoutH;
};

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOWDEBUG_HPP_

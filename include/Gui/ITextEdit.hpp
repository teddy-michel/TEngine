/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ITextEdit.hpp
 * \date 04/11/2010 Création de la classe GuiBaseTextEdit.
 * \date 13/11/2010 On peut préciser la taille du texte.
 * \date 17/01/2011 Cette classe n'hérite plus de GuiWidget.
 * \date 19/01/2011 Utilisation d'une structure TTextParams pour stocker les paramètres du texte.
 * \date 29/05/2011 La classe est renommée en ITextEdit.
 */

#ifndef T_FILE_GUI_ITEXTEDIT_HPP_
#define T_FILE_GUI_ITEXTEDIT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Graphic/CFontManager.hpp"


namespace Ted
{

/**
 * \class   ITextEdit
 * \ingroup Gui
 * \brief   Classe de base des champs de texte.
 *
 * Cette classe n'hérite pas de IWidget, si vous souhaitez créer un nouveau
 * type de champ texte, il faudra qu'il hérite à la fois de IWidget (ou de
 * n'importe quelle classe dérivée) et de ITextEdit.
 *
 * \todo Différencier le texte simple du texte mis en forme.
 *
 * \section Signaux
 * \li onTextChange
 * \li onClear
 * \li onEnter
 * \li onCursorMove
 * \li onClick
 ******************************/

class T_GUI_API ITextEdit
{
public:

    // Constructeur
    explicit ITextEdit(const CString& text = CString());

    // Accesseurs
    CString getText() const;
    CString getOldText() const;
    unsigned int getTextLength() const;
    bool isEditable() const;
    unsigned int getMaxLength() const;
    unsigned int getCursor() const;
    int getSelect() const;
    unsigned int getTextSize() const;

    // Mutateurs
    void setText(const CString& text);
    void setEditable(bool editable = true);
    void setMaxLength(unsigned int maxlength);
    void setCursor(unsigned int cursor);
    void setSelect(int select);
    void setTextSize(unsigned int size);

    // Méthodes publiques
    void undoLastModification();
    void onTextEvent(const CTextEvent& event);
    void onKeyboardEvent(const CKeyboardEvent& event);
    void clear();
    void selectAll();
    void copySelectedText() const;
    void removeSelectedText();
    void paste();

    // Signaux
    sigc::signal<void> onTextChange;               ///< Signal émis lorsque le texte est modifié.
    sigc::signal<void> onClear;                    ///< Signal émis lorsque le texte est effacé.
    sigc::signal<void> onEnter;                    ///< Signal émis lorsque la touche Enter est enfoncée.
    sigc::signal<void> onClick;                    ///< Signal émis lorsqu'on clique sur le widget.
    sigc::signal<void, unsigned int> onCursorMove; ///< Signal émis lorsque le curseur se déplace.

    // Donnée statique publique
    static const unsigned int timeBlink = 500; ///< Durée du clignotement en millisecondes.

private:

    // Données privées
    CString m_oldText;        ///< Ancien texte (avant la dernière modification).
    bool m_editable;          ///< Indique si le texte est modifiable.
    unsigned int m_maxlength; ///< Longueur maximale du texte.

protected:

    // Données protégées
    unsigned int m_cursor;    ///< Position du curseur.
    int m_select;             ///< Nombre de caractères sélectionnés.
    unsigned int m_time;      ///< Instant du dernier affichage.
    unsigned int m_timeBlink; ///< Compteur de temps utilisé pour le clignotement du curseur.
    TTextParams m_params;     ///< Paramètres du texte à afficher.
};

} // Namespace Ted

#endif // T_FILE_GUI_ITEXTEDIT_HPP_

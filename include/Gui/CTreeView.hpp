/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CTreeView.hpp
 * \date 26/02/2010 Création de la classe GuiTreeView.
 * \date 18/01/2011 Héritage de GuiFrame.
 * \date 04/03/2011 Création des signaux onExpand et onSelect.
 * \date 29/05/2011 La classe est renommée en CTreeView.
 */

#ifndef T_FILE_GUI_CTREEVIEW_HPP_
#define T_FILE_GUI_CTREEVIEW_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IScrollArea.hpp"


namespace Ted
{

class CTreeNode;

/**
 * \class   CTreeView
 * \ingroup Gui
 * \brief   Ce widget permet d'afficher des données d'un arbre.
 *
 * \section Signaux
 * \li onExpand
 * \li onSelect
 ******************************/

class T_GUI_API CTreeView : public IScrollArea
{
public:

    // Constructeur
    explicit CTreeView(IWidget * parent = nullptr);

    // Accesseurs
    CTreeNode * getHeadNode() const;
    CTreeNode * getSelectedNode() const;

    // Mutateur
    void setHeadNode(CTreeNode * headnode);

    // Méthode publique
    void draw();

    // Signaux
    sigc::signal<void> onExpand; ///< Signal émis lorsque un nœud est ouvert.
    sigc::signal<void> onSelect; ///< Signal émis lorsque un item est sélectionné.

protected:

    // Données protégées
    CTreeNode * m_headnode; ///< Pointeur sur la racine de l'arbre.
    CTreeNode * m_selected; ///< Pointeur sur le nœud de l'arbre sélectionné.
};

} // Namespace Ted

#endif // T_FILE_GUI_CTREEVIEW_HPP_

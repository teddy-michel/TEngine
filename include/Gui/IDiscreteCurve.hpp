/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IDiscreteCurve.hpp
 * \date 19/04/2013 Création de la classe IDiscreteCurve.
 */

#ifndef T_FILE_GUI_IDISCRETECURVE_HPP_
#define T_FILE_GUI_IDISCRETECURVE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/ICurve.hpp"


namespace Ted
{

/**
 * \class   IDiscreteCurve
 * \ingroup Gui
 * \brief   Courbe
 ******************************/

class T_GUI_API IDiscreteCurve : public ICurve
{
public:

    // Constructeur et destructeur
    explicit IDiscreteCurve(IWidget * parent = nullptr);
    virtual ~IDiscreteCurve();

    virtual float getCurveYFromX(int x) const = 0;
    virtual float getCurveYFromX(float x) const;
    virtual float getCurveMinY(int minX, int maxX) const = 0;
    virtual float getCurveMinY(float minX, float maxX) const;
    virtual float getCurveMaxY(int minX, int maxX) const = 0;
    virtual float getCurveMaxY(float minX, float maxX) const;
};

} // Namespace Ted

#endif // T_FILE_GUI_IDISCRETECURVE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IScrollArea.hpp
 * \date 17/01/2011 Création de la classe GuiFrame.
 * \date 20/01/2011 Création des méthodes getWidgetSize et ComputeScrollSize.
 * \date 14/04/2011 La classe est renommée en GuiBaseScrollArea.
 * \date 29/05/2011 La classe est renommée en IScrollArea.
 */

#ifndef T_FILE_GUI_ISCROLLAREA_HPP_
#define T_FILE_GUI_ISCROLLAREA_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"
#include "Core/Maths/CVector2.hpp"


namespace Ted
{

class CScrollBar;


/**
 * \class   IScrollArea
 * \ingroup Gui
 * \brief   Widget qui affiche une barre de navigation verticale et une barre
 *          de navigation horizontale s'il ne peut pas être affiché complètement.
 *
 * Il est déconseillé d'utiliser cette classe directement pour le moment.
 ******************************/

class T_GUI_API IScrollArea : public IWidget
{
public:

    // Constructeur et destructeur
    explicit IScrollArea(IWidget * parent = nullptr);
    virtual ~IScrollArea();

    // Accesseurs
    int getFrameWidth() const;
    int getFrameHeight() const;
    CScrollBar * getVerticalScrollBar() const;
    CScrollBar * getHorizontalScrollBar() const;
    bool isShowVerticalScrollBar() const;
    bool isShowHorizontalScrollBar() const;

    // Mutateurs
    virtual void setWidth(int width);
    virtual void setHeight(int height);
    void setFrameWidth(int frameWidth);
    void setFrameHeight(int frameHeight);
    void setFrameSize(int frameWidth, int frameHeight);
    void setShowVerticalScrollBar(bool show = true);
    void setShowHorizontalScrollBar(bool show = true);

    // Méthodes publiques
    bool shouldShowVerticalScrollBar() const;
    bool shouldShowHorizontalScrollBar() const;

private:

    // Données privées
    bool m_showVScroll; ///< Indique si la barre verticale doit toujours être affichée.
    bool m_showHScroll; ///< Indique si la barre horizontale doit toujours être affichée.

protected:

    // Méthodes protégées
    void computeScrollSize();
    TVector2I getWidgetSize() const;

    // Données protégées
    CScrollBar * m_vScroll; ///< Pointeur sur la barre verticale.
    CScrollBar * m_hScroll; ///< Pointeur sur la barre horizontale.
};

} // Namespace Ted

#endif // T_FILE_GUI_ISCROLLAREA_HPP_

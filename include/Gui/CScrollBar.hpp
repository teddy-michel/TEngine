/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CScrollBar.hpp
 * \date 14/03/2010 Création de la classe GuiScrollBar.
 * \date 07/11/2010 Ajout des boutons pour monter et descendre l'ascenseur, des
 *                  inputs, et de la gestion des évènements de la souris.
 *                  Ajout du paramètre single_step.
 * \date 06/03/2011 Création du signal onValueChange.
 * \date 30/04/2011 Amélioration du déplacement avec la souris.
 * \date 29/05/2011 La classe est renommée en CScrollBar.
 *                  La barre se déplace lorsqu'un bouton reste enfoncé.
 */

#ifndef T_FILE_GUI_CSCROLLBAR_HPP_
#define T_FILE_GUI_CSCROLLBAR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sigc++/sigc++.h>

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"


namespace Ted
{

class CPushButton;


/**
 * \class   CScrollBar
 * \ingroup Gui
 * \brief   Une scroll bar représente un ascenseur, qui peut être horizontal ou vertical.
 *
 * \todo    Utiliser une classe de base commune entre CSlider et CScrollBar.
 *
 * Une scroll bar permet d'accéder à un document qui est plus grand que le
 * widget qui l'affiche. Il indique à l'utilisateur la position actuelle dans le
 * document, et la partie du document actuellement affichée.
 *
 * Une valeur indique la position actuelle (le numéro de la première ligne
 * affichée). Une autre valeur indique le nombre de lignes d'une page. Une
 * dernière valeur indique le nombre total de ligne.
 *
 * \section Signaux
 * \li onValueChange
 ******************************/

class T_GUI_API CScrollBar : public IWidget
{
public:

    // Constructeur et destructeur
    explicit CScrollBar(IWidget * parent = nullptr);
    virtual ~CScrollBar();

    // Accesseurs
    int getValue() const;
    int getTotal() const;
    int getSingleStep() const;
    int getStep() const;
    bool isVertical() const;
    bool isHorizontal() const;

    // Mutateurs
    void setValue(int value);
    void setTotal(int total);
    void setSingleStep(int step);
    void setStep(int step);
    void setVertical(bool vertical = true);
    void setHorizontal(bool horizontal = true);

    // Méthodes publiques
    int getMax() const;
    virtual void draw();
    virtual void onEvent(const CMouseEvent& event);
    void stepUp();
    void stepDown();
    void pageUp();
    void pageDown();

    // Signaux
    sigc::signal<void, int> onValueChange; ///< Signal émis lorsque la valeur change.

private:

    // Données privées
    int m_value;       ///< Valeur actuelle.
    int m_valueTmp;     ///< Valeur temporaire (pour le déplacement à la souris).
    int m_total;       ///< Valeur totale.
    int m_singleStep;  ///< Taille d'une ligne.
    int m_step;        ///< Nombre de lignes dans une page.
    int m_clic;        ///< Donne la position du clic sur la barre.
    unsigned int m_timeLast;   ///< Instant du dernier affichage du widget.
    unsigned int m_timeRepeat; ///< Compteur de temps utilisé pour la répétition lorsqu'un bouton est enfoncé.

    // Méthode protégée
    int computeBarSize() const;

    // Données protégées
    bool m_vertical;     ///< Indique si la barre est verticale (par défaut).
    CPushButton * m_bup; ///< Bouton pour monter.
    CPushButton * m_bdw; ///< Bouton pour descendre.
};

} // Namespace Ted

#endif // T_FILE_GUI_CSCROLLBAR_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ILayoutBox.hpp
 * \date 14/06/2014 Création de la classe ILayoutBox.
 */

#ifndef T_FILE_GUI_ILAYOUTBOX_HPP_
#define T_FILE_GUI_ILAYOUTBOX_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Gui/Export.hpp"
#include "Gui/ILayout.hpp"


namespace Ted
{

/**
 * \class   ILayoutBox
 * \ingroup Gui
 * \brief   Layout permettant de disposer les objets horizontallement ou verticalement.
 ******************************/

class T_GUI_API ILayoutBox : public ILayout
{
public:

    // Constructeur et destructeur
    explicit ILayoutBox(IWidget * parent = nullptr);
    virtual ~ILayoutBox();

    // Accesseur
    IWidget * getChild(int pos) const;

    // Mutateurs
    virtual void setWidth(int width);
    virtual void setHeight(int height);

    // Méthodes publiques
    virtual void draw();
    int getNumChildren() const;
    virtual void addChild(IWidget * widget, TAlignment align = AlignMiddleCenter, int pos = -1, int stretch = 0) = 0;
    bool isChild(const IWidget * widget) const;
    void removeChild(const IWidget * widget);
    void removeChild(int pos);
    void clearChildren();
    virtual void onEvent(const CMouseEvent& event);

protected:

    /**
     * \struct  TLayoutItem
     * \ingroup Gui
     * \brief   Description d'une case du layout.
     ******************************/

    struct TLayoutItem
    {
        IWidget * widget; ///< Pointeur sur le widget.
        int size;         ///< Dimension (largeur ou hauteur) de la case en pixels.
        int stretch;      ///< Facteur d'étirement.
        TAlignment align; ///< Alignement du widget.

        inline TLayoutItem() : widget (nullptr), size (0), stretch (0), align (AlignMiddleCenter) { }
    };

    // Donnée protégée
    std::list<TLayoutItem> m_children; ///< Liste des objets enfants.
};

} // Namespace Ted

#endif // T_FILE_GUI_ILAYOUTBOX_HPP_

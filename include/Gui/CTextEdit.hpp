/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CTextEdit.hpp
 * \date 26/04/2009 Création de la classe GuiTextEdit.
 * \date 16/07/2010 Utilisation possible des signaux, ajout de l'input Clear.
 * \date 04/11/2010 Héritage de la classe GuiBaseTextEdit.
 * \date 05/11/2010 Modification de la gestion du texte et ajout de l'ascenseur.
 * \date 08/01/2011 Ajout de la méthode setTop.
 * \date 17/01/2011 Héritage de la classe GuiFrame.
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 29/05/2011 La classe est renommée en CTextEdit.
 */

#ifndef T_FILE_GUI_CTEXTEDIT_HPP_
#define T_FILE_GUI_CTEXTEDIT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/ITextEdit.hpp"
#include "Gui/IScrollArea.hpp"


namespace Ted
{

/**
 * \class   CTextEdit
 * \ingroup Gui
 * \brief   Champ de texte à plusieurs lignes.
 * \todo    Ajouter un attribut pour stocker le texte mis en forme.
 ******************************/

class T_GUI_API CTextEdit : public IScrollArea, public ITextEdit
{
public:

    // Constructeurs
    explicit CTextEdit(const CString& text = CString(), IWidget * parent = nullptr);
    explicit CTextEdit(IWidget * parent);

    // Mutateurs
    void setText(const CString& text);
    void addText(const CString& text);
    void setWidth(int width);
    void setHeight(int height);

    // Méthodes publiques
    unsigned int getTextHeight() const;
    virtual void draw();
    virtual TCursor getCursorType(int x, int y) const;
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CMouseEvent& event);
};

} // Namespace Ted

#endif // T_FILE_GUI_CTEXTEDIT_HPP_

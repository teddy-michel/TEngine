/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CProgressBar.hpp
 * \date       2008 Création de la classe GuiProgressBar.
 * \date 29/05/2011 La classe est renommée en CProgressBar.
 */

#ifndef T_FILE_GUI_CPROGRESSBAR_HPP_
#define T_FILE_GUI_CPROGRESSBAR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/IWidget.hpp"


namespace Ted
{

/**
 * \class   CProgressBar
 * \ingroup Gui
 * \brief   Affichage d'une barre de progression.
 *
 * Deux variables sont utilisées pour gérer la barre de progression : valeur et
 * total, qui permettent de définir un pourcentage de progression.
 * Si total vaut 0, la barre se déplace en continue de gauche à droite pour
 * indiquer par exemple que l'application est en train d'effectuer une action.
 *
 * Info temporaire : Ce widget est fonctionnel.
 ******************************/

class T_GUI_API CProgressBar : public IWidget
{
public:

    // Constructeurs
    explicit CProgressBar(float total = 0.0f, IWidget * parent = nullptr);
    explicit CProgressBar(IWidget * parent);

    // Accesseur
    float getPourcentage() const;
    float getValue() const;
    float getTotal() const;

    // Mutateurs
    void setPourcentage(float pourcentage);
    void setValue(float valeur);
    void setTotal(float total);

    // Méthode publique
    virtual void draw();

private:

    // Données protégées
    float m_value; ///< Nombre d'unités effectuées.
    float m_total; ///< Nombre total d'unités.
};

} // Namespace Ted

#endif // T_FILE_GUI_CPROGRESSBAR_HPP_

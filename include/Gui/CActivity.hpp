/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CActivity.hpp
 * \date 02/06/2014 Création de la classe CActivity.
 * \date 04/06/2014 Déplacement du module Core vers le module Gui.
 * \date 07/06/2014 La classe CActivity s'occupe de la gestion des objets de l'interface.
 */

#ifndef T_FILE_GUI_CACTIVITY_HPP_
#define T_FILE_GUI_CACTIVITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Core/CString.hpp"
#include "Core/IEventReceiver.hpp"
#include <list>


namespace Ted
{

class CWindow;
class CMenu;
class IWidget;


/**
 * \class   CActivity
 * \ingroup Gui
 * \brief   Classe de base des activités.
 *
 * Les activités représentent les différents modes d'utilisation d'une application. L'application
 * exécute une seule activité à la fois, et lorsqu'une activité se termine, une autre activité
 * s'éxecute.
 *
 * Par exemple, dans un jeu vidéo, on aura une ou plusieurs activités pour lire des vidéos, puis
 * une activité pour gérer le menu principal, qui peut ensuite amener à une activité pour gérer le
 * jeu et une activité pour le menu en jeu, etc.
 *
 * Chaque activité possède un nom unique. Une activité peut être démarrée plusieurs fois au cours
 * de l'exécution du programme. Lorsqu'une activité démarre, la méthode start est appellée. À
 * chaque frame, la méthode update est appellée avec la durée de la frame. Enfin, lorsque l'activité
 * se termine, la méthode stop est appellée.
 ******************************/

class T_GUI_API CActivity : public IEventReceiver
{
public:

    CActivity(const CString& activityName);
    virtual ~CActivity();

    // Accesseurs
    bool hasFocus(const IWidget * objet) const;
    IWidget * getFocus() const;
    bool isPointed(const IWidget * objet) const;
    IWidget * getPointed() const;
    bool isSelected(const IWidget * objet) const;
    IWidget * getSelected() const;
    bool isForeground(const IWidget * objet) const;
    IWidget * getForeground() const;

    void setFocus(IWidget * widget = nullptr);
    void setPointed(IWidget * widget = nullptr);
    void setSelected(IWidget * widget = nullptr);
    void setForeground(IWidget * widget = nullptr);

    CString getActivityName() const;

    // Fenêtres
    void addWindow(CWindow * window);
    void removeWindow(CWindow * window);
    std::list<CWindow *> getWindows() const;
    void onWindowOpen(CWindow * window);
    void onWindowClose(CWindow * window);

    void setMenu(CMenu * menu);
    CMenu * getMenu() const;

    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event);

    virtual void start();
    void updateActivity();
    virtual void stop();

protected:

    virtual void update();

    CString m_activityName;         ///< Nom de l'activité.
    std::list<CWindow *> m_windows; ///< Liste des fenêtres.
    CMenu * m_menu;                 ///< Menu à utiliser.
    IWidget * m_focus;              ///< Pointeur sur l'objet qui a le focus.
    IWidget * m_pointed;            ///< Pointeur sur l'objet pointé par le curseur.
    IWidget * m_selected;           ///< Pointeur sur l'objet sélectionné (bouton gauche de la souris enfoncé).
    IWidget * m_foreground;         ///< Pointeur sur l'objet situé au premier plan (menu, infobulle, ou combobox).
};

} // Namespace Ted

#endif // T_FILE_GUI_CACTIVITY_HPP_

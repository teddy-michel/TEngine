/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow_Save.hpp
 * \date 20/06/2010 Création de la classe GuiWindow_Save.
 * \date 29/05/2011 La classe est renommée en CWindow_Save.
 */

#ifndef T_FILE_GUI_CWINDOW_SAVE_HPP_
#define T_FILE_GUI_CWINDOW_SAVE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/CWindow.hpp"


namespace Ted
{

/**
 * \class   CWindow_Save
 * \ingroup Gui
 * \brief   Classe permettant de gérer la fenêtre Sauvegarder une partie.
 ******************************/

class T_GUI_API CWindow_Save : public CWindow
{
public:

    // Constructeur
    explicit CWindow_Save(IWidget * parent = nullptr);
};

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOW_SAVE_HPP_

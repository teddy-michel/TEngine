/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindowFile.hpp
 * \date 18/02/2010 Création de la classe GuiWindowFile.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CWindowFile.
 */

#ifndef T_FILE_GUI_CWINDOWFILE_HPP_
#define T_FILE_GUI_CWINDOWFILE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/Export.hpp"
#include "Gui/CWindow.hpp"
#include "Core/CDirectory.hpp"


namespace Ted
{

/**
 * \enum    TFileMode
 * \ingroup Gui
 * \brief   This enum is used to indicate what the user may select in the file dialog.
 ******************************/

enum TFileMode
{
    AnyFileOpen,      ///< Nom d'un fichier qui peut ne pas exister pour l'ouverture.
    AnyFileSave,      ///< Nom d'un fichier qui peut ne pas exister pour la sauvegarde.
    ExistingFileOpen, ///< Nom d'un fichier existant pour l'ouverture.
    ExistingFileSave, ///< Nom d'un fichier existant pour la sauvegarde.
    ExistingFiles,    ///< Nom de plusieurs fichiers existants.
    Directory         ///< Nom d'un répertoire.
};


class CPushButton;
class CLineEdit;


/**
 * \class   CWindowFile
 * \ingroup Gui
 * \brief   Fenêtre permettant d'ouvrir ou de sauvegarde des fichiers.
 *
 * \section Signaux
 * \li onFileSelected
 * \li onFilterSelected
 * \li onDirectoryChange
 ******************************/

class T_GUI_API CWindowFile : public CWindow
{
public:

    // Constructeur et desutrcteur
    explicit CWindowFile(const CString& dir = CString(), const CString& filter = CString(), IWidget * parent = nullptr);
    virtual ~CWindowFile();

    // Accesseurs
    bool isDirsOnly() const;
    TFileMode getMode() const;
    CString getSuffix() const;
    CString getDirectory() const;
    CString getFilter() const;

    // Mutateurs
    void setDirsOnly(bool value = true);
    void setMode(TFileMode mode);
    void setSuffix(const CString& suffix);
    void setDirectory(const CString& dir);
    void setFilter(const CString& filter);

    // Méthode publique
    void parentDirectory();

    // Signaux
    sigc::signal<void> onFileSelected;    ///< Signal émis lorsque le fichier sélectionné change.
    sigc::signal<void> onFilterSelected;  ///< Signal émis lorsque le filtre change.
    sigc::signal<void> onDirectoryChange; ///< Signal émis lorsque le répertoire change.

protected:

    // Données protégées
    bool m_dirs_only;              ///< Indique qu'on doit afficher uniquement les répertoires.
    TFileMode m_mode;              ///< Mode de sélection.
    CString m_suffix;              ///< Suffixe à ajouter aux noms de fichier.
    CString m_dir;                 ///< Répertoire affiché.
    CString m_filter;              ///< Filtre à appliquer aux noms de fichiers et répertoires.
    CPushButton * m_button_accept; ///< Bouton pour accepter (ouvrir ou sauvegarder un fichier).
    CPushButton * m_button_cancel; ///< Bouton pour fermer la fenêtre.
    CLineEdit * m_line;            ///< Ligne de texte pour le nom du fichier.
};

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOWFILE_HPP_

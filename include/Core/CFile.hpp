/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CFile.hpp
 * \date 12/01/2010 Création de la classe CFile.
 * \date 27/07/2010 La copie est interdite.
 * \date 09/04/2012 Ajout de l'énumération TPermission.
 *                  Améliorations.
 */

#ifndef T_FILE_CORE_CFILE_HPP_
#define T_FILE_CORE_CFILE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <sstream>
#include <string>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/INonCopyable.hpp"
#include "Core/CString.hpp"
#include "Core/CFlags.hpp"


namespace Ted
{

class ILoaderArchive;


/**
 * \class   CFile
 * \ingroup Core
 * \brief   Classe pour gérer un fichier.
 *
 * La méthode Open doit être appelée pour ouvrir le fichier avec le mode désiré. Le fichier est
 * recherché dans le système de fichier virtuel. Les opérations de lecture et d'écriture peuvent
 * être réalisées avec les méthodes Read et Write.
 ******************************/

class T_CORE_API CFile : public INonCopyable
{
public:

    /// Seeking directions.
    enum TSeekDir
    {
        Current, ///< Beginning of the stream buffer.
        Begin,   ///< Current position in the stream buffer.
        End      ///< End of the stream buffer.
    };

    /// Permissions des fichiers.
    enum TPermission
    {
        OwnerRead  = 0x4000, ///< The file is readable by the owner of the file.
        OwnerWrite = 0x2000, ///< The file is writable by the owner of the file.
        OwnerExe   = 0x1000, ///< The file is executable by the owner of the file.
        UserRead   = 0x0400, ///< The file is readable by the user.
        UserWrite  = 0x0200, ///< The file is writable by the user.
        UserExe    = 0x0100, ///< The file is executable by the user.
        GroupRead  = 0x0040, ///< The file is readable by the group.
        GroupWrite = 0x0020, ///< The file is writable by the group.
        GroupExe   = 0x0010, ///< The file is executable by the group.
        OtherRead  = 0x0004, ///< The file is readable by anyone.
        OtherWrite = 0x0002, ///< The file is writable by anyone.
        OtherExe   = 0x0001  ///< The file is executable by anyone.
    };

    T_DECLARE_FLAGS(TPermissions, TPermission)

    /// Modes d'ouverture des fichiers
    enum TOpenModeFlag
    {
        ReadOnly     = 0x01, ///< Le fichier est ouvert en lecture seule.
        WriteOnly    = 0x02, ///< Le fichier est ouvert en écriture seule.
        ReadAndWrite = ReadOnly | WriteOnly, ///< Le fichier est ouvert en lecture et en écriture.
        Append       = 0x04, ///< Les données sont ajoutés à la fin du fichier.
        Truncate     = 0x08, ///< Les données présentes dans le fichier sont effacées.
        Text         = 0x10  ///< Les caractères de fin de ligne dépendent du système d'exploitation.
    };

    T_DECLARE_FLAGS(TOpenMode, TOpenModeFlag)


    // Constructeur et destructeur
    CFile(const CString& fileName = CString());
    ~CFile();

    // Nom du fichier
    CString getAbsolutePath() const;
    CString getFullName() const;
    CString getFileName() const;
    CString getName() const;
    CString getExtension() const;
    void setFileName(const CString& fileName);

    // Ouverture et fermeture
    bool isOpen() const;
    bool open(const CString& fileName, TOpenMode mode);
    bool open(TOpenMode mode);
    void close();

    // Taille du fichier
    uint64_t getFileSize() const;
    static uint64_t getFileSize(const CString& fileName);

    // Curseur
    CFile& seekCursor(uint64_t off, TSeekDir dir = Begin);
    uint64_t tellCursor();

    // Lecture
    std::streambuf * readBuffer() const;
    int64_t read(char * s, uint64_t n);

    // Écriture
    int64_t write(const char * s, uint64_t n);

private:

    // Méthodes privées
    bool open(const CString& archive, const CString& fileName);
    bool open(ILoaderArchive * archive, const CString& fileName);

protected:

    enum TFileType
    {
        FileNone,  ///< Le fichier n'est pas ouvert.
        FileDisk,  ///< Le fichier est sur le disque.
        FileMemory ///< Le fichier est en mémoire.
    };

    // Données protégées
    CString m_fileName;         ///< Adresse ou nom du fichier.
    uint64_t m_size;            ///< Taille du fichier en octets.
    TFileType m_type;           ///< Indique où se trouve le fichier.
    TOpenMode m_mode;           ///< Mode d'ouverture du fichier.
    std::fstream m_file;        ///< Contient le buffer si c'est un fichier réel.
    std::stringstream m_buffer; ///< Contient le buffer si c'est un fichier virtuel.
};


T_DECLARE_OPERATORS_FOR_FLAGS(CFile::TPermissions)

T_DECLARE_OPERATORS_FOR_FLAGS(CFile::TOpenMode)

} // Namespace Ted

#endif // T_FILE_CORE_CFILE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IResourceManager.hpp
 * \date 07/10/2012 Création de la classe IResourceManager.
 * \date 03/06/2014 Gestion des répertoires de recherche.
 */

#ifndef T_FILE_CORE_IRESOURCEMANAGER_HPP_
#define T_FILE_CORE_IRESOURCEMANAGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/CString.hpp"

#include <list>


namespace Ted
{

/**
 * \class   IResourceManager
 * \ingroup Core
 * \brief   Classe de base des gestionnaires de ressources.
 ******************************/

class T_CORE_API IResourceManager
{
public:

    // Constructeur et destructeur
    IResourceManager();
    virtual ~IResourceManager();

    virtual unsigned int getMemorySize() const = 0;
    virtual unsigned int getNumElements() const = 0;
    virtual int getResourceId(const CString& name) = 0;
    virtual CString getResourceName(int id) const = 0;

    // Répertoires
    void addPath(const CString& pathName);
    void removePath(const CString& pathName);
    std::list<CString> getPaths() const;

private:

    inline virtual void freeMemory();

protected:

    std::list<CString> m_paths; ///< Liste des répertoires où rechercher les ressources.
};


inline void IResourceManager::freeMemory()
{ }

} // Namespace Ted

#endif // T_FILE_CORE_IRESOURCEMANAGER_HPP_

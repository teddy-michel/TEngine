/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILoader.hpp
 * \date       2008 Création de la classe ILoader.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */

#ifndef T_FILE_CORE_ILOADER_HPP_
#define T_FILE_CORE_ILOADER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>

#include "Core/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   ILoader
 * \ingroup Core
 * \brief   Classe de base des chargeurs de fichiers.
 ******************************/

class T_CORE_API ILoader
{
public:

    // Constructeur et destructeur
    ILoader();
    virtual ~ILoader();

    // Accesseur
    CString getFileName() const;

    // Méthodes publiques
    virtual bool loadFromFile(const CString& fileName) = 0;
    virtual bool saveToFile(const CString& fileName) const;

protected:

    // Données protégées
    CString m_fileName; ///< Adresse du fichier.
};

} // Namespace Ted

#endif // T_FILE_CORE_ILOADER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CAStar.hpp
 * \date 31/01/2011 Création de la classe CAStar.
 */

#ifndef T_FILE_CORE_CASTAR_HPP_
#define T_FILE_CORE_CASTAR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <algorithm>
#include <limits>

#include "Core/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \struct  TNode
 * \ingroup Core
 * \brief   Représente un nœud du graphe.
 ******************************/

struct TNode
{
    TVector3F pos;          ///< Position du nœud dans l'espace.
    std::list<TNode *> adj; ///< Liste des voisins de ce nœud.
    float g;                ///< Coût depuis le point de départ.
    float h;                ///< Coût jusqu'au point d'arrivée.
    TNode * parent;         ///< Nœud par lequel on est arrivé.
};


/**
 * \class   CAStar
 * \ingroup Core
 * \brief   Utilisation de l'algorithme A* pour trouver un chemin entre deux nœuds.
 ******************************/

class T_CORE_API CAStar
{
public:

    // Constructeur
    CAStar(TNode * from, TNode * to);

    // Méthodes publiques
    void setCurrentNode(TNode * node);
    void setDestination(TNode * node);
    TNode * nextNode() const;
    float getDistance();
    static float nodeDistance(TNode * a, TNode * b);

private:

    // Méthode privée
    void computePath();

    // Données privées
    TNode * m_current_node;    ///< Pointeur sur le nœud actuel.
    TNode * m_final_node;      ///< Pointeur sur le nœud final.
    std::list<TNode *> m_path; ///< Liste des nœuds à parcourir.
};

} // Namespace Ted

#endif // T_FILE_CORE_CASTAR_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoader7Z.hpp
 * \date 31/01/2011 Création de la classe CLoader7Z.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 14/03/2011 Inclusion de stdint.h.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */

#ifndef T_FILE_CORE_CLOADER7Z_HPP_
#define T_FILE_CORE_CLOADER7Z_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/ILoaderArchive.hpp"


namespace Ted
{

/**
 * \class   CLoader7Z
 * \ingroup Core
 * \brief   Lecture d'une archive au format 7Z.
 ******************************/

class T_CORE_API CLoader7Z : public ILoaderArchive
{
public:

    // Constructeur
    CLoader7Z();

    // Méthodes publiques
    bool getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size);
    uint64_t getFileSize(const CString& fileName);
    bool loadFromFile(const CString& fileName);

    // Méthodes statiques publiques
    static bool isCorrectFormat(const char * fileName);

#ifdef T_NO_COVARIANT_RETURN
    static ILoaderArchive * createInstance(const char * fileName);
#else
    static CLoader7Z * createInstance(const char * fileName);
#endif

private:

    /// Propriétés.
    enum TProperty
    {
        kEnd                    = 0x00,
        kHeader                 = 0x01,
        kArchiveProperties      = 0x02,
        kAdditionalStreamsInfo  = 0x03,
        kMainStreamsInfo        = 0x04,
        kFilesInfo              = 0x05,
        kPackInfo               = 0x06,
        kUnPackInfo             = 0x07,
        kSubStreamsInfo         = 0x08,
        kSize                   = 0x09,
        kCRC                    = 0x0A,
        kFolder                 = 0x0B,
        kCodersUnPackSize       = 0x0C,
        kNumUnPackStream        = 0x0D,
        kEmptyStream            = 0x0E,
        kEmptyFile              = 0x0F,
        kAnti                   = 0x10,
        kName                   = 0x11,
        kCTime                  = 0x12,
        kATime                  = 0x13,
        kMTime                  = 0x14,
        kWinAttributes          = 0x15,
        kComment                = 0x16,
        kEncodedHeader          = 0x17,
        kStartPos               = 0x18,
        kDummy                  = 0x19
    };

/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

#include "Core/struct_alignment_start.h"

    /// En-tête du fichier.
    struct TSignatureHeader
    {
        uint8_t signature[6];        ///< Signature du fichier 0x377ABCAF271C ou {'7', 'z', 0xBC, 0xAF, 0x27, 0x1C}
        uint8_t version_major;       ///< Numéro de version (0).
        uint8_t version_minor;       ///< Numéro de sous-version (2 ou 3).
        uint32_t start_header_crc;   ///< CRC de l'en-tête.
        uint64_t next_header_offset; ///< Offset du prochain header.
        uint64_t next_header_size;   ///< Longueur du prochain header.
        uint32_t next_header_crc;    ///< CRC du prochain header.
    };

#include "Core/struct_alignment_end.h"

    // Méthodes privées
    void ReadBloc(const std::streampos offset, const std::size_t size);
    uint32_t ConvertUint32(char buf[4]);
    uint64_t ConvertUint64(char buf[8]);

    char * ReadHeader(char * buf);
    char * ReadArchiveProperties(char * buf);
    char * ReadAdditionalStreamsInfo(char * buf);
    char * ReadMainStreamsInfo(char * buf);
    char * ReadFilesInfo(char * buf);
    char * ReadPackInfo(char * buf);
    char * ReadUnPackInfo(char * buf);
    char * ReadSubStreamsInfo(char * buf);
    //char * ReadSize(char * buf);
    //char * ReadCRC(char * buf);
    char * ReadFolder(char * buf);
    //char * ReadCodersUnPackSize(char * buf);
    //char * ReadNumUnPackStream(char * buf);
    //char * ReadEmptyStream(char * buf);
    //char * ReadEmptyFile(char * buf);
    //char * ReadAnti(char * buf);
    //char * ReadName(char * buf);
    //char * ReadCTime(char * buf);
    //char * ReadATime(char * buf);
    //char * ReadMTime(char * buf);
    //char * ReadWinAttributes(char * buf);
    //char * ReadComment(char * buf);
    char * ReadEncodedHeader(char * buf);
    //char * ReadStartPos(char * buf);

    // Donnée privée
    std::ifstream m_file; ///< Fichier de l'archive.
};

} // Namespace Ted

#endif // T_FILE_CORE_CLOADER7Z_HPP_

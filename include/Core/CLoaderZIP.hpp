/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderZIP.hpp
 * \date 10/01/2010 Création de la classe CLoaderZIP.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */

#ifndef T_FILE_CORE_CLOADERZIP_HPP_
#define T_FILE_CORE_CLOADERZIP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/ILoaderArchive.hpp"


namespace Ted
{

/**
 * \class   CLoaderZIP
 * \ingroup Core
 * \brief   Lecture d'une archive au format ZIP.
 ******************************/

class T_CORE_API CLoaderZIP : public ILoaderArchive
{
public:

    // Constructeur
    CLoaderZIP();

    // Méthodes publiques
    bool getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size);
    uint64_t getFileSize(const CString& fileName);
    bool loadFromFile(const CString& fileName);

    // Méthodes statiques publiques
    static bool isCorrectFormat(const char * fileName);

#ifdef T_NO_COVARIANT_RETURN
    static ILoaderArchive * createInstance(const char * fileName);
#else
    static CLoaderZIP * createInstance(const char * fileName);
#endif

private:

/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

#include "Core/struct_alignment_start.h"

    /// Description du contenu de l'archive (22 octets à la fin du fichier)
    struct ZIP_EndOfCentralDirRecord
    {
        uint32_t signature;                ///< Signature : 0x06054b50 = PK56.
        uint16_t number_of_this_disk;
        uint16_t number_of_the_disk_with_start_of_central_directory;
        uint16_t central_directory_entries_this_disk;
        uint16_t central_directory_entries_total;
        uint32_t central_directory_size;
        uint32_t start_of_central_dir_offset;
        uint16_t comment_length;           ///< File comment length.
        // Commentaire (taille variable)
    };

    /// Description d'un fichier
    struct ZIP_FileHeader
    {
        uint32_t signature;                       ///< Signature : 0x02014b50 = PK12.
        uint16_t version_made_by;                 ///< Version made by.
        uint16_t version_needed_to_extract;       ///< Version needed to extract.
        uint16_t flags;                           ///< General purpose bit flag.
        uint16_t compression_method;              ///< Compression method.
        uint16_t last_modified_time;              ///< Last mod file time.
        uint16_t last_modified_date;              ///< Last mod file date.
        uint32_t crc32;                           ///< CRC-32.
        uint32_t compressed_size;                 ///< Compressed size.
        uint32_t uncompressed_size;               ///< Uncompressed size.
        uint16_t file_name_length;                ///< File name length.
        uint16_t extra_field_length;              ///< Extra field length.
        uint16_t file_comment_length;             ///< File comment length.
        uint16_t disk_number_start;               ///< Disk number start.
        uint16_t internal_file_attribs;           ///< Internal file attributes.
        uint32_t external_file_attribs;           ///< External file attributes.
        uint32_t relative_offset_of_local_header; ///< Relative offset of local header.
        // Nom du fichier (taille variable)
        // Informations supplémentaires (taille variable)
        // Commentaire (taille variable)
    };

    /// Contenu d'un fichier
    struct ZIP_LocalFileHeader
    {
        uint32_t signature;                 ///< Signature : 0x04034b50 = PK34.
        uint16_t version_needed_to_extract; ///< Version needed to extract.
        uint16_t flags;                     ///< General purpose bit flag.
        uint16_t compression_method;        ///< Compression method.
        uint16_t last_modified_time;        ///< Last mod file time.
        uint16_t last_modified_date;        ///< Last mod file date.
        uint32_t crc32;                     ///< CRC-32.
        uint32_t compressed_size;           ///< Compressed size.
        uint32_t uncompressed_size;         ///< Uncompressed size.
        uint16_t file_name_length;          ///< File name length.
        uint16_t extra_field_length;        ///< Extra field length.
        // Nom du fichier (taille variable)
        // Informations supplémentaires (taille variable)
        // Contenu du fichier (taille variable)
    };

#include "Core/struct_alignment_end.h"

protected:

    // Données protégées
    std::vector<uint64_t> m_offsets; ///< Offset de chaque fichier.
    std::vector<uint64_t> m_lengths; ///< Longueur de chaque fichier.
    std::ifstream m_file;            ///< Fichier de l'archive.
};

} // Namespace Ted

#endif // T_FILE_CORE_CLOADERZIP_HPP_

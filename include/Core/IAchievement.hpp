/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IAchievement.hpp
 * \date 01/07/2010 Création de la classe IAchievement.
 * \date 03/07/2010 On peut bloquer un succès.
 */

#ifndef T_FILE_CORE_IACHIEVEMENT_HPP_
#define T_FILE_CORE_IACHIEVEMENT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   IAchievement
 * \ingroup Core
 * \brief   Classe de base des succès.
 ******************************/

class T_CORE_API IAchievement
{
public:

    // Constructeur
    explicit IAchievement(const CString& name);

    // Accesseurs
    inline CString getName() const;
    inline unsigned int getMaxSteps() const;
    inline unsigned int getNumSteps() const;
    inline bool isAchieve() const;
    inline bool isLocked() const;

    // Mutateurs
    void setName(const CString& name);
    void setMaxSteps(unsigned int steps);
    void setNumSteps(unsigned int steps);
    void setLocked(bool locked = true);

    // Méthode publique
    virtual void StepUp(unsigned int steps = 1) = 0;

protected:

    // Données protégées
    CString m_name;           ///< Nom du succès.
    unsigned int m_max_steps; ///< Nombre de pas pour que le succès soit dévérouillé.
    unsigned int m_nbr_steps; ///< Nombre actuel de pas.
    bool m_locked;            ///< Indique si le succès est bloqué.
};


/**
 * Donne le nom du succès.
 *
 * \return Nom du succès.
 ******************************/

inline CString IAchievement::getName() const
{
    return m_name;
}


/**
 * Donne le nombre de pas pour que le succès soit dévérouillé.
 *
 * \return Nombre de pas pour que le succès soit dévérouillé.
 ******************************/

inline unsigned int IAchievement::getMaxSteps() const
{
    return m_max_steps;
}


/**
 * Donne le nombre actuel de pas.
 *
 * \return Nombre actuel de pas.
 ******************************/

inline unsigned int IAchievement::getNumSteps() const
{
    return m_nbr_steps;
}


/**
 * Indique si le succès est dévérouillé.
 *
 * \return Booléen.
 ******************************/

inline bool IAchievement::isAchieve() const
{
    return (!m_locked && m_nbr_steps >= m_max_steps);
}


/**
 * Indique si le succès est bloqué.
 *
 * \return Booléen.
 ******************************/

inline bool IAchievement::isLocked() const
{
    return m_locked;
}

} // Namespace Ted

#endif // T_FILE_CORE_IACHIEVEMENT_HPP_

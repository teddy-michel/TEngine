/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Time.hpp
 * \date 18/07/2010 Création des fonctions de gestion du temps.
 */

#ifndef T_FILE_CORE_TIME_HPP_
#define T_FILE_CORE_TIME_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdint.h>

#include "Core/Export.hpp"


namespace Ted
{

#ifdef T_TIME_64BITS
typedef uint64_t TTime;
#else
typedef uint32_t TTime;
#endif

T_CORE_API TTime getElapsedTime();
T_CORE_API void Delay(TTime time);

} // Namespace Ted

#endif // T_FILE_CORE_TIME_HPP_

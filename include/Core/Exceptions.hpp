/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Exceptions.hpp
 * \date       2008 Création des classes d'exception.
 * \date 03/12/2010 Ajout de l'exception CUncorrectValue.
 * \date 27/12/2010 Création de la macro T_DEPRECATED.
 * \date 14/11/2011 Création de l'exception CBadArgument qui remplace CUncorrectValue.
 */

#ifndef T_FILE_CORE_EXCEPTIONS_HPP_
#define T_FILE_CORE_EXCEPTIONS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <exception>
#include <string>

#include "Core/Export.hpp"


namespace Ted
{

/**
 * \class   IException
 * \ingroup Core
 * \brief   Classe de base pour les exceptions.
 ******************************/

class T_CORE_API IException : public std::exception
{
public:

    // Constructeur et destructeur
    IException(const std::string& message = std::string());
    virtual ~IException() throw();

    // Méthode publique
    virtual const char * what() const throw();

private:

    std::string m_message; ///< Message décrivant l'exception.
};


/**
 * \struct  CAssertException
 * \ingroup Core
 * \brief   Exception lancée si une condition n'est pas vérifiée.
 ******************************/

struct T_CORE_API CAssertException : public IException
{
    CAssertException(const std::string& file, int line, const std::string& message);
};


#ifdef T_DEBUG
#  define T_ASSERT(condition) if (!(condition)) throw Ted::CAssertException(__FILE__, __LINE__, "Condition non satisfaite\n" #condition)
#else
   inline void DoNothing(bool) {}
#  define T_ASSERT(condition) Ted::DoNothing(!(condition))
#endif


#define T_DEPRECATED(name) ILogger::log("La fonction ou la methode " #name " est obsolete.\n")



/**
 * \struct  CBadDelete
 * \ingroup Core
 * \brief   Exception lancée lors d'une anomalie d'allocation mémoire.
 ******************************/

struct T_CORE_API CBadDelete : public IException
{
    CBadDelete(const void * ptr, const std::string& file, int line, bool new_array);
};


/**
 * \struct  CLoadingFailed
 * \ingroup Core
 * \brief   Exception lancée lors d'erreur de chargement de fichiers.
 ******************************/

struct T_CORE_API CLoadingFailed : public IException
{
    CLoadingFailed(const std::string& file, const std::string& message);
};


/**
 * \struct  COutOfMemory
 * \ingroup Core
 * \brief   Exception lancée lors de saturations de mémoire.
 ******************************/

struct T_CORE_API COutOfMemory : public IException
{
    COutOfMemory(const std::string& message);
};


/**
 * \struct  CUnsupported
 * \ingroup Core
 * \brief   Exception lancée lors de l'utilisation d'une caractéristique non supportée.
 ******************************/

struct T_CORE_API CUnsupported : public IException
{
    CUnsupported(const std::string& feature);
};


/**
 * \struct  CBadConversion
 * \ingroup Core
 * \brief   Exception lancée lors d'une erreur de conversion.
 ******************************/

struct T_CORE_API CBadConversion : public IException
{
    CBadConversion(const std::string& error);
};


/**
 * \struct  CBadArgument
 * \ingroup Core
 * \brief   Exception lancée si un argument d'une méthode est incorrect.
 ******************************/

struct T_CORE_API CBadArgument : public IException
{
    CBadArgument(const std::string& var);
};

} // Namespace Ted

#endif // T_FILE_CORE_EXCEPTIONS_HPP_

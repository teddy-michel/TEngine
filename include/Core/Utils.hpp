/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Utils.hpp
 * \date       2008 Création des fonctions utilitaires.
 * \date 14/08/2010 Création de la fonction DoubleToString.
 * \date 24/12/2010 Déplacement de certaines fonctions vers le module de mathématiques.
 * \date 14/01/2011 Ajout de la fonction swap.
 * \date 02/06/2011 Ajout de la fonction PointerToString.
 * \date 25/03/2012 Ajout de la fonction SumUint.
 */

#ifndef T_FILE_CORE_UTILS_HPP_
#define T_FILE_CORE_UTILS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sstream>
#include <ctime>
#include <algorithm>
#include <limits>
#include <sstream>

#include "os.h"

#ifdef T_SYSTEM_WINDOWS
#  ifdef max
#    undef max
#  endif
#endif

#include "Core/Export.hpp"


namespace Ted
{

// Tests de type
T_CORE_API bool isShort(const std::string& str);
T_CORE_API bool isUShort(const std::string& str);
T_CORE_API bool isInt(const std::string& str);
T_CORE_API bool isUInt(const std::string& str);
T_CORE_API bool isFloat(const std::string& str);
T_CORE_API bool isDouble(const std::string& str);

// Fonctions non inlinables
T_CORE_API std::string toLower(const std::string& str);
T_CORE_API std::string toUpper(const std::string& str);
T_CORE_API std::string UTF8toLatin1(const std::string& str);
T_CORE_API std::string DoubleToString(double nbr, unsigned int decimals = 2);
T_CORE_API std::string PointerToString(void * ptr);


/**
 * Additionne deux entiers non signés sans dépassement de capacité.
 * Si la somme des deux entiers est supérieure à la valeur maximale possible,
 * celle-ci est retournée à la place de la somme calculée.
 *
 * \param i1 Premier entier.
 * \param i2 Second entier.
 * \return Somme des deux entiers.
 ******************************/

inline unsigned int SumUint(unsigned int i1, unsigned int i2)
{
    if (i1 > std::numeric_limits<unsigned int>::max() - i2)
    {
        return std::numeric_limits<unsigned int>::max();
    }
    else
    {
        return (i1 + i2);
    }
}


/**
 * Additionne deux entiers signés sans dépassement de capacité.
 * Si la somme des deux entiers est supérieure à la valeur maximale possible
 * ou inférieure à la valeur minimale possible, la valeur maximale ou
 * minimale (respectivement) est retournée.
 *
 * \param i1 Premier entier.
 * \param i2 Second entier.
 * \return Somme des deux entiers.
 ******************************/

inline int SumInt(int i1, int i2)
{
    if (i2 > 0 && i1 > (std::numeric_limits<int>::max() - i2))
    {
        return std::numeric_limits<int>::max();
    }
    else if (i2 < 0 && i1 < (std::numeric_limits<int>::min() - i2))
    {
        return std::numeric_limits<int>::min();
    }
    else
    {
        return (i1 + i2);
    }
}


/**
 * Inverse deux variables pour lesquels le XOR est défini.
 *
 * \param v1 Première variable.
 * \param v2 Seconde variable.
 ******************************/

template<typename T>
inline void swap(T& v1, T& v2)
{
    v1 ^= v2;
    v2 ^= v1;
    v1 ^= v2;
}


/**
 * Convertit n'importe quel type en chaîne de caractères.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param value Objet à transformer.
 * \return Chaine de caractères.
 ******************************/

template<typename T>
std::string toString(const T& value)
{
    std::ostringstream oss;
    oss << value;
    return oss.str();
}


/**
 * Convertit n'importe quel type en booléen.
 *
 * \param value Objet à transformer.
 * \return Booléen.
 ******************************/

template<typename T>
bool toBool(const T& value)
{
    std::istringstream iss(value);
    bool nbr;
    iss >> nbr;

    return nbr;
}


/**
 * Convertit n'importe quel type en int si possible.
 *
 * \param value Objet à transformer.
 * \return Entier.
 ******************************/

template<typename T>
int toInt(const T& value)
{
    std::istringstream iss(value);
    int nbr;
    iss >> nbr;

    return nbr;
}


/**
 * Convertit n'importe quel type en unsigned int si possible.
 *
 * \param value Objet à transformer.
 * \return Entier non signé.
 ******************************/

template<typename T>
unsigned int toUInt(const T& value)
{
    std::istringstream iss(value);
    unsigned int nbr;
    iss >> nbr;

    return nbr;
}


/**
 * Convertit n'importe quel type en float si possible.
 *
 * \param value Objet à transformer.
 * \return Nombre à virgule flottant.
 ******************************/

template<typename T>
float toFloat(const T& value)
{
    std::istringstream iss(value);
    float nbr;
    iss >> nbr;

    return nbr;
}


/**
 * Convertit n'importe quel type en double si possible.
 *
 * \param value Objet à transformer.
 * \return Nombre à virgule flottant.
 ******************************/

template<typename T>
double toDouble(const T& value)
{
    std::istringstream iss(value);
    double nbr;
    iss >> nbr;

    return nbr;
}


/**
 * Convertit une chaine de caractères en un autre type.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str  Chaine de caractères.
 * \param dest Objet de destination.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

template<typename T>
bool fromString(const std::string& str, T& dest)
{
    std::istringstream iss(str);
    return ((iss >> dest) != 0);
}


/**
 * Supprime toutes les occurences d'un caractère dans une chaine.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \param c   Caractère à supprimer.
 ******************************/

inline void StringDeleteChar(std::string& str, const char c)
{
    str.erase(std::remove(str.begin(), str.end(), c), str.end());
}

} // Namespace Ted

#endif // T_FILE_CORE_UTILS_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Core/CMemoryManager.hpp
 * \date 29/12/2009 Création de la classe CMemoryManager.
 * \date 13/07/2010 Le gestionnaire de mémoire fait de nouveau partie du projet.
 */

#ifdef T_USING_MEMORY_MANAGER

#ifndef T_FILE_CORE_CMEMORYMANAGER_HPP_
#define T_FILE_CORE_CMEMORYMANAGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <map>
//#include <stack>
#include <string>

#include "Core/Export.hpp"


namespace Ted
{

/**
 * \class   CMemoryManager
 * \ingroup Core
 * \brief   Gestion de la mémoire.
 ******************************/

class T_CORE_API CMemoryManager
{
public:

    // Méthodes publiques
    static CMemoryManager& Instance();
    void * allocate(std::size_t size, const std::string& file, int line, bool array);
    void Free(void * ptr, bool array);
    void nextDelete(const std::string& file, int line);

private:

    // Constructeur et destructeur
    CMemoryManager();
    ~CMemoryManager();

    // Méthode privée
    void reportLeaks();

    struct TBlock
    {
        std::size_t size;  ///< Taille allouée.
        std::string file;  ///< Fichier contenant l'allocation.
        int line;          ///< Ligne de l'allocation.
        bool array;        ///< Indique si c'est un tableau.
    };

    typedef std::map<void *, TBlock> TBlockMap;

    // Données membres
    std::ofstream m_file;             ///< Fichier de sortie.
    TBlockMap m_blocks;               ///< Blocs de mémoire alloués.

    //std::stack<TBlock> m_deleteStack; ///< Pile dont le sommet contient la ligne et le fichier de la prochaine désallocation.
    static const int nextDeleteCount = 16;
    TBlock m_nextDelete[nextDeleteCount];
    int m_nextDeleteCurrent;
};

} // Namespace Ted

#endif // T_FILE_CORE_CMEMORYMANAGER_HPP_

#endif // T_USING_MEMORY_MANAGER

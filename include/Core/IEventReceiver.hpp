/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IEventReceiver.hpp
 * \date 11/05/2009 Création de la classe IEventReceiver.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 06/04/2013 Ajout de la méthode onEvent avec un évènement de type CResizeEvent.
 */

#ifndef T_FILE_CORE_IEVENTRECEIVER_HPP_
#define T_FILE_CORE_IEVENTRECEIVER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"


namespace Ted
{

struct CMouseEvent;
struct CTextEvent;
struct CKeyboardEvent;
struct CResizeEvent;


/**
 * \class   IEventReceiver
 * \ingroup Core
 * \brief   Classe de base des récepteurs d'évènements, comme les caméras et les joueurs.
 ******************************/

class T_CORE_API IEventReceiver
{
public:

    // Constructeur et destructeur
    IEventReceiver();
    virtual ~IEventReceiver() = 0;

    // Méthodes publiques
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event);
};

} // Namespace Ted

#endif // T_FILE_CORE_IEVENTRECEIVER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILogger.hpp
 * \date       2008 Création de la classe ILogger.
 * \date 02/10/2011 Ajout de la méthode hasLogger.
 * \date 09/04/2012 Ajout de la méthode pour logger une liste.
 */

#ifndef T_FILE_CORE_ILOGGER_HPP_
#define T_FILE_CORE_ILOGGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sstream>
#include <string>
#include <cstdarg>
#include <list>

#include "Core/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   ILogger
 * \ingroup Core
 * \brief   Classe abstraite destinée à crée un logger.
 *
 * Elle peut ensuite être dérivée pour créer un logger dans des fichiers, dans une page HTML,
 * dans une fenêtre, etc.
 ******************************/

class T_CORE_API ILogger
{
public:

    /// Niveau prédéfinis.
    enum TLevel
    {
        Debug   = -5, ///< Message de debug.
        Info    =  0, ///< Message d'information.
        Warning =  5, ///< Message d'avertissement.
        Error   = 10, ///< Message d'erreur.
        Alert   = 15  ///< Message d'alerte.
    };


    // Constructeur et destructeur
    ILogger();
    virtual ~ILogger();

    // Méthodes publiques
    virtual void write(const CString& message, int level = Info) = 0;

    template<class T> inline ILogger& operator<<(const T& toLog);
    template<class T> inline ILogger& operator<<(const std::list<T>& toLog);
    static CString date();
    static CString time();
};


/**
 * Surcharge de l'opérateur << pour logger des messages.
 *
 * \param toLog Données à inscrire dans le log.
 * \return Référence sur le logger.
 ******************************/

template <class T>
inline ILogger& ILogger::operator<<(const T& toLog)
{
    std::ostringstream stream;
    stream << toLog;
    write(stream.str().c_str());

    return *this;
}


/**
 * Surcharge de l'opérateur << pour logger le contenu d'une liste.
 * Seuls les 20 premiers éléments sont affichés.
 *
 * \param toLog Données à inscrire dans le log.
 * \return Référence sur le logger.
 ******************************/

template <class T>
inline ILogger& ILogger::operator<<(const std::list<T>& toLog)
{
    std::size_t i = 0;
    std::ostringstream stream;

    for (typename std::list<T>::const_iterator it = toLog.begin(); it != toLog.end() && i < 20; ++it, ++i)
    {
        if (i > 0) stream << ", ";
        stream << *it;
    }

    write(stream.str().c_str());

    return *this;
}

} // Namespace Ted

#endif // T_FILE_CORE_ILOGGER_HPP_

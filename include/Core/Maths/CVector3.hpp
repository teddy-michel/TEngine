/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CVector3.hpp
 * \date       2008 Création de la classe CVector3.
 * \date 12/07/2010 Ajout de la fonction VectorAngle.
 * \date 15/12/2010 Modification des constructeurs.
 */

#ifndef T_FILE_MATHS_CVECTOR3_HPP_
#define T_FILE_MATHS_CVECTOR3_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <iostream>
#include <cmath>
#include <limits>

#include "Core/Export.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CVector3
 * \ingroup Core
 * \brief   Classe pour gérer des vecteurs à trois dimensions.
 ******************************/

template <class T>
class T_CORE_API CVector3
{
public:

    // Données publiques
    T X; ///< Coordonnée X.
    T Y; ///< Coordonnée Y.
    T Z; ///< Coordonnée Z.

    // Constructeurs
    inline CVector3();
    CVector3(const CVector2<T>& vect);
    CVector3(const CVector3<T>& vect);
    inline CVector3(const T& x, const T& y, const T& z);

    // Opérateurs
    inline CVector3<T> operator+() const;
    inline CVector3<T> operator-() const;
    inline CVector3<T> operator+(const CVector3<T>& vecteur) const;
    inline CVector3<T> operator-(const CVector3<T>& vecteur) const;
    inline const CVector3<T>& operator+=(const CVector3<T>& vecteur);
    inline const CVector3<T>& operator-=(const CVector3<T>& vecteur);
    inline CVector3<T> operator*(const T& k) const;
    inline CVector3<T> operator/(const T& k) const;
    inline const CVector3<T>& operator*=(const T& k);
    inline const CVector3<T>& operator/=(const T& k);
    inline bool operator==(const CVector3<T>& vecteur) const;
    inline bool operator!=(const CVector3<T>& vecteur) const;
    inline operator T*();
    inline operator const T*() const;

    // Méthodes publiques
    inline void set(const T& x, const T& y, const T& z);
    inline T norm() const;
    inline void normalize();
    inline CVector3<T> getNormalized() const;
    inline CString toString() const;

    // Valeurs prédéfinies
    static const CVector3<short> Origin3S;           ///< Origine avec short.
    static const CVector3<unsigned short> Origin3US; ///< Origine avec unsigned short.
    static const CVector3<int> Origin3I;             ///< Origine avec int.
    static const CVector3<unsigned int> Origin3UI;   ///< Origine avec unsigned int.
    static const CVector3<float> Origin3F;           ///< Origine avec float.
    static const CVector3<double> Origin3D;          ///< Origine avec double.
};


/*-----------------------------------*
 *   Fonctions communes aux vecteurs *
 *-----------------------------------*/

template<class T> inline CVector3<T> operator*(const CVector3<T>& vecteur, const T& k);
template<class T> inline CVector3<T> operator/(const CVector3<T>& vecteur, const T& k);
template<class T> inline CVector3<T> operator*(const T& k, const CVector3<T>& vecteur);
template<class T> inline T VectorDot(const CVector3<T>& v1, const CVector3<T>& v2);
template<class T> inline CVector3<T> VectorCross(const CVector3<T>& v1, const CVector3<T>& v2);
template<class T> inline T VectorAngle(const CVector3<T>& v1, const CVector3<T>& v2);
template<class T> inline std::istream& operator>>(std::istream& stream, CVector3<T>& vecteur);
template<class T> inline std::ostream& operator<<(std::ostream& stream, const CVector3<T>& vecteur);


/*-----------------------------------*
 *   Formats usuels                  *
 *-----------------------------------*/

typedef CVector3<short> TVector3S;           ///< Vecteur de short.
typedef CVector3<unsigned short> TVector3US; ///< Vecteur de short non signés.
typedef CVector3<int> TVector3I;             ///< Vecteur d'entiers.
typedef CVector3<unsigned int> TVector3UI;   ///< Vecteur d'entiers non signés.
typedef CVector3<float> TVector3F;           ///< Vecteur de flottants.
typedef CVector3<double> TVector3D;          ///< Vecteur de doubles.


/*-----------------------------------*
 *   Valeurs prédéfinies             *
 *-----------------------------------*/

const TVector3S  Origin3S(0, 0, 0);
const TVector3US Origin3US(0, 0, 0);
const TVector3I  Origin3I(0, 0, 0);
const TVector3UI Origin3UI(0, 0, 0);
const TVector3F  Origin3F(0.0f, 0.0f, 0.0f);
const TVector3D  Origin3D(0.0f, 0.0f, 0.0f);


/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CVector3<T>::CVector3() :
X (0), Y (0), Z (0)
{ }


template <class T>
inline CVector3<T>::CVector3(const CVector2<T>& vect) :
X (vect.X), Y (vect.Y), Z (0)
{ }


template <class T>
inline CVector3<T>::CVector3(const CVector3<T>& vect) :
X (vect.X), Y (vect.Y), Z (vect.Z)
{ }


/**
 * Constructeur.
 *
 * \param x Coordonnée X.
 * \param y Coordonnée Y.
 * \param z Coordonnée Z.
 ******************************/

template <class T>
inline CVector3<T>::CVector3(const T& x, const T& y, const T& z) :
X (x), Y (y), Z (z)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator+() const
{
    return CVector3<T>(X, Y, Z);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator-() const
{
    return CVector3<T>(-X, -Y, -Z);
}


/**
 * Opération binaire +.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator+(const CVector3<T>& vecteur) const
{
    return CVector3<T>(X + vecteur.X, Y + vecteur.Y, Z + vecteur.Z);
}


/**
 * Opération binaire -.
 *
 * \param vecteur Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator-(const CVector3<T>& vecteur) const
{
    return CVector3<T>(X - vecteur.X, Y - vecteur.Y, Z - vecteur.Z);
}


/**
 * Opération +=.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator+=(const CVector3<T>& vecteur)
{
    X += vecteur.X;
    Y += vecteur.Y;
    Z += vecteur.Z;

    return *this;
}

/**
 * Opération -=.
 *
 * \param vecteur Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator-=(const CVector3<T>& vecteur)
{
    X -= vecteur.X;
    Y -= vecteur.Y;
    Z -= vecteur.Z;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator*(const T& k) const
{
    return CVector3<T>(X * k, Y * k, Z * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::operator/(const T& k) const
{
    return CVector3<T>(X / k, Y / k, Z / k);
}


/**
 * Opération *=.
 *
 * \param  k : Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator*=(const T& k)
{
    X *= k;
    Y *= k;
    Z *= k;

    return *this;
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector3<T>& CVector3<T>::operator/=(const T& k)
{
    X /= k;
    Y /= k;
    Z /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector3<T>::operator==(const CVector3<T>& vecteur) const
{
    return ((std::abs(X - vecteur.X) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(Y - vecteur.Y) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(Z - vecteur.Z) <= std::numeric_limits<T>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector3<T>::operator !=(const CVector3<T>& vecteur) const
{
    return !(*this == vecteur);
}


/**
 * Transtypage en T*.
 ******************************/

template <class T>
inline CVector3<T>::operator T*()
{
    return &X;
}


/**
 * Transtypage en const T*.
 ******************************/

template <class T>
inline CVector3<T>::operator const T*() const
{
    return &X;
}


/**
 * Réinitialise le vecteur.
 *
 * \param x Composante x du vecteur.
 * \param y Composante y du vecteur.
 * \param z Composante z du vecteur.
 ******************************/

template <class T>
inline void CVector3<T>::set(const T& x, const T& y, const T& z)
{
    X = x;
    Y = y;
    Z = z;
}


/**
 * Calcul la norme du vecteur.
 *
 * \return Norme du vecteur.
 ******************************/

template <class T>
inline T CVector3<T>::norm() const
{
    return std::sqrt(X * X + Y * Y + Z * Z);
}


/**
 * Normalise le vecteur.
 ******************************/

template <class T>
inline void CVector3<T>::normalize()
{
    const T n = norm();

    if (n > std::numeric_limits<T>::epsilon())
    {
        X /= n;
        Y /= n;
        Z /= n;
    }
}


/**
 * Retourne le vecteur normalisé.
 *
 * \return Vecteur normalisé.
 ******************************/

template <class T>
inline CVector3<T> CVector3<T>::getNormalized() const
{
    const T n = norm();

    if (n > std::numeric_limits<T>::epsilon())
    {
        return (*this / n);
    }
    else
    {
        return CVector3<T>();
    }
}


/**
 * Retourne une chaine de caractères contenant les composantes du vecteur.
 *
 * \return Chaine de caractère.
 ******************************/

template <class T>
CString CVector3<T>::toString() const
{
    return (CString::fromNumber(X) + " " + CString::fromNumber(Y) + " " + CString::fromNumber(Z));
}


/**
 * Multiplication d'un vecteur par un scalaire.
 *
 * \relates CVector3
 *
 * \param k       Scalaire.
 * \param vecteur Vecteur à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator*(const CVector3<T>& vecteur, const T& k)
{
    return (vecteur * k);
}


/**
 * Division d'un vecteur par un scalaire.
 *
 * \relates CVector3
 *
 * \param k       Scalaire.
 * \param vecteur Vecteur à diviser.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator/(const CVector3<T>& vecteur, const T& k)
{
    return (vecteur / k);
}


/**
 * Multiplication d'un vecteur par un scalaire.
 *
 * \relates CVector3
 *
 * \param k       Scalaire.
 * \param vecteur Vecteur à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> operator*(const T& k, const CVector3<T>& vecteur)
{
    return (vecteur * k);
}


/**
 * Effectue le produit scalaire de deux vecteurs.
 *
 * \relates CVector3
 *
 * \param v1 Vecteur 1.
 * \param v2 Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorDot(const CVector3<T>& v1, const CVector3<T>& v2)
{
    return (v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z);
}


/**
 * Effectue le produit vectoriel de deux vecteurs.
 *
 * \relates CVector3
 *
 * \param v1 Vecteur 1.
 * \param v2 Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> VectorCross(const CVector3<T>& v1, const CVector3<T>& v2)
{
    return CVector3<T>(v1.Y * v2.Z - v1.Z * v2.Y , v1.Z * v2.X - v1.X * v2.Z , v1.X * v2.Y - v1.Y * v2.X);
}


/**
 * Calcule l'angle en radians entre deux vecteurs.
 *
 * \relates CVector3
 *
 * \param v1 Vecteur 1.
 * \param v2 Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorAngle(const CVector3<T>& v1, const CVector3<T>& v2)
{
    T norms = v1.norm() * v2.norm();

    if (norms > std::numeric_limits<T>::epsilon())
    {
        T cosinus = VectorDot(v1, v2) / norms;

        // On rectifie les valeurs du cosinus
        if (cosinus < -1.0f)
            cosinus = -1.0f;
        else if (cosinus > 1.0f)
            cosinus = 1.0f;

        return std::acos(cosinus);
    }

    return 0.0f;
}


/**
 * Surcharge de l'opérateur >> entre un flux et un vecteur.
 *
 * \relates CVector3
 *
 * \param stream  Flux d'entrée.
 * \param vecteur Vecteur.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CVector3<T>& vecteur)
{
    return stream >> vecteur.X >> vecteur.Y >> vecteur.Z;
}


/**
 * Surcharge de l'opérateur << entre un flux et un vecteur.
 *
 * \relates CVector3
 *
 * \param stream  Flux de sortie.
 * \param vecteur Vecteur.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CVector3<T>& vecteur)
{
    return stream << vecteur.X << " " << vecteur.Y << " " << vecteur.Z;
}

} // Namespace Ted

#endif // T_FILE_MATHS_CVECTOR3_HPP_

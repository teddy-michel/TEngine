/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix4.hpp
 * \date       2008 Création de la classe CMatrix4.
 */

#ifndef T_FILE_MATHS_CMATRIX44_HPP_
#define T_FILE_MATHS_CMATRIX44_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/Maths/CVector4.hpp"


namespace Ted
{

/**
 * \class   CMatrix4
 * \ingroup Core
 * \brief   Classe pour gérer des matrices carrées à quatre dimensions.
 ******************************/

template <class T>
class T_CORE_API CMatrix4
{
public:

    // Données
    T AA; ///< Premier nombre de la première ligne.
    T AB; ///< Deuxième nombre de la première ligne.
    T AC; ///< Troisième nombre de la première ligne.
    T AD; ///< Quatrième nombre de la première ligne.
    T BA; ///< Premier nombre de la deuxième ligne.
    T BB; ///< Deuxième nombre de la deuxième ligne.
    T BC; ///< Troisième nombre de la deuxième ligne.
    T BD; ///< Quatrième nombre de la deuxième ligne.
    T CA; ///< Premier nombre de la troisième ligne.
    T CB; ///< Deuxième nombre de la troisième ligne.
    T CC; ///< Troisième nombre de la troisième ligne.
    T CD; ///< Quatrième nombre de la troisième ligne.
    T DA; ///< Premier nombre de la quatrième ligne.
    T DB; ///< Deuxième nombre de la quatrième ligne.
    T DC; ///< Troisième nombre de la quatrième ligne.
    T DD; ///< Quatrième nombre de la quatrième ligne.

    // Constructeurs
    CMatrix4();

    CMatrix4(const T& a_a, const T& a_b, const T& a_c, const T& a_d,
             const T& b_a, const T& b_b, const T& b_c, const T& b_d,
             const T& c_a, const T& c_b, const T& c_c, const T& c_d,
             const T& d_a, const T& d_b, const T& d_c, const T& d_d);

    CMatrix4(const CVector4<T>& A, const CVector4<T>& B, const CVector4<T>& C, const CVector4<T>& D);

    // Opérateurs
    CMatrix4 operator+() const;
    CMatrix4 operator-() const;
    CMatrix4 operator+(const CMatrix4<T>& matrix) const;
    CMatrix4 operator-(const CMatrix4<T>& matrix) const;
    const CMatrix4<T>& operator+=(const CMatrix4<T>& matrix);
    const CMatrix4<T>& operator-=(const CMatrix4<T>& matrix);
    CMatrix4<T> operator*(const T& k) const;
    CMatrix4<T> operator*(const CMatrix4<T>& matrix) const;
    CMatrix4<T> operator/(const T& k) const;
    const CMatrix4<T>& operator*=(const T& k);
    const CMatrix4<T>& operator*=(const CMatrix4<T>& matrix);
    const CMatrix4<T>& operator/=(const T& k);
    bool operator==(const CMatrix4<T>& matrix) const;
    bool operator!=(const CMatrix4<T>& matrix) const;
    T& operator()(std::size_t row, std::size_t col);
    const T& operator()(std::size_t row, std::size_t col) const;
    operator T*();
    operator const T*() const;

    // Méthodes publiques
    void setIdentity();
    void transpose();
    void inverse();
    T getDeterminant() const;
    T getTrace() const;
    void SetTranslation(float x, float y, float z);
    void SetScaling(float x, float y, float z);
    void SetRotationX(float angle);
    void SetRotationY(float angle);
    void SetRotationZ(float angle);
    void SetRotationX(float angle, const TVector3F& centre);
    void SetRotationY(float angle, const TVector3F& centre);
    void SetRotationZ(float angle, const TVector3F& centre);
    TVector3F GetTranslation() const;
    TVector3F Transform(const TVector3F& v, float w = 1.0f) const;
    TVector4F Transform(const TVector4F& v) const;
    void OrthoOffCenter(float left, float top, float right, float bottom);
    void PerspectiveFOV(float fov, float ratio, float near, float far);
    void LookAt(const TVector3F& from, const TVector3F& to, const TVector3F& up = TVector3F(0.0f, 0.0f, 1.0f));

    // Valeurs prédéfinies
    static const CMatrix4<short> Identity4S;           ///< Matrice identité avec des entiers courts.
    static const CMatrix4<unsigned short> Identity4US; ///< Matrice identité avec des entiers courts non signés.
    static const CMatrix4<int> Identity4I;             ///< Matrice identité avec des entiers.
    static const CMatrix4<unsigned int> Identity4UI;   ///< Matrice identité avec des entiers non signés.
    static const CMatrix4<float> Identity4F;           ///< Matrice identité avec des flottants.
    static const CMatrix4<double> Identity4D;          ///< Matrice identité avec des flottants double précision.
};


/*******************************
 *  Fonctions communes aux vecteurs
 ******************************/

template <class T> CMatrix4<T> operator*(const CMatrix4<T>& matrix, float k);
template <class T> CMatrix4<T> operator*(float k , const CMatrix4<T>& matrix);
template <class T> CMatrix4<T> operator/(const CMatrix4<T>& matrix, float k);
template <class T> std::istream& operator>>(std::istream& stream, CMatrix4<T>& matrix);
template <class T> std::ostream& operator<<(std::ostream& stream, const CMatrix4<T>& matrix);


/*******************************
 *  Formats usuels
 ******************************/

typedef CMatrix4<short>          TMatrix4S;  ///< Matrice d'entiers courts.
typedef CMatrix4<unsigned short> TMatrix4US; ///< Matrice d'entiers courts non signés.
typedef CMatrix4<int>            TMatrix4I;  ///< Matrice d'entiers.
typedef CMatrix4<unsigned int>   TMatrix4UI; ///< Matrice d'entiers non signés.
typedef CMatrix4<float>          TMatrix4F;  ///< Matrice de flottants.
typedef CMatrix4<double>         TMatrix4D;  ///< Matrice de flottants double précision.


/*******************************
 *  Valeurs prédéfinies
 ******************************/

const TMatrix4S  Identity4S (1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
const TMatrix4US Identity4US(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
const TMatrix4I  Identity4I (1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
const TMatrix4UI Identity4UI(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
const TMatrix4F  Identity4F (1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
const TMatrix4D  Identity4D (1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

} // Namespace Ted


#include "CMatrix4.inl.hpp"

#endif // T_FILE_MATHS_CMATRIX44_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix3.hpp
 * \date       2008 Création de la classe CMatrix3.
 * \date 07/07/2010 Multiplication par un vecteur.
 */

#ifndef T_FILE_MATHS_CMATRIX3_HPP_
#define T_FILE_MATHS_CMATRIX3_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

template <class T> class CVector3;


/**
 * \class   CMatrix3
 * \ingroup Core
 * \brief   Classe pour gérer des matrices carrées à trois dimensions.
 ******************************/

template <class T>
class T_CORE_API CMatrix3
{
public:

    // Données publiques
    T AA; ///< Premier nombre de la première ligne.
    T AB; ///< Deuxième nombre de la première ligne.
    T AC; ///< Troisième nombre de la première ligne.
    T BA; ///< Première nombre de la deuxième ligne.
    T BB; ///< Deuxième nombre de la deuxième ligne.
    T BC; ///< Troisième nombre de la deuxième ligne.
    T CA; ///< Première nombre de la troisième ligne.
    T CB; ///< Deuxième nombre de la troisième ligne.
    T CC; ///< Troisième nombre de la troisième ligne.

    // Constructeurs
    CMatrix3();

    CMatrix3(const T& a_a, const T& a_b, const T& a_c,
             const T& b_a, const T& b_b, const T& b_c,
             const T& c_a, const T& c_b, const T& c_c);

    CMatrix3(const CVector3<T>& A, const CVector3<T>& B, const CVector3<T>& C);

    // Opérateurs
    CMatrix3<T> operator+() const;
    CMatrix3<T> operator-() const;
    CMatrix3<T> operator+(const CMatrix3<T>& matrix) const;
    CMatrix3<T> operator-(const CMatrix3<T>& matrix) const;
    const CMatrix3<T>& operator+=(const CMatrix3<T>& matrix);
    const CMatrix3<T>& operator-=(const CMatrix3<T>& matrix);
    CMatrix3<T> operator*(const T& k) const;
    CMatrix3<T> operator*(const CMatrix3<T>& matrix) const;
    CVector3<T> operator*(const CVector3<T>& v) const;
    CMatrix3<T> operator/(const T& k) const;
    const CMatrix3<T>& operator*=(const T& k);
    const CMatrix3<T>& operator*=(const CMatrix3<T>& matrix);
    const CMatrix3<T>& operator/=(const T& k);
    bool operator==(const CMatrix3<T>& matrix) const;
    bool operator!=(const CMatrix3<T>& matrix) const;
    T& operator()(std::size_t i, std::size_t j);
    const T& operator()(std::size_t i, std::size_t j) const;
    operator T*();
    operator const T*() const;

    // Méthodes publiques
    void setIdentity();
    void transpose();
    void inverse();
    T getDeterminant() const;
    T getTrace() const;

    // Valeurs prédéfinies
    static const CMatrix3<short> Identity3S;           ///< Matrice identité avec des entiers courts.
    static const CMatrix3<unsigned short> Identity3US; ///< Matrice identité avec des entiers courts non signés.
    static const CMatrix3<int> Identity3I;             ///< Matrice identité avec des entiers.
    static const CMatrix3<unsigned int> Identity3UI;   ///< Matrice identité avec des entiers non signés.
    static const CMatrix3<float> Identity3F;           ///< Matrice identité avec des flottants.
    static const CMatrix3<double> Identity3D;          ///< Matrice identité avec des flottants double précision.
};


/*******************************
 *  Fonctions communes aux vecteurs
 ******************************/

template <class T> CMatrix3<T> operator*(const CMatrix3<T>& matrix, const T& k);
template <class T> CMatrix3<T> operator*(const T& k, const CMatrix3<T>& matrix);
template <class T> CMatrix3<T> operator/(const CMatrix3<T>& matrix, const T& k);
template <class T> std::istream& operator>>(std::istream& stream, CMatrix3<T>& matrix);
template <class T> std::ostream& operator<<(std::ostream& stream, const CMatrix3<T>& matrix);


/*******************************
 *  Formats usuels
 ******************************/

typedef CMatrix3<short> TMatrix3S;           ///< Matrice d'entiers courts.
typedef CMatrix3<unsigned short> TMatrix3US; ///< Matrice d'entiers courts non signés.
typedef CMatrix3<int> TMatrix3I;             ///< Matrice d'entiers.
typedef CMatrix3<unsigned int> TMatrix3UI;   ///< Matrice d'entiers non signés.
typedef CMatrix3<float> TMatrix3F;           ///< Matrice de flottants.
typedef CMatrix3<double> TMatrix3D;          ///< Matrice de flottants double précision.


/*******************************
 *  Valeurs prédéfinies
 ******************************/

const TMatrix3S  Identity3S (1, 0, 0, 0, 1, 0, 0, 0, 1);
const TMatrix3US Identity3US(1, 0, 0, 0, 1, 0, 0, 0, 1);
const TMatrix3I  Identity3I (1, 0, 0, 0, 1, 0, 0, 0, 1);
const TMatrix3UI Identity3UI(1, 0, 0, 0, 1, 0, 0, 0, 1);
const TMatrix3F  Identity3F (1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);
const TMatrix3D  Identity3D (1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);

} // Namespace Ted


#include "Core/Maths/CMatrix3.inl.hpp"

#endif // T_FILE_MATHS_CMATRIX3_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CQuaternion.hpp
 * \date       2008 Création de la classe CQuaternion.
 * \date 12/07/2010 Création d'un quaternion depuis une matrice de rotation.
 *                  Création d'un quaternion depuis un axe de rotation et un angle.
 *                  Ajout d'une méthode pour normaliser un quaternion.
 * \date 15/12/2010 Modification des constructeurs, ajout de la conversion depuis un angle.
 */

#ifndef T_FILE_MATHS_CQUATERNION_HPP_
#define T_FILE_MATHS_CQUATERNION_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/Maths/CMatrix3.hpp"
#include "Core/Maths/CMatrix4.hpp"


namespace Ted
{

/**
 * \class   CQuaternion
 * \ingroup Core
 * \brief   Classe pour gérer les quaternions, utiles pour les rotations.
 ******************************/

class T_CORE_API CQuaternion
{
public:

    // Données publiques
    float A; ///< Premier nombre.
    float B; ///< Deuxième nombre.
    float C; ///< Troisième nombre.
    float D; ///< Quatrième nombre.

    // Constructeur
    CQuaternion();
    CQuaternion(float a, float b, float c, float d);

    // Opérateurs
    CQuaternion operator+() const;
    CQuaternion operator-() const;
    CQuaternion operator+(const CQuaternion& quaternion) const;
    CQuaternion operator-(const CQuaternion& quaternion) const;
    const CQuaternion& operator+=(const CQuaternion& quaternion);
    const CQuaternion& operator-=(const CQuaternion& quaternion);
    CQuaternion operator*(float k) const;
    CQuaternion operator*(const CQuaternion& quaternion) const;
    CQuaternion operator/(float k) const;
    const CQuaternion& operator*=( float k);
    const CQuaternion& operator/=( float k);
    bool operator==(const CQuaternion& quaternion) const;
    bool operator!=(const CQuaternion& quaternion) const;
    operator float*();
    operator const float*() const;

    // Méthodes publiques
    TMatrix4F Matrice() const;
    TMatrix3F toRotationMatrix() const;
    void FromRotationMatrix(const TMatrix3F& matrix);
    void FromAxeRotation(const TVector3F& axis, float angle);
    void FromAngles(const TVector3F& angles);
    CQuaternion Conjugue() const;
    CQuaternion Inverse() const;
    float Norm() const;
    void Normalize();
};

} // Namespace Ted

#endif // T_FILE_MATHS_CQUATERNION_HPP_

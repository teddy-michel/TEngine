/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CVector2.hpp
 * \date       2008 Création de la classe CVector2.
 * \date 15/12/2010 Modification des constructeurs.
 */

#ifndef T_FILE_MATHS_CVECTOR2_HPP_
#define T_FILE_MATHS_CVECTOR2_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <iostream>

#include "Core/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CVector2
 * \ingroup Core
 * \brief   Classe pour gérer des vecteurs à deux dimensions.
 ******************************/

template <class T>
class T_CORE_API CVector2
{
public:

    // Données publiques
    T X; ///< Coordonnée X.
    T Y; ///< Coordonnée Y.

    // Constructeurs
    CVector2();
    CVector2(const T& x, const T& y);

    // Opérateurs
    CVector2<T> operator+() const;
    CVector2<T> operator-() const;
    CVector2<T> operator+(const CVector2<T>& vecteur) const;
    CVector2<T> operator-(const CVector2<T>& vecteur) const;
    const CVector2<T>& operator+=(const CVector2<T>& vecteur);
    const CVector2<T>& operator-=(const CVector2<T>& vecteur);
    CVector2<T> operator*(const T& k) const;
    CVector2<T> operator/(const T& k) const;
    const CVector2<T>& operator*=(const T& k);
    const CVector2<T>& operator/=(const T& k);
    bool operator==(const CVector2<T>& vecteur) const;
    bool operator!=(const CVector2<T>& vecteur) const;
    operator T*();
    operator const T*() const;

    // Méthodes publiques
    void set(const T& x, const T& y);
    T norm() const;
    void normalize();
    inline CString toString() const;

    // Valeurs prédéfinies
    static const CVector2<short> Origin2S;           ///< Origine avec short.
    static const CVector2<unsigned short> Origin2US; ///< Origine avec unsigned short.
    static const CVector2<int> Origin2I;             ///< Origine avec int.
    static const CVector2<unsigned int> Origin2UI;   ///< Origine avec unsigned int.
    static const CVector2<float> Origin2F;           ///< Origine avec float.
    static const CVector2<double> Origin2D;          ///< Origine avec double.
};


/*******************************
 *  Fonctions communes aux vecteurs
 ******************************/

template <class T> CVector2<T> operator*(const CVector2<T>& vecteur, const T& k);
template <class T> CVector2<T> operator/(const CVector2<T>& vecteur, const T& k);
template <class T> CVector2<T> operator*(const T& k, const CVector2<T>& vecteur);
template <class T> T VectorDot(const CVector2<T>& v1, const CVector2<T>& v2);
template <class T> std::istream& operator>>(std::istream& stream, CVector2<T>& vecteur);
template <class T> std::ostream& operator<<(std::ostream& stream, const CVector2<T>& vecteur);


/*******************************
 *  Formats usuels
 ******************************/

typedef CVector2<short> TVector2S;           ///< Vecteur de short.
typedef CVector2<unsigned short> TVector2US; ///< Vecteur de short.
typedef CVector2<int> TVector2I;             ///< Vecteur d'entiers.
typedef CVector2<unsigned int> TVector2UI;   ///< Vecteur d'entiers.
typedef CVector2<float> TVector2F;           ///< Vecteur de flottants.
typedef CVector2<double> TVector2D;          ///< Vecteur de doubles.


/*******************************
 *  Valeurs prédéfinies
 ******************************/

const TVector2S  Origin2S(0, 0);
const TVector2US Origin2US(0, 0);
const TVector2I  Origin2I(0, 0);
const TVector2UI Origin2UI(0, 0);
const TVector2F  Origin2F(0.0f, 0.0f);
const TVector2D  Origin2D(0.0f, 0.0f);


template <class T>
CString CVector2<T>::toString() const
{
    return (CString::fromNumber(X) + " " + CString::fromNumber(Y));
}

} // Namespace Ted


#include "Core/Maths/CVector2.inl.hpp"

#endif // T_FILE_MATHS_CVECTOR2_HPP_

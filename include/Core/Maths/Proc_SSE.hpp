/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/Proc_SSE.hpp
 * \date 24/12/2010 Création des fonctions utilisant les instructions SSE.
 */

#ifndef T_FILE_MATHS_PROC_SSE_HPP_
#define T_FILE_MATHS_PROC_SSE_HPP_


namespace Ted
{

float SSE_Sqrt(float x);
void SSE_SinCos(float x, float * s, float * c);
float SSE_Cos(float x);
void SSE2_SinCos(float x, float * s, float * c);
float SSE2_Cos(float x);

} // Namespace Ted

#endif // T_FILE_MATHS_PROC_SSE_HPP_

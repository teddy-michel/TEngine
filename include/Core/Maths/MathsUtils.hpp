/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/MathsUtils.hpp
 * \date 24/12/2010 Création, déplacement de certaines fonctions depuis le fichier base/Utils.hpp.
 * \date 06/10/2013 Création de la fonction RandUnsignedInt.
 */

#ifndef T_FILE_MATHS_MATHSUTILS_HPP_
#define T_FILE_MATHS_MATHSUTILS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include <stdint.h>


namespace Ted
{

T_CORE_API unsigned int RandInt(int32_t min, int32_t max);
T_CORE_API unsigned int RandUnsignedInt(uint32_t min, uint32_t max);
T_CORE_API float RandFloat(float min, float max);


inline int Modulo(int num, int mod)
{
    num %= mod;
    if (num < 0) num += mod;
    return num;
}


/**
 * Indique si un nombre est une puissance de 2.
 *
 * \param value Entier.
 * \return Booléen.
 ******************************/

inline bool isPowerOfTwo(unsigned int value)
{
    return ((value & (value - 1)) == 0);
}


/**
 * Donne la plus petite puissance de 2 supérieure ou égale à un nombre.
 *
 * \param value Entier.
 * \return Nombre.
 ******************************/

inline unsigned int smallestPowerOfTwoGreaterOrEqual(unsigned int value)
{
    value -= 1;

    value |= value >> 1;
    value |= value >> 2;
    value |= value >> 4;
    value |= value >> 8;
    value |= value >> 16;

    return (value + 1);
}


/**
 * Donne la plus grande puissance de 2 inférieure ou égale à un nombre.
 *
 * \param value Entier.
 * \return Nombre.
 ******************************/

inline unsigned int largestPowerOfTwoLessThanOrEqual(unsigned int value)
{
    if (value >= 0x80000000)
    {
        return 0x80000000;
    }

    return (smallestPowerOfTwoGreaterOrEqual(value + 1) >> 1);
}

} // Namespace Ted

#endif // T_FILE_MATHS_MATHSUTILS_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix4.inl.hpp
 * \date       2008 Création de la classe CMatrix4.
 * \date 12/07/2010 Correction d'un bug dans les opérateurs ().
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CMatrix4<T>::CMatrix4() :
AA (0), AB (0), AC (0), AD (0),
BA (0), BB (0), BC (0), BD (0),
CA (0), CB (0), CC (0), CD (0),
DA (0), DB (0), DC (0), DD (0)
{ }


/**
 * Constructeur à partir de seize nombres.
 *
 * \param a_a Premier nombre de la première ligne.
 * \param a_b Deuxième nombre de la première ligne.
 * \param a_c Troisième nombre de la première ligne.
 * \param a_d Quatrième nombre de la première ligne.
 * \param b_a Premier nombre de la deuxième ligne.
 * \param b_b Deuxième nombre de la deuxième ligne.
 * \param b_c Troisième nombre de la deuxième ligne.
 * \param b_d Quatrième nombre de la deuxième ligne.
 * \param c_a Premier nombre de la troisième ligne.
 * \param c_b Deuxième nombre de la troisième ligne.
 * \param c_c Troisième nombre de la troisième ligne.
 * \param c_d Quatrième nombre de la troisième ligne.
 * \param d_a Premier nombre de la quatrième ligne.
 * \param d_b Deuxième nombre de la quatrième ligne.
 * \param d_c Troisième nombre de la quatrième ligne.
 * \param d_d Quatrième nombre de la quatrième ligne.
 ******************************/

template <class T>
inline CMatrix4<T>::CMatrix4(const T& a_a, const T& a_b, const T& a_c, const T& a_d,
                             const T& b_a, const T& b_b, const T& b_c, const T& b_d,
                             const T& c_a, const T& c_b, const T& c_c, const T& c_d,
                             const T& d_a, const T& d_b, const T& d_c, const T& d_d) :
AA (a_a), AB (a_b), AC (a_c), AD (a_d),
BA (b_a), BB (b_b), BC (b_c), BD (b_d),
CA (c_a), CB (c_b), CC (c_c), CD (c_d),
DA (d_a), DB (d_b), DC (d_c), DD (d_d)
{ }


/**
 * Constructeur à partir de quatre vecteurs.
 *
 * \param A Premier vecteur.
 * \param B Deuxième vecteur.
 * \param C Troisième vecteur.
 * \param D Quatrième vecteur.
 ******************************/

template <class T>
inline CMatrix4<T>::CMatrix4(const CVector4<T>& A, const CVector4<T>& B, const CVector4<T>& C, const CVector4<T>& D) :
AA (A.X), AB (B.X), AC (C.X), AD (D.X),
BA (A.Y), BB (B.Y), BC (C.Y), BD (D.Y),
CA (A.Z), CB (B.Z), CC (C.Z), CD (D.Z),
DA (A.W), DB (B.W), DC (C.W), DD (D.W)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator+() const
{
    return CMatrix4<T>(AA , AB , AC , AD , BA , BB , BC , BD , CA , CB , CC , CD , DA , DB , DC , DD);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator-() const
{
    return CMatrix4<T>(-AA , -AB , -AC , -AD , -BA , -BB , -BC , -BD , -CA , -CB , -CC , -CD , -DA , -DB , -DC , -DD);
}


/**
 * Opération binaire +.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator+(const CMatrix4<T>& matrix) const
{
    return CMatrix4<T>(AA + matrix.AA, AB + matrix.AB, AC + matrix.AC, AD + matrix.AD,
                       BA + matrix.BA, BB + matrix.BB, BC + matrix.BC, BD + matrix.BD,
                       CA + matrix.CA, CB + matrix.CB, CC + matrix.CC, CD + matrix.CD,
                       DA + matrix.DA, DB + matrix.DB, DC + matrix.DC, DD + matrix.DD);
}


/**
 * Opération binaire -.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator-(const CMatrix4<T> &matrix) const
{
    return CMatrix4<T>(AA - matrix.AA, AB - matrix.AB, AC - matrix.AC, AD - matrix.AD,
                       BA - matrix.BA, BB - matrix.BB, BC - matrix.BC, BD - matrix.BD,
                       CA - matrix.CA, CB - matrix.CB, CC - matrix.CC, CD - matrix.CD,
                       DA - matrix.DA, DB - matrix.DB, DC - matrix.DC, DD - matrix.DD);
}


/**
 * Opération +=.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix4<T>& CMatrix4<T>::operator+=(const CMatrix4<T>& matrix)
{
    AA += matrix.AA;
    AB += matrix.AB;
    AC += matrix.AC;
    AD += matrix.AD;
    BA += matrix.BA;
    BB += matrix.BB;
    BC += matrix.BC;
    BD += matrix.BD;
    CA += matrix.CA;
    CB += matrix.CB;
    CC += matrix.CC;
    CD += matrix.CD;
    DA += matrix.DA;
    DB += matrix.DB;
    DC += matrix.DC;
    DD += matrix.DD;

    return *this;
}


/**
 * Opération -=.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix4<T>& CMatrix4<T>::operator-=(const CMatrix4<T>& matrix)
{
    AA -= matrix.AA;
    AB -= matrix.AB;
    AC -= matrix.AC;
    AD -= matrix.AD;
    BA -= matrix.BA;
    BB -= matrix.BB;
    BC -= matrix.BC;
    BD -= matrix.BD;
    CA -= matrix.CA;
    CB -= matrix.CB;
    CC -= matrix.CC;
    CD -= matrix.CD;
    DA -= matrix.DA;
    DB -= matrix.DB;
    DC -= matrix.DC;
    DD -= matrix.DD;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator*(const T& k) const
{
    return CMatrix4<T>(AA * k, AB * k, AC * k, AD * k,
                       BA * k, BB * k, BC * k, BD * k,
                       CA * k, CB * k, CC * k, CD * k,
                       DA * k, DB * k, DC * k, DD * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator/(const T& k) const
{
    return CMatrix4<T>(AA / k, AB / k, AC / k, AD / k,
                       BA / k, BB / k, BC / k, BD / k,
                       CA / k, CB / k, CC / k, CD / k,
                       DA / k, DB / k, DC / k, DD / k);
}


/**
 * Multiplication interne.
 *
 * \param matrix Matrice à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> CMatrix4<T>::operator*(const CMatrix4<T>& matrix) const
{
    return CMatrix4<T>(
        AA * matrix.AA + AB * matrix.BA + AC * matrix.CA + AD * matrix.DA,
        AA * matrix.AB + AB * matrix.BB + AC * matrix.CB + AD * matrix.DB,
        AA * matrix.AC + AB * matrix.BC + AC * matrix.CC + AD * matrix.DC,
        AA * matrix.AD + AB * matrix.BD + AC * matrix.CD + AD * matrix.DD,
        BA * matrix.AA + BB * matrix.BA + BC * matrix.CA + BD * matrix.DA,
        BA * matrix.AB + BB * matrix.BB + BC * matrix.CB + BD * matrix.DA,
        BA * matrix.AC + BB * matrix.BC + BC * matrix.CC + BD * matrix.DA,
        BA * matrix.AD + BB * matrix.BD + BC * matrix.CD + BD * matrix.DA,
        CA * matrix.AA + CB * matrix.BA + CC * matrix.CA + CD * matrix.DA,
        CA * matrix.AB + CB * matrix.BB + CC * matrix.CB + CD * matrix.DA,
        CA * matrix.AC + CB * matrix.BC + CC * matrix.CC + CD * matrix.DA,
        CA * matrix.AD + CB * matrix.BD + CC * matrix.CD + CD * matrix.DA,
        DA * matrix.AA + DB * matrix.BA + DC * matrix.CA + DD * matrix.DA,
        DA * matrix.AB + DB * matrix.BB + DC * matrix.CB + DD * matrix.DA,
        DA * matrix.AC + DB * matrix.BC + DC * matrix.CC + DD * matrix.DA,
        DA * matrix.AD + DB * matrix.BD + DC * matrix.CD + DD * matrix.DA
    );
}


/**
 * Opération *=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix4<T>& CMatrix4<T>::operator*=(const T& k)
{
    AA *= k;
    AB *= k;
    AC *= k;
    AD *= k;
    BA *= k;
    BB *= k;
    BC *= k;
    BD *= k;
    CA *= k;
    CB *= k;
    CC *= k;
    CD *= k;
    DA *= k;
    DB *= k;
    DC *= k;
    DD *= k;

    return *this;
}


/**
 * Opération *=.
 *
 * \param matrix Matrice à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix4<T>& CMatrix4<T>::operator*=(const CMatrix4<T>& matrix)
{
    return (*this * matrix);
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix4<T>& CMatrix4<T>::operator/=(const T& k)
{
    AA /= k;
    AB /= k;
    AC /= k;
    AD /= k;
    BA /= k;
    BB /= k;
    BC /= k;
    BD /= k;
    CA /= k;
    CB /= k;
    CC /= k;
    CD /= k;
    DA /= k;
    DB /= k;
    DC /= k;
    DD /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \param matrix Matrice à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix4<T>::operator==(const CMatrix4<T>& matrix) const
{
    return ((std::abs(AA - matrix.AA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AB - matrix.AB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AC - matrix.AC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AD - matrix.AD) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BA - matrix.BA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BB - matrix.BB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BC - matrix.BC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BD - matrix.BD) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CA - matrix.CA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CB - matrix.CB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CC - matrix.CC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CD - matrix.CD) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(DA - matrix.DA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(DB - matrix.DB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(DC - matrix.DC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(DD - matrix.DD) <= std::numeric_limits<T>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \param matrix Matrice à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix4<T>::operator!=(const CMatrix4<T>& matrix) const
{
    return !(*this == matrix);
}


/**
 * Opérateur de cast en const T*.
 *
 * \return Pointeur constant sur les composantes du vecteur.
 ******************************/

template <class T>
inline CMatrix4<T>::operator const T*() const
{
    return &AA;
}


/**
 * Opérateur de cast en T*.
 *
 * \return Pointeur sur les composantes du vecteur.
 ******************************/

template <class T>
inline CMatrix4<T>::operator T*()
{
    return &AA;
}


/**
 * Opérateur d'accès indexé aux éléments.
 *
 * \param row Ligne de la composante à récupérer (entre 1 et 4).
 * \param col Colonne de la composante à récupérer (entre 1 et 4).
 * \return Référence sur la composante (row, col) de la matrice.
 ******************************/

template <class T>
inline T& CMatrix4<T>::operator()(std::size_t row, std::size_t col)
{
    return operator T*()[4 * (row - 1) + col - 1];
}


/**
 * Opérateur d'accès indexé aux éléments - constant.
 *
 * \param row Ligne de la composante à récupérer.
 * \param col Colonne de la composante à récupérer.
 * \return Référence sur la composante (row, col) de la matrice.
 ******************************/

template <class T>
inline const T& CMatrix4<T>::operator()(std::size_t row, std::size_t col) const
{
    return operator()(row, col);
}


/**
 * Transforme la matrice en identité.
 ******************************/

template <class T>
inline void CMatrix4<T>::setIdentity()
{
    AA = 1;
    AB = 0;
    AC = 0;
    AD = 0;
    BA = 0;
    BB = 1;
    BC = 0;
    BD = 0;
    CA = 0;
    CB = 0;
    CC = 1;
    CD = 0;
    DA = 0;
    DB = 0;
    DC = 0;
    DD = 1;
}


/**
 * Transforme la matrice en sa transposée.
 ******************************/

template <class T>
inline void CMatrix4<T>::transpose()
{
    T a_b = AB;
    T a_c = AC;
    T a_d = AD;
    T b_c = BC;
    T b_d = DB;
    T c_d = CD;

    AB = BA;
    AC = CA;
    AD = DA;
    BA = a_b;
    BC = CB;
    BD = DB;
    CA = a_c;
    CB = b_c;
    CD = DC;
    DA = a_d;
    DB = b_d;
    DC = c_d;
}


/**
 * Transforme la matrice en son inverse.
 ******************************/

template <class T>
inline void CMatrix4<T>::inverse()
{
    T det = getDeterminant();

    if (std::abs(det) <= std::numeric_limits<T>::epsilon())
    {
        return;
    }

    CMatrix4<T> matrix();

    matrix.AA =  (BB * (CC * DD - CD * DC) - CB * (BC * DD - DC * BD) + DB * (BC * CD - CC *  BD)) / det;
    matrix.AB = -(AB * (CC * DD - DC * CD) - CB * (AC * DD - DC * AD) + DB * (AC * CD - CC *  AD)) / det;
    matrix.AC =  (AB * (BC * DD - DC * BD) - BB * (AC * DD - DC * AD) + DB * (AC * BD - BC *  AD)) / det;
    matrix.AD = -(AB * (BC * CD - CC * BD) - BB * (AC * CD - CC * AD) + CB * (AC * BD - BC *  AD)) / det;
    matrix.BA = -(BA * (CC * DD - CD * DC) - BC * (CA * DD - CD * DA) + BD * (CA * DC - CC *  DA)) / det;
    matrix.BB =  (AA * (CC * DD - CD * DC) - AC * (CA * DD - CD * DA) + AD * (CA * DC - CC *  DA)) / det;
    matrix.BC = -(AA * (BC * DD - BD * DC) - AC * (BA * DD - BD * DA) + AD * (BA * DC - BC *  DA)) / det;
    matrix.BD =  (AA * (BC * CD - BD * CC) - AC * (BA * CD - BD * CA) + AD * (BA * CC - BC *  CA)) / det;
    matrix.CA =  (BA * (CB * DD - CD * DB) - BB * (CA * DD - CD * DA) + BD * (CA * DB - CB *  DA)) / det;
    matrix.CB = -(AA * (CB * DD - CD * DB) - AB * (CA * DD - CD * DA) + AD * (CA * DB - CB *  DA)) / det;
    matrix.CC =  (AA * (BB * DD - BD * DB) - AB * (BA * DD - BD * DA) + AD * (BA * DB - BB *  DA)) / det;
    matrix.CD = -(AA * (BB * CD - BD * CB) - AB * (BA * CD - BD * CA) + AD * (BA * CB - BB *  CA)) / det;
    matrix.DA = -(BA * (CB * DC - CC * DB) - BB * (CA * DC - CC * DA) + BC * (CA * DB - CB *  DA)) / det;
    matrix.DB =  (AA * (CB * DC - CC * DB) - AB * (CA * DC - CC * DA) + AC * (CA * DB - CB *  DA)) / det;
    matrix.DC = -(AA * (BB * DC - BC * DB) - AB * (BA * DC - BC * DA) + AC * (BA * DB - BB *  DA)) / det;
    matrix.DD =  (AA * (BB * CC - BC * CB) - AB * (BA * CC - BC * CA) + AC * (BA * CB - BB *  CA)) / det;

    *this = matrix;
}


/**
 * Calcule le déterminant de la matrice.
 *
 * \return Déterminant de la matrice.
 ******************************/

template <class T>
inline T CMatrix4<T>::getDeterminant() const
{
    return (AA * (BB * CC * DD + BC * CD * DB + BD * CB * DC - BB * CD * DC - BC * CB * DD - BD * CC * DB)
          - AB * (BA * CC * DD + BC * CD * DA + BD * CA * DC - BA * CD * DC - BC * CA * DD - BD * CC * DA)
          + AC * (BA * CB * DD + BB * CD * DA + BD * CA * DB - BA * CD * DB - BB * CA * DD - BD * CB * DA)
          - AD * (BA * CB * DC + BB * CC * DA + BC * CA * DB - BA * CC * DB - BB * CA * DC - BC * CB * DA));
}


/**
 * Calcule la trace de la matrice.
 *
 * \return Trace de la matrice.
 ******************************/

template <class T>
inline T CMatrix4<T>::getTrace() const
{
    return (AA + BB + CC + DD);
}


/**
 * Construit une matrice de translation.
 *
 * \param x Translation selon X.
 * \param y Translation selon Y.
 * \param z Translation selon Z.
 ******************************/

template <class T>
inline void CMatrix4<T>::SetTranslation(float x, float y, float z)
{
    AA = 1.0f; AB = 0.0f; AC = 0.0f; AD = x;
    BA = 0.0f; BB = 1.0f; BC = 0.0f; BD = y;
    CA = 0.0f; CB = 0.0f; CC = 1.0f; CD = z;
    DA = 0.0f; DB = 0.0f; DC = 0.0f; DD = 1.0f;
}


/**
 * Construit une matrice de mise à l'échelle.
 *
 * \param x Facteur de redimensionnement selon X.
 * \param y Facteur de redimensionnement selon Y.
 * \param z Facteur de redimensionnement selon Z.
 ******************************/

template <class T>
inline void CMatrix4<T>::SetScaling(float x, float y, float z)
{
    AA = x;    AB = 0.0f; AC = 0.0f; AD = 0.0f;
    BA = 0.0f; BB = y;    BC = 0.0f; BD = 0.0f;
    CA = 0.0f; CB = 0.0f; CC = z;    CD = 0.0f;
    DA = 0.0f; DB = 0.0f; DC = 0.0f; DD = 1.0f;
}


/**
 * Construit une matrice de rotation sur l'axe X.
 *
 * \param angle Angle de la rotation (en radians).
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationX(float angle)
{
    float Cos = std::cos(angle);
    float Sin = std::sin(angle);

    AA = 1.0f; AB = 0.0f; AC = 0.0f; AD = 0.0f;
    BA = 0.0f; BB = Cos;  BC = Sin;  BD = 0.0f;
    CA = 0.0f; CB = -Sin; CC = Cos;  CD = 0.0f;
    DA = 0.0f; DB = 0.0f; DC = 0.0f; DD = 1.0f;
}


/**
 * Construit une matrice de rotation sur l'axe Y.
 *
 * \param angle Angle de la rotation (en radians).
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationY(float angle)
{
    float Cos = std::cos(angle);
    float Sin = std::sin(angle);

    AA = Cos;  AB = 0.0f; AC = -Sin; AD = 0.0f;
    BA = 0.0f; BB = 1.0f; BC = 0.0f; BD = 0.0f;
    CA = Sin;  CB = 0.0f; CC = Cos;  CD = 0.0f;
    DA = 0.0f; DB = 0.0f; DC = 0.0f; DD = 1.0f;
}


/**
 * Construit une matrice de rotation sur l'axe Z.
 *
 * \param angle Angle de la rotation (en radians).
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationZ(float angle)
{
    float Cos = std::cos(angle);
    float Sin = std::sin(angle);

    AA = Cos;  AB = Sin;  AC = 0.0f; AD = 0.0f;
    BA = -Sin; BB = Cos;  BC = 0.0f; BD = 0.0f;
    CA = 0.0f; CB = 0.0f; CC = 1.0f; CD = 0.0f;
    DA = 0.0f; DB = 0.0f; DC = 0.0f; DD = 1.0f;
}


/**
 * Construit une matrice de rotation sur l'axe X.
 *
 * \param angle Angle de la rotation (en radians).
 * \param centre Centre de la rotation.
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationX(float angle, const TVector3F& centre)
{
    CMatrix4<T> Tr1, Tr2, Rot;

    Tr1.SetTranslation(centre.X, centre.Y, centre.Z);
    Tr2.SetTranslation(-centre.X, -centre.Y, -centre.Z);
    Rot.SetRotationX(angle);

    *this = Tr1 * Rot * Tr2;
}

/**
 * Construit une matrice de rotation sur l'axe Y.
 *
 * \param angle Angle de la rotation (en radians).
 * \param centre Centre de la rotation.
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationY(float angle, const TVector3F& centre)
{
    CMatrix4<T> Tr1, Tr2, Rot;

    Tr1.SetTranslation(centre.X, centre.Y, centre.Z);
    Tr2.SetTranslation(-centre.X, -centre.Y, -centre.Z);
    Rot.SetRotationY(angle);

    *this = Tr1 * Rot * Tr2;
}


/**
 * Construit une matrice de rotation sur l'axe Z.
 *
 * \param angle Angle de la rotation (en radians).
 * \param centre Centre de la rotation.
 ******************************/

template <class T>
inline void CMatrix4<T>::SetRotationZ(float angle, const TVector3F& centre)
{
    CMatrix4<T> Tr1, Tr2, Rot;

    Tr1.SetTranslation(centre.X, centre.Y, centre.Z);
    Tr2.SetTranslation(-centre.X, -centre.Y, -centre.Z);
    Rot.SetRotationZ(angle);

    *this = Tr1 * Rot * Tr2;
    return;
}

/**
 * Renvoie la translation contenue dans la matrice.
 *
 * \return Vecteur translation.
 ******************************/

template <class T>
inline TVector3F CMatrix4<T>::GetTranslation() const
{
    return TVector3F(AD, BD, CD);
}


/**
 * Transforme un vecteur à 3 composantes.
 *
 * \param v Vecteur à transformer.
 * \param w Composante W.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline TVector3F CMatrix4<T>::Transform(const TVector3F& v, float w) const
{
    return TVector3F(v.X * AA + v.Y * BA + v.Z * CA + w * DA,
                     v.X * AB + v.Y * BB + v.Z * CB + w * DB,
                     v.X * AC + v.Y * BC + v.Z * CC + w * DC);
}


/**
 * Transforme un vecteur à 4 composantes.
 *
 * \param v Vecteur à transformer.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline TVector4F CMatrix4<T>::Transform(const TVector4F& v) const
{
    return TVector4F(v.X * AA + v.Y * BA + v.Z * CA + v.W * DA,
                     v.X * AB + v.Y * BB + v.Z * CB + v.W * DB,
                     v.X * AC + v.Y * BC + v.Z * CC + v.W * DC,
                     v.X * AD + v.Y * BD + v.Z * CD + v.W * DD);
}

/**
 * Construit une matrice orthogonale non centrée.
 *
 * \param Left   Gauche.
 * \param Top    Haut.
 * \param Right  Droite.
 * \param Bottom Bas.
 ******************************/

template <class T>
inline void CMatrix4<T>::OrthoOffCenter(float Left, float Top, float Right, float Bottom)
{
    AA = 2.0f / (Right - Left);
    AB = 0.0f;
    AC = 0.0f;
    AD = (Left + Right) / (Left - Right);
    BA = 0.0f;
    BB = 2.0f / (Top - Bottom);
    BC = 0.0f;
    BD = (Bottom + Top) / (Bottom - Top);
    CA = 0.0f;
    CB = 0.0f;
    CC = 1.0f;
    CD = 0.0f;
    DA = 0.0f;
    DB = 0.0f;
    DC = 0.0f;
    DD = 1.0f;
}

/**
 * Construit une matrice de projection perspective.
 *
 * \param Fov   Champ de vision en radians.
 * \param Ratio Ratio largeur / hauteur.
 * \param Near  Valeur du plan rapproché.
 * \param Far   Valeur du plan éloigné.
 ******************************/

template <class T>
inline void CMatrix4<T>::PerspectiveFOV(float Fov, float Ratio, float Near, float Far)
{
    float YScale = 1.0f / std::tan(Fov / 2);
    float XScale = YScale / Ratio;
    float Coeff  = Far / (Far - Near);

    AA = XScale;
    AB = 0.0f;
    AC = 0.0f;
    AD = 0.0f;
    BA = 0.0f;
    BB = YScale;
    BC = 0.0f;
    BD = 0.0f;
    CA = 0.0f;
    CB = 0.0f;
    CC = Coeff;
    CD = Near * -Coeff;
    DA = 0.0f;
    DB = 0.0f;
    DC = 1.0f;
    DD = 0.0f;
}


/**
 * Construit une matrice de vue.
 *
 * \todo Gérer le cas où (To - From) et Up sont colinéaires.
 * \param From Position de la caméra.
 * \param To   Cible de la caméra.
 * \param Up   Vecteur up.
 ******************************/

template <class T>
inline void CMatrix4<T>::LookAt(const TVector3F& From, const TVector3F& To, const TVector3F& Up)
{
    TVector3F ZAxis = To - From;
    ZAxis.normalize();
    TVector3F XAxis = VectorCross(Up, ZAxis);
    XAxis.normalize();
    TVector3F YAxis = VectorCross(ZAxis, XAxis);

    AA = XAxis.X; AB = XAxis.Y; AC = XAxis.Z; AD = -VectorDot (XAxis, From);
    BA = YAxis.X; BB = YAxis.Y; BC = YAxis.Z; BD = -VectorDot (YAxis, From);
    CA = ZAxis.X; CB = ZAxis.Y; CC = ZAxis.Z; CD = -VectorDot (ZAxis, From);
    DA = 0.0f;    DB = 0.0f;    DC = 0.0f;    DD = 1.0f;
}


/**
 * Surcharge de l'opérateur * entre une matrice et un scalaire.
 *
 * \relates CMatrix4
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> operator*(const CMatrix4<T>& matrix, float k)
{
    return CMatrix4<T>(matrix.AA * k, matrix.AB * k, matrix.AC * k, matrix.AD * k,
                       matrix.BA * k, matrix.BB * k, matrix.BC * k, matrix.BD * k,
                       matrix.CA * k, matrix.CB * k, matrix.CC * k, matrix.CD * k,
                       matrix.DA * k, matrix.DB * k, matrix.DC * k, matrix.DD * k);
}


/**
 * Surcharge de l'opérateur * entre un scalaire et une matrice.
 *
 * \relates CMatrix4
 *
 * \param k      Scalaire.
 * \param matrix Matrice.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> operator*(const T& k, const CMatrix4<T>& matrix)
{
    return (matrix * k);
}


/**
 * Surcharge de l'opérateur / entre une matrice et un scalaire.
 *
 * \relates CMatrix4
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix4<T> operator/(const CMatrix4<T>& matrix, const T& k)
{
    return CMatrix4<T>(matrix.AA / k, matrix.AB / k, matrix.AC / k, matrix.AD / k,
                       matrix.BA / k, matrix.BB / k, matrix.BC / k, matrix.BD / k,
                       matrix.CA / k, matrix.CB / k, matrix.CC / k, matrix.CD / k,
                       matrix.DA / k, matrix.DB / k, matrix.DC / k, matrix.DD / k);
}


/**
 * Surcharge de l'opérateur >> entre un flux et une matrice.
 *
 * \relates CMatrix4
 *
 * \param stream Flux d'entrée.
 * \param matrix Matrice.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CMatrix4<T>& matrix)
{
    stream >> matrix.AA >> matrix.AB >> matrix.AC >> matrix.AD;
    stream >> matrix.BA >> matrix.BB >> matrix.BC >> matrix.BD;
    stream >> matrix.CA >> matrix.CB >> matrix.CC >> matrix.CD;
    stream >> matrix.DA >> matrix.DB >> matrix.DC >> matrix.DD;

    return stream;
}


/**
 * Surcharge de l'opérateur << entre un flux et une matrice.
 *
 * \relates CMatrix4
 *
 * \param stream Flux de sortie.
 * \param matrix Matrice.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CMatrix4<T>& matrix)
{
    stream << matrix.AA << " " << matrix.AB << " " << matrix.AC << " " << matrix.AD << std::endl;
    stream << matrix.BA << " " << matrix.BB << " " << matrix.BC << " " << matrix.BD << std::endl;
    stream << matrix.CA << " " << matrix.CB << " " << matrix.CC << " " << matrix.CD << std::endl;
    stream << matrix.DA << " " << matrix.DB << " " << matrix.DC << " " << matrix.DD << std::endl;

    return stream;
}

} // Namespace Ted

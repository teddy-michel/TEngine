/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix2.inl.hpp
 * \date       2008 Création de la classe CMatrix2.
 * \date 12/07/2010 Ajout des opérateurs de transtypage et des opérateurs ().
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CMatrix2<T>::CMatrix2() :
AA (0), AB (0),
BA (0), BB (0)
{ }


/**
 * Constructeur à partir de quatre nombres.
 *
 * \param a_a Premier nombre de la première ligne.
 * \param a_b Second nombre de la première ligne.
 * \param b_a Premier nombre de la seconde ligne.
 * \param b_b Second nombre de la seconde ligne.
 ******************************/

template <class T>
inline CMatrix2<T>::CMatrix2(const T& a_a, const T& a_b, const T& b_a, const T& b_b) :
AA (a_a), AB (a_b),
BA (b_a), BB (b_b)
{ }


/**
 * Constructeur à partir de deux vecteurs.
 *
 * \param A Premier vecteur.
 * \param B Second vecteur.
 ******************************/

template <class T>
inline CMatrix2<T>::CMatrix2(const CVector2<T>& A, const CVector2<T>& B) :
AA (A.X), AB (B.X),
BA (A.Y), BB (B.Y)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator+() const
{
    return CMatrix2<T>(AA, AB, BA, BB);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator-() const
{
    return CMatrix2<T>(-AA, -AB, -BA, -BB);
}


/**
 * Opération binaire +.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator+(const CMatrix2<T>& matrix) const
{
    return CMatrix2<T>(AA + matrix.AA, AB + matrix.AB, BA + matrix.BA, BB + matrix.BB);
}


/**
 * Opération binaire -.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator-(const CMatrix2<T>& matrix) const
{
    return CMatrix2<T>(AA - matrix.AA, AB - matrix.AB, BA - matrix.BA, BB - matrix.BB);
}


/**
 * Opération +=.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix2<T>& CMatrix2<T>::operator+=(const CMatrix2<T>& matrix)
{
    AA += matrix.AA;
    AB += matrix.AB;
    BA += matrix.BA;
    BB += matrix.BB;

    return *this;
}


/**
 * Opération -=.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix2<T>& CMatrix2<T>::operator-=(const CMatrix2<T>& matrix)
{
    AA -= matrix.AA;
    AB -= matrix.AB;
    BA -= matrix.BA;
    BB -= matrix.BB;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator*(const T& k) const
{
    return CMatrix2<T>(AA * k, AB * k, BA * k, BB * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator/(const T& k) const
{
    return CMatrix2<T>(AA / k, AB / k, BA / k, BB / k);
}


/**
 * Multiplication interne.
 *
 * \param matrix Matrice avec laquelle on multiplie la matrice courant.
 * \return Résultat de l'opération
 ******************************/

template <class T>
inline CMatrix2<T> CMatrix2<T>::operator*(const CMatrix2<T>& matrix) const
{
    return CMatrix2<T> (
        AA * matrix.AA + AB * matrix.BA,
        AA * matrix.AB + AB * matrix.BB,
        BA * matrix.AA + BB * matrix.BA,
        BA * matrix.AB + BB * matrix.BB
    );
}


/**
 * Opération *=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix2<T>& CMatrix2<T>::operator*=(const T& k)
{
    AA *= k;
    AB *= k;
    BA *= k;
    BB *= k;

    return *this;
}


/**
 * Opération /=.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix2<T>& CMatrix2<T>::operator/=(const T& k)
{
    AA /= k;
    AB /= k;
    BA /= k;
    BB /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix2<T>::operator==(const CMatrix2<T>& matrix) const
{
    return ((std::abs(AA - matrix.AA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AB - matrix.AB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BA - matrix.BA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BB - matrix.BB) <= std::numeric_limits<T>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix2<T>::operator!=(const CMatrix2<T>& matrix) const
{
    return !(*this == matrix);
}


/**
 * Transtypage en const T*.
 *
 * \return Pointeur constant sur les composantes de la matrice.
 ******************************/

template <class T>
inline CMatrix2<T>::operator const T*() const
{
    return &AA;
}


/**
 * Transtypage en T*.
 *
 * \return Pointeur sur les composantes de la matrice.
 ******************************/

template <class T>
inline CMatrix2<T>::operator T*()
{
    return &AA;
}


/**
 * Opérateur d'accès indexé aux éléments.
 *
 * \param i Ligne de la composante à récupérer (1 ou 2).
 * \param j Colonne de la composante à récupérer (1 ou 2).
 * \return Référence sur la composante (i, j) de la matrice.
 ******************************/

template <class T>
inline T& CMatrix2<T>::operator()(std::size_t i, std::size_t j)
{
    return operator T*() [ 2 * (i-1) + j - 1 ];
}


/**
 * Opérateur d'accès indexé aux éléments - constant.
 *
 * \param i Ligne de la composante à récupérer.
 * \param j Colonne de la composante à récupérer.
 * \return Référence sur la composante (i, j) de la matrice.
 ******************************/

template <class T>
inline const T& CMatrix2<T>::operator()(std::size_t i, std::size_t j) const
{
    return operator()(i, j);
}


/**
 * Transforme la matrice en identité.
 ******************************/

template <class T>
inline void CMatrix2<T>::setIdentity()
{
    AA = 1;
    AB = 0;
    BA = 0;
    BB = 1;
}


/**
 * Transforme la matrice en sa transposée.
 ******************************/

template <class T>
inline void CMatrix2<T>::transpose()
{
    T a_b = AB;
    AB = BA;
    BA = a_b;
}


/**
 * Transforme la matrice en son inverse.
 ******************************/

template <class T>
inline void CMatrix2<T>::inverse()
{
    T det = getDeterminant();

    if (std::abs(det) <= std::numeric_limits<T>::epsilon())
    {
        return;
    }

    AA = BB / det;
    AB = - BA / det;
    BA = - AB / det;
    BB = AA / det;
}


/**
 * Calcule le déterminant de la matrice.
 *
 * \return Déterminant de la matrice.
 ******************************/

template <class T>
inline T CMatrix2<T>::getDeterminant() const
{
    return (AA * BB - AB * BA);
}


/**
 * Calcule la trace de la matrice.
 *
 * \return Trace de la matrice.
 ******************************/

template <class T>
inline T CMatrix2<T>::getTrace() const
{
    return (AA + BB);
}


/**
 * Surcharge de l'opérateur * entre une matrice et un scalaire.
 *
 * \relates CMatrix2
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> operator*(const CMatrix2<T>& matrix, const T& k)
{
    return CMatrix2<T>(matrix.AA * k, matrix.AB * k,
                       matrix.BA * k, matrix.BB * k);
}


/**
 * Surcharge de l'opérateur * entre un scalaire et une matrice.
 *
 * \relates CMatrix2
 *
 * \param k      Scalaire.
 * \param matrix Matrice.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> operator*(const T& k, const CMatrix2<T>& matrix)
{
    return (matrix * k);
}


/**
 * Surcharge de l'opérateur / entre une matrice et un scalaire.
 *
 * \relates CMatrix2
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix2<T> operator/(const CMatrix2<T>& matrix, const T& k)
{
    return CMatrix2<T>(matrix.AA / k, matrix.AB / k,
                       matrix.BA / k, matrix.BB / k);
}


/**
 * Surcharge de l'opérateur >> entre un flux et une matrice.
 *
 * \relates CMatrix2
 *
 * \param stream Flux d'entrée.
 * \param matrix Matrice.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CMatrix2<T>& matrix)
{
    stream >> matrix.AA >> matrix.AB;
    stream >> matrix.BA >> matrix.BB;

    return stream;
}


/**
 * Surcharge de l'opérateur << entre un flux et une matrice.
 *
 * \relates CMatrix2
 *
 * \param stream Flux de sortie.
 * \param matrix Matrice.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CMatrix2<T>& matrix)
{
    stream << matrix.AA << " " << matrix.AB << std::endl;
    stream << matrix.BA << " " << matrix.BB << std::endl;

    return stream;
}

} // Namespace Ted

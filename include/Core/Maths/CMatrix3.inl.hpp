/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix3.inl.hpp
 * \date       2008 Création de la classe CMatrix3.
 * \date 07/07/2010 Multiplication par un vecteur.
 * \date 12/07/2010 Correction d'un bug dans l'opérateur ().
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CMatrix3<T>::CMatrix3() :
AA (0), AB (0), AC (0),
BA (0), BB (0), BC (0),
CA (0), CB (0), CC (0)
{ }


/**
 * Constructeur à partir de neuf nombres.
 *
 * \param a_a Premier nombre de la première ligne.
 * \param a_b Deuxième nombre de la première ligne.
 * \param a_c Troisième nombre de la première ligne.
 * \param b_a Premier nombre de la deuxième ligne.
 * \param b_b Deuxième nombre de la deuxième ligne.
 * \param b_c Troisième nombre de la deuxième ligne.
 * \param c_a Premier nombre de la troisième ligne.
 * \param c_b Deuxième nombre de la troisième ligne.
 * \param c_c Troisième nombre de la troisième ligne.
 ******************************/

template <class T>
inline CMatrix3<T>::CMatrix3(const T& a_a, const T& a_b, const T& a_c,
                             const T& b_a, const T& b_b, const T& b_c,
                             const T& c_a, const T& c_b, const T& c_c) :
AA (a_a), AB (a_b), AC (a_c),
BA (b_a), BB (b_b), BC (b_c),
CA (c_a), CB (c_b), CC (c_c)
{ }


/**
 * Constructeur à partir de trois vecteurs.
 *
 * \param A Premier vecteur.
 * \param B Deuxième vecteur.
 * \param C Troisième vecteur.
 ******************************/

template <class T>
inline CMatrix3<T>::CMatrix3(const CVector3<T>& A, const CVector3<T>& B, const CVector3<T>& C) :
AA (A.X), AB (B.X), AC (C.X),
BA (A.Y), BB (B.Y), BC (C.Y),
CA (A.Z), CB (B.Z), CC (C.Z)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator+() const
{
    return CMatrix3<T>(AA, AB, AC, BA, BB, BC, CA, CB, CC);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator-() const
{
    return CMatrix3<T>(-AA, -AB, -AC, -BA, -BB, -BC, -CA, -CB, -CC);
}


/**
 * Opération binaire +.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator+(const CMatrix3<T>& matrix) const
{
    return CMatrix3<T>(AA + matrix.AA, AB + matrix.AB, AC + matrix.AC,
                       BA + matrix.BA, BB + matrix.BB, BC + matrix.BC,
                       CA + matrix.CA, CB + matrix.CB, CC + matrix.CC);
}


/**
 * Opération binaire -.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator-(const CMatrix3<T>& matrix) const
{
    return CMatrix3<T>(AA - matrix.AA, AB - matrix.AB, AC - matrix.AC,
                       BA - matrix.BA, BB - matrix.BB, BC - matrix.BC,
                       CA - matrix.CA, CB - matrix.CB, CC - matrix.CC);
}


/**
 * Opération +=.
 *
 * \param matrix Matrice à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix3<T>& CMatrix3<T>::operator+=(const CMatrix3<T>& matrix)
{
    AA += matrix.AA;
    AB += matrix.AB;
    AC += matrix.AC;
    BA += matrix.BA;
    BB += matrix.BB;
    BC += matrix.BC;
    CA += matrix.CA;
    CB += matrix.CB;
    CC += matrix.CC;

    return *this;
}


/**
 * Opération -=.
 *
 * \param matrix Matrice à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix3<T>& CMatrix3<T>::operator-=(const CMatrix3<T>& matrix)
{
    AA -= matrix.AA;
    AB -= matrix.AB;
    AC -= matrix.AC;
    BA -= matrix.BA;
    BB -= matrix.BB;
    BC -= matrix.BC;
    CA -= matrix.CA;
    CB -= matrix.CB;
    CC -= matrix.CC;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator*(const T& k) const
{
    return CMatrix3<T>(AA * k, AB * k, AC * k, BA * k, BB * k, BC * k, CA * k, CB * k, CC * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator/(const T& k) const
{
    return CMatrix3<T>(AA / k, AB / k, AC / k, BA / k, BB / k, BC / k, CA / k, CB / k, CC / k);
}


/**
 * Multiplication interne.
 *
 * \param matrix Matrice à multiplier.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> CMatrix3<T>::operator*(const CMatrix3<T>& matrix) const
{
    return CMatrix3<T> (
        AA * matrix.AA + AB * matrix.BA + AC * matrix.CA,
        AA * matrix.AB + AB * matrix.BB + AC * matrix.CB,
        AA * matrix.AC + AB * matrix.BC + AC * matrix.CC,
        BA * matrix.AA + BB * matrix.BA + BC * matrix.CA,
        BA * matrix.AB + BB * matrix.BB + BC * matrix.CB,
        BA * matrix.AC + BB * matrix.BC + BC * matrix.CC,
        CA * matrix.AA + CB * matrix.BA + CC * matrix.CA,
        CA * matrix.AB + CB * matrix.BB + CC * matrix.CB,
        CA * matrix.AC + CB * matrix.BC + CC * matrix.CC
    );
}


/**
 * Multiplication par un vecteur.
 *
 * \param v Vecteur.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector3<T> CMatrix3<T>::operator*(const CVector3<T>& v) const
{
    return CVector3<T>(v.X * AA + v.Y * AB + v.Z * AC,
                       v.X * BA + v.Y * BB + v.Z * BC,
                       v.X * CA + v.Y * CB + v.Z * CC);
}


/**
 * Opération *= avec un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix3<T>& CMatrix3<T>::operator*=(const T& k)
{
    AA *= k;
    AB *= k;
    AC *= k;
    BA *= k;
    BB *= k;
    BC *= k;
    CA *= k;
    CB *= k;
    CC *= k;

    return *this;
}


/**
 * Opération *= avec une matrice.
 *
 * \param matrix Autre matrice.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix3<T>& CMatrix3<T>::operator*=(const CMatrix3<T>& matrix)
{
    *this = *this * matrix;
    return *this;
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CMatrix3<T>& CMatrix3<T>::operator/=(const T& k)
{
    AA /= k;
    AB /= k;
    AC /= k;
    BA /= k;
    BB /= k;
    BC /= k;
    CA /= k;
    CB /= k;
    CC /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \param matrix Matrice à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix3<T>::operator==(const CMatrix3<T>& matrix) const
{
    return ((std::abs(AA - matrix.AA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AB - matrix.AB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(AC - matrix.AC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BA - matrix.BA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BB - matrix.BB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(BC - matrix.BC) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CA - matrix.CA) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CB - matrix.CB) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(CC - matrix.CC) <= std::numeric_limits<T>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \param matrix Matrice à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CMatrix3<T>::operator!=(const CMatrix3<T>& matrix) const
{
    return !(*this == matrix);
}


/**
 * Transtypage en const T*.
 *
 * \return Pointeur constant sur les composantes de la matrice.
 ******************************/

template <class T>
inline CMatrix3<T>::operator const T*() const
{
    return &AA;
}


/**
 * Transtypage en T*.
 *
 * \return Pointeur sur les composantes de la matrice.
 ******************************/

template <class T>
inline CMatrix3<T>::operator T*()
{
    return &AA;
}


/**
 * Opérateur d'accès indexé aux éléments.
 *
 * \param row Ligne de la composante à récupérer (1, 2 ou 3).
 * \param col Colonne de la composante à récupérer (1, 2 ou 3).
 * \return Référence sur la composante (i, j) de la matrice.
 ******************************/

template <class T>
inline T& CMatrix3<T>::operator()(std::size_t row, std::size_t col)
{
    return operator T*()[3 * (row - 1) + col - 1];
}


/**
 * Opérateur d'accès indexé aux éléments - constant.
 *
 * \param row Ligne de la composante à récupérer (1, 2 ou 3).
 * \param col Colonne de la composante à récupérer (1, 2 ou 3).
 * \return Référence sur la composante (i, j) de la matrice.
 ******************************/

template <class T>
inline const T& CMatrix3<T>::operator()(std::size_t row, std::size_t col) const
{
    return operator()(row , col);
}


/**
 * Transforme la matrice en identité.
 ******************************/

template <class T>
inline void CMatrix3<T>::setIdentity()
{
    AA = 1;
    AB = 0;
    AC = 0;
    BA = 0;
    BB = 1;
    BC = 0;
    CA = 0;
    CB = 0;
    CC = 1;
}


/**
 * Transforme la matrice en sa transposée.
 ******************************/

template <class T>
inline void CMatrix3<T>::transpose()
{
    T a_b = AB;
    T a_c = AC;
    T b_c = BC;

    AB = BA;
    AC = CA;
    BA = a_b;
    BC = CB;
    CA = a_c;
    CB = b_c;
}


/**
 * Transforme la matrice en son inverse.
 ******************************/

template <class T>
inline void CMatrix3<T>::inverse()
{
    T det = getDeterminant();

    if (std::abs(det) <= std::numeric_limits<T>::epsilon())
    {
        return;
    }

    AA = (BB * CC - BC * CB) / det;
    AB = (AB * CB - AB * CC) / det;
    AC = (AB * BC - AC * BB) / det;
    BA = (BC * CA - BA * CC) / det;
    BB = (AA * CC - AC * CA) / det;
    BC = (AC * BA - AA * BC) / det;
    CA = (BA * CB - BB * CA) / det;
    CB = (AB * CA - AA * CB) / det;
    CC = (AA * BB - AB * BA) / det;
}


/**
 * Calcul le déterminant de la matrice.
 ******************************/

template <class T>
inline T CMatrix3<T>::getDeterminant() const
{
    return (AA * BB * CC + AB * BC * CA + AC * BA * CB - AA * BC * CA - AB * BA * CC - AC * BB * CA);
}


/**
 * Calcul la trace de la matrice.
 ******************************/

template <class T>
inline T CMatrix3<T>::getTrace() const
{
    return (AA + BB + CC);
}


/**
 * Surcharge de l'opérateur * entre une matrice et un scalaire.
 *
 * \relates CMatrix3
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> operator*(const CMatrix3<T>& matrix, const T& k)
{
    return CMatrix3<T>(matrix.AA * k, matrix.AB * k, matrix.AC * k,
                       matrix.BA * k, matrix.BB * k, matrix.BC * k,
                       matrix.CA * k, matrix.CB * k, matrix.CC * k);
}


/**
 * Surcharge de l'opérateur * entre un scalaire et une matrice.
 *
 * \relates CMatrix3
 *
 * \param k      Scalaire.
 * \param matrix Matrice.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> operator*(const T& k, const CMatrix3<T>& matrix)
{
    return (matrix * k);
}


/**
 * Surcharge de l'opérateur / entre une matrice et un scalaire.
 *
 * \relates CMatrix3
 *
 * \param matrix Matrice.
 * \param k      Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CMatrix3<T> operator/(const CMatrix3<T>& matrix, const T& k)
{
    return CMatrix3<T>(matrix.AA / k, matrix.AB / k, matrix.AC / k,
                       matrix.BA / k, matrix.BB / k, matrix.BC / k,
                       matrix.CA / k, matrix.CB / k, matrix.CC / k);
}


/**
 * Surcharge de l'opérateur >> entre un flux et une matrice.
 *
 * \relates CMatrix3
 *
 * \param stream Flux d'entrée.
 * \param matrix Matrice.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CMatrix3<T>& matrix)
{
    stream >> matrix.AA >> matrix.AB >> matrix.AC;
    stream >> matrix.BA >> matrix.BB >> matrix.BC;
    stream >> matrix.CA >> matrix.CB >> matrix.CC;

    return stream;
}


/**
 * Surcharge de l'opérateur << entre un flux et une matrice.
 *
 * \relates CMatrix3
 *
 * \param stream Flux de sortie.
 * \param matrix Matrice.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CMatrix3<T>& matrix)
{
    stream << matrix.AA << " " << matrix.AB << " " << matrix.AC << std::endl;
    stream << matrix.BA << " " << matrix.BB << " " << matrix.BC << std::endl;
    stream << matrix.CA << " " << matrix.CB << " " << matrix.CC << std::endl;

    return stream;
}

} // Namespace Ted

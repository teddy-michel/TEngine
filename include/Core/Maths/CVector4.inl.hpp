/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CVector4.inl.hpp
 * \date       2008 Création de la classe CVector4.
 * \date 15/12/2010 Modification des constructeurs.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CVector4<T>::CVector4() :
X (0) , Y (0) , Z (0) , W (0)
{ }


template <class T>
inline CVector4<T>::CVector4(const CVector2<T>& vect) :
X (vect.X), Y (vect.Y), Z (0), W (0)
{ }


template <class T>
inline CVector4<T>::CVector4(const CVector3<T>& vect) :
X (vect.X), Y (vect.Y), Z (vect.Z), W (0)
{ }


template <class T>
inline CVector4<T>::CVector4(const CVector4<T>& vect) :
X (vect.X), Y (vect.Y), Z (vect.Z), W (vect.W)
{ }


/**
 * Constructeur.
 *
 * \param x Coordonnée X.
 * \param y Coordonnée Y.
 * \param z Coordonnée W.
 * \param w Coordonnée Z.
 ******************************/

template <class T>
inline CVector4<T>::CVector4(const T& x, const T& y, const T& z, const T& w) :
X (x) , Y (y) , Z (z) , W (w)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator+() const
{
    return CVector4<T>(X, Y, Z, W);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator-() const
{
    return CVector4<T>(-X, -Y, -Z, -W);
}


/**
 * Opération binaire +.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator+(const CVector4<T>& vecteur) const
{
    return CVector4<T>(X + vecteur.X, Y + vecteur.Y, Z + vecteur.Z, W + vecteur.W);
}


/**
 * Opération binaire -.
 *
 * \param vecteur Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator-(const CVector4<T>& vecteur) const
{
    return CVector4(X - vecteur.X, Y - vecteur.Y, Z - vecteur.Z, W - vecteur.W);
}


/**
 * Opération +=.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector4<T>& CVector4<T>::operator+=(const CVector4<T>& vecteur)
{
    X += vecteur.X;
    Y += vecteur.Y;
    Z += vecteur.Z;
    W += vecteur.W;

    return *this;
}


/**
 * Opération -=.
 *
 * \param vecteur Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector4<T>& CVector4<T>::operator-=(const CVector4<T>& vecteur)
{
    X -= vecteur.X;
    Y -= vecteur.Y;
    Z -= vecteur.Z;
    W -= vecteur.W;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator*(const T& k) const
{
    return CVector4<T>(X * k, Y * k, Z * k, W * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> CVector4<T>::operator/(const T& k) const
{
    return CVector4<T>(X / k, Y / k, Z / k, W / k);
}


/**
 * Opération *=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector4<T>& CVector4<T>::operator*=(const T& k)
{
    X *= k;
    Y *= k;
    Z *= k;
    W *= k;

    return *this;
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector4<T>& CVector4<T>::operator/=(const T& k)
{
    X /= k;
    Y /= k;
    Z /= k;
    W /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector4<T>::operator==( const CVector4<T>& vecteur) const
{
    return ((std::abs(X - vecteur.X) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(Y - vecteur.Y) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(Z - vecteur.Z) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(W - vecteur.W) <= std::numeric_limits<float>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector4<T>::operator!=(const CVector4<T>& vecteur) const
{
    return !(*this == vecteur);
}


/**
 * Transtypage en T*.
 ******************************/

template <class T>
inline CVector4<T>::operator T*()
{
    return &X;
}


/**
 * Transtypage en const T*.
 ******************************/

template <class T>
inline CVector4<T>::operator const T*() const
{
    return &X;
}


/**
 * Réinitialise le vecteur.
 *
 * \param x Composante x du vecteur.
 * \param y Composante y du vecteur.
 * \param z Composante z du vecteur.
 * \param w Composante w du vecteur.
 ******************************/

template <class T>
inline void CVector4<T>::set(const T& x, const T& y, const T& z, const T& w)
{
    X = x;
    Y = y;
    Z = z;
    W = w;
}


/**
 * Calcul la norme du vecteur.
 *
 * \return Valeur de la norme du vecteur.
 ******************************/

template <class T>
inline T CVector4<T>::norm() const
{
    return sqrt(X*X + Y*Y + Z*Z + W*W);
}


/**
 * Normalise le vecteur.
 ******************************/

template <class T>
inline void CVector4<T>::normalize()
{
    const T n = norm();

    if (std::abs(n) > std::numeric_limits<T>::epsilon())
    {
        X /= n;
        Y /= n;
        Z /= n;
        W /= n;
    }
}


/**
 * Opérateur de multiplication d'un vecteur par un scalaire.
 *
 * \relates CVector4
 *
 * \param vecteur Vecteur.
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> operator*(const CVector4<T>& vecteur, const T& k)
{
    return CVector4<T>(vecteur.X * k, vecteur.Y * k, vecteur.Z * k, vecteur.W * k);
}


/**
 * Opérateur de division d'un vecteur par un scalaire.
 *
 * \relates CVector4
 *
 * \param vecteur Vecteur.
 * \param k       Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> operator/(const CVector4<T>& vecteur, const T& k)
{
    return CVector4<T>(vecteur.X / k, vecteur.Y / k, vecteur.Z / k, vecteur.W / k);
}


/**
 * Opérateur de multiplication d'un scalaire par un vecteur.
 *
 * \relates CVector4
 *
 * \param k       Scalaire.
 * \param vecteur Vecteur.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector4<T> operator*(const T& k, const CVector4<T>& vecteur)
{
    return vecteur * k;
}


/**
 * Effectue le produit scalaire de deux vecteurs.
 *
 * \param v1 Vecteur 1.
 * \param v2 Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorDot(const CVector4<T>& v1, const CVector4<T>& v2)
{
    return (v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z + v1.W * v2.W);
}


/**
 * Surcharge de l'opérateur >> entre un flux et un vecteur.
 *
 * \relates CVector4
 *
 * \param stream  Flux d'entrée.
 * \param vecteur Vecteur.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CVector4<T>& vecteur)
{
    return stream >> vecteur.X >> vecteur.Y >> vecteur.Z >> vecteur.W;
}


/**
 * Surcharge de l'opérateur << entre un flux et un vecteur.
 *
 * \relates CVector4
 *
 * \param stream    Flux de sortie.
 * \param vecteur Vecteur.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CVector4<T>& vecteur)
{
    return stream << vecteur.X << " " << vecteur.Y << " " << vecteur.Z << " " << vecteur.W;
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CMatrix2.hpp
 * \date       2008 Création de la classe CMatrix2.
 * \date 12/07/2010 Ajout des opérateurs de transtypage et des opérateurs ().
 */

#ifndef T_FILE_CORE_MATHS_CMATRIX2_HPP_
#define T_FILE_CORE_MATHS_CMATRIX2_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"
#include "Core/Maths/CVector2.hpp"


namespace Ted
{

/**
 * \class   CMatrix2
 * \ingroup Core
 * \brief   Classe pour gérer des matrices carrées à deux dimensions.
 ******************************/

template <class T>
class T_CORE_API CMatrix2
{
public:

    // Données publiques
    T AA; ///< Premier nombre de la première ligne
    T AB; ///< Second nombre de la première ligne
    T BA; ///< Premier nombre de la deuxième ligne
    T BB; ///< Second nombre de la deuxième ligne

    // Constructeurs
    CMatrix2();
    CMatrix2(const T& a_a, const T& a_b, const T& b_a, const T& b_b);
    CMatrix2(const CVector2<T>& A, const CVector2<T>& B);

    // Opérateurs
    CMatrix2<T> operator+() const;
    CMatrix2<T> operator-() const;
    CMatrix2<T> operator+(const CMatrix2<T>& matrix) const;
    CMatrix2<T> operator-(const CMatrix2<T>& matrix) const;
    const CMatrix2<T>& operator+=(const CMatrix2<T>& matrix);
    const CMatrix2<T>& operator-=(const CMatrix2<T>& matrix);
    CMatrix2<T> operator*(const T& k) const;
    CMatrix2<T> operator*(const CMatrix2<T>& matrix) const;
    CMatrix2<T> operator/(const T& k) const;
    const CMatrix2<T>& operator*=(const T& k);
    const CMatrix2<T>& operator/=(const T& k);
    bool operator==(const CMatrix2<T>& matrix) const;
    bool operator!=(const CMatrix2<T>& matrix) const;
    T& operator()(std::size_t row, std::size_t col);
    const T& operator()(std::size_t row, std::size_t col) const;
    operator T*();
    operator const T*() const;

    // Méthodes publiques
    void setIdentity();
    void transpose();
    void inverse();
    T getDeterminant() const;
    T getTrace() const;

    // Valeurs prédéfinies
    static const CMatrix2<short> Identity3S;           ///< Matrice identité avec des entiers courts.
    static const CMatrix2<unsigned short> Identity3US; ///< Matrice identité avec des entiers courts non signés.
    static const CMatrix2<int> Identity3I;             ///< Matrice identité avec des entiers.
    static const CMatrix2<unsigned int> Identity3UI;   ///< Matrice identité avec des entiers non signés.
    static const CMatrix2<float> Identity3F;           ///< Matrice identité avec des flottants.
    static const CMatrix2<double> Identity3D;          ///< Matrice identité avec des flottants double précision.
};


/*-----------------------------------*
 *   Fonctions communes aux vecteurs *
 *-----------------------------------*/

template <class T> inline CMatrix2<T> operator*(const CMatrix2<T>& matrix, const T& k);
template <class T> inline CMatrix2<T> operator*(const T& k, const CMatrix2<T>& matrix);
template <class T> inline CMatrix2<T> operator/(const CMatrix2<T>& matrix, const T& k);
template <class T> inline std::istream& operator>>(std::istream& stream, CMatrix2<T>& matrix);
template <class T> inline std::ostream& operator<<(std::ostream& stream, const CMatrix2<T>& matrix);


/*-----------------------------------*
 *   Formats usuels                  *
 *-----------------------------------*/

typedef CMatrix2<short> TMatrix2S;           ///< Matrice d'entiers courts.
typedef CMatrix2<unsigned short> TMatrix2US; ///< Matrice d'entiers courts non signés.
typedef CMatrix2<int> TMatrix2I;             ///< Matrice d'entiers.
typedef CMatrix2<unsigned int> TMatrix2UI;   ///< Matrice d'entiers non signés.
typedef CMatrix2<float> TMatrix2F;           ///< Matrice de flottants.
typedef CMatrix2<double> TMatrix2D;          ///< Matrice de flottants double précision.


/*-----------------------------------*
 *   Valeurs prédéfinies             *
 *-----------------------------------*/

const TMatrix2S  Identity2S(1, 0, 0, 1);
const TMatrix2US Identity2US(1, 0, 0, 1);
const TMatrix2I  Identity2I(1, 0, 0, 1);
const TMatrix2UI Identity2UI(1, 0, 0, 1);
const TMatrix2F  Identity2F(1.0f, 0.0f, 0.0f, 1.0f);
const TMatrix2D  Identity2D(1.0f, 0.0f, 0.0f, 1.0f);

} // Namespace Ted


#include "Core/Maths/CMatrix2.inl.hpp"

#endif // T_FILE_CORE_MATHS_CMATRIX2_HPP_

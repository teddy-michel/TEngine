/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CVector2.inl.hpp
 * \date       2008 Création de la classe CVector2.
 * \date 15/12/2010 Modification des constructeurs.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

template <class T>
inline CVector2<T>::CVector2() :
X (0), Y (0)
{ }


/**
 * Constructeur.
 *
 * \param x Coordonnée X.
 * \param y Coordonnée Y.
 ******************************/

template <class T>
inline CVector2<T>::CVector2(const T& x, const T& y) :
X (x), Y (y)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator+() const
{
    return CVector2(X, Y);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator-() const
{
    return CVector2<T>(-X, -Y);
}


/**
 * Opération binaire +.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator+(const CVector2<T>& vecteur) const
{
    return CVector2<T>(X + vecteur.X, Y + vecteur.Y);
}


/**
 * Opération binaire -.
 *
 * \param vecteur Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator-(const CVector2<T>& vecteur) const
{
    return CVector2<T>(X - vecteur.X, Y - vecteur.Y);
}


/**
 * Opération +=.
 *
 * \param vecteur Vecteur à ajouter.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator+=(const CVector2<T>& vecteur)
{
    X += vecteur.X;
    Y += vecteur.Y;

    return *this;
}

/**
 * Opération -=.
 *
 * \param vecteur : Vecteur à soustraire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator-=(const CVector2<T>& vecteur)
{
    X -= vecteur.X;
    Y -= vecteur.Y;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator*(const T& k) const
{
    return CVector2<T>(X * k, Y * k);
}


/**
 * Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> CVector2<T>::operator/(const T& k) const
{
    return CVector2<T>(X / k, Y / k);
}


/**
 * Opération *=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator*=(const T& k)
{
    X *= k;
    Y *= k;

    return *this;
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline const CVector2<T>& CVector2<T>::operator/=(const T& k)
{
    X /= k;
    Y /= k;

    return *this;
}


/**
 * Opérateur de comparaison (==).
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector2<T>::operator==(const CVector2<T>& vecteur) const
{
    return ((std::abs(static_cast<double>(X - vecteur.X)) <= std::numeric_limits<T>::epsilon()) &&
            (std::abs(static_cast<double>(Y - vecteur.Y)) <= std::numeric_limits<T>::epsilon()));
}


/**
 * Opérateur de comparaison (!=).
 *
 * \param vecteur Vecteur à comparer.
 * \return Booléen.
 ******************************/

template <class T>
inline bool CVector2<T>::operator!=(const CVector2<T>& vecteur) const
{
    return !(*this == vecteur);
}


/**
 * Transtypage en T*.
 ******************************/

template <class T>
inline CVector2<T>::operator T*()
{
    return &X;
}


/**
 * Transtypage en const T*.
 ******************************/

template <class T>
inline CVector2<T>::operator const T*() const
{
    return &X;
}


/**
 * Réinitialise le vecteur.
 *
 * \param x Composante x du vecteur.
 * \param y Composante y du vecteur.
 ******************************/

template <class T>
inline void CVector2<T>::set(const T& x, const T& y)
{
    X = x;
    Y = y;
}


/**
 * Calcule la norme du vecteur.
 *
 * \return Norme du vecteur.
 ******************************/

template <class T>
inline T CVector2<T>::norm() const
{
    return sqrt(X*X + Y*Y);
}


/**
 * Normalise le vecteur.
 ******************************/

template <class T>
inline void CVector2<T>::normalize()
{
    const T n = norm();

    if (std::abs(n) > std::numeric_limits<T>::epsilon())
    {
        X /= n;
        Y /= n;
    }
}


/**
 * Opérateur de multiplication avec un scalaire.
 *
 * \relates CVector2
 *
 * \param vecteur Vecteur.
 * \param k       Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator*(const CVector2<T>& vecteur, const T& k)
{
    return CVector2<T>(vecteur.X * k, vecteur.Y * k);
}


/**
 * Opérateurs de division par un scalaire.
 *
 * \relates CVector2
 *
 * \param vecteur Vecteur.
 * \param k       Scalaire.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator/(const CVector2<T>& vecteur, const T& k)
{
    return CVector2<T>(vecteur.X / k, vecteur.Y / k);
}


/**
 * Opérateur de multiplication avec un scalaire.
 *
 * \relates CVector2
 *
 * \param k       Scalaire.
 * \param vecteur Vecteur.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline CVector2<T> operator*(const T& k, const CVector2<T>& vecteur)
{
    return vecteur * k;
}


/**
 * Effectue le produit scalaire de deux vecteurs.
 *
 * \relates CVector2
 *
 * \param v1 Vecteur 1.
 * \param v2 Vecteur 2.
 * \return Résultat de l'opération.
 ******************************/

template <class T>
inline T VectorDot(const CVector2<T>& v1, const CVector2<T>& v2)
{
    return (v1.X * v2.X + v1.Y * v2.Y);
}


/**
 * Surcharge de l'opérateur >> entre un flux et un vecteur.
 *
 * \relates CVector2
 *
 * \param stream  Flux d'entrée.
 * \param vecteur Vecteur.
 * \return Référence sur le flux d'entrée.
 ******************************/

template <class T>
inline std::istream& operator>>(std::istream& stream, CVector2<T>& vecteur)
{
    return stream >> vecteur.X >> vecteur.Y;
}


/**
 * Surcharge de l'opérateur << entre un flux et un vecteur.
 *
 * \relates CVector2
 *
 * \param stream  Flux de sortie.
 * \param vecteur Vecteur.
 * \return Référence sur le flux de sortie.
 ******************************/

template <class T>
inline std::ostream& operator<<(std::ostream& stream, const CVector2<T>& vecteur)
{
    return stream << vecteur.X << " " << vecteur.Y;
}

} // Namespace Ted

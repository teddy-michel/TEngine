/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CVector4.hpp
 * \date       2008 Création de la classe CVector4.
 * \date 15/12/2010 Modification des constructeurs.
 */

#ifndef T_FILE_MATHS_CVECTOR4_HPP_
#define T_FILE_MATHS_CVECTOR4_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <iostream>

#include "Core/Export.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CVector4
 * \ingroup Core
 * \brief   Classe pour gérer des vecteurs à quatre dimensions.
 ******************************/

template <class T>
class T_CORE_API CVector4
{
public:

    // Données publiques
    T X; ///< Coordonnée X.
    T Y; ///< Coordonnée Y.
    T Z; ///< Coordonnée Z.
    T W; ///< Coordonnée W.

    // Constructeurs
    CVector4();
    CVector4(const CVector2<T>& vect);
    CVector4(const CVector3<T>& vect);
    CVector4(const CVector4<T>& vect);
    CVector4(const T& x, const T& y, const T& z, const T& w);

    // Opérateurs
    CVector4<T> operator+() const;
    CVector4<T> operator-() const;
    CVector4<T> operator+(const CVector4<T>& vecteur) const;
    CVector4<T> operator-(const CVector4<T>& vecteur) const;
    const CVector4<T>& operator+=(const CVector4<T>& vecteur);
    const CVector4<T>& operator-=(const CVector4<T>& vecteur);
    CVector4<T> operator*(const T& k) const;
    CVector4<T> operator/(const T& k) const;
    const CVector4<T>& operator*=(const T& k);
    const CVector4<T>& operator/=(const T& k);
    bool operator==(const CVector4<T>& vecteur) const;
    bool operator!=(const CVector4<T>& vecteur) const;
    operator T*();
    operator const T*() const;

    // Méthodes publiques
    void set(const T& x, const T& y, const T& z, const T& w);
    T norm() const;
    void normalize();
    inline CString toString() const;

    // Valeurs prédéfinies
    static const CVector4<short> Origin4S;           ///< Origine avec short.
    static const CVector4<unsigned short> Origin4US; ///< Origine avec unsigned short.
    static const CVector4<int> Origin4I;             ///< Origine avec int.
    static const CVector4<unsigned int> Origin4UI;   ///< Origine avec unsigned int.
    static const CVector4<float> Origin4F;           ///< Origine avec float.
    static const CVector4<double> Origin4D;          ///< Origine avec double.
};


/*-----------------------------------*
 *   Fonctions communes aux vecteurs *
 *-----------------------------------*/

template <class T> CVector4<T> operator*(const CVector4<T>& vecteur, const T& k);
template <class T> CVector4<T> operator/(const CVector4<T>& vecteur, const T& k);
template <class T> CVector4<T> operator*(const T& k, const CVector4<T>& vecteur);
template <class T> T VectorDot(const CVector4<T>& v1, const CVector4<T>& v2);
template <class T> std::istream& operator>>(std::istream& stream, CVector4<T>& vecteur);
template <class T> std::ostream& operator<<(std::ostream& stream, const CVector4<T>& vecteur);


/*-------------------------------*
 *   Formats usuels              *
 *-------------------------------*/

typedef CVector4<short> TVector4S;           ///< Vecteur d'entiers.
typedef CVector4<unsigned short> TVector4US; ///< Vecteur d'entiers.
typedef CVector4<int> TVector4I;             ///< Vecteur d'entiers.
typedef CVector4<unsigned int> TVector4UI;   ///< Vecteur d'entiers.
typedef CVector4<float> TVector4F;           ///< Vecteur de flottants.
typedef CVector4<double> TVector4D;          ///< Vecteur de doubles.


/*-------------------------------*
 *   Valeurs prédéfinies         *
 *-------------------------------*/

const TVector4S  Origin4S(0, 0, 0, 0);
const TVector4US Origin4US(0, 0, 0, 0);
const TVector4I  Origin4I(0, 0, 0, 0);
const TVector4UI Origin4UI(0, 0, 0, 0);
const TVector4F  Origin4F(0.0f, 0.0f, 0.0f, 0.0f);
const TVector4D  Origin4D(0.0f, 0.0f, 0.0f, 0.0f);


template <class T>
CString CVector4<T>::toString() const
{
    return (CString::fromNumber(X) + " " + CString::fromNumber(Y) + " " + CString::fromNumber(Z) + " " + CString::fromNumber(W));
}

} // Namespace Ted


#include "Core/Maths/CVector4.inl.hpp"

#endif // T_FILE_MATHS_CVECTOR4_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CRectangle.hpp
 * \date 22/07/2010 Création de la classe CRectangle.
 */

#ifndef T_FILE_MATHS_CRECTANGLE_HPP_
#define T_FILE_MATHS_CRECTANGLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"


namespace Ted
{

/**
 * \class   CRectangle
 * \ingroup Core
 * \brief   Classe pour gérer des rectangles.
 ******************************/

class T_CORE_API CRectangle
{
public:

    // Constructeurs
    inline CRectangle();
    inline CRectangle(int x, int y);
    inline CRectangle(int x, int y, int width, int height);

    // Accesseurs
    inline int getX() const;
    inline int getY() const;
    inline int getWidth() const;
    inline int getHeight() const;

    // Mutateurs
    inline void setX(int x);
    inline void setY(int y);
    inline void setWidth(int width);
    inline void setHeight(int height);

    inline bool isEmpty() const;

    /// Opérateur de comparaison (==).
    inline bool operator==(const CRectangle& rec)
    {
        return (rec.m_x == m_x && rec.m_y == m_y && rec.m_width == m_width && rec.m_height == m_height);
    }

    /// Opérateur de comparaison (!=).
    inline bool operator!=(const CRectangle& rec)
    {
        return !operator==(rec);
    }

private:

    // Attributs privées
    int m_x;      ///< Coordonnée horizontale.
    int m_y;      ///< Coordonnée verticale.
    int m_width;  ///< Largeur.
    int m_height; ///< Hauteur.
};


/// Constructeur par défaut.
inline CRectangle::CRectangle() :
m_x      (0),
m_y      (0),
m_width  (0),
m_height (0)
{

}


/// Constructeur.
inline CRectangle::CRectangle(int x, int y) :
m_x      (x),
m_y      (y),
m_width  (0),
m_height (0)
{

}


/// Constructeur
inline CRectangle::CRectangle(int x, int y, int width, int height) :
m_x      (x),
m_y      (y),
m_width  (width  < 0 ? 0 : width),
m_height (height < 0 ? 0 : height)
{

}


inline int CRectangle::getX() const
{
    return m_x;
}


inline int CRectangle::getY() const
{
    return m_y;
}


inline int CRectangle::getWidth() const
{
    return m_width;
}


inline int CRectangle::getHeight() const
{
    return m_height;
}


inline void CRectangle::setX(int x)
{
    m_x = x;
}


inline void CRectangle::setY(int y)
{
    m_y = y;
}


inline void CRectangle::setWidth(int width)
{
    m_width = (width < 0 ? 0 : width);
}


inline void CRectangle::setHeight(int height)
{
    m_height = (height < 0 ? 0 : height);
}


inline bool CRectangle::isEmpty() const
{
    return (m_width <= 0 || m_height <= 0);
}

} // Namespace Ted

#endif // T_FILE_MATHS_CRECTANGLE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CTreeNode.hpp
 * \date 26/02/2010 Création de la classe CTreeNode.
 */

#ifndef T_FILE_CORE_CTREENODE_HPP_
#define T_FILE_CORE_CTREENODE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Core/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   CTreeNode
 * \ingroup Core
 * \brief   Nœud d'un arbre.
 *
 * Un nœud possède un certain nombre d'enfants, et un texte. S'il possède au moins un
 * enfant, le nœud peut être développé.
 ******************************/

class T_CORE_API CTreeNode
{
public:

    // Constructeur et destructeur
    CTreeNode(const CString& text = CString());
    ~CTreeNode();

    // Accesseurs
    CString getText() const;
    bool isExpanded() const;
    bool isExpandable() const;
    unsigned int getNumChildren() const;

    // Mutateurs
    void setText(const CString& text);
    void setExpanded(bool expanded = true);
    void setExpandable(bool expandable = true);
    void AddChild(CTreeNode * child);
    void AddChild(const CString& text);
    void RemoveChild(CTreeNode * child);
    void ClearChildren();

protected:

    // Données protégées
    CString m_text;                    ///< Texte du nœud. Transformer en un type quelconque ?
    bool m_expanded;                   ///< Indique si les éléments enfants sont visibles.
    bool m_expandable;                 ///< Indique si le nœud peut être développé.
    std::list<CTreeNode *> m_children; ///< Liste des enfants.
};

} // Namespace Ted

#endif // T_FILE_CORE_CTREENODE_HPP_

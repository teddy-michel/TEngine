/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CCircularBuffer.hpp
 * \date 28/03/2013 Création de la classe CCircularBuffer.
 * \date 03/04/2013 Utilisation de la COW.
 * \date 13/06/2014 Correction d'un bug avec la lecture des valeurs.
 */

#ifndef T_FILE_CORE_CCIRCULARBUFFER_HPP_
#define T_FILE_CORE_CCIRCULARBUFFER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cstring>


/**
 * \struct  CCircularBuffer
 * \ingroup Core
 * \brief   Buffer circulaire dont tous les éléments sont initialisés.
 ******************************/

template<class T>
class CCircularBuffer
{
public:

    template <class C>
    class iterator_base : public std::iterator<std::bidirectional_iterator_tag, C>
    {
        friend class CCircularBuffer;

    public:

        template<class D>
        iterator_base(const iterator_base<D>& other) :
        m_ptr(other.m_ptr), m_buffer(other.m_buffer)
        {

        }

        iterator_base& operator=(const iterator_base& other)
        {
            m_ptr = other.m_ptr;
            m_buffer = other.m_buffer;
            return *this;
        }

        iterator_base& operator++()
        {
            if (m_ptr == m_buffer->m_data->m_end)
                m_ptr = m_buffer->m_data->m_start;
            else
                ++m_ptr;

            return *this;
        }

        iterator_base operator++(int)
        {
            iterator_base old(*this);
            ++(*this);
            return old;
        }

        iterator_base& operator--()
        {
            if (m_ptr == m_buffer->m_data->m_start)
                m_ptr = m_buffer->m_data->m_end;
            else
                --m_ptr;

            return *this;
        }

        iterator_base operator--(int)
        {
            iterator_base old(*this);
            --(*this);
            return old;
        }

        template<class D>
        bool operator==(const iterator_base<D>& it) const
        {
            return (it.m_buffer == m_buffer && it.m_ptr == m_ptr);
        }

        template<class D>
        bool operator!=(const iterator_base<D>& it) const
        {
            return !operator==(it);
        }

        C& operator*()
        {
            return *m_ptr;
        }

        const C& operator*() const
        {
            return *m_ptr;
        }

        C * operator->()
        {
            return m_ptr;
        }

        const C * operator->() const
        {
            return m_ptr;
        }

    private:

        iterator_base(C * current, const CCircularBuffer * buffer) :
        m_ptr(current), m_buffer(buffer)
        {

        }

        C * m_ptr;
        const CCircularBuffer * m_buffer;
    };


    typedef iterator_base<T> iterator;
    typedef iterator_base<T const> const_iterator;
    typedef iterator reverse_iterator;
    typedef const_iterator const_reverse_iterator;


    CCircularBuffer(std::size_t s, const T& defValue = T())
    {
        m_data = new TData;

        m_data->m_ref = 1;
        m_data->m_start = new T[s + 1];
        m_data->m_end = m_data->m_start + s;
        m_data->m_current = m_data->m_start;

        for (T * ptr = m_data->m_start; ptr <= m_data->m_end; ++ptr)
        {
            *ptr = defValue;
        }
    }


    CCircularBuffer(const CCircularBuffer<T>& other) : m_data(other.m_data)
    {
        ++(m_data->m_ref);
    }


    /**
     * Opérateur d'affectation.
     *
     * \param other Buffer circulaire à copier.
     * \return Référence sur le buffer circulaire.
     ******************************/

    CCircularBuffer& operator=(const CCircularBuffer<T>& other)
    {
        if (--(m_data->m_ref) == 0)
            delete m_data;

        m_data = other.m_data;
        ++(m_data->m_ref);
        return *this;
    }


    /**
     * Supprime le buffer.
     ******************************/

    ~CCircularBuffer()
    {
        if (--(m_data->m_ref) == 0)
        {
            delete m_data;
        }
    }


    /**
     * Efface le contenu du buffer.
     *
     * \param defValue Valeur par défaut de tous les éléments.
      ******************************/

    void clear(const T& defValue = T())
    {
        if (m_data->m_ref > 1)
        {
            std::size_t s = size();

            TData * data = new TData;

            data->m_ref = 1;
            data->m_start = new T[s + 1];
            data->m_end = data->m_start + s;

            for (T * ptr = data->m_start; ptr <= data->m_end; ++ptr)
            {
                *ptr = defValue;
            }

            if (--(m_data->m_ref) == 0)
                delete m_data;

            m_data = data;
        }
        else
        {
            for (T * ptr = m_data->m_start; ptr <= m_data->m_end; ++ptr)
            {
                *ptr = defValue;
            }
        }

        m_data->m_current = m_data->m_start;
    }


    /**
     * Retourne le nombre d'élément pouvant être stockés dans le buffer.
     ******************************/

    std::size_t size() const
    {
        return (m_data->m_end - m_data->m_start);
    }


    /**
     * Redimensionne le buffer circulaire.
     * Si \c newSize est inférieure à la taille actuelle, les éléments en trop sont enlevés.
     * Si \c newSize est supérieure à la taille actuelle, les éléments supplémentaires sont
     * initialisés à la valeur par défaut.
     *
     * \param newSize  Nouvelle taille du buffer.
     * \param defValue Valeur par défaut des nouveaux éléments.
     ******************************/

    void resize(std::size_t newSize, const T& defValue = T())
    {
        std::size_t oldSize = size();

        if (oldSize == newSize)
            return;

        linearize();

        T * newStart = new T[newSize + 1];

        if (oldSize < newSize)
        {
            std::size_t i = 0;

            for (; i <= oldSize; ++i)
            {
                newStart[i] = m_data->m_start[i];
            }

            for (; i <= newSize; ++i)
            {
                newStart[i] = defValue;
            }
        }
        else if (oldSize > newSize)
        {
            for (std::size_t i = 0; i <= newSize; ++i)
            {
                newStart[i] = m_data->m_start[i];
            }
        }

        if (m_data->m_ref > 1)
        {
            TData * data = new TData;
            data->m_ref = 1;

            if (--(m_data->m_ref) == 0)
                delete m_data;

            m_data = data;
        }
        else
        {
            delete[] m_data->m_start;
        }

        m_data->m_start = newStart;
        m_data->m_end = newStart + newSize;
        m_data->m_current = m_data->m_start;
    }


    void append(const T& item)
    {
        copyData();

        *m_data->m_current = item;

        if (m_data->m_current == m_data->m_end)
            m_data->m_current = m_data->m_start;
        else
            ++(m_data->m_current);
    }


    T * linearize()
    {
        if (m_data->m_current != m_data->m_start)
        {
            std::size_t offset = m_data->m_current - m_data->m_start;

            if (m_data->m_ref > 1)
            {
                std::size_t s = size();

                TData * data = new TData;

                data->m_ref = 1;
                data->m_start = new T[s + 1];
                data->m_end = data->m_start + s;

                std::size_t i = 0;

                for (; i < offset; ++i)
                {
                    *(data->m_end - offset + i + 1) = m_data->m_start[i];
                }

                // On décale les éléments suivants vers le début
                for (; i < s + 1; ++i)
                {
                    data->m_start[i - offset] = m_data[i];
                }

                if (--(m_data->m_ref) == 0)
                    delete m_data;

                m_data = data;
            }
            else
            {
                // On mémorise les premiers éléments du buffer
                T * tmp = new T[offset];

                for (std::size_t i = 0; i < offset; ++i)
                {
                    tmp[i] = m_data->m_start[i];
                }

                // On décale les éléments suivants vers le début
                for (T * ptr = m_data->m_start; ptr <= m_data->m_end - offset; ++ptr)
                {
                    *ptr = *(ptr + offset);
                }

                // On place les éléments mémorisés à la fin du buffer
                for (std::size_t i = 0; i < offset; ++i)
                {
                    *(m_data->m_end - offset + i + 1) = tmp[i];
                }

                delete[] tmp;
            }

            m_data->m_current = m_data->m_start;
        }

        return m_data->m_start;
    }


    bool isLinearized() const
    {
        return (m_data->m_current == m_data->m_start);
    }


    void copyData()
    {
        if (m_data->m_ref > 1)
        {
            std::size_t s = size();
            std::size_t d = m_data->m_current - m_data->m_start; // (current >= start) => (d >= 0)

            TData * data = new TData;

            data->m_ref = 1;
            data->m_start = new T[s + 1];
            data->m_end = data->m_start + s;
            data->m_current = data->m_start + d;

            memcpy(data->m_start, m_data->m_start, s * sizeof(T));

            if (--(m_data->m_ref) == 0)
                delete m_data;

            m_data = data;
        }
    }


    iterator begin()
    {
        copyData();

        if (m_data->m_current == m_data->m_end)
            return iterator(m_data->m_start, this);
        else
            return iterator(m_data->m_current + 1, this);
    }


    iterator end()
    {
        copyData();

        return iterator(m_data->m_current, this);
    }


    const_iterator begin() const
    {
        if (m_data->m_current == m_data->m_end)
            return const_iterator(m_data->m_start, this);
        else
            return const_iterator(m_data->m_current + 1, this);
    }


    const_iterator cbegin() const
    {
        return begin();
    }


    const_iterator end() const
    {
        return const_iterator(m_data->m_current, this);
    }


    const_iterator cend() const
    {
        return end();
    }


    reverse_iterator rbegin()
    {
        copyData();

        if (m_data->m_current == m_data->m_start)
            return reverse_iterator(m_data->m_end, this);
        else
            return reverse_iterator(m_data->m_current - 1, this);
    }


    reverse_iterator rend()
    {
        copyData();

        return reverse_iterator(m_data->m_current, this);
    }


    const_reverse_iterator rbegin() const
    {
        if (m_data->m_current == m_data->m_start)
            return const_reverse_iterator(m_data->m_end, this);
        else
            return const_reverse_iterator(m_data->m_current - 1, this);
    }


    const_reverse_iterator crbegin() const
    {
        return rbegin();
    }


    const_reverse_iterator rend() const
    {
        return const_reverse_iterator(m_data->m_current, this);
    }


    const_reverse_iterator crend() const
    {
        return rend();
    }


    T& operator[](std::size_t index)
    {
        copyData();

        index %= size();
        T * tmp = m_data->m_current + index + 1;

        if (tmp > m_data->m_end)
            return *(tmp - size() - 1);
        else
            return *(tmp);
    }


    const T& operator[](std::size_t index) const
    {
        index %= size();
        T * tmp = m_data->m_current + index;

        if (tmp + 1 > m_data->m_end)
            return *(tmp - size() - 1);
        else
            return *(tmp);
    }


    /**
     * Retourne un élément stocké dans le buffer.
     *
     * \param index Indice de l'élément à récupérer.
     * \return Référence constante sur l'élément.
     ******************************/

    const T& at(std::size_t index) const
    {
        return operator[](index);
    }

private:

    struct TData
    {
        /**
         *             m_current   ---->
         *                 v
         *   +---+---+---+---+---+---+---+---+---+
         *   | 5 | 6 | 7 | ? | 0 | 1 | 2 | 3 | 4 |
         *   +---+---+---+---+---+---+---+---+---+
         *     ^                               ^
         *  m_start                          m_end
         *
         **/

        int m_ref;     ///< Compteur de référence.
        T * m_start;   ///< Pointeur sur le début de la zone allouée.
        T * m_end;     ///< Pointeur sur la fin de la zone allouée.
        T * m_current; ///< Pointeur sur le prochain élément à modifier.

        ~TData()
        {
            delete[] m_start;
        }
    };

    TData * m_data; ///< Pointeur sur le contenu partagé du buffer.
};

#endif // T_FILE_CORE_CCIRCULARBUFFER_HPP_

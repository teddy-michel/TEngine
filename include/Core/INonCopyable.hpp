/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/INonCopyable.hpp
 * \date 31/01/2011 Déplacement de la classe INonCopyable depuis le fichier ISingleton.hpp.
 */

#ifndef T_FILE_CORE_INONCOPYABLE_HPP_
#define T_FILE_CORE_INONCOPYABLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"


namespace Ted
{

/**
 * \class   INonCopyable
 * \ingroup Core
 * \brief   Classe de base pour empêcher la copie d'une instance.
 ******************************/

class T_CORE_API INonCopyable
{
public:

    inline INonCopyable() { };
    inline virtual ~INonCopyable() { };

private:

    INonCopyable(const INonCopyable&); // Copy constructor declaration only
    INonCopyable& operator=(const INonCopyable&); // Assignment operator declaration only
};

} // Namespace Ted

#endif // T_FILE_CORE_INONCOPYABLE_HPP_

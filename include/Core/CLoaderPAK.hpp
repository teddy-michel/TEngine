/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderPAK.hpp
 * \date 30/12/2009 Création de la classe CLoaderPAK.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */

#ifndef T_FILE_CORE_CLOADERPAK_HPP_
#define T_FILE_CORE_CLOADERPAK_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/ILoaderArchive.hpp"


namespace Ted
{

/**
 * \class   CLoaderPAK
 * \ingroup Core
 * \brief   Chargement des archives PAK de Quake2.
 ******************************/

class T_CORE_API CLoaderPAK : public ILoaderArchive
{
public:

    // Constructeur
    CLoaderPAK();

    // Méthodes publiques
    bool getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size);
    uint64_t getFileSize(const CString& fileName);
    bool loadFromFile(const CString& fileName);

    // Méthodes statiques publiques
    static bool isCorrectFormat(const char * fileName);

#ifdef T_NO_COVARIANT_RETURN
    static ILoaderArchive * createInstance(const char * fileName);
#else
    static CLoaderPAK * createInstance(const char * fileName);
#endif

protected:

    // Données protégées
    std::vector<uint64_t> m_offsets; ///< Offset de chaque fichier.
    std::vector<uint64_t> m_lengths; ///< Longueur de chaque fichier.
    std::ifstream m_file;            ///< Fichier de l'archive.
};

} // Namespace Ted

#endif // T_FILE_CORE_CLOADERPAK_HPP_

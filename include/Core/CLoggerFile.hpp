/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoggerFile.hpp
 * \date       2008 Création de la classe CLoggerFile.
 */

#ifndef T_FILE_CORE_CLOGGERFILE_HPP_
#define T_FILE_CORE_CLOGGERFILE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Core/Export.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * \class   CLoggerFile
 * \ingroup Core
 * \brief   Logger qui inscrit les messages dans un fichier.
 ******************************/

class T_CORE_API CLoggerFile : public ILogger
{
public :

    // Constructeur et destructeur
    CLoggerFile(const CString& file = "output.log");
    virtual ~CLoggerFile();

    // Log un message
    virtual void write(const CString& message, int level = Info);

private :

    // Donnée membre
    std::ofstream m_file; ///< Fichier de sortie des logs.
};

}

#endif // T_FILE_CORE_CLOGGERFILE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ISingleton.hpp
 * \date       2008 Création de la classe ISingleton.
 * \date 04/11/2010 Création de la classe INonCopyable.
 * \date 31/01/2011 Déplacement de la classe INonCopyable vers un fichier séparé.
 * \date 05/12/2011 Utilisation des classes de la SFML.
 */

#ifndef T_FILE_CORE_ISINGLETON_HPP_
#define T_FILE_CORE_ISINGLETON_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <SFML/System.hpp>

#include "Core/Export.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * \class   ISingleton
 * \ingroup Core
 * \brief   Définition d'une classe Singleton.
 ******************************/

template <class T>
class T_CORE_API ISingleton
{
public:

    /**
     * Renvoie l'instance unique de la classe.
     *
     * \return Instance de la classe.
     ******************************/

    static T& Instance()
    {
        static sf::Mutex mutex;

        if (m_inst == nullptr)
        {
            sf::Lock lock(mutex);

            if (m_inst == nullptr)
            {
                m_inst = new T();
            }
        }

        return *m_inst;
    }

    /**
     * Détruit l'instance unique de la classe.
     ******************************/

    static void Destroy()
    {
        delete m_inst;
        m_inst = nullptr;
    }

protected:

    /**
     * Constructeur par défaut.
     ******************************/

    ISingleton() { }

    /**
     * Destructeur.
     ******************************/

    virtual ~ISingleton() { }

private :

    // Donnée privée
    static T * m_inst; ///< Instance unique de la classe.

    // Copie interdite
    ISingleton(const ISingleton&);
    ISingleton& operator=(const ISingleton&);
};


#undef new
#undef delete


/**
 * Macro automatisant la création d'un singleton.
 ******************************/
#define T_MAKE_SINGLETON(Class) \
    public: \
        friend class ISingleton<Class>; \
        static Class& Instance(); \
    private:

/**
 * Macro automatisant la création d'un singleton.
 ******************************/
#define T_SINGLETON_IMPL(Class) \
    template <> Class * ISingleton<Class>::m_inst = nullptr; \
    Class& Class::Instance() \
    { \
        return ISingleton<Class>::Instance(); \
    } \

} // Namespace Ted

#endif // T_FILE_CORE_ISINGLETON_HPP_

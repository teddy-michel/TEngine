/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CVideoMode.hpp
 * \date 23/07/2010 Création de la classe CVideoMode.
 */

#ifndef T_FILE_CORE_CVIDEOMODE_HPP_
#define T_FILE_CORE_CVIDEOMODE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Export.hpp"


namespace Ted
{

/*
 * \class   CVideoMode
 * \ingroup Core
 * \brief   Mode vidéo.
 ******************************/
/*
class T_CORE_API CVideoMode
{
public:

    unsigned int width;  ///< Largeur en pixels.
    unsigned int height; ///< Hauteur en pixels.
    unsigned int bpp;    ///< Nombre de bits par pixel.

    /// Constructeur par défaut.
    CVideoMode(unsigned int p_width = 0, unsigned int p_height = 0, unsigned int p_bpp = 0) :
        width (p_width), height (p_height), bpp (p_bpp) {}

    /// Opérateur de comparaison (==).
    inline bool operator==(const CVideoMode& mode) const
    {
        return (mode.width == width && mode.height == height && mode.bpp == bpp);
    }

    /// Opérateur de comparaison (!=).
    inline bool operator!=(const CVideoMode& mode) const
    {
        return (mode.width != width || mode.height != height || mode.bpp != bpp);
    }

    /// Opérateur de comparaison (<).
    inline bool operator<(const CVideoMode& mode) const
    {
        if (bpp > mode.bpp) return false;
        if (bpp < mode.bpp) return true;
        if (width > mode.width) return false;
        if (width < mode.width) return true;
        return (height < mode.height);
    }

    /// Opérateur de comparaison (>).
    inline bool operator>(const CVideoMode& mode) const
    {
        if (bpp > mode.bpp) return true;
        if (bpp < mode.bpp) return false;
        if (width > mode.width) return true;
        if (width < mode.width) return false;
        return (height > mode.height);
    }
};
*/
} // Namespace Ted

#endif // T_FILE_CORE_CVIDEOMODE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Core/Allocation.hpp
 * \date 29/12/2009 Création des fonctions d'allocation.
 * \date 13/07/2010 Le gestionnaire de mémoire fait de nouveau partie du projet.
 */

#ifdef T_USING_MEMORY_MANAGER

#ifndef T_FILE_CORE_ALLOCATION_HPP_
#define T_FILE_CORE_ALLOCATION_HPP_

/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CMemoryManager.hpp"


/**
 * Surcharge de l'opérateur new.
 *
 * \param size Taille à allouer.
 * \param file Fichier source.
 * \param line Ligne dans le fichier source.
 * \return Pointeur sur la zone allouée.
 ******************************/

inline void * operator new(std::size_t size, const char * file, int line)
{
    return Ted::CMemoryManager::Instance().allocate(size, file, line, false);
}


/**
 * Surcharge de l'opérateur new[].
 *
 * \param size Taille à allouer.
 * \param file Fichier source.
 * \param line Ligne dans le fichier source.
 * \return Pointeur sur la zone allouée.
 ******************************/

inline void * operator new[](std::size_t size, const char * file, int line)
{
    return Ted::CMemoryManager::Instance().allocate(size, file, line, true);
}


/**
 * Surcharge de l'opérateur delete.
 *
 * \param ptr Pointeur sur la zone à libérer.
 ******************************/

inline void operator delete(void * ptr) throw()
{
    Ted::CMemoryManager::Instance().Free(ptr, false);
}


/**
 * Surcharge de l'opérateur delete[].
 *
 * \param ptr Pointeur sur la zone à libérer.
 ******************************/

inline void operator delete[](void * ptr) throw()
{
    Ted::CMemoryManager::Instance().Free(ptr, true);
}


/**
 * Surcharge de l'opérateur delete.
 *
 * \param ptr  Pointeur sur la zone à libérer.
 * \param file Fichier source.
 * \param line Ligne dans le fichier source.
 ******************************/

inline void operator delete(void * ptr, const char * file, int line) throw()
{
    Ted::CMemoryManager::Instance().nextDelete(file, line);
    Ted::CMemoryManager::Instance().Free(ptr, false);
}


/**
 * Surcharge de l'opérateur delete[].
 *
 * \param ptr  Pointeur sur la zone à libérer.
 * \param file Fichier source.
 * \param line Ligne dans le fichier source.
 ******************************/

inline void operator delete[](void * ptr, const char * file, int line) throw()
{
    Ted::CMemoryManager::Instance().nextDelete(file, line);
    Ted::CMemoryManager::Instance().Free(ptr, true);
}

#endif // T_FILE_CORE_ALLOCATION_HPP_


/**
 * Définition de macros servant à automatiser le tracking.
 ******************************/

#ifndef new
#  define new     new(__FILE__, __LINE__)
#  define delete  Ted::CMemoryManager::Instance().nextDelete(__FILE__, __LINE__), delete
#endif

#endif // T_USING_MEMORY_MANAGER

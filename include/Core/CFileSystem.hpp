/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CFileSystem.hpp
 * \date 12/01/2010 Création de la classe CFileSystem.
 * \date 08/07/2010 La classe FileSystem est renommée en CFileSystem.
 * \date 27/07/2010 Ajout du paramètre separator.
 */

#ifndef T_FILE_CORE_CFILESYSTEM_HPP_
#define T_FILE_CORE_CFILESYSTEM_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <fstream>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/ISingleton.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class ILoaderArchive;


/**
 * \class   CFileSystem
 * \ingroup Core
 * \brief   Système de fichier local.
 *
 * Permet de gérer un système de fichier propre à l'application.
 * Les fichiers et dossiers sont recherchés dans le système de fichier réel,
 * et s'ils n'existent pas, ils sont recherchés dans les archives.
 * Dès que le fichier est trouvé, on ne cherche pas s'il existe dans une autre archive.
 ******************************/

class T_CORE_API CFileSystem : public ISingleton<CFileSystem>
{
    T_MAKE_SINGLETON(CFileSystem)

public:

    // Constructeur et destructeur
    CFileSystem();
    ~CFileSystem();

    // Méthodes publiques
    void addArchive(const CString& fileName);
    void addDirectory(const CString& dir);
    void removeArchive(const CString& fileName);
    void removeArchive(ILoaderArchive * archive);
    void removeDirectory(const CString& dir);
    bool archiveExists(const CString& fileName) const;
    bool fileExists(const CString& fileName) const;
    bool fileExistsInDirs(const CString& fileName) const;
    bool fileExistsInArchives(const CString& fileName) const;
    CString getFileDirectory(const CString& fileName) const;
    CString getFileArchive(const CString& fileName) const;
    uint64_t getFileSize(const CString& fileName) const;
    CString getCurrentDirectory() const;
    ILoaderArchive * getArchive(const CString& archive);
    void logArchivesList() const;
    void logDirectoriesList() const;
    CString getAbsolutePath(const CString& fileName) const;

    // Méthodes statiques
    static CString convertName(const CString& fileName);
    static void changeDirectory(const CString& dir);

    // Donnée statique protégée
    static const char separator; ///< Séparateur dans les chemins.

protected:

    // Données protégées
    std::vector<ILoaderArchive *> m_archives; ///< Liste des archives du système de fichier.
    std::vector<CString> m_dirs;              ///< Liste des répertoires.
};

} // Namespace Ted

#endif // T_FILE_CORE_CFILESYSTEM_HPP_

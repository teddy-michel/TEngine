/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderTAR.hpp
 * \date 31/01/2010 Création de la classe CLoaderTAR.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */

#ifndef T_FILE_CORE_CLOADERTAR_HPP_
#define T_FILE_CORE_CLOADERTAR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Core/Export.hpp"
#include "Core/ILoaderArchive.hpp"


namespace Ted
{

/**
 * \class   CLoaderTAR
 * \ingroup Core
 * \brief   Lecture d'une archive au format TAR.
 ******************************/

class T_CORE_API CLoaderTAR : public ILoaderArchive
{
public:

    // Constructeur
    CLoaderTAR();

    // Méthodes publiques
    bool getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size);
    uint64_t getFileSize(const CString& fileName);
    bool loadFromFile(const CString& fileName);

    // Méthodes statiques publiques
    static bool isCorrectFormat(const char * fileName);

#ifdef T_NO_COVARIANT_RETURN
    static ILoaderArchive * createInstance(const char * fileName);
#else
    static CLoaderTAR * createInstance(const char * fileName);
#endif

private:

#include "Core/struct_alignment_start.h"

    /// En-tête de chaque fichier.
    struct TARFileHeader
    {
        char fileName[100]; ///< Nom du fichier.
        char filemode[8];   ///< Mode du fichier.
        char owner[8];      ///< Propriété du fichier.
        char group[8];      ///< Groupe propriété du fichier.
        char filesize[12];  ///< Taille du fichier.
        char time[12];      ///< Dernière modification (temps UNIX).
        char checksum[8];   ///< Checksum de l'header.
        char link;          ///< Indicateur du type de fichier.
        char linkname[100]; ///< Nom du lien.
    };

#include "Core/struct_alignment_end.h"

protected:

    // Données protégées
    std::vector<uint64_t> m_offsets; ///< Offset de chaque fichier.
    std::vector<uint64_t> m_lengths; ///< Longueur de chaque fichier.
    std::ifstream m_file;            ///< Fichier de l'archive.
};

} // Namespace Ted

#endif // T_FILE_CORE_CLOADERTAR_HPP_

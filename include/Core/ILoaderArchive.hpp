/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILoaderArchive.hpp
 * \date 12/01/2010 Création de la classe ILoaderArchive.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Ajout d'une liste de chargeurs d'archives.
 *                  Ajout des méthodes statiques getArchiveLoader et AddArchiveLoader.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */

#ifndef T_FILE_CORE_ILOADERARCHIVE_HPP_
#define T_FILE_CORE_ILOADERARCHIVE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>
#include <stdint.h>

#include "Core/Export.hpp"
#include "Core/ILoader.hpp"


namespace Ted
{

/**
 * \class   ILoaderArchive
 * \ingroup Core
 * \brief   Classe de base pour ouvrir les archives.
 ******************************/

class T_CORE_API ILoaderArchive : public ILoader
{
public:

    typedef ILoaderArchive * (*CreateInstanceType)(const char *);

    // Constructeur et destructeur
    ILoaderArchive();
    virtual ~ILoaderArchive();

    // Méthodes publiques
    unsigned int getNumFiles() const;
    std::vector<CString> getFilesList();
    bool isFileExists(const CString& fileName);
    virtual bool getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size) = 0;
    virtual uint64_t getFileSize(const CString& fileName) = 0;

#ifdef T_DEBUG
    void Debug_LogFiles() const;
#endif

    // Méthodes statiques publiques
    static ILoaderArchive * getArchiveLoader(const CString& fileName);
    static void addArchiveLoader(CreateInstanceType loader);

protected:

    // Donnée protégée
    std::vector<CString> m_files; ///< Liste des fichiers de l'archive.

private:

    // Donnée statique privée
    static std::list<CreateInstanceType> s_loaders; ///< Liste de chargeurs d'archives.
};

} // Namespace Ted

#endif // T_FILE_CORE_ILOADERARCHIVE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CApplication.hpp
 * \date       2008 Création de la classe Application.
 * \date 21/06/2010 Modification de la gestion des évènements.
 * \date 15/07/2010 La classe Application est renommée en CApplication.
 * \date 19/07/2010 La méthode ShowCursor est déplacée du GuiEngine vers cette classe.
 * \date 23/07/2010 Modification de la gestion de la fenêtre.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 28/11/2010 Création de la méthode getGameData.
 * \date 27/02/2011 Suppression de la méthode NewGame.
 * \date 11/03/2011 Création de la méthode SetMouseCursor.
 * \date 03/04/2011 Ajout de l'énumération TAction.
 *                  Ajout des méthodes pour gérer les actions associées aux touches.
 * \date 02/10/2011 Ajout d'un attribut pour les paramètres du jeu.
 * \date 04/10/2011 Ajout de la méthode getSettingValue.
 * \date 04/12/2011 Utilisation de la SFML.
 * \date 08/10/2012 Ajout de l'énumération TLang.
 * \date 16/03/2013 Séparation de la boucle principale dans deux méthodes virtuelles.
 */

#ifndef T_FILE_CORE_CAPPLICATION_HPP_
#define T_FILE_CORE_CAPPLICATION_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <SFML/Window.hpp>
#include <string>
#include <vector>
#include <map>

#include "os.h"
#include "Core/Export.hpp"
#include "Core/Events.hpp"
#include "Core/CSettings.hpp"
#include "Core/CString.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/CCircularBuffer.hpp"
#include "Core/IEventReceiver.hpp"


namespace Ted
{

/**
 * \enum    TAction
 * \ingroup Core
 * \brief   Liste des actions possibles pour un joueur.
 ******************************/

enum TAction
{
    ActionNone,        ///< Aucune action.
    ActionForward,     ///< Déplacement vers l'avant.
    ActionBackward,    ///< Déplacement vers l'arrière.
    ActionStrafeLeft,  ///< Déplacement sur le côté gauche.
    ActionStrafeRight, ///< Déplacement sur le côté droit.
    ActionTurnLeft,    ///< Rotation vers la gauche.
    ActionTurnRight,   ///< Rotation vers la droite.
    ActionJump,        ///< Saut.
    ActionCrunch,      ///< S'accroupir.
    ActionZoom,        ///< Zoom.
    ActionFire1,       ///< Tir primaire.
    ActionFire2        ///< Tir secondaire.
};


/**
 * \enum    TLang
 * \ingroup Core
 * \brief   Liste des langues supportées par le moteur.
 ******************************/

enum TLang
{
    LangEN, ///< Anglais.
    LangFR  ///< Français.
};


class ILogger;


/**
 * \class   CApplication
 * \ingroup Core
 * \brief   Gestion de l'application et de la fenêtre.
 *
 * Cette classe est la classe de base du TEngine. Toutes ses méthodes sont
 * accessibles en appelant CApplication::Instance(), qui retourne l'instance
 * unique de cette classe.
 *
 * Elle permet de définir les propriétés de la fenêtre, comme ses dimensions,
 * son titre, ou son icône. La méthode DisplayWindow doit être appellée au
 * démarrage de l'application pour crée la fenêtre et le contexte OpenGL
 * associé. Il est possible ensuite de changer la taille de la fenêtre, ou de
 * passer en plein écran, avec les méthodes SetSize et SwitchFullScreen.
 *
 * Cette classe gère aussi les évènements de la souris et du clavier, et
 * contient donc la boucle principale dans la méthode MainLoop.
 *
 * Elle gère également les données du jeu, définies avec la méthode setGameData.
 * Les paramètres de l'application sont définis dans le fichier config.ini, qui
 * est chargé dans le constructeur de la classe.
 ******************************/

class T_CORE_API CApplication : public IEventReceiver
{
public:

    // Constructeur et destructeur
    CApplication(const CString& title);
    virtual ~CApplication();

    // Accesseurs
    unsigned int getGameTime() const;
    CString getWindowTitle() const;
    float getFPS() const;
    bool getKeyState(TKey key) const;
    bool getButtonState(TMouseButton button) const;
    bool isActive() const;
    IEventReceiver * getEventReceiver() const;
    bool isArg(const CString& arg) const;
    unsigned int getArgPosition(const CString& arg) const;
    CString getArg(unsigned int position) const;
    unsigned int getNumArgs() const;
    TAction getActionForKey(TKey key) const;
    TKey getKeyForAction(TAction action) const;
    bool isActionActive(TAction action) const;

    // Mutateurs
    void setWindowTitle(const CString& title);
    void setInactive();
    void setEventReceiver(IEventReceiver * receiver);
    void setKeyForAction(TAction action, TKey key);

    // Méthodes publiques
    void addLogger(ILogger * logger);
    void removeLogger(ILogger * logger);
    void log(const CString& message, int level = 0);

    bool inGame() const;
    bool isGameActive() const;
    void setGameActive(bool active = true);

    virtual void mainLoop();
    virtual bool eventLoop();
    void sendArgs(int argc, char * argv[]);
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event) { T_UNUSED(event); } // MinGW ne trouve pas la méthode virtuelle...
    void closeWindow();
    bool isWindowOpened() const;
    void showWindow(bool state = true);
    unsigned int getWidth() const;
    float getRatio() const;
    TVector2I getCursorPosition() const;
    bool isFullScreen() const;
    bool isCursorVisible() const;
    unsigned int getHeight() const;

    CString getSettingValue(const CString& group, const CString& key, const CString& defval = CString()) const;
    void setSettingValue(const CString& group, const CString& key, const CString& value);

    void enableKeyRepeat(bool enabled = true);
    void setSize(unsigned int width, unsigned int height);
    void setPosition(int left, int top);
    void showMouseCursor(bool show = true);
    void setCursorPosition(const TVector2US& pos);
    virtual void switchFullScreen(bool fullscreen = true);
    void displayWindow();
    virtual void displayWindow(const sf::VideoMode& mode, const CString& title, bool fullscreen = false);

    inline CCircularBuffer<unsigned int> getFrameTimeList() const
    {
        return m_frameTimeList;
    }

    inline CCircularBuffer<float> getFPSList() const
    {
        return m_fpsList;
    }


    // Méthodes publiques statiques
    static bool isValidResolution(unsigned int width , unsigned int height);
    static void getSupportedResolutions(std::vector<TVector2UI>& resolutions);
    static void getSupportedVideoModes(std::vector<sf::VideoMode>& modes);

    static CApplication * getApp()
    {
        return m_instance;
    }

    static CApplication * m_instance;

protected:

    unsigned int m_time;                    ///< Instant du début de la frame.
    bool m_inGame;                          ///< Indique si une partie est lancée.
    bool m_gameActive;                      ///< Indique si le jeu est actif ou pas.
    bool m_active;                          ///< Indique si l'application est active.
    float m_fps;                            ///< Nombre de frames affichées chaque seconde.
    sf::Window * m_window;                  ///< Pointeur sur la fenêtre associée à l'application.
    TVector2US m_cursorPos;                 ///< Position du curseur.
    unsigned int m_gameTime;                ///< Nombre de millisecondes écoulées dans la partie.
    bool m_cursorVisible;                   ///< Indique si le curseur de la souris doit être affiché.
    CCircularBuffer<unsigned int> m_frameTimeList;
    CCircularBuffer<float> m_fpsList;

    static TKey getKeyCode(const CString& keycode);
    static TKey getKeyCode(const sf::Keyboard::Key& keycode);

private:


#ifndef T_NO_DEFAULT_LOGGER
    ILogger * m_defaultLogger;              ///< Pointeur sur le logger par défaut.
#endif // T_NO_DEFAULT_LOGGER

    CSettings m_settings;                   ///< Paramètres du jeu.
    IEventReceiver * m_receiver;            ///< Pointeur sur le récepteur d'évènement.
    CString m_title;                        ///< Titre de la fenêtre.
    sf::VideoMode m_videoMode;              ///< Mode vidéo de l'application.
    bool m_fullscreen;                      ///< Indique si l'application est en plein écran.
    std::vector<CString> m_args;            ///< Arguments envoyés à l'application.
    std::map<TAction, TKey> m_keyActions;   ///< Touches du clavier associées aux actions. \todo Pouvoir utiliser les boutons de la souris et du joystick.
    std::map<TKey, bool> m_keyStates;       ///< État des touches.
    std::map<TMouseButton, bool> m_buttons; ///< État des boutons de la souris.
    std::list<ILogger *> m_loggers;         ///< Liste des loggers.
};

} // Namespace Ted

#endif // T_FILE_CORE_CAPPLICATION_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CDirectory.hpp
 * \date 21/05/2009 Création de la classe CDirectory.
 * \date 23/03/2012 Utilisation de la classe CFlags.
 */

#ifndef T_FILE_CORE_CDIRECTORY_HPP_
#define T_FILE_CORE_CDIRECTORY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <string>
#include <algorithm>
#include <cctype>

#include "Core/Export.hpp"
#include "Core/CString.hpp"
#include "Core/CFlags.hpp"


namespace Ted
{

/**
 * \enum    TFileFlag
 * \ingroup Core
 * \brief   Flags pour les fichiers.
 ******************************/

#if __cplusplus < 201103L
enum TFileFlag
#else
enum TFileFlag : uint8_t
#endif
{
    ARCHIVE    = 0x01, ///< Archive.
    COMPRESSED = 0x02, ///< Fichier compressé.
    ENCRYPTED  = 0x04, ///< Fichier crypté.
    HIDDEN     = 0x08, ///< Fichier caché.
    READONLY   = 0x10, ///< Fichier en lecture seule.
    SYSTEM     = 0x20, ///< Fichier système.
    TEMPORARY  = 0x40  ///< Fichier temporaire.
};

T_DECLARE_FLAGS(TFileFlags, TFileFlag)
T_DECLARE_OPERATORS_FOR_FLAGS(TFileFlags)


/**
 * \struct  TFileInfos
 * \ingroup Core
 * \brief   Informations sur un fichier.
 ******************************/

struct T_CORE_API TFileInfos
{
    CString name;       ///< Nom du fichier.
    unsigned int size;  ///< Taille en octets.
    TFileFlags flags;   ///< Flags.
    unsigned long time; ///< Date de modification.
};


class CTreeNode;


/**
 * \class   CDirectory
 * \ingroup Core
 * \brief   Représentation d'un répertoire.
 ******************************/

class T_CORE_API CDirectory
{
public:

    /**
     * \enum    TSort
     * \ingroup Core
     * \brief   Critères de tri des fichiers d'un répertoire.
     ******************************/

    enum TSort
    {
        SortByName,  ///< Par nom de fichier.
        SortByTime,  ///< Par date.
        SortBySize,  ///< Par taille.
        SortByType   ///< Par type (extension).
    };

    // Constructeur
    CDirectory(const CString& dir = CString());

    // Accesseurs
    CString getDirectory() const;
    CString getFilter() const;
    std::vector<TFileInfos> getFilesList() const;
    std::vector<CString> getDirsList() const;
    CTreeNode * getTree() const;

    // Mutateurs
    void setDirectory(const CString& dir = CString());
    void setRelativeDirectory(const CString& dir);
    void setFilter(const CString& filter = "*.*");
    void setSorting(TSort sort, bool asc = true);

    // Méthodes publiques
    unsigned int getNumFiles() const;
    unsigned int getNumDirs() const;
    unsigned int getSize() const;

protected:

    // Méthodes protégées
    void listFiles();
    void sortFiles();

    /// Tri des fichiers par nom croissant.
    struct SortFilesByNameASC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file2.name > file1.name);
        }
    };

    /// Tri des fichiers par nom décroissant.
    struct SortFilesByNameDESC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file1.name > file2.name);
        }
    };

    /// Tri des fichiers par date de modification croissante.
    struct SortFilesByTimeASC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file1.time < file2.time);
        }
    };

    /// Tri des fichiers par date de modification décroissante.
    struct SortFilesByTimeDESC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file2.time < file1.time);
        }
    };

    /// Tri des fichiers par taille croissante.
    struct SortFilesBySizeASC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file1.size < file2.size);
        }
    };

    /// Tri des fichiers par taille décroissante.
    struct SortFilesBySizeDESC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            return (file2.size < file1.size);
        }
    };

    /// Tri des fichiers par type croissant.
    struct SortFilesByTypeASC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            std::ptrdiff_t pos1 = file1.name.lastIndexOf('.');
            std::ptrdiff_t pos2 = file2.name.lastIndexOf('.');
            CString ext1, ext2;

            if (pos1 != -1)
                ext1 = file1.name.subString(pos1 + 1);

            if (pos2 != -1)
                ext2 = file2.name.subString(pos2 + 1);

            return (ext1 < ext2);
        }
    };

    /// Tri des fichiers par type décroissant.
    struct SortFilesByTypeDESC
    {
        bool operator () (const TFileInfos& file1, const TFileInfos& file2) const
        {
            std::ptrdiff_t pos1 = file1.name.lastIndexOf('.');
            std::ptrdiff_t pos2 = file2.name.lastIndexOf('.');
            CString ext1, ext2;

            if (pos1 != -1)
                ext1 = file1.name.subString(pos1 + 1);

            if (pos2 != -1)
                ext2 = file2.name.subString(pos2 + 1);

            return (ext2 < ext1);
        }
    };

    // Données protégées
    CString m_directory;             ///< Adresse du répertoire.
    CString m_filter;                ///< Filtre de recherche.
    TSort m_sort;                    ///< Ordre de tri.
    bool m_asc;                      ///< Tri en ordre ascendant.
    std::vector<TFileInfos> m_files; ///< Liste des fichiers du répertoire.
    std::vector<CString> m_dirs;     ///< Liste des dossiers du répertoire.
};

} // Namespace Ted

#endif // T_FILE_CORE_CDIRECTORY_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHeightMap.hpp
 * \date 29/04/2011 Création de la classe CHeightMap.
 * \date 30/04/2011 Améliorations.
 * \date 08/05/2011 Améliorations.
 * \date 12/05/2011 Améliorations.
 */

#ifndef T_FILE_GAME_CHEIGHTMAP_HPP_
#define T_FILE_GAME_CHEIGHTMAP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Graphic/CTextureManager.hpp"


namespace Ted
{

class CBuffer;


/**
 * \class   CHeightMap
 * \ingroup Game
 * \brief   Cette classe permet de manipuler un terrain.
 ******************************/

class T_GAME_API CHeightMap
{
public:

    // Constructeur et destructeur
    CHeightMap(unsigned int numCaseX, unsigned int numCaseY, const TVector3F& origin = Origin3F, const TVector2F& size = TVector2F(48.0f, 48.0f));
    ~CHeightMap();

    // Accesseurs
    unsigned int getNumCaseX() const;
    unsigned int getNumCaseY() const;
    TVector2F getCaseSize() const;
    TTextureId getTextureId() const;
    TVector2F getTextureScale() const;
    const CBuffer * getBuffer() const;
    TVector3F getNodeRelativeCoords(unsigned int x, unsigned int y) const;
    bool getTriangleDirection(unsigned int x, unsigned int y) const;

    // Mutateurs
    void setCaseSize(const TVector2F& size);
    void setOrigin(const TVector3F& origin, bool update = true);
    void setTextureId(const TTextureId textureId);
    void setTextureScale(const TVector2F& scale);
    void setNodeRelativeCoords(unsigned int x, unsigned int y, const TVector3F& coords, bool update = true);
    void setNodeRelativeHeight(unsigned int x, unsigned int y, float height, bool update = true);
    void setTriangleDirection(unsigned int x, unsigned int y, bool direction, bool update = true);

private:

    // Méthodes privées
    unsigned int getNodeNumber(unsigned int x, unsigned int y) const;
    unsigned int getTriangleNumber(unsigned int x, unsigned int y) const;
    void updateBufferCoords();
    void updateBufferIndices();

    // Données privées
    const unsigned int m_numCaseX;   ///< Nombre de cases sur l'axe X.
    const unsigned int m_numCaseY;   ///< Nombre de cases sur l'axe Y.
    TVector3F m_origin;              ///< Coordonnées du point de départ.
    TVector2F m_size;                ///< Dimensions d'une case.
    TTextureId m_textureId;          ///< Identifiant de la texture à utiliser.
    TVector2F m_textureScale;        ///< Facteur de redimensionnement de la texture (1,1 par défaut).
    CBuffer * m_buffer;              ///< Buffer graphique.
    std::vector<TVector3F> m_coords; ///< Coordonnées relatives de chaque point.
    std::vector<bool> m_cases;       ///< Indique le sens des triangles de chaque case.
};

} // Namespace Ted

#endif // T_FILE_GAME_CHEIGHTMAP_HPP_

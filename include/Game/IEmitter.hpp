/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IEmitter.hpp
 * \date       2008 Création de la classe IEmitter.
 */

#ifndef T_FILE_PHYSIC_IEMITTER_HPP_
#define T_FILE_PHYSIC_IEMITTER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "Physic/CParticle.hpp"
#include "Graphic/CColor.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \enum    TParticleType
 * \ingroup Game
 * \brief   Types de particules à afficher.
 ******************************/

enum TParticleType
{
    Billboard, ///< La surface fait toujours face à la caméra.
    Sprite,    ///< La surface est orientée dans la direction de la caméra (rotation autour de l'axe Z).
    Point,     ///< Affichage d'un point.
    Line       ///< Affichage d'une ligne.
};


class CBuffer;


/**
 * \class   IEmitter
 * \ingroup Game
 * \brief   Gestion des particules.
 *
 * Gestion des emitters de particules, caractérisés par une position, une direction,
 * et d'autres paramètres permettant de concevoir toutes sortes d'effets de particules
 * (flammes, brouillard, fumée, explosion, etc.).
 *
 * \todo Pouvoir utiliser des textures.
 * \todo Pouvoir tourner les sprites.
 * \todo Pouvoir modifier les dimensions des surfaces générées.
 * \todo Pouvoir générer des modèles.
 * \todo Les particules doivent être gérés par deux "callbacks" : un pour calculer
 *       la position de départ, un autre pour déplacer la particule.
 ******************************/

class T_GAME_API IEmitter
{
public:

    // Constructeur et destructeur
    explicit IEmitter(const TVector3F& direction = TVector3F(0.0f, 0.0f, 1.0f), unsigned int life = 0, const CColor& color = CColor::Grey , unsigned int nbr = 500);
    virtual ~IEmitter();

    // Accesseurs
    TParticleType getType() const;
    TVector3F getDirection() const;
    float getDirectionVar() const;
    float getVitesseVar() const;
    unsigned int getLife() const;
    float getLifeVar() const;
    CColor getColor() const;
    float getColorVar() const;
    unsigned int getNumParticles() const;
    bool isRegeneration() const;
    CBuffer const * getBuffer() const;

    // Mutateurs
    void setType(TParticleType type);
    void setDirection(const TVector3F& direction);
    void setDirectionVar(float var);
    void setVitesseVar(float var);
    void setLife(unsigned int life);
    void setLifeVar(float var);
    void setColor(const CColor& color);
    void setColorVar(float var);
    void setNumParticles(unsigned int nbr);
    void setRegeneration(bool regeneration = true);

    // Méthodes publiques
    virtual void update(unsigned int frameTime);
    void regenere();
    void deleteParticles();

private:

    // Données privées
    TParticleType m_type;  ///< Type de particules.
    unsigned int m_nbr;    ///< Nombre de particules.
    float m_direction_var; ///< Variation de direction (entre 0 et 1).
    float m_vitesse_var;   ///< Variation de la vitesse initiale de la particule (entre 0 et 1).
    float m_life_var;      ///< Variation autour de la durée de vie moyenne (entre 0 et 1).
    float m_color_var;     ///< Écart maximale des couleurs avec la couleur moyenne (entre 0 et 1).

protected:

    // Méthodes protégées
    virtual TVector3F randSpeed() const;
    virtual CColor randColor() const;
    virtual unsigned int randLife() const;
    virtual void updateParticlePosition(CParticle& p, unsigned int frameTime);
    virtual CParticle createParticle() = 0;

    // Données protégées
    TVector3F m_direction;  ///< Direction des particules.
    unsigned int m_life;    ///< Durée de vie moyenne de chaque particule en millisecondes.
    CColor m_color;         ///< Couleur moyenne des particules.
    bool m_regeneration;    ///< Indique si on doit regénérer les particules mortes.
    CBuffer * m_buffer;     ///< Buffer graphique pour l'affichage des particules.
    std::vector<CParticle> m_particles; ///< Tableau des particules.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_IEMITTER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelMesh.hpp
 * \date 20/02/2010 Création de la classe CModelMesh.
 * \date 08/07/2010 Déplacement du dossier Base vers le dossier Physic.
 * \date 15/07/2010 Ajout des tests d'intersection avec un rayon.
 */

#ifndef T_FILE_GAME_CMODELMESH_HPP_
#define T_FILE_GAME_CMODELMESH_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "Game/IModel.hpp"


namespace Ted
{


class ILoaderModel;


/**
 * \class   CModelMesh
 * \ingroup Game
 * \brief   Gestion des modèles statiques sous forme de maillage.
 ******************************/

class T_GAME_API CModelMesh : public IModel
{
public:

    // Constructeur et destructeur
    explicit CModelMesh(ILoaderModel * file = nullptr);
    virtual ~CModelMesh();

    // Chargeur de fichier
    ILoaderModel * getLoaderModel() const;
    void setLoaderModel(ILoaderModel * loader);

    // Collisions
    TContentType getContentType(const TVector3F& position) const;
    bool isCorrectMovement(const TVector3F& from, const TVector3F& to) const;
    void update(unsigned int frameTime);
    void drawModel() const;
    bool hasImpact(const CRay& ray) const;
    CRayImpact getRayImpact(const CRay& ray) const;

protected:

    // Donnée protégée
    ILoaderModel * m_file; ///< Chargeur de fichier du modèle.
};

} // Namespace Ted

#endif // T_FILE_GAME_CMODELMESH_HPP_

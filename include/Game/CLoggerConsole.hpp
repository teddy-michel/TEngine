/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CLoggerConsole.hpp
 * \date       2008 Création de la classe CLoggerConsole.
 * \date 29/03/2013 Déplacement du module Core vers le module Game.
 */

#ifndef T_FILE_GAME_CLOGGERCONSOLE_HPP_
#define T_FILE_GAME_CLOGGERCONSOLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Game/Export.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

class CConsole;


/**
 * \class   CLoggerConsole
 * \ingroup Game
 * \brief   Logger qui inscrit les messages dans la console.
 ******************************/

class T_GAME_API CLoggerConsole : public ILogger
{
public :

    // Constructeur et destructeur
    CLoggerConsole(CConsole * console);
    virtual ~CLoggerConsole();

    // Log un message
    virtual void write(const CString& message, int level = Info);

private:

    CConsole * m_console;
    int m_minLevel;
};

}

#endif // T_FILE_GAME_CLOGGERCONSOLE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelDataOBJ.hpp
 * \date 08/10/2012 Création de la classe CModelDataOBJ.
 * \date 12/10/2012 Ajout de la méthode createInstance.
 * \date 03/06/2014 Ajout de la méthode getMemorySize.
 */

#ifndef T_FILE_LOADERS_CMODELDATAOBJ_HPP_
#define T_FILE_LOADERS_CMODELDATAOBJ_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>
#include <vector>

#include "Game/Export.hpp"
#include "Game/IModelData.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CModelDataOBJ
 * \ingroup Game
 * \brief   Gestion des modèles au format OBJ (Wavefront).
 ******************************/

class T_GAME_API CModelDataOBJ : public IModelData
{
public:

    // Constructeur
    inline CModelDataOBJ();
    inline virtual ~CModelDataOBJ();

    // Méthodes publiques
    virtual unsigned int getMemorySize() const;
    virtual void updateBuffer(CBuffer * buffer, IModel::TModelParams& params);
    virtual bool loadFromFile(const CString& fileName);
    bool saveToFile(const CString& fileName) const;

    static bool isCorrectFormat(const CString& fileName);

#ifdef T_NO_COVARIANT_RETURN
    static IModelData * createInstance(const CString& fileName);
#else
    static CModelDataOBJ * createInstance(const CString& fileName);
#endif

private:

/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

    /// Représente un matériau.
    struct TOBJMaterial
    {
        CString name;       ///< Nom du matériau.
        CColor ambient;     ///< Couleur ambiante.
        CColor diffuse;     ///< Couleur diffuse.
        CColor specular;    ///< Couleur spéculaire.
        float transparency; ///< Transparence (comprise entre 0 et 1).
        float shininess;    ///< Brillance.
        bool illumination;  ///< Indique si le matériau doit être illuminé avec le couleur spéculaire.

        /// Constructeur par défaut.
        TOBJMaterial(const CString& mname = CString()) :
            name         (mname),
            ambient      (CColor(102, 102, 102)),
            diffuse      (CColor(204, 204, 204)),
            specular     (CColor(76, 76, 76)),
            transparency (1.0f),
            shininess    (60.0f),
            illumination (false) { }
    };

    /// Représente une face triangulaire du maillage.
    struct TOBJFace
    {
        unsigned int s1; ///< Indice du premier sommet.
        unsigned int s2; ///< Indice du deuxième sommet.
        unsigned int s3; ///< Indice du troisième sommet.

        /// Constructeur par défaut.
        TOBJFace(unsigned int i1 = 0, unsigned int i2 = 0, unsigned i3 = 0) :
            s1 (i1),
            s2 (i2),
            s3 (i3) { }
    };

    /// Représente un groupe du modèle.
    struct TOBJGroup
    {
        CString name;                ///< Nom du groupe.
        TOBJMaterial * material;     ///< Pointeur sur le matériau du groupe.
        std::vector<TOBJFace> faces; ///< Tableau des faces du groupe.
    };

private:

    // Méthodes privées
    void addFace(TOBJGroup * group, unsigned int s1, unsigned int s2, unsigned int s3);
    bool readMTL(const CString& fileName);
    bool writeMTL(const CString& fileName) const;
    void freeMemory();

private:

    // Données privées
    std::list<TOBJGroup *> m_groups;       ///< Liste des groupes du modèle.
    std::list<TOBJMaterial *> m_materials; ///< Liste des matériaux du modèle.
    std::vector<TVector3F> m_vertices;     ///< Tableau des vertices.
    std::vector<TVector3F> m_normales;     ///< Tableau des normales.
};


/**
 * Constructeur par défaut.
 ******************************/

inline CModelDataOBJ::CModelDataOBJ() :
    IModelData ()
{ }


/**
 * Destructeur.
 ******************************/

inline CModelDataOBJ::~CModelDataOBJ()
{
    freeMemory();
}

} // Namespace Ted

#endif // T_FILE_LOADERS_CMODELDATAOBJ_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CConsole.hpp
 * \date       2008 Création de la classe CConsole.
 * \date 17/06/2010 La file est remplacée par un tableau.
 * \date 13/11/2010 Ajout du compteur de messages.
 * \date 28/04/2012 Suppression des méthodes variadiques.
 * \date 14/10/2012 Ajout des commandes get et set pour gérer les variables.
 * \date 29/03/2013 Déplacement du module Core vers le module Game.
 * \date 04/06/2014 On peut ajouter des commandes à la console.
 */

#ifndef T_FILE_GAME_CCONSOLE_HPP_
#define T_FILE_GAME_CCONSOLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <map>

#include "Game/Export.hpp"
#include "Core/ISingleton.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \enum    TConsoleDataType
 * \ingroup Game
 * \brief   Types de message de la console.
 ******************************/

enum TConsoleDataType
{
    ConsoleCommand, ///< Commande entrée par l'utilisateur.
    ConsoleInfos,   ///< Message d'information.
    ConsoleError,   ///< Erreur.
    ConsoleWarning  ///< Avertissement.
};


/**
 * \struct  TConsoleData
 * \ingroup Game
 * \brief   Donnée à afficher dans la console.
 ******************************/

struct T_GAME_API TConsoleData
{
    TConsoleDataType type; ///< Type de message.
    CString txt;           ///< Texte de la donnée.
};


/**
 * \class   CConsole
 * \ingroup Game
 * \brief   Cette classe sert à gérer la console.
 ******************************/

class T_GAME_API CConsole
{
public:

    // Constructeur et destructeur
    CConsole();
    ~CConsole();

    // Accesseurs
    std::vector<TConsoleData> getData() const;
    unsigned int getCount() const;

    // Méthodes publiques
    void clear();
    void sendCommand(const CString& cmd);
    void sendMessageInfos(const CString& msg);
    void sendMessageError(const CString& msg);
    void sendMessageWarning(const CString& msg);

    // Commandes
    typedef bool (*ConsoleFunction)(const std::list<CString>& args);

    void addCommand(const CString& name, ConsoleFunction callback, const CString& description = CString(), const CString& help = CString());
    bool isCommandExist(const CString& name) const;
    CString getCommandDescription(const CString& name) const;

private:

    // Méthodes privées
    void addData(const CString& txt, TConsoleDataType type);
    bool commandGet(const std::list<CString>& args);
    bool commandSet(const std::list<CString>& args);
    bool commandHelp(const std::list<CString>& args);


    struct TConsoleFunction
    {
        CString description;      ///< Description de la commande
        CString help;             ///< Message d'aide sur l'utilisation de la commande.
        ConsoleFunction callback; ///< Fonction à appeller.

        inline TConsoleFunction() : callback(nullptr) { }
    };


    // Données privées
    std::vector<TConsoleData> m_datas; ///< Liste des commandes et des réponses.
    std::map<CString, CString> m_vars; ///< Liste des variables.
    std::map<CString, TConsoleFunction> m_commands; ///< Liste des commandes disponibles.
    unsigned int m_counter;            ///< Compteur de messages.
};

} // Namespace Ted

#endif // T_FILE_GAME_CCONSOLE_HPP_

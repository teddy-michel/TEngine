/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterSphere.hpp
 * \date 28/07/2010 Création de la classe CEmitterSphere.
 */

#ifndef T_FILE_PHYSIC_CEMITTERSPHERE_HPP_
#define T_FILE_PHYSIC_CEMITTERSPHERE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IEmitter.hpp"


namespace Ted
{

/**
 * \class   CEmitterSphere
 * \ingroup Game
 * \brief   Gestion des particules émises par une sphère.
 ******************************/

class T_GAME_API CEmitterSphere : public IEmitter
{
public:

    // Constructeur
    CEmitterSphere(const TVector3F& center = Origin3F, float radius = 1.0f, const TVector3F& direction = TVector3F(0.0f, 0.0f, 1.0f), unsigned int life = 0, const CColor& color = CColor::Grey, unsigned int nbr = 500);

    // Accesseurs
    TVector3F getCenter() const;
    float getRadius() const;

    // Mutateurs
    void setCenter(const TVector3F& position);
    void setRadius(float radius);

private:

    // Méthode privée
    virtual CParticle createParticle();

protected:

    // Donnée protégée
    TVector3F m_center; ///< Position de l'emitter.
    float m_radius;     ///< Rayon de la sphère.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CEMITTERSPHERE_HPP_

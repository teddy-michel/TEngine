/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHUD_Health.hpp
 * \date 29/01/2011 Création de la classe CHUD_Health.
 * \date 28/03/2013 Déplacement du module Graphic vers le module Game.
 */

#ifndef T_FILE_GAME_CHUD_HEALTH_HPP_
#define T_FILE_GAME_CHUD_HEALTH_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Graphic/IHUD.hpp"


namespace Ted
{

class IBasePlayer;


/**
 * \class   CHUD_Health
 * \ingroup Game
 * \brief   HUD affichant le nombre de points de vie du joueur.
 ******************************/

class T_GAME_API CHUD_Health : public IHUD
{
public:

    // Constructeur
    CHUD_Health();

    // Accesseur
    IBasePlayer * getPlayer() const;

    // Mutateur
    void setPlayer(IBasePlayer * player);

    // Méthodes publiques
    void update();
    void paint();

private:

    // Donnée privée
    IBasePlayer * m_player; ///< Pointeur sur le joueur.
};

} // Namespace Ted

#endif // T_FILE_GAME_CHUD_HEALTH_HPP_

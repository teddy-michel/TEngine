/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterSnow.hpp
 * \date 10/04/2011 Création de la classe CEmitterSnow.
 */

#ifndef T_FILE_PHYSIC_CEMITTERSNOW_HPP_
#define T_FILE_PHYSIC_CEMITTERSNOW_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/IEmitter.hpp"
#include "Physic/CBoundingBox.hpp"


namespace Ted
{

/**
 * \class   CEmitterSnow
 * \ingroup Game
 * \brief   Gestion des particules de neige.
 ******************************/

class T_GAME_API CEmitterSnow : public IEmitter
{
public:

    // Constructeur
    CEmitterSnow(const CBoundingBox& volume, float density = 0.5f);

    // Accesseurs
    CBoundingBox getVolume() const;
    float getDensity() const;

    // Mutateurs
    void setVolume(const CBoundingBox& volume);
    void setDensity(float density);

private:

    // Méthodes privées
    virtual CParticle createParticle();
    virtual void updateParticlePosition(CParticle& p, unsigned int frameTime);
    void computeNumParticles();

    // Données privées
    CBoundingBox m_volume; ///< Volume dans lequel les particules sont créées.
    float m_density;       ///< Densité de la neige (entre 0 et 1).
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CEMITTERSNOW_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CGameApplication.hpp
 * \date 27/03/2013 Création de la classe CGameApplication.
 * \date 08/06/2014 Ajout d'un répertoire pour charger les maps.
 */

#ifndef T_FILE_GAME_CGAMEAPPLICATION_HPP_
#define T_FILE_GAME_CGAMEAPPLICATION_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <SFML/Window/VideoMode.hpp>

#include "Game/Export.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

class CImage;
class IMap;
class CGameApplication;
class CConsole;
class CBuffer;
class CSoundEngine;
class CEntityManager;
class CRenderer;
class CLoggerConsole;
class CPhysicEngine;
class CModelManager;
class CBulletDebugDraw;
class CGuiEngine;

#ifdef T_ACTIVE_DEBUG_MODE
class CActivity;
#endif


/// Entités globales du moteur de jeu.
namespace Game
{
    extern T_GAME_API CGameApplication * app; // Défini dans CGameApplication.cpp
    extern T_GAME_API CConsole * console; // Défini dans CGameApplication.cpp
    extern T_GAME_API CEntityManager * entityManager; // Défini dans CEntityManager.cpp
    extern T_GAME_API CModelManager * modelManager; // Défini dans CModelManager.cpp
}


/**
 * \class   CGameApplication
 * \ingroup Game
 * \brief   Gestion de l'application et de la fenêtre.
 ******************************/

class T_GAME_API CGameApplication : public CApplication
{
public:

    // Constructeur et destructeur
    CGameApplication(const CString& title);
    virtual ~CGameApplication();

    bool isMultiplayer() const;
    void setMultiplayer(bool multiplayer = true);
    inline bool isShowBullet() const;
    void setShowBullet(bool show = true);

    IMap * getGameData() const;
    void setGameData(IMap * gamedata);

    virtual void mainLoop();
    virtual bool eventLoop();
    virtual bool loadMap(const CString& fileName);
    CString getMapDirectory() const;
    void setMapDirectory(const CString& dir);
    void closeGame();

    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CTextEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    virtual void onEvent(const CResizeEvent& event);

    void setIcon(const CImage& img);
    virtual void switchFullScreen(bool fullscreen = true);
    virtual void displayWindow(const sf::VideoMode& mode, const CString& title, bool fullscreen = false);

private:

    // Données privées
    bool m_multiplayer;   ///< Indique si le jeu est de type multijoueur ou pas.
    bool m_showBullet;    ///< Indique si on affiche les données de Bullet.
    IMap * m_gamedata;    ///< Pointeur sur les données du jeu.
    IMap * m_gamedataNew; ///< Pointeur sur les nouvelles données du jeu (utilisé pour le changement de map).
    CLoggerConsole * m_loggerConsole; ///< Pointeur sur le logger vers la console.
    CBulletDebugDraw * m_debugDraw;
    CBuffer * m_debugBuffer;          ///< Buffer graphique utilisé par Bullet.
    CString m_mapDirectory;           ///< Chemin du répertoire contenant les maps.

#ifdef T_ACTIVE_DEBUG_MODE
    CActivity * m_activityMenu;
    CActivity * m_activityDebug;
#endif // T_ACTIVE_DEBUG_MODE

};


inline bool CGameApplication::isShowBullet() const
{
    return m_showBullet;
}

} // Namespace Ted

#endif // T_FILE_GAME_CGAMEAPPLICATION_HPP_

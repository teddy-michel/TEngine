/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterTriangle.hpp
 * \date 28/01/2010 Création de la classe CEmitterTriangle.
 */

#ifndef T_FILE_PHYSIC_CEMITTERTRIANGLE_HPP_
#define T_FILE_PHYSIC_CEMITTERTRIANGLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IEmitter.hpp"


namespace Ted
{

/**
 * \class   CEmitterTriangle
 * \ingroup Game
 * \brief   Gestion des particules émises à la surface d'un triangle.
 ******************************/

class T_GAME_API CEmitterTriangle : public IEmitter
{
public:

    // Constructeur
    CEmitterTriangle(const TVector3F& point1 = Origin3F, const TVector3F& point2 = Origin3F,
                     const TVector3F& point3 = Origin3F, const TVector3F& direction = TVector3F(0.0f, 0.0f, 1.0f),
                     unsigned int life = 0, const CColor& color = CColor::Grey, unsigned int nbr = 500);

private:

    // Méthode privée
    virtual CParticle createParticle();

protected:

    // Donnée protégée
    TVector3F m_point1; ///< Premier sommet.
    TVector3F m_point2; ///< Deuxième sommet.
    TVector3F m_point3; ///< Troisième sommet.
};

} // Namespace Ted

#endif // T_FILE_PHYSIC_CEMITTERTRIANGLE_HPP_

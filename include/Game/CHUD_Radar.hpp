/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHUD_Radar.hpp
 * \date 29/01/2011 Création de la classe CHUD_Radar.
 * \date 30/03/2013 Déplacement du module Graphic vers le module Game.
 */

#ifndef T_FILE_GAME_CHUD_RADAR_HPP_
#define T_FILE_GAME_CHUD_RADAR_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Game/Export.hpp"
#include "Graphic/IHUD.hpp"


namespace Ted
{

class ILocalEntity;


/**
 * \class   CHUD_Radar
 * \ingroup Game
 * \brief   HUD affichant un radar avec la position de différent objets.
 ******************************/

class T_GAME_API CHUD_Radar : public IHUD
{
public:

    // Constructeur
    CHUD_Radar();

    // Accesseur
    ILocalEntity * getSubject() const;

    // Mutateur
    void setSubject(ILocalEntity * entity);

    // Méthodes publiques
    void update();
    void paint();
    void addEntity(ILocalEntity * entity);
    void removeEntity(ILocalEntity * entity);
    bool isEntity(const ILocalEntity * entity) const;
    unsigned int getNumEntities() const;

private:

    // Données privées
    ILocalEntity * m_subject;             ///< Pointeur sur l'entité au centre du radar.
    std::list<ILocalEntity *> m_entities; ///< Liste des objets à suivre.
};

} // Namespace Ted

#endif // T_FILE_GAME_CHUD_RADAR_HPP_

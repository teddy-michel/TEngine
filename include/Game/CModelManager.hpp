/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelManager.hpp
 * \date 08/10/2012 Création de la classe CModelManager.
 */

#ifndef T_FILE_GAME_CMODELMANAGER_HPP_
#define T_FILE_GAME_CMODELMANAGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <list>

#include "Game/Export.hpp"
#include "Core/IResourceManager.hpp"


namespace Ted
{

class IModelData;


/**
 * \class   CModelManager
 * \ingroup Core
 * \brief   Gestionnaire de modèles.
 ******************************/

class T_GAME_API CModelManager : public IResourceManager
{
public:

    typedef IModelData * (*CreateInstanceType)(const CString&);

    // Constructeur et destructeur
    CModelManager();
    virtual ~CModelManager();

    virtual unsigned int getMemorySize() const;
    inline virtual unsigned int getNumElements() const;
    virtual int getResourceId(const CString& name);
    virtual CString getResourceName(int id) const;

    IModelData * getModelData(int id) const;

    void addModelDataLoader(CreateInstanceType modelDataLoader);

private:

    inline virtual void freeMemory();

    struct TModelDataInfos
    {
        CString name;       ///< Nom du modèle.
        IModelData * model; ///< Pointeur sur le modèle chargé.
        //unsigned int size; ///< Taille du modèle en mémoire.
        //unsigned int time; ///< Temps de la dernière utilisation du modèle.

        TModelDataInfos() : name(), model(nullptr) { }
    };

    typedef std::vector<TModelDataInfos> TModelInfoVector;

    TModelInfoVector m_models;               ///< Liste des modèles gérés par le gestionnaire.
    std::list<CreateInstanceType> m_loaders; ///< Liste des chargeurs de modèles disponibles.
};


inline unsigned int CModelManager::getNumElements() const
{
    return m_models.size();
}


inline void CModelManager::freeMemory()
{ }

} // Namespace Ted

#endif // T_FILE_GAME_CMODELMANAGER_HPP_

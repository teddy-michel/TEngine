/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IMap.hpp
 * \date 14/05/2009 Création de la classe IGameData.
 * \date 18/07/2010 L'ouverture du fichier se fait dans la méthode LoadData.
 * \date 28/11/2010 Ajout des méthodes AddLightmap et RemoveLightmap.
 * \date 12/03/2011 Ajout d'un attribut pour le pourcentage de chargement.
 * \date 05/10/2011 Ajout de la méthode Clone.
 * \date 06/10/2012 La classe est renommée en IMap.
 */

#ifndef T_FILE_GAME_IMAP_HPP_
#define T_FILE_GAME_IMAP_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <vector>
#include <string>

#include "Game/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class IModel;
class ILocalEntity;


/**
 * \class   IMap
 * \ingroup Game
 * \brief   Classe de base pour stocker les informations sur une map.
 *
 * Cette classe doit être dérivée pour définir un nouveau format de map.
 * Elle s'occupe de charger les informations depuis les fichiers.
 ******************************/

class T_GAME_API IMap
{
public:

    // Constructeur et destructeur
    IMap();
    virtual ~IMap();

    // Accesseurs
    inline float getProgress() const;
    inline CString getName() const;
    inline CString getFileName() const;
    inline ILocalEntity * getSceneNode() const;

    // Mutateur
    void setName(const CString& name);

    // Méthodes publiques
    void update(unsigned int frameTime);
    void addLightmap(unsigned short face, unsigned short unit);
    void removeLightmap(unsigned short face, unsigned short unit);
    virtual bool loadData(const CString& fileName) = 0;
    virtual void unloadData();
    virtual IMap * clone() = 0;

protected:

    // Données protégées
    float m_progress;               ///< Pourcentage de chargement de la map (entre 0 et 100).
    CString m_name;                 ///< Nom de la map.
    CString m_fileName;             ///< Adresse du fichier contenant la map.
    std::ifstream m_file;           ///< Fichier de la map. \todo Utiliser un CFile à la place.
    ILocalEntity * m_scene_node;    ///< Racine du graphe de scène.
    std::vector<IModel *> m_models; ///< Liste des modèles. \todo Supprimer (et déplacer dans les classes dérivées au besoin).
};


/**
 * Donne le pourcentage de chargement de la map.
 *
 * \return Pourcentage de chargement de la map (entre 0 et 100).
 ******************************/

inline float IMap::getProgress() const
{
    return m_progress;
}


/**
 * Donne le nom de la map, qui peut être identique au nom du fichier.
 *
 * \return Nom de la map.
 *
 * \sa IMap::setName
 ******************************/

inline CString IMap::getName() const
{
    return m_name;
}


/**
 * Donne l'adresse du fichier contenant la map.
 *
 * \return Adresse du fichier contenant la map.
 ******************************/

inline CString IMap::getFileName() const
{
    return m_fileName;
}


/**
 * Donne le pointeur sur l'entité racine du graphe de scène.
 *
 * \return Pointeur sur la racine du graphe de scène.
 ******************************/

inline ILocalEntity * IMap::getSceneNode() const
{
    return m_scene_node;
}

} // Namespace Ted

#endif // T_FILE_GAME_IMAP_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CBulletDebugDraw.hpp
 * \date 12/12/2011 Création de la classe CBulletDebugDraw.
 * \date 30/03/2013 Déplacement du module Physic vers le module Game.
 */

#ifndef T_FILE_GAME_CBULLETDEBUGDRAW_HPP_
#define T_FILE_GAME_CBULLETDEBUGDRAW_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#ifdef T_USING_PHYSIC_BULLET
#  include <bullet/btBulletDynamicsCommon.h>
#endif

#include "Game/Export.hpp"


namespace Ted
{

class CBuffer;


/**
 * \class   CBulletDebugDraw
 * \ingroup Game
 * \brief   Permet d'afficher les données physiques.
 ******************************/

class T_GAME_API CBulletDebugDraw : public btIDebugDraw
{
public:

    // Constructeur et destructeur
    CBulletDebugDraw();
    virtual ~CBulletDebugDraw();

    // Accesseurs
    CBuffer * getBuffer() const;
    int getDebugMode() const;

    // Mutateur
    void setDebugMode(int debug_mode);

    // Méthodes publiques
    virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
    virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);
    virtual void reportErrorWarning(const char * warningString);
    virtual void draw3dText(const btVector3& location, const char * text);

private:

    // Données privées
    CBuffer * m_buffer; ///< Pointeur sur le buffer graphique.
    int m_debug_mode;   ///< Mode d'affichage (définit quelles données afficher).
};

} // Namespace Ted

#endif // T_FILE_GAME_CBULLETDEBUGDRAW_HPP_

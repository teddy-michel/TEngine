/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CGameDataModel.hpp
 * \date 20/02/2010 Création de la classe CGameDataModel.
 * \date 18/07/2010 L'ouverture du fichier se fait dans la méthode LoadData.
 */

#ifndef T_FILE_GAME_CGAMEDATAMODEL_HPP_
#define T_FILE_GAME_CGAMEDATAMODEL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/IMap.hpp"


namespace Ted
{

/**
 * \class   CGameDataModel
 * \ingroup Game
 * \brief   Stockage des informations du jeu pour l'affichage d'un modèle.
 ******************************/

class T_GAME_API CGameDataModel : public IMap
{
public:

    // Constructeur et destructeur
    CGameDataModel();
    ~CGameDataModel();

    // Méthodes publiques
    bool LoadData(const CString& fileName);
    void setModel(IModel * model);
    void UnloadData();
};

} // Namespace Ted

#endif // T_FILE_GAME_CGAMEDATAMODEL_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CCameraFPS.hpp
 * \date 10/05/2009 Création de la classe CCameraFPS.
 * \date 20/02/2011 Suppression d'attributs et de méthodes inutiles.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 28/03/2013 Déplacement du module Graphic vers le module Game.
 */

#ifndef T_FILE_GAME_CCAMERAFPS_HPP_
#define T_FILE_GAME_CCAMERAFPS_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Graphic/ICamera.hpp"
#include "Core/Events.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class IBasePlayer;


/**
 * \class   CCameraFPS
 * \ingroup Game
 * \brief   Caméra attachée à un joueur, vision à la première personne.
 ******************************/

class T_GAME_API CCameraFPS : public ICamera
{
public:

    // Constructeur et destructeur
    CCameraFPS(const TVector3F& position = Origin3F, const TVector3F& direction = TVector3F(1.0f, 0.0f, 0.0f));
    virtual ~CCameraFPS();

    // Accesseurs
    IBasePlayer * getPlayer() const;
    TVector3F getEyesPosition() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void setPlayer(IBasePlayer * player, const TVector3F& position = Origin3F);
    void setEyesPosition(const TVector3F& position);

    // Méthodes publiques
    void animate(unsigned int frameTime);
    void initialize(const TVector3F& position = Origin3F, float angleH = 0.0f, float angleV = 0.0f);

protected:

    // Données protégées
    IBasePlayer * m_player;  ///< Player attaché à la caméra.
    TVector3F m_player_eyes; ///< Position des yeux du joueur par rapport à son centre de gravité.
};

} // Namespace Ted

#endif // T_FILE_GAME_CCAMERAFPS_HPP_

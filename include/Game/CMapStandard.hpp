/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CMapStandard.hpp
 * \date 09/04/2012 Création de la classe CGameDataStandard.
 * \date 14/04/2012 Améliorations.
 * \date 15/04/2012 Améliorations.
 * \date 30/04/2012 Création d'un joueur.
 * \date 13/10/2012 La classe est renommée en CMapStandard.
 */

#ifndef T_FILE_GAME_CGAMEDATASTANDARD_HPP_
#define T_FILE_GAME_CGAMEDATASTANDARD_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>

#include "Game/Export.hpp"
#include "Game/IMap.hpp"
#include "Graphic/CTextureManager.hpp"


namespace Ted
{

class CStaticEntity;
class IBasePlayer;


/**
 * \class   CMapStandard
 * \ingroup Game
 * \brief   Stockage des informations du jeu pour le format interne du moteur.
 ******************************/

class T_GAME_API CMapStandard : public IMap
{
public:

    // Constructeur et destructeur
    CMapStandard();
    virtual ~CMapStandard();

    // Méthodes publiques
    bool loadData(const CString& fileName);
    void unloadData();
    IMap * clone();
    inline IBasePlayer * getPlayer() const;

protected:

    // Chargement
    bool loadTextures();
    bool loadLightmaps();
    bool loadGeometry();
    bool loadVolumes();
    bool loadEntities();

    // Données protégées
    CStaticEntity * m_world;              ///< Pointeur sur l'entité représentant le monde.
    IBasePlayer * m_player;               ///< Pointeur sur le joueur.
    std::map<int, TTextureId> m_textures; ///< Tableau contenant les identifiants de textures.
};


inline IBasePlayer * CMapStandard::getPlayer() const
{
    return m_player;
}

} // Namespace Ted

#endif // T_FILE_GAME_CGAMEDATASTANDARD_HPP_

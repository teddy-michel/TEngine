/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CWindow_Console.hpp
 * \date 27/05/2009 Création de la classe GuiWindow_Console.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CWindow_Console.
 */

#ifndef T_FILE_GUI_CWINDOW_CONSOLE_HPP_
#define T_FILE_GUI_CWINDOW_CONSOLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Gui/CWindow.hpp"


namespace Ted
{

class CTextEdit;
class CLineEdit;


/**
 * \class   CWindow_Console
 * \ingroup Game
 * \brief   Classe permettant de gérer la fenêtre Console.
 ******************************/

class T_GAME_API CWindow_Console : public CWindow
{
public:

    // Constructeur
    explicit CWindow_Console(IWidget * parent = nullptr);

    // Méthode publique
    virtual void draw();

private:

    // Méthode privée
    void addLine();

    // Données privées
    CTextEdit * m_text; ///< Zone de texte de console.
    CLineEdit * m_line; ///< Ligne de texte pour ajouter une commande.
};

} // Namespace Ted

#endif // T_FILE_GUI_CWINDOW_CONSOLE_HPP_

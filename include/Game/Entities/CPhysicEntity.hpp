/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPhysicEntity.hpp
 * \date 12/12/2010 Création de la classe CPhysicEntity.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CPHYSICENTITY_HPP_
#define T_FILE_ENTITIES_CPHYSICENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPhysicEntity.hpp"


namespace Ted
{

/**
 * \class   CPhysicEntity
 * \ingroup Game
 * \brief   Entité permettant d'afficher un modèle obéissant aux lois de la physique.
 ******************************/

class T_GAME_API CPhysicEntity : public IPhysicEntity
{
public:

    // Constructeurs
    explicit CPhysicEntity(ILocalEntity * parent);
    explicit CPhysicEntity(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseur
    inline virtual CString getClassName() const;

    // Méthode publique
    virtual void frame(unsigned int frame);
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CPhysicEntity::getClassName() const
{
    return "physic_entity";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CPHYSICENTITY_HPP_

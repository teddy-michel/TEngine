/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CConnection.hpp
 * \date 12/04/2009 Création de la classe COutput.
 * \date 05/07/2009 Déplacement du constructeur dans l'en-tête.
 * \date 24/03/2011 La classe COutput est renommée en CConnection.
 * \date 25/03/2011 L'identifiant d'entité est remplacé par une chaine de caractères.
 *                  Ajout du pointeur sur l'entité à l'origine de l'émission du signal.
 */

#ifndef T_FILE_ENTITES_CCONNECTION_HPP_
#define T_FILE_ENTITES_CCONNECTION_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>

#include "Game/Export.hpp"
#include "Core/CString.hpp"


namespace Ted
{

class IEntity;


/**
 * \class   CConnection
 * \ingroup Game
 * \brief   Représente une connexion entre le signal d'une entité et le slot d'une autre.
 ******************************/

class T_GAME_API CConnection
{
public:

    /**
     * Nom des entités à appeler.
     * Liste dont les éléments sont séparés par une virgule. Chaque élément est
     * soit un nom d'entité, soit une valeur spéciale dans la liste ci-dessous.
     *
     * \section Valeurs spéciales
     * \li caller : Entité à l'origine du déclenchement de la connexion (utilisé dans les triggers).
     * \li player : Joueur actif.
     ******************************/
    CString ent;

    CString slot;      ///< Nom du slot à appeler.
    CString param;     ///< Paramètres supplémentaires.
    unsigned int time; ///< Temps en millisecondes avant d'exécuter le slot.
    IEntity * caller;  ///< Pointeur sur l'entité à l'origine de l'émission du signal.


    /**
     * Constructeur.
     *
     * \param p_ent   Entités à appeler.
     * \param p_slot  Nom du slot à appeler.
     * \param p_param Paramètres supplémentaires.
     * \param p_time  Temps en millisecondes avant d'exécuter le slot.
     ******************************/

    CConnection(const CString& p_ent, const CString& p_slot, const CString& p_param = CString(), const unsigned int p_time = 0) :
        ent    (p_ent),
        slot   (p_slot),
        param  (p_param),
        time   (p_time),
        caller (nullptr) {}
};


typedef std::multimap<CString, CConnection> TConnectionMap;

} // Namespace Ted

#endif // T_FILE_ENTITES_CCONNECTION_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/ILocalEntity.hpp
 * \date 17/12/2010 Création de la classe ILocalEntity.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_ILOCALENTITY_HPP_
#define T_FILE_ENTITIES_ILOCALENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Game/Entities/IEntity.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

/**
 * \class   ILocalEntity
 * \ingroup Game
 * \brief   Classe de base des entités localisées.
 *
 * Les entités sont placées dans un graphe de scène. Chaque entité peut avoir un
 * parent, et peut posséder un nombre illimité d'enfants.
 ******************************/

class T_GAME_API ILocalEntity : public IEntity
{
public:

    // Constructeur et destructeur
    explicit ILocalEntity(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~ILocalEntity();

    // Accesseurs
    inline virtual CString getClassName() const;
    ILocalEntity * getParent() const;
    bool isChild(const ILocalEntity * child) const;
    bool isChildHierarchy(const ILocalEntity * child) const;
    virtual TVector3F getPosition() const = 0;

    // Mutateurs
    void setParent(ILocalEntity * parent);
    virtual void setPosition(const TVector3F& position) = 0;

    // Méthodes publiques
    void addChild(ILocalEntity * child);
    void removeChild(ILocalEntity * child);
    void deleteChild(ILocalEntity * child);
    void deleteChildren();
    void deleteChildrenHierarchy();
    virtual void frame(unsigned int frameTime);

private:

    // Données privées
    ILocalEntity * m_parent;              ///< Pointeur sur l'entité parent.
    std::list<ILocalEntity *> m_children; ///< Liste des entités enfants.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString ILocalEntity::getClassName() const
{
    return "local_entity";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_ILOCALENTITY_HPP_

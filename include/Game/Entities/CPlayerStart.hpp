/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPlayerStart.hpp
 * \date 08/04/2009 Création de la classe CPlayerStart.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression de l'attribut name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CPLAYERSTART_HPP_
#define T_FILE_ENTITIES_CPLAYERSTART_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPointEntity.hpp"


namespace Ted
{

/**
 * \class   CPlayerStart
 * \ingroup Game
 * \brief   Point de départ des joueurs.
 ******************************/

class T_GAME_API CPlayerStart : public IPointEntity
{
public:

    // Constructeurs
    explicit CPlayerStart(const CString& name = CString(), TVector3F angle = Origin3F, ILocalEntity * parent = nullptr);
    CPlayerStart(const CString& name, ILocalEntity * parent);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline TVector3F getAngle() const;

    // Mutateur
    void setAngle(const TVector3F& angle);

    // Méthode publique
    void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    TVector3F m_angle; ///< Angle de l'entité.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CPlayerStart::getClassName() const
{
    return "player_start";
}


/**
 * Donne l'angle de l'entité.
 *
 * \return Angle de l'entité.
 *
 * \sa CPlayerStart::setAngle
 ******************************/

inline TVector3F CPlayerStart::getAngle() const
{
    return m_angle;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CPLAYERSTART_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBasePlayer.hpp
 * \date 11/04/2009 Création de la classe IBasePlayer.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 16/02/2011 Ajout des méthodes onEvent.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création de la méthode isPlayer.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBASEPLAYER_HPP_
#define T_FILE_ENTITIES_IBASEPLAYER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "IBaseCharacter.hpp"
#include "Core/IEventReceiver.hpp"
#include "Core/Events.hpp"


namespace Ted
{

class IBaseWeapon;


/**
 * \class   IBasePlayer
 * \ingroup Game
 * \brief   Classe de base des personnages controlables par le joueur.
 ******************************/

class T_GAME_API IBasePlayer : public IBaseCharacter, public IEventReceiver
{
public:

    // Constructeur et destructeur
    explicit IBasePlayer(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IBasePlayer() = 0;

    // Accesseurs
    virtual inline CString getClassName() const;
    inline IBaseWeapon * getWeapon() const;
    bool hasWeapon(const IBaseWeapon * weapon) const;
    inline bool isWalking() const;
    inline bool isUnderWater() const;
    inline unsigned int getTimeWalking() const;
    inline unsigned int getTimeUnderWater() const;
    virtual inline bool isPlayer() const;

    // Mutateur
    void setWeapon(IBaseWeapon * weapon);

    // Méthodes publiques
    void addWeapon(IBaseWeapon * weapon);
    void removeWeapon(IBaseWeapon * weapon);
    virtual void frame(unsigned int frameTime);
    virtual void onEvent(const CMouseEvent& event);
    virtual void onEvent(const CKeyboardEvent& event);
    void animate(unsigned int frameTime);

private:

    // Données privées
    float m_theta;                         ///< Angle Theta.
    float m_phi;                           ///< Angle Phi.

protected:

    // Données protégées
    IBaseWeapon * m_weapon;                ///< Arme actuellement utilisée.
    bool m_walking;                        ///< Indique si le joueur est en train de marcher.
    bool m_underwater;                     ///< Indique si le joueur est sous l'eau.
    unsigned int m_time_walking;           ///< Durée passé à marcher (en ms).
    unsigned int m_time_underwater;        ///< Durée passé sous l'eau (en ms).
    unsigned int m_time_underwater_health; ///< Durée avant de retirer des points de vie si le joueur est sous l'eau.
    std::vector<IBaseWeapon *> m_weapons;  ///< Liste des armes du joueur.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IBasePlayer::getClassName() const
{
    return "base_player";
}


/**
 * Donne l'arme actuellement utilisée par le joueur.
 *
 * \return Pointeur sur l'arme actuellement utilisée par le joueur.
 *
 * \sa IBasePlayer::setWeapon
 ******************************/

inline IBaseWeapon * IBasePlayer::getWeapon() const
{
    return m_weapon;
}


/**
 * Indique si le joueur est en train de marcher.
 *
 * \return Booléen indiquant si le joueur est en train de marcher.
 ******************************/

inline bool IBasePlayer::isWalking() const
{
    return m_walking;
}


/**
 * Indique si le joueur est sous l'eau.
 *
 * \return Booléen indiquant si le joueur est sous l'eau.
 ******************************/

inline bool IBasePlayer::isUnderWater() const
{
    return m_underwater;
}


/**
 * Donne la durée passé à marcher en millisecondes.
 *
 * \return Durée passé à marcher en millisecondes.
 ******************************/

inline unsigned int IBasePlayer::getTimeWalking() const
{
    return m_time_walking;
}


/**
 * Donne la durée passé sous l'eau en millisecondes.
 *
 * \return Durée passé sous l'eau en millisecondes.
 ******************************/

inline unsigned int IBasePlayer::getTimeUnderWater() const
{
    return m_time_underwater;
}


/**
 * Indique si l'entité est un joueur.
 *
 * \return true.
 ******************************/

inline bool IBasePlayer::isPlayer() const
{
    return true;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASEPLAYER_HPP_

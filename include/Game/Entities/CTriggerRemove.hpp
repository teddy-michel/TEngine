/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerRemove.hpp
 * \date 07/07/2010 Création de la classe CTriggerRemove.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTRIGGERREMOVE_HPP_
#define T_FILE_ENTITIES_CTRIGGERREMOVE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBaseTrigger.hpp"


namespace Ted
{

/**
 * \class   CTriggerRemove
 * \ingroup Game
 * \brief   Trigger qui supprime toutes les entités qui le traversent.
 ******************************/

class T_GAME_API CTriggerRemove : public IBaseTrigger
{
public:

    // Constructeurs
    explicit CTriggerRemove(const CString& name = CString(), ILocalEntity * parent = nullptr);
    explicit CTriggerRemove(ILocalEntity * parent);

    // Accesseur
    virtual CString getClassName() const;

    // Méthode publique
    virtual void frame(unsigned int frameTime);
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTRIGGERREMOVE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerTeleport.hpp
 * \date 28/03/2011 Création de la classe CTriggerTeleport.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTRIGGERTELEPORT_HPP_
#define T_FILE_ENTITIES_CTRIGGERTELEPORT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseTrigger.hpp"


namespace Ted
{

/**
 * \class   CTriggerTeleport
 * \ingroup Game
 * \brief   Trigger qui permet de téléporter les entités qui le traversent.
 ******************************/

class T_GAME_API CTriggerTeleport : public IBaseTrigger
{
public:

    // Constructeur
    explicit CTriggerTeleport(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseur
    virtual CString getClassName() const;

    // Méthode publique
    virtual void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    TVector3F m_destination; ///< Point de destination.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTRIGGERTELEPORT_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CStaticLight.hpp
 * \date 23/11/2010 Création de la classe CStaticEntity.
 * \date 24/11/2010 Ajout du tableau des faces à modifier.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 20/02/2011 Ajout d'un pointeur sur le chargeur de maps.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CSTATICLIGHT_HPP_
#define T_FILE_ENTITIES_CSTATICLIGHT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "Game/Entities/IGlobalEntity.hpp"


namespace Ted
{

class IMap;


/**
 * \class   CStaticLight
 * \ingroup Game
 * \brief   Cette entité permet de gérer des lumières statiques qui peuvent
 *          être allumées ou éteintes.
 *
 * \section Signaux
 * \li onLightOn
 * \li onLightOff
 ******************************/

class T_GAME_API CStaticLight : public IGlobalEntity
{
public:

    // Constructeur
    CStaticLight(const CString& name, IMap * gamedata, bool on = true);

    // Accesseurs
    virtual CString getClassName() const;
    bool isOn() const;

    // Mutateurs
    void turnOn();
    void turnOff();
    void toggle();

    // Méthodes publiques
    void addFace(unsigned short face, unsigned short unit = 2);
    void frame(unsigned int frameTime);
    bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    bool m_on;                              ///< Indique si la lumière est allumée ou éteinte.
    bool m_start_on;                        ///< Indique si la lumière doit être allumée au lancement de la map.
    IMap * m_gamedata;                      ///< Pointeur sur le chargeur de map.
    std::vector<unsigned short> m_faces[4]; ///< Faces à modifier pour ajouter ou enlever des lightmaps.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CSTATICLIGHT_HPP_

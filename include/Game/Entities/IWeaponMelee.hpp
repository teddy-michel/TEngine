/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IWeaponMelee.hpp
 * \date 09/07/2010 Création de la classe IWeaponMelee.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IWEAPONMELEE_HPP_
#define T_FILE_ENTITIES_IWEAPONMELEE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBaseWeapon.hpp"


namespace Ted
{

/**
 * \class   IWeaponMelee
 * \ingroup Game
 * \brief   Classe de base des armes de melée.
 ******************************/

class T_GAME_API IWeaponMelee : public IBaseWeapon
{
public:

    // Constructeur et destructeur
    explicit IWeaponMelee(const CString& name = CString());
    virtual ~IWeaponMelee();

    // Accesseur
    virtual CString getClassName() const;
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IWEAPONMELEE_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CLocalSound.hpp
 * \date 05/02/2010 Création de la classe CLocalSound.
 * \date 10/07/2010 Ajout du nom du fichier contenant le son à jouer.
 * \date 11/07/2010 Gestion des slots et du son.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 05/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CLOCALSOUND_HPP_
#define T_FILE_ENTITIES_CLOCALSOUND_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPointEntity.hpp"
#include "Sound/CMusic.hpp"
#include "Sound/CSound.hpp"


namespace Ted
{

/**
 * \class   CLocalSound
 * \ingroup Game
 * \brief   Joue un son lorsque le joueur s'approche de l'entité.
 *
 * \section Slots
 * \li PlaySound
 * \li PauseSound
 * \li StopSound
 * \li SetVolume(int)
 ******************************/

class T_GAME_API CLocalSound : public IPointEntity
{
public:

    // Constructeur et destructeur
    explicit CLocalSound(const CString& name = CString(), ILocalEntity * parent = nullptr);
    ~CLocalSound();

    // Accesseurs
    inline CString getClassName() const;
    float getVolume() const;
    float getDistance() const;
    bool isLoop() const;
    CString getFilename() const;

    // Mutateurs
    void setVolume(float volume);
    void setDistance(float distance);
    void setLoop(bool loop = true);
    void setFilename(const CString& fileName, bool music = false);
    void playSound();
    void pauseSound();
    void stopSound();

    // Méthodes publiques
    void frame(unsigned int frameTime);
    bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    CSound m_sound;  ///< Son à jouer.
    CMusic m_music;  ///< Musique à jouer.
    bool m_is_music; ///< Indique si le son est une musique.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CLocalSound::getClassName() const
{
    return "local_sound";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CLOCALSOUND_HPP_

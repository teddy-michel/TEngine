/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IPointEntity.hpp
 * \date 08/04/2009 Création de la classe IPointEntity.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 07/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 17/12/2010 Héritage de la classe ILocalEntity.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IPOINTENTITY_HPP_
#define T_FILE_ENTITIES_IPOINTENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "ILocalEntity.hpp"


namespace Ted
{

/**
 * \class   IPointEntity
 * \ingroup Game
 * \brief   Classe de base des entités point.
 *
 * Une entité point possède une position précise, et doit généralement afficher
 * quelque chose à l'endroit demandé (explosion, faisceau, fumée, etc.).
 ******************************/

class T_GAME_API IPointEntity : public ILocalEntity
{
public:

    // Constructeur et destructeur
    explicit IPointEntity(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IPointEntity() = 0;

    // Accesseurs
    virtual CString getClassName() const;
    TVector3F getPosition() const;

    // Mutateur
    void setPosition(const TVector3F& position);

protected:

    // Donnée protégée
    TVector3F m_position; ///< Position de l'entité.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IPOINTENTITY_HPP_

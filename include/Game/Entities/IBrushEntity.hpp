/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBrushEntity.hpp
 * \date 05/02/2010 Création de la classe IBrushEntity.
 * \date 14/07/2010 Déplacement de la méthode WeaponImpact.
 * \date 17/07/2010 Définition de la méthode Frame.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 17/12/2010 Héritage de la classe ILocalEntity.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBRUSHENTITY_HPP_
#define T_FILE_ENTITIES_IBRUSHENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "ILocalEntity.hpp"
#include "Core/Maths/CQuaternion.hpp"


namespace Ted
{

class IModel;
class IBaseCharacter;
class IBaseWeapon;


/**
 * \class   IBrushEntity
 * \ingroup Game
 * \brief   Classe de base des entités occupant un volume.
 ******************************/

class T_GAME_API IBrushEntity : public ILocalEntity
{
public:

    // Constructeurs et destructeur
    explicit IBrushEntity(const CString& name = CString(), IModel * model = nullptr, ILocalEntity * parent = nullptr);
    IBrushEntity(const CString& name, ILocalEntity * parent);
    explicit IBrushEntity(ILocalEntity * parent);
    virtual ~IBrushEntity() = 0;

    // Accesseurs
    virtual inline CString getClassName() const;
    IModel * getModel() const;
    TVector3F getPosition() const;

    // Mutateurs
    virtual void setModel(IModel * model);
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& translate);
    void setRotation(const CQuaternion& rotation);

    // Méthode publique
    virtual void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    IModel * m_model; ///< Pointeur sur le modèle lié à l'entité.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IBrushEntity::getClassName() const
{
    return "brush_entity";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBRUSHENTITY_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CGlobalSound.hpp
 * \date 07/07/2010 Création de la classe CGlobalSound.
 * \date 11/07/2010 Gestion des slots et du son.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CGLOBALSOUND_HPP_
#define T_FILE_ENTITIES_CGLOBALSOUND_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IGlobalEntity.hpp"
#include "Sound/CMusic.hpp"
#include "Sound/CSound.hpp"


namespace Ted
{

/**
 * \class   CGlobalSound
 * \ingroup Game
 * \brief   Son qui peut être entendu quelle que soit la position de la caméra.
 *
 * \section Slots
 * \li PlaySound
 * \li PauseSound
 * \li StopSound
 * \li SetVolume(int)
 ******************************/

class T_GAME_API CGlobalSound : public IGlobalEntity
{
public:

    // Constructeur
    explicit CGlobalSound(const CString& name = CString());

    // Accesseurs
    virtual CString getClassName() const;
    float getVolume() const;
    bool isLoop() const;
    CString getFilename() const;

    // Mutateurs
    void setVolume(float volume);
    void setLoop(bool loop = true);
    void setFilename(const CString& fileName, bool music = false);

    void playSound();
    void pauseSound();
    void stopSound();

    // Méthodes publiques
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    CSound m_sound;  ///< Son à jouer.
    CMusic m_music;  ///< Musique à jouer.
    bool m_is_music; ///< Indique si le son est une musique.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CGLOBALSOUND_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CDynamicLight.hpp
 * \date 08/04/2009 Création de la classe CDynamicLight.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 16/11/2010 Ajout des paramètres pour créer un spot.
 * \date 23/11/2010 CLight est renommée en CDynamicLight.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CDYNAMICLIGHT_HPP_
#define T_FILE_ENTITIES_CDYNAMICLIGHT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IPointEntity.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CDynamicLight
 * \ingroup Game
 * \brief   Cette entité représente une lumière dynamique.
 *
 * \section Slots
 * \li TurnOn
 * \li TurnOff
 * \li Toggle
 *
 * \section Signaux
 * \li onLightOn
 * \li onLightOff
 ******************************/

class T_GAME_API CDynamicLight : public IPointEntity
{
public:

    // Constructeurs
    explicit CDynamicLight(const CString& name = CString(), bool on = true, ILocalEntity * parent = nullptr);
    CDynamicLight(const CString& name, ILocalEntity * parent);
    explicit CDynamicLight(ILocalEntity * parent);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline bool isOn() const;
    inline CColor getColor() const;
    inline unsigned int getInner() const;
    inline unsigned int getOuter() const;
    inline TVector3F getDirection() const;

    // Mutateurs
    void turnOn();
    void turnOff();
    void toggle();
    void setColor(const CColor& color);
    void setInner(unsigned int inner);
    void setOuter(unsigned int outer);
    void setDirection(const TVector3F& direction);

    // Méthodes publiques
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    bool m_on;             ///< Indique si la lumière est allumée ou éteinte.
    CColor m_color;        ///< Couleur de la lumière.
    unsigned int m_inner;  ///< Angle du cone dans lequel la lumière est maximale.
    unsigned int m_outer;  ///< Angle du cone contenant la lumière.
    TVector3F m_direction; ///< Axe du cône.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CDynamicLight::getClassName() const
{
    return "dynamic_light";
}


/**
 * Accesseur pour on.
 *
 * \return Booléen indiquant si la lumière allumée ou éteinte.
 ******************************/

inline bool CDynamicLight::isOn() const
{
    return m_on;
}


/**
 * Accesseur pour color.
 *
 * \return Couleur et intensité de la lumière.
 *
 * \sa CDynamicLight::setColor
 ******************************/

inline CColor CDynamicLight::getColor() const
{
    return m_color;
}


/**
 * Accesseur pour inner.
 *
 * \return Angle du cone dans lequel la lumière est maximale.
 *
 * \sa CDynamicLight::setInner
 ******************************/

inline unsigned int CDynamicLight::getInner () const
{
    return m_inner;
}


/**
 * Accesseur pour outer.
 *
 * \return Angle du cone contenant la lumière.
 *
 * \sa CDynamicLight::setOuter
 ******************************/

inline unsigned int CDynamicLight::getOuter () const
{
    return m_outer;
}


/**
 * Accesseur pour direction.
 *
 * \return Axe du cone.
 *
 * \sa CDynamicLight::setDirection
 ******************************/

inline TVector3F CDynamicLight::getDirection () const
{
    return m_direction;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CDYNAMICLIGHT_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CSprite.hpp
 * \date 09/02/2010 Création de la classe CSprite.
 * \date 07/07/2010 Ajout des informations sur le rectangle à afficher.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 11/03/2011 Ajout du paramètre parent au constructeur.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CSPRITE_HPP_
#define T_FILE_ENTITIES_CSPRITE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IPointEntity.hpp"
#include "Core/Maths/CMatrix4.hpp"


namespace Ted
{

class CBuffer;


/**
 * \class   CSprite
 * \ingroup Game
 * \brief   Affiche un billboard ou un impostor.
 *
 * Un billboard fait face à la caméra mais est toujours vertical (rotation
 * autour de l'axe Z), alors qu'un impostor fait toujours face à la caméra.
 ******************************/

class T_GAME_API CSprite : public IPointEntity
{
public:

    // Constructeur et destructeur
    explicit CSprite(ILocalEntity * parent);
    explicit CSprite(const CString& name = CString(), ILocalEntity * parent = nullptr);
    ~CSprite();

    // Accesseurs
    inline virtual CString getClassName() const;
    inline unsigned int getTextureId() const;
    inline bool isImpostor() const;
    inline float getHeight() const;
    inline float getWidth() const;
    inline CBuffer * getBuffer() const;

    // Mutateurs
    void setTextureId(unsigned int texture);
    void setImpostor(bool impostor = true);
    void setHeight(float height);
    void setWidth(float width);
    void setSize(float width, float height);

    // Méthode publique
    void frame(unsigned int frameTime);

private:

    // Méthode privée
    void updateBuffer();

protected:

    // Données protégées
    unsigned int m_texture; ///< Identifiant de la texture à utiliser.
    bool m_impostor;        ///< Indique si on doit toujours faire face à la caméra.
    float m_height;         ///< Hauteur du rectangle divisée par deux.
    float m_width;          ///< Largeur du rectangle divisée par deux.
    CBuffer * m_buffer;     ///< Buffer graphique pour afficher le sprite.
    TMatrix4F m_matrix;     ///< Matrice de vue de l'entité.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CSprite::getClassName() const
{
    return "sprite";
}


/**
 * Donne l'identifiant de la texture à utiliser.
 *
 * \return Identifiant de la texture.
 *
 * \sa CSprite::setTextureId
 ******************************/

inline unsigned int CSprite::getTextureId() const
{
    return m_texture;
}


/**
 * Indique si le sprite fait toujours face à la caméra.
 *
 * \return Booléen.
 *
 * \sa CSprite::setImpostor
 ******************************/

inline bool CSprite::isImpostor() const
{
    return m_impostor;
}


/**
 * Donne la hauteur du rectangle.
 *
 * \return Hauteur du rectangle divisée par deux.
 *
 * \sa CSprite::setHeight
 ******************************/

inline float CSprite::getHeight() const
{
    return m_height;
}


/**
 * Donne la largeur du rectangle.
 *
 * \return Largeur du rectangle divisée par deux.
 *
 * \sa CSprite::setWidth
 ******************************/

inline float CSprite::getWidth() const
{
    return m_width;
}

/**
 * Donne le buffer graphique utilisé pour afficher le sprite.
 *
 * \return Pointeur sur le buffer graphique.
 ******************************/

inline CBuffer * CSprite::getBuffer() const
{
    return m_buffer;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CSPRITE_HPP_

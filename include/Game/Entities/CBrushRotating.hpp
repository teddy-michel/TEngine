/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBrushRotating.hpp
 * \date 07/02/2010 Création de la classe CBrushRotating.
 * \date 16/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètren name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CBRUSHROTATING_HPP_
#define T_FILE_ENTITIES_CBRUSHROTATING_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBrushMoving.hpp"


namespace Ted
{

/**
 * \class   CBrushRotating
 * \ingroup Game
 * \brief   Brush pouvant tourner autour d'un axe.
 ******************************/

class T_GAME_API CBrushRotating : public IBrushMoving
{
public:

    // Constructeurs
    explicit CBrushRotating(const CString& name = CString(), ILocalEntity * parent = nullptr);
    explicit CBrushRotating(ILocalEntity * parent);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline TVector3F getOrigin() const;
    inline float getAngleMax() const;

    // Mutateurs
    void setOrigin(const TVector3F& origin);
    void setAngleMax(float angle);

    // Méthode publique
    virtual void frame(unsigned int frameTime);

protected:

    // Données protégées
    TVector3F m_origin; ///< Axe de rotation.
    float m_angle_max;  ///< Angle maximal de rotation.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CBrushRotating::getClassName() const
{
    return "brush_rotating";
}


/**
 * Accesseur pour origin.
 *
 * \return Origine définissant l'axe de rotation.
 *
 * \sa CBrushRotating::setOrigin
 ******************************/

inline TVector3F CBrushRotating::getOrigin() const
{
    return m_origin;
}


/**
 * Accesseur pour angle_max.
 *
 * \return Angle maximal de rotation.
 *
 * \sa CBrushRotating::setAngleMax
 ******************************/

inline float CBrushRotating::getAngleMax() const
{
    return m_angle_max;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CBRUSHROTATING_HPP_

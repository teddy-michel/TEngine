/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBasicPlayer.hpp
 * \date 30/04/2012 Création de la classe CBasicPlayer.
 */

#ifndef T_FILE_ENTITIES_CBASICPLAYER_HPP_
#define T_FILE_ENTITIES_CBASICPLAYER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBasePlayer.hpp"


namespace Ted
{

class T_GAME_API CBasicPlayer : public IBasePlayer
{
public:

    // Constructeur et destructeur
    explicit CBasicPlayer(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~CBasicPlayer();

    inline virtual CString getClassName() const;
};


inline CString CBasicPlayer::getClassName() const
{
    return "basic_player";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CBASICPLAYER_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CMapLoad.hpp
 * \date 18/04/2009 Création de la classe CMapLoad.
 * \date 17/07/2010 Ajout de la méthode getClassName et utilisation possible des signaux.
 * \date 23/07/2010 La méthode getClassName est déclarée inline.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CMAPLOAD_HPP_
#define T_FILE_ENTITIES_CMAPLOAD_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IGlobalEntity.hpp"


namespace Ted
{

/**
 * \class   CMapLoad
 * \ingroup Game
 * \brief   Entité appellée à la fin du chargement de la map.
 *
 * \section Signaux
 * \li onMapLoad
 ******************************/

class T_GAME_API CMapLoad : public IGlobalEntity
{
public:

    // Constructeur
    explicit CMapLoad(const CString& name = CString());

    // Accesseur
    virtual CString getClassName() const;

    // Méthode publique
    void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    bool m_load; ///< Indique si le chargement est terminé.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CMAPLOAD_HPP_

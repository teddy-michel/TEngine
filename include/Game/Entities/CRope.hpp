/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CRope.hpp
 * \date 09/02/2010 Création de la classe CRope.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CROPE_HPP_
#define T_FILE_ENTITIES_CROPE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IPointEntity.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CRope
 * \ingroup Game
 * \brief   Affiche un cable entre deux points.
 ******************************/

class T_GAME_API CRope : public IPointEntity
{
public:

    // Constructeur
    explicit CRope(const CString& name = CString());

    // Accesseurs
    CString getClassName() const;
    TVector3F getPoint() const;
    CColor getColor() const;
    unsigned short getSubdiv() const;

    // Mutateurs
    void setPoint(const TVector3F& point);
    void setColor(const CColor& color);
    void setSubdiv(unsigned short subdiv);

    // Méthode publique
    void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    TVector3F m_point;       ///< Point d'arrivée du cable.
    CColor m_color;          ///< Couleur du cable (noire par défaut).
    unsigned short m_subdiv; ///< Nombre de subdivisions (8 par défaut).
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CROPE_HPP_

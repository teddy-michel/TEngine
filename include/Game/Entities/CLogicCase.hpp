/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CLogicCase.hpp
 * \date 18/11/2010 Création de la classe CLogicCase.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 13/01/2011 Ajout des signaux.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CLOGICCASE_HPP_
#define T_FILE_ENTITIES_CLOGICCASE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IGlobalEntity.hpp"


namespace Ted
{

/**
 * \class   CLogicCase
 * \ingroup Game
 * \brief   Compare la valeur envoyée par une connexion à une des valeurs
 *          prédéfinies, et envoit le signal correspondant.
 *
 * \section Slots
 * \li Value(int)
 * \li RandomCase
 *
 * \section Signaux
 * \li onCase01
 * \li onCase02
 * \li onCase03
 * \li onCase04
 * \li onCase05
 * \li onCase06
 * \li onCase07
 * \li onCase08
 * \li onCase09
 * \li onCase10
 * \li onCase11
 * \li onCase12
 * \li onCase13
 * \li onCase14
 * \li onCase15
 * \li onCase16
 * \li onDefault
 ******************************/

class T_GAME_API CLogicCase : public IGlobalEntity
{
public:

    // Constructeur
    explicit CLogicCase(const CString& name = CString());

    // Accesseurs
    CString getClassName() const;
    float getValue(int numCase) const;

    // Mutateur
    void setValue(int numCase, float value);

    // Méthodes publiques
    void frame(unsigned int frameTime);
    bool callSlot(const CString& slot, const CString& param = CString());

private:

    // Méthode privée
    void sendSignalForValue(int c);

protected:

    /**
     * \struct  TCase
     * \ingroup Game
     * \brief   Représente une case.
     ******************************/

    struct TCase
    {
        float value; ///< Valeur de la case.
        bool active; ///< Indique si la case est active.
    };

    // Donnée protégée
    TCase m_cases[16]; ///< Liste des valeurs.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CLOGICCASE_HPP_

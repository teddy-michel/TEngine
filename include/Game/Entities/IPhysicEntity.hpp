/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IPhysicEntity.hpp
 * \date 12/04/2009 Création de la classe IPhysicEntity.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IPHYSICENTITY_HPP_
#define T_FILE_ENTITIES_IPHYSICENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseAnimating.hpp"


namespace Ted
{

/**
 * \class   IPhysicEntity
 * \ingroup Game
 * \brief   Classe de base des entités soumises à la physique.
 ******************************/

class T_GAME_API IPhysicEntity : public IBaseAnimating
{
public:

    // Constructeurs et destructeur
    explicit IPhysicEntity(ILocalEntity * parent);
    explicit IPhysicEntity(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IPhysicEntity() = 0;

    // Accesseur
    inline virtual CString getClassName() const;
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IPhysicEntity::getClassName() const
{
    return "physic_entity";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IPHYSICENTITY_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerChangeLevel.hpp
 * \date 07/07/2010 Création de la classe CTriggerChangeLevel.
 * \date 13/07/2010 Ajout du paramètre level.
 * \date 16/07/2010 Ajout du slot et du signal.
 * \date 20/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTRIGGERCHANGELEVEL_HPP_
#define T_FILE_ENTITIES_CTRIGGERCHANGELEVEL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseTrigger.hpp"


namespace Ted
{

/**
 * \class   CTriggerChangeLevel
 * \ingroup Game
 * \brief   Trigger qui permet de changer de niveau.
 *
 * \section Slots
 * \li ChangeLevel
 *
 * \section Signaux
 * \li onChangeLevel
 ******************************/

class T_GAME_API CTriggerChangeLevel : public IBaseTrigger
{
public:

    // Constructeur
    explicit CTriggerChangeLevel(const CString& name = CString(), ILocalEntity * parent = nullptr);
    explicit CTriggerChangeLevel(ILocalEntity * parent);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline CString getLevel() const;

    // Mutateur
    void setLevel(const CString& level);

    // Méthodes publiques
    void changeLevel();
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Donnée protégée
    CString m_level; ///< Nom du niveau à charger.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CTriggerChangeLevel::getClassName() const
{
    return "trigger_change_level";
}


/**
 * Donne le nom du niveau à charger.
 *
 * \return Nom du niveau à charger.
 ******************************/

inline CString CTriggerChangeLevel::getLevel() const
{
    return m_level;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTRIGGERCHANGELEVEL_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseCharacter.hpp
 * \date 17/04/2009 Création de la classe IBaseCharacter.
 * \date 13/07/2010 Le nombre de points de vie est déplacé vers la classe IBaseAnimating.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBASECHARACTER_HPP_
#define T_FILE_ENTITIES_IBASECHARACTER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseAnimating.hpp"


namespace Ted
{

/**
 * \class   IBaseCharacter
 * \ingroup Game
 * \brief   Classe de base des entités vivantes.
 ******************************/

class T_GAME_API IBaseCharacter : public IBaseAnimating
{
public:

    // Constructeur
    explicit IBaseCharacter(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline float getAngleH() const;
    inline float getAngleV() const;

    // Mutateur
    virtual void setModel(IModel * model);
    void setAngleH(float angleH);
    void setAngleV(float angleV);
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& translate);

    // Méthode publique
    void weaponImpact(float damage, IBaseCharacter * character, IBaseWeapon * weapon);

protected:

    // Donnée protégée
    float m_angleH; ///< Angle d'observation horizontal du personnage.
    float m_angleV; ///< Angle d'observation vertical du personnage.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IBaseCharacter::getClassName() const
{
    return "base_character";
}


inline float IBaseCharacter::getAngleH() const
{
    return m_angleH;
}


inline float IBaseCharacter::getAngleV() const
{
    return m_angleV;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASECHARACTER_HPP_

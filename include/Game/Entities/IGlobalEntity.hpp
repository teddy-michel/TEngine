/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IGlobalEntity.hpp
 * \date 06/02/2010 Création de la classe IGlobalEntity.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IGLOBALENTITY_HPP_
#define T_FILE_ENTITIES_IGLOBALENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IEntity.hpp"


namespace Ted
{

/**
 * \class   IGlobalEntity
 * \ingroup Game
 * \brief   Classe de base des entités non localisées.
 ******************************/

class T_GAME_API IGlobalEntity : public IEntity
{
public:

    // Constructeur et destructeur
    explicit IGlobalEntity(const CString& name = CString());
    virtual ~IGlobalEntity();

    // Accesseur
    virtual CString getClassName() const;
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IGLOBALENTITY_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CSpark.hpp
 * \date 10/02/2010 Création de la classe CSpark.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 26/07/2010 La méthode getClassName est déclarée inline.
 *                  Ajout des paramètres color, delay et time.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CSPARK_HPP_
#define T_FILE_ENTITIES_CSPARK_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPointEntity.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CSpark
 * \ingroup Game
 * \brief   Lance des étincelles à intervalle variable.
 ******************************/

class T_GAME_API CSpark : public IPointEntity
{
public:

    // Constructeur
    explicit CSpark(const CString& name = CString());

    // Accesseur
    inline virtual CString getClassName() const;
    inline unsigned int getTime() const;
    inline unsigned short getDelay() const;
    inline CColor getColor() const;

    // Mutateurs
    void setDelay(unsigned short delay);
    void setColor(const CColor& color);

    // Méthode publique
    void frame(unsigned int frameTime);

protected:

    // Données protégées
    unsigned int m_time;    ///< Durée depuis laquelle les particules ont été émises pour la dernière fois.
    unsigned short m_delay; ///< Durée entre deux émissions de particules.
    CColor m_color;         ///< Couleur des particules.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CSpark::getClassName() const
{
    return "spark";
}


/**
 * Donne le temps écoulé depuis la dernière émission de particules.
 *
 * \return Temps écoulé en millisecondes.
 ******************************/

inline unsigned int CSpark::getTime() const
{
    return m_time;
}


/**
 * Donne la durée moyenne entre deux émissions de particules.
 *
 * \return Durée moyenne en millisecondes.
 *
 * \sa CSpark::setDelay
 ******************************/

inline unsigned short CSpark::getDelay() const
{
    return m_delay;
}


/**
 * Donne la couleur des particules.
 *
 * \return Couleur des particules.
 *
 * \sa CSpark::setColor
 ******************************/

inline CColor CSpark::getColor() const
{
    return m_color;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CSPARK_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseNPC.hpp
 * \date 17/04/2009 Création de la classe IBaseNPC.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création de la méthode isNPC.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBASENPC_HPP_
#define T_FILE_ENTITIES_IBASENPC_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBaseCharacter.hpp"


namespace Ted
{

/**
 * \class   IBaseNPC
 * \ingroup Game
 * \brief   Classe de base des NPC (personnage non contrôlables par le joueur).
 ******************************/

class T_GAME_API IBaseNPC : public IBaseCharacter
{
public:

    // Constructeur et destructeur
    explicit IBaseNPC(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IBaseNPC() = 0;

    // Accesseurs
    inline virtual CString getClassName() const;
    inline virtual bool isNPC() const;

    // Méthode publique
    virtual void frame(unsigned int frameTime);
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IBaseNPC::getClassName() const
{
    return "base_npc";
}


/**
 * Indique si l'entité est un NPC.
 *
 * \return true.
 ******************************/

inline bool IBaseNPC::isNPC() const
{
    return true;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASENPC_HPP_

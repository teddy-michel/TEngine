/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBrushTranslating.hpp
 * \date 07/02/2010 Création de la classe CBrushTranslating.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CBRUSHTRANSLATING_HPP_
#define T_FILE_ENTITIES_CBRUSHTRANSLATING_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBrushMoving.hpp"


namespace Ted
{

/**
 * \class   CBrushTranslating
 * \ingroup Game
 * \brief   Brush pouvant se translater d'une position à une autre.
 ******************************/

class T_GAME_API CBrushTranslating : public IBrushMoving
{
public:

    // Constructeurs
    explicit CBrushTranslating(const CString& name = CString(), ILocalEntity * parent = nullptr);
    explicit CBrushTranslating(ILocalEntity * parent);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline TVector3F getDirection() const;

    // Mutateur
    void setDirection(const TVector3F& direction);

    // Méthode publique
    void frame(unsigned int frameTime);

private:

    // Donnée privée
    TVector3F m_direction; ///< Vecteur de translation du brush.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CBrushTranslating::getClassName() const
{
    return "brush_translating";
}


/**
 * Accesseur pour direction.
 *
 * \return Position finale du brush.
 *
 * \sa CBrushTranslating::setDirection
 ******************************/

inline TVector3F CBrushTranslating::getDirection() const
{
    return m_direction;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CBRUSHTRANSLATING_HPP_

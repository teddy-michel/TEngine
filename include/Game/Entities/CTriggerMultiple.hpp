/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerMultiple.hpp
 * \date 09/04/2009 Création de la classe CTriggerMultiple.
 * \date 16/11/2010 Ajout d'un compteur d'activation du trigger.
 *                  Ajout d'un nombre maximal d'activations possibles.
 *                  Ajout de la méthode getClassName.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTRIGGERMULTIPLE_HPP_
#define T_FILE_ENTITIES_CTRIGGERMULTIPLE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseTrigger.hpp"


namespace Ted
{

/**
 * \class   CTriggerMultiple
 * \ingroup Game
 * \brief   Trigger qui s'active à chaque passage d'une entité.
 ******************************/

class T_GAME_API CTriggerMultiple : public IBaseTrigger
{
public:

    // Constructeur
    explicit CTriggerMultiple(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline unsigned int getCount() const;
    inline unsigned int getMaxCount() const;

    // Mutateur
    void setMaxCount(unsigned int max);

    // Méthodes publiques
    void trigger(IBaseAnimating * entity);
    void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    unsigned int m_count;     ///< Nombre d'activations du trigger.
    unsigned int m_max_count; ///< Nombre maximal d'activations possibles.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CTriggerMultiple::getClassName() const
{
    return "trigger_multiple";
}


/**
 * Donne le nombre d'activations du trigger.
 *
 * \return Nombre d'activations.
 ******************************/

inline unsigned int CTriggerMultiple::getCount() const
{
    return m_count;
}


/**
 * Donne le nombre maximal d'activations du trigger.
 *
 * \return Nombre maximal d'activations.
 *
 * \sa CTriggerMultiple::setMaxCount
 ******************************/

inline unsigned int CTriggerMultiple::getMaxCount() const
{
    return m_max_count;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTRIGGERMULTIPLE_HPP_

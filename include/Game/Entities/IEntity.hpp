/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IEntity.hpp
 * \date 08/04/2009 Création de la classe IEntity.
 * \date 16/07/2010 Ajout de la méthode getClassName.
 * \date 02/12/2010 Ajout du paramètre identifiant.
 * \date 06/12/2010 Ajout de la liste des entités enfant.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/12/2010 Ajout de la méthode isChildHierarchy.
 * \date 17/12/2010 Le graphe de scène est géré par la classe dérivée ILocalEntity.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IENTITY_HPP_
#define T_FILE_ENTITIES_IENTITY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "CConnection.hpp"
#include "Core/CString.hpp"


namespace Ted
{

/**
 * \class   IEntity
 * \ingroup Game
 * \brief   Classe de base des entités.
 *
 * \todo    Créer les méthodes Save et Restore.
 *
 * Une entité représente tout ce qui peut être utilisé dans le jeu : les personnages,
 * les armes, les objets à ramasser, les lumières, les modèles, le soleil, etc.
 *
 * Chaque entité est identifiée par un nom, et peut ainsi être appelée par une
 * autre entité. Certains noms ne doivent pas être utilisés, pour éviter un
 * disfonctionnement de l'application (voir la liste ci-dessous).
 *
 * Il existe deux types d'entités : celles qui ont une position définie dans la
 * map (ILocalEntity) et qui sont placées dans un graphe de scène, et les
 * autres (IGlobalEntity).
 *
 * Les entités peuvent émettre des signaux et en recevoir d'autres entités. Un signal
 * est représenté par un nom (une chaine de caractères). Une connection entre une
 * entité et une autre requiert le nom du signal, le nom du slot (la fonction à
 * appeler), une durée entre l'envoi du signal et l'activation du slot, et
 * éventuellement des paramètres représentés par une chaine de caractères.
 *
 * Les termes signal et slot sont équivalents à output et input dans le Source Engine.
 *
 * \section Noms réservés
 * \li player
 * \li caller
 * \li tous les noms contenant les caractères '*' ou '?'
 ******************************/

class T_GAME_API IEntity
{
public:

    // Constructeur et destructeur
    explicit IEntity(const CString& name = CString());
    virtual ~IEntity();

    // Accesseurs
    virtual CString getClassName() const;
    CString getName() const;

    // Méthodes publiques
    unsigned int getNumConnections() const;
    virtual void frame(unsigned int frameTime) = 0;
    virtual bool callSlot(const CString& slot, const CString& param = CString());
    void addConnection(const CString& signal, const CConnection& connection);
    //virtual bool Save(std::vector<unsigned char>& data) = 0;
    //virtual bool Restore(const std::vector<unsigned char>& data) = 0;

protected:

    // Méthode protégée
    void sendSignal(const CString& name, IEntity * caller = nullptr) const;

    // Données protégées
    CString m_name;               ///< Nom de l'entité.
    TConnectionMap m_connections; ///< Liste des connexions de l'entité.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IENTITY_HPP_

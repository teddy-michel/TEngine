/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBeam.hpp
 * \date 09/02/2010 Création de la classe CLaser.
 * \date 11/07/2010 La classe CLaser est renommée en CBeam.
 * \date 13/07/2010 La copie directe est interdite.
 * \date 16/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Plus de paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CBEAM_HPP_
#define T_FILE_ENTITIES_CBEAM_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPointEntity.hpp"
#include "Graphic/CColor.hpp"
#include "Physic/CHitBox.hpp"


namespace Ted
{

class CBuffer;


/**
 * \class   CBeam
 * \ingroup Game
 * \brief   Affiche un faisceau entre deux points.
 ******************************/

class T_GAME_API CBeam : public IPointEntity
{
public:

    // Constructeurs et destructeur
    explicit CBeam(const CString& name = CString(), ILocalEntity * parent = nullptr);
    explicit CBeam(ILocalEntity * parent);
    virtual ~CBeam();

    // Accesseurs
    inline virtual CString getClassName() const;
    inline TVector3F getPoint() const;
    CColor getColor() const;
    float getDamage() const;
    float getWidth() const;
    CBuffer * getBuffer() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void setPoint(const TVector3F& point);
    void setColor(const CColor& color);
    void setDamage(float damage);
    void setWidth(float width);

    // Méthode publique
    virtual void frame(unsigned int frameTime);

protected:

    // Données protégées
    TVector3F m_point;  ///< Point d'arrivée du faisceau.
    CColor m_color;     ///< Couleur du faisceau (rouge par défaut).
    float m_damage;     ///< Nombre de points de dommage par seconde pour les entités qui traversent le faisceau.
    float m_width;      ///< Largeur du faisceau (1 par défaut).
    CHitBox m_box;      ///< Boite orientée contenant le faisceau.
    CBuffer * m_buffer; ///< Buffer graphique pour afficher le faisceau.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CBeam::getClassName() const
{
    return "beam";
}


/**
 * Donne le point d'arrivé du faisceau.
 *
 * \return Point d'arrivée du faisceau.
 *
 * \sa CBeam::setPoint
 ******************************/

inline TVector3F CBeam::getPoint() const
{
    return m_point;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CBEAM_HPP_

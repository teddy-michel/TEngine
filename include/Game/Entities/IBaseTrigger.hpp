/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseTrigger.hpp
 * \date 09/04/2009 Création de la classe IBaseTrigger.
 * \date 11/07/2010 Ajout de la liste des entités du trigger.
 * \date 13/07/2010 Suppression du paramètre start_disable.
 *                  Modification de la gestion des entités présentes dans le trigger.
 * \date 16/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 13/01/2011 Ajout des signaux.
 * \date 02/03/2011 Création de l'énumération TTriggerFilter.
 *                  Ajout d'un filtre à appliquer aux entités traversant le trigger.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 23/03/2012 Utilisation de la classe CFlags.
 */

#ifndef T_FILE_ENTITIES_IBASETRIGGER_HPP_
#define T_FILE_ENTITIES_IBASETRIGGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Game/Export.hpp"
#include "Game/Entities/IBrushEntity.hpp"
#include "Core/CFlags.hpp"


namespace Ted
{

enum TTriggerFilter
{
    FilterPlayer     = 0x0001, ///< Le trigger réagit au passage d'un joueur.
    FilterNPC        = 0x0002, ///< Le trigger réagit au passage d'un NPC.
    FilterEverything = 0x1000  ///< Le trigger réagit au passage de toutes les entités.
};

T_DECLARE_FLAGS(TTriggerFilters, TTriggerFilter)

T_DECLARE_OPERATORS_FOR_FLAGS(TTriggerFilters)


class IBaseAnimating;


/**
 * \class   IBaseTrigger
 * \ingroup Game
 * \brief   Classe de base des entités trigger.
 *
 * Un trigger représente un volume de la map qui s'active lorsqu'une entité la
 * traverse. Le trigger peut ensuite envoyer des signaux, ou déclencher une
 * action particulière sur les entités qui le traversent.
 *
 * Un trigger peut être désactivé. Dans ce cas, il continue de gérer la liste
 * des entités qui le traversent, mais ne fait rien en réaction.
 *
 * \section Slots
 * \li Enable
 * \li Disable
 * \li Toggle
 *
 * \section Signaux
 * \li onStartTouch
 * \li onEndTouch
 * \li onTrigger
 * \li onEndTouchAll
 ******************************/

class T_GAME_API IBaseTrigger : public IBrushEntity
{
public:

    // Constructeurs et destructeur
    explicit IBaseTrigger(const CString& name = CString(), bool enable = true, ILocalEntity * parent = nullptr);
    IBaseTrigger(const CString& name, ILocalEntity * parent);
    virtual ~IBaseTrigger() = 0;

    // Accesseurs
    virtual CString getClassName() const;
    bool isEnable() const;
    TTriggerFilters getFilter() const;

    // Mutateurs
    void setFilter(TTriggerFilters filter);
    void AddFilter(TTriggerFilter filter);

    // Méthodes publiques
    unsigned int getNumEntities() const;
    void enable();
    void disable();
    void toggle();
    void trigger(IBaseAnimating * entity);
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    /**
     * \struct  TTriggerEntity
     * \ingroup Game
     * \brief   Description d'une entité qui se trouve à l'intérieur d'un trigger.
     ******************************/

    struct TTriggerEntity
    {
        IBaseAnimating * entity; ///< Pointeur sur l'entité.
        unsigned int time;       ///< Durée passée dans le trigger en millisecondes.
        bool inside;             ///< Indique si l'entité est actuellement dans le trigger.

        /// Constructeur par défaut.
        TTriggerEntity (IBaseAnimating * p_entity = nullptr, unsigned int p_time = 0, bool p_inside = true) :
            entity (p_entity), time (p_time), inside (p_inside) { }
    };

    typedef std::list<TTriggerEntity> TTriggerEntityList;


    // Données protégées
    bool m_enable;                 ///< Indique si le trigger est actif ou pas.
    TTriggerFilters m_filter;      ///< Filtre les entités pouvant activer le trigger.
    TTriggerEntityList m_entities; ///< Liste des entités à l'intérieur du trigger.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASETRIGGER_HPP_

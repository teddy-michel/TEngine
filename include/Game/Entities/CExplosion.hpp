/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CExplosion.hpp
 * \date 07/07/2010 Création de la classe CExplosion.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 05/12/2010 Plus de méthodes inline, et ajout de paramètres.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 02/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création du signal onExplode.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CEXPLOSION_HPP_
#define T_FILE_ENTITIES_CEXPLOSION_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPointEntity.hpp"
#include "Core/Maths/CMatrix4.hpp"


namespace Ted
{

class CSprite;


/**
 * \class   CExplosion
 * \ingroup Game
 * \brief   Pour utiliser une explosion.
 *
 * \section Signaux
 * \li onExplode
 ******************************/

class T_GAME_API CExplosion : public IPointEntity
{
public:

    // Constructeur et destructeur
    explicit CExplosion(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~CExplosion();

    // Accesseurs
    inline virtual CString getClassName() const;
    inline bool isDamage() const;
    inline bool isRepeatable() const;
    inline float getRadius() const;
    inline CString getSound() const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void setDamage(bool damage = true);
    void setRepeatable(bool repeatable = true);
    void setSound(const CString& sound);

    // Méthodes publiques
    void explode();
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    bool m_damage;       ///< Indique si l'explosion entraine des dégâts.
    bool m_repeatable;   ///< Indique si l'explosion peut se produire plusieurs fois.
    bool m_done;         ///< Indique si l'explosion s'est produite.
    float m_radius;      ///< Rayon de la sphère dans laquelle les dégâts se font ressentir.
    CString m_sound;     ///< Nom du son à joué pendant l'explosion.
    CSprite * m_sprite;  ///< Pointeur sur le sprite à afficher pendant l'explosion.
    TMatrix4F m_matrix;  ///< Matrice de transformation.
    unsigned int m_time; ///< Durée avant la fin de l'explosion en millisecondes.
};


/**
 * Donne le nom de _la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CExplosion::getClassName() const
{
    return "explosion";
}


/**
 * Indique si l'explosion entraine des dégâts.
 *
 * \return Booléen.
 *
 * \sa CExplosion::setDamage
 ******************************/

inline bool CExplosion::isDamage() const
{
    return m_damage;
}


/**
 * Indique si l'explosion peut se produire plusieurs fois.
 *
 * \return Booléen.
 *
 * \sa CExplosion::setRepeatable
 ******************************/

inline bool CExplosion::isRepeatable() const
{
    return m_repeatable;
}


/**
 * Donne le rayon de la sphère dans laquelle les dégâts se font ressentir.
 *
 * \return Rayon de la sphère.
 ******************************/

inline float CExplosion::getRadius() const
{
    return m_radius;
}


/**
 * Donne le nom du son à jouer pendant l'explosion.
 *
 * \return Nom du son.
 *
 * \sa CExplosion::setSound
 ******************************/

inline CString CExplosion::getSound() const
{
    return m_sound;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CEXPLOSION_HPP_

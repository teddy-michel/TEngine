/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTeam.hpp
 * \date 21/05/2009 Création de la classe CTeam.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTEAM_HPP_
#define T_FILE_ENTITIES_CTEAM_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "Game/Entities/IGlobalEntity.hpp"


namespace Ted
{


class IBasePlayer;


/**
 * \class   CTeam
 * \ingroup Game
 * \brief   Classe représentant une équipe.
 ******************************/

class T_GAME_API CTeam : public IGlobalEntity
{
public:

    // Constructeur et destructeur
    explicit CTeam(const CString& name = CString());
    virtual ~CTeam();

    // Accesseurs
    virtual CString getClassName() const;
    unsigned int getNumDeads() const;
    unsigned int getNumPoints() const;

    // Méthode publiques
    void Frame(unsigned int frameTime);
    unsigned int getNumPlayers() const;
    void AddPlayer(IBasePlayer * player);
    void DeletePlayer(IBasePlayer * player);
    void ClearPlayers();

protected:

    // Donnée protégées
    std::vector<IBasePlayer *> m_players; ///< Liste des joueurs de l'équipe.
    unsigned int m_deads;                 ///< Nombre de morts dans l'équipe.
    unsigned int m_points;                ///< Nombre de points remportés.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTEAM_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseProjectile.hpp
 * \date 11/02/2010 Création de la classe IBaseProjectile.
 * \date 14/07/2010 Ajout de l'arme et du joueur qui ont envoyés le projectile.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBASEPROJECTILE_HPP_
#define T_FILE_ENTITIES_IBASEPROJECTILE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IPhysicEntity.hpp"


namespace Ted
{

class IBaseCharacter;
class IBaseWeapon;


/**
 * \class   IBaseProjectile
 * \ingroup Game
 * \brief   Classe de base des projectiles.
 ******************************/

class T_GAME_API IBaseProjectile : public IPhysicEntity
{
public:

    // Constructeur et destructeur
    explicit IBaseProjectile(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IBaseProjectile() = 0;

    // Accesseurs
    virtual CString getClassName() const;
    IBaseCharacter * getCharacter() const;
    IBaseWeapon * getWeapon() const;

    // Mutateurs
    void setCharacter(IBaseCharacter * character);
    void setWeapon(IBaseWeapon * weapon);

protected:

    // Données protégées
    IBaseCharacter * m_character; ///< Pointeur sur le personnage qui a envoyé le projectile.
    IBaseWeapon * m_weapon;       ///< Pointeur sur l'arme qui a envoyée le projectile.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASEPROJECTILE_HPP_

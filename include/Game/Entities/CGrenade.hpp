/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CGrenade.hpp
 * \date 14/07/2010 Création de la classe CGrenade.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CGRENADE_HPP_
#define T_FILE_ENTITIES_CGRENADE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBaseProjectile.hpp"


namespace Ted
{

/**
 * \class   CGrenade
 * \ingroup Game
 * \brief   Une grenade explose au bout d'un certain temps.
 *
 * \section Slots
 * \li Explode
 *
 * \section Signaux
 * \li onExplode
 ******************************/

class T_GAME_API CGrenade : public IBaseProjectile
{
public:

    // Constructeur
    explicit CGrenade(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseurs
    inline virtual CString getClassName() const;
    unsigned int getTime() const;
    bool isExplodeOnTouch() const;

    // Mutateurs
    void setTime(unsigned int time);
    void setExplodeOnTouch(bool explode = true);

    // Méthodes publiques
    void explode();
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    unsigned int m_time;     ///< Durée en millisecondes avant que la grenade explose (3000 par défaut).
    bool m_explode_on_touch; ///< Indique si la grenade explose lorsqu'elle touche un personnage (false par défaut).
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CGrenade::getClassName() const
{
    return "grenade";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CGRENADE_HPP_

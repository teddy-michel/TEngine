/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CEntityManager.hpp
 * \date 08/04/2009 Création de la classe CEntityManager.
 * \date 11/07/2010 On peut enlever une entité du gestionnaire (RemoveEntity) ou la supprimer de la mémoire (DeleteEntity).
 * \date 16/07/2010 Ajout de la méthode Debug_LogEntities.
 * \date 28/02/2011 Ajout d'une liste d'entités globales.
 * \date 23/03/2011 Création de la méthode getCorrectEntityId.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CENTITYMANAGER_HPP_
#define T_FILE_ENTITIES_CENTITYMANAGER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <string>
#include <list>

#include "Game/Export.hpp"
#include "CConnection.hpp"
#include "IEntity.hpp"


namespace Ted
{

class IGlobalEntity;


/**
 * \class   CEntityManager
 * \ingroup Game
 * \brief   Gestionnaire d'entités.
 *
 * Cette classe contient la liste de toutes les entités du jeu, et gère les
 * connexions entre toutes ces entités. Une liste des connexions en attente
 * d'exécution est mise-à-jour à chaque frame et à chaque déclenchement d'une
 * nouvelle connexion.
 ******************************/

class T_GAME_API CEntityManager
{
public:

    // Constructeur et destructeur
    CEntityManager();
    ~CEntityManager();

    // Méthodes publiques
    unsigned int getNumEntities() const;
    unsigned int getNumConnections() const;
    IEntity * getEntityByName(const CString& name) const;
    void addEntity(IEntity * entity);
    void addGlobalEntity(IGlobalEntity * entity);
    void addTemporaryEntity(IEntity * entity, const unsigned int time);
    void removeEntity(IEntity * entity);
    void deleteEntities();
    void frame(unsigned int frameTime);
    bool runConnection(const CConnection& connection);

#ifdef T_DEBUG
    void Debug_LogEntities() const;
#endif

protected:

    /**
     * \struct  CTemporaryEntity
     * \ingroup Game
     * \brief   Cette classe est utilisée pour définir une entité temporaire.
     ******************************/

    struct CTemporaryEntity
    {
        IEntity * entity;  ///< Pointeur sur l'entité.
        unsigned int time; ///< Durée en millisecondes avant de détruire l'entité.

        /**
         * Constructeur par défaut.
         *
         * \param p_entity Pointeur sur l'entité.
         * \param p_time   Durée en millisecondes avant de détruire l'entité.
         ******************************/

        inline CTemporaryEntity(IEntity * p_entity = nullptr, unsigned int p_time = 0) :
            entity (p_entity), time(p_time) {}
    };


    // Données protégées
    std::list<IEntity *> m_entities;              ///< Liste des entités.
    std::list<IGlobalEntity *> m_global_entities; ///< Liste des entités globales (qui n'appartiennent donc pas au graphe de scène).
    std::list<CTemporaryEntity> m_tmp_entities;   ///< Liste des entités temporaires.
    std::list<CConnection> m_connections;         ///< Liste des connections en attente.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CENTITYMANAGER_HPP_

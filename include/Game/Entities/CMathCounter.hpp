/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CMathCounter.hpp
 * \date 18/04/2009 Création de la classe CMathCounter.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 23/07/2010 La méthode getClassName est déclarée inline.
 * \date 06/12/2010 Plus de méthodes inline, création des accesseurs et mutateurs.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CMATHCOUNTER_HPP_
#define T_FILE_ENTITIES_CMATHCOUNTER_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IGlobalEntity.hpp"


namespace Ted
{

/**
 * \class   CMathCounter
 * \ingroup Game
 * \brief   Cette entité permet de mettre en place un compteur dans la map.
 *
 * La valeur du compteur peut être comprise entre une valeur minimale et une
 * valeur maximale. Un signal est émis chaque fois que la valeur change, et si
 * la valeur atteint l'une des deux bornes.
 *
 * \section Signaux
 * \li onHitMin
 * \li onHitMax
 * \li onValueChange(float)
 ******************************/

class T_GAME_API CMathCounter : public IGlobalEntity
{
public:

    // Constructeur
    explicit CMathCounter(const CString& name = CString(), float value = 0.0f);

    // Accesseurs
    inline virtual CString getClassName() const;
    float getMin() const;
    float getMax() const;
    float getValue() const;

    // Mutateurs
    void setMin(float min);
    void setMax(float max);
    void setRange(float min, float max);

    // Méthodes publiques
    virtual void frame(unsigned int frameTime);
    virtual bool callSlot(const CString& slot, const CString& param = CString());
    void set(float value);
    void add(float value);
    void subtract(float value);
    void multiply(float value);
    void divide(float value);

private:

    // Données privées
    float m_hitmin; ///< Valeur minimale du compteur.
    float m_hitmax; ///< Valeur maximale du compteur.
    float m_value;  ///< Valeur actuelle du compteur.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CMathCounter::getClassName() const
{
    return "math_counter";
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CMATHCOUNTER_HPP_

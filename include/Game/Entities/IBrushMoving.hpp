/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBrushMoving.hpp
 * \date 07/02/2010 Création de la classe IBrushMoving.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBRUSHMOVING_HPP_
#define T_FILE_ENTITIES_IBRUSHMOVING_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBrushEntity.hpp"


namespace Ted
{

/**
 * \class   IBrushMoving
 * \ingroup Game
 * \brief   Brush pouvant se déplacer.
 *
 * \section Slots
 * \li Open
 * \li Close
 * \li Toggle
 * \li Lock
 * \li Unlock
 *
 * \section Signaux
 * \li onOpen
 * \li onClose
 * \li onLocked
 ******************************/

class T_GAME_API IBrushMoving : public IBrushEntity
{
public:

    // Constructeur et destructeur
    explicit IBrushMoving(const CString& name = CString(), bool startOpen = false, bool startLocked = false, ILocalEntity * parent = nullptr);
    IBrushMoving(const CString& name, ILocalEntity * parent);
    explicit IBrushMoving(ILocalEntity * parent);
    virtual ~IBrushMoving() = 0;

    // Accesseurs
    inline CString getClassName() const;
    inline float getFraction() const;
    inline float getSpeed() const;
    inline bool isStartOpen() const;
    inline bool isStartLocked() const;
    inline bool isLocked() const;
    inline bool isOpen() const;
    inline bool isClose() const;

    // Mutateurs
    void setFraction(float fraction);
    void setSpeed(float speed);
    void setLocked(bool locked = true);
    void setDelay(unsigned int delay);

    // Méthodes publiques
    void open();
    void close();
    void toggle();
    virtual bool callSlot(const CString& slot, const CString& param = CString());

protected:

    // Données protégées
    float m_fraction;     ///< Fraction du déplacement totale (entre 0 et 1).
    float m_speed;        ///< Vitesse de déplacement en unité ou degré par seconde.
    bool m_startOpen;     ///< Indique si le brush est au départ dans sa position finale.
    bool m_startLocked;  ///< Indique si le brush est bloqué au départ.
    bool m_locked;        ///< Indique si le brush est bloqué.
    unsigned int m_delay; ///< Durée en millisecondes pendant laquelle le brush reste dans sa position finale.
    unsigned int m_time_before_reset; ///< Durée en millisecondes avant que le brush ne revienne dans sa position initiale.
    bool m_opening;       ///< Indique si le brush est en train d'aller vers sa position finale.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IBrushMoving::getClassName() const
{
    return "brush_moving";
}


/**
 * Donne le pourcentage du déplacement final.
 *
 * \return Pourcentage du déplacement final (entre 0 et 1).
 ******************************/

inline float IBrushMoving::getFraction() const
{
    return m_fraction;
}


/**
 * Donne la vitesse de déplacement.
 *
 * \return Vitesse de déplacement en unité ou degré par seconde.
 *
 * \sa IBrushMoving::setSpeed
 ******************************/

inline float IBrushMoving::getSpeed() const
{
    return m_speed;
}


/**
 * Indique si le brush est au départ dans sa position finale.
 *
 * \return Booléen.
 ******************************/

inline bool IBrushMoving::isStartOpen() const
{
    return m_startOpen;
}


/**
 * Indique si le brush est bloqué au départ.
 *
 * \return Booléen.
 ******************************/

inline bool IBrushMoving::isStartLocked() const
{
    return m_startLocked;
}


/**
 * Indique si le brush est bloqué.
 *
 * \return Booléen.
 *
 * \sa IBrushMoving::setLocked
 ******************************/

inline bool IBrushMoving::isLocked() const
{
    return m_locked;
}


/**
 * Indique si le brush est dans sa position finale.
 *
 * \return Booléen.
 *
 * \sa IBrushMoving::isClose
 ******************************/

inline bool IBrushMoving::isOpen() const
{
    return (m_fraction >= 1.0f);
}


/**
 * Indique si le brush est dans sa position initiale.
 *
 * \return Booléen.
 *
 * \sa IBrushMoving::isOpen
 ******************************/

inline bool IBrushMoving::isClose() const
{
    return (m_fraction <= 0.0f);
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBRUSHMOVING_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPathNode.hpp
 * \date 10/05/2009 Création de la classe CPathNode.
 * \date 23/07/2010 Création de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression de l'attribut name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CPATHNODE_HPP_
#define T_FILE_ENTITIES_CPATHNODE_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "Game/Export.hpp"
#include "IPointEntity.hpp"


namespace Ted
{

/**
 * \class   CPathNode
 * \ingroup Game
 * \brief   Entité servant à tracer des chemins pour les caméras ou les NPC.
 *
 * Pour qur les NPC aient plus de liberté, il sera préférable à l'avenir
 * d'utiliser des volumes ou zones de déplacement. Les données devront être
 * générées à la compilation de la map.
 ******************************/

class T_GAME_API CPathNode : public IPointEntity
{
public:

    // Constructeur
    explicit CPathNode(const CString& name = CString());

    // Accesseur
    virtual CString getClassName() const;
    bool isNeighboor(const CPathNode * pathnode) const;

    // Méthode publique
    void frame(unsigned int frameTime);
    unsigned int getNumNeighboors() const;
    void addNeighboor(CPathNode * pathnode);

private:

    // Donnée privée
    std::list<CPathNode *> m_neighboors; ///< Liste de points voisins.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_CPATHNODE_HPP_

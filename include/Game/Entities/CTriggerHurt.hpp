/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerHurt.hpp
 * \date 08/02/2010 Création de la classe CTriggerHurt.
 * \date 13/07/2010 Ajout du paramètre damage.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_CTRIGGERHURT_HPP_
#define T_FILE_ENTITIES_CTRIGGERHURT_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseTrigger.hpp"


namespace Ted
{

/**
 * \class   CTriggerHurt
 * \ingroup Game
 * \brief   Trigger qui fait perdre de la vie aux entités entrant dedans.
 ******************************/

class T_GAME_API CTriggerHurt : public IBaseTrigger
{
public:

    // Constructeur
    explicit CTriggerHurt(const CString& name = CString(), ILocalEntity * parent = nullptr);

    // Accesseurs
    inline virtual CString getClassName() const;
    inline float getDamage() const;

    // Mutateur
    void setDamage(float damage);

    // Méthode publiques
    void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    float m_damage; ///< Nombre de points de dégâts par seconde.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString CTriggerHurt::getClassName() const
{
    return "trigger_hurt";
}


/**
 * Donne le nombre de points de dommage par seconde.
 *
 * \return Nombre de points de dommage par seconde.
 *
 * \sa CTriggerHurt::setDamage
 ******************************/

inline float CTriggerHurt::getDamage() const
{
    return m_damage;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_CTRIGGERHURT_HPP_

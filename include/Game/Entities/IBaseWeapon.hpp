/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseWeapon.hpp
 * \date 11/04/2009 Création de la classe IBaseWeapon.
 * \date 09/07/2010 Création des classes dérivées pour chaque type d'arme.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 09/04/2011 Ajout du paramètre parent au constructeur.
 */

#ifndef T_FILE_ENTITIES_IBASEWEAPON_HPP_
#define T_FILE_ENTITIES_IBASEWEAPON_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Game/Entities/IBaseAnimating.hpp"


namespace Ted
{

class IBaseCharacter;

/**
 * \class   IBaseWeapon
 * \ingroup Game
 * \brief   Classe de base des armes.
 *
 * \section Signaux
 * \li onPickUpPlayer
 * \li onPickUpNPC
 ******************************/

class T_GAME_API IBaseWeapon : public IBaseAnimating
{
public:

    // Constructeur et destructeur
    explicit IBaseWeapon(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IBaseWeapon() = 0;

    // Accesseurs
    virtual CString getClassName() const;
    unsigned int getTimeBeforeFire() const;
    unsigned int getTimeBetweenTwoFire() const;
    bool isFire() const;
    bool canFireUnderWater() const;
    IBaseCharacter * getCharacter() const;

    // Mutateur
    void setCharacter(IBaseCharacter * character);

    // Méthodes publiques
    void primaryAttack();
    void secondaryAttack();
    virtual void frame(unsigned int frameTime);
    virtual void reload();

private:

    // Méthode privée
    virtual void fire() = 0;

protected:

    // Données protégées
    unsigned int m_timeBeforeFire;     ///< Durée avant de pouvoir tirer à nouveau en millisecondes.
    unsigned int m_timeBetweenTwoFire; ///< Durée entre deux tirs en millisecondes.
    bool m_primaryAttack;              ///< Indique si l'arme est actuellement en train de tirer (attaque primaire).
    bool m_secondaryAttack;            ///< Indique si l'arme est actuellement en train de tirer (attaque secondaire).
    bool m_canFireUnderwater;          ///< Indique si l'arme peut être utilisée sous l'eau.
    float m_minDamage;                 ///< Points minimaux de dégats.
    float m_maxDamage;                 ///< Points maximaux de dégats.
    float m_minDamageDistance;         ///< Distance au-delà de laquelle les points minimaux sont appliqués.
    float m_maxDamageDistance;         ///< Distance en-deça de laquelle les points maximaux sont appliqués.
    float m_randomDamage;              ///< Pourcentage de variation aléatoire pour les dégats (entre 0 et 1).
    IBaseCharacter * m_character;      ///< Pointeur sur le personnage qui tient l'arme.
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASEWEAPON_HPP_

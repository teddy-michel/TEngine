/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseAnimating.hpp
 * \date 11/04/2009 Création de la classe IBaseAnimating.
 * \date 13/07/2010 Le nombre de points de vie est désormais géré par cette classe.
 * \date 14/07/2010 Ajout de la méthode WeaponImpact.
 * \date 16/07/2010 Ajout de la méthode ProjectileImpact.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création des méthodes isPlayer et isNPC.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */

#ifndef T_FILE_ENTITIES_IBASEANIMATING_HPP_
#define T_FILE_ENTITIES_IBASEANIMATING_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBrushEntity.hpp"


namespace Ted
{

class IBaseProjectile;


/**
 * \class   IBaseAnimating
 * \ingroup Game
 * \brief   Classe de base des entités animées.
 ******************************/

class T_GAME_API IBaseAnimating : public IBrushEntity
{
public:

    // Constructeur et destructeur
    explicit IBaseAnimating(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IBaseAnimating() = 0;

    // Accesseurs
    virtual CString getClassName() const;
    float getHealth() const;
    bool isAlive() const;
    virtual bool isPlayer() const;
    virtual bool isNPC() const;

    // Mutateurs
    void addHealth(float health);
    void setHealth(float health = 100.0f);

    // Méthodes publiques
    virtual void weaponImpact(float damage, IBaseCharacter * character, IBaseWeapon * weapon);
    virtual void projectileImpact(float damage, IBaseProjectile * projectile);
    virtual void frame(unsigned int frameTime);

protected:

    // Donnée protégée
    float m_health; ///< Points de vie (si négatifs, l'entité est morte, 100 par défaut).
};

} // Namespace Ted

#endif // T_FILE_ENTITIES_IBASEANIMATING_HPP_

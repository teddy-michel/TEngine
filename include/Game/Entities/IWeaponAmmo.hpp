/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IWeaponAmmo.hpp
 * \date 09/07/2010 Création de la classe IWeaponAmmo.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 09/04/2011 Ajout du paramètre parent au constructeur.
 */

#ifndef T_FILE_ENTITIES_IWEAPONAMMO_HPP_
#define T_FILE_ENTITIES_IWEAPONAMMO_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "IBaseWeapon.hpp"


namespace Ted
{

/**
 * \class   IWeaponAmmo
 * \ingroup Game
 * \brief   Classe de base des armes utilisant des munitions.
 ******************************/

class T_GAME_API IWeaponAmmo : public IBaseWeapon
{
public:

    // Constructeur et destructeur
    explicit IWeaponAmmo(const CString& name = CString(), ILocalEntity * parent = nullptr);
    virtual ~IWeaponAmmo() = 0;

    // Accesseurs
    inline virtual CString getClassName() const;
    inline unsigned short getNumAmmo() const;
    inline unsigned short getNumMaxAmmo() const;
    inline unsigned short getNumAmmoInClip() const;
    inline unsigned short getNumMaxAmmoInClip() const;

    // Mutateurs
    void setNumAmmo(unsigned short nbr);
    void setNumMaxAmmo(unsigned short nbr);
    void setNumAmmoInClip(unsigned short nbr);
    void setNumMaxAmmoInClip(unsigned short nbr);

    // Méthode publique
    virtual void reload();

private:

    // Méthode privée
    void fire();

protected:

    // Données protégées
    unsigned short m_nbr_ammo;             ///< Nombre de munitions hors du chargeur.
    unsigned short m_nbr_max_ammo;         ///< Nombre maximal de munitions hors du chargeur.
    unsigned short m_nbr_ammo_in_clip;     ///< Nombre de munitions dans le chargeur.
    unsigned short m_nbr_max_ammo_in_clip; ///< Nombre maximal de munitions dans le chargeur.
};


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

inline CString IWeaponAmmo::getClassName() const
{
    return "weapon_ammo";
}


/**
 * Accesseur pour nbr_munitions.
 *
 * \return Nombre de munitions.
 ******************************/

inline unsigned short IWeaponAmmo::getNumAmmo() const
{
    return m_nbr_ammo;
}


/**
 * Accesseur pour nbr_max_munitions.
 *
 * \return Nombre maximal de munitions.
 ******************************/

inline unsigned short IWeaponAmmo::getNumMaxAmmo() const
{
    return m_nbr_max_ammo;
}


/**
 * Accesseur pour nbr_munitions_in_chargeur.
 *
 * \return Nombre de munitions dans le chargeur.
 ******************************/

inline unsigned short IWeaponAmmo::getNumAmmoInClip() const
{
    return m_nbr_ammo_in_clip;
}


/**
 * Accesseur pour nbr_max_munitions_in_chargeur.
 *
 * \return Nombre maximal de munitions dans le chargeur.
 ******************************/

inline unsigned short IWeaponAmmo::getNumMaxAmmoInClip() const
{
    return m_nbr_max_ammo_in_clip;
}

} // Namespace Ted

#endif // T_FILE_ENTITIES_IWEAPONAMMO_HPP_

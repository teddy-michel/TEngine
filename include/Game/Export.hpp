/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Export.hpp
 * \date 30/03/2013 Création du fichier.
 */

#ifndef T_FILE_GAME_EXPORT_HPP_
#define T_FILE_GAME_EXPORT_HPP_


#include "library.h"


#ifdef T_BUILD_GAME
#  define T_GAME_API T_EXPORT
#else
#  define T_GAME_API T_IMPORT
#endif


#endif // T_FILE_GAME_EXPORT_HPP_

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CLoaderTGeometry.cpp
 * \date 09/04/2012 Création de la classe CLoaderTGeometry.
 * \date 17/04/2012 Améliorations.
 */

#ifndef T_FILE_GAME_CLOADERTGEOMETRY_HPP_
#define T_FILE_GAME_CLOADERTGEOMETRY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <map>
#include <stdint.h>

#include "Game/Export.hpp"
#include "Core/ILoader.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Maths/CVector3.hpp"


namespace Ted
{

class IModel;


/**
 * \class   CLoaderTGeometry
 * \ingroup Game
 * \brief   Lecture d'un fichier au format TGeometry.
 *
 * Le format TGeometry est un format interne au format de map TMap.
 * Les fichiers contiennent les données graphiques de la map.
 ******************************/

class T_GAME_API CLoaderTGeometry : public ILoader
{
public:

    // Constructeur
    CLoaderTGeometry();

    // Méthodes publiques
    bool loadFromFile(const CString& fileName);
    void updateModel(IModel * model, const std::map<int, TTextureId>& textures) const;

private:

/*-------------------------------*
 *   Structures internes         *
 *-------------------------------*/

    static const uint16_t FileIdent = ('T' + ('G' << 8)); ///< Identifiant du format de fichier.

    /// Liste des versions du format de fichier.
#if __cplusplus < 201103L
    enum TVersion
#else
    enum TVersion : uint16_t
#endif
    {
        Version_1_0 = ('1' + ('0' << 8)) ///< Version 1.0 (09/04/2012).
    };

#include "Core/struct_alignment_start.h"

    /// En-tête du fichier.
    struct TGeometryHeader
    {
        uint16_t ident;             ///< Identifiant du fichier.
        uint16_t version;           ///< Version du fichier.
        uint32_t num_vertices;      ///< Nombre total de sommets.
        uint32_t offset_vertices;   ///< Offset pour lire la liste des sommets.
        uint16_t num_groups;        ///< Nombre total de groupes.
        uint32_t offset_groups;     ///< Offset pour lire la liste des groupes.
        uint32_t offset_visibility; ///< Offset pour lire le tableau de visibilité.
    };

    /// Description d'un groupe de faces.
    struct TGeometryGroup
    {
        uint32_t num_faces;     ///< Nombre de faces du groupe.
        uint32_t offset_faces;  ///< Offset pour lire la liste des faces du groupe.
        uint32_t min;           ///< Indice dans la liste des sommets du point minimal de la boite AABB.
        uint32_t max;           ///< Indice dans la liste des sommets du point maximal de la boite AABB.
    };

    /// Description d'une face.
    struct TGeometryFace
    {
        uint32_t s1;            ///< Indice du premier sommet.
        uint32_t s2;            ///< Indice du deuxième sommet.
        uint32_t s3;            ///< Indice du troisième sommet.
        uint32_t texture;       ///< Indice de la texture.
        uint16_t meta;          ///< Indice de la méta-texture pour la lightmap.
        uint16_t lightmap;      ///< Indice de la lightmap dans la méta-texture.
        float u1, v1;           ///< Coordonnées de la lightmap pour le premier sommet.
        float u2, v2;           ///< Coordonnées de la lightmap pour le deuxième sommet.
        float u3, v3;           ///< Coordonnées de la lightmap pour le troisième sommet.
    };

#include "Core/struct_alignment_end.h"

    std::vector<unsigned int> m_indices;  ///< Tableau des indices.
    std::vector<TVector3F> m_vertices;    ///< Tableau des vertices.
    std::vector<unsigned int> m_textures; ///< Identifiants des textures pour chaque face.
    std::vector<TVector2F> m_coords;      ///< Coordonnées des textures.
};

} // Namespace Ted

#endif // T_FILE_GAME_CLOADERTGEOMETRY_HPP_

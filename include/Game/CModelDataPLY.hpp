/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelDataPLY.hpp
 * \date 08/10/2012 Création de la classe CModelDataPLY.
 * \date 12/10/2012 Ajout de la méthode createInstance.
 * \date 03/06/2014 Ajout de la méthode getMemorySize.
 */

#ifndef T_FILE_GAME_CMODELDATAPLY_HPP_
#define T_FILE_GAME_CMODELDATAPLY_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Game/Export.hpp"
#include "Game/IModelData.hpp"
#include "Graphic/CColor.hpp"


namespace Ted
{

/**
 * \class   CModelDataPLY
 * \ingroup Game
 * \brief   Gestion des modèles au format PLY.
 ******************************/

class T_GAME_API CModelDataPLY : public IModelData
{
public:

    // Constructeur
    inline CModelDataPLY();

    // Accesseurs
    virtual unsigned int getMemorySize() const;
    inline unsigned int getNumIndices() const;
    inline unsigned int getNumVertices() const;

    // Méthodes publiques
    virtual void updateBuffer(CBuffer * buffer, IModel::TModelParams& params);
    virtual bool loadFromFile(const CString& fileName);

    static bool isCorrectFormat(const CString& fileName);

#ifdef T_NO_COVARIANT_RETURN
    static IModelData * createInstance(const CString& fileName);
#else
    static CModelDataPLY * createInstance(const CString& fileName);
#endif

private:

    // Données privées
    std::vector<unsigned int> m_indices; ///< Tableau des indices.
    std::vector<TVector3F> m_vertices;   ///< Tableau des vertices.
    std::vector<float> m_colors;         ///< Tableau des couleurs.
    CBoundingBox m_box;                  ///< Volume non orienté englobant le modèle.
};


/**
 * Constructeur par défaut.
 ******************************/

inline CModelDataPLY::CModelDataPLY() :
    IModelData ()
{ }


/**
 * Donne le nombre d'indices lus dans le fichier.
 *
 * \return Nombre d'indices.
 ******************************/

inline unsigned int CModelDataPLY::getNumIndices() const
{
    return m_indices.size();
}


/**
 * Donne le nombre de vertices lus dans le fichier.
 *
 * \return Nombre de vertices.
 ******************************/

inline unsigned int CModelDataPLY::getNumVertices() const
{
    return m_vertices.size();
}

} // Namespace Ted

#endif // T_FILE_GAME_CMODELDATAPLY_HPP_

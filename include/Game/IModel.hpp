/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IModel.hpp
 * \date 11/10/2012 Création de la classe IModelV2.
 * \date 13/10/2012 Poursuite de l'implémentation.
 * \date 19/10/2013 Renommage en IModel.
 */

#ifndef T_FILE_GAME_IMODEL_HPP_
#define T_FILE_GAME_IMODEL_HPP_


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Export.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/Maths/CQuaternion.hpp"
#include "Physic/CBoundingBox.hpp"


namespace Ted
{

class CBuffer;
class IBrushEntity;
class IModelData;


/**
 * \class   IModel
 * \ingroup Game
 * \brief   Cette classe contient les informations d'un modèle.
 *
 * Un modèle est caractérisé par des propriétés physiques (position, angle, vitesse,
 * vitesse angulaire, masse), un volume englobant principal et des volumes
 * englobants secondaires, un buffer graphique pour l'affichage, et une entité à
 * laquelle il est lié.
 *
 * La classe abtraite IModel est dérivée pour chaque type de modèle géré par le
 * moteur : modèle classique (objet visible dans le jeu), personnages, triggers.
 ******************************/

class T_GAME_API IModel
{
public:

    /**
     * \enum    TModelSeqMode
     * \ingroup Game
     * \brief   Mode de lecture des séquences.
     ******************************/

    enum TModelSeqMode
    {
        Auto,   ///< Le modèle choisit ce qu'il faut faire.
        Loop,   ///< Lecture en boucle de la séquence.
        Stop,   ///< Arrêter la lecture.
        Next    ///< Passe à la séquence suivante si elle est indiquée, ou arrête la lecture.
    };


    /**
     * \struct  TModelParams
     * \ingroup Physic
     * \brief   Paramètres du modèle à afficher.
     ******************************/

    struct TModelParams
    {
        static const unsigned int NumControllers = 8; ///< Nombre de contrôleurs d'un modèle.
        //static const unsigned int NumBlenders = 2;    ///< Nombre de blenders (?).

        // Données publiques
        unsigned short sequence;           ///< Numéro de la séquence à jouer.
        unsigned short frame;              ///< Numéro de la frame dans la séquence.
        float interpolation;               ///< Interpolation avec la frame suivante.
        TModelSeqMode mode;                ///< Mode de lecture des séquences.
        unsigned short skin;               ///< Numéro du skin à utiliser.
        unsigned short group;              ///< Numéro du groupe à utiliser.
        float controllers[NumControllers]; ///< Valeurs des contrôleurs.
        float blending[2];                 ///< Valeurs du blending.

        /// Constructeur par défaut.
        inline TModelParams() :
            sequence      (0),
            frame         (0),
            interpolation (0.0f),
            mode          (Auto),
            skin          (0),
            group         (0)
        {
            for (unsigned int i = 0; i < NumControllers; ++i)
            {
                controllers[i] = 0.0f;
            }

            blending[0] = blending[1] = 0.0f;
        }
    };


    // Constructeur et destructeur
    IModel(IModelData * modelData);
    virtual ~IModel();

    // Accesseurs
    TVector3F getPosition() const;
    CQuaternion getRotation() const;
    TVector3F getLinearVelocity() const;
    TVector3F getAngularVelocity() const;
    float getMass() const;
    TMatrix3F getInvInertia() const;
    inline bool isMovable() const;
    CBoundingBox getBoundingBox() const;
    inline IBrushEntity * getEntity() const;
    inline CBuffer * getBuffer() const;
    inline IModelData * getModelData() const;
    inline btRigidBody * getRigidBody() const;
    inline btMotionState * getMotionState() const;
    inline btCollisionShape * getCollisionShape() const;
    unsigned int getSequence() const;
    TModelSeqMode getSeqMode() const;
    unsigned int getSkin() const;
    unsigned int getGroup() const;
    float getControllerValue(unsigned int controller) const;
    float getBlendingValue(unsigned int blender) const;

    // Mutateurs
    void setPosition(const TVector3F& position);
    void translate(const TVector3F& v);
    void setRotation(const CQuaternion& rotation);
    void setLinearVelocity(const TVector3F& velocity);
    void setAngularVelocity(const TVector3F& velocity);
    void setMass(float mass);
    void setLocalInertia(const TVector3F& inertia);
    void setMovable(bool movable = true);
    void setEntity(IBrushEntity * entity);
    void setBuffer(CBuffer * buffer);
    void setModelData(IModelData * modelData);
    void setShape(btCollisionShape * shape);
    void setSequence(unsigned int sequence);
    void setSequenceByName(const CString& name);
    void setSeqMode(TModelSeqMode mode);
    void setSkin(unsigned int skin);
    void setGroup(unsigned int group);
    void setControllerValue(unsigned int controller, float value);
    void setBlendingValue(unsigned int blender, float value);
    void setOffset(const TVector3F& offset);

    // Méthodes publiques
    void addForce(const TVector3F& force);
    void addTorque(const TVector3F& torque);
    void drawModel() const;
    virtual void update(unsigned int frameTime);

protected:

    // Données protégées
    btRigidBody * m_body;       ///< Pointeur sur l'objet rigide.
    btMotionState * m_motionState;
    btCollisionShape * m_shape; ///< Volume englobant l'objet.
    float m_mass;               ///< Masse du modèle en kilogrammes.
    bool m_movable;             ///< Indique si le modèle est mobile.
    IBrushEntity * m_entity;    ///< Pointeur sur l'entité liée au modèle.
    CBuffer * m_buffer;         ///< Pointeur sur le buffer permettant l'affichage du modèle.
    IModelData * m_data;        ///< Pointeur sur les données du modèle.
    TModelParams m_params;      ///< Paramètres du modèle.
    TVector3F m_offset;         ///< Décalage entre l'objet physique et le buffer graphique.
};


/**
 * Indique si l'objet est mobile.
 *
 * \return Booléen indiquant si l'objet est mobile.
 *
 * \sa IModel::setMovable
 ******************************/

inline bool IModel::isMovable() const
{
    return m_movable;
}


/**
 * Retourne le pointeur sur l'entité liée au modèle.
 *
 * \return Pointeur sur l'entité liée au modèle.
 *
 * \sa IModel::setEntity
 ******************************/

inline IBrushEntity * IModel::getEntity() const
{
    return m_entity;
}


/**
 * Retourne le buffer graphique utilisé par le modèle.
 *
 * \return Pointeur sur le buffer graphique du modèle.
 *
 * \sa IModel::setBuffer
 ******************************/

inline CBuffer * IModel::getBuffer() const
{
    return m_buffer;
}


/**
 * Retourne les données utilisées par le modèle.
 *
 * \return Pointeur sur les données utilisées par le modèle.
 ******************************/

inline IModelData * IModel::getModelData() const
{
    return m_data;
}


/**
 * Retourne le pointeur sur le corps rigide du modèle.
 *
 * \return Pointeur sur le corps rigide.
 ******************************/

inline btRigidBody * IModel::getRigidBody() const
{
    return m_body;
}


inline btMotionState * IModel::getMotionState() const
{
    return m_motionState;
}


/**
 * Retourne le volume englobant le modèle.
 *
 * \return Pointeur sur le volume englobant le modèle.
 ******************************/

inline btCollisionShape * IModel::getCollisionShape() const
{
    return m_shape;
}

} // Namespace Ted

#endif // T_FILE_GAME_IMODEL_HPP_

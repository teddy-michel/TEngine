/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file library.h
 * \date 18/05/2009 Création du fichier.
 * \date 14/03/2011 Séparation du code Windows des autres OS.
 * \date 03/04/2012 Définition de la macro nullptr.
 */

#ifndef T_FILE_LIBRARY_H_
#define T_FILE_LIBRARY_H_

#include "os.h"


/*------------------------------------------*
 *   Macros d'importation et d'exportation  *
 *------------------------------------------*/

#ifdef T_SYSTEM_WINDOWS
#  define T_EXPORT __declspec(dllexport)
#  define T_IMPORT __declspec(dllimport)
#else
#  define T_EXPORT
#  define T_IMPORT
#endif


/*------------------------------------------*
 *   Macros de debug                        *
 *------------------------------------------*/

#ifdef T_DEBUG
#  ifndef _DEBUG
#    define _DEBUG
#  endif
#  undef NDEBUG
#else
#  ifdef _DEBUG
#    define T_DEBUG
#    undef NDEBUG
#  else
#    ifndef NDEBUG
#      define NDEBUG
#    endif
#  endif
#endif


/*------------------------------------------*
 *   Utilisation de la norme C++ 2011       *
 *------------------------------------------*/

#if __cplusplus < 201103L
#  ifndef nullptr
#    define nullptr NULL
#  endif
#endif


// Macros pour masquer les avertissements du compilateur si une variable n'est pas utilisée
#define T_UNUSED(VARNAME)               (void)VARNAME
#define T_UNUSED_UNIMPLEMENTED(VARNAME) (void)VARNAME

#endif // T_FILE_LIBRARY_H_

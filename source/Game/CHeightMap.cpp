/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHeightMap.cpp
 * \date 29/04/2011 Création de la classe CHeightMap.
 * \date 30/04/2011 Améliorations.
 * \date 08/05/2011 Améliorations.
 * \date 12/05/2011 Améliorations.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CHeightMap.hpp"
#include "Graphic/CBuffer.hpp"

// DEBUG
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param numCaseX Nombre de cases sur l'axe X.
 * \param numCaseY Nombre de cases sur l'axe Y.
 * \param origin   Coordonnées du point de départ.
 * \param size     Dimensions d'une case (48x48 par défaut).
 ******************************/

CHeightMap::CHeightMap(unsigned int numCaseX, unsigned int numCaseY, const TVector3F& origin, const TVector2F& size) :
m_numCaseX     (numCaseX == 0 ? 1 : numCaseX),
m_numCaseY     (numCaseY == 0 ? 1 : numCaseY),
m_origin       (origin),
m_size         (size),
m_textureId    (0),
m_textureScale (TVector2F(1.0f, 1.0f)),
m_buffer       (nullptr)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    m_coords.resize((m_numCaseX + 1) * (m_numCaseY + 1));
    m_cases.resize(m_numCaseX * m_numCaseY);

    // Initialisation des coordonnées relatives
    for (std::vector<TVector3F>::iterator it = m_coords.begin(); it != m_coords.end(); ++it)
    {
        *it = m_origin;
    }

    updateBufferIndices();
}


/**
 * Destructeur. Supprime le buffer graphique.
 ******************************/

CHeightMap::~CHeightMap()
{
    delete m_buffer;
}


/**
 * Donne le nombre de case sur l'axe X.
 *
 * \return Nombre de cases sur l'axe X.
 ******************************/

unsigned int CHeightMap::getNumCaseX() const
{
    return m_numCaseX;
}


/**
 * Donne le nombre de case sur l'axe Y.
 *
 * \return Nombre de cases sur l'axe Y.
 ******************************/

unsigned int CHeightMap::getNumCaseY() const
{
    return m_numCaseY;
}


/**
 * Donne les dimensions d'une case.
 *
 * \return Dimensions d'une case.
 *
 * \sa CHeightMap::setCaseSize
 ******************************/

TVector2F CHeightMap::getCaseSize() const
{
    return m_size;
}


/**
 * Donne l'identifiant de la texture utilisée.
 *
 * \return Identifiant de la texture.
 *
 * \sa CHeightMap::setTextureId
 ******************************/

TTextureId CHeightMap::getTextureId() const
{
    return m_textureId;
}


/**
 * Donne le facteur de redimensionnement de la texture.
 *
 * \return Facteur de redimensionnement de la texture.
 *
 * \sa CHeightMap::setTextureScale
 ******************************/

TVector2F CHeightMap::getTextureScale() const
{
    return m_textureScale;
}


/**
 * Donne le pointeur sur le buffer graphique du terrain.
 *
 * \return Pointeur constant sur le buffer graphique.
 ******************************/

const CBuffer * CHeightMap::getBuffer() const
{
    return m_buffer;
}


/**
 * Donne les coordonnées relatives d'un nœud.
 *
 * \param x Numéro de ligne (à partir de 0).
 * \param y Numéro de colonne (à partir de 0).
 * \return Coordonnées relatives du nœud demandé.
 ******************************/

TVector3F CHeightMap::getNodeRelativeCoords(unsigned int x, unsigned int y) const
{
    return m_coords[getNodeNumber(x, y )];
}


/**
 * Indique si un triangle est dans le sens normal ou pas.
 *
 * \param x Numéro de ligne (à partir de 0).
 * \param y Numéro de colonne (à partir de 0).
 * \return Booléen.
 ******************************/

bool CHeightMap::getTriangleDirection(unsigned int x, unsigned int y) const
{
    return m_cases[getTriangleNumber(x, y)];
}


/**
 * Modifie les dimensions d'une case.
 *
 * \param size Dimensions d'une case.
 *
 * \sa CHeightMap::getCaseSize
 ******************************/

void CHeightMap::setCaseSize(const TVector2F& size)
{
    if (size.X > 1.0f && size.Y > 1.0f)
    {
        m_size = size;
        updateBufferCoords();
    }
}


/**
 * Modifie les coordonnées du point de départ.
 *
 * \param origin Coordonnées du point de départ.
 * \param update Indique si on doit mettre-à-jour le buffer graphique.
 ******************************/

void CHeightMap::setOrigin(const TVector3F& origin, bool update)
{
    m_origin = origin;
    if (update)
        updateBufferCoords();
}


/**
 * Modifie l'identifiant de la texture à utiliser.
 *
 * \param textureId Identifiant de la texture à utiliser.
 *
 * \sa CHeightMap::getTextureId
 ******************************/

void CHeightMap::setTextureId(const TTextureId textureId)
{
    m_textureId = textureId;
    TBufferTextureVector& textures = m_buffer->getTextures();

    textures[0].texture[0] = m_textureId;
    textures[0].nbr = m_cases.size() * 6;
}


/**
 * Modifie le facteur de redimensionnement de la texture.
 *
 * \param scale Facteur de redimensionnement de la texture.
 *
 * \sa CHeightMap::getTextureScale
 ******************************/

void CHeightMap::setTextureScale(const TVector2F& scale)
{
    m_textureScale = scale;
}


/**
 * Modifie les coordonnées relatives d'un nœud.
 *
 * \param x      Numéro de ligne (à partir de 0).
 * \param y      Numéro de colonne (à partir de 0).
 * \param coords Coordonnées relatives du nœud.
 * \param update Indique si on doit mettre-à-jour le buffer graphique.
 ******************************/

void CHeightMap::setNodeRelativeCoords(unsigned int x, unsigned int y, const TVector3F& coords, bool update)
{
    m_coords[getNodeNumber(x, y)] = coords;
    if (update)
        updateBufferCoords();
}


/**
 * Modifie la hauteur relative d'un nœud.
 *
 * \param x      Numéro de ligne (à partir de 0).
 * \param y      Numéro de colonne (à partir de 0).
 * \param height Hauteur relative du nœud.
 * \param update Indique si on doit mettre-à-jour le buffer graphique.
 ******************************/

void CHeightMap::setNodeRelativeHeight(unsigned int x, unsigned int y, const float height, bool update)
{
    m_coords[getNodeNumber(x, y)].Z = height;
    if (update)
        updateBufferCoords();
}


/**
 * Modifie la direction d'un triangle.
 *
 * \param x         Numéro de ligne (à partir de 0).
 * \param y         Numéro de colonne (à partir de 0).
 * \param direction Booléen indiquant si le triangle est dans le sens normal ou pas.
 * \param update    Indique si on doit mettre-à-jour le buffer graphique.
 ******************************/

void CHeightMap::setTriangleDirection(unsigned int x, unsigned int y, bool direction, bool update)
{
    m_cases[getTriangleNumber(x, y)] = direction;
    if (update)
        updateBufferCoords();
}


/**
 * Donne le numéro d'un nœud à partir de son numéro de ligne et de colonne.
 * Si le numéro de ligne ou le numéro de colonne est incorrect, 0 est retourné.
 *
 * \param x Numéro de ligne (à partir de 0).
 * \param y Numéro de colonne (à partir de 0).
 * \return Numéro du nœud dans le tableau des coordonnées.
 ******************************/

unsigned int CHeightMap::getNodeNumber(unsigned int x, unsigned int y) const
{
    if (x > m_numCaseX || y > m_numCaseY) return 0;
    return (y + (m_numCaseY + 1) * x );
}


/**
 * Donne le numéro d'un triangle à partir de son numéro de ligne et de colonne.
 * Si le numéro de ligne ou le numéro de colonne est incorrect, 0 est retourné.
 *
 * \param x Numéro de ligne (à partir de 0).
 * \param y Numéro de colonne (à partir de 0).
 * \return Numéro du triangle dans le tableau des triangles.
 ******************************/

unsigned int CHeightMap::getTriangleNumber(unsigned int x, unsigned int y) const
{
    if (x >= m_numCaseX || y >= m_numCaseY) return 0;
    return (y + m_numCaseY * x);
}


/**
 * Met-à-jour les coordonnées des sommets du buffer graphique.
 *
 * \todo Implémentation.
 ******************************/

void CHeightMap::updateBufferCoords()
{
    T_ASSERT(m_buffer != nullptr);

    // Modification du tableau de sommets
    std::vector<TVector3F>& vertices = m_buffer->getVertices();
    vertices.clear();
    vertices.reserve(m_coords.size());

    for (unsigned int i = 0; i < m_numCaseX; ++i)
    {
        for (unsigned int j = 0; j < m_numCaseY; ++j)
        {
            TVector3F coords = m_origin + m_coords[getNodeNumber(i, j)];
            coords.X += i * m_size.X;
            coords.Y += j * m_size.Y;

            vertices.push_back(coords);
        }
    }

/*
    std::vector<float>& getTextCoords(unsigned short unit = 0);
    TBufferTextureVector& getTextures();
*/
}


/**
 * Met-à-jour les indices du buffer graphique.
 ******************************/

void CHeightMap::updateBufferIndices()
{
    T_ASSERT(m_buffer != nullptr);

    // Modification du tableau des indices
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.clear();
    indices.reserve(m_cases.size() * 6);

    for (unsigned int i = 0; i < m_numCaseX; ++i)
    {
        for (unsigned int j = 0; j < m_numCaseY; ++j)
        {
            unsigned int tmp = i * (m_numCaseY + 1);

            if (getTriangleDirection(i, j))
            {
                indices.push_back(j + tmp);
                indices.push_back(j + tmp + 1);
                indices.push_back(j + tmp + 1 + m_numCaseY);

                indices.push_back(j + tmp + 1);
                indices.push_back(j + tmp + 1 + m_numCaseY + 1);
                indices.push_back(j + tmp + 1 + m_numCaseY);
            }
            else
            {
                indices.push_back(j + tmp);
                indices.push_back(j + tmp + 1 );
                indices.push_back(j + tmp + 1 + m_numCaseY + 1);

                indices.push_back(j + tmp + 1 + m_numCaseY + 1);
                indices.push_back(j + tmp + 1 + m_numCaseY);
                indices.push_back(j + tmp);
            }
        }
    }
}

} // Namespace Ted

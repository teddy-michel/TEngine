/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CWindow_Console.cpp
 * \date 27/05/2009 Création de la classe GuiWindow_Console.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 22/07/2010 Modification des codes spéciaux pour colorier le texte.
 * \date 13/11/2010 Vérification qu'un nouveau message a été posté à partir du
 * \date 13/11/2010 compteur de messages de la console.
 * \date 29/05/2011 La classe est renommée en CWindow_Console.
 * \date 13/12/2011 Transformation du texte en Latin1.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CWindow_Console.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CTextEdit.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CSpacer.hpp"
#include "Gui/CLayoutVertical.hpp"
#include "Gui/CLayoutHorizontal.hpp"
#include "Game/CConsole.hpp"
#include "Game/CGameApplication.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow_Console::CWindow_Console(IWidget * parent) :
CWindow ("Console", WindowCenter, parent)
{
    setMinSize(400, 300);

    // Layout vertical
    CLayoutVertical * layout = new CLayoutVertical(this);
    layout->setPadding(CMargins(5, 5, 5, 5));

    // Layout horizontal
    CLayoutHorizontal * layout_2 = new CLayoutHorizontal(layout);
    layout_2->setPadding(CMargins(0, 0, 0, 0));

    // Spacer
    CSpacer * spacer = new CSpacer(layout_2);
    spacer->setMinSize(10, 10);
    spacer->setMaxSize(10, 10);

    // Bouton
    CPushButton * buttonClose = new CPushButton("Fermer", layout_2);
    buttonClose->setMaxSize(80, 20);
    buttonClose->onClicked.connect(sigc::mem_fun(this, &CWindow::close));

    // Champ de texte
    m_text = new CTextEdit();
    m_text->setEditable(false);

    // Ligne de texte
    m_line = new CLineEdit();
    m_line->onEnter.connect(sigc::mem_fun(this, &CWindow_Console::addLine));

    layout_2->addChild(m_line);
    layout_2->addChild(spacer);
    layout_2->addChild(buttonClose);

    layout->addChild(m_text);
    layout->addChild(layout_2);

    setLayout(layout);
}


/**
 * Méthode appellée lors de l'affichage de la fenêtre.
 ******************************/

void CWindow_Console::draw()
{
    static unsigned int oldCount = 0;
    unsigned int newCount = Game::console->getCount();

    // De nouveaux messages ont été postés
    if (newCount > oldCount)
    {
        std::vector<TConsoleData> datas = Game::console->getData();
        CString text;

        for (std::vector<TConsoleData>::const_iterator it = datas.begin(); it != datas.end(); ++it)
        {
            switch (it->type)
            {
                case ConsoleCommand:
                    text += "> " + it->txt + "\n";
                    break;

                default:
                case ConsoleInfos:
                    text += it->txt + "\n";
                    break;

                case ConsoleError:
                    text += static_cast<char>(4) + it->txt + static_cast<char>(1) + '\n';
                    break;

                case ConsoleWarning:
                    text += static_cast<char>(8) + it->txt + static_cast<char>(1) + '\n';
                    break;
            }
        }

        m_text->setText(text);
        oldCount = newCount;
    }

    CWindow::draw();
}


/**
 * Méthode appellée pour ajouter une commande à la console.
 ******************************/

void CWindow_Console::addLine()
{
    CString text = m_line->getText();

    if (!text.isEmpty())
    {
        m_line->clear();
        Game::console->sendCommand(text);
    }
}

} // Namespace Ted

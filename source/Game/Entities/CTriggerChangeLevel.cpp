/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerChangeLevel.cpp
 * \date 07/07/2010 Création de la classe CTriggerChangeLevel.
 * \date 13/07/2010 Ajout du paramètre level.
 * \date 14/07/2010 La map est chargée.
 * \date 16/07/2010 Ajout du slot et du signal.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 05/10/2011 Le changement de niveau est activé.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CTriggerChangeLevel.hpp"
#include "Core/CApplication.hpp"
#include "Core/Exceptions.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerChangeLevel::CTriggerChangeLevel(const CString& name, ILocalEntity * parent) :
IBaseTrigger (name, parent),
m_level      ()
{
    AddFilter(FilterPlayer);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerChangeLevel::CTriggerChangeLevel(ILocalEntity * parent) :
IBaseTrigger (CString(), parent),
m_level      ()
{
    AddFilter(FilterPlayer);
}


/**
 * Modifie le nom du niveau à charger.
 *
 * \param level Nom du niveau à charger.
 ******************************/

void CTriggerChangeLevel::setLevel(const CString& level)
{
    m_level = level;
}


/**
 * Change de niveau.
 ******************************/

void CTriggerChangeLevel::changeLevel()
{
    CApplication::getApp()->log(CString("CTriggerChangeLevel::changeLevel(%1) : fonction désactivée").arg(CString::fromPointer(this)));
    sendSignal("onChangeLevel");
    //CApplication::getApp()->loadMap(m_level);
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTriggerChangeLevel::frame(unsigned int frameTime)
{
    // S'il y a un joueur, on change de niveau
    for (std::list<TTriggerEntity>::const_iterator it = m_entities.begin() ; it != m_entities.end() ; ++it)
    {
        if (it->inside)
        {
            T_ASSERT(it->entity);
            changeLevel();
        }
    }

    IBaseTrigger::frame(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CTriggerChangeLevel::callSlot(const CString& slot, const CString& param)
{
    if (slot == "ChangeLevel")
    {
        changeLevel();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

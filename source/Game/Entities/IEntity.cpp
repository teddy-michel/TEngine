/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IEntity.cpp
 * \date 08/04/2009 Création de la classe IEntity.
 * \date 10/07/2010 Lorsque l'entité est supprimée, elle prévient le gestionnaire d'entités.
 * \date 15/07/2010 Le destructeur n'avertit plus le gestionnaire d'entités.
 * \date 16/07/2010 Ajout de la méthode getClassName.
 * \date 02/12/2010 Ajout du paramètre identifiant.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/12/2010 Ajout de la méthode isChildHierarchy.
 * \date 17/12/2010 Le graphe de scène est géré par la classe dérivée ILocalEntity.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IEntity.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/CGameApplication.hpp"
#include "Game/CConsole.hpp"
#include "Core/ILogger.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur. Ajoute l'entité au gestionnaire d'entités.
 *
 * \param name Nom de l'entité.
 ******************************/

IEntity::IEntity(const CString& name) :
m_name (name)
{
    Game::entityManager->addEntity(this);
}


/**
 * Destructeur. Enlève l'entité du gestionnaire d'entités.
 ******************************/

IEntity::~IEntity()
{
    Game::entityManager->removeEntity(this);
}


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IEntity::getClassName() const
{
    return "entity";
}


/**
 * Donne le nom de l'entité.
 *
 * \return Nom de l'entité.
 ******************************/

CString IEntity::getName() const
{
    return m_name;
}


/**
 * Donne le nombre de connexions de l'entité.
 *
 * \return Nombre de connexions.
 ******************************/

unsigned int IEntity::getNumConnections() const
{
    return m_connections.size();
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IEntity::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Gestion des slots.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération (ici false).
 ******************************/

bool IEntity::callSlot(const CString& slot, const CString& param)
{
    T_UNUSED(param);
    CApplication::getApp()->log(CString("No slot called %1 for entity %2").arg(slot).arg(m_name), ILogger::Warning);
    return false;
}


/**
 * Ajoute une connexion associée à un signal.
 *
 * \param signal     Nom du signal.
 * \param connection Connexion à utiliser.
 ******************************/

void IEntity::addConnection(const CString& signal, const CConnection& connection)
{
    m_connections.insert(std::pair<CString, CConnection>(signal, connection));
}


/**
 * Émission d'un signal
 *
 * \param name   Nom du signal.
 * \param caller Pointeur sur l'entité à l'origine de l'émission du signal.
 ******************************/

void IEntity::sendSignal(const CString& name, IEntity * caller) const
{
//ILogger::log() << "DEBUG: Envoi du signal " << name << " (this = " << this << ", caller = " << caller << ")\n";

    if (m_connections.find(name) != m_connections.end())
    {
        // Recherche les connexions correpondantes
        for (std::multimap<CString, CConnection>::const_iterator it = m_connections.lower_bound(name); it != m_connections.upper_bound(name); ++it)
        {
            CConnection c = it->second;
            c.caller = caller;
            Game::entityManager->runConnection(c);
        }
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CStaticLight.cpp
 * \date 23/11/2010 Création de la classe CStaticLight.
 * \date 24/11/2010 Gestion des faces concernées par la lumière.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 20/02/2011 Ajout d'un pointeur sur le chargeur de maps.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CStaticLight.hpp"
#include "Game/IMap.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name     Nom de l'entité.
 * \param gamedata Pointeur sur le chargeur de map.
 * \param on       Indique si la lumière est allumée dès le départ.
 ******************************/

CStaticLight::CStaticLight(const CString& name, IMap * gamedata, bool on) :
IGlobalEntity (name),
m_on          (false),
m_start_on    (on),
m_gamedata    (gamedata)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CStaticLight::getClassName() const
{
    return "static_light";
}


/**
 * Accesseur pour on.
 *
 * \return Booléen indiquant si la lumière allumée ou éteinte.
 ******************************/

bool CStaticLight::isOn() const
{
    return m_on;
}


/**
 * Allume la lumière.
 *
 * \sa CStaticLight::TurnOff
 ******************************/

void CStaticLight::turnOn()
{
    m_on = true;

    sendSignal("onLightOn");

    if (m_gamedata == nullptr)
    {
        return;
    }

    // Pour chaque unité de texture
    for (unsigned short i = 0; i < 4; ++i)
    {
        // Pour chaque face à modifier
        for (std::vector<unsigned short>::iterator it = m_faces[i].begin(); it != m_faces[i].end(); ++it)
        {
            m_gamedata->addLightmap(*it, i);
        }
    }
}


/**
 * Éteint la lumière.
 *
 * \sa CStaticLight::TurnOn
 ******************************/

void CStaticLight::turnOff()
{
    m_on = false;

    sendSignal("onLightOff");

    if (m_gamedata == nullptr)
    {
        return;
    }

    // Pour chaque unité de texture
    for (unsigned short i = 0; i < 4; ++i)
    {
        // Pour chaque face à modifier
        for (std::vector<unsigned short>::iterator it = m_faces[i].begin(); it != m_faces[i].end(); ++it)
        {
            m_gamedata->removeLightmap(*it, i);
        }
    }
}


/**
 * Change l'état de la lumière.
 *
 * \sa CStaticLight::TurnOn
 * \sa CStaticLight::TurnOff
 ******************************/

void CStaticLight::toggle()
{
    if (m_on)
    {
        turnOff();
    }
    else
    {
        turnOn();
    }
}


/**
 * Ajoute une face dans la liste des faces à modifier.
 *
 * \param face Numéro de la face à modifier.
 * \param unit Unité de texture à utiliser (2 par défaut, entre 2 et 5).
 ******************************/

void CStaticLight::addFace(unsigned short face, unsigned short unit)
{
    if (unit >= 2 && unit < 6)
    {
        m_faces[unit - 2].push_back(face);
    }
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CStaticLight::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);

    // Premier appel : on allume les lumières
    if (m_start_on)
    {
        turnOn();
        m_start_on = false;
    }
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CStaticLight::callSlot(const CString& slot, const CString& param)
{
    if (slot == "TurnOn")
    {
        turnOn();
        return true;
    }

    if (slot == "TurnOff")
    {
        turnOff();
        return true;
    }

    if (slot == "Toggle")
    {
        toggle();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

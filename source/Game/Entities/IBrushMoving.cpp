/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBrushMoving.cpp
 * \date 07/02/2010 Création de la classe IBrushMoving.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBrushMoving.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name        Nom de l'entité.
 * \param startOpen   Indique si le brush est au départ dans sa position finale.
 * \param startLocked Indique si le brush est bloqué au départ.
 * \param parent      Pointeur sur l'entité parent.
 ******************************/

IBrushMoving::IBrushMoving(const CString& name, bool startOpen, bool startLocked, ILocalEntity * parent) :
IBrushEntity        (name, parent),
m_fraction          (0.0f),
m_speed             (100.0f),
m_startOpen         (startOpen),
m_startLocked       (startLocked),
m_locked            (startLocked),
m_delay             (0),
m_time_before_reset (m_delay),
m_opening           (!startOpen)
{
    if (m_startOpen)
    {
        m_fraction = 1.0f;
    }
}

/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBrushMoving::IBrushMoving(const CString& name, ILocalEntity * parent) :
IBrushEntity        (name, parent),
m_fraction          (0.0f),
m_speed             (100.0f),
m_startOpen         (false),
m_startLocked       (false),
m_locked            (false),
m_delay             (0),
m_time_before_reset (m_delay),
m_opening           (true)
{
    if (m_startOpen)
    {
        m_fraction = 1.0f;
    }
}


/**
 * Destructeur.
 ******************************/

IBrushMoving::~IBrushMoving()
{ }


/**
 * Mutateur pour fraction.
 *
 * \param fraction Pourcentage du déplacement final (entre 0 et 1).
 *
 * \sa IBrushMoving::getFraction
 ******************************/

void IBrushMoving::setFraction(float fraction)
{
         if (fraction < 0.0f) fraction = 0.0f;
    else if (fraction > 1.0f) fraction = 1.0f;

    if (std::abs(m_fraction - fraction) > std::numeric_limits<float>::epsilon())
    {
        m_fraction = fraction;

             if (m_fraction <= 0.0f) sendSignal("onClose");
        else if (m_fraction >= 1.0f) sendSignal("onOpen");
    }
}


/**
 * Mutateur pour speed.
 *
 * \param speed Vitesse de déplacement en unité ou degré par seconde.
 *
 * \sa IBrushMoving::getSpeed
 ******************************/

void IBrushMoving::setSpeed(float speed)
{
    m_speed = speed;
    if (m_speed < 0.0f) m_speed = 0.0f;
}


/**
 * Mutateur pour locked.
 *
 * \param locked Indique si le brush est bloqué.
 *
 * \sa IBrushMoving::isLocked
 ******************************/

void IBrushMoving::setLocked(bool locked)
{
    m_locked = locked;
    sendSignal("onLocked");
}


/**
 * Mutateur pour delay.
 *
 * \param delay Durée en millisecondes pendant laquelle le brush reste dans sa position finale.
 ******************************/

void IBrushMoving::setDelay(unsigned int delay)
{
    m_delay = delay;
}


/**
 * Amène le brush dans sa position finale.
 ******************************/

void IBrushMoving::open()
{
    if (!m_locked)
    {
        m_time_before_reset = m_delay;
        m_opening = true;

        sendSignal("onOpen");
    }
}


/**
 * Amène le brush dans sa position initiale.
 ******************************/

void IBrushMoving::close()
{
    if (!m_locked)
    {
        m_time_before_reset = 0;
        m_opening = false;

        sendSignal("onClose");
    }
}


/**
 * Inverse le sens de déplacement du brush.
 *
 * \sa IBrushMoving::Open
 * \sa IBrushMoving::Close
 ******************************/

void IBrushMoving::toggle()
{
    // Le brush est dans sa position initiale
    if (m_fraction <= 0.0f)
    {
        open();
    }
    // Le brush est dans sa position finale
    else if (m_fraction >= 1.0f)
    {
        close();
    }
    // Position intermédiaire
    else
    {
        if (m_opening)
        {
            m_opening = false;
            sendSignal("onClose");
        }
        else
        {
            m_opening = true;
            sendSignal("onOpen");
        }
    }
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool IBrushMoving::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Open")
    {
        open();
        return true;
    }

    if (slot == "Close")
    {
        close();
        return true;
    }

    if (slot == "Toggle")
    {
        toggle();
        return true;
    }

    if (slot == "Lock")
    {
        setLocked(true);
        return true;
    }

    if (slot == "Unlock")
    {
        setLocked(false);
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

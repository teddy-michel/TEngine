/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IPointEntity.cpp
 * \date 08/04/2009 Création de la classe IPointEntity.
 * \date 07/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 17/12/2010 Héritage de la classe ILocalEntity.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IPointEntity.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IPointEntity::IPointEntity(const CString& name, ILocalEntity * parent) :
ILocalEntity (name, parent),
m_position   (Origin3F)
{ }


/**
 * Destructeur.
 ******************************/

IPointEntity::~IPointEntity()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IPointEntity::getClassName() const
{
    return "point_entity";
}


/**
 * Donne la position de l'entité. Si l'entité a un parent, cette position dépend
 * de celle de son parent.
 *
 * \return Position de l'entité.
 *
 * \sa IPointEntity::setPosition
 ******************************/

TVector3F IPointEntity::getPosition() const
{
    ILocalEntity * parent = getParent();
    if (parent)
        return m_position + parent->getPosition();
    return m_position;
}


/**
 * Définit la position de l'entité. Cette position est relative à celle de son
 * parent si elle en a un.
 *
 * \param position Position de l'entité.
 *
 * \sa IPointEntity::getPosition
 ******************************/

void IPointEntity::setPosition(const TVector3F& position)
{
    m_position = position;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IWeaponMelee.cpp
 * \date 09/07/2010 Création de la classe IWeaponMelee.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IWeaponMelee.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

IWeaponMelee::IWeaponMelee(const CString& name) :
IBaseWeapon (name)
{ }


/**
 * Destructeur.
 ******************************/

IWeaponMelee::~IWeaponMelee()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IWeaponMelee::getClassName() const
{
    return "weapon_melee";
}

} // Namespace Ted

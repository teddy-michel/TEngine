/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPhysicEntity.cpp
 * \date 12/12/2010 Création de la classe CPhysicEntity.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CPhysicEntity.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CPhysicEntity::CPhysicEntity(ILocalEntity * parent) :
IPhysicEntity (CString(), parent)
{ }


/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CPhysicEntity::CPhysicEntity(const CString& name, ILocalEntity * parent) :
IPhysicEntity (name, parent)
{ }


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CPhysicEntity::frame (unsigned int frameTime)
{
    //...

    IBaseAnimating::frame(frameTime);
}

} // Namespace Ted

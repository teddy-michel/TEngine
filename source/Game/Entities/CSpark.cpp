/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CSpark.cpp
 * \date 10/02/2010 Création de la classe CSpark.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 26/07/2010 La méthode getClassName est déclarée inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CSpark.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

CSpark::CSpark(const CString& name) :
IPointEntity (name),
m_time       (0),
m_delay      (2000),
m_color      (CColor(255,255,128,240))
{ }


/**
 * Modifie la durée entre deux émissions de particules.
 *
 * \param delay Durée entre deux émissions en millisecondes.
 *
 * \sa CSpark::getDelay
 ******************************/

void CSpark::setDelay(unsigned short delay)
{
    m_delay = delay;
}


/**
 * Modifie la couleur des particules
 *
 * \param color Couleur des particules.
 *
 * \sa CSpark::getColor
 ******************************/

void CSpark::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CSpark::frame(unsigned int frameTime)
{
    m_time += frameTime;

    if (m_time > m_delay)
    {
        // Émission des particules
        //...

        m_time = 0;
    }

    //...
}

} // Namespace Ted

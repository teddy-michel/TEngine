/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CDynamicLight.cpp
 * \date 08/04/2009 Création de la classe CDynamicLight.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 16/11/2010 Ajout des paramètres pour créer un spot.
 * \date 23/11/2010 CLight est renommée en CDynamicLight.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CDynamicLight.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param on     Indique si la lumière est allumée dès le départ.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CDynamicLight::CDynamicLight(const CString& name, bool on, ILocalEntity * parent) :
IPointEntity (name, parent),
m_on         (on),
m_color      (CColor(255, 255, 255, 200)),
m_inner      (180.0f),
m_outer      (180.0f),
m_direction  (TVector3F(0.0f, 0.0f, 1.0f))
{ }


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CDynamicLight::CDynamicLight(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_on         (true),
m_color      (CColor(255,255,255,200)),
m_inner      (180.0f),
m_outer      (180.0f),
m_direction  (TVector3F(0.0f, 0.0f, 1.0f))
{ }


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CDynamicLight::CDynamicLight(ILocalEntity * parent) :
IPointEntity (CString(), parent),
m_on         (true),
m_color      (CColor(255,255,255,200)),
m_inner      (180.0f),
m_outer      (180.0f),
m_direction  (TVector3F(0.0f, 0.0f, 1.0f))
{ }


/**
 * Allume la lumière.
 *
 * \sa CDynamicLight::TurnOff
 ******************************/

void CDynamicLight::turnOn()
{
    m_on = true;
    sendSignal("onLightOn");
}


/**
 * Éteint la lumière.
 *
 * \sa CDynamicLight::TurnOn
 ******************************/

void CDynamicLight::turnOff()
{
    m_on = false;
    sendSignal("onLightOff");
}


/**
 * Change l'état de la lumière.
 *
 * \sa CDynamicLight::TurnOn
 * \sa CDynamicLight::TurnOff
 ******************************/

void CDynamicLight::toggle()
{
    if (m_on)
    {
        turnOff();
    }
    else
    {
        turnOn();
    }
}


/**
 * Modifie la couleur et l'intensité.
 *
 * \param color Couleur et intensité.
 *
 * \sa CDynamicLight::getColor
 ******************************/

void CDynamicLight::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Mutateur pour inner.
 *
 * \param inner Angle du cone dans lequel la lumière est maximale.
 *
 * \sa CDynamicLight::getInner
 ******************************/

void CDynamicLight::setInner(unsigned int inner)
{
    m_inner = inner;
}


/**
 * Mutateur pour outer.
 *
 * \param outer Angle du cone contenant la lumière.
 *
 * \sa CDynamicLight::getOuter
 ******************************/

void CDynamicLight::setOuter(unsigned int outer)
{
    m_outer = outer;
}


/**
 * Mutateur pour direction.
 *
 * \param direction Axe du cone.
 *
 * \sa CDynamicLight::getDirection
 ******************************/

void CDynamicLight::setDirection(const TVector3F& direction)
{
    m_direction = direction;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CDynamicLight::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CDynamicLight::callSlot(const CString& slot, const CString& param)
{
    if (slot == "TurnOn")
    {
        turnOn();
        return true;
    }

    if (slot == "TurnOff")
    {
        turnOff();
        return true;
    }

    if (slot == "Toggle")
    {
        toggle();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CGlobalSound.cpp
 * \date 07/07/2010 Création de la classe CGlobalSound.
 * \date 11/07/2010 Gestion des slots et du son.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CGameApplication.hpp"
#include "Game/Entities/CGlobalSound.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"
#include "Sound/CSoundEngine.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name Nom de l'entité.
 ******************************/

CGlobalSound::CGlobalSound(const CString& name) :
IGlobalEntity (name),
m_sound       (),
m_music       (),
m_is_music    (false)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CGlobalSound::getClassName() const
{
    return "global_sound";
}


/**
 * Accesseur pour volume.
 *
 * \return Volume sonore du son.
 * \sa CGlobalSound::setVolume
 ******************************/

float CGlobalSound::getVolume() const
{
    return m_sound.getVolume();
}


/**
 * Indique si le son est joué en boucle.
 *
 * \return Booléen.
 * \sa CGlobalSound::setLoop
 ******************************/

bool CGlobalSound::isLoop() const
{
    return m_sound.isLoop();
}


/**
 * Donne le nom du fichier contenant le son à jouer.
 *
 * \return Nom du fichier contenant le son à jouer.
 * \sa CGlobalSound::setFilename
 ******************************/

CString CGlobalSound::getFilename() const
{
    return m_sound.getFileName();
}


/**
 * Mutateur pour volume.
 *
 * \param volume Volume sonore du son.
 * \sa CGlobalSound::getVolume
 ******************************/

void CGlobalSound::setVolume(float volume)
{
    m_sound.setVolume(volume);
    m_music.setVolume(volume);
}


/**
 * Indique si on doit jouer le son en boucle.
 *
 * \param loop Booléen.
 * \sa CGloabelSound::isLoop
 ******************************/

void CGlobalSound::setLoop(bool loop)
{
    m_sound.setLoop(loop);
    m_music.setLoop(loop);
}


/**
 * Modifie le nom du fichier contenant le son à jouer et charge le son.
 *
 * \param fileName Nom du fichier contenant le son à jouer.
 * \param music    Indique si le son est une musique.
 * \sa CGlobalSound::getFilename
 ******************************/

void CGlobalSound::setFilename(const CString& fileName, bool music)
{
    m_is_music = music;

    Game::soundEngine->removeSound(&m_sound);
    Game::soundEngine->removeMusic(&m_music);

    if (m_is_music)
    {
        if (m_music.loadFromFile(fileName))
        {
            Game::soundEngine->addMusic(&m_music);
        }
    }
    else
    {
        if (m_sound.loadFromFile(fileName))
        {
            Game::soundEngine->addSound(&m_sound);
        }
    }
}


/**
 * Démarre la lecture du son.
 ******************************/

void CGlobalSound::playSound()
{
    if (m_is_music)
    {
        m_music.play();
    }
    else
    {
        m_sound.play();
    }
}


/**
 * Met la lecture du son en pause.
 ******************************/

void CGlobalSound::pauseSound()
{
    if (m_is_music)
    {
        m_music.pause();
    }
    else
    {
        m_sound.pause();
    }
}


/**
 * Stoppe la lecture du son.
 ******************************/

void CGlobalSound::stopSound()
{
    if (m_is_music)
    {
        m_music.stop();
    }
    else
    {
        m_sound.stop();
    }
}


/**
 * Méthode appelée à chaque frame.
 *
 * \param frameTime Durée de la frame.
 ******************************/

void CGlobalSound::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);

    // On modifie la position du son (est-ce utile ?)
    TVector3F pos = Game::renderer->getCamera()->getPosition();

    m_music.setPosition(pos);
    m_sound.setPosition(pos);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CGlobalSound::callSlot(const CString& slot, const CString& param)
{
    if (slot == "PlaySound")
    {
        playSound();
        return true;
    }

    if (slot == "PauseSound")
    {
        pauseSound();
        return true;
    }

    if (slot == "StopSound")
    {
        stopSound();
        return true;
    }

    if (slot == "SetVolume" )
    {
        setVolume(param.toFloat());
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

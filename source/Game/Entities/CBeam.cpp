/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBeam.cpp
 * \date 09/02/2010 Création de la classe CBeam.
 * \date 07/07/2010 Le buffer graphique est modifié que quand la caméra s'est déplacée.
 * \date 11/07/2010 La classe CLaser est renommée en CBeam.
 * \date 13/07/2010 Amélioration du debuguage.
 * \date 16/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Plus de paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CBeam.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CBeam::CBeam(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_point      (Origin3F),
m_color      (CColor::Red),
m_damage     (0.0f),
m_width      (1.0f),
m_box        (),
m_buffer     (nullptr)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    m_box.setSize(m_width, m_width, 1.0f);

    // Indices du buffer
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6);

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 0;
    indices[4] = 2;
    indices[5] = 3;

    setColor(m_color);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CBeam::CBeam(ILocalEntity * parent) :
IPointEntity (CString(), parent),
m_point      (Origin3F),
m_color      (CColor::Red),
m_damage     (0.0f),
m_width      (1.0f),
m_box        (),
m_buffer     (nullptr)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    m_box.setSize(m_width, m_width, 1.0f);

    // Indices du buffer
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6);

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 0;
    indices[4] = 2;
    indices[5] = 3;

    setColor(m_color);
}


/**
 * Destructeur. Supprime le buffer graphique.
 ******************************/

CBeam::~CBeam()
{
    delete m_buffer;
}


/**
 * Donne la couleur du faisseau.
 *
 * \return Couleur du faisceau.
 *
 * \sa CBeam::setColor
 ******************************/

CColor CBeam::getColor() const
{
    return m_color;
}


/**
 * Donne le nombre de points de dégât par seconde infligés par le faisceau.
 *
 * \return Nombre de points de dégât par seconde pour les entités qui traversent le faisceau.
 *
 * \sa CBeam::setDamage
 ******************************/

float CBeam::getDamage() const
{
    return m_damage;
}


/**
 * Donne la largeur du faisceau.
 *
 * \return Largeur du faisceau.
 *
 * \sa CBeam::setWidth
 ******************************/

float CBeam::getWidth() const
{
    return m_width;
}


/**
 * Donne le buffer graphique utilisé par l'entité.
 *
 * \return Buffer graphique pour l'affichage du faisceau.
 ******************************/

CBuffer * CBeam::getBuffer() const
{
    return m_buffer;
}


/**
 * Donne le point de départ du faisceau.
 *
 * \param position Position du point de départ du faisceau.
 ******************************/

void CBeam::setPosition(const TVector3F& position)
{
    m_position = position;

    TVector3F tmp = m_point - m_position;

    // Modification de la boite orientée
    m_box.setLength(tmp.norm());

    CQuaternion q;
    q.FromAxeRotation(VectorCross(TVector3F (1.0f, 0.0f, 0.0f), tmp), VectorAngle(TVector3F(1.0f, 0.0f, 0.0f), tmp));

    m_box.setAngles(q);
}


/**
 * Mutateur pour point.
 *
 * \param point Point d'arrivée du faisceau.
 *
 * \sa CBeam::getPoint
 ******************************/

void CBeam::setPoint(const TVector3F& point)
{
    m_point = point;

    TVector3F tmp = m_point - m_position;

    // Modification de la boite orientée
    m_box.setLength(tmp.norm());

    CQuaternion q;
    q.FromAxeRotation(VectorCross(TVector3F(1.0f, 0.0f, 0.0f), tmp), VectorAngle(TVector3F(1.0f, 0.0f, 0.0f), tmp));
}


/**
 * Mutateur pour color.
 *
 * \param color Couleur du faisceau.
 *
 * \sa CBeam::getColor
 ******************************/

void CBeam::setColor(const CColor& color)
{
    T_ASSERT(m_buffer != nullptr);

    m_color = color;

    std::vector<float>& colors = m_buffer->getColors();
    colors.resize(6*4);

    float dest[4];
    m_color.toFloat(dest);

    for (int i = 0; i < 6*4; ++i)
    {
        colors[i] = dest[i % 4];
    }
}


/**
 * Modifie les dégâts infligés par le faisceau.
 *
 * \param damage Nombre de points de dommage par seconde pour les entités qui traversent le faisceau.
 *
 * \sa CBeam::getDamage
 ******************************/

void CBeam::setDamage(float damage)
{
    m_damage = (damage < 0.0f ? 0.0f : damage);
}


/**
 * Modifie la largeur du faisceau.
 *
 * \param width Largeur du faisceau (entre 1 et 16).
 *
 * \sa CBeam::getWidth
 ******************************/

void CBeam::setWidth(float width)
{
         if (width < 1.0f)  m_width = 1.0f;
    else if (width > 16.0f) m_width = 16.0f;
    else                    m_width = width;

    // Modification de la boite orientée
    m_box.setWidth(m_width);
    m_box.setHeight(m_width);
}


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Utiliser une matrice de transformation au lieu de modifier chaque coordonnée.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CBeam::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
    T_ASSERT(m_buffer != nullptr);

    static TVector3F old_camera = Game::renderer->getCamera()->getPosition();

    // Position de la caméra
    TVector3F camera = Game::renderer->getCamera()->getPosition();

    // Si la caméra s'est déplacée
    if (camera != old_camera)
    {
        // La faisceau fait face à la caméra
        TVector3F n = VectorCross((m_position + m_point) * 0.5f - camera, m_point - m_position);

        // On calcule sa largeur
        float norme = 0.5f * m_width / n.norm();

        if (std::abs(norme) > std::numeric_limits<float>::epsilon())
        {
            n *= norme;
        }

        // Tableau de vertices
        std::vector<TVector3F>& vertices = m_buffer->getVertices();
        vertices.clear();
        vertices.reserve(4);

        // Vertices
        vertices.push_back(m_position + n);
        vertices.push_back(m_position - n);
        vertices.push_back(m_point - n);
        vertices.push_back(m_point + n);

        // Mise à jour du buffer
        m_buffer->update();
    }

    m_buffer->draw();
}

} // Namespace Ted

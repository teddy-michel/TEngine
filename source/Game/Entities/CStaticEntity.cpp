/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CStaticEntity.cpp
 * \date 05/02/2010 Création de la classe CStaticEntity.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CStaticEntity.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param model  Pointeur sur le modèle à utiliser.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CStaticEntity::CStaticEntity(const CString& name, IModel * model, ILocalEntity * parent) :
IBrushEntity (name, model, parent)
{ }


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CStaticEntity::CStaticEntity(const CString& name, ILocalEntity * parent) :
IBrushEntity (name, parent)
{ }


/**
 * Destructeur.
 ******************************/

CStaticEntity::~CStaticEntity()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CStaticEntity::getClassName() const
{
    return "static_entity";
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CStaticEntity::frame(unsigned int frameTime)
{
    IBrushEntity::frame(frameTime);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerHurt.cpp
 * \date 08/02/2010 Création de la classe CTriggerHurt.
 * \date 13/07/2010 Ajout du paramètre damage.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CTriggerHurt.hpp"
#include "Game/Entities/IBaseAnimating.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerHurt::CTriggerHurt(const CString& name, ILocalEntity * parent) :
IBaseTrigger (name, parent)
{ }


/**
 * Modifie le nombre de points de dommage par seconde.
 * Remarque : les points de dommage peuvent être négatifs pour ajouter des points
 * de vie à l'entité.
 *
 * \return damage Nombre de points de dommage par seconde.
 *
 * \sa CTriggerHurt::getDamage
 ******************************/

void CTriggerHurt::setDamage(float damage)
{
    m_damage = damage;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTriggerHurt::frame(unsigned int frameTime)
{
    // Application des points de dégâts à chaque entité
    for (std::list<TTriggerEntity>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        if (it->inside)
        {
            T_ASSERT(it->entity);
            it->entity->addHealth(-m_damage);
        }
    }

    IBaseTrigger::frame(frameTime);
}

} // Namespace Ted

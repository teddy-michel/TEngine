/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CLogicCase.cpp
 * \date 18/11/2010 Création de la classe CLogicCase.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>
#include <cmath>

#include "Game/Entities/CLogicCase.hpp"
#include "Core/Utils.hpp"
#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name Nom de l'entité.
 ******************************/

CLogicCase::CLogicCase(const CString& name) :
IGlobalEntity (name)
{
    // Initialisation des valeurs
    for (int i = 0; i < 16; ++i)
    {
        m_cases[i].value = 0.0f;
        m_cases[i].active = false;
    }
}


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CLogicCase::getClassName() const
{
    return "logic_case";
}


/**
 * Donne la valeur d'une case.
 *
 * \param numCase Numéro de la case (entre 0 et 15).
 * \return Valeur de la case.
 *
 * \sa CLogicCase::setValue
 ******************************/

float CLogicCase::getValue(int numCase) const
{
    if (numCase < 0 || numCase >= 16)
        return 0.0f;

    return m_cases[numCase].value;
}


/**
 * Modifie la valeur d'une case.
 *
 * \param numCase Numéro de la case à modifier (entre 0 et 15).
 * \param value   Nouvelle valeur.
 *
 * \sa CLogicCase::getValue
 ******************************/

void CLogicCase::setValue(int numCase, float value)
{
    if (numCase < 0 || numCase >= 16)
        return;

    m_cases[numCase].value = value;
    m_cases[numCase].active = true;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CLogicCase::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLogicCase::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Value")
    {
        float v = param.toFloat();

        for (int i = 0; i < 16; ++i)
        {
            if (m_cases[i].active && std::abs(m_cases[i].value - v) < std::numeric_limits<float>::epsilon())
            {
                sendSignalForValue(i);
            }
        }

        sendSignal("OnDefault");
        return true;
    }

    if (slot == "RandomCase")
    {
        int numActives = 0; // Nombre de cases actives

        // On compte le nombre de cases actives
        for (int i = 0; i < 16; ++i)
        {
            if (m_cases[i].active)
                ++numActives;
        }

        if (numActives == 1)
        {
            for (int i = 0; i < 16; ++i)
            {
                if (m_cases[i].active)
                    sendSignalForValue(i);
            }
        }
        else if (numActives > 1)
        {
            int rand = RandInt(1, numActives);

            // On compte le nombre de cases actives
            for (int i = 0; i < 16; ++i)
            {
                if (m_cases[i].active && --rand == 0)
                {
                    sendSignalForValue(i);
                }
            }
        }

        sendSignal("OnDefault");
        return true;
    }

    return IEntity::callSlot(slot, param);
}


/**
 * Envoie le signal correspondant à un numéro de case.
 *
 * \param c Numéro de case.
 ******************************/

void CLogicCase::sendSignalForValue(int c)
{
    switch (c)
    {
        default: sendSignal("onDefault"); return;
        case  0: sendSignal("onCase01");  return;
        case  1: sendSignal("onCase02");  return;
        case  2: sendSignal("onCase03");  return;
        case  3: sendSignal("onCase04");  return;
        case  4: sendSignal("onCase05");  return;
        case  5: sendSignal("onCase06");  return;
        case  6: sendSignal("onCase07");  return;
        case  7: sendSignal("onCase08");  return;
        case  8: sendSignal("onCase09");  return;
        case  9: sendSignal("onCase10");  return;
        case 10: sendSignal("onCase11");  return;
        case 11: sendSignal("onCase12");  return;
        case 12: sendSignal("onCase13");  return;
        case 13: sendSignal("onCase14");  return;
        case 14: sendSignal("onCase15");  return;
        case 15: sendSignal("onCase16");  return;
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public PURPOSE. SeeLicense as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CMathCounter.cpp
 * \date 18/04/2009 Création de la classe CMathCounter.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 23/07/2010 La méthode getClassName est déclarée inline.
 * \date 06/12/2010 Plus de méthodes inline, création des accesseurs et mutateurs.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>
#include <cmath>

#include "Game/Entities/CMathCounter.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name  Nom de l'entité.
 * \param value Valeur initiale.
 ******************************/

CMathCounter::CMathCounter(const CString& name, float value) :
IGlobalEntity (name),
m_value       (value)
{ }


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CMathCounter::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Donne la valeur minimale.
 *
 * \return Valeur minimale.
 *
 * \sa CMathCounter::setMin
 ******************************/

float CMathCounter::getMin() const
{
    return m_hitmin;
}


/**
 * Donne la valeur maximale.
 *
 * \return Valeur maximale.
 *
 * \sa CMathCounter::setMax
 ******************************/

float CMathCounter::getMax() const
{
    return m_hitmax;
}


/**
 * Donne la valeur actuelle.
 *
 * \return Valeur actuelle.
 ******************************/

float CMathCounter::getValue() const
{
    return m_value;
}


/**
 * Modifie la valeur minimale.
 *
 * \param min Valeur minimale.
 *
 * \sa CMathCounter::getMin
 * \sa CMathCounter::setRange
 ******************************/

void CMathCounter::setMin(float min)
{
    m_hitmin = min;
    if (m_hitmax < m_hitmin)
        m_hitmax = min;
}


/**
 * Modifie la valeur maximale.
 *
 * \param max Valeur maximale.
 *
 * \sa CMathCounter::getMax
 * \sa CMathCounter::setRange
 ******************************/

void CMathCounter::setMax(float max)
{
    m_hitmax = max;
    if (m_hitmin > m_hitmax)
        m_hitmin = max;
}


/**
 * Modifie les valeurs minimale et maximale.
 * Si max est inférieur à min, rien n'est modifié.
 *
 * \param min Valeur minimale.
 * \param max Valeur maximale.
 *
 * \sa CMathCounter::setMin
 * \sa CMathCounter::setMax
 ******************************/

void CMathCounter::setRange(float min, float max)
{
    if (min < max)
    {
        m_hitmin = min;
        m_hitmax = max;
    }
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CMathCounter::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Add")
    {
        add(param.toFloat());
        return true;
    }

    if (slot == "Subtract")
    {
        subtract (param.toFloat());
        return true;
    }

    if (slot == "Multiply")
    {
        multiply (param.toFloat());
        return true;
    }

    if (slot == "Divide")
    {
        divide (param.toFloat());
        return true;
    }

    if (slot == "SetValue")
    {
        set (param.toFloat());
        return true;
    }

    if (slot == "SetHitMax")
    {
        m_hitmax = param.toFloat();

        if (m_hitmax < m_hitmin)
        {
            m_hitmin = m_hitmax;
        }

        return true;
    }

    if (slot == "SetHitMin")
    {
        m_hitmin = param.toFloat();

        if (m_hitmin < m_hitmax)
        {
            m_hitmax = m_hitmin;
        }

        return true;
    }

    return IEntity::callSlot(slot, param);
}


/**
 * Modifie la valeur.
 *
 * \param value Nouvelle valeur.
 ******************************/

void CMathCounter::set(float value)
{
    if (std::abs (m_value - value) > std::numeric_limits<float>::epsilon())
    {
        m_value = value;

        sendSignal("onValueChange");

        if (m_value >= m_hitmax)
        {
            m_value = m_hitmax;

            sendSignal("onHitMax");
        }

        if (m_value <= m_hitmin)
        {
            m_value = m_hitmin;

            sendSignal("onHitMin");
        }
    }
}


/**
 * Augmente la valeur.
 *
 * \param value Nombre à ajouter.
 ******************************/

void CMathCounter::add(float value)
{
    set(m_value + value);
}


/**
 * Diminue la valeur.
 *
 * \param value Nombre à soustraire.
 ******************************/

void CMathCounter::subtract(float value)
{
    set(m_value - value);
}


/**
 * Multiplie la valeur.
 *
 * \param value Nombre par lequel multiplier.
 ******************************/

void CMathCounter::multiply(float value)
{
    set(m_value * value);
}


/**
 * Divise la valeur.
 *
 * \param value Nombre par lequel diviser.
 ******************************/

void CMathCounter::divide(float value)
{
    if (std::abs(value) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    set (m_value / value);
}

} // Namespace Ted

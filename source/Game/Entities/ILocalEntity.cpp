/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/ILocalEntity.cpp
 * \date 17/12/2010 Création de la classe ILocalEntity.
 * \date 28/02/2011 Correction d'erreurs liées aux itérateurs.
 * \date 11/03/2011 Les entités enfant sont rattachées au parent dans le destructeur.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/ILocalEntity.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

ILocalEntity::ILocalEntity(const CString& name, ILocalEntity * parent) :
IEntity  (name),
m_parent (parent)
{
    if (m_parent)
    {
        m_parent->addChild(this);
    }
}


/**
 * Destructeur. Les entités enfant sont rattachées au parent de cette entité (à
 * leur grand-parent donc).
 ******************************/

ILocalEntity::~ILocalEntity()
{
    if (m_parent)
    {
        m_parent->removeChild(this); // Appelle this->setParent(nullptr)
    }

    // On indique aux entités enfant qu'elles n'ont plus de parent
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); )
    {
        ILocalEntity * ent = *it; // On enregistre le pointeur pour pouvoir modifier l'itérateur
        it = m_children.erase(it); // On supprime l'élément de la liste
        ent->setParent(m_parent); // On change le parent de l'entité
    }
}


/**
 * Donne le pointeur sur l'entité parent.
 *
 * \return Pointeur sur l'entité parent.
 *
 * \sa ILocalEntity::setParent
 ******************************/

ILocalEntity * ILocalEntity::getParent() const
{
    return m_parent;
}


/**
 * Cherche si une entité fait partie des enfants de cette entité.
 *
 * \param child Pointeur sur l'entité à rechercher.
 * \return Booléen valant true si l'entité est dans la liste, false si le
 *         pointeur est invalide, ou si on veut que l'entité soit son propre
 *         enfant, ou si l'entité n'est pas dans la liste.
 ******************************/

bool ILocalEntity::isChild(const ILocalEntity * child) const
{
    if (child == this || child == nullptr)
    {
        return false;
    }

    for (std::list<ILocalEntity *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (*it == child)
        {
            return true;
        }
    }

    return false;
}


/**
 * Cherche si une entité fait partie des descendants de cette entité.
 *
 * \param child Pointeur sur l'entité à rechercher.
 * \return Booléen.
 ******************************/

bool ILocalEntity::isChildHierarchy(const ILocalEntity * child) const
{
    if (child == this || child == nullptr)
    {
        return false;
    }

    // On parcourt la liste des entités enfants
    for (std::list<ILocalEntity *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (*it == child || (*it)->isChildHierarchy(child))
        {
            return true;
        }
    }

    return false;
}


/**
 * Modifie le parent de l'entité.
 *
 * \param parent Pointeur sur l'entité parent.
 *
 * \sa ILocalEntity::getParent
 ******************************/

void ILocalEntity::setParent(ILocalEntity * parent)
{
    if (parent == this || parent == m_parent)
    {
        return;
    }

    ILocalEntity * old_parent = m_parent; // On enregistre le pointeur sur l'ancien parent
    m_parent = nullptr; // Temporairement, l'entité n'a pas de parent

    // On indique à l'ancien parent le changement
    if (old_parent)
    {
        old_parent->removeChild(this); // Appelle this->setParent(nullptr);
    }

    if (parent)
    {
        m_parent = parent; // On change la valeur du parent de cette entité
        m_parent->addChild(this);
    }
}


/**
 * Ajoute une entité à la liste des enfants.
 * Si le pointeur est invalide ou que l'entité est déjà dans la liste, rien n'est fait.
 *
 * \todo Vérifier que l'entité n'est pas un ascendant.
 *
 * \param child Pointeur sur l'entité à ajouter.
 ******************************/

void ILocalEntity::addChild(ILocalEntity * child)
{
    if (child != nullptr && child != this && !isChild(child))
    {
        m_children.push_front(child);
        child->setParent(this);
    }
}


/**
 * Enlève une entité de la liste des enfants, mais ne désalloue pas la mémoire.
 *
 * \param child Pointeur sur l'entité à enlever.
 ******************************/

void ILocalEntity::removeChild(ILocalEntity * child)
{
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (*it == child)
        {
            m_children.erase(it);
            child->setParent(nullptr);
            return;
        }
    }
}


/**
 * Enlève une entité de la liste des enfants, et libère la mémoire.
 *
 * \param child Pointeur sur l'entité à supprimer.
 ******************************/

void ILocalEntity::deleteChild(ILocalEntity * child)
{
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (*it == child)
        {
            m_children.erase(it);
            delete child;
            return;
        }
    }
}


/**
 * Supprime toutes les entités enfant, et libère la mémoire.
 ******************************/

void ILocalEntity::deleteChildren ()
{
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); )
    {
        ILocalEntity * ent = *it;
        it = m_children.erase(it);
        delete ent;
    }
}


/**
 * Supprime toutes les entités descendant directement ou indirectement de cette
 * entité, et libère la mémoire.
 ******************************/

void ILocalEntity::deleteChildrenHierarchy()
{
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); )
    {
        ILocalEntity * ent = *it; // On enregistre le pointeur sur l'entité enfant
        it = m_children.erase(it); // On enlève l'entité de la liste.
        ent->deleteChildrenHierarchy(); // Appelle récursif
        delete ent;
    }
}


/**
 * Met-à-jour chaque entité enfant.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void ILocalEntity::frame(unsigned int frameTime)
{
    for (std::list<ILocalEntity *>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        (*it)->frame(frameTime);
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerRemove.cpp
 * \date 07/07/2010 Création de la classe CTriggerRemove.
 * \date 14/07/2010 Les entités sont supprimées.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CTriggerRemove.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/Entities/IBaseAnimating.hpp"
#include "Game/CGameApplication.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerRemove::CTriggerRemove(const CString& name, ILocalEntity * parent) :
IBaseTrigger (name, parent)
{ }


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerRemove::CTriggerRemove(ILocalEntity * parent) :
IBaseTrigger (CString(), parent)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CTriggerRemove::getClassName() const
{
    return "trigger_remove";
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTriggerRemove::frame(unsigned int frameTime)
{
    // Suppression des entités
    for (std::list<TTriggerEntity>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        if (it->inside)
        {
            T_ASSERT(it->entity);

            // On place l'entité dans la liste des entités temporaires pour qu'elle soit supprimée à la prochaine frame
            Game::entityManager->addTemporaryEntity(it->entity, 0);
        }
    }

    IBaseTrigger::frame(frameTime);
}

} // Namespace Ted

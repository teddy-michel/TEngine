/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseTrigger.cpp
 * \date 09/04/2009 Création de la classe IBaseTrigger.
 * \date 11/07/2010 Ajout de la liste des entités du trigger.
 * \date 13/07/2010 Suppression du paramètre start_disable.
 *                  Modification de la gestion des entités présentes dans le trigger.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 13/01/2011 Ajout des signaux.
 *                  Ajout d'un filtre à appliquer aux entités traversant le trigger.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 23/03/2012 Utilisation de la classe CFlags.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Game/Entities/IBaseTrigger.hpp"
#include "Game/Entities/IBaseAnimating.hpp"
#include "Physic/CPhysicEngine.hpp"

// DEBUG
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur. Avertit le CPhysicEngine de la création du trigger.
 *
 * \param name    Nom de l'entité.
 * \param enabled Indique si le trigger est activé au départ.
 * \param parent  Pointeur sur l'entité parent.
 ******************************/

IBaseTrigger::IBaseTrigger(const CString& name, bool enabled, ILocalEntity * parent) :
IBrushEntity (name, parent),
m_enable     (enabled),
m_filter     (0)
{
    //Game::physicEngine->addTrigger(this);
}


/**
 * Constructeur. Avertit le CPhysicEngine de la création du trigger.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseTrigger::IBaseTrigger(const CString& name, ILocalEntity * parent) :
IBrushEntity (name, parent),
m_enable     (true),
m_filter     (0)
{
    //Game::physicEngine->addTrigger(this);
}


/**
 * Destructeur. Avertit le CPhysicEngine de la destruction du trigger.
 ******************************/

IBaseTrigger::~IBaseTrigger()
{
    //Game::physicEngine->removeTrigger(this);
}


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IBaseTrigger::getClassName() const
{
    return "base_trigger";
}


/**
 * Indique si le trigger est actif.
 *
 * \return Booléen.
 *
 * \sa IBaseTrigger::Enable
 * \sa IBaseTrigger::Disable
 * \sa IBaseTrigger::Toggle
 ******************************/

bool IBaseTrigger::isEnable() const
{
    return m_enable;
}


/**
 * Donne le filtre à appliquer aux entités traversant le trigger.
 *
 * \return Filtre (ensemble de flags).
 *
 * \sa IBaseTrigger::setFilter
 ******************************/

CFlags<TTriggerFilter> IBaseTrigger::getFilter() const
{
    return m_filter;
}


/**
 * Modifie le filtre à appliquer aux entités traversant le trigger.
 *
 * \param filter Filtre (ensemble de flags).
 *
 * \sa IBaseTrigger::getFilter
 ******************************/

void IBaseTrigger::setFilter(CFlags<TTriggerFilter> filter)
{
    m_filter = filter;
}


/**
 * Modifie le filtre à appliquer aux entités traversant le trigger.
 *
 * \param filter Flag à ajouter au filtre.
 *
 * \sa IBaseTrigger::getFilter
 * \sa IBaseTrigger::setFilter
 ******************************/

void IBaseTrigger::AddFilter(TTriggerFilter filter)
{
    m_filter |= filter;
}


/**
 * Donne le nombre d'entités présentes dans le trigger.
 * Cette valeur représente le nombre d'entités qui étaient présentes lors de la
 * dernière frame, et ne tient pas compte des entités qui sont sorties depuis.
 *
 * \return Nombre d'entités présentes dans le trigger.
 ******************************/

unsigned int IBaseTrigger::getNumEntities() const
{
    return m_entities.size();
}


/**
 * Active le trigger.
 *
 * \sa IBaseTrigger::Disable
 * \sa IBaseTrigger::Toggle
 ******************************/

void IBaseTrigger::enable()
{
    m_enable = true;
}


/**
 * Désactive le trigger.
 *
 * \sa IBaseTrigger::Enable
 * \sa IBaseTrigger::Toggle
 ******************************/

void IBaseTrigger::disable()
{
    m_enable = false;
}


/**
 * Inverse l'activation du trigger.
 *
 * \sa IBaseTrigger::Enable
 * \sa IBaseTrigger::Disable
 ******************************/

void IBaseTrigger::toggle()
{
    m_enable = !m_enable;
}


/**
 * Méthode appellée lorsque qu'une entité se trouve dans le trigger.
 *
 * \param entity Pointeur sur l'entité.
 ******************************/

void IBaseTrigger::trigger(IBaseAnimating * entity)
{
    if (m_enable && entity)
    {
        // Vérification du filtre
        if (!(m_filter & FilterEverything) &&
            !((m_filter & FilterPlayer) && entity->isPlayer()) &&
            !((m_filter & FilterNPC   ) && entity->isNPC()))
        {
            return;
        }

        // On cherche l'entité dans la liste
        for (TTriggerEntityList::iterator it = m_entities.begin(); it != m_entities.end(); ++it)
        {
            if (it->entity == entity)
            {
                it->inside = true;

                sendSignal("onTrigger", entity);
                return;
            }
        }

        // L'entité n'est pas dans la lite
        m_entities.push_back(TTriggerEntity(entity));

        sendSignal("onStartTouch", entity);
        sendSignal("onTrigger", entity);
    }
}


/**
 * Méthode appellée à chaque frame.
 * Cette méthode doit être appellée depuis les classes dérivées pour gérer
 * correctement les entités qui sortent du trigger.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBaseTrigger::frame(unsigned int frameTime)
{
    // On parcourt la liste des entités
    for (TTriggerEntityList::iterator it = m_entities.begin(); it != m_entities.end(); )
    {
        // L'entité est sortie du trigger, on la supprime de la liste
        if (!it->inside)
        {
            IBaseAnimating * ent = it->entity;
            it = m_entities.erase(it);

            sendSignal("onEndTouch", ent);

            // La dernière entité est sortie du trigger
            if (m_entities.size() == 0)
            {
                sendSignal("onEndTouchAll");
            }

            continue;
        }

        it->time += frameTime;
        it->inside = false; // L'entité est sortie du trigger tant qu'on n'appelle pas la méthode Trigger

        ++it;
    }

    IBrushEntity::frame(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool IBaseTrigger::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Enable")
    {
        enable();
        return true;
    }

    if (slot == "Disable")
    {
        disable();
        return true;
    }

    if (slot == "Toggle")
    {
        toggle();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

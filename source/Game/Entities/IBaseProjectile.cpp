/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseProjectile.cpp
 * \date 11/02/2010 Création de la classe IBaseProjectile.
 * \date 14/07/2010 Ajout de l'arme et du joueur qui ont envoyés le projectile.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBaseProjectile.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseProjectile::IBaseProjectile(const CString& name, ILocalEntity * parent) :
IPhysicEntity (name, parent),
m_character   (nullptr),
m_weapon      (nullptr)
{ }


/**
 * Destructeur.
 ******************************/

IBaseProjectile::~IBaseProjectile()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IBaseProjectile::getClassName() const
{
    return "base_projectile";
}


/**
 * Donne le personnage qui a envoyé le projectile.
 *
 * \return Pointeur sur le personnage qui a envoyé le projectile.
 *
 * \sa IBaseProjectile::setCharacter
 ******************************/

IBaseCharacter * IBaseProjectile::getCharacter() const
{
    return m_character;
}


/**
 * Donne l'arme qui a envoyé le projectile.
 *
 * \return Pointeur sur l'arme qui a envoyé le projectile.
 *
 * \sa IBaseProjectile::setWeapon
 ******************************/

IBaseWeapon * IBaseProjectile::getWeapon() const
{
    return m_weapon;
}


/**
 * Modifie le personnage qui a envoyé le projectile.
 *
 * \param character Pointeur sur le personnage qui a envoyé le projectile.
 *
 * \sa IBaseProjectile::getCharacter
 ******************************/

void IBaseProjectile::setCharacter(IBaseCharacter * character)
{
    m_character = character;
}


/**
 * Modifie l'arme qui a envoyé le projectile.
 *
 * \param weapon Pointeur sur l'arme qui a envoyé le projectile.
 *
 * \sa IBaseProjectile::getWeapon
 ******************************/

void IBaseProjectile::setWeapon(IBaseWeapon * weapon)
{
    m_weapon = weapon;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPathNode.cpp
 * \date 10/05/2009 Création de la classe CPathNode.
 * \date 23/07/2010 Création de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression de l'attribut name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Game/Entities/CPathNode.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

CPathNode::CPathNode(const CString& name) :
IPointEntity (name)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CPathNode::getClassName() const
{
    return "path_node";
}


/**
 * Indique si un nœud est voisin de ce nœud.
 *
 * \return Booléen.
 ******************************/

bool CPathNode::isNeighboor(const CPathNode * pathnode) const
{
    return (std::find(m_neighboors.begin(), m_neighboors.end(), pathnode) != m_neighboors.end());
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CPathNode::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Donne le nombre de points voisins.
 *
 * \return Nombre de voisins.
 ******************************/

unsigned int CPathNode::getNumNeighboors() const
{
    return m_neighboors.size();
}


/**
 * Ajoute un nœud à la liste des voisins.
 *
 * \param pathnode Nœud à ajouter.
 ******************************/

void CPathNode::addNeighboor(CPathNode * pathnode)
{
    if (!isNeighboor(pathnode))
    {
        m_neighboors.push_front(pathnode);
    }
}

} // Namespace Ted

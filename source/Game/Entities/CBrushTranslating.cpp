/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBrushTranslating.cpp
 * \date 07/02/2010 Création de la classe CBrushTranslating.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 02/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 02/06/2011 Correction d'un bug dans le déplacement.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CBrushTranslating.hpp"
#include "Game/IModel.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CBrushTranslating::CBrushTranslating(const CString& name, ILocalEntity * parent) :
IBrushMoving (name, parent),
m_direction  (Origin3F)
{ }


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CBrushTranslating::CBrushTranslating(ILocalEntity * parent) :
IBrushMoving (CString(), parent),
m_direction  (Origin3F)
{ }


/**
 * Mutateur pour direction.
 *
 * \param direction Position finale du brush.
 *
 * \sa CBrushTranslating::getDirection
 ******************************/

void CBrushTranslating::setDirection(const TVector3F& direction)
{
    m_direction = direction;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CBrushTranslating::frame(unsigned int frameTime)
{
    if (m_locked)
        return;

    const float old_fraction = m_fraction;
    const float speed_frac = m_speed / (1000.0f * m_direction.norm());

    // Position finale
    if (m_fraction >= 1.0f)
    {
        // Le brush revient dans sa position initiale
        if (m_time_before_reset <= frameTime)
        {
            m_opening = false;
            setFraction(1.0f - static_cast<float>(frameTime - m_time_before_reset) * speed_frac);
            m_time_before_reset = 0;
        }
        else
        {
            m_time_before_reset -= frameTime;
        }
    }
    // Position intermédiaire, on déplace le brush
    else if (m_fraction > 0.0f)
    {
        if (m_opening)
        {
            setFraction(m_fraction + static_cast<float>(frameTime) * speed_frac);
        }
        else
        {
            setFraction(m_fraction - static_cast<float>(frameTime) * speed_frac);
        }
    }

    // Mise à jour de la position du modèle
    if (m_model)
    {
        m_model->translate((m_fraction - old_fraction) * m_direction);
    }

    IBrushEntity::frame(frameTime);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CEntityManager.cpp
 * \date 08/04/2009 Création de la classe CEntityManager.
 * \date 10/07/2010 Si on enlève une entité, la mémoire n'est plus désallouée.
 * \date 11/07/2010 On peut enlever une entité du gestionnaire (RemoveEntity) ou la supprimer de la mémoire (DeleteEntity).
 * \date 16/07/2010 Ajout de la méthode Debug_LogEntities.
 * \date 28/02/2011 Correction d'une erreur liée aux itérateurs.
 *                  Ajout d'une liste d'entités globales.
 * \date 23/03/2011 Création de la méthode getCorrectEntityId.
 * \date 25/03/2011 Changements dans le système des connexions.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Core/Utils.hpp"
#include "Game/CGameApplication.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/Entities/IGlobalEntity.hpp"

#ifdef T_DEBUG
#  include "Core/ILogger.hpp"
#  include "Core/CApplication.hpp"
#endif


namespace Ted
{

namespace Game
{
    CEntityManager * entityManager = nullptr;
}


/**
 * Constructeur par défaut.
 ******************************/

CEntityManager::CEntityManager()
{
    if (Game::entityManager)
        exit(-1);

    Game::entityManager = this;
}


/**
 * Destructeur.
 ******************************/

CEntityManager::~CEntityManager()
{
    deleteEntities();
    Game::entityManager = nullptr;
}


/**
 * Donne le nombre d'entités.
 *
 * \return Nombre d'entités.
 ******************************/

unsigned int CEntityManager::getNumEntities() const
{
    return m_entities.size();
}


/**
 * Donne le nombre de connexions en attente.
 *
 * \return Nombre de connexions.
 ******************************/

unsigned int CEntityManager::getNumConnections() const
{
    return m_connections.size();
}


/**
 * Recherche une entité à partir de son nom.
 *
 * \param name Nom de l'entité à rechercher.
 * \return Pointeur sur l'entité. Si aucune entité ne porte le nom demandé, le pointeur est
 *         invalide. Si plusieurs entités portent ce nom, la première entité trouvée est retournée.
 ******************************/

IEntity * CEntityManager::getEntityByName(const CString& name) const
{
    for (std::list<IEntity *>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        if ((*it)->getName() == name)
        {
            return *it;
        }
    }

    return nullptr;
}


/**
 * Enlève une entité du gestionnaire.
 *
 * \param entity Pointeur sur l'entité à enlever.
 ******************************/

void CEntityManager::removeEntity(IEntity * entity)
{
    // On cherche dans la liste des entités
    std::list<IEntity *>::iterator it = std::find(m_entities.begin(), m_entities.end(), entity);

    if (it != m_entities.end())
    {
        m_entities.erase(it);

        // On cherche dans la liste des entités temporaires
        for (std::list<CTemporaryEntity>::iterator it2 = m_tmp_entities.begin(); it2 != m_tmp_entities.end(); )
        {
            if (it2->entity == entity)
            {
                it2 = m_tmp_entities.erase(it2);
                //break;
            }
            else
            {
                ++it2;
            }
        }

        // On cherche dans la liste des entités globales
        for (std::list<IGlobalEntity *>::iterator it2 = m_global_entities.begin(); it2 != m_global_entities.end(); )
        {
            if (*it2 == entity)
            {
                it2 = m_global_entities.erase(it2);
                //break;
            }
            else
            {
                ++it2;
            }
        }
    }
}


/**
 * Supprime toutes les entités du gestionnaire et libère la mémoire.
 ******************************/

void CEntityManager::deleteEntities()
{
    for (std::list<IEntity *>::iterator it = m_entities.begin(); it != m_entities.end(); /*++it*/)
    {
        IEntity * ent = *it;
        it = m_entities.erase(it);
        delete ent;
    }

    m_tmp_entities.clear();
    m_global_entities.clear();
}


/**
 * Ajoute une entité.
 *
 * \param entity Pointeur sur l'entité à ajouter.
 ******************************/

void CEntityManager::addEntity(IEntity * entity)
{
    std::list<IEntity *>::iterator it = std::find(m_entities.begin(), m_entities.end(), entity);

    if (it == m_entities.end())
    {
        m_entities.insert(it, entity);
    }
}


/**
 * Ajoute une entité.
 *
 * \param entity Pointeur sur l'entité à ajouter.
 ******************************/

void CEntityManager::addGlobalEntity(IGlobalEntity * entity)
{
    std::list<IGlobalEntity *>::iterator it = std::find(m_global_entities.begin(), m_global_entities.end(), entity);

    if (it == m_global_entities.end())
    {
        m_global_entities.push_back(entity);
    }

    addEntity(entity);
}


/**
 * Ajoute une entité temporaire.
 *
 * \param entity Pointeur sur l'entité à ajouter.
 * \param time   Durée en millisecondes avant de supprimer l'entité.
 ******************************/

void CEntityManager::addTemporaryEntity(IEntity * entity, const unsigned int time)
{
    if (std::find(m_entities.begin(), m_entities.end(), entity) == m_entities.end())
    {
        m_entities.push_back(entity);
    }

    // On cherche dans la liste des entités temporaires
    for (std::list<CTemporaryEntity>::iterator it = m_tmp_entities.begin(); it != m_tmp_entities.end(); ++it)
    {
        // On diminue le temps qu'il reste avant la suppression de l'entité
        if (it->entity == entity)
        {
            return;
        }
    }

    CTemporaryEntity tmp(entity, time);
    m_tmp_entities.push_back(tmp);
}


/**
 * Méthode exécutée à chaque frame.
 *
 * \todo Ne plus mettre à jour les entités dans cette méthode.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CEntityManager::frame(unsigned int frameTime)
{
    // Pour chaque connexion en attente
    for (std::list<CConnection>::iterator it = m_connections.begin(); it != m_connections.end(); )
    {
        // On diminue le temps qu'il reste avant l'appel du slot
        if (it->time > frameTime)
        {
            it->time -= frameTime;
        }
        else
        {
            it->time = 0;
        }

        // Il est temps d'exécuter la connexion
        if (it->time == 0)
        {
            runConnection(*it);

            // On supprime la connexion et on passe à la suivante
            it = m_connections.erase(it);
        }
        else
        {
            // On passe à la connexion suivante
            ++it;
        }
    }

    // Pour chaque entité temporaire
    for (std::list<CTemporaryEntity>::iterator it = m_tmp_entities.begin(); it != m_tmp_entities.end(); /*++it*/)
    {
        // On diminue le temps qu'il reste avant la suppression de l'entité
        if (it->time > frameTime)
        {
            it->time -= frameTime;
        }
        else
        {
            it->time = 0;
        }

        // Suppression de l'entité
        if (it->time == 0)
        {
            IEntity * ent = it->entity;
            it = m_tmp_entities.erase(it);
            delete ent;
        }
        else
        {
            ++it;
        }
    }
/*
    // Pour chaque entité
    // TODO: Supprimer ce bloc
    for (std::list<IEntity *>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        (*it)->frame(frameTime);
    }
*/

    // Mise-à-jour des entités globales
    for (std::list<IGlobalEntity *>::const_iterator it = m_global_entities.begin(); it != m_global_entities.end(); ++it)
    {
        (*it)->frame(frameTime);
    }
}


/**
 * Exécuter une connexion.
 *
 * \todo Appeler le slot su joueur si demandé.
 *
 * \param connection Connexion à exécuter.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CEntityManager::runConnection(const CConnection& connection)
{
    // Mise en file d'attente
    if (connection.time > 0)
    {
        m_connections.push_back(connection);
        return true;
    }
    else
    {
        unsigned short nbr = 0;
        bool no_error = true;

        std::list<CString> elements = connection.ent.split(',', false);

        for (std::list<CString>::const_iterator it2 = elements.begin(); it2 != elements.end(); ++it2)
        {
            CString element = *it2;
/*
        std::ptrdiff_t start = 0;
        std::ptrdiff_t pos = connection.ent.IndexOf(',');

        // Découpage de la chaine
        for ( ; ; )
        {
            if (pos > start)
            {
                CString element = connection.ent.SubString(start, pos - start);
*/
                if (!element.isEmpty())
                {
                    if (element == "player")
                    {
                        //
                    }
                    else if (element == "caller")
                    {
                        if (connection.caller)
                        {
                            // Exécute la connexion
                            if (!connection.caller->callSlot(connection.slot, connection.param))
                            {
                                no_error = false;
                                ++nbr;
                            }
                        }
                    }
                    else
                    {
                        // Recherche des entités correspondantes
                        for (std::list<IEntity *>::iterator it = m_entities.begin(); it != m_entities.end(); ++it)
                        {
                            if ((*it)->getName() == element)
                            {
                                // Exécute la connexion
                                if (!(*it)->callSlot(connection.slot, connection.param))
                                {
                                    no_error = false;
                                }

                                ++nbr;
                            }
                        }
                    }
                }
            }
/*
            // Dernier élément
            if (pos == -1)
            {
                break;
            }

            start = pos + 1;
            pos = connection.ent.indexOf(',', start);
        }
*/
        // Aucune entité trouvée
        if (nbr == 0)
        {
            no_error = false;
        }

        return no_error;
    }
}


#ifdef T_DEBUG

/**
 * Log la liste des entités.
 ******************************/

void CEntityManager::Debug_LogEntities() const
{
    CApplication::getApp()->log(CString::fromUTF8("\nListe des entités :"));

    // Pour chaque entité
    for (std::list<IEntity *>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        CApplication::getApp()->log(CString(" - %1 : %2 (%3)").arg((*it)->getName()).arg((*it)->getClassName()).arg(CString::fromPointer(*it)));
    }

    CApplication::getApp()->log("\n");
}

#endif

} // Namespace Ted

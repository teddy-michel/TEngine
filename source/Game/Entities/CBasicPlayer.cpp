/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CBasicPlayer.cpp
 * \date 30/04/2012 Création de la classe CBasicPlayer.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <bullet/BulletDynamics/Character/btKinematicCharacterController.h>

#include "Game/Entities/CBasicPlayer.hpp"
#include "Game/IModel.hpp"
/*
#ifdef T_USE_MODELS_V2
#  include "Game/IModelV2.hpp"
#else
#  include "Game/CStaticModel.hpp"
#endif
*/
#include "Graphic/CBuffer.hpp"


using namespace Ted;


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CBasicPlayer::CBasicPlayer(const CString& name, ILocalEntity * parent) :
IBasePlayer (name, parent)
{
    IModel * model = new IModel(nullptr);
/*
#ifdef T_USE_MODELS_V2
    IModel * model = new IModel(nullptr);
#else
    CStaticModel * model = new CStaticModel();
#endif
*/
    setModel(model);

    //Game::physicEngine->removeModel(model);
    Game::physicEngine->removeRigidBody(model->getRigidBody());

    // Suppression du buffer graphique
    CBuffer * buffer = model->getBuffer();
    delete buffer;
    model->setBuffer(nullptr);

    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(btVector3(-100, 0, 0));

    btPairCachingGhostObject * m_ghostObject = new btPairCachingGhostObject();
    m_ghostObject->setWorldTransform(startTransform);

    btScalar characterHeight = 100;
    btScalar characterWidth = 40;
    btCapsuleShapeZ * capsule = new btCapsuleShapeZ(characterWidth, characterHeight);
    m_ghostObject->setCollisionShape(capsule);
    m_ghostObject->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

    btScalar stepHeight = btScalar(2);
    btKinematicCharacterController * m_character = new btKinematicCharacterController(m_ghostObject, capsule, stepHeight, 2);
    //m_character->setWalkDirection(btVector3(0.2, 1.1, 0));

    btDiscreteDynamicsWorld * world = Game::physicEngine->getWorld();
	world->addCollisionObject(m_ghostObject, btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter);
	world->addAction(m_character);

    world->getPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
}


/**
 * Destructeur.
 ******************************/

CBasicPlayer::~CBasicPlayer()
{ }

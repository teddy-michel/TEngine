/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CExplosion.cpp
 * \date 07/07/2010 Création de la classe CExplosion.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 22/07/2010 La méthode getClassName est déclarée inline.
 * \date 05/12/2010 Plus de méthodes inline, et ajout de paramètres.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 02/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création du signal onExplode.
 * \date 06/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CGameApplication.hpp"
#include "Game/Entities/CExplosion.hpp"
#include "Game/Entities/CSprite.hpp"
#include "Graphic/CRenderer.hpp"
#include "Sound/CSoundEngine.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

const unsigned short ExplosionDuration = 1000; ///< Durée d'une explosion en millisecondes.
const unsigned short ExplosionSizeD = 64;      ///< Dimensions du sprite au début de l'explosion.
const unsigned short ExplosionSizeF = 128;     ///< Dimensions du sprite à la fin de l'explosion.
const float ExplosionFactor = (ExplosionSizeF - ExplosionSizeD) / ExplosionDuration;


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CExplosion::CExplosion(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_damage     (true),
m_repeatable (false),
m_done       (false),
m_radius     (128.0f),
m_sound      (),
m_sprite     (nullptr),
m_time       (0)
{
    m_sprite = new CSprite(this);
    T_ASSERT(m_sprite);
    m_sprite->setTextureId(1);
}


/**
 * Destructeur.
 ******************************/

CExplosion::~CExplosion()
{
    delete m_sprite;
}


/**
 * Modifie la position de l'explosion.
 *
 * \param position Position de l'explosion.
 ******************************/

void CExplosion::setPosition(const TVector3F& position)
{
    m_position = position;

    m_matrix = TMatrix4F(1.0f, 0.0f, 0.0f, m_position.X,
                         0.0f, 1.0f, 0.0f, m_position.Y,
                         0.0f, 0.0f, 1.0f, m_position.Z,
                         0.0f, 0.0f, 0.0f, 1.0f);
}


/**
 * Modifie la valeur de damage.
 *
 * \param damage Indique si l'explosion entraine des dégâts.
 *
 * \sa CExplosion::isDamage
 ******************************/

void CExplosion::setDamage(bool damage)
{
    m_damage = damage;
}


/**
 * Modifie la valeur de repeatable.
 *
 * \param repeatable Indique si l'explosion peut se produire plusieurs fois.
 *
 * \sa CExplosion::isRepeatable
 ******************************/

void CExplosion::setRepeatable(bool repeatable)
{
    m_repeatable = repeatable;
}


/**
 * Modifie le son à jouer pendant l'explosion.
 *
 * \param sound Nom du son à jouer pendant l'explosion.
 *
 * \sa CExplosion::getSound
 ******************************/

void CExplosion::setSound(const CString& sound)
{
    m_sound = sound;
}


/**
 * Active l'explosion.
 *
 * \todo Implémentation.
 ******************************/

void CExplosion::explode()
{
    if (!m_repeatable && m_done)
    {
        return;
    }

    m_time = ExplosionDuration;

    // Lecture du son de l'explosion
    if (!m_sound.isEmpty())
    {
        Game::soundEngine->playSound(m_sound);
    }

    sendSignal("onExplode");

    m_done = true;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Mettre-à-jour les dimensions du billboard.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CExplosion::frame(unsigned int frameTime)
{
    // Explosion en cours
    if (m_time > 0)
    {
        // Modification de la durée restante
        if (m_time > frameTime)
        {
            m_time -= frameTime;
        }
        else
        {
            m_time = 0;
        }

        // Modification des dimensions du sprite
        unsigned short size = ExplosionSizeD + ExplosionFactor * (ExplosionDuration - m_time);
        m_sprite->setSize(size, size);

        Game::renderer->pushMatrix(MatrixModelView);
        Game::renderer->multMatrix(MatrixModelView, m_matrix);

        m_sprite->frame(frameTime);

        // Traitement des entités enfant
        ILocalEntity::frame(frameTime);

        Game::renderer->popMatrix(MatrixModelView);
    }
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CExplosion::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Explode")
    {
        explode();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

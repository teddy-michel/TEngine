/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseAnimating.cpp
 * \date 11/04/2009 Création de la classe IBaseAnimating.
 * \date 13/07/2010 Le nombre de points de vie est désormais géré par cette classe.
 * \date 14/07/2010 Ajout de la méthode WeaponImpact.
 * \date 16/07/2010 Ajout de la méthode ProjectileImpact.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 16/12/2010 Implémentation de la méthode Frame.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création des méthodes isPlayer et isNPC.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBaseAnimating.hpp"
#include "Physic/CPhysicEngine.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseAnimating::IBaseAnimating(const CString& name, ILocalEntity * parent) :
IBrushEntity (name, parent),
m_health     (100.0f)
{ }


/**
 * Destructeur.
 ******************************/

IBaseAnimating::~IBaseAnimating()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IBaseAnimating::getClassName() const
{
    return "base_animating";
}


/**
 * Accesseur pour health.
 *
 * \return Points de vie.
 *
 * \sa IBaseAnimating::setHealth
 ******************************/

float IBaseAnimating::getHealth() const
{
    return m_health;
}


/**
 * Indique si l'entité est vivante.
 *
 * \return Booléen.
 *
 * \sa IBaseAnimating::getHealth
 ******************************/

bool IBaseAnimating::isAlive() const
{
    return (m_health > 0.0f);
}


/**
 * Indique si l'entité est un joueur.
 *
 * \return false.
 ******************************/

bool IBaseAnimating::isPlayer() const
{
    return false;
}


/**
 * Indique si l'entité est un NPC.
 *
 * \return false.
 ******************************/

bool IBaseAnimating::isNPC() const
{
    return false;
}


/**
 * Ajoute des points de vie.
 *
 * \param health Nombre de points à ajouter.
 *
 * \sa IBaseAnimating::setHealth
 ******************************/

void IBaseAnimating::addHealth(float health)
{
    m_health = (m_health + health < 0.0f ? 0.0f : m_health + health);
}


/**
 * Définit le nombre de points de vie.
 *
 * \param health Nombre de points.
 *
 * \sa IBaseAnimating::getHealth
 ******************************/

void IBaseAnimating::setHealth(float health)
{
    m_health = (health < 0.0f ? 0.0f : health);
}


/**
 * Gestion de l'impact fait par une arme.
 * Cette méthode doit être redéfinie dans les entités dérivées.
 *
 * \param damage    Nombre de points à enlever (ou à ajouter).
 * \param character Pointeur sur le personnage qui a tiré.
 * \param weapon    Pointeur sur l'arme à l'origine de l'impact.
 ******************************/

void IBaseAnimating::weaponImpact(float damage, IBaseCharacter * character, IBaseWeapon * weapon)
{
    T_UNUSED(damage);
    T_UNUSED(character);
    T_UNUSED(weapon);
}


/**
 * Gestion de l'impact fait par un projectile.
 * Cette méthode doit être redéfinie dans les entités dérivées.
 *
 * \param damage     Nombre de points à enlever (ou à ajouter).
 * \param projectile Pointeur sur le projectile à l'origine de l'impact.
 ******************************/

void IBaseAnimating::projectileImpact(float damage, IBaseProjectile * projectile)
{
    T_UNUSED(damage);
    T_UNUSED(projectile);
}


/**
 * Met-à-jour l'entité.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBaseAnimating::frame(unsigned int frameTime)
{
    //Game::physicEngine->checkTriggers(this);

    IBrushEntity::frame(frameTime);
}

} // Namespace Ted

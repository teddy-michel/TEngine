/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTeam.cpp
 * \date 21/05/2009 Création de la classe CTeam.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Game/Entities/CTeam.hpp"
#include "Game/Entities/IBasePlayer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

CTeam::CTeam(const CString& name) :
IGlobalEntity (name)
{ }


/**
 * Destructeur.
 ******************************/

CTeam::~CTeam()
{
    ClearPlayers();
}


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CTeam::getClassName() const
{
    return "team";
}


/**
 * Accesseur pour deads.
 *
 * \return Nombre de joueurs morts dans l'équipe.
 ******************************/

unsigned int CTeam::getNumDeads() const
{
    return m_deads;
}


/**
 * Accesseur pour points.
 *
 * \return Nombre de points remportés.
 ******************************/

unsigned int CTeam::getNumPoints() const
{
    return m_points;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTeam::Frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Donne le nombre de joueurs dans l'équipe
 *
 * \return Nombre de joueur
 ******************************/

unsigned int CTeam::getNumPlayers() const
{
    return m_players.size();
}


/**
 * Ajoute un joueur à l'équipe.
 *
 * \param player Pointeur sur le joueur à ajouter.
 ******************************/

void CTeam::AddPlayer(IBasePlayer * player)
{
    if (std::find(m_players.begin(), m_players.end(), player) == m_players.end())
        m_players.push_back(player);
}


/**
 * Enlève un joueur à l'équipe.
 *
 * \param player Pointeur sur le joueur à enlever.
 ******************************/

void CTeam::DeletePlayer(IBasePlayer * player)
{
    for (std::vector<IBasePlayer *>::iterator it = m_players.begin() ; it != m_players.end() ; ++it)
    {
        if ((*it) == player)
        {
            m_players.erase(it);
            return;
        }
    }
}


/**
 * Supprime tous les joueurs de l'équipe.
 ******************************/

void CTeam::ClearPlayers()
{
    for (std::vector<IBasePlayer *>::iterator it = m_players.begin() ; it != m_players.end() ; ++it)
    {
        delete *it;
    }

    m_players.clear();
    m_deads = 0;
}

} // Namespace Ted

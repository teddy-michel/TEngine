/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseCharacter.cpp
 * \date 17/04/2009 Création de la classe IBaseCharacter.
 * \date 13/07/2010 Le nombre de points de vie est déplacé vers la classe IBaseAnimating.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 20/04/2012 Le modèle associé est bloqué en rotation.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBaseCharacter.hpp"
#include "Game/IModel.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseCharacter::IBaseCharacter(const CString& name, ILocalEntity * parent) :
IBaseAnimating (name, parent),
m_angleH       (0.0f),
m_angleV       (0.0f)
{ }


void IBaseCharacter::setAngleH(float angleH)
{
    m_angleH = angleH;
    IBaseAnimating::setRotation(CQuaternion(std::cos(angleH/2), 0, 0, std::sin(angleH/2)));
}


void IBaseCharacter::setAngleV(float angleV)
{
    m_angleV = angleV;
}


/**
 * Modifie le modèle utilisé par le personnage.
 *
 * \param model Pointeur sur le modèle utilisé par le personnage.
 *
 * \sa IBrushEntity::getModel
 ******************************/

void IBaseCharacter::setModel(IModel * model)
{
#ifdef T_USING_PHYSIC_BULLET
    if (model)
    {
        model->getRigidBody()->setAngularFactor(0);
    }
#endif
    IBaseAnimating::setModel(model);
}


/**
 * Modifie la position de l'entité.
 *
 * \param position Nouvelle position de l'entité.
 ******************************/

void IBaseCharacter::setPosition(const TVector3F& position)
{
    IBrushEntity::setPosition(position);
}


/**
 * Translate l'entité.
 *
 * \param v Vecteur de translation.
 *
 * \sa IBaseCharacter::setPosition
 ******************************/

void IBaseCharacter::translate(const TVector3F& v)
{
    IBrushEntity::translate(v);
}


/**
 * Gestion de l'impact fait par une arme.
 *
 * \todo Multiplier le nombre de points de dégâts en fonction de la localisation de l'impact (tête, corps, etc.).
 *
 * \param damage    Nombre de points à enlever (ou à ajouter).
 * \param character Pointeur sur le personnage qui a tiré.
 * \param weapon    Pointeur sur l'arme à l'origine de l'impact.
 ******************************/

void IBaseCharacter::weaponImpact(float damage, IBaseCharacter * character, IBaseWeapon * weapon)
{
    T_UNUSED(character);
    T_UNUSED(weapon);

    addHealth(-damage);
}

} // Namespace Ted

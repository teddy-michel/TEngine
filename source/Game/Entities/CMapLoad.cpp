/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CMapLoad.cpp
 * \date 18/04/2009 Création de la classe CMapLoad.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 23/07/2010 La méthode getClassName est déclarée inline.
 * \date 06/12/2010 Plus de méthodes inline, le signal est envoyé au premier appel de la méthode Frame.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CMapLoad.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

CMapLoad::CMapLoad(const CString& name) :
IGlobalEntity (name),
m_load        (false)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CMapLoad::getClassName() const
{
    return "map_load";
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CMapLoad::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);

    if (!m_load)
    {
        sendSignal("onMapLoad");
        m_load = true;
    }
}

} // Namespace Ted

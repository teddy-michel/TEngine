/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBrushEntity.cpp
 * \date 05/02/2010 Création de la classe IBrushEntity.
 * \date 14/07/2010 Déplacement de la méthode WeaponImpact.
 * \date 17/07/2010 Définition de la méthode Frame.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 17/12/2010 Héritage de la classe ILocalEntity.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBrushEntity.hpp"
#include "Game/IModel.hpp"
#include "Graphic/CRenderer.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param model  Pointeur sur le modèle à utiliser.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBrushEntity::IBrushEntity(const CString& name, IModel * model, ILocalEntity * parent) :
ILocalEntity (name, parent),
m_model      (model)
{ }


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBrushEntity::IBrushEntity(const CString& name, ILocalEntity * parent) :
ILocalEntity (name, parent),
m_model      (nullptr )
{ }


/**
 * Destructeur. Supprime le modèle attaché à l'entité.
 *
 * \todo Revoir la gestion mémoire pour le modèle.
 ******************************/

IBrushEntity::~IBrushEntity()
{
    delete m_model;
}


/**
 * Donne le modèle utilisé par l'entité.
 *
 * \return Pointeur sur le modèle utilisé par l'entité
 *
 * \sa IBrushEntity::setModel
 ******************************/

IModel * IBrushEntity::getModel() const
{
    return m_model;
}


/**
 * Retourne la position de l'entité.
 *
 * \return Position de l'entité.
 *
 * \sa IBrushEntity::setPosition
 ******************************/

TVector3F IBrushEntity::getPosition() const
{
    TVector3F pos = (m_model ? m_model->getPosition() : Origin3F);

    ILocalEntity * parent = getParent();
    if (parent)
        pos += parent->getPosition();

    return pos;
}


/**
 * Modifie le modèle utilisé par l'entité.
 * L'ancien modèle n'est pas détruit.
 *
 * \param model Pointeur sur le modèle utilisé par l'entité.
 *
 * \sa IBrushEntity::getModel
 ******************************/

void IBrushEntity::setModel(IModel * model)
{
    if (m_model)
    {
        m_model->setEntity(nullptr);
    }

    m_model = model;

    if (m_model)
    {
        m_model->setEntity(this);
    }
}


/**
 * Définit la position de l'entité.
 * Cette méthode permet de déplacer le modèle associé à l'entité.
 *
 * \param position Position de l'entité.
 *
 * \sa IBrushEntity::getPosition
 * \sa IBrushEntity::translate
 ******************************/

void IBrushEntity::setPosition(const TVector3F& position)
{
    if (m_model)
    {
        m_model->setPosition(position);
    }
}


/**
 * Translate l'entité.
 *
 * \param v Vecteur de translation.
 *
 * \sa IBrushEntity::setPosition
 ******************************/

void IBrushEntity::translate(const TVector3F& v)
{
    if (m_model)
        m_model->translate(v);
}


void IBrushEntity::setRotation(const CQuaternion& rotation)
{
    if (m_model)
        m_model->setRotation(rotation);
}


/**
 * Met-à-jour l'entité.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBrushEntity::frame(unsigned int frameTime)
{
    Game::renderer->pushMatrix(MatrixModelView);

    if (m_model)
    {
        m_model->update(frameTime);
        m_model->drawModel();
    }

    // Mise-à-jour des entités enfant
    ILocalEntity::frame(frameTime);

    Game::renderer->popMatrix(MatrixModelView);
}

} // Namespace Ted

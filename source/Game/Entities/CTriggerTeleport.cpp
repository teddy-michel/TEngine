/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerTeleport.cpp
 * \date 28/03/2011 Création de la classe CTriggerTeleport.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CTriggerTeleport.hpp"
#include "Game/Entities/IBaseAnimating.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerTeleport::CTriggerTeleport(const CString& name, ILocalEntity * parent) :
IBaseTrigger (name, parent)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CTriggerTeleport::getClassName() const
{
    return "trigger_teleport";
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTriggerTeleport::frame(unsigned int frameTime)
{
    // Téléportation des entités
    for (std::list<TTriggerEntity>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        if (it->inside)
        {
            T_ASSERT(it->entity);
            it->entity->setPosition(m_destination);
        }
    }

    IBaseTrigger::frame(frameTime);
}

} // Namespace Ted

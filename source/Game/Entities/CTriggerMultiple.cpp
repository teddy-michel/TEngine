/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CTriggerMultiple.cpp
 * \date 09/04/2009 Création de la classe CTriggerMultiple.
 * \date 16/11/2010 Ajout d'un compteur d'activation du trigger.
 *                  Ajout d'un nombre maximal d'activations possibles.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CTriggerMultiple.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CTriggerMultiple::CTriggerMultiple(const CString& name, ILocalEntity * parent) :
IBaseTrigger (name, parent),
m_count      (0),
m_max_count  (static_cast<unsigned int>(-1))
{ }


/**
 * Modifie le nombre maximal d'activations du trigger.
 *
 * \param max Nombre maximal d'activations.
 *
 * \sa CTriggerMultiple::getMaxCount
 ******************************/

void CTriggerMultiple::setMaxCount(unsigned int max)
{
    m_max_count = max;
}


/**
 * Méthode appellée lorsque qu'une entité se trouve dans le trigger.
 *
 * \param entity Pointeur sur l'entité.
 ******************************/

void CTriggerMultiple::trigger(IBaseAnimating * entity)
{
    ++m_count;

    if (m_count <= m_max_count)
    {
        IBaseTrigger::trigger(entity);
    }
}


/**
 * Méthode appellée à chaque frame.
 * Cette méthode doit-elle faire quelque chose en particulier ?
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CTriggerMultiple::frame(unsigned int frameTime)
{
    //...

    IBaseTrigger::frame(frameTime);
}

} // Namespace Ted

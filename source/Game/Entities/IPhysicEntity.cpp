/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IPhysicEntity.cpp
 * \date 12/04/2009 Création de la classe IPhysicEntity.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IPhysicEntity.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IPhysicEntity::IPhysicEntity(ILocalEntity * parent) :
IBaseAnimating (CString(), parent)
{ }


/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IPhysicEntity::IPhysicEntity(const CString& name, ILocalEntity * parent) :
IBaseAnimating (name, parent)
{ }


/**
 * Destructeur.
 ******************************/

IPhysicEntity::~IPhysicEntity()
{ }

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CGrenade.cpp
 * \date 14/07/2010 Création de la classe CGrenade.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 14/01/2011 Ajout des signaux.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CGrenade.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/CGameApplication.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CGrenade::CGrenade(const CString& name, ILocalEntity * parent) :
IBaseProjectile    (name, parent),
m_time             (3000),
m_explode_on_touch (false)
{ }


/**
 * Donne la durée avant que la granade explose.
 * -1 indique la grenade n'explose pas seule.
 *
 * \return Durée en millisecondes avant que la grenade explose.
 * \sa CGrenade::setTime
 ******************************/

unsigned int CGrenade::getTime() const
{
    return m_time;
}


/**
 * Indique si la grenade explose lorsqu'elle touche un personnage.
 *
 * \return Booléen
 * \sa CGrenade::setExplodeOnTouch
 ******************************/

bool CGrenade::isExplodeOnTouch() const
{
    return m_explode_on_touch;
}


/**
 * Modifie la durée avant que la granade explose.
 *
 * \param time Durée en millisecondes avant que la grenade explose.
 * \sa CGrenade::getTime
 ******************************/

void CGrenade::setTime(unsigned int time)
{
    m_time = time;
}


/**
 * Fait exploser la grenade lorsqu'elle touche un personnage.
 *
 * \param e Indique si la grenade explose lorsqu'elle touche un personnage.
 *
 * \sa CGrenade::isExplodeOnTouch
 ******************************/

void CGrenade::setExplodeOnTouch(bool e)
{
    m_explode_on_touch = e;
}


/**
 * Fait exploser la grenade.
 *
 * \todo Implémentation.
 ******************************/

void CGrenade::explode()
{
    //...

    sendSignal("onExplode");

    // Suppression de l'entité
    Game::entityManager->addTemporaryEntity(this, 0);
}

/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame.
 ******************************/

void CGrenade::frame(unsigned int frameTime)
{
    if (m_time < frameTime)
    {
        m_time = 0;
        explode();
    }
    else
    {
        m_time -= frameTime;
    }

    IBaseAnimating::frame(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CGrenade::callSlot(const CString& slot, const CString& param)
{
    if (slot == "Explode")
    {
        explode();
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

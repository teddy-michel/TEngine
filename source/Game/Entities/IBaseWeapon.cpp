/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseWeapon.cpp
 * \date 11/04/2009 Création de la classe IBaseWeapon.
 * \date 09/07/2010 Création des classes dérivées pour chaque type d'arme.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 09/04/2011 Ajout du paramètre parent au constructeur.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBaseWeapon.hpp"
#include "Game/Entities/IBaseCharacter.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseWeapon::IBaseWeapon(const CString& name, ILocalEntity * parent) :
IBaseAnimating       (name, parent),
m_timeBeforeFire     (0),
m_timeBetweenTwoFire (0),
m_primaryAttack      (false),
m_secondaryAttack    (false),
m_canFireUnderwater  (false),
m_minDamage          (0.0f),
m_maxDamage          (0.0f),
m_minDamageDistance  (0.0f),
m_maxDamageDistance  (0.0f),
m_randomDamage       (0.0f),
m_character          (nullptr)
{ }


/**
 * Destructeur.
 ******************************/

IBaseWeapon::~IBaseWeapon()
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString IBaseWeapon::getClassName() const
{
    return "base_weapon";
}


/**
 * Accesseur pour time_before_fire.
 *
 * \return Durée avant de pouvoir tirer à nouveau en millisecondes.
 ******************************/

unsigned int IBaseWeapon::getTimeBeforeFire() const
{
    return m_timeBeforeFire;
}


/**
 * Accesseur pour time_between_two_fire.
 *
 * \return Durée entre deux tirs en millisecondes.
 ******************************/

unsigned int IBaseWeapon::getTimeBetweenTwoFire() const
{
    return m_timeBetweenTwoFire;
}


/**
 * \brief Accesseur pour fire.
 *
 * \return Booléen.
 ******************************/

bool IBaseWeapon::isFire() const
{
    return (m_primaryAttack || m_secondaryAttack);
}


/**
 * Accesseur pour can_fire_underwater.
 *
 * \return Booléen.
 ******************************/

bool IBaseWeapon::canFireUnderWater() const
{
    return m_canFireUnderwater;
}


/**
 * Accesseur pour character.
 *
 * \return Pointeur sur le personnage qui tient l'arme.
 ******************************/

IBaseCharacter * IBaseWeapon::getCharacter() const
{
    return m_character;
}


/**
 * Mutateur pour character.
 *
 * \param character Pointeur sur le personnage qui tient l'arme.
 ******************************/

void IBaseWeapon::setCharacter(IBaseCharacter * character)
{
    m_character = character;
}


/**
 * On demande à l'arme de tirer (attaque primaire).
 ******************************/

void IBaseWeapon::primaryAttack()
{
    m_primaryAttack = true;
}


/**
 * On demande à l'arme de tirer (attaque secondaire).
 ******************************/

void IBaseWeapon::secondaryAttack()
{
    m_secondaryAttack = true;
}


/**
 * Méthode appelée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBaseWeapon::frame(unsigned int frameTime)
{
    if (m_primaryAttack)
    {
        if (m_timeBetweenTwoFire > 0)
        {
            if (m_timeBeforeFire > frameTime)
            {
                m_timeBeforeFire -= frameTime;
            }
            else
            {
                // Tir
                fire();

                m_timeBeforeFire = m_timeBetweenTwoFire + m_timeBeforeFire - (frameTime % m_timeBetweenTwoFire);
            }
        }
        else
        {
            // Tir
            fire();
        }

        m_primaryAttack = false;
    }
    else
    {
        if (m_timeBeforeFire > frameTime)
        {
            m_timeBeforeFire -= frameTime;
        }
        else if (m_timeBeforeFire > 0)
        {
            m_timeBeforeFire = 0;
        }
    }

    IBaseAnimating::frame(frameTime);
}


/**
 * Recharge l'arme.
 ******************************/

void IBaseWeapon::reload()
{

}

} // Namespace Ted

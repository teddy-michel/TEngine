/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CRope.cpp
 * \date 09/02/2010 Création de la classe CRope.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CRope.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name Nom de l'entité.
 ******************************/

CRope::CRope (const CString& name) :
IPointEntity (name),
m_point      (Origin3F),
m_color      (CColor::Black),
m_subdiv     (8)
{ }


/**
 * Donne le nom de la classe correspondant à l'entité.
 *
 * \return Nom de la classe de l'entité.
 ******************************/

CString CRope::getClassName() const
{
    return "rope";
}


/**
 * Accesseur pour point.
 *
 * \return Point d'arrivée du faisceau.
 *
 * \sa CRope::setPoint
 ******************************/

TVector3F CRope::getPoint() const
{
    return m_point;
}


/**
 * Accesseur pour color.
 *
 * \return Couleur du faisceau.
 *
 * \sa CRope::setColor
 ******************************/

CColor CRope::getColor() const
{
    return m_color;
}


/**
 * Accesseur pour subdiv.
 *
 * \return Nombre de subdivisions du cable.
 *
 * \sa CRope::setSubdiv
 ******************************/

unsigned short CRope::getSubdiv() const
{
    return m_subdiv;
}


/**
 * Mutateur pour point.
 *
 * \param point Point d'arrivée du faisceau.
 *
 * \sa CRope::getPoint
 ******************************/

void CRope::setPoint(const TVector3F& point)
{
    m_point = point;
}


/**
 * Mutateur pour color.
 *
 * \param color Couleur du faisceau.
 *
 * \sa CRope::getColor
 ******************************/

void CRope::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Mutateur pour subdiv.
 *
 * \param subdiv Nombre de subdivisions du cable (inférieur à 32).
 *
 * \sa CRope::getSubdiv
 ******************************/

void CRope::setSubdiv(unsigned short subdiv)
{
    if (subdiv < 32)
        m_subdiv = subdiv;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CRope::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}

} // Namespace Ted

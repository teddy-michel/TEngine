/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CLocalSound.cpp
 * \date 05/02/2010 Création de la classe CLocalSound.
 * \date 10/07/2010 Ajout du nom du fichier contenant le son à jouer.
 * \date 11/07/2010 Gestion des slots et du son.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 05/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CGameApplication.hpp"
#include "Game/Entities/CLocalSound.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"
#include "Sound/CSoundEngine.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CLocalSound::CLocalSound(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_sound      (),
m_music      (),
m_is_music   (false)
{ }


/**
 * Destructeur.
 ******************************/

CLocalSound::~CLocalSound()
{
    Game::soundEngine->removeSound(&m_sound);
    Game::soundEngine->removeMusic(&m_music);
}


/**
 * Donne le volume du son.
 *
 * \return Volume sonore du son.
 *
 * \sa CLocalSound::setVolume
 ******************************/

float CLocalSound::getVolume() const
{
    return m_sound.getVolume();
}


/**
 * Donne la distance à partir de laquelle le son n'est plus audible.
 *
 * \return Distance à partir de laquelle le son n'est plus audible.
 *
 * \sa CLocalSound::setDistance
 ******************************/

float CLocalSound::getDistance() const
{
    return m_sound.getMinimaleDistance();
}


/**
 * Indique si le son est joué en boucle.
 *
 * \return Booléen.
 *
 * \sa CLocalSound::setLoop
 ******************************/

bool CLocalSound::isLoop() const
{
    return m_sound.isLoop();
}


/**
 * Donne le nom du fichier contenant le son à jouer.
 *
 * \return Nom du fichier contenant le son à jouer.
 *
 * \sa CLocalSound::setFilename
 ******************************/

CString CLocalSound::getFilename() const
{
    return m_sound.getFileName();
}


/**
 * Mutateur pour volume.
 *
 * \param volume Volume sonore du son.
 *
 * \sa CLocalSound::getVolume
 ******************************/

void CLocalSound::setVolume(float volume)
{
    m_sound.setVolume(volume);
    m_music.setVolume(volume);
}


/**
 * Mutateur pour distance.
 *
 * \param distance Distance à partir de laquelle le son n'est plus audible.
 *
 * \sa CLocalSound::getDistance
 ******************************/

void CLocalSound::setDistance(float distance)
{
    m_sound.setMinimaleDistance(distance);
    m_music.setMinimaleDistance(distance);
}


/**
 * Indique si on doit jouer le son en boucle.
 *
 * \param loop Booléen.
 *
 * \sa CLocalSound::isLoop
 ******************************/

void CLocalSound::setLoop(bool loop)
{
    m_sound.setLoop(loop);
    m_music.setLoop(loop);
}


/**
 * Modifie le nom du fichier contenant le son à jouer et charge le son.
 *
 * \param fileName Nom du fichier contenant le son à jouer.
 * \param music    Indique si le son est une musique.
 *
 * \sa CLocalSound::getFilename
 ******************************/

void CLocalSound::setFilename(const CString& fileName, bool music)
{
    m_is_music = music;

    Game::soundEngine->removeSound(&m_sound);
    Game::soundEngine->removeMusic(&m_music);

    if (m_is_music)
    {
        if (m_music.loadFromFile(fileName))
        {
            Game::soundEngine->addMusic(&m_music);
        }
    }
    else
    {
        if (m_sound.loadFromFile(fileName))
        {
            Game::soundEngine->addSound(&m_sound);
        }
    }
}


/**
 * Démarre la lecture du son.
 ******************************/

void CLocalSound::playSound()
{
    if (m_is_music)
    {
        if (m_music)
            m_music.play();
    }
    else
    {
        if (m_sound)
            m_sound.play();
    }
}


/**
 * Met la lecture du son en pause.
 ******************************/

void CLocalSound::pauseSound()
{
    if (m_is_music)
    {
        if (m_music)
            m_music.pause();
    }
    else
    {
        if (m_sound)
            m_sound.pause();
    }
}


/**
 * Stoppe la lecture du son.
 ******************************/

void CLocalSound::stopSound()
{
    if (m_is_music)
    {
        if (m_music)
            m_music.stop();
    }
    else
    {
        if (m_sound)
            m_sound.stop();
    }
}


/**
 * Méthode appelée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CLocalSound::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Appel d'un slot.
 *
 * \param slot  Nom du slot.
 * \param param Paramètres supplémentaires.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLocalSound::callSlot(const CString& slot, const CString& param)
{
    if (slot == "PlaySound")
    {
        playSound();
        return true;
    }

    if (slot == "PauseSound")
    {
        pauseSound();
        return true;
    }

    if (slot == "StopSound")
    {
        stopSound();
        return true;
    }

    if (slot == "SetVolume")
    {
        setVolume(param.toFloat());
        return true;
    }

    return IEntity::callSlot(slot, param);
}

} // Namespace Ted

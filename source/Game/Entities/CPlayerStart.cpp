/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CPlayerStart.cpp
 * \date 08/04/2009 Création de la classe CPlayerStart.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 06/12/2010 Plus de méthodes inline.
 * \date 08/12/2010 Suppression de l'attribut name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CPlayerStart.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param angle  Angle de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CPlayerStart::CPlayerStart(const CString& name, TVector3F angle, ILocalEntity * parent) :
IPointEntity (name, parent),
m_angle      (angle)
{ }


/**
 * Constructeur.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CPlayerStart::CPlayerStart(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_angle      (Origin3F)
{ }


/**
 * Définit l'angle de l'entité.
 *
 * \param angle Angle de l'entité.
 *
 * \sa CPlayerStart::getAngle
 ******************************/

void CPlayerStart::setAngle(const TVector3F& angle)
{
    m_angle = angle;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CPlayerStart::frame(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}

} // Namespace Ted

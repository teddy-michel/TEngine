/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/CSprite.cpp
 * \date 09/02/2010 Création de la classe CSprite.
 * \date 07/07/2010 Ajout des informations sur le rectangle à afficher.
 * \date 11/07/2010 Le buffer graphique est supprimé du Renderer à la destruction.
 *                  Les coordonnées de texture sont envoyées au buffer.
 * \date 15/07/2010 Correction des coordonnées de texture.
 * \date 17/07/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Suppression du paramètre name, les transformations fonctionnent.
 * \date 11/03/2011 Ajout du paramètre parent au constructeur.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/CSprite.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CSprite::CSprite(ILocalEntity * parent) :
IPointEntity (CString(), parent),
m_texture    (0),
m_impostor   (false),
m_height     (1.0f),
m_width      (1.0f),
m_buffer     (nullptr),
m_matrix     (Identity4F)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    // Indices du buffer
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    // Vertices
    std::vector<TVector3F>& vertices = m_buffer->getVertices();
    vertices.reserve(4);

    vertices.push_back(TVector3F( 1.0f, 0.0f,  1.0f ));
    vertices.push_back(TVector3F( 1.0f, 0.0f, -1.0f ));
    vertices.push_back(TVector3F(-1.0f, 0.0f, -1.0f ));
    vertices.push_back(TVector3F(-1.0f, 0.0f,  1.0f ));

    // Coordonnées de texture
    std::vector<TVector2F>& text_coord = m_buffer->getTextCoords(0);
    text_coord.reserve(4);

    text_coord.push_back(TVector2F(1.0f, 1.0f));
    text_coord.push_back(TVector2F(1.0f, 0.0f));
    text_coord.push_back(TVector2F(0.0f, 0.0f));
    text_coord.push_back(TVector2F(0.0f, 1.0f));

    m_buffer->update();
}


/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

CSprite::CSprite(const CString& name, ILocalEntity * parent) :
IPointEntity (name, parent),
m_texture    (0),
m_impostor   (false),
m_height     (1.0f),
m_width      (1.0f),
m_buffer     (nullptr),
m_matrix     (Identity4F)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    // Indices du buffer
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    indices.reserve(6);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    // Vertices
    std::vector<TVector3F>& vertices = m_buffer->getVertices();
    vertices.reserve(4);

    vertices.push_back(TVector3F( 1.0f, 0.0f,  1.0f));
    vertices.push_back(TVector3F( 1.0f, 0.0f, -1.0f));
    vertices.push_back(TVector3F(-1.0f, 0.0f, -1.0f));
    vertices.push_back(TVector3F(-1.0f, 0.0f,  1.0f));

    // Coordonnées de texture
    std::vector<TVector2F>& text_coord = m_buffer->getTextCoords(0);
    text_coord.reserve(4);

    text_coord.push_back(TVector2F(1.0f, 1.0f));
    text_coord.push_back(TVector2F(1.0f, 0.0f));
    text_coord.push_back(TVector2F(0.0f, 0.0f));
    text_coord.push_back(TVector2F(0.0f, 1.0f));

    m_buffer->update();
}


/**
 * Destructeur. Supprime le buffer graphique.
 ******************************/

CSprite::~CSprite()
{
    delete m_buffer;
}


/**
 * Modifie l'identifiant de la texture à utiliser.
 *
 * \param texture Identifiant de la texture.
 *
 * \sa CSprite::getTextureId
 ******************************/

void CSprite::setTextureId(unsigned int texture)
{
    T_ASSERT(m_buffer != nullptr);

    m_texture = texture;

    TBufferTexture txt;
    txt.texture[0] = m_texture;
    txt.nbr = 6;

    TBufferTextureVector& t = m_buffer->getTextures();
    t.resize(1);
    t[0] = txt;

    m_buffer->update();
}


/**
 * Modifie la valeur de impostor.
 *
 * \param impostor Booléen indiquant si le sprite est un impostor.
 *
 * \sa CSprite::setImpostor
 ******************************/

void CSprite::setImpostor(bool impostor)
{
    m_impostor = impostor;
}


/**
 * Modifie la hauteur du rectangle.
 *
 * \param height Hauteur du rectangle divisée par deux.
 *
 * \sa CSprite::setSize
 * \sa CSprite::getHeight
 ******************************/

void CSprite::setHeight(float height)
{
    m_height = (height < 1.0f ? 1.0f : height);
    updateBuffer();
}


/**
 * Modifie la largeur du rectangle.
 *
 * \param width Largeur du rectangle divisée par deux.
 *
 * \sa CSprite::setSize
 * \sa CSprite::getWidth
 ******************************/

void CSprite::setWidth(float width)
{
    m_width = (width < 1.0f ? 1.0f : width);
    updateBuffer();
}


/**
 * Modifie les dimensions du rectangle.
 *
 * \param width  Largeur du rectangle divisée par deux.
 * \param height Hauteur du rectangle divisée par deux.
 *
 * \sa CSprite::setWidth
 * \sa CSprite::setHeight
 ******************************/

void CSprite::setSize(float width, float height)
{
    m_width = (width < 1.0f ? 1.0f : width);
    m_height = (height < 1.0f ? 1.0f : height);

    updateBuffer();
}


/**
 * Méthode appellée à chaque frame.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CSprite::frame(unsigned int frameTime)
{
    T_ASSERT(m_buffer != nullptr);

    TVector3F up = TVector3F(0.0f, 0.0f, 1.0f);
    TVector3F look = m_position - Game::renderer->getCamera()->getPosition();
    TVector3F right = VectorCross(look, up);
    right.normalize();

    if (m_impostor)
    {
        up = VectorCross(right, look);
        up.normalize();
    }

    m_matrix = TMatrix4F(right.X, look.X, up.X, 0,//m_position.X ,
                         right.Y, look.Y, up.Y, 0,//m_position.Y ,
                         right.Z, look.Z, up.Z, 0,//m_position.Z ,
                            0.0f,   0.0f, 0.0f, 1.0f);

    Game::renderer->pushMatrix(MatrixModelView);

glTranslatef(m_position.X, m_position.Y, m_position.Z);

    Game::renderer->multMatrix(MatrixModelView, m_matrix);

    m_buffer->draw();

    ILocalEntity::frame(frameTime);

    Game::renderer->popMatrix(MatrixModelView);
}


/**
 * Met à jour les coordonnées des sommets dans le buffer graphique.
 ******************************/

void CSprite::updateBuffer()
{
    T_ASSERT(m_buffer != nullptr);

    // Vertices
    std::vector<TVector3F>& vertices = m_buffer->getVertices();
    vertices.clear();
    vertices.reserve(4);

    vertices.push_back(TVector3F( m_width, 0.0f,  m_height));
    vertices.push_back(TVector3F( m_width, 0.0f, -m_height));
    vertices.push_back(TVector3F(-m_width, 0.0f, -m_height));
    vertices.push_back(TVector3F(-m_width, 0.0f,  m_height));

    m_buffer->update();
}

} // Namespace Ted

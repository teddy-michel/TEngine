/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IWeaponAmmo.cpp
 * \date 09/07/2010 Création de la classe IWeaponAmmo.
 * \date 18/11/2010 Ajout de la méthode getClassName.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 * \date 09/04/2011 Ajout du paramètre parent au constructeur.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IWeaponAmmo.hpp"
#include "Game/Entities/IBaseCharacter.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/Utils.hpp"
#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IWeaponAmmo::IWeaponAmmo(const CString& name, ILocalEntity * parent) :
IBaseWeapon            (name, parent),
m_nbr_ammo             (0),
m_nbr_max_ammo         (0),
m_nbr_ammo_in_clip     (0),
m_nbr_max_ammo_in_clip (0)
{ }


/**
 * Destructeur.
 ******************************/

IWeaponAmmo::~IWeaponAmmo()
{ }


/**
 * Mutateur pour nbr_munitions.
 *
 * \param nbr Nombre de munitions.
 ******************************/

void IWeaponAmmo::setNumAmmo(unsigned short nbr)
{
    m_nbr_ammo = nbr;
}


/**
 * Mutateur pour nbr_max_munitions.
 *
 * \param nbr Nombre maximal de munitions.
 ******************************/

void IWeaponAmmo::setNumMaxAmmo(unsigned short nbr)
{
    m_nbr_max_ammo = nbr;
}


/**
 * Mutateur pour nbr_munitions_in_chargeur.
 *
 * \param nbr Nombre de munitions dans le chargeur.
 ******************************/

void IWeaponAmmo::setNumAmmoInClip(unsigned short nbr)
{
    m_nbr_ammo_in_clip = nbr;
}


/**
 * Mutateur pour nbr_max_munitions_in_chargeur.
 *
 * \param nbr Nombre maximal de munitions dans le chargeur.
 ******************************/

void IWeaponAmmo::setNumMaxAmmoInClip(unsigned short nbr)
{
    m_nbr_max_ammo_in_clip = nbr;
}


/**
 * Recharge l'arme.
 ******************************/

void IWeaponAmmo::reload()
{
    if (m_nbr_ammo_in_clip < m_nbr_max_ammo_in_clip && m_nbr_ammo > 0)
    {
        // Nombre de munitions à charger
        unsigned short diff = m_nbr_max_ammo_in_clip - m_nbr_ammo_in_clip;

        if (m_nbr_ammo < diff)
        {
            diff = m_nbr_ammo;
        }

        m_nbr_ammo_in_clip += diff;
        m_nbr_ammo -= diff;
    }
}


/**
 * Tir primaire.
 ******************************/

void IWeaponAmmo::fire()
{
    // Le personnage n'est pas défini
    if (m_character == nullptr)
        return;

    TVector3F position = getPosition();
    float angleH = m_character->getAngleH();
    float angleV = m_character->getAngleV();
    float temp = std::cos(angleV);
    TVector3F direction = position + TVector3F(temp * std::cos(angleH), temp * std::sin(angleH), std::sin(angleV));

    CRay ray(position, direction);
    CRayImpact ray_impact = Game::physicEngine->getRayImpact(ray);

    if (ray_impact.entity)
    {
        TVector3F t = ray_impact.point - ray.origin;
        float distance = t.norm();
        float damage;

        if (distance < m_maxDamageDistance)
        {
            damage = m_maxDamage;
        }
        else if (distance > m_minDamageDistance)
        {
            damage = m_minDamage;
        }
        else
        {
            damage = m_minDamage + (m_maxDamage - m_minDamage) *
                         (distance - m_maxDamageDistance) / (m_minDamageDistance - m_maxDamageDistance);
        }

        float random = RandFloat(-m_randomDamage, m_randomDamage);
        damage *= random;

        ray_impact.entity->weaponImpact(damage, m_character, this);
    }
}

} // Namespace Ted

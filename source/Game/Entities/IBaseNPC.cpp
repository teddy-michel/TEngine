/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBaseNPC.cpp
 * \date 17/04/2009 Création de la classe IBaseNPC.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création de la méthode isNPC.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/Entities/IBaseNPC.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBaseNPC::IBaseNPC(const CString& name, ILocalEntity * parent) :
IBaseCharacter (name, parent)
{ }


/**
 * Destructeur.
 ******************************/

IBaseNPC::~IBaseNPC()
{ }


/**
 * Méthode appelée à chaque frame.
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame.
 ******************************/

void IBaseNPC::frame(unsigned int frameTime)
{
    IBaseAnimating::frame(frameTime);
}

} // Namespace Ted

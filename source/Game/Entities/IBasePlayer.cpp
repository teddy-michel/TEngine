/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/Entities/IBasePlayer.cpp
 * \date 11/04/2009 Création de la classe IBasePlayer.
 * \date 08/12/2010 Plus de méthodes inline, suppression du paramètre name.
 * \date 16/02/2011 Ajout des méthodes onEvent.
 * \date 20/02/2011 Gestion des évènements de la souris pour modifier la direction.
 * \date 01/03/2011 Ajout du paramètre parent au constructeur.
 * \date 02/03/2011 Création de la méthode isPlayer.
 * \date 27/03/2011 Déplacement correct du joueur.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 07/04/2011 Les entités sont identifiées par un nom à la place d'un nombre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Game/Entities/IBasePlayer.hpp"
#include "Game/Entities/IBaseWeapon.hpp"
#include "Core/Events.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/*-------------------------------*
 *   Paramètres divers           *
 *-------------------------------*/

const unsigned short TimeUnderWater = 10000; ///< Durée pendant laquelle on peut rester sous l'eau avant de perdre de la vie en millisecondes.
const float HealthDecrease = 15.0f;          ///< Nombre de points de vie en moins chaque seconde si le joueur est sous l'eau.
const float PiOver180 = 0.0174532925f;       ///< Pi sur 180.
const float PlayerMouseMotionSpeed = 0.2f;   ///< Vitesse de déplacement du curseur.
const float PlayerVelocity = 0.4f;           ///< Vitesse de déplacement de la caméra.


/**
 * Constructeur par défaut.
 *
 * \param name   Nom de l'entité.
 * \param parent Pointeur sur l'entité parent.
 ******************************/

IBasePlayer::IBasePlayer(const CString& name, ILocalEntity * parent) :
IBaseCharacter           (name, parent),
IEventReceiver           (),
m_theta                  (0.0f),
m_phi                    (0.0f),
m_weapon                 (nullptr),
m_walking                (false),
m_underwater             (false),
m_time_underwater        (0),
m_time_underwater_health (0)
{ }


/**
 * Destructeur.
 ******************************/

IBasePlayer::~IBasePlayer()
{ }


/**
 * Indique si le joueur possède une certaine arme.
 * La recherche se fait sur le nom de la classe, pas sur le pointeur.
 *
 * \return Booléen.
 ******************************/

bool IBasePlayer::hasWeapon(const IBaseWeapon * weapon) const
{
    for (std::vector<IBaseWeapon *>::const_iterator it = m_weapons.begin() ; it != m_weapons.end() ; ++it)
    {
        if ((*it)->getClassName() == weapon->getClassName())
        {
            return true;
        }
    }

    return false;
}


/**
 * Ajoute une arme au joueur.
 *
 * \todo Récupérer les munitions de l'arme.
 *
 * \param weapon Pointeur sur l'arme à ajouter.
 ******************************/

void IBasePlayer::addWeapon(IBaseWeapon * weapon)
{
    if (weapon == nullptr)
        return;

    // Le joueur possède déjà cette arme, on récupère les munitions.
    if (hasWeapon(weapon))
    {
        //...

        return;
    }

    m_weapons.push_back(weapon);
}


/**
 * Enlève une arme au joueur.
 *
 * \param weapon Pointeur sur l'arme à enlever.
 ******************************/

void IBasePlayer::removeWeapon(IBaseWeapon * weapon)
{
    for (std::vector<IBaseWeapon *>::iterator it = m_weapons.begin(); it != m_weapons.end(); ++it)
    {
        if (*it == weapon)
        {
            m_weapons.erase(it);
            return;
        }
    }
}


/**
 * Ajoute une arme au joueur et l'utilise.
 *
 * \param weapon Pointeur sur l'arme à ajouter.
 *
 * \sa IBasePlayer::getWeapon
 ******************************/

void IBasePlayer::setWeapon(IBaseWeapon * weapon)
{
    addWeapon(weapon);
    m_weapon = weapon;
}


/**
 * Méthode appelée à chaque frame.
 *
 * \todo Gérer le tir.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBasePlayer::frame(unsigned int frameTime)
{
    // Le joueur est sous l'eau
    if (m_underwater)
    {
        m_time_underwater += frameTime;

        // Depuis trop longtemps
        if (m_time_underwater > TimeUnderWater)
        {
            if (m_time_underwater_health > frameTime)
            {
                m_time_underwater_health -= frameTime;
            }
            else
            {
                unsigned short nbr = (frameTime - frameTime % 1000) / 1000 + 1;

                // On retire les points de vie chaque seconde
                addHealth(-HealthDecrease * nbr);

                m_time_underwater_health += 1000 - frameTime % 1000;
            }
        }
    }
    // On diminue la durée passée sous l'eau
    else if (m_time_underwater > 0)
    {
        m_time_underwater_health = 1000;

        if (m_time_underwater > frameTime)
        {
            m_time_underwater -= frameTime;
        }
        else
        {
            m_time_underwater = 0;
        }
    }

    // Le joueur marche
    if (m_walking)
    {
        m_time_walking += frameTime;
    }

/*
    // Si la touche Fire est enclenchée et que le joueur porte une arme
    if (m_weapon && TOUCHE_PRIMARY_ATTACK)
    {
        if (m_underwater && m_weapon->CanFireUnderWater()) m_weapon->PrimaryAttack();
        else if (!m_underwater) m_weapon->PrimaryAttack();
    }
*/

    IBaseAnimating::frame(frameTime);
}


/**
 * Gestion des évènements de la souris.
 *
 * \todo Tester.
 *
 * \param event Évènement.
 ******************************/

void IBasePlayer::onEvent(const CMouseEvent& event)
{
    m_theta -= event.xrel * PlayerMouseMotionSpeed;
    m_phi   -= event.yrel * PlayerMouseMotionSpeed;

    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    float phi2 = m_phi * PiOver180;
    float theta2 = m_theta * PiOver180;
    /*double r_temp =*/ std::cos(phi2);
/*
    TVector3F forward(r_temp * std::cos(theta2),
                      r_temp * std::sin(theta2),
                      std::sin(phi2));
*/
    setAngleH(theta2);
    setAngleV(phi2);
}


/**
 * Gestion des évènements du clavier.
 *
 * \todo Implémentation.
 *
 * \param event Évènement.
 ******************************/

void IBasePlayer::onEvent(const CKeyboardEvent& event)
{
    T_UNUSED_UNIMPLEMENTED(event);

    //...
}


/**
 * Déplace le joueur.
 *
 * \todo Implémenter les rotations.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IBasePlayer::animate(unsigned int frameTime)
{
    float temp = std::cos(m_angleV);
    TVector3F forward(temp * std::cos(m_angleH), temp * std::sin(m_angleH), std::sin(m_angleV));
    forward.normalize();

    // Déplacement vers l'avant
    if (CApplication::getApp()->isActionActive(ActionForward))
    {
        translate(forward * frameTime * PlayerVelocity);
    }

    // Déplacement vers l'arrière
    if (CApplication::getApp()->isActionActive(ActionBackward))
    {
        translate(-forward * frameTime * PlayerVelocity);
    }

    // Déplacement vers la gauche
    if (CApplication::getApp()->isActionActive(ActionStrafeLeft))
    {
        translate(VectorCross(TVector3F(0.0f, 0.0f, 1.0f), forward) * frameTime * PlayerVelocity);
    }

    // Déplacement vers la droite
    if (CApplication::getApp()->isActionActive(ActionStrafeRight))
    {
        translate(-VectorCross(TVector3F(0.0f, 0.0f, 1.0f), forward) * frameTime * PlayerVelocity);
    }

    // Rotation vers la gauche
    if (CApplication::getApp()->isActionActive(ActionTurnLeft))
    {
        m_theta += frameTime * PlayerMouseMotionSpeed;

        m_angleH = m_theta * PiOver180;
        m_angleV = m_phi * PiOver180;
/*
        double r_temp = std::cos(m_phi * PiOver180);

        TVector3F dforward (r_temp * std::cos(m_theta * PiOver180),
                            r_temp * std::sin(m_theta * PiOver180),
                            std::sin(m_phi * PiOver180));

        setDirection(getPosition() + dforward);
*/
    }

    // Rotation vers la droite
    if (CApplication::getApp()->isActionActive(ActionTurnRight))
    {
        m_theta -= frameTime * PlayerMouseMotionSpeed;

        m_angleH = m_theta * PiOver180;
        m_angleV = m_phi * PiOver180;
/*
        double r_temp = std::cos(m_phi * PiOver180);

        TVector3F dforward(r_temp * std::cos(m_theta * PiOver180),
                           r_temp * std::sin(m_theta * PiOver180),
                           std::sin(m_phi * PiOver180));

        setDirection(getPosition() + dforward);
*/
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CConsole.cpp
 * \date       2008 Création de la classe CConsole.
 * \date 24/06/2010 Les messages d'erreurs et d'avertissements sont loggués.
 * \date 17/07/2010 Ajout de la commande model.
 * \date 05/11/2010 Conversion de certaines lignes de texte en Latin1.
 * \date 13/11/2010 Ajout du compteur de messages.
 * \date 18/12/2010 Ajout des commandes de debug d_goto et d_where.
 * \date 13/12/2011 Le texte est stocké en UTF8.
 * \date 28/04/2012 Suppression des méthodes variadiques.
 * \date 14/10/2012 Ajout des commandes get et set pour gérer les variables.
 * \date 29/03/2013 Déplacement du module Core vers le module Game.
 * \date 04/06/2014 On peut ajouter des commandes à la console.
 * \date 08/06/2014 Ajout de la commande maps pour afficher la liste des maps.
 * \date 14/06/2014 Ajout de la commande clear.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sstream>
#include <stdio.h>

#include "Game/CGameApplication.hpp"
#include "Game/CConsole.hpp"
#include "Core/CDirectory.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"

#ifdef T_DEBUG
#  include "Core/Maths/CVector3.hpp"
#  include "Graphic/CRenderer.hpp"
#  include "Graphic/ICamera.hpp"
#endif

namespace Ted
{

const unsigned int ConsoleMaxData = 1000; ///< Nombre maximal de lignes dans la console.


#ifdef T_DEBUG

bool consoleCommandGoto(const std::list<CString>& args);
bool consoleCommandWhere(const std::list<CString>& args);


bool consoleCommandGoto(const std::list<CString>& args)
{
    if (args.size() != 3)
    {
        Game::console->sendMessageWarning("Syntaxe : d_goto X Y Z");
        return false;
    }

    std::list<CString> argsCopy = args;
    TVector3F pos;
    pos.X = argsCopy.front().toFloat(); argsCopy.pop_front();
    pos.Y = argsCopy.front().toFloat(); argsCopy.pop_front();
    pos.Z = argsCopy.front().toFloat(); argsCopy.pop_front();

    Game::renderer->getCamera()->setPosition(pos);
    return true;
}


bool consoleCommandWhere(const std::list<CString>& args)
{
    T_UNUSED(args);
    Game::console->sendMessageInfos(Game::renderer->getCamera()->getPosition().toString());
    return true;
}

#endif // T_DEBUG


bool consoleCommandMap(const std::list<CString>& args);
bool consoleCommandMaps(const std::list<CString>& args);


bool consoleCommandMap(const std::list<CString>& args)
{
    if (args.size() != 1)
    {
        Game::console->sendMessageWarning("Syntaxe : map FICHIER");
        return false;
    }

    return Game::app->loadMap(args.front());
}


bool consoleCommandMaps(const std::list<CString>& args)
{
    T_UNUSED(args);

    CDirectory mapDir(Game::app->getMapDirectory());
    std::vector<TFileInfos> files = mapDir.getFilesList();

    for (std::vector<TFileInfos>::const_iterator it = files.begin(); it != files.end(); ++it)
    {
        Game::console->sendMessageInfos(it->name);
    }

    return true;
}


/**
 * Constructeur par défaut.
 *
 * \todo Déplacer les variables vers la classe CGameApplication ?
 ******************************/

CConsole::CConsole() :
m_counter (0)
{
    m_datas.reserve(ConsoleMaxData);

    // Commandes internes
    m_commands["clear"].description = "Efface le contenu de la console.";
    m_commands["get"].description = "Affiche la valeur d'une variable.";
    m_commands["get"].help = "Syntaxe : get VARIABLE";
    m_commands["set"].description = "Modifie la valeur d'une variable.";
    m_commands["set"].help = "Syntaxe : set VARIABLE VALUE";
    m_commands["help"].description = "Affiche la liste des commandes ou des informations sur une commande.";
    m_commands["quit"].description = "Quitte l'application.";

    // Commandes prédéfinies
    addCommand("map", &consoleCommandMap, "Charge une map.", "Syntaxe : map FICHIER");
    addCommand("maps", &consoleCommandMaps, "Donne la liste des maps disponibles.");

#ifdef T_DEBUG
    // Variables
    m_vars["show_bullet"] = "0";

    // Commandes de debug
    addCommand("d_goto", &consoleCommandGoto, CString::fromUTF8("Place la caméra à une position précise."), "Syntaxe : d_goto X Y Z");
    addCommand("d_where", &consoleCommandWhere, CString::fromUTF8("Donne la position de la caméra."));
#endif // T_DEBUG

}


/**
 * Destructeur.
 ******************************/

CConsole::~CConsole()
{

}


/**
 * Donne le contenu de la console.
 *
 * \return Tableau contenant les données de la console.
 ******************************/

std::vector<TConsoleData> CConsole::getData() const
{
    return m_datas;
}


/**
 * Donne la valeur du compteur de messages.
 *
 * \return Valeur du compteur.
 ******************************/

unsigned int CConsole::getCount() const
{
    return m_counter;
}


/**
 * Efface le contenu de la console.
 ******************************/

void CConsole::clear()
{
    m_datas.clear();
}


/**
 * Ajoute une commande.
 *
 * \param cmd Commande à ajouter.
 ******************************/

void CConsole::sendCommand(const CString& cmd)
{
    std::list<CString> cmdSplit = cmd.split(' ', false);

    if (cmdSplit.empty())
    {
        return;
    }

    // Ajout de la commande à la liste
    addData(cmd, ConsoleCommand);

    CString commandName = cmdSplit.front();
    cmdSplit.pop_front();

    // Commandes prédéfinies
    if (commandName == "clear")
    {
        m_datas.clear();
        ++m_counter;
        return;
    }
    else if (commandName == "get")
    {
        commandGet(cmdSplit);
        return;
    }
    else if (commandName == "set")
    {
        commandSet(cmdSplit);
        return;
    }
    else if (commandName == "help")
    {
        commandHelp(cmdSplit);
        return;
    }
    else if (commandName == "quit")
    {
        Game::app->setInactive();
        return;
    }

    if (isCommandExist(commandName))
    {
        m_commands[commandName].callback(cmdSplit);
    }
    else
    {
        sendMessageWarning(commandName + " : Commande inconnue");
    }
}


/**
 * Ajoute un message d'information.
 *
 * \param msg Message à ajouter.
 ******************************/

void CConsole::sendMessageInfos(const CString& msg)
{
    addData(msg, ConsoleInfos);
}


/**
 * Ajoute un message d'erreur.
 *
 * \param msg Message à ajouter.
 ******************************/

void CConsole::sendMessageError(const CString& msg)
{
    addData(msg, ConsoleError);
}


/**
 * Ajoute un message d'avertissement.
 *
 * \param msg Message à ajouter.
 ******************************/

void CConsole::sendMessageWarning(const CString& msg)
{
    addData(msg, ConsoleWarning);
}


/**
 * Ajoute un message à la console.
 *
 * \param txt Message à ajouter.
 * \param type Type de message.
 ******************************/

void CConsole::addData(const CString& txt, TConsoleDataType type)
{
    ++m_counter;
    TConsoleData data = { type , txt };

    std::size_t size = m_datas.size();

    if (size >= ConsoleMaxData)
    {
        // On décale tous les messages
        for (std::size_t i = 0 ; i < size - 1 ; ++i)
        {
            m_datas[i] = m_datas[i + 1];
        }

        m_datas[size - 1] = data;
    }
    else
    {
        m_datas.push_back(data);
    }
}


bool CConsole::commandGet(const std::list<CString>& args)
{
    // Il manque le nom de la variable à afficher
    if (args.size() != 1)
    {
        sendMessageWarning("Syntaxe : get VARIABLE");
        return false;
    }

    CString varName = args.front();

    // Affichage de la variable
    if (m_vars.find(varName) != m_vars.end())
        sendMessageInfos(varName + CString(" = ") + m_vars[varName]);
    else
        sendMessageWarning(CString("Unknown variable ") + varName);

    return true;
}


bool CConsole::commandSet(const std::list<CString>& args)
{
    // Il manque le nom de la variable et la valeur
    if (args.size() != 2)
    {
        sendMessageWarning("Syntaxe : set VARIABLE VALUE");
        return false;
    }

    std::list<CString> argsCopy = args;
    CString varName = argsCopy.front();
    argsCopy.pop_front();

    CString val = argsCopy.front();
    argsCopy.pop_front();
    m_vars[varName] = val;

#ifdef T_DEBUG

    if (varName == "show_bullet")
    {
        if (val == "1")
        {
            Game::app->setShowBullet(true);
        }
        else
        {
            Game::app->setShowBullet(false);
        }
    }

#endif // T_DEBUG

    return true;
}


bool CConsole::commandHelp(const std::list<CString>& args)
{
    // Liste des commandes
    if (args.empty())
    {
        sendMessageInfos("Liste des commandes reconnues :");

        for (std::map<CString, TConsoleFunction>::const_iterator it = m_commands.begin(); it != m_commands.end(); ++it)
        {
            sendMessageInfos(CString("  %1 : %2").arg(it->first).arg(it->second.description));
        }

        return true;
    }
    // Aide sur une commande particulière
    else if (args.size() == 1)
    {
        CString cmdHelp = args.front();

        if (isCommandExist(cmdHelp))
        {
            sendMessageInfos(CString("%1 : %2").arg(cmdHelp).arg(m_commands[cmdHelp].description));

            if (!m_commands[cmdHelp].help.isEmpty())
            {
                sendMessageInfos(CString("%3").arg(m_commands[cmdHelp].help));
            }
        }

        return true;
    }

    return false;
}


void CConsole::addCommand(const CString& name, ConsoleFunction callback, const CString& description, const CString& help)
{
    if (callback == nullptr)
    {
        return;
    }

    if (isCommandExist(name))
    {
        CApplication::getApp()->log(CString::fromUTF8("La commande %1 existe déjà").arg(name), ILogger::Warning);
        return;
    }

    m_commands[name].description = description;
    m_commands[name].help = help;
    m_commands[name].callback = callback;
}


/**
 * Recherche une commande à partir de son nom.
 *
 * \param name Nom de la commande à rechercher.
 * \return True si la commande existe, false sinon.
 ******************************/

bool CConsole::isCommandExist(const CString& name) const
{
    return (m_commands.find(name) != m_commands.end());
}


/**
 * Donne la description d'une commande.
 *
 * \param name Nom de la commande à rechercher.
 * \return Description de la commande.
 ******************************/

CString CConsole::getCommandDescription(const CString& name) const
{
    std::map<CString, TConsoleFunction>::const_iterator it = m_commands.find(name);

    if (it != m_commands.end())
    {
        return it->second.description;
    }

    return CString();
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CGameApplication.cpp
 * \date 27/03/2013 Création de la classe CGameApplication.
 * \date 04/06/2014 Correction d'un bug avec les boutons de la souris.
 * \date 08/06/2014 Ajout d'un répertoire pour charger les maps.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CGameApplication.hpp"
#include "Game/CConsole.hpp"
#include "Game/IMap.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/CLoggerConsole.hpp"
#include "Game/CBulletDebugDraw.hpp"
#include "Game/CModelManager.hpp"
#include "Core/CFile.hpp"
#include "Core/CLoggerFile.hpp"
#include "Core/Utils.hpp"
#include "Core/IEventReceiver.hpp"
#include "Core/Time.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/CBuffer.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CMenu.hpp"
#include "Gui/CActivity.hpp"
#include "Gui/CWindowDebug.hpp"
#include "Sound/CSoundEngine.hpp"

#include "os.h"

// DEBUG
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

namespace Game
{
    CGameApplication * app = nullptr;
    CConsole * console = nullptr;
}


/**
 * Constructeur par défaut.
 *
 * \param title Titre de l'application.
 ******************************/

CGameApplication::CGameApplication(const CString& title) :
CApplication    (title),
m_multiplayer   (false),
m_showBullet    (false),
m_gamedata      (nullptr),
m_gamedataNew   (nullptr),
m_loggerConsole (nullptr),
m_debugDraw     (nullptr),
m_debugBuffer   (nullptr)
#ifdef T_ACTIVE_DEBUG_MODE
                         ,
m_activityMenu  (nullptr),
m_activityDebug (nullptr)
#endif // T_ACTIVE_DEBUG_MODE
{
    if (Game::app)
        exit(-1);

    Game::app = this;
    Game::console = new CConsole();
    new CSoundEngine();
    new CRenderer();
    new CGuiEngine();
    new CPhysicEngine();
    new CEntityManager();
    new CModelManager();

    Game::renderer->initOpenGL();

    m_debugDraw = new CBulletDebugDraw();
    m_debugBuffer = m_debugDraw->getBuffer();
    Game::physicEngine->getWorld()->setDebugDrawer(m_debugDraw);

    m_loggerConsole = new CLoggerConsole(Game::console);
    addLogger(m_loggerConsole);
}


/**
 * Destructeur.
 ******************************/

CGameApplication::~CGameApplication()
{
    // Suppression des données de jeu
    //delete m_gamedata;

    Game::app = nullptr;

    removeLogger(m_loggerConsole);
    delete m_loggerConsole;

    delete Game::console;
    Game::console = nullptr;

    delete m_debugDraw;

    // Fermeture des différentes parties du moteur
    delete Game::modelManager;
    delete Game::entityManager;
    delete Game::physicEngine;
    delete Game::guiEngine;
    delete Game::renderer;
    delete Game::soundEngine;
}


/**
 * Indique si le jeu est de type multijoueur.
 *
 * \return Booléen.
 *
 * \sa CGameApplication::setMultiplayer
 ******************************/

bool CGameApplication::isMultiplayer() const
{
    return m_multiplayer;
}


/**
 * Mutateur pour multiplayer.
 *
 * \param multiplayer Indique si le jeu est de type multijoueur.
 *
 * \sa CGameApplication::isMultiplayer
 ******************************/

void CGameApplication::setMultiplayer(bool multiplayer)
{
    m_multiplayer = multiplayer;
}


/**
 * Affiche ou masque les éléments de debug de Bullet.
 *
 * \param show Indique si on doit afficher ou masquer le debug.
 ******************************/

void CGameApplication::setShowBullet(bool show)
{
    m_showBullet = show;
}


/**
 * Donne le gestionnaire de données de jeu lié à l'application.
 *
 * \return Pointeur sur les données de jeu.
 ******************************/

IMap * CGameApplication::getGameData() const
{
    return m_gamedata;
}


/**
 * Change le chargeur de map à utiliser. L'ancien chargeur n'est pas supprimé.
 *
 * \param gamedata Chargeur de map.
 *
 * \sa CGameApplication::getGameData
 ******************************/

void CGameApplication::setGameData(IMap * gamedata)
{
    m_gamedata = gamedata;
}


/**
 * Modifie l'état de l'application.
 *
 * \param state État de l'application.
 ******************************/
/*
void CGameApplication::setState(TAppState state)
{
    // On ne peut passer à l'état GAME si le jeu est lancé
    if (state == AppGame && m_inGame)
    {
        m_state = AppGame;
        return;
    }

    CApplication::setState(state);
}
*/

/**
 * Boucle principale de l'application.
 ******************************/

void CGameApplication::mainLoop()
{
/*
    // Définition de la langue de l'application
    if (isArg("--lang"))
    {
        CString lang = getArg(getArgPosition("--lang") + 1);
        setLanguage(TLang::French); ...
    }
*/

    // Chargement d'une map au démarrage
    if (isArg("--map"))
    {
        CString map_name = getArg(getArgPosition("--map") + 1);
        loadMap(map_name);
    }

/*
    // Lancement en mode serveur
    if (isArg ("--server"))
    {
        //...
    }
*/

#ifdef T_ACTIVE_DEBUG_MODE
    m_activityDebug = new CActivity("debug");
    CWindowDebug * windowDebug = new CWindowDebug();
    m_activityDebug->addWindow(windowDebug);
    windowDebug->open();
    windowDebug->setFocus();
#endif // T_ACTIVE_DEBUG_MODE

    // Boucle de l'application
    while (eventLoop());

    closeGame();
}


/**
 * Méthode appellée à chaque frame dans la boucle principale de l'application.
 * Gère les évènements et met à jour les données du jeu et l'affichage.
 * Cette méthode peut être redéfinie dans une classe dérivée pour changer le comportement
 * de l'application.
 *
 * \return Booléen indiquant si l'application est active.
 */

bool CGameApplication::eventLoop()
{
    // Déclaration des variables
    static unsigned int time_fps = 0;
    static unsigned int nbr_img = 0;
    bool screenshot = false;
    sf::Event event;

    // Gestion du temps
    unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_time;
    m_time = time;

    // Boucle évènementielle
    while (m_window->pollEvent(event))
    {
        switch (event.type)
        {
            default:
                break;

            // Bouton Quitter (croix ou Alt-F4)
            case sf::Event::Closed:

                if (m_gameActive)
                {
                    showMouseCursor();
                    m_gameActive = false;
                }

                m_active = false;

                break;

            // Changement de taille de la fenêtre
            case sf::Event::Resized:
            {
                CResizeEvent ev;
                ev.width = event.size.width;
                ev.height = event.size.height;
                onEvent(ev);
                break;
            }

            // Texte provenant du clavier
            case sf::Event::TextEntered:
            {
                CTextEvent ev;
                ev.type = EventText;
                ev.unicode = event.text.unicode;
                onEvent(ev);
                break;
            }

            // Enfoncement d'une touche du clavier
            case sf::Event::KeyPressed:
            {
                switch (event.key.code)
                {
/*
                    // Touche F1 : Console
                    case sf::Keyboard::F1:

                        if (m_gameActive)
                        {
                            m_state = AppConsole;
                            showMouseCursor();
                        }

                        break;
*/
#ifdef T_ACTIVE_DEBUG_MODE

                    // Touche F2 : Debug
                    case sf::Keyboard::F2:

                        if (m_gameActive)
                        {
                            m_activityMenu = Game::guiEngine->getCurrentActivity();
                            Game::guiEngine->setCurrentActivity(m_activityDebug);
                            m_gameActive = false;
                            showMouseCursor();
                        }
                        else if (m_inGame && Game::guiEngine->getCurrentActivity() == m_activityDebug)
                        {
                            Game::guiEngine->setCurrentActivity(m_activityMenu);
                            m_gameActive = true;
                            showMouseCursor(false);
                        }

                        break;

#endif // T_ACTIVE_DEBUG_MODE

/* --------------------------- *
 *    Début du bloc de test    *
 * --------------------------- */

                    // DEBUG : Touche F4 : Quitter
                    case sf::Keyboard::F4:
                        m_active = false;
                        break;

                    // DEBUG : Touche F5 : Capture d'écran
                    case sf::Keyboard::F5:
                        screenshot = true;
                        Game::soundEngine->beep(2000, 100);
                        break;

/* --------------------------- *
*    Fin du bloc de test      *
* --------------------------- */

                    // Touche ÉCHAP : Quitter
                    case sf::Keyboard::Escape:

                        if (m_gameActive)
                        {
                            m_gameActive = false;
                            showMouseCursor();
                            break;
                        }
                        else if (m_inGame)
                        {
                            m_gameActive = true;
                            showMouseCursor(false);

#ifdef T_ACTIVE_DEBUG_MODE
                            if (Game::guiEngine->getCurrentActivity() == m_activityDebug)
                            {
                                Game::guiEngine->setCurrentActivity(m_activityMenu);
                            }
#endif // T_ACTIVE_DEBUG_MODE

                            break;
                        }

                        {
                            CKeyboardEvent ev;

                            ev.type  = KeyPressed;
                            ev.code  = getKeyCode(event.key.code);
                            ev.modif = NoModifier;

                            if (event.key.alt    ) ev.modif |= AltModifier;
                            if (event.key.control) ev.modif |= ControlModifier;
                            if (event.key.shift  ) ev.modif |= ShiftModifier;

                            onEvent(ev);
                        }

                        break;

                    // Autres touches : transmission de l'évènement
                    default:
                    {
                        CKeyboardEvent ev;

                        ev.type  = KeyPressed;
                        ev.code  = getKeyCode(event.key.code);
                        ev.modif = NoModifier;

                        if (event.key.alt    ) ev.modif |= AltModifier;
                        if (event.key.control) ev.modif |= ControlModifier;
                        if (event.key.shift  ) ev.modif |= ShiftModifier;

                        onEvent(ev);
                        break;
                    }
                }

                break;
            }

            // Relachement d'une touche du clavier
            case sf::Event::KeyReleased:
            {
                CKeyboardEvent ev;

                ev.type  = KeyReleased;
                ev.code  = getKeyCode(event.key.code);
                ev.modif = NoModifier;

                if (event.key.alt    ) ev.modif |= AltModifier;
                if (event.key.control) ev.modif |= ControlModifier;
                if (event.key.shift  ) ev.modif |= ShiftModifier;

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonPressed:
            {
                CMouseEvent ev;

                ev.type = ButtonPressed;
                ev.x    = event.mouseButton.x;
                ev.y    = event.mouseButton.y;
                ev.xrel = 0;
                ev.yrel = 0;

                switch (event.mouseButton.button)
                {
                    default:
                        ev.button = MouseNoButton;
                        break;

                    case sf::Mouse::Left:
                        ev.button = MouseButtonLeft;
                        break;

                    case sf::Mouse::Right:
                        ev.button = MouseButtonRight;
                        break;

                    case sf::Mouse::Middle:
                        ev.button = MouseButtonMiddle;
                        break;

                    case sf::Mouse::XButton1:
                        ev.button = MouseButtonX1;
                        break;

                    case sf::Mouse::XButton2:
                        ev.button = MouseButtonX2;
                        break;
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonReleased:
            {
                CMouseEvent ev;

                ev.type = ButtonReleased;
                ev.x    = event.mouseButton.x;
                ev.y    = event.mouseButton.y;
                ev.xrel = 0;
                ev.yrel = 0;

                switch (event.mouseButton.button)
                {
                    default:
                        ev.button = MouseNoButton;
                        break;

                    case sf::Mouse::Left:
                        ev.button = MouseButtonLeft;
                        break;

                    case sf::Mouse::Right:
                        ev.button = MouseButtonRight;
                        break;

                    case sf::Mouse::Middle:
                        ev.button = MouseButtonMiddle;
                        break;

                    case sf::Mouse::XButton1:
                        ev.button = MouseButtonX1;
                        break;

                    case sf::Mouse::XButton2:
                        ev.button = MouseButtonX2;
                        break;
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseWheelMoved:
            {
                CMouseEvent ev;

                ev.type   = MouseWheel;
                ev.x      = event.mouseWheel.x;
                ev.y      = event.mouseWheel.y;
                ev.xrel   = 0;
                ev.yrel   = 0;
                ev.button = (event.mouseWheel.delta > 0 ? MouseWheelUp : MouseWheelDown);

                onEvent(ev);
                break;
            }

            case sf::Event::MouseMoved:
            {
                CMouseEvent ev;

                ev.type   = MoveMouse;
                ev.x      = event.mouseMove.x;
                ev.y      = event.mouseMove.y;
                ev.xrel   = static_cast<int>(event.mouseMove.x) - static_cast<int>(m_cursorPos.X);
                ev.yrel   = static_cast<int>(event.mouseMove.y) - static_cast<int>(m_cursorPos.Y);
                ev.button = MouseNoButton;

                if (m_cursorVisible)
                {
                    m_cursorPos.set(ev.x, ev.y);
                }

                // On place le curseur au centre de l'écran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), *m_window);
                }

                onEvent(ev);
                break;
            }

        }
    }

    Game::renderer->beginScene();

    // Affichage de la scène
    if (m_inGame)
    {
        // Animation de la caméra si le jeu est lancé
        if (m_gameActive || m_multiplayer)
        {
            m_gameTime += frameTime;
            Game::renderer->getCamera()->animate(frameTime);

            // MAJ de la simulation physique
            Game::physicEngine->setFrameTime(frameTime);

            if (m_showBullet && m_debugBuffer)
            {
                m_debugBuffer->clear();
                Game::physicEngine->getWorld()->debugDrawWorld();
            }
        }

        // Affichage de la skybox
        Game::renderer->displayScene();

        // Mise-à-jour et affichage des entités
        m_gamedata->update((m_gameActive || m_multiplayer ? frameTime : 0));
        Game::entityManager->frame((m_gameActive || m_multiplayer ? frameTime : 0));

        // Affichage des données du moteur physique
        if (m_showBullet && m_debugBuffer)
        {
            m_debugBuffer->update();
            m_debugBuffer->draw();
        }
    }

    // Changement de map
    if (m_gamedataNew)
    {
        delete m_gamedata;
        m_gamedata = m_gamedataNew;
        m_gamedataNew = nullptr;
    }

    // Blur motion
    Game::renderer->useAccumBuffer(0.2f, 0.8f);

    // Capture d'écran
    if (m_gameActive && screenshot)
    {
        CRenderer::screenShot();
    }

    // Affichage des objets de l'interface graphique
    Game::guiEngine->beginFrame();
    Game::guiEngine->displayFrame();
    Game::guiEngine->endFrame();

    Game::renderer->endScene();

    if (m_window)
    {
        m_window->display();
    }

    // Mise-à-jour des sons
    Game::soundEngine->frame();

    // Mise-à-jour des textures et des buffers graphiques
    Game::textureManager->doPendingTasks();
    Game::renderer->doPendingTasks();

    // Calcul des FPS
    ++nbr_img;

    // À chaque seconde écoulée, on met à jour le compteur de FPS
    if (m_time - time_fps > 1000)
    {
        m_fps = (1000.0f * nbr_img / static_cast<float>(m_time - time_fps));
        nbr_img = 1;
        time_fps = m_time;

#ifdef T_ACTIVE_DEBUG_MODE
        // Enregistrement des dernières mesures de FPS
        m_fpsList.append(m_fps);
#endif // T_ACTIVE_DEBUG_MODE
    }

#ifdef T_ACTIVE_DEBUG_MODE
        // Enregistrement de la durée de chaque frame
        m_frameTimeList.append(frameTime);
#endif // T_ACTIVE_DEBUG_MODE

    return m_active;
}


/**
 * Chargement d'une map.
 *
 * \param fileName Adresse du fichier de la map.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CGameApplication::loadMap(const CString& fileName)
{
    CApplication::getApp()->log(CString("Chargement de la map %1").arg(fileName));

    m_gameActive = false;
    m_inGame = false;

    if (m_gamedata)
    {
        m_gamedataNew = m_gamedata->clone();

        if (m_gamedataNew->loadData(m_mapDirectory + fileName))
        {
            m_inGame = true;
            m_gameActive = true;

            // On réinitialise le compteur de temps pour ne pas compter
            // le chargement de la map dans la durée de la frame.
            m_time = getElapsedTime();

            showMouseCursor(false);

            return true;
        }
    }

    CApplication::getApp()->log(CString("Impossible de charger la map %1").arg(fileName), ILogger::Error);

    return false;
}


CString CGameApplication::getMapDirectory() const
{
    return m_mapDirectory;
}


void CGameApplication::setMapDirectory(const CString& dir)
{
    m_mapDirectory = dir;
}


/**
 * Ferme la partie en cours.
 ******************************/

void CGameApplication::closeGame()
{
    // On supprime les anciennes données de la map
    if (m_gamedata)
    {
        m_gamedata->unloadData();
    }

    m_gameActive = false;
    m_inGame = false;

    showMouseCursor(true);
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CGameApplication::onEvent(const CMouseEvent& event)
{
    CApplication::onEvent(event);

    // On envoie l'évènement au GuiEngine
    Game::guiEngine->onEvent(event);
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CGameApplication::onEvent(const CTextEvent& event)
{
    CApplication::onEvent(event);

    // On envoie l'évènement au GuiEngine
    if (!m_gameActive)
    {
        Game::guiEngine->onEvent(event);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CGameApplication::onEvent(const CKeyboardEvent& event)
{
    switch (event.code)
    {
        default:
            break;

        // DEBUG : Touche F8 : Mode gris
        case Key_F8:
            Game::renderer->setMode(Grey);
            break;

        // DEBUG : Touche F9 : Mode normal
        case Key_F9:
            Game::renderer->setMode(Normal);
            break;

        // DEBUG : Touche F10 : Mode textures
        case Key_F10:
            Game::renderer->setMode(Textured);
            break;

        // DEBUG : Touche F11 : Mode lightmaps
        case Key_F11:
            Game::renderer->setMode(Lightmaps);
            break;

        // DEBUG : Touche F12 : Mode wireframe
        case Key_F12:
            Game::renderer->setMode(Wireframe);
            break;
    }

    CApplication::onEvent(event);

    // On envoie l'évènement au GuiEngine
    if (!m_gameActive)
    {
        Game::guiEngine->onEvent(event);
    }
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void CGameApplication::onEvent(const CResizeEvent& event)
{
    Game::renderer->resize(event.width, event.height);

    // On envoie l'évènement au GuiEngine
    if (!m_gameActive)
    {
        Game::guiEngine->onEvent(event);
    }
}


/**
 * Modifie l'icône de la fenêtre.
 *
 * \todo Vérifier le format des pixels (RGBA / BGRA).
 *
 * \param img Image servant d'icône.
 ******************************/

void CGameApplication::setIcon(const CImage& img)
{
    T_ASSERT(m_window != nullptr);

    m_window->setIcon(img.getWidth(), img.getHeight(), reinterpret_cast<const uint8_t *>(img.getPixels()));
}


/**
 * Affiche l'application en plein écran ou dans une fenêtre.
 *
 * \param fullScreen Indique si la fenêtre doit être en plein écran.
 ******************************/

void CGameApplication::switchFullScreen(bool fullScreen)
{
    CApplication::switchFullScreen(fullScreen);

    // Initialisation de OpenGL
    Game::renderer->initOpenGL();
}


/**
 * Affiche la fenêtre et crée le contexte OpenGL.
 *
 * \param mode       Mode vidéo.
 * \param title      Titre de la fenêtre.
 * \param fullScreen Indique si la fenêtre doit être en plein écran.
 ******************************/

void CGameApplication::displayWindow(const sf::VideoMode& mode, const CString& title, bool fullScreen)
{
    CApplication::displayWindow(mode, title, fullScreen);

    // Initialisation de OpenGL
    Game::renderer->initOpenGL();

    // Initialisation du GuiEngine
    Game::guiEngine->init();
}

} // Namespace Ted

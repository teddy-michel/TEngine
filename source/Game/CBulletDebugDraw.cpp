/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CBulletDebugDraw.cpp
 * \date 12/12/2011 Création de la classe CBulletDebugDraw.
 * \date 30/03/2013 Déplacement du module Physic vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CBulletDebugDraw.hpp"
#include "Graphic/CBuffer.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CBulletDebugDraw::CBulletDebugDraw() :
m_buffer (nullptr)
{
    m_buffer = new CBuffer();
    m_buffer->setPrimitiveType(PrimLine); // Pour le moment on n'affiche que des lignes
}


/**
 * Destructeur.
 ******************************/

CBulletDebugDraw::~CBulletDebugDraw()
{
    delete m_buffer;
}


/**
 * Donne le buffer graphique utilisé pour l'affichage.
 *
 * \return Pointeur sur le buffer graphique.
 ******************************/

CBuffer * CBulletDebugDraw::getBuffer() const
{
    return m_buffer;
}


/**
 * Donne le mode d'affichage, c'est-à-dire un nombre qui définit quelles
 * données doivent être affichée.
 *
 * \return Mode d'affichage.
 *
 * \sa CBulletDebugDraw::setDebugMode
 ******************************/

int CBulletDebugDraw::getDebugMode() const
{
    return m_debug_mode;
}


/**
 * Modifie le mode d'affichage.
 *
 * \param debugMode Mode d'affichage.
 *
 * \sa CBulletDebugDraw::getDebugMode
 ******************************/

void CBulletDebugDraw::setDebugMode(int debugMode)
{
    m_debug_mode = debugMode;
}


/**
 * Affiche une ligne en couleur.
 *
 * \todo Tester.
 *
 * \param from  Point de départ de la ligne.
 * \param to    Point d'arrivée de la ligne.
 * \param color Couleur de la ligne (composantes RGB entre 0 et 1).
 ******************************/

void CBulletDebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
{
    // Ajout des vertices
    std::vector<TVector3F>& vertices = m_buffer->getVertices();

    const unsigned int index = vertices.size();

    vertices.push_back(TVector3F(from.getX(), from.getY(), from.getZ()));
    vertices.push_back(TVector3F(to.getX(), to.getY(), to.getZ()));

    // Ajout des indices
    std::vector<unsigned int>& indices = m_buffer->getIndices();

    indices.push_back(index);
    indices.push_back(index + 1);

    // Ajout des couleurs
    std::vector<float>& colors = m_buffer->getColors();

    colors.push_back(color.getX());
    colors.push_back(color.getY());
    colors.push_back(color.getZ());
    colors.push_back(1.0f);

    colors.push_back(color.getX());
    colors.push_back(color.getY());
    colors.push_back(color.getZ());
    colors.push_back(1.0f);
}


/**
 * Affiche un point de contact.
 *
 * \todo Implémentation.
 *
 * \param PointOnB  Position du point.
 * \param normalOnB Vecteur normal au point de contact.
 * \param distance  ?
 * \param lifeTime  ?
 * \param color     Couleur du point (composantes RGB entre 0 et 1).
 ******************************/

void CBulletDebugDraw::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
{
    T_UNUSED_UNIMPLEMENTED(PointOnB);
    T_UNUSED_UNIMPLEMENTED(normalOnB);
    T_UNUSED_UNIMPLEMENTED(distance);
    T_UNUSED_UNIMPLEMENTED(lifeTime);
    T_UNUSED_UNIMPLEMENTED(color);

    //...
}


/**
 * Affiche un message d'avertissement.
 *
 * \param warningString Chaine à afficher.
 ******************************/

void CBulletDebugDraw::reportErrorWarning(const char * warningString)
{
    CApplication::getApp()->log(warningString, ILogger::Warning);
}


/**
 * Affiche du texte en 3D.
 *
 * \todo Implémentation.
 *
 * \param location Position du texte.
 * \param text     Texte à afficher.
 ******************************/

void CBulletDebugDraw::draw3dText(const btVector3& location, const char * text)
{
    T_UNUSED_UNIMPLEMENTED(location);
    T_UNUSED_UNIMPLEMENTED(text);

    //...
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterPoint.cpp
 * \date 28/01/2010 Création de la classe CEmitterPoint.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CEmitterPoint.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param position  Position de l'émetteur.
 * \param direction Direction des particules.
 * \param life      Durée de vie moyenne de chaque particule.
 * \param color     Couleur moyenne des particules.
 * \param nbr       Nombre de particules.
 ******************************/

CEmitterPoint::CEmitterPoint(const TVector3F& position, const TVector3F& direction, unsigned int life, const CColor& color, unsigned int nbr) :
IEmitter   (direction, life, color, nbr),
m_position (position)
{ }


/**
 * Donne la position de l'émetteur.
 *
 * \return Position de l'émetteur.
 *
 * \sa CEmitterPoint::setPosition
 ******************************/

TVector3F CEmitterPoint::getPosition() const
{
    return m_position;
}


/**
 * Modifie la position de l'émetteur.
 *
 * \param position Position de l'émetteur.
 *
 * \sa CEmitterPoint::getPosition
 ******************************/

void CEmitterPoint::setPosition(const TVector3F& position)
{
    m_position = position;
}


/**
 * Crée une nouvelle particule.
 *
 * \return Particule créée.
 ******************************/

CParticle CEmitterPoint::createParticle()
{
    return CParticle(m_position, randSpeed(), randColor(), randLife());
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IMap.cpp
 * \date 14/04/2009 Création de la classe IGameData.
 * \date 10/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 12/07/2010 Seuls les modèles de la liste sont supprimés, et les buffer graphique ne sont plus supprimés ici.
 * \date 18/07/2010 L'ouverture du fichier se fait dans la méthode LoadData.
 * \date 28/11/2010 Ajout des méthodes AddLightmap et RemoveLightmap.
 * \date 12/03/2011 Ajout d'un attribut pour le pourcentage de chargement.
 * \date 06/10/2012 La classe est renommée en IMap.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/IMap.hpp"
#include "Game/IModel.hpp"
#include "Core/ILogger.hpp"
#include "Game/Entities/CStaticEntity.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

IMap::IMap() :
m_progress   (0.0f),
m_name       (""),
m_fileName   (""),
m_scene_node (nullptr)
{ }


/**
 * Destructeur. Suppression du graphe de scène.
 ******************************/

IMap::~IMap()
{
    // Suppression du graphe de scène.
    if (m_scene_node)
    {
        m_scene_node->deleteChildrenHierarchy();
        delete m_scene_node;
    }
}


/**
 * Modifie le nom de la map.
 *
 * \param name Nom de la map.
 *
 * \sa IMap::getName
 ******************************/

void IMap::setName(const CString& name)
{
    m_name = name;
}


/**
 * Met-à-jour le graphe de scène.
 *
 * \param frameTime Durée de la dernière frame en millisecondes.
 ******************************/

void IMap::update(unsigned int frameTime)
{
    if (m_scene_node)
        m_scene_node->frame(frameTime);
}


/**
 * Ajoute une lightmap à une face. Cette méthode doit être redéfinie dans les
 * classes dérivées.
 *
 * \param face Indice de la face à modifier.
 * \param unit Unité de texture à utiliser.
 ******************************/

void IMap::addLightmap(unsigned short face, unsigned short unit)
{
    T_UNUSED(face);
    T_UNUSED(unit);
}


/**
 * Enlève une lightmap d'une face. Cette méthode doit être redéfinie dans les
 * classes dérivées.
 *
 * \param face Indice de la face à modifier.
 * \param unit Unité de texture à utiliser.
 ******************************/

void IMap::removeLightmap(unsigned short face, unsigned short unit)
{
    T_UNUSED(face);
    T_UNUSED(unit);
}


/**
 * Supprime toutes les données chargées en mémoire pour ce niveau.
 *
 * \todo Supprimer les entités globales.
 * \todo Certaines entités doivent être conservées d'un niveau à un autre.
 * \todo Ne pas s'occuper des modèles chargés dans le moteur physique (ils sont liés au graphe de scène).
 ******************************/

void IMap::unloadData()
{
    m_progress = 0.0f;
    m_name = CString();
    m_fileName = CString();
    m_file.close();

    // Suppression des modèles
    // TODO: Inutile car les modèles sont supprimés en même temps que les entités
    // DEBUT SUPPRESSION
/*
    for (std::vector<IModel *>::iterator it = m_models.begin(); it != m_models.end(); ++it)
    {
        Game::physicEngine->removeModel(*it);
    }
*/
    // FIN SUPPRESION

    m_models.clear();

    if (m_scene_node)
    {
        m_scene_node->deleteChildrenHierarchy();
        delete m_scene_node;
        m_scene_node = nullptr;
    }
}

} // Namespace Ted

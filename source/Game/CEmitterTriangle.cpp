/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterTriangle.cpp
 * \date 28/01/2010 Création de la classe CEmitterTriangle.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CEmitterTriangle.hpp"
#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param point1    Premier sommet du triangle.
 * \param point2    Deuxième sommet du triangle.
 * \param point3    Troisième sommet du triangle.
 * \param direction Direction des particules.
 * \param life      Durée de vie moyenne de chaque particule.
 * \param color     Couleur moyenne des particules.
 * \param nbr       Nombre de particules.
 ******************************/

CEmitterTriangle::CEmitterTriangle(const TVector3F& point1, const TVector3F& point2, const TVector3F& point3, const TVector3F& direction , unsigned int life , const CColor& color , unsigned int nbr ) :
IEmitter (direction, life, color, nbr),
m_point1 (point1),
m_point2 (point2),
m_point3 (point3)
{ }


/**
 * Crée une nouvelle particule.
 *
 * \todo Tester.
 *
 * \return Particule créée.
 ******************************/

CParticle CEmitterTriangle::createParticle()
{
    // Calcul de la position initiale
    float tmp_rand1 = RandFloat(0.0f, 1.0f);
    float tmp_rand2 = RandFloat(0.0f, 1.0f);

    if (tmp_rand1 + tmp_rand2 > 1.0f)
    {
        tmp_rand1 = 1.0f - tmp_rand1;
        tmp_rand2 = 1.0f - tmp_rand2;
    }

    TVector3F position = m_point1 + tmp_rand1 * (m_point2 - m_point1) + tmp_rand2 * (m_point3 - m_point1);

    return CParticle(position, randSpeed(), randColor(), randLife());
}

} // Namespace Ted

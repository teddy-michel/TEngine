/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CLoggerConsole.cpp
 * \date       2008 Création de la classe CLoggerConsole.
 * \date 29/03/2013 Déplacement du module Core vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CLoggerConsole.hpp"
#include "Game/CConsole.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param console Pointeur sur la console à utiliser.
 ******************************/

CLoggerConsole::CLoggerConsole(CConsole * console) :
m_console  (console),
m_minLevel (0)
{
    T_ASSERT(m_console != NULL);
    m_console->sendMessageInfos(CString::fromUTF8("Démarrage du moteur : %1").arg(date()));
}


/**
 * Destructeur.
 ******************************/

CLoggerConsole::~CLoggerConsole()
{
    m_console->sendMessageInfos(CString("Fermeture du moteur : %1").arg(time()));
}


/**
 * Log un message.
 *
 * \param message Message à inscrire.
 * \param level   Niveau du message.
 ******************************/

void CLoggerConsole::write(const CString& message, int level)
{
    if (level < m_minLevel)
        return;

    switch (level)
    {
        default:
            m_console->sendMessageInfos(message);
            break;

        case Warning:
            m_console->sendMessageWarning(message);
            break;

        case Error:
            m_console->sendMessageError(message);
            break;
    }
}

} // Namespace

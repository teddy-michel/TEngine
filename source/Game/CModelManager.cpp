/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelManager.cpp
 * \date 08/10/2012 Création de la classe CModelManager.
 * \date 04/06/2014 Les modèles sont recherchées dans la liste des répertoires du gestionnaire.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CModelManager.hpp"
#include "Game/CGameApplication.hpp"
#include "Game/IModelData.hpp"


namespace Ted
{

namespace Game
{
    CModelManager * modelManager = nullptr;
}


/**
 * Construit le gestionnaire de modèles.
 ******************************/

CModelManager::CModelManager() :
IResourceManager ()
{
    if (Game::modelManager)
        exit(-1);

    Game::modelManager = this;
}


/**
 * Détruit le gestionnaire de modèles.
 ******************************/

CModelManager::~CModelManager()
{
    Game::modelManager = nullptr;
}


/**
 * Donne la taille totale occupée par les modèles.
 ******************************/

unsigned int CModelManager::getMemorySize() const
{
    unsigned int s = 0;

    for (TModelInfoVector::const_iterator it = m_models.begin(); it != m_models.end(); ++it)
    {
        s += it->model->getMemorySize();
        s += sizeof(TModelDataInfos);
    }

    return s;
}


/**
 * Retourne l'identifiant correspondant à un modèle.
 * Le modèle est chargé en mémoire si nécessaire.
 *
 * \param name Nom du modèle.
 * \return Identifiant du modèle.
 ******************************/

int CModelManager::getResourceId(const CString& name)
{
    int resourceId = 0;

    for (TModelInfoVector::const_iterator it = m_models.begin(); it != m_models.end(); ++it, ++resourceId)
    {
        if (it->name == name)
        {
            return resourceId;
        }
    }

    // Recherche du modèle dans chaque répertoire du gestionnaire
    for (std::list<CString>::const_iterator itP = m_paths.begin(); itP != m_paths.end(); ++itP)
    {
        // On parcourt la liste des chargeurs
        for (std::list<CreateInstanceType>::iterator it = m_loaders.begin(); it != m_loaders.end(); ++it)
        {
            IModelData * modelData = (*it)(*itP + name);

            if (modelData)
            {
                int id = m_models.size();
                modelData->loadFromFile(*itP + name);

                TModelDataInfos modelDataInfos;
                modelDataInfos.name  = name;
                modelDataInfos.model = modelData;

                m_models.push_back(modelDataInfos);
                return id;
            }
        }
    }

    // Aucun chargeur ne prend en charge ce type de fichier
    return -1;
}


CString CModelManager::getResourceName(int id) const
{
    if (id >= 0 && id < static_cast<int>(m_models.size()))
    {
        return m_models[id].name;
    }

    return CString();
}


IModelData * CModelManager::getModelData(int id) const
{
    if (id >= 0 && id < static_cast<int>(m_models.size()))
        return m_models[id].model;

    return nullptr;
}


/**
 * Ajoute un chargeur de modèles.
 *
 * \param modelDataLoader Chargeur à ajouter.
 ******************************/

void CModelManager::addModelDataLoader(CreateInstanceType modelDataLoader)
{
    if (std::find(m_loaders.begin(), m_loaders.end(), modelDataLoader) == m_loaders.end())
        m_loaders.push_back(modelDataLoader);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelMesh.cpp
 * \date 20/02/2010 Création de la classe CModelMesh.
 * \date 07/07/2010 Appel de la méthode Update de la classe IModel.
 * \date 08/07/2010 Déplacement du dossier Base vers le dossier Physic.
 * \date 15/07/2010 Ajout des tests d'intersection avec un rayon.
 */


#ifndef T_USE_MODELS_V2


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CModelMesh.hpp"
#include "Graphic/CBuffer.hpp"
#include "Loaders/CLoaderOBJ.hpp"
#include "Loaders/CLoaderPLY.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param file Pointeur sur le fichier contenant le modèle.
 ******************************/

CModelMesh::CModelMesh(ILoaderModel * file) :
IModel (),
m_file (file)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);
}


/**
 * Destructeur. Supprime le buffer graphique.
 ******************************/

CModelMesh::~CModelMesh()
{
    //Game::renderer->removeBuffer(m_buffer);
    delete m_buffer;
}


/**
 * Donne le chargeur de fichier utilisé par le modèle.
 *
 * \return Pointeur sur le chargeur de fichier utilisé.
 *
 * \sa CModelMesh::setLoaderModel
 ******************************/

ILoaderModel * CModelMesh::getLoaderModel() const
{
    return m_file;
}


/**
 * Change le chargeur de modèle à utiliser.
 *
 * \param loader Pointeur sur le chargeur de modèle à utiliser.
 *
 * \sa CModelMesh::getLoaderModel
 ******************************/

void CModelMesh::setLoaderModel(ILoaderModel * loader)
{
    m_file = loader;
}


/**
 * Cherche le type de contenu en fonction d'une position.
 *
 * \todo Implémentation.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType CModelMesh::getContentType(const TVector3F& position) const
{
    // Le point n'est pas dans le volume englobant
    if (getBoundingBox().getContentType(position) == Empty)
    {
        return Empty;
    }

    //...

    return Empty;
}


/**
 * Indique si un mouvement est correct par rapport au modèle.
 *
 * \todo Implémentation.
 *
 * \param from Position du point de départ.
 * \param to   Position du point d'arrivé.
 * \return Booléen.
 ******************************/

bool CModelMesh::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    //...

    return true;
}


/**
 * Met à jour le modèle.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CModelMesh::update(unsigned int frameTime)
{
    T_ASSERT(m_buffer != nullptr);

    IModel::update(frameTime); // Simulation physique

    TModelParams p;

    // On met à jour le buffer graphique
    m_file->update(m_buffer, p);
    m_buffer->update();
}


/**
 * Indique s'il y a intersection entre l'arbre BSP et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CModelMesh::hasImpact(const CRay& ray) const
{
    //...

    return false;
}


/**
 * Cherche l'intersection entre le modèle et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CModelMesh::getRayImpact(const CRay& ray) const
{
    CRayImpact impact;

    //...

    return impact;
}

} // Namespace Ted


#endif

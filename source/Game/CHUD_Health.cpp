/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHUD_Health.cpp
 * \date 29/01/2011 Création de la classe CHUD_Health.
 * \date 28/03/2013 Déplacement du module Graphic vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CHUD_Health.hpp"
#include "Game/Entities/IBasePlayer.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/CApplication.hpp"
#include "Core/Utils.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CHUD_Health::CHUD_Health() :
IHUD     (),
m_player (nullptr)
{
    setSize(150, 60);
}


/**
 * Donne le joueur dont on affiche la vie.
 *
 * \return Pointeur sur le joueur.
 *
 * \sa CHUD_Health::setPlayer
 ******************************/

IBasePlayer * CHUD_Health::getPlayer() const
{
    return m_player;
}


/**
 * Modifie le joueur dont on affiche la vie.
 *
 * \param player Pointeur sur le joueur
 *
 * \sa CHUD_Radar::getPlayer
 ******************************/

void CHUD_Health::setPlayer(IBasePlayer * player)
{
    m_player = player;
}


/**
 * Met à jour le contenu du HUD.
 *
 * \todo Implémentation.
 ******************************/

void CHUD_Health::update()
{
    T_ASSERT(m_buffer != nullptr);

    const unsigned short win_h = CApplication::getApp()->getHeight();
    setPosition(20, win_h - 70);

    m_buffer->clear();

    m_buffer->addRectangle(m_rec, CColor(255, 127, 0, 153));

    // Texte
    TTextParams params;
    params.text  = (m_player ? CString::fromNumber(m_player->getHealth()) : "0");
    params.font  = Game::fontManager->getFontId("Helvetica");
    params.size  = 40;
    params.color = CColor::Black;
    params.rec.setX(m_rec.getX() + 10);
    params.rec.setY(m_rec.getY() + 10);
    params.rec.setWidth(m_rec.getWidth() - 10);
    params.rec.setHeight(m_rec.getHeight() - 10);

    Game::fontManager->drawText(m_buffer, params);

    m_buffer->update();
}


/**
 * Affiche le HUD.
 ******************************/

void CHUD_Health::paint()
{
    m_buffer->draw();
}

} // Namespace Ted

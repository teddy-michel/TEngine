/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterSnow.cpp
 * \date 10/04/2011 Création de la classe CEmitterSnow.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CEmitterSnow.hpp"
#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

const float NumParticlesPerUnitySquare = 0.01f; ///< Nombre de particules par unité au carré.


/**
 * Constructeur.
 *
 * \param volume  Volume contenant les particules.
 * \param density Densité des particules (entre 0 et 1).
 ******************************/

CEmitterSnow::CEmitterSnow(const CBoundingBox& volume, float density) :
IEmitter (TVector3F(0.0f, 0.0f, -1.0f), 10000, CColor::White),
m_volume (volume)
{
    setDensity(density);
    setType(Point);
}


/**
 * Donne le volume dans lequel les particules sont créées.
 *
 * \return Volume contenant les particules.
 *
 * \sa CEmitterSnow::setVolume
 ******************************/

CBoundingBox CEmitterSnow::getVolume() const
{
    return m_volume;
}


/**
 * Donne la densité des particules.
 *
 * \return Densité des particules (entre 0 et 1).
 *
 * \sa CEmitterSnow::setDensity
 ******************************/

float CEmitterSnow::getDensity() const
{
    return m_density;
}


/**
 * Modifie le volume dans lequel les particules sont créées.
 *
 * \param volume Volume contenant les particules.
 *
 * \sa CEmitterSnow::getVolume
 ******************************/

void CEmitterSnow::setVolume(const CBoundingBox& volume)
{
    m_volume = volume;
    computeNumParticles();
}


/**
 * Modifie la densité de la pluie.
 *
 * \param density Densité de la pluie (entre 0 et 1).
 *
 * \sa CEmitterSnow::getDensity
 ******************************/

void CEmitterSnow::setDensity(float density)
{
         if (density < 0.0f) m_density = 0.0f;
    else if (density > 1.0f) m_density = 1.0f;
    else                     m_density = density;

    computeNumParticles();
}


/**
 * Crée une nouvelle particule.
 *
 * \return Particule créée.
 ******************************/

CParticle CEmitterSnow::createParticle()
{
    TVector3F min = m_volume.getMin();
    TVector3F max = m_volume.getMax();

    // Calcul de la position initiale
    float tmp_rand1 = RandFloat(min.X, max.X);
    float tmp_rand2 = RandFloat(min.Y, max.Y);

    return CParticle(TVector3F(tmp_rand1, tmp_rand2, max.Z), randSpeed(), randColor(), randLife());
}


/**
 * Modifie la position d'une particule.
 *
 * \todo Modifier la position des flocons de manière réaliste.
 *
 * \param p         Particule à déplacer.
 * \param frameTime Durée de la dernière frame en millisecondes.
 ******************************/

void CEmitterSnow::updateParticlePosition(CParticle& p, unsigned int frameTime)
{
    TVector3F pos = p.getPosition() + m_direction * frameTime;

    // On vérifie que la nouvelle position est correcte
    if (m_volume.getContentType(pos) == Solid)
    {
        p.setPosition(pos);
    }
    else
    {
        // Suppression de la particule
        p.setLife(0);
    }
}


/**
 * Calcule le nombre de particules à partir de la surface du rectangle et de la densité.
 ******************************/

void CEmitterSnow::computeNumParticles()
{
    TVector3F min = m_volume.getMin();
    TVector3F max = m_volume.getMax();

    setNumParticles((max.X - min.X) * (max.Y - min.Y) * m_density * NumParticlesPerUnitySquare);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelDataPLY.hpp
 * \date 08/10/2012 Création de la classe CModelDataPLY.
 * \date 03/06/2014 Ajout de la méthode getMemorySize.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Game/CModelDataPLY.hpp"
#include "Game/CConsole.hpp"
#include "Game/CGameApplication.hpp"
#include "Graphic/CBuffer.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

unsigned int CModelDataPLY::getMemorySize() const
{
    unsigned int s = 0;

    s += m_indices.size() * sizeof(unsigned int);
    s += m_vertices.size() * sizeof(TVector3F);
    s += m_colors.size() * sizeof(float);
    s += sizeof(CBoundingBox);

    return s;
}


/**
 * Met à jour le buffer graphique d'un modèle.
 *
 * \param buffer Pointeur sur le buffer à mettre à jour.
 * \param params Paramètres du modèle.
 ******************************/

void CModelDataPLY::updateBuffer(CBuffer * buffer, IModel::TModelParams& params)
{
    T_UNUSED(params); // Modèle statique

    // Buffer invalide
    if (buffer == nullptr)
        return;

    std::vector<unsigned int>& buffer_indices = buffer->getIndices();
    buffer_indices = m_indices;

    std::vector<TVector3F>& buffer_vertices = buffer->getVertices();
    buffer_vertices = m_vertices;

    std::vector<float>& buffer_color = buffer->getColors();
    buffer_color = m_colors;
}


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataPLY::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in);

    if (!file)
    {
        CApplication::getApp()->log(CString("CModelDataPLY::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;
    m_name = fileName;

    std::string texte;

    // Type du fichier
    std::getline(file, texte);

    if (texte != "ply")
    {
        CApplication::getApp()->log(CString::fromUTF8("CModelDataPLY::loadFromFile : le fichier PLY doit être au format ASCII et commencer par \"ply\""), ILogger::Error);
        return false;
    }

    // Format du fichier
    std::getline(file, texte);

    if (texte != "format ascii 1.0")
    {
        CApplication::getApp()->log(CString::fromUTF8("CModelDataPLY::loadFromFile : le fichier PLY doit être au format ASCII 1.0"), ILogger::Error);
        return false;
    }

    unsigned int nbr_vertices = 0;
    unsigned int nbr_faces = 0;
    unsigned short nbr_vertices_property = 0;
    unsigned short nbr_faces_property = 0;
    bool vertices_property = false;
    bool faces_property = false;

    // Lecture de l'en-tête
    while (texte != "end_header")
    {
        file >> texte;

        if (texte == "element")
        {
            file >> texte;

            if (texte == "vertex")
            {
                file >> nbr_vertices;
                vertices_property = true;
                faces_property = false;
            }
            else if (texte == "face")
            {
                file >> nbr_faces;
                vertices_property = false;
                faces_property = true;
            }
            else
            {
                vertices_property = false;
                faces_property = false;
            }
        }
        else if (texte == "property" && vertices_property)
        {
            if (vertices_property) ++nbr_vertices_property;
            if (faces_property)    ++nbr_faces_property;
        }
    }

    if (nbr_vertices_property < 3)
    {
        CApplication::getApp()->log(CString::fromUTF8("CModelDataPLY::loadFromFile : les vertices des fichiers PLY doivent posséder au moins trois propriétés"), ILogger::Error);
        return false;
    }

    // Points extrêmes de la bounding box
    TVector3F min = Origin3F;
    TVector3F max = Origin3F;

    // Lecture des vertices
    m_vertices.reserve(nbr_vertices);

    for (unsigned int i = 0; i < nbr_vertices; ++i)
    {
        float vertex_x, vertex_y, vertex_z, supp;
        file >> vertex_x >> vertex_y >> vertex_z;

        // Création du vertex
        TVector3F vertex(vertex_x, vertex_z, vertex_y);
        vertex *= 1000.0f;

        m_vertices.push_back(vertex);

        // Bounding box
        if (vertex.X < min.X) min.X = vertex.X;
        else if (vertex.X > max.X) max.X = vertex.X;

        if (vertex.Y < min.Y) min.Y = vertex.Y;
        else if (vertex.Y > max.Y) max.Y = vertex.Y;

        if (vertex.Z < min.Z) min.Z = vertex.Z;
        else if (vertex.Z > max.Z) max.Z = vertex.Z;

        // Propriétés supplémentaires
        for (unsigned int j = 3; j < nbr_vertices_property; ++j)
        {
            file >> supp;

            if (j == 3)
            {
                m_colors.push_back(supp);
                m_colors.push_back(supp);
                m_colors.push_back(supp);
                m_colors.push_back(1.0f);
            }
        }
    }


    // Bounding box
    m_box.setMin(min);
    m_box.setMax(max);


    // Lecture des indices
    m_indices.reserve(nbr_faces * 3);

    for (unsigned int i = 0 ; i < nbr_faces ; ++i)
    {
        unsigned int nbr = 0;
        unsigned int index1, index2, index3;

        file >> nbr;

        // Première face
        if (nbr >= 3)
        {
            file >> index1 >> index2 >> index3;

            m_indices.push_back(index1);
            m_indices.push_back(index2);
            m_indices.push_back(index3);
        }
        // Face avec moins de 3 sommets
        else
        {
            CApplication::getApp()->log(CString::fromUTF8("CModelDataPLY::loadFromFile : le fichier PLY contient des faces ayant moins de 3 sommets"), ILogger::Error);
            continue;
        }

        // Découpage de la face en triangles
        for (unsigned int j = 3 ; j < nbr ; ++j)
        {
            index2 = index3;
            file >> index3;

            m_indices.push_back(index1);
            m_indices.push_back(index2);
            m_indices.push_back(index3);
        }
    }

    return true;
}


/**
 * Indique si le fichier contient un modèle pouvant être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CModelDataPLY::isCorrectFormat(const CString& fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    std::string texte;

    // Type du fichier
    std::getline(file, texte);

    if (!file.good() || texte != "ply")
    {
        file.close();
        return false;
    }

    // Format du fichier
    std::getline(file, texte);

    if (!file.good() || texte != "format ascii 1.0")
    {
        file.close();
        return false;
    }

    file.close();
    return true;
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
IModelData * CModelDataPLY::createInstance(const CString& fileName)
#else
CModelDataPLY * CModelDataPLY::createInstance(const CString& fileName)
#endif
{
    return (CModelDataPLY::isCorrectFormat(fileName) ? new CModelDataPLY() : nullptr);
}

} // Namespace Ted

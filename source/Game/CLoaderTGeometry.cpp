/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CLoaderTGeometry.cpp
 * \date 09/04/2012 Création de la classe CLoaderTGeometry.
 * \date 17/04/2012 Améliorations.
 * \date 18/04/2012 Lecture des coordonnées de texture.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Game/CLoaderTGeometry.hpp"
#include "Game/IModel.hpp"
#include "Graphic/CBuffer.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderTGeometry::CLoaderTGeometry() :
ILoader ()
{ }


/**
 * Chargement du fichier.
 *
 * \todo Implémentation.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderTGeometry::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        CApplication::getApp()->log(CString("CLoaderTGeometry::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // Lecture de l'en-tête
    TGeometryHeader header;
    file.read(reinterpret_cast<char *>(&header), sizeof(TGeometryHeader));

    // Vérification de l'identifiant du fichier
    if (header.ident != FileIdent)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderTGeometry::loadFromFile : l'identifiant du fichier de géométrie est incorrect (%1)").arg(header.ident), ILogger::Error);
        file.close();
        return false;
    }

    // Vérification de la version du fichier
    if (header.version != Version_1_0)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderTGeometry::loadFromFile : la version du fichier de géométrie est incorrecte (%1)").arg(header.version), ILogger::Error);
        file.close();
        return false;
    }

    m_indices.clear();
    m_vertices.clear();
    m_textures.clear();
    m_coords.clear();


    // Lectures des sommets
    std::vector<TVector3F> vertices;
    vertices.reserve(header.num_vertices);
    file.seekg(header.offset_vertices, std::ios_base::beg);

    for (unsigned int i = 0; i < header.num_vertices; ++i)
    {
        TVector3F v;
        file.read(reinterpret_cast<char *>(&v), sizeof(TVector3F));
        vertices.push_back(v);
    }

    unsigned int indice = 0;

    // Lecture des groupes
    for (unsigned int i = 0; i < header.num_groups; ++i)
    {
        file.seekg(header.offset_groups + i * sizeof(TGeometryGroup), std::ios_base::beg);
        TGeometryGroup group;

        file.read(reinterpret_cast<char *>(&group), sizeof(TGeometryGroup));
        file.seekg(group.offset_faces, std::ios_base::beg);

        // Lecture des faces du groupe
        for (unsigned int j = 0; j < group.num_faces; ++j)
        {
            TGeometryFace face;
            file.read(reinterpret_cast<char *>(&face), sizeof(TGeometryFace));

            m_vertices.push_back(vertices[face.s1]);
            m_vertices.push_back(vertices[face.s2]);
            m_vertices.push_back(vertices[face.s3]);

            m_coords.push_back(TVector2F(face.u1, face.v1));
            m_coords.push_back(TVector2F(face.u2, face.v2));
            m_coords.push_back(TVector2F(face.u3, face.v3));

            m_indices.push_back(indice++);
            m_indices.push_back(indice++);
            m_indices.push_back(indice++);

            m_textures.push_back(face.texture);
        }
    }

    file.close();

    return true;
}


/**
 * Récupère le modèle contenant les données chargées depuis le fichier.
 * On définit les correspondances entre les identifiants de texture du moteur et ceux du fichier.
 *
 * \todo Gérer les coordonnées de texture.
 *
 * \param model    Pointeur sur le modèle à modifier.
 * \param textures Tableau contenant les identifiants de texture.
 ******************************/

void CLoaderTGeometry::updateModel(IModel * model, const std::map<int, TTextureId>& textures) const
{
    if (model == nullptr)
        return;

    CBuffer * buffer = model->getBuffer();

    if (buffer == nullptr)
        return;

    buffer->clear();

    const std::size_t numFaces = m_textures.size();

    std::vector<unsigned int>& indices = buffer->getIndices();
    indices = m_indices;

    std::vector<TVector3F>& vertices = buffer->getVertices();
    vertices = m_vertices;

    std::vector<TVector2F>& text_coords = buffer->getTextCoords();
    text_coords = m_coords;

    TBufferTextureVector& text = buffer->getTextures();
    text.clear();
    text.reserve(numFaces);

    for (std::size_t i = 0; i < numFaces; ++i)
    {
        TBufferTexture t;
        t.nbr = 3;

        std::map<int, TTextureId>::const_iterator it = textures.find(m_textures[i]);

        if (it == textures.end())
        {
            t.texture[0] = CTextureManager::DefaultTexture;
        }
        else
        {
            t.texture[0] = it->second;
        }

        text.push_back(t);
    }

    buffer->update();
}

} // Namespace Ted

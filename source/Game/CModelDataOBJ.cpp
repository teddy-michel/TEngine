/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CModelDataOBJ.hpp
 * \date 08/10/2012 Création de la classe CModelDataOBJ.
 * \date 03/06/2014 Ajout de la méthode getMemorySize.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Game/CModelDataOBJ.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/CFileSystem.hpp"
#include "Graphic/CBuffer.hpp"


namespace Ted
{

unsigned int CModelDataOBJ::getMemorySize() const
{
    unsigned int s = 0;
    s += m_normales.size() * sizeof(TVector3F);
    s += m_vertices.size() * sizeof(TVector3F);
    s += m_materials.size() * sizeof(TOBJMaterial);

    for (std::list<TOBJGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        s += (*it)->faces.size() * sizeof(TOBJFace);
        s += sizeof(TOBJGroup);
    }

    return s;
}


/**
 * Met à jour le buffer graphique d'un modèle.
 *
 * \param buffer Pointeur sur le buffer à mettre à jour.
 * \param params Paramètres du modèle.
 ******************************/

void CModelDataOBJ::updateBuffer(CBuffer * buffer, IModel::TModelParams& params)
{
    T_UNUSED(params); // Modèle statique

    // Buffer invalide
    if (buffer == nullptr)
        return;

    unsigned int nbr_faces = 0;

    // On compte le nombre de faces de chaque groupe
    for (std::list<TOBJGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        nbr_faces += (*it)->faces.size();
    }

    std::vector<unsigned int>& indices = buffer->getIndices();
    indices.clear();
    indices.reserve(nbr_faces * 3);

    std::vector<float>& colors = buffer->getColors();
    colors.clear();
    colors.resize(m_vertices.size() * 4);

    // On parcourt la liste des groupes
    for (std::list<TOBJGroup *>::const_iterator it = m_groups.begin(); it != m_groups.end(); ++it)
    {
        TOBJMaterial * material = (*it)->material;
        CColor color = (material ? material->diffuse : CColor(204, 204, 204));
        float dest[4];
        color.toFloat(dest);

        // On parcourt la liste des faces du groupe
        for (std::vector<TOBJFace>::const_iterator it2 = (*it)->faces.begin() ; it2 != (*it)->faces.end() ; ++it2)
        {
            indices.push_back(it2->s1);
            indices.push_back(it2->s2);
            indices.push_back(it2->s3);

            /* FIXME: Un sommet peut avoir plusieurs couleurs s'il est présent
             * dans plusieurs groupes différents.
             * Il faudrait alors duppliquer le sommet.
             */
            for (int i = 0 ; i < 4 ; ++i)
            {
                colors[ it2->s1 * 4 + i ] = dest[i];
                colors[ it2->s2 * 4 + i ] = dest[i];
                colors[ it2->s3 * 4 + i ] = dest[i];
            }
        }
    }

    // Copie du tableau des sommets
    std::vector<TVector3F>& vertices = buffer->getVertices();
    vertices = m_vertices;
}


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataOBJ::loadFromFile(const CString& fileName)
{
    freeMemory();

    std::ifstream file(fileName.toCharArray(), std::ios::in);

    if (!file)
    {
        CApplication::getApp()->log(CString("CModelDataOBJ::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString::fromUTF8("Chargement du modèle OBJ %1").arg(fileName));
    m_fileName = fileName;
    m_name = fileName;

    unsigned int nbr_sommets = 0;
    unsigned int nbr_normales = 0;

    TOBJGroup * group = nullptr;
    TOBJMaterial * mtl = nullptr;

    std::string line;

    // On parcourt le fichier ligne par ligne
    while (std::getline(file, line))
    {
        std::istringstream iss(line);

        std::string type;
        iss >> type;

        // Vertex
        if (type == "v")
        {
            TVector3F vertex;
            iss >> vertex;

            // Inversion des composantes Y et Z
            float tmp = vertex.Y;
            vertex.Y = vertex.Z;
            vertex.Z = tmp;

            m_vertices.push_back(vertex);
            ++nbr_sommets;
        }
        // Vecteurs normaux
        else if (type == "vn")
        {
            TVector3F vertex;
            iss >> vertex;

            m_normales.push_back(vertex);
            ++nbr_normales;
        }
        // Face
        else if (type == "f")
        {
            int index1 = 1, index2 = 1, index3 = 1, index4 = 1;
            unsigned int s1 = 0, s2 = 0, s3 = 0;

            iss >> index1;

            // Si les trois indices sont présents, on passe au sommet suivant
            if (iss.peek() == '/')
            {
                iss.ignore(100, ' ');
            }

            if (index1 < 0) s1 = nbr_sommets + index1;
            else if (index1 > 0) s1 = index1 - 1;
            else continue;

            iss >> index2;

            // Si les trois indices sont présents, on passe au sommet suivant
            if (iss.peek() == '/')
            {
                iss.ignore(100, ' ');
            }

            if (index2 < 0) s2 = nbr_sommets + index2;
            else if (index1 > 0) s2 = index2 - 1;
            else continue;

            iss >> index3;

            // Si les trois indices sont présents, on passe au sommet suivant
            if (iss.peek() == '/')
            {
                iss.ignore(100, ' ');
            }

            if (index3 < 0) s3 = nbr_sommets + index3;
            else if (index3 > 0) s3 = index3 - 1;
            else continue;

            // Création du groupe
            if (!group)
            {
                group = new TOBJGroup();
                m_groups.push_back(group);
            }

            // On vérifie que les indices sont corrects pour ajouter la face
            if (s1 < nbr_sommets && s2 < nbr_sommets && s3 < nbr_sommets &&
                s1 != s2 && s1 != s3 && s2 != s3)
            {
                addFace(group, s1, s2, s3);
            }

            // Découpage de la face en triangles
            while (iss >> index4)
            {
                // Si les trois indices sont présents, on passe au sommet suivant
                if (iss.peek() == '/')
                {
                    iss.ignore(100, ' ');
                }

                s2 = s3;

                if (index4 < 0) s3 = nbr_sommets + index4;
                else if (index4 > 0) s3 = index4 - 1;
                else continue;

                // On vérifie que les indices sont corrects pour ajouter la face
                if (s1 < nbr_sommets && s2 < nbr_sommets && s3 < nbr_sommets &&
                    s1 != s2 && s1 != s3 && s2 != s3)
                {
                    addFace(group, s1, s2, s3);
                }
            }
        }
        // Groupe
        else if (type == "g")
        {
            group = nullptr;
            std::string grp_name;
            iss >> grp_name;

            // On cherche si le nom est déjà utilisé
            for (std::list<TOBJGroup *>::iterator it = m_groups.begin(); it != m_groups.end(); ++it)
            {
                if ((*it)->name == grp_name.c_str())
                {
                    group = *it;
                    break;
                }
            }

            // Création du groupe
            if (!group)
            {
                group = new TOBJGroup();
                group->name = grp_name.c_str();
                if (mtl != nullptr) group->material = mtl;
                m_groups.push_back(group);
            }
        }
        // Fichier de matériaux
        else if (type == "mtllib")
        {
            std::string mtl_file;
            iss >> mtl_file;
            CString mtl_file2;

            std::ptrdiff_t pos = m_fileName.lastIndexOf(CFileSystem::separator);

            if (pos != -1)
            {
                mtl_file2 = m_fileName.subString(0, pos) + CFileSystem::separator + mtl_file.c_str();
            }
            else
            {
                mtl_file2 = mtl_file.c_str();
            }

            // Chargement du fichier MTL
            readMTL(mtl_file2);
        }
        // Matériau
        else if (type == "usemtl")
        {
            std::string mtl_name;
            iss >> mtl_name;
            mtl = nullptr;

            // On cherche si le matériau existe
            for (std::list<TOBJMaterial *>::iterator it = m_materials.begin(); it != m_materials.end(); ++it)
            {
                if ((*it)->name == mtl_name.c_str())
                {
                    mtl = *it;
                    break;
                }
            }

            // Le matériau n'existe pas
            if (mtl == nullptr)
            {
                continue;
            }

            if (group)
            {
                group->material = mtl;
            }
        }
    }

    return true;
}


/**
 * Enregistre le modèle dans un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataOBJ::saveToFile(const CString& fileName) const
{
    std::ofstream file(fileName.toCharArray(), std::ios::out);

    if (!file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CModelDataOBJ::saveToFile : impossible d'ouvrir le fichier %1 en écriture.").arg(fileName), ILogger::Error);
        return false;
    }

    // Matériaux
    if (m_materials.size() > 0)
    {
        CString mtl_file = fileName.subString(0, fileName.lastIndexOf('.')) + ".mtl";
        writeMTL(mtl_file);

        std::ptrdiff_t pos = mtl_file.lastIndexOf(CFileSystem::separator);

        if (pos != -1)
        {
            mtl_file = mtl_file.subString(pos + 1);
        }

        file << "mtllib " << mtl_file << std::endl << std::endl;
    }

    // Écritures de la liste des points
    for (std::vector<TVector3F>::const_iterator it = m_vertices.begin() ; it != m_vertices.end() ; ++it)
    {
        file << "v " << *it << std::endl;
    }

    file << "# " << m_vertices.size() << " vertices" << std::endl << std::endl;

    // Écritures de la liste des vecteurs normaux
    for (std::vector<TVector3F>::const_iterator it = m_normales.begin() ; it != m_normales.end() ; ++it)
    {
        file << "vn " << *it << std::endl;
    }

    file << "# " << m_normales.size() << " vertex normals" << std::endl << std::endl;

    // Écritures de la liste des faces de chaque groupe
    for (std::list<TOBJGroup *>::const_iterator it = m_groups.begin() ; it != m_groups.end() ; ++it)
    {
        unsigned int nbr_faces = (*it)->faces.size();

        if (nbr_faces == 0)
            continue;

        file << "g " << (*it)->name << std::endl;

        if ((*it)->material)
        {
            file << "usemtl " << (*it)->material->name << std::endl;
        }

        file << std::endl;

        for (unsigned int i = 0 ; i < nbr_faces ; ++i)
        {
            TOBJFace f = (*it)->faces[i];
            file << "f " << (f.s1 + 1) << " " << (f.s2 + 1) << " " << (f.s3 + 1) << std::endl;
        }

        file << "# " << nbr_faces << " faces" << std::endl << std::endl;
    }

    return true;
}


/**
 * Ajoute une face au modèle.
 *
 * \param group Pointeur sur le groupe contenant le face.
 * \param s1    Indice du premier sommet.
 * \param s2    Indice du deuxième sommet.
 * \param s3    Indice du troisième sommet.
 ******************************/

void CModelDataOBJ::addFace(TOBJGroup * group, unsigned int s1, unsigned int s2, unsigned int s3)
{
    // Indices identiques (face d'aire nulle)
    if (s1 == s2 || s1 == s3 || s2 == s3)
        return;

    group->faces.push_back(TOBJFace(s1, s2, s3));
}


/**
 * Lit un fichier MTL et crée les matériaux.
 *
 * \param fileName Adresse du fichier MTL.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataOBJ::readMTL(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in);

    if (!file)
    {
        CApplication::getApp()->log(CString("CModelDataOBJ::readMTL : impossible d'ouvrir le fichier %1 en lecture").arg(fileName), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString::fromUTF8("Lecture du fichier de matériaux %1").arg(fileName));

    TOBJMaterial * mtl = nullptr;
    std::string line;

    // Pour chaque ligne du fichier
    while (std::getline(file, line))
    {
        std::istringstream iss(line);

        std::string type;
        iss >> type;

        // Matériau
        if (type == "newmtl")
        {
            std::string mtl_name;
            iss >> mtl_name;

            // Création du matériau
            mtl = new TOBJMaterial(mtl_name.c_str());
            m_materials.push_back(mtl);
        }
        else if (mtl)
        {
            // Couleur ambiante
            if (type == "Ka")
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->ambient = CColor(static_cast<unsigned char>(c1 * 255.0f),
                                      static_cast<unsigned char>(c2 * 255.0f),
                                      static_cast<unsigned char>(c3 * 255.0f));
            }
            // Couleur diffuse
            else if (type == "Kd")
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->diffuse = CColor(static_cast<unsigned char>(c1 * 255.0f),
                                      static_cast<unsigned char>(c2 * 255.0f),
                                      static_cast<unsigned char>(c3 * 255.0f));
            }
            // Couleur spéculaire
            else if (type == "Ks")
            {
                float c1, c2, c3;
                iss >> c1 >> c2 >> c3;

                mtl->specular = CColor(static_cast<unsigned char>(c1 * 255.0f),
                                       static_cast<unsigned char>(c2 * 255.0f),
                                       static_cast<unsigned char>(c3 * 255.0f));
            }
            // Illumination
            else if (type == "illum")
            {
                int i;
                iss >> i;

                mtl->illumination = (i == 2);
            }
            // Transparence
            else if (type == "d" || type == "Tr")
            {
                float tr = 1.0f;
                iss >> tr;

                mtl->transparency = tr;
            }
            // Brillance
            else if (type == "Ns")
            {
                float ns = 0.0f;
                iss >> ns;

                mtl->shininess = ns;
            }
        }
    }

    return true;
}


/**
 * Écrit le fichier MTL contenant les matériaux utilisés par le maillage.
 *
 * \param fileName Adresse du fichier MTL.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CModelDataOBJ::writeMTL(const CString& fileName) const
{
    std::ofstream file(fileName.toCharArray(), std::ios::out);

    if (!file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CModelDataOBJ::writeMTL : impossible d'ouvrir le fichier %1 en écriture").arg(fileName), ILogger::Error);
        return false;
    }

    // Écritures de la liste des matériaux
    for (std::list<TOBJMaterial *>::const_iterator it = m_materials.begin(); it != m_materials.end(); ++it)
    {
        CColor color;
        float a;

        file << std::endl << "newmtl " << (*it)->name << std::endl;

        color = (*it)->ambient;

        if (color.getRed() != 102 || color.getGreen() != 102 || color.getBlue() != 102)
        {
            file << "Ka ";
            file << (static_cast<float>(color.getRed())   / 255.0f) << " ";
            file << (static_cast<float>(color.getGreen()) / 255.0f) << " ";
            file << (static_cast<float>(color.getBlue())  / 255.0f) << std::endl;
        }

        color = (*it)->diffuse;

        if (color.getRed() != 204 || color.getGreen() != 204 || color.getBlue() != 204)
        {
            file << "Kd ";
            file << (static_cast<float>(color.getRed()) / 255.0f) << " ";
            file << (static_cast<float>(color.getGreen()) / 255.0f) << " ";
            file << (static_cast<float>(color.getBlue()) / 255.0f) << std::endl;
        }

        color = (*it)->specular;

        if (color.getRed() != 76 || color.getGreen() != 76 || color.getBlue() != 76)
        {
            file << "Ks ";
            file << (static_cast<float>(color.getRed()) / 255.0f) << " ";
            file << (static_cast<float>(color.getGreen()) / 255.0f) << " ";
            file << (static_cast<float>(color.getBlue()) / 255.0f) << std::endl;
        }

        a = (*it)->transparency;
        if (a >= 0.0f && a < 1.0f) file << "Tr " << a << std::endl;

        a = (*it)->shininess;
        if (a > 0.0f) file << "Ns " << a << std::endl;

        file << "illum " << ((*it)->illumination ? 2 : 1) << std::endl;
    }

    return true;
}


/**
 * Supprime toutes les données du modèle.
 ******************************/

void CModelDataOBJ::freeMemory()
{
    // Suppression des groupes
    for (std::list<TOBJGroup *>::iterator it = m_groups.begin() ; it != m_groups.end() ; ++it)
    {
        delete *it;
    }

    // Suppression des matériaux
    for (std::list<TOBJMaterial *>::iterator it = m_materials.begin() ; it != m_materials.end() ; ++it)
    {
        delete *it;
    }

    // Suppression des anciennes données
    m_groups.clear();
    m_materials.clear();
    m_vertices.clear();
    m_normales.clear();

    m_fileName = CString();
    m_name = CString();
}


/**
 * Indique si le fichier contient un modèle pouvant être chargé.
 * Remarque : il suffit que le fichier puisse être ouvert en lecture.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CModelDataOBJ::isCorrectFormat(const CString& fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName.toCharArray(), std::ios::in);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    file.close();
    return true;
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
IModelData * CModelDataOBJ::createInstance(const CString& fileName)
#else
CModelDataOBJ * CModelDataOBJ::createInstance(const CString& fileName)
#endif
{
    return (CModelDataOBJ::isCorrectFormat(fileName) ? new CModelDataOBJ() : nullptr);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CGameDataModel.cpp
 * \date 20/02/2010 Création de la classe CGameDataModel.
 * \date 17/07/2010 Création d'une entité brush pour gérer le modèle.
 * \date 18/07/2010 L'ouverture du fichier se fait dans la méthode LoadData.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CGameDataModel.hpp"
#include "Game/IModel.hpp"
#include "Game/Entities/CEntityManager.hpp"
#include "Game/CGameApplication.hpp"
#include "Graphic/CRenderer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CGameDataModel::CGameDataModel() :
IMap ()
{ }


/**
 * Destructeur.
 ******************************/

CGameDataModel::~CGameDataModel()
{
    UnloadData();
}


/**
 * Chargement des données.
 *
 * \param fileName Adresse du fichier de la map.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CGameDataModel::LoadData(const CString& fileName)
{
    UnloadData();

    m_fileName = fileName;
    m_name = fileName;

    return true;
}


/**
 * Supprime toutes les données chargées en mémoire.
 ******************************/

void CGameDataModel::UnloadData()
{
    Game::entityManager->deleteEntities();

    // Suppression des modèles
    for (std::vector<IModel *>::iterator it = m_models.begin(); it != m_models.end(); ++it)
    {
        Game::physicEngine->removeRigidBody((*it)->getRigidBody());
    }
}


/**
 * Définit le modèle à afficher.
 *
 * \param model Pointeur sur le modèle à afficher.
 ******************************/

void CGameDataModel::setModel(IModel * model)
{
    // Suppression des anciens modèles
    for (std::vector<IModel *>::iterator it = m_models.begin(); it != m_models.end(); ++it)
    {
        Game::physicEngine->removeRigidBody((*it)->getRigidBody());
    }

    m_models.clear();
    m_models.push_back(model);

    //Game::renderer->addBuffer(model->getBuffer());
    Game::physicEngine->addRigidBody(model->getRigidBody());
}

} // Namespace Ted

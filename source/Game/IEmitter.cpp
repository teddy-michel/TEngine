/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IEmitter.cpp
 * \date       2008 Création de la classe IEmitter.
 * \date 24/06/2010 Modification de la regénération des particules.
 * \date 08/12/2010 Affichage des particules sous forme de ligne.
 * \date 30/03/2013 Déplacement du module Physic vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/IEmitter.hpp"
#include "Physic/CPhysicEngine.hpp"
#include "Core/Maths/MathsUtils.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/ICamera.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param direction Direction des particules.
 * \param life      Durée de vie moyenne de chaque particule.
 * \param color     Couleur moyenne des particules.
 * \param nbr       Nombre de particules.
 ******************************/

IEmitter::IEmitter(const TVector3F& direction, unsigned int life, const CColor& color, unsigned int nbr) :
m_type          (Billboard),
m_nbr           (nbr),
m_direction_var (0.1f),
m_vitesse_var   (0.1f),
m_life_var      (0.1f),
m_color_var     (0.1f),
m_direction     (direction),
m_life          (life),
m_color         (color),
m_regeneration  (true),
m_buffer        (nullptr)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    //Game::renderer->addBuffer(m_buffer);
    m_buffer->setPrimitiveType(PrimTriangle);

    m_particles.reserve(nbr);
}


/**
 * Destructeur. Supprime le buffer graphique.
 ******************************/

IEmitter::~IEmitter()
{
    delete m_buffer;
}


/**
 * Donne le type de particules à afficher.
 *
 * \return Type de particules.
 *
 * \sa IEmitter::setType
 ******************************/

TParticleType IEmitter::getType() const
{
    return m_type;
}


/**
 * Donne la direction des particules.
 *
 * \return Direction des particules.
 *
 * \sa IEmitter::setDirection
 ******************************/

TVector3F IEmitter::getDirection() const
{
    return m_direction;
}


/**
 * Donne la variation de la direction des particules.
 *
 * \return Variation de la direction.
 *
 * \sa IEmitter::setDirectionVar
 ******************************/

float IEmitter::getDirectionVar() const
{
    return m_direction_var;
}


/**
 * Donne la variation de la vitesse initiale.
 *
 * \return Variation de la vitesse initiale.
 *
 * \sa IEmitter::setVitesseVar
 ******************************/

float IEmitter::getVitesseVar() const
{
    return m_vitesse_var;
}


/**
 * Accesseur pour life.
 *
 * \return Durée de vie moyenne de chaque particule en millisecondes.
 *
 * \sa IEmitter::setLife
 ******************************/

unsigned int IEmitter::getLife() const
{
    return m_life;
}


/**
 * Accesseur pour life_var.
 *
 * \return Variation autour de la durée de vie moyenne.
 *
 * \sa IEmitter::setLifeVar
 ******************************/

float IEmitter::getLifeVar() const
{
    return m_life_var;
}


/**
 * Accesseur pour color.
 *
 * \return Couleur moyenne des particules.
 *
 * \sa IEmitter::setColor
 ******************************/

CColor IEmitter::getColor() const
{
    return m_color;
}


/**
 * Accesseur pour color_var.
 *
 * \return Écart avec la couleur moyenne.
 *
 * \sa IEmitter::setColorVar
 ******************************/

float IEmitter::getColorVar() const
{
    return m_color_var;
}


/**
 * Donne le nombre de particules de l'émetteur.
 *
 * \return Nombre de particules.
 *
 * \sa IEmitter::setNumParticules
 ******************************/

unsigned int IEmitter::getNumParticles() const
{
    return m_nbr;
}


/**
 * Indique si on doit regénérer les particules mortes.
 *
 * \return Booléen.
 *
 * \sa IEmitter::setRegeneration
 ******************************/

bool IEmitter::isRegeneration() const
{
    return m_regeneration;
}


/**
 * Renvoie un pointeur sur le buffer graphique.
 *
 * \return Pointeur sur le buffer.
 ******************************/

CBuffer const * IEmitter::getBuffer() const
{
    return m_buffer;
}


/**
 * Modifie le type de particules à afficher.
 *
 * \param type Type de particules.
 *
 * \sa IEmitter::getType
 ******************************/

void IEmitter::setType(TParticleType type)
{
    T_ASSERT(m_buffer != nullptr);

    if (m_type != type)
    {
        // Modification du buffer
        switch (m_type)
        {
            default:
            case Billboard: m_buffer->setPrimitiveType(PrimTriangle); break;
            case Sprite:    m_buffer->setPrimitiveType(PrimTriangle); break;
            case Point:     m_buffer->setPrimitiveType(PrimPoint);    break;
            case Line:      m_buffer->setPrimitiveType(PrimLine);     break;
        }
    }

    m_type = type;
}


/**
 * Modifie la direction.
 *
 * \param direction Direction des particules.
 *
 * \sa IEmitter::getDirection
 ******************************/

void IEmitter::setDirection(const TVector3F& direction)
{
    m_direction = direction;
}


/**
 * Modifie la variation de la vitesse.
 *
 * \param var Variation de la vitesse initiale.
 *
 * \sa IEmitter::getVitesseVar
 ******************************/

void IEmitter::setVitesseVar(float var)
{
         if (var < 0.0f) m_vitesse_var = 0.0f;
    else if (var > 1.0f) m_vitesse_var = 1.0f;
    else                 m_vitesse_var = var;
}

/**
 * Modifie la variation de la direction.
 *
 * \param var Variation de direction.
 *
 * \sa IEmitter::getDirectionVar
 ******************************/

void IEmitter::setDirectionVar(float var)
{
         if (var < 0.0f) m_direction_var = 0.0f;
    else if (var > 1.0f) m_direction_var = 1.0f;
    else                 m_direction_var = var;
}


/**
 * Modifie la durée de vie moyenne.
 *
 * \param life Durée de vie moyenne de chaque particule.
 *
 * \sa IEmitter::getLife
 ******************************/

void IEmitter::setLife(unsigned int life)
{
    m_life = life;
}


/**
 * Modifie la variation de la durée de vie.
 *
 * \param var Variation autour de la durée de vie moyenne.
 *
 * \sa IEmitter::getLifeVar
 ******************************/

void IEmitter::setLifeVar(float var)
{
         if (var < 0.0f) m_life_var = 0.0f;
    else if (var > 1.0f) m_life_var = 1.0f;
    else                 m_life_var = var;
}

/**
 * Modifie la couleur moyenne.
 *
 * \param color Couleur moyenne des particules.
 *
 * \sa IEmitter::getColor
 ******************************/

void IEmitter::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Modifie la variation de la couleur.
 *
 * \param var Écart avec la couleur moyenne.
 *
 * \sa IEmitter::getColorVar
 ******************************/

void IEmitter::setColorVar(float var)
{
         if (var < 0.0f) m_color_var = 0.0f;
    else if (var > 1.0f) m_color_var = 1.0f;
    else                 m_color_var = var;
}


/**
 * Modifie le nombre de particules.
 *
 * \param nbr Nombre de particules.
 *
 * \sa IEmitter::getNumParticules
 ******************************/

void IEmitter::setNumParticles(unsigned int nbr)
{
    // Aucun changement
    if (m_nbr == nbr)
        return;

    m_particles.resize(nbr);

    // Si les particules sont immortelles, on modifie le tableau directement
    if (m_life == 0 && m_nbr < nbr)
    {
        // On ajoute des particules
        for (unsigned int i = m_nbr; i < nbr; ++i)
        {
            m_particles[i] = createParticle();
        }
    }

    m_nbr = nbr;
}


/**
 * Change le paramètre regeneration.
 *
 * \param regeneration Indique si on doit regénérer les particules mortes.
 *
 * \sa IEmitter::getRegeneration
 ******************************/

void IEmitter::setRegeneration(bool regeneration)
{
    m_regeneration = regeneration;
}


/**
 * Met à jour les particules.
 *
 * \todo Afficher les lignes.
 * \todo Afficher les points.
 * \todo Ne plus utiliser que 4 sommets par particules au lieu de 6.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IEmitter::update(unsigned int frameTime)
{
    T_ASSERT(m_buffer != nullptr);

    unsigned int deads = 0; // Nombre de particules mortes

    // Mise-à-jour des particules
    for (std::vector<CParticle>::iterator it = m_particles.begin(); it != m_particles.end(); ++it)
    {
        it->update(frameTime);
        updateParticlePosition(*it, frameTime);

        if (it->isDead())
        {
            ++deads;

            if (m_regeneration && deads < 100)
            {
                *it = createParticle();
            }
        }
    }

/*
    // Si on doit regénérer les particules mortes
    if (m_regeneration && part < m_nbr)
    {
        // On remplace les particules supprimées (au maximum 100 particules sont créées par frame)
        for (unsigned int i = part; i < m_nbr && i < part + 100; ++i)
        {
            m_particles.push_back(createParticle());
        }
    }
*/
    float sz = 10.0f; // Facteur de taille.

    // Position de la caméra
    TVector3F camera = Game::renderer->getCamera()->getPosition();

    switch (m_type)
    {
        default:
        case Billboard:
        {
            unsigned int size_part = 4 * m_particles.size();
            unsigned int indice = 0;

            std::vector<unsigned int>& indices = m_buffer->getIndices();
            indices.clear();
            indices.reserve(size_part);

            std::vector<TVector3F>& vertices = m_buffer->getVertices();
            vertices.clear();
            vertices.reserve(size_part);

            std::vector<float>& colors = m_buffer->getColors();
            colors.clear();
            colors.reserve(size_part * 4);

            // Mise-à-jour du buffer graphique
            for (std::vector<CParticle>::iterator it = m_particles.begin(); it != m_particles.end(); ++it)
            {
                if (it->isDead())
                    continue;

                TVector3F pt = it->getPosition();

                float dest[4];
                it->getColor().toFloat(dest);

                TVector3F tmp(pt - camera);
                tmp.normalize();
                tmp = VectorCross(tmp, TVector3F(0.0f, 0.0f, sz * 1.0f));

                // Vertices
                vertices.push_back(pt + TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt - TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt - tmp);

                //vertices.push_back(pt + TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt + tmp);
                //vertices.push_back(pt - TVector3F(0.0f, 0.0f, sz * 1.0f));

                indices.push_back(indice + 0);
                indices.push_back(indice + 1);
                indices.push_back(indice + 2);

                indices.push_back(indice + 0);
                indices.push_back(indice + 3);
                indices.push_back(indice + 1);

                // Indices et couleurs
                for (int i = 0; i < 4; ++i)
                {
                    //indices.push_back(indice++);

                    colors.push_back(dest[0]);
                    colors.push_back(dest[1]);
                    colors.push_back(dest[2]);
                    colors.push_back(dest[3]);
                }
            }

            break;
        }

        case Sprite:
        {
            unsigned int size_part = 6 * m_particles.size();
            unsigned int indice = 0;

            std::vector<unsigned int>& indices = m_buffer->getIndices();
            indices.clear();
            indices.reserve(size_part);

            std::vector<TVector3F>& vertices = m_buffer->getVertices();
            vertices.clear();
            vertices.reserve(size_part);

            std::vector<float>& colors = m_buffer->getColors();
            colors.clear();
            colors.reserve(size_part * 4);

            // Mise-à-jour du buffer graphique
            for (std::vector<CParticle>::iterator it = m_particles.begin(); it != m_particles.end(); ++it)
            {
                if (it->isDead())
                    continue;

                TVector3F pt = it->getPosition();

                float dest[4];
                it->getColor().toFloat(dest);

                TVector3F tmp(pt - camera);
                tmp.normalize();
                tmp = VectorCross(tmp, TVector3F(0.0f, 0.0f, sz * 1.0f));

                // Vertices
                vertices.push_back(pt + TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt - TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt - tmp);

                vertices.push_back(pt + TVector3F(0.0f, 0.0f, sz * 1.0f));
                vertices.push_back(pt + tmp);
                vertices.push_back(pt - TVector3F(0.0f, 0.0f, sz * 1.0f));

                // Indices et couleurs
                for (int i = 0; i < 6; ++i)
                {
                    indices.push_back(indice++);

                    colors.push_back(dest[0]);
                    colors.push_back(dest[1]);
                    colors.push_back(dest[2]);
                    colors.push_back(dest[3]);
                }
            }

            break;
        }

        case Point:
        {
            unsigned int size_part = m_particles.size();
            unsigned int indice = 0;

            std::vector<unsigned int>& indices = m_buffer->getIndices();
            indices.clear();
            indices.reserve(size_part);

            std::vector<TVector3F>& vertices = m_buffer->getVertices();
            vertices.clear();
            vertices.reserve(size_part);

            std::vector<float>& colors = m_buffer->getColors();
            colors.clear();
            colors.reserve(size_part * 4);

            // Mise-à-jour du buffer graphique
            for (std::vector<CParticle>::iterator it = m_particles.begin(); it != m_particles.end(); ++it)
            {
                if (it->isDead())
                    continue;

                float dest[4];
                it->getColor().toFloat(dest);

                // Vertex
                vertices.push_back(it->getPosition());

                // Indice
                indices.push_back(indice++);

                // Couleurs
                colors.push_back(dest[0]);
                colors.push_back(dest[1]);
                colors.push_back(dest[2]);
                colors.push_back(dest[3]);
            }

            break;
        }

        case Line:
        {
            unsigned int size_part = 2 * m_particles.size();
            unsigned int indice = 0;

            std::vector<unsigned int>& indices = m_buffer->getIndices();
            indices.clear();
            indices.reserve(size_part);

            std::vector<TVector3F>& vertices = m_buffer->getVertices();
            vertices.clear();
            vertices.reserve(size_part);

            std::vector<float>& colors = m_buffer->getColors();
            colors.clear();
            colors.reserve(size_part * 4);

            // Mise-à-jour du buffer graphique
            for (std::vector<CParticle>::iterator it = m_particles.begin(); it != m_particles.end(); ++it)
            {
                if (it->isDead())
                    continue;

                float dest[4];
                it->getColor().toFloat(dest);

                // Vertex
                vertices.push_back(it->getPosition());
                vertices.push_back(it->getPosition() - it->getVitesse());

                // Indice
                indices.push_back(indice++);
                indices.push_back(indice++);

                // Couleurs
                colors.push_back(dest[0]);
                colors.push_back(dest[1]);
                colors.push_back(dest[2]);
                colors.push_back(dest[3]);

                colors.push_back(dest[0]);
                colors.push_back(dest[1]);
                colors.push_back(dest[2]);
                colors.push_back(dest[3]);
            }

            break;
        }
    }

    m_buffer->update();
}


/**
 * Regénère les particules.
 ******************************/

void IEmitter::regenere()
{
    m_particles.clear();

    // On modifie temporairement les paramètres de durée de vie pour ne pas afficher toutes les particules en même temps
    const unsigned int tmp_life = m_life;
    const float tmp_var = m_life_var;

    m_life *= 0.5f;
    m_life_var = 1.0f;

    for (unsigned int i = 0; i < m_nbr; ++i)
    {
        m_particles.push_back(createParticle());
    }

    // On remet les bonnes valeurs
    m_life = tmp_life;
    m_life_var = tmp_var;
}


/**
 * Supprime toutes les particules.
 ******************************/

void IEmitter::deleteParticles()
{
    m_particles.clear();
}


/**
 * Choisir aléatoirement une vitesse.
 *
 * \todo Modifier la direction.
 *
 * \return Vitesse d'une particule.
 ******************************/

TVector3F IEmitter::randSpeed() const
{
    TVector3F vitesse = m_direction;

    // Modification de la norme de la vitesse
    if (m_vitesse_var > 0.0f)
    {
        vitesse *= RandFloat(1.0f - m_vitesse_var, 1.0f + m_vitesse_var);
    }

    // Modification de la direction de la vitesse
    if (m_direction_var > 0.0f)
    {
        //... RandInt(0, m_direction_var * m_life)
    }

    return vitesse;
}


/**
 * Choisir aléatoirement une couleur.
 *
 * \return Couleur d'une particule.
 ******************************/

CColor IEmitter::randColor() const
{
    CColor couleur = m_color;

    // Modification de la couleur
    if (m_color_var > 0.0f)
    {
        float color[4];
        m_color.toFloat(color);

        for (int i = 0; i < 4; ++i)
        {
            if (color[i] < std::numeric_limits<float>::epsilon())
            {
                continue;
            }

            if (color[i] < 0.5f)
            {
                color[i] += (1.0f - color[i]) * RandFloat(-m_color_var, m_color_var);
            }
            else
            {
                color[i] += color[i] * RandFloat(-m_color_var, m_color_var);
            }

            if (color[i] < 0.0f)
                color[i] = 0.0f;
            else if (color[i] > 1.0f)
                color[i] = 1.0f;
        }

        couleur.set(color[0], color[1], color[2] , color[3]);
    }

    return couleur;
}


/**
 * Choisir aléatoirement une durée de vie.
 *
 * \return Durée de vie d'une particule.
 ******************************/

unsigned int IEmitter::randLife() const
{
    unsigned int life = m_life;

    // Modification de la durée de vie
    if (m_life > 0 && m_life_var > 0.0f)
    {
        life = static_cast<unsigned int>(m_life * RandFloat(1.0f - m_life_var, 1.0f + m_life_var));
    }

    return life;
}


/**
 * Modifie la position et la vitesse d'une particule en tenant compte de la gravité.
 * Cette méthode doit être rédéfinie dans les classes dérivées si on souhaite
 * obtenir un mouvement différent (particules statiques, déplacements aléatoires,
 * neige, etc.).
 *
 * \todo Dans cette classe de base, les particules ne devraient pas être déplacées.
 *
 * \param p         Particule à déplacer.
 * \param frameTime Durée de la dernière frame en millisecondes.
 ******************************/

void IEmitter::updateParticlePosition(CParticle& p, unsigned int frameTime)
{
    const float coef = 0.4f;

    TVector3F pt = p.getPosition();
    TVector3F vt = p.getVitesse();
    TVector3F g = Game::physicEngine->getGravity() * 0.001f * frameTime;

    pt += coef * 0.001f * static_cast<float>(frameTime) * (vt + coef * 0.5f * g);
    vt += g;

    p.setPosition(pt);
    p.setVitesse(vt);
}

} // Namespace Ted

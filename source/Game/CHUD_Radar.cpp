/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CHUD_Radar.cpp
 * \date 29/01/2011 Création de la classe CHUD_Radar.
 * \date 30/03/2013 Déplacement du module Graphic vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Game/CHUD_Radar.hpp"
#include "Game/Entities/ILocalEntity.hpp"
#include "Graphic/CBuffer2D.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CHUD_Radar::CHUD_Radar() :
IHUD ()
{ }


/**
 * Donne l'entité au centre du radar.
 *
 * \return Pointeur sur l'entité au centre du radar.
 *
 * \sa CHUD_Radar::setSubject
 ******************************/

ILocalEntity * CHUD_Radar::getSubject() const
{
    return m_subject;
}


/**
 * Modifie l'entité au centre du radar.
 * Si l'entité est présente dans la liste des entités suivies, elle est enlevée
 * de cette liste.
 *
 * \param entity Pointeur sur l'entité au centre du radar.
 *
 * \sa CHUD_Radar::getSubject
 ******************************/

void CHUD_Radar::setSubject(ILocalEntity * entity)
{
    m_subject = entity;

    // L'entité ne peut pas être dans la liste des entités suivies
    if (isEntity(m_subject))
    {
        removeEntity(m_subject);
    }
}


/**
 * Met à jour le contenu du HUD.
 *
 * \todo Afficher l'image du radar.
 * \todo Récupérer la position de chaque entité suivie.
 * \todo Calculer l'angle et la distance de chaque entité avec le sujet.
 * \todo Afficher les flèches.
 ******************************/

void CHUD_Radar::update()
{
    T_ASSERT(m_buffer != nullptr);

    m_buffer->clear();

    // On récupère les tableaux du buffer graphique
    std::vector<TPrimitiveType>& elements = m_buffer->getElements();
    std::vector<unsigned int>& indices = m_buffer->getIndices();
    std::vector<TVector2S>& vertices = m_buffer->getVertices();
    std::vector<float>& colors = m_buffer->getColors();
    std::vector<float>& coords = m_buffer->getTextCoords();
    std::vector<unsigned int>& textures = m_buffer->getTextures();

    elements.push_back ( PrimTriangle );
    textures.push_back ( 0 );
    elements.push_back ( PrimTriangle );
    textures.push_back ( 0 );

    indices.push_back ( 0 );
    indices.push_back ( 1 );
    indices.push_back ( 2 );
    indices.push_back ( 0 );
    indices.push_back ( 2 );
    indices.push_back ( 3 );

    for ( int i = 0 ; i < 4 ; ++i )
    {
        coords.push_back ( 0.0f );
        coords.push_back ( 0.0f );

        colors.push_back ( 0.5f );
        colors.push_back ( 0.5f );
        colors.push_back ( 0.5f );
        colors.push_back ( 0.5f );
    }

    // Sommets du rectangle
    vertices.push_back ( TVector2S ( m_rec.x , m_rec.y ) );
    vertices.push_back ( TVector2S ( m_rec.x + m_rec.w , m_rec.y ) );
    vertices.push_back ( TVector2S ( m_rec.x + m_rec.w , m_rec.y + m_rec.h ) );
    vertices.push_back ( TVector2S ( m_rec.x , m_rec.y + m_rec.h ) );


    m_buffer->update();
}


/**
 * Affiche le HUD.
 ******************************/

void CHUD_Radar::paint()
{
    m_buffer->draw();
}


/**
 * Ajoute une entité à la liste des entités suivies.
 *
 * \param entity Entité à ajouter.
 *
 * \sa CHUD_Radar::RemoveEntity
 ******************************/

void CHUD_Radar::addEntity(ILocalEntity * entity)
{
    if (entity == nullptr || entity == m_subject || isEntity(entity))
    {
        return;
    }

    m_entities.insert(m_entities.end(), entity);
}


/**
 * Enlève une entité de la liste des entités suivies.
 *
 * \param entity Entité à enlever.
 *
 * \sa CHUD_Radar::AddEntity
 ******************************/

void CHUD_Radar::removeEntity(ILocalEntity * entity)
{
    std::list<ILocalEntity *>::iterator it = std::find(m_entities.begin(), m_entities.end(), entity);

    if (it != m_entities.end())
    {
        m_entities.erase(it);
    }
}


/**
 * Indique si une entité est présente dans la liste des entités suivies.
 *
 * \param entity Entité à rechercher.
 * \return Booléen.
 ******************************/

bool CHUD_Radar::isEntity(const ILocalEntity * entity) const
{
    if (entity == nullptr)
        return false;

    // On parcourt la liste des entités
    for (std::list<ILocalEntity *>::const_iterator it = m_entities.begin(); it != m_entities.end(); ++it)
    {
        if (*it == entity)
        {
            return true;
        }
    }

    return false;
}


/**
 * Donne le nombre d'entités dans la liste des entités suivies.
 *
 * \return Nombre d'entités.
 ******************************/

unsigned int CHUD_Radar::getNumEntities() const
{
    return m_entities.size();
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CEmitterSphere.cpp
 * \date 28/01/2010 Création de la classe CEmitterSphere.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/CEmitterSphere.hpp"
#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

const float Pi = 3.1415926535f; ///< Pi.


/**
 * Constructeur.
 *
 * \param center    Centre de la sphère.
 * \param radius    Rayon de la sphère.
 * \param direction Direction des particules.
 * \param life      Durée de vie moyenne de chaque particule.
 * \param color     Couleur moyenne des particules.
 * \param nbr       Nombre de particules.
 ******************************/

CEmitterSphere::CEmitterSphere(const TVector3F& center, float radius, const TVector3F& direction, unsigned int life, const CColor& color, unsigned int nbr) :
IEmitter (direction, life, color, nbr),
m_center (center),
m_radius (0.1f)
{
    setRadius(radius);
}


/**
 * Donne la position du centre de la sphère d'émission.
 *
 * \return Centre de la sphère.
 *
 * \sa CEmitterSphere::setCenter
 ******************************/

TVector3F CEmitterSphere::getCenter() const
{
    return m_center;
}


/**
 * Donne le rayon de la sphère d'émission.
 *
 * \return Rayon de la sphère.
 *
 * \sa CEmitterSphere::setRadius
 ******************************/

float CEmitterSphere::getRadius() const
{
    return m_radius;
}


/**
 * Modifie la position du centre de la sphère d'émission.
 *
 * \param center Centre de la sphère.
 *
 * \sa CEmitterSphere::getCenter
 ******************************/

void CEmitterSphere::setCenter(const TVector3F& center)
{
    m_center = center;
    return;
}


/**
 * Modifie le rayon de la sphère d'émission.
 *
 * \param radius Rayon de la sphère.
 *
 * \sa CEmitterSphere::getRadius
 ******************************/

void CEmitterSphere::setRadius(float radius)
{
    if (radius < 0.1f) m_radius = 0.1f;
    else m_radius = radius;

    return;
}


/**
 * Crée une nouvelle particule.
 *
 * \return Particule créée.
 ******************************/

CParticle CEmitterSphere::createParticle()
{
    // Calcul de la position initiale
    TVector3F position = m_center;

    float radius = RandFloat(0.0f, m_radius);
    float angle_phi = RandFloat(0.0f, Pi);
    float angle_theta = RandFloat(0.0f, 2 * Pi);

    float sin_phi = std::sin(angle_phi);

    position += TVector3F(sin_phi * cos(angle_theta), sin_phi * sin(angle_theta), cos(angle_phi)) * radius;

    return CParticle(position, randSpeed(), randColor(), randLife());
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CMapStandard.cpp
 * \date 09/04/2012 Création de la classe CGameDataStandard.
 * \date 12/04/2012 Améliorations.
 * \date 14/04/2012 Améliorations.
 * \date 15/04/2012 Améliorations.
 * \date 17/04/2012 Améliorations.
 * \date 30/04/2012 Création d'un joueur.
 * \date 13/10/2012 La classe est renommée en CMapStandard.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <tinyxml.h>

#include "Game/CMapStandard.hpp"
#include "Game/IModel.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Exceptions.hpp"
#include "Core/Utils.hpp"
#include "Core/CLoaderZIP.hpp"
#include "Game/CLoaderTGeometry.hpp"
#include "Game/Entities/CStaticEntity.hpp"
#include "Game/Entities/CPhysicEntity.hpp"
#include "Game/Entities/CBasicPlayer.hpp"
#include "Physic/CStaticVolume.hpp"

#ifdef T_PERFORMANCE
#  include "Core/Time.hpp"
#endif


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CMapStandard::CMapStandard() :
IMap     (),
m_world  (nullptr),
m_player (nullptr)
{
    m_player = new CBasicPlayer("player", nullptr);
}


/**
 * Destructeur.
 ******************************/

CMapStandard::~CMapStandard()
{
    unloadData();
    delete m_player;
}


/**
 * Chargement des données.
 *
 * \param fileName Adresse du fichier de la map.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CMapStandard::loadData(const CString& fileName)
{
#ifdef T_PERFORMANCE
    unsigned int _perf_time = getElapsedTime();
#endif

    unloadData();


    // Ouverture du fichier
    CLoaderZIP loader;

    if (!loader.loadFromFile(fileName))
    {
        CApplication::getApp()->log(CString("CMapStandard : impossible de charger la map %1").arg(fileName), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString("Lecture de la map %1").arg(fileName));
    m_fileName = fileName;
    m_name = fileName;


    // Enregistrement des fichiers sur le disque (en attendant que le système de fichiers soit fonctionnel)
    std::vector<char> file_content;
    uint64_t file_size;

    // Fichier "geometry"
    if (!loader.getFileContent("geometry", file_content, &file_size))
    {
        CApplication::getApp()->log("CMapStandard : le fichier \"geometry\" n'existe pas dans l'archive.", ILogger::Error);
        return false;
    }

    std::ofstream file_geometry("_tmap_geometry" , std::ios::out | std::ios::binary);

    for (std::size_t i = 0; i < file_size; ++i)
    {
        file_geometry << file_content[i];
    }

    file_geometry.close();

    // Fichier "lightmaps"
    if (!loader.getFileContent("lightmaps", file_content, &file_size))
    {
        CApplication::getApp()->log("CMapStandard : le fichier \"lightmaps\" n'existe pas dans l'archive.", ILogger::Error);
        return false;
    }

    std::ofstream file_lightmaps("_tmap_lightmaps", std::ios::out | std::ios::binary);

    for (std::size_t i = 0; i < file_size; ++i)
    {
        file_lightmaps << file_content[i];
    }

    file_lightmaps.close();

    // Fichier "textures.xml"
    if (!loader.getFileContent("textures.xml", file_content, &file_size))
    {
        CApplication::getApp()->log("CMapStandard : le fichier \"textures.xml\" n'existe pas dans l'archive.", ILogger::Error);
        return false;
    }

    std::ofstream file_textures("_tmap_textures.xml", std::ios::out | std::ios::binary);

    for (std::size_t i = 0; i < file_size; ++i)
    {
        file_textures << file_content[i];
    }

    file_textures.close();

    // Fichier "volumes.xml"
    if (!loader.getFileContent("volumes.xml", file_content, &file_size))
    {
        CApplication::getApp()->log("CMapStandard : le fichier \"volumes.xml\" n'existe pas dans l'archive.", ILogger::Error);
        return false;
    }

    std::ofstream file_volumes("_tmap_volumes.xml", std::ios::out | std::ios::binary);

    for (std::size_t i = 0; i < file_size; ++i)
    {
        file_volumes << file_content[i];
    }

    file_volumes.close();

    // Fichier "entities.xml"
    if (!loader.getFileContent("entities.xml", file_content, &file_size))
    {
        CApplication::getApp()->log("CMapStandard : le fichier \"entities.xml\" n'existe pas dans l'archive.", ILogger::Error);
        return false;
    }

    std::ofstream file_entities("_tmap_entities.xml" , std::ios::out | std::ios::binary );

    for (std::size_t i = 0; i < file_size; ++i)
    {
        file_entities << file_content[i];
    }

    file_entities.close();


    // Création de la racine du graphe de scène
    m_world = new CStaticEntity("world");
    T_ASSERT(m_world != nullptr);
    m_scene_node = m_world;

    IModel * model = new IModel(nullptr);
    m_world->setModel(model);

    if (m_player)
    {
        m_player->setParent(m_scene_node);
    }

    // Chargement des données
    if (!loadTextures())
    {
        //return false;
    }

    if (!loadLightmaps())
    {
        //return false;
    }

    if (!loadGeometry())
    {
        //return false;
    }

    if (!loadVolumes())
    {
        //return false;
    }

    if (!loadEntities())
    {
        //return false;
    }

    //...

#ifdef T_PERFORMANCE
    _perf_time = getElapsedTime() - _perf_time;
    ILogger::log() << "PERFORMANCE: CMapStandard::LoadData() = " << _perf_time << " ms\n";
#endif

    return true;
}


/**
 * Supprime toutes les données chargées en mémoire.
 ******************************/

void CMapStandard::unloadData()
{
    m_textures.clear();

    // Pour ne pas supprimer le joueur
    if (m_player)
    {
        m_player->setParent(nullptr);
    }

    IMap::unloadData();
}


/**
 * Donne une nouvelle instance de cette classe en vue du chargement d'une
 * nouvelle map. Le joueur est transféré vers la nouvelle classe.
 *
 * \return Pointeur sur les nouvelles données de jeu.
 ******************************/

IMap * CMapStandard::clone()
{
    CMapStandard * gd = new CMapStandard();

    // Transfert du joueur
    delete gd->m_player;
    gd->m_player = m_player;
    m_player = nullptr;

    return gd;
}


/**
 * Charge la liste des textures de la map.
 *
 * \return Booléen indiquant si le chargement s'est bien passé.
 ******************************/

bool CMapStandard::loadTextures()
{
    // Chargement des textures
    TiXmlDocument doc("_tmap_textures.xml");

    if (!doc.LoadFile())
    {
        CApplication::getApp()->log("CMapStandard::loadTextures : impossible de lire le fichier \"textures.xml\"", ILogger::Error);
        return false;
    }

    TiXmlHandle hDoc(&doc);
    TiXmlElement * pElem;

    pElem = hDoc.FirstChildElement().Element();

    if (!pElem || std::string(pElem->Value()) != "textures")
    {
        CApplication::getApp()->log("CMapStandard::loadTextures : fichier XML incorrect", ILogger::Error);
        return false;
    }

    TiXmlHandle hRoot = TiXmlHandle(pElem);

    // Liste des volumes
    for (TiXmlNode * node = hRoot.FirstChild().Node(); node; node = node->NextSibling())
    {
        TiXmlElement * elem = node->ToElement();

        if (elem == nullptr)
        {
            continue;
        }

        // Lecture d'une texture
        if (CString(elem->Value()) == "texture")
        {
            int texture_id = 0;
            elem->Attribute("id", &texture_id);

            if (m_textures.find(texture_id) != m_textures.end())
            {
                CApplication::getApp()->log(CString::fromUTF8("CMapStandard::loadTextures : identifiant de texture déjà utilisé (%1)").arg(texture_id), ILogger::Warning);
                continue;
            }

            CString texture_file = elem->GetText();

            // Chargement de la texture
            m_textures[texture_id] = Game::textureManager->loadTexture("../../textures/" + texture_file);
        }
        else
        {
            CApplication::getApp()->log("CMapStandard::loadTextures : fichier XML incorrect", ILogger::Warning);
        }
    }

    return true;
}


/**
 * Charge les lightmaps de la map.
 *
 * \todo Implémentation.
 *
 * \return Booléen indiquant si le chargement s'est bien passé.
 ******************************/

bool CMapStandard::loadLightmaps()
{
    //...

    return false;
}


/**
 * Charge la géométrie de la map.
 *
 * \return Booléen indiquant si le chargement s'est bien passé.
 ******************************/

bool CMapStandard::loadGeometry()
{
    CLoaderTGeometry loader_geometry;

    if (!loader_geometry.loadFromFile("_tmap_geometry"))
    {
        CApplication::getApp()->log("CMapStandard : impossible de charger le fichier \"geometry\"", ILogger::Error);
        return false;
    }

    loader_geometry.updateModel(m_world->getModel(), m_textures);

    return true;
}


/**
 * Charge la liste des volumes de la map.
 *
 * \todo Ajouter les volumes convexes au modèle.
 *
 * \return Booléen indiquant si le chargement s'est bien passé.
 ******************************/

bool CMapStandard::loadVolumes()
{
    // Chargement des volumes
    TiXmlDocument doc("_tmap_volumes.xml");

    if (!doc.LoadFile())
    {
        CApplication::getApp()->log("CMapStandard::loadVolumes : impossible de lire le fichier \"volumes.xml\"", ILogger::Error);
        return false;
    }

    TiXmlHandle hDoc(&doc);
    TiXmlElement * pElem;

    pElem = hDoc.FirstChildElement().Element();

    if (!pElem || CString(pElem->Value()) != "volumes")
    {
        CApplication::getApp()->log("CMapStandard::loadVolumes : fichier XML incorrect", ILogger::Error);
        return false;
    }

    TiXmlHandle hRoot = TiXmlHandle(pElem);

    // Liste des volumes
    for (TiXmlNode * node = hRoot.FirstChild().Node(); node; node = node->NextSibling())
    {
        TiXmlElement * elem = node->ToElement();

        if (elem == nullptr)
        {
            continue;
        }

        // Lecture d'un volume convexe
        if (CString(elem->Value()) == "volume")
        {
#ifdef T_USE_MODELS_V2
            CApplication::getApp()->log(CString::fromUTF8("CMapStandard::loadVolumes : les volumes ne sont pas encore gérés"), ILogger::Error);
#else
            //int volume_id = 0;
            //elem->Attribute("id", &volume_id);

            btAlignedObjectArray<btVector3> vertices;

            // Liste des sommets
            for (TiXmlNode * node2 = node->FirstChild(); node2; node2 = node2->NextSibling())
            {
                TiXmlElement * elem2 = node2->ToElement();

                if (elem2 == nullptr)
                    continue;

                if (CString(elem2->Value()) == "vertex")
                {
                    std::string vertex = elem2->GetText();
                    TVector3F v;

                    if (fromString(vertex, v))
                    {
                        vertices.push_back(btVector3(v.X, v.Y, v.Z));
                    }
                }
            }

            if (vertices.size() >= 4)
            {
                CStaticVolume * vol = new CStaticVolume(vertices);
                m_world->getModel()->addStaticVolume(vol);
            }
            else
            {
                CApplication::getApp()->log(CString::fromUTF8("CMapStandard::loadVolumes : pas assez de sommets pour définir un volume convexe"), ILogger::Error);
            }
#endif
        }
        // Lecture d'un trimesh
        else if (CString(elem->Value()) == "trimesh")
        {
            //int volume_id = 0;
            //elem->Attribute("id", &volume_id);

            btTriangleMesh * data = new btTriangleMesh();

            // Liste des triangles
            for (TiXmlNode * node2 = node->FirstChild() ; node2 ; node2 = node2->NextSibling())
            {
                TiXmlElement * elem2 = node2->ToElement();

                if (elem2 == nullptr)
                    continue;

                if (CString(elem2->Value()) == "triangle")
                {
                    btVector3 vertices[3];
                    int num = -1;

                    // Liste des sommets
                    for (TiXmlNode * node3 = node2->FirstChild(); node3; node3 = node3->NextSibling())
                    {
                        TiXmlElement * elem3 = node3->ToElement();

                        if (elem3 == nullptr)
                        {
                            continue;
                        }

                        if (CString(elem3->Value()) == "vertex" && ++num < 3)
                        {
                            std::string vertex = elem3->GetText();
                            TVector3F v;

                            if (fromString(vertex, v))
                            {
                                vertices[num] = btVector3(v.X, v.Y, v.Z);
                            }
                        }
                    }

                    if (num >= 3)
                    {
                        CApplication::getApp()->log("CMapStandard::loadVolumes : un triangle contient plus de trois sommets", ILogger::Error);
                    }

                    data->addTriangle(vertices[0], vertices[1], vertices[2], false);
                }
                else
                {
                    CApplication::getApp()->log(CString::fromUTF8("CMapStandard::loadVolumes : élément incorrect à l'intérieur d'un trimesh"), ILogger::Error);
                }
            }

            if (data->getNumTriangles() < 1)
            {
                CApplication::getApp()->log("CMapStandard::loadVolumes : un trimesh doit contenir au moins un triangle", ILogger::Error);
                delete data;
            }
            else
            {
                btBvhTriangleMeshShape * shape = new btBvhTriangleMeshShape(data, true, true);
                m_world->getModel()->setShape(shape);
            }
        }
        else
        {
            CApplication::getApp()->log("CMapStandard::loadVolumes : type de volume inconnu", ILogger::Warning);
        }
    }

    return true;
}


/**
 * Charge la liste des entités de la map.
 *
 * \todo Implémentation.
 *
 * \return Booléen indiquant si le chargement s'est bien passé.
 ******************************/

bool CMapStandard::loadEntities()
{
    // Chargement des entités
    TiXmlDocument doc("_tmap_entities.xml");

    if (!doc.LoadFile())
    {
        CApplication::getApp()->log("CMapStandard::loadEntities : impossible de lire le fichier \"entities.xml\"", ILogger::Error);
        return false;
    }

    TiXmlHandle hDoc(&doc);
    TiXmlElement * pElem;

    pElem = hDoc.FirstChildElement().Element();

    if (!pElem || CString(pElem->Value()) != "entities")
    {
        CApplication::getApp()->log("CMapStandard::loadEntities : fichier XML incorrect", ILogger::Error);
        return false;
    }

    TiXmlHandle hRoot = TiXmlHandle(pElem);

    // Liste des entités
    for (TiXmlNode * node = hRoot.FirstChild().Node(); node; node = node->NextSibling())
    {
        TiXmlElement * elem = node->ToElement();

        if (elem == nullptr)
            continue;

        // Lecture d'une entité
        if (CString(elem->Value()) == "entity")
        {
            int entity_id = 0;
            elem->Attribute("id", &entity_id);

            //...
        }
        else
        {
            CApplication::getApp()->log("CMapStandard::loadEntities : fichier XML incorrect", ILogger::Warning);
        }
    }

    //...

    // Création d'une balle pour tester le moteur physique
    btSphereShape * shape = new btSphereShape(50);
    //btBoxShape * shape = new btBoxShape(btVector3(50.0f, 50.0f, 50.0f));

    IModel * model = new IModel(nullptr);
    model->setShape(shape);
    model->setMovable(true);
    model->setMass(10);
    model->setPosition(TVector3F(0, 100, 0 ));

    CPhysicEntity * entity = new CPhysicEntity("ball", m_world);
    entity->setModel(model);

    return false;
}

} // Namespace Ted

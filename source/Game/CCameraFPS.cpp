/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/CCameraFPS.cpp
 * \date 10/05/2009 Création de la classe CCameraFPS.
 * \date 20/07/2010 Utilisation des codes clavier internes.
 * \date 20/02/2011 Suppression d'attributs et de méthodes inutiles.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 28/03/2013 Déplacement du module Graphic vers le module Game.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <GL/glew.h>

#include "Game/CCameraFPS.hpp"
#include "Core/CApplication.hpp"
#include "Game/Entities/IBasePlayer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \todo Déplacer la liste des touches.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

CCameraFPS::CCameraFPS(const TVector3F& position, const TVector3F& direction) :
ICamera       (position, direction),
m_player      (nullptr),
m_player_eyes (Origin3F)
{ }


/**
 * Destructeur.
 ******************************/

CCameraFPS::~CCameraFPS()
{ }


/**
 * Accesseur pour player.
 *
 * \return Pointeur sur le player associé à la caméra.
 *
 * \sa CCameraFPS::setPlayer
 ******************************/

IBasePlayer * CCameraFPS::getPlayer() const
{
    return m_player;
}


/**
 * Accesseur pour player_eyes.
 *
 * \return Position des yeux du joueur par rapport à son centre de gravité.
 *
 * \sa CCameraFPS::setEyesPosition
 ******************************/

TVector3F CCameraFPS::getEyesPosition() const
{
    return m_player_eyes;
}


/**
 * Modifie la position de la caméra.
 *
 * \param position Nouvelle position.
 ******************************/

void CCameraFPS::setPosition(const TVector3F& position)
{
    if (m_player)
    {
        m_player->setPosition(position - m_player_eyes);
        m_position = m_player->getPosition() + m_player_eyes;
    }
}


/**
 * Mutateur pour player.
 *
 * \param player   Pointeur sur le player associé à la caméra.
 * \param position Position des yeux du joueur par rapport à son centre de gravité.
 *
 * \sa CCameraFPS::getPlayer
 ******************************/

void CCameraFPS::setPlayer(IBasePlayer * player, const TVector3F& position)
{
    m_player = player;
    m_player_eyes = position;
}


/**
 * Mutateur pour player_eyes.
 *
 * \param position Position des yeux du joueur par rapport à son centre de gravité.
 *
 * \sa CCameraFPS::getEyesPosition
 ******************************/

void CCameraFPS::setEyesPosition(const TVector3F& position)
{
    m_player_eyes = position;
}


/**
 * Déplace la caméra.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CCameraFPS::animate(unsigned int frameTime)
{
    if (m_player)
    {
        m_player->animate(frameTime);

        m_position = m_player->getPosition() + m_player_eyes;

        float angleH = m_player->getAngleH();
        float angleV = m_player->getAngleV();
        float temp = std::cos(angleV);

        m_direction = m_position + TVector3F(temp * std::cos(angleH), temp * std::sin(angleH), std::sin(angleV));
    }

    // Zoom
    if (CApplication::getApp()->isActionActive(ActionZoom))
    {
        if (m_zoom > frameTime)
        {
            m_zoom -= frameTime;
        }
        else if (m_zoom > 0)
        {
            m_zoom = 0;
        }
    }
    else
    {
        if (m_zoom + frameTime < ZOOM_TIME)
        {
            m_zoom += frameTime;
        }
        else if (m_zoom < ZOOM_TIME)
        {
            m_zoom = ZOOM_TIME;
        }
    }
}


/**
 * Initialise les paramètres de la caméra.
 *
 * \param position  Position de départ.
 * \param angleH    Angle d'observation horizontal.
 * \param angleV    Angle d'observation vertical.
 ******************************/

void CCameraFPS::initialize(const TVector3F& position, float angleH, float angleV)
{
    m_position = position;

    float temp = std::cos(angleV);
    m_direction = m_position + TVector3F(temp * std::cos(angleH), temp * std::sin(angleH), std::sin(angleV));

    m_zoom = ZOOM_TIME;

    if (m_player)
    {
        m_player->setPosition(m_position - m_player_eyes);
        m_player->setAngleH(angleH);
        m_player->setAngleV(angleV);
    }
}

} // Namespace Ted

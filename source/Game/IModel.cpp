/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Game/IModel.cpp
 * \date 11/10/2012 Création de la classe IModelV2.
 * \date 12/10/2012 Poursuite de l'implémentation.
 * \date 13/10/2012 Poursuite de l'implémentation.
 * \date 19/10/2013 Renommage en IModel.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Game/IModel.hpp"
#include "Game/IModelData.hpp"
#include "Graphic/CBuffer.hpp"


namespace Ted
{

/**
 * Constructeur. Le modèle est ajouté au moteur physique.
 * Un buffer graphique est crée.
 *
 * \todo Déléguer la création du buffer aux classes filles.
 *
 * \param modelData Pointeur sur les données du modèle.
 ******************************/

IModel::IModel(IModelData * modelData) :
m_body        (nullptr),
m_motionState (nullptr),
m_shape       (nullptr),
m_mass        (0.0f),
m_movable     (false),
m_entity      (nullptr),
m_buffer      (nullptr),
m_data        (modelData)
{
    m_buffer = new CBuffer();
    T_ASSERT(m_buffer != nullptr);

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(0.0f, 0.0f, 0.0f)); // Position initiale
    m_motionState = new btDefaultMotionState(transform);

    if (m_data)
    {
        CBoundingBox box = m_data->getBoundingBox(0);
        TVector3F boxExtent = (box.getMax() - box.getMin()) / 2;
        m_shape = new btBoxShape(btVector3(boxExtent.X, boxExtent.Y, boxExtent.Z));
    }
    else
    {
        m_shape = new btBoxShape(btVector3(10.0f, 10.0f, 10.0f));
    }

    btRigidBody::btRigidBodyConstructionInfo construction_info(m_mass, m_motionState, m_shape, btVector3(0.0f, 0.0f, 0.0f));
    m_body = new btRigidBody(construction_info);

    Game::physicEngine->addRigidBody(m_body);

    setSequence(0);
}


/**
 * Destructeur. Le modèle est enlevé du moteur physique.
 ******************************/

IModel::~IModel()
{
    Game::physicEngine->removeRigidBody(m_body);

    delete m_buffer;
    delete m_shape;
    delete m_motionState;
    delete m_body;
}


/**
 * Donne la position du modèle.
 *
 * \return Position du modèle.
 *
 * \sa IModel::setPosition
 ******************************/

TVector3F IModel::getPosition() const
{
    if (m_body)
    {
        const btVector3& vect = m_body->getCenterOfMassPosition();
        return TVector3F(vect.x(), vect.y(), vect.z());
    }

    return Origin3F;
}


/**
 * Donne l'orientation du modèle.
 *
 * \return Orientation du modèle.
 *
 * \sa IModel::setOrientation
 ******************************/

CQuaternion IModel::getRotation() const
{
    btQuaternion quat = m_body->getOrientation();
    return CQuaternion(quat.x(), quat.y(), quat.z(), quat.w());
}


/**
 * Donne la vitesse linéaire de l'objet.
 *
 * \return Vitesse linéaire de l'objet.
 *
 * \sa IModel::setLinearVelocity
 ******************************/

TVector3F IModel::getLinearVelocity() const
{
    const btVector3& vect = m_body->getLinearVelocity();
    return TVector3F(vect.x(), vect.y(), vect.z());
}


/**
 * Donne la vitesse angulaire de l'objet.
 *
 * \return Vitesse angulaire du modèle.
 *
 * \sa IModel::setAngularVelocity
 ******************************/

TVector3F IModel::getAngularVelocity() const
{
    const btVector3& vect = m_body->getAngularVelocity();
    return TVector3F(vect.x(), vect.y(), vect.z());
}


/**
 * Donne la masse du modèle.
 *
 * \return Masse du modèle en kilogrammes.
 *
 * \sa IModel::setMass
 ******************************/

float IModel::getMass() const
{
    return m_mass;
    //return (1.0f / m_body->getInvMass());
}


/**
 * Donne l'inverse la matrice d'inertie du modèle.
 *
 * \return Inverse de la matrice d'inertie.
 *
 * \sa IModel::setLocalInertia
 ******************************/

TMatrix3F IModel::getInvInertia() const
{
    const btMatrix3x3& matrix = m_body->getInvInertiaTensorWorld();

    return TMatrix3F(matrix[0].x(), matrix[0].y(), matrix[0].z(),
                     matrix[1].x(), matrix[1].y(), matrix[1].z(),
                     matrix[2].x(), matrix[2].y(), matrix[2].z());
}


/**
 * Donne la boite AABB englobant le modèle.
 *
 * \return Bounding box non orientée englobant le modèle.
 *
 * \sa IModel::setBoundingBox
 ******************************/

CBoundingBox IModel::getBoundingBox() const
{
    btVector3 min, max;
    m_body->getAabb(min, max);
    return CBoundingBox(TVector3F(min.x(), min.y(), min.z()), TVector3F(max.x(), max.y(), max.z()));
}


/**
 * Donne le numéro de la séquence actuellement utilisée.
 *
 * \return Numéro de la séquence.
 *
 * \sa IModel::setSequence
 ******************************/

unsigned int IModel::getSequence() const
{
    return m_params.sequence;
}


/**
 * Donne le mode de lecture des séquences.
 *
 * \return Mode de lecture des séquences.
 *
 * \sa IModel::setSeqMode
 ******************************/

IModel::TModelSeqMode IModel::getSeqMode() const
{
    return m_params.mode;
}


/**
 * Donne le numéro du skin utilisé.
 *
 * \return Numéro du skin.
 *
 * \sa IModel::setSkin
 ******************************/

unsigned int IModel::getSkin() const
{
    return m_params.skin;
}


/**
 * Donne le groupe utilisé par le modèle.
 *
 * \return Numéro du groupe.
 *
 * \sa IModel::setGroup
 ******************************/

unsigned int IModel::getGroup() const
{
    return m_params.group;
}


/**
 * Donne la valeur d'un contrôleur.
 *
 * \param controller Numéro du contrôleur.
 * \return Valeur du contrôleur.
 *
 * \sa IModel::setControllerValue
 ******************************/

float IModel::getControllerValue(unsigned int controller) const
{
    T_ASSERT(controller < TModelParams::NumControllers);
    return m_params.controllers[controller];
}


/**
 * Donne la valeur d'un blender.
 *
 * \param blender Numéro du blender.
 * \return Valeur du blender.
 *
 * \sa IModel::setBlendingValue
 ******************************/

float IModel::getBlendingValue(unsigned int blender) const
{
    T_ASSERT(blender < 2);
    return m_params.blending[blender];
}


/**
 * Modifie la position du modèle.
 *
 * \param position Position du modèle.
 *
 * \sa IModel::getPosition
 ******************************/

void IModel::setPosition(const TVector3F& position)
{
    TVector3F positionWithOffset = position - m_offset;

    btTransform transform;
    m_motionState->getWorldTransform(transform);
    transform.setOrigin(btVector3(positionWithOffset.X, positionWithOffset.Y, positionWithOffset.Z));
    m_motionState->setWorldTransform(transform);
    m_body->setWorldTransform(transform);
}


/**
 * Translate le modèle.
 *
 * \param v Vecteur de translation.
 *
 * \sa IModel::setPosition
 ******************************/

void IModel::translate(const TVector3F& v)
{
    m_body->translate(btVector3(v.X, v.Y, v.Z));
}


/**
 * Modifie l'orientation de l'objet.
 *
 * \param orientation Orientation du modèle.
 *
 * \sa IModel::getRotation
 ******************************/

void IModel::setRotation(const CQuaternion& orientation)
{
    btTransform transform;
    m_motionState->getWorldTransform(transform);
    transform.setRotation(btQuaternion(orientation.B, orientation.C, orientation.D, orientation.A));
    m_motionState->setWorldTransform(transform);
}


/**
 * Modifie la vitesse linéaire de l'objet.
 *
 * \param velocity Vitesse du modèle.
 *
 * \sa IModel::getLinearVelocity
 ******************************/

void IModel::setLinearVelocity(const TVector3F& velocity)
{
    Game::physicEngine->removeRigidBody(m_body);
    m_body->setLinearVelocity(btVector3(velocity.X, velocity.Y, velocity.Z));
    Game::physicEngine->addRigidBody(m_body);
}


/**
 * Modifie la vitesse angulaire de l'objet.
 *
 * \param velocity Vitesse angulaire du modèle.
 *
 * \sa IModelV::getOmega
 ******************************/

void IModel::setAngularVelocity(const TVector3F& velocity)
{
    Game::physicEngine->removeRigidBody(m_body);
    m_body->setAngularVelocity(btVector3(velocity.X, velocity.Y, velocity.Z));
    Game::physicEngine->addRigidBody(m_body);
}


/**
 * Modifie la masse du modèle.
 *
 * \param mass Masse du modèle en kilogrammes.
 *
 * \sa IModel::getMass
 ******************************/

void IModel::setMass(float mass)
{
    btVector3 local_inertia(0.0f, 0.0f, 0.0f);

    if (mass > 0.0f)
    {
        m_shape->calculateLocalInertia(mass, local_inertia);
        m_mass = mass;
    }
    else
    {
        m_mass = 0.0f;
    }

    Game::physicEngine->removeRigidBody(m_body);
    m_body->setMassProps(m_mass, local_inertia);
    Game::physicEngine->addRigidBody(m_body);
}


/**
 * Modifie la matrice d'inertie du modèle.
 *
 * \param inertia Inverse de la matrice d'inertie du modèle.
 *
 * \sa IModel::getInvInertia
 ******************************/

void IModel::setLocalInertia(const TVector3F& inertia)
{
    btVector3 local_inertia(inertia.X, inertia.Y, inertia.Z);

    if (m_mass > 0.0f)
    {
        m_shape->calculateLocalInertia(m_mass, local_inertia);
    }

    Game::physicEngine->removeRigidBody(m_body);
    m_body->setMassProps(m_mass, local_inertia);
    Game::physicEngine->addRigidBody(m_body);
}


/**
 * Rend le modèle mobile ou immobile.
 *
 * \param movable Booléen indiquant si l'objet est mobile.
 *
 * \sa IModelV::isMovable
 ******************************/

void IModel::setMovable(bool movable)
{
    m_movable = movable;

    btVector3 local_inertia(0.0f, 0.0f, 0.0f);

    if (movable)
    {
        if (m_mass > 0.0f)
        {
            m_shape->calculateLocalInertia(m_mass, local_inertia);
        }

        Game::physicEngine->removeRigidBody(m_body);
        m_body->setMassProps(m_mass, local_inertia);
        Game::physicEngine->addRigidBody(m_body);
    }
    else
    {
        Game::physicEngine->removeRigidBody(m_body);
        m_body->setMassProps(0.0f, local_inertia);
        Game::physicEngine->addRigidBody(m_body);
    }
}


/**
 * Modifie l'entité liée au modèle.
 *
 * \param entity Pointeur sur l'entité liée au modèle.
 *
 * \sa IModel::getEntity
 ******************************/

void IModel::setEntity(IBrushEntity * entity)
{
    m_entity = entity;
}


/**
 * Modifie le buffer graphique du modèle.
 * L'ancien buffer n'est pas détruit.
 *
 * \param buffer Pointeur sur le buffer du modèle.
 *
 * \sa IModel::getBuffer
 ******************************/

void IModel::setBuffer(CBuffer * buffer)
{
    m_buffer = buffer;
}


void IModel::setModelData(IModelData * modelData)
{
    m_data = modelData;
}


/**
 * Modifie le volume physique du modèle.
 *
 * \param shape Pointeur sur un volume.
 ******************************/

void IModel::setShape(btCollisionShape * shape)
{
    //if (shape)
    {
        delete m_shape;
        m_shape = shape;

        if (m_mass > 0.0f)
        {
            btVector3 inertia = m_body->getInvInertiaDiagLocal();
            m_shape->calculateLocalInertia(m_mass, inertia);
        }

        Game::physicEngine->removeRigidBody(m_body);
        m_body->setCollisionShape(m_shape);
        Game::physicEngine->addRigidBody(m_body);
    }
}


/**
 * Modifie le numéro de la séquence à utiliser.
 * Si le numéro est trop grand, la première séquence est utilisée.
 *
 * \param sequence Numéro de la séquence à utiliser.
 *
 * \sa IModel::getSequence
 ******************************/

void IModel::setSequence(unsigned int sequence)
{
    if (m_data && sequence >= m_data->getNumSequences())
    {
        sequence = 0;
    }

    m_params.sequence = sequence;
    m_params.frame = 0;
}


/**
 * Modifie le numéro de la séquence à utiliser.
 *
 * \param name Nom de la séquence à utiliser.
 *
 * \sa IModelV::getSequence
 ******************************/

void IModel::setSequenceByName(const CString& name)
{
    T_ASSERT(m_data != nullptr);

    m_params.sequence = m_data->getSequenceNumber(name);
    m_params.frame = 0;
}


/**
 * Modifie le mode de lecture des séquences.
 *
 * \param mode Mode de lecture des séquences.
 *
 * \sa IModel::getSeqMode
 ******************************/

void IModel::setSeqMode(TModelSeqMode mode)
{
    m_params.mode = mode;
}


/**
 * Modifie le numéro du skin à utiliser.
 *
 * \param skin Numéro du skin à utiliser.
 *
 * \sa IModel::getSkin
 ******************************/

void IModel::setSkin(unsigned int skin)
{
    T_ASSERT(m_data != nullptr);
    m_params.skin = (skin < m_data->getNumSkins() ? skin : 0);
}


/**
 * Modifie le numéro du groupe à utiliser.
 *
 * \param group Numéro du groupe à utiliser.
 *
 * \sa IModel::getGroup
 ******************************/

void IModel::setGroup(unsigned int group)
{
    T_ASSERT(m_data != nullptr);
    m_params.group = (group < m_data->getNumGroups() ? group : 0);
}


/**
 * Modifie la valeur d'un controlleur.
 *
 * \param controller Numéro du controlleur (entre 0 et 7).
 * \param value      Valeur du controlleur.
 *
 * \sa IModel::getControllerValue
 ******************************/

void IModel::setControllerValue(unsigned int controller, float value)
{
    T_ASSERT(controller < TModelParams::NumControllers);
    m_params.controllers[controller] = value;
}


/**
 * Modifie la valeur du blending.
 *
 * \param blender Numéro du blender.
 * \param value   Valeur du blending.
 *
 * \sa IModel::getBlendingValue
 ******************************/

void IModel::setBlendingValue(unsigned int blender, float value)
{
    T_ASSERT(blender < 2);
    m_params.blending[blender] = value;
}


void IModel::setOffset(const TVector3F& offset)
{
    m_offset = offset;
    translate(m_offset - offset);
}


/**
 * Ajoute une force au modèle.
 *
 * \param force Force à ajouter.
 ******************************/

void IModel::addForce(const TVector3F& force)
{
    m_body->applyCentralImpulse(btVector3(force.X, force.Y, force.Z));
}


/**
 * Ajoute un couple au modèle.
 *
 * \param torque Couple à ajouter.
 ******************************/

void IModel::addTorque(const TVector3F& torque)
{
    m_body->applyTorqueImpulse(btVector3(torque.X, torque.Y, torque.Z));
}


/**
 * Affiche le modèle.
 *
 * \todo Utiliser une matrice de transformation prenant en compte la translation et les rotations.
 ******************************/

void IModel::drawModel() const
{
    if (!m_buffer)
        return;

    T_ASSERT(m_body != nullptr);

    btTransform transform;
    m_motionState->getWorldTransform(transform);
    //transform.setOrigin(transform.getOrigin() + btVector3(m_offset.X, m_offset.Y, m_offset.Z));

    btScalar matrix[16];
    transform.getOpenGLMatrix(matrix);

    glMultMatrixf(matrix);
    glTranslatef(m_offset.X, m_offset.Y, m_offset.Z);
//ILogger::log() << "Affichage du buffer " << m_buffer << "\n";
    // Affichage du buffer graphique
    m_buffer->draw();
}


/**
 * Met à jour les données du modèle.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void IModel::update(unsigned int frameTime)
{
    //T_ASSERT(m_data != nullptr);
    T_ASSERT(m_body != nullptr);

    m_body->clearForces();

    if (m_data)
    {
        // Mise-à-jour de la boite englobante
        //setBoundingBox(m_data->getBoundingBox(m_params.sequence));

        m_data->updateParams(m_params, frameTime);

        if (m_buffer)
        {
            m_data->updateBuffer(m_buffer, m_params);
            m_buffer->update();
        }
    }
    else if (m_buffer)
    {
        m_buffer->update();
    }
}

} // Namespace Ted

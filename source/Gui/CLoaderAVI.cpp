/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLoaderAVI.cpp
 * \date 26/04/2009 Création de la classe CLoaderAVI.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 11/07/2010 Chargement d'une vidéo et création de la texture.
 * \date 14/07/2010 Correction d'un bug dans la mise-à-jour de la vidéo.
 * \date 15/07/2010 La lecture en boucle fonctionne correctement.
 *                  Les images utilisent le format RGBA.
 * \date 04/12/2010 Ajout des méthodes Play et Stop.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CLoaderAVI.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/CImage.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"
#include "Sound/CSoundEngine.hpp"
#include "Sound/CMusic.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderAVI::CLoaderAVI() :
ILoader     (),
m_loop      (false),
m_play      (false),
m_texture   (0),
m_frameTime (0),
m_time      (0),
m_width     (0),
m_height    (0),
m_frame     (0),
m_lastFrame (0),
#ifdef T_SYSTEM_WINDOWS
m_video     (nullptr),
m_pgf       (nullptr),
#endif
m_audio     (nullptr)
{ }


/**
 * Destructeur.
 ******************************/

CLoaderAVI::~CLoaderAVI()
{
#ifdef T_SYSTEM_WINDOWS
    AVIStreamGetFrameClose(m_pgf);

    // Suppression des flux
    if (m_video)
        AVIStreamRelease(m_video);

    if (m_audio)
    {
        //AVIStreamRelease(m_audio);
        Game::soundEngine->removeMusic(m_audio);
        delete m_audio;
    }

    Game::textureManager->deleteTexture(m_texture);
    AVIFileExit();
#endif
}


/**
 * Indique si la vidéo est jouée en boucle.
 *
 * \return Booléen.
 *
 * \sa CLoaderAVI::setLoop
 ******************************/

bool CLoaderAVI::isLoop() const
{
    return m_loop;
}


/**
 * Indique si la vidéo est en cours de lecture.
 *
 * \return Booléen.
 ******************************/

bool CLoaderAVI::isPlaying() const
{
    return m_play;
}


/**
 * Donne l'identifiant de la texture contenant la vidéo.
 *
 * \return Identifiant de la texture.
 ******************************/

unsigned int CLoaderAVI::getTextureId() const
{
    return m_texture;
}


/**
 * Donne la durée d'une frame de la vidéo.
 *
 * \return Durée d'une frame en millisecondes.
 ******************************/

unsigned int CLoaderAVI::getFrameTime() const
{
    return m_frameTime;
}


/**
 * Donne la position de lecture.
 *
 * \return Position de lecture en millisecondes.
 *
 * \sa CLoaderAVI::setTime
 ******************************/

unsigned int CLoaderAVI::getTime() const
{
    return m_time;
}


/**
 * Donne la largeur de l'image.
 *
 * \return Largeur de l'image en pixels.
 ******************************/

int CLoaderAVI::getWidth() const
{
    return m_width;
}


/**
 * Donne la hauteur de l'image.
 *
 * \return Hauteur de l'image en pixels.
 ******************************/

int CLoaderAVI::getHeight() const
{
    return m_height;
}


/**
 * Pour jouer la vidéo en boucle.
 *
 * \param loop Indique si la vidéo doit être jouée en boucle.
 *
 * \sa CLoaderAVI::isLoop
 ******************************/

void CLoaderAVI::setLoop(bool loop)
{
    m_loop = loop;

    if (m_audio)
    {
        m_audio->setLoop();
    }
}


/**
 * Change la position de lecture de la vidéo.
 *
 * \todo Modifier le flux audio.
 *
 * \param time Position de lecture en millisecondes.
 *
 * \sa CLoaderAVI::getTime
 ******************************/

void CLoaderAVI::setTime(unsigned int time)
{
    m_time = (time > m_lastFrame * m_frameTime ? m_lastFrame * m_frameTime : time);
}


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderAVI::loadFromFile(const CString& fileName)
{
    CApplication::getApp()->log(CString::fromUTF8("Chargement de la vidéo %1").arg(fileName));

#ifdef T_SYSTEM_WINDOWS
    AVIStreamGetFrameClose(m_pgf);

    // Suppression des flux
    if (m_video)
        AVIStreamRelease(m_video);

    if (m_audio)
    {
        //AVIStreamRelease(m_audio);
        Game::soundEngine->removeMusic(m_audio);
        delete m_audio;
        m_audio = nullptr;
    }

    Game::textureManager->deleteTexture(m_texture);
    AVIFileExit();
#endif

    m_time = 0;
    m_play = false;

    // Suppression de la précédente texture
    if (m_texture)
    {
        Game::textureManager->deleteTexture(m_texture);
    }

#ifdef T_SYSTEM_WINDOWS

    AVIFileInit();

    // Ouverture du flux vidéo
    if (AVIStreamOpenFromFile(&m_video, fileName.toCharArray(), streamtypeVIDEO, 0, OF_READ, nullptr))
    {
        CApplication::getApp()->log(CString::fromUTF8("AVIStreamOpenFromFile : impossible de lire le flux video."), ILogger::Error);
        return false;
    }

    // Infos sur le stream video
    AVISTREAMINFO psi;

    if (AVIStreamInfo(m_video, &psi, sizeof(psi)))
    {
        CApplication::getApp()->log("AVIStreamInfo donne une erreur", ILogger::Error);
        return false;
    }

    // Dimensions de la vidéo
    m_width = psi.rcFrame.right - psi.rcFrame.left;
    m_height = psi.rcFrame.bottom - psi.rcFrame.top;

    m_fileName = fileName;

    m_pixels.resize(4 * m_width * m_height);

    for (int i = 0; i < m_width * m_height; ++i)
    {
        m_pixels[4 * i + 0] = 0x00;
        m_pixels[4 * i + 1] = 0x00;
        m_pixels[4 * i + 2] = 0x00;
        m_pixels[4 * i + 3] = 0xFF;
    }

    // Création de la texture
    CImage img(m_width, m_height, reinterpret_cast<CColor *>(&m_pixels[0]));

    m_texture = Game::textureManager->loadTexture(m_fileName, img, CTextureManager::FilterNone);


    m_lastFrame = AVIStreamLength(m_video);

    if (m_lastFrame == 0)
    {
        return false;
    }

    m_frameTime = AVIStreamSampleToTime(m_video, m_lastFrame) / m_lastFrame;
    m_pgf = AVIStreamGetFrameOpen(m_video, nullptr);

    // On ne peut pas récupérer les frames
    if (m_pgf == nullptr)
    {
        CApplication::getApp()->log("AVIStreamGetFrameOpen retourne un pointeur nul", ILogger::Error);
        return false;
    }

    // Ouverture du flux audio
    m_audio = new CMusic();
    m_audio->loadFromVideo(m_fileName);

    if (m_loop)
    {
        m_audio->setLoop();
    }

    m_audio->play();
    Game::soundEngine->addMusic(m_audio);

#endif

    //m_play = true;
    return true;
}


/**
 * Met-à-jour la texture et le buffer sonore.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CLoaderAVI::update(unsigned int frameTime)
{
    if (!m_play || m_frameTime == 0)
    {
        return;
    }

    m_time += frameTime;
    unsigned int new_frame = m_time / m_frameTime;

    // La frame n'a pas changée
    if (new_frame == m_frame)
    {
        return;
    }

    // La vidéo est terminée
    if (new_frame >= m_lastFrame)
    {
        if (m_loop)
        {
            m_frame = 0;
            m_time = 0;
        }
        // Dernière frame
        else
        {
            if (new_frame == m_lastFrame)
            {
                // On remplace la vidéo par une image noire
                for (int i = 0; i < m_width * m_height; ++i)
                {
                    m_pixels[4 * i + 0] = 0x00;
                    m_pixels[4 * i + 1] = 0x00;
                    m_pixels[4 * i + 2] = 0x00;
                    m_pixels[4 * i + 3] = 0xFF;
                }

                // Mise-à-jour de la texture
                CImage img(m_width, m_height, reinterpret_cast<CColor *>(&m_pixels[0]));
                Game::textureManager->reloadTexture(m_texture, img);
            }

            m_frame = m_lastFrame;
            m_time = m_lastFrame * m_frameTime;
            m_play = false;

            return;
        }
    }
    else
    {
        m_frame = new_frame;
    }

#ifdef T_SYSTEM_WINDOWS

    // On récupère la frame (plante !)
    LPBITMAPINFOHEADER lpbi = reinterpret_cast<LPBITMAPINFOHEADER>(AVIStreamGetFrame(m_pgf, m_frame));

    if (lpbi == nullptr)
    {
        CApplication::getApp()->log("AVIStreamGetFrame retourne un pointeur nul", ILogger::Error);
        return;
    }

    // DEBUG
    if (lpbi->biBitCount != 24)
    {
        CApplication::getApp()->log(CString::fromUTF8("La vidéo n'utilise pas 3 composantes par couleur (%1)").arg(lpbi->biBitCount), ILogger::Error);
        return;
    }

    unsigned char * temp = reinterpret_cast<unsigned char *>(lpbi) + lpbi->biSize + lpbi->biClrUsed * sizeof(RGBQUAD);
    CColor * tempRGBA = new CColor[m_width * m_height];

    for (int h = 0; h < m_height; ++h)
    {
        for (int w = 0; w < m_width; ++w)
        {
            tempRGBA[(m_height - h - 1) * m_width + w].setBlue(temp[(h * m_width + w) * 3 + 0]);
            tempRGBA[(m_height - h - 1) * m_width + w].setGreen(temp[(h * m_width + w) * 3 + 1]);
            tempRGBA[(m_height - h - 1) * m_width + w].setRed(temp[(h * m_width + w) * 3 + 2]);
        }
    }

    // Mise-à-jour de la texture
    CImage img(m_width, m_height, tempRGBA);
    Game::textureManager->reloadTexture(m_texture, img);

#endif
}


/**
 * Démarre la lecture de la vidéo.
 ******************************/

void CLoaderAVI::play()
{
    m_frame = 0;
    m_time = 0;
    m_play = true;

    if (m_audio)
    {
        m_audio->play();
    }
}


/**
 * Arrête la lecture de la vidéo.
 ******************************/

void CLoaderAVI::stop()
{
    m_frame = m_lastFrame;
    m_time = m_lastFrame * m_frameTime;
    m_play = false;

    if (m_audio)
    {
        m_audio->stop();
    }
}

} // Namespace Ted

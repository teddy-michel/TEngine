/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWidgetDebugFPS.cpp
 * \date 01/04/2013 Création de la classe CWidgetDebugFPS.
 * \date 13/06/2014 Amélioration de l'affichage du graphe.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWidgetDebugFPS.hpp"
#include "Core/CApplication.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CGraph.hpp"
#include "Gui/CCurveCircularBuffer.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWidgetDebugFPS::CWidgetDebugFPS(IWidget * parent) :
IWidget     (parent),
m_graphFPS  (nullptr),
m_bufferFPS (CApplication::getApp()->getFPSList())
{
    m_graphFPS = new CGraph(this);
    m_graphFPS->setPosition(50, 50);
    m_graphFPS->setSize(600, 300);
    m_graphFPS->setAutoX(false);
    m_graphFPS->setChartMinX(0);
    m_graphFPS->setChartMaxX(100 - 1);
    m_graphFPS->setHorizontalGridEnabled(true);
    m_graphFPS->setVerticalGridEnabled(true);

    CCurveCircularBuffer * curve = new CCurveCircularBuffer(&m_bufferFPS);
    curve->setMode(ICurve::VLine);
    curve->setColor(CColor(255, 0, 0, 128));
    m_graphFPS->addCurve(curve);
}


/**
 * Dessine le widget.
 ******************************/

void CWidgetDebugFPS::draw()
{
    m_bufferFPS = CApplication::getApp()->getFPSList();
    m_graphFPS->draw();
}

} // Namespace Ted

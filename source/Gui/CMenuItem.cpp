/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CMenuItem.cpp
 * \date 06/05/2009 Création de la classe GuiMenuItem.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 23/07/2010 L'affichage du texte est limité dans un rectangle.
 * \date 29/05/2011 La classe est renommée en CMenuItem.
 * \date 13/06/2014 Utilisation d'une marge à l'intérieur des éléments.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CMenuItem.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CMenu.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/Time.hpp"
#include "Core/Events.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

const unsigned int MenuItemTimeFocus = 500; ///< Durée en millisecondes pour passer d'une couleur à une autre.


/**
 * Constructeur.
 *
 * \param name Nom de l'item.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CMenuItem::CMenuItem(const CString& name, IWidget * parent) :
IWidget     (parent),
m_menu      (nullptr),
m_name      (name),
m_separator (false),
m_time      (getElapsedTime()),
m_timeFocus (MenuItemTimeFocus)
{ }


/**
 * Constructeur.
 *
 * \param separator Indique si l'item est un séparateur.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CMenuItem::CMenuItem(bool separator, IWidget * parent) :
IWidget     (parent),
m_menu      (nullptr),
m_name      (),
m_separator (separator),
m_time      (getElapsedTime()),
m_timeFocus (MenuItemTimeFocus)
{ }


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CMenuItem::CMenuItem(IWidget * parent) :
IWidget     (parent),
m_menu      (nullptr),
m_name      (),
m_separator (false),
m_time      (getElapsedTime()),
m_timeFocus (MenuItemTimeFocus)
{ }


/**
 * Accesseur pour name.
 *
 * \return Nom de l'item.
 ******************************/

CString CMenuItem::getName() const
{
    return m_name;
}


/**
 * Indique si l'item est un séparateur.
 *
 * \return Booléen.
 ******************************/

bool CMenuItem::isSeparator() const
{
    return m_separator;
}


/**
 * Modifie le nom de l'item.
 *
 * \param name Nom de l'item.
 ******************************/

void CMenuItem::setName(const CString& name)
{
    m_name = name;
}


/**
 * Transforme l'item en séparateur.
 *
 * \param separator Indique si l'item est un séparateur.
 ******************************/

void CMenuItem::setSeparator(bool separator)
{
    m_separator = separator;
}


/**
 * Définit le menu qui possède cet élément.
 * Les paramètres d'affichage sont récupérés depuis le menu.
 *
 * \param menu Pointeur sur le menu.
 ******************************/

void CMenuItem::setMenu(CMenu * menu)
{
    m_menu = menu;
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CMenuItem::onEvent(const CMouseEvent& event)
{
    if (event.type == ButtonReleased)
    {
        onClick();
    }
}


/**
 * Dessine l'item.
 *
 * \todo Pouvoir changer les paramètres d'affichage.
 ******************************/

void CMenuItem::draw()
{
    unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_time;
    m_time = time;

    TTextParams params;
    params.font  = (m_menu ? m_menu->getFontId() : Game::fontManager->getFontId("Arial"));

#ifdef T_USE_FREETYPE
    params.size  = (m_menu ? m_menu->getFontSize() : 18);
#else
    params.size  = (m_menu ? m_menu->getFontSize() : 20);
#endif // T_USE_FREETYPE

    params.rec.setWidth(getWidth() - (m_menu ? m_menu->getItemPadding().getLeft() + m_menu->getItemPadding().getRight() : 0));
    params.rec.setHeight(getHeight() - (m_menu ? m_menu->getItemPadding().getTop() + m_menu->getItemPadding().getBottom() : 0));

    if (m_separator)
    {
        params.text  = "--------------------";
        params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0));
        params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0));
        params.color = (m_menu ? m_menu->getFontColorInactive() : CColor::White);
    }
    else
    {
        params.text = m_name;

        TVector2UI txtdim = Game::fontManager->getTextSize(params);

        // Alignement horizontal
        switch (m_menu->getTextAlignment())
        {
            case AlignTopLeft:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0));
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0));
                break;

            case AlignMiddleLeft:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0));
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0) + (m_height - (m_menu ? m_menu->getItemPadding().getTop() + m_menu->getItemPadding().getBottom() : 0) - txtdim.Y) / 2);
                break;

            case AlignBottomLeft:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0));
                params.rec.setY(getY() + m_height - (m_menu ? m_menu->getItemPadding().getBottom() : 0) - txtdim.Y);
                break;

            case AlignTopCenter:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0) + (m_width - (m_menu ? m_menu->getItemPadding().getLeft() + m_menu->getItemPadding().getRight() : 0) - txtdim.X) / 2);
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0));
                break;

            case AlignMiddleCenter:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0) + (m_width - (m_menu ? m_menu->getItemPadding().getLeft() + m_menu->getItemPadding().getRight() : 0) - txtdim.X) / 2);
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0) + (m_height - (m_menu ? m_menu->getItemPadding().getTop() + m_menu->getItemPadding().getBottom() : 0) - txtdim.Y) / 2);
                break;

            case AlignBottomCenter:
                params.rec.setX(getX() + (m_menu ? m_menu->getItemPadding().getLeft() : 0) + (m_width - (m_menu ? m_menu->getItemPadding().getLeft() + m_menu->getItemPadding().getRight() : 0) - txtdim.X) / 2);
                params.rec.setY(getY() + m_height - (m_menu ? m_menu->getItemPadding().getBottom() : 0) - txtdim.Y);
                break;

            case AlignTopRight:
                params.rec.setX(getX() + m_width - (m_menu ? m_menu->getItemPadding().getRight() : 0) - txtdim.X);
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0));
                break;

            case AlignMiddleRight:
                params.rec.setX(getX() + m_width - (m_menu ? m_menu->getItemPadding().getRight() : 0) - txtdim.X);
                params.rec.setY(getY() + (m_menu ? m_menu->getItemPadding().getTop() : 0) + (m_height - (m_menu ? m_menu->getItemPadding().getTop() + m_menu->getItemPadding().getBottom() : 0) - txtdim.Y) / 2);
                break;

            case AlignBottomRight:
                params.rec.setX(getX() + m_width - (m_menu ? m_menu->getItemPadding().getRight() : 0) - txtdim.X);
                params.rec.setY(getY() + m_height - (m_menu ? m_menu->getItemPadding().getBottom() : 0) - txtdim.Y);
                break;
        }

        // Si le texte est pointé par le curseur de la souris, on change sa couleur
        if (Game::guiEngine->isPointed(this))
        {
            if (m_timeFocus > frameTime)
            {
                m_timeFocus -= frameTime;
            }
            else
            {
                m_timeFocus = 0;
            }

            //const CColor colorText(255, 255, static_cast<unsigned char>(255.0f * m_timeFocus / static_cast<float>(MenuItemTimeFocus)));
            //params.color = colorText;
            //params.color = (m_menu ? m_menu->getFontColorActive() : CColor::Yellow);
        }
        else
        {
            m_timeFocus += frameTime;

            if (m_timeFocus > MenuItemTimeFocus)
            {
                m_timeFocus = MenuItemTimeFocus;
            }

            //const CColor colorText(255, 255, static_cast<unsigned char>(255.0f * m_timeFocus / static_cast<float>(MenuItemTimeFocus)));
            //params.color = colorText;
            //params.color = (m_menu ? m_menu->getFontColorInactive() : CColor::White);
        }

        float percentage = static_cast<float>(m_timeFocus) / static_cast<float>(MenuItemTimeFocus);

        // Couleur du texte
        const CColor fontColorActive = (m_menu ? m_menu->getFontColorActive() : CColor::Yellow);
        const CColor fontColorInactive = (m_menu ? m_menu->getFontColorInactive() : CColor::White);
        params.color = fontColorActive.interpolate(fontColorInactive, percentage);

        // Fond de l'élément
        const CColor bgColorActive = (m_menu ? m_menu->getItemColorActive() : CColor(0, 0, 0, 0));
        const CColor bgColorInactive = (m_menu ? m_menu->getItemColorInactive() : CColor(0, 0, 0, 0));
        Game::guiEngine->drawRectangle(CRectangle(getX(), getY(), getWidth(), getHeight()), bgColorActive.interpolate(bgColorInactive, percentage));
    }

    Game::guiEngine->drawText(params);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ILayoutBox.cpp
 * \date 14/06/2014 Création de la classe ILayoutBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>
#include <vector>

#include "Gui/ILayoutBox.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

ILayoutBox::ILayoutBox(IWidget * parent) :
ILayout (parent)
{ }


/**
 * Destructeur.
 ******************************/

ILayoutBox::~ILayoutBox()
{
    clearChildren();
}


/**
 * Donne l'objet se trouvant à une certaine position.
 *
 * \param  pos Position (commence à 0).
 * \return Pointeur sur l'objet.
 *
 * \sa ILayoutBox::setChild
 ******************************/

IWidget * ILayoutBox::getChild(int pos) const
{
    if (pos < 0 || pos > getNumChildren())
    {
        return nullptr;
    }

    std::list<TLayoutItem>::const_iterator it = m_children.begin();
    std::advance(it, pos);

    return (it == m_children.end() ? nullptr : it->widget);
}


/**
 * Modifie la largeur du layout.
 *
 * \param width Largeur du layout en pixels.
 *
 * \sa ILayoutBox::setHeight
 ******************************/

void ILayoutBox::setWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width > m_maxWidth)
        m_width = m_maxWidth;
    else
        m_width = width;

    if (m_parent && m_parent->getWidth() < m_width)
    {
        m_parent->setWidth(m_width);
    }

    computeSize();
}


/**
 * Modifie la hauteur du layout.
 *
 * \param height Hauteur du layout en pixels.
 *
 * \sa ILayoutBox::setWidth
 ******************************/

void ILayoutBox::setHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height > m_maxHeight)
        m_height = m_maxHeight;
    else
        m_height = height;

    if (m_parent && m_parent->getHeight() < m_height)
    {
        m_parent->setHeight(m_height);
    }

    computeSize();
}


/**
 * Dessine le layout.
 ******************************/

void ILayoutBox::draw()
{
    // Affichage du contenu
    for (std::list<TLayoutItem>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        it->widget->draw();
    }
}


/**
 * Donne le nombre d'objets enfants.
 *
 * \return Nombre d'objets enfants.
 ******************************/

int ILayoutBox::getNumChildren() const
{
    return m_children.size();
}


/**
 * Indique si un objet est présent dans la liste.
 *
 * \param widget Objet à chercher.
 * \return Booléen.
 ******************************/

bool ILayoutBox::isChild(const IWidget * widget) const
{
    if (widget == nullptr)
    {
        return false;
    }

    for (std::list<TLayoutItem>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (it->widget == widget)
        {
            return true;
        }
    }

    return false;
}


/**
 * Enlève un objet de la liste mais ne le supprime pas.
 *
 * \param widget Pointeur sur l'objet à enlever.
 ******************************/

void ILayoutBox::removeChild(const IWidget * widget)
{
    if (widget == nullptr)
    {
        return;
    }

    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (it->widget == widget)
        {
            it->widget->setParent(nullptr);
            m_children.erase(it);

            computeSize();
            break;
        }
    }
}


/**
 * Enlève un objet de la liste mais ne le supprime pas.
 *
 * \param pos Position de l'objet à supprimer.
 ******************************/

void ILayoutBox::removeChild(int pos)
{
    if (pos < 0 || pos >= getNumChildren())
    {
        return;
    }

    std::list<TLayoutItem>::iterator it = m_children.begin();
    std::advance(it, pos);

    if (it != m_children.end())
    {
        it->widget->setParent(nullptr);
        m_children.erase(it);

        computeSize();
    }
}


/**
 * Supprime tous les objets de la liste.
 ******************************/

void ILayoutBox::clearChildren()
{
    for (std::list<TLayoutItem>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        delete it->widget;
    }

    m_children.clear();
    computeSize();
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void ILayoutBox::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
    {
        return;
    }

    // On parcourt la liste des objets enfants
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        int tmp_x = it->widget->getX();
        int tmp_y = it->widget->getY();

        if (static_cast<int>(event.x) >= tmp_x &&
            static_cast<int>(event.x) <= tmp_x + it->widget->getWidth() &&
            static_cast<int>(event.y) >= tmp_y &&
            static_cast<int>(event.y) <= tmp_y + it->widget->getHeight())
        {
            if (event.type == ButtonPressed || event.type == DblClick)
            {
                if (event.button == MouseButtonLeft)
                {
                    Game::guiEngine->setSelected(it->widget);
                }

                Game::guiEngine->setFocus(it->widget);
            }

            Game::guiEngine->setPointed(it->widget);

            it->widget->onEvent(event);
            return;
        }
    }
}

} // Namespace Ted

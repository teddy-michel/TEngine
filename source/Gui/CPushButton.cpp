/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CPushButton.cpp
 * \date       2008 Création de la classe GuiPushButton.
 * \date 12/07/2010 Correction d'un bug d'affichage du texte lorsque les dimensions du bouton étaient trop petites.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 23/07/2010 L'affichage du texte est limité dans un rectangle.
 * \date 24/07/2010 Redéfinition de la méthode setText et suppression de ComputeSize.
 * \date 12/05/2011 Les paramètres du texte sont conservés par la classe.
 * \date 29/05/2011 La classe est renommée en CPushButton.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Time.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/Events.hpp"


namespace Ted
{

const int ButtonMinWidth = 20;   ///< Longueur minimale d'un bouton en pixels.
const int ButtonMinHeight = 20;  ///< Hauteur minimale d'un bouton en pixels.
const int ButtonMargin = 5;      ///< Marges intérieures d'un bouton.
const unsigned int ButtonTimeFocus = 500; ///< Durée en millisecondes pour passer d'une couleur à une autre.

#ifdef T_USE_FREETYPE
const unsigned int ButtonTextSize = 13; ///< Taille du texte.
#else
const unsigned int ButtonTextSize = 16; ///< Taille du texte.
#endif // T_USE_FREETYPE


/**
 * Constructeur.
 *
 * \param text   Texte du bouton.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CPushButton::CPushButton(const CString& text, IWidget * parent) :
IButton     (text, parent),
m_time      (getElapsedTime()),
m_timeFocus (ButtonTimeFocus)
{
    m_textParams.text = m_text;
    m_textParams.font = Game::fontManager->getFontId("Arial");
    m_textParams.size = ButtonTextSize;

    int minw = Game::fontManager->getTextSize (m_textParams).X + 2 * ButtonMargin;
    setMinSize ((minw < ButtonMinWidth ? ButtonMinWidth : minw), ButtonMinHeight);
}


/**
 * Modifie le texte du bouton.
 *
 * \param text Texte du bouton.
 ******************************/

void CPushButton::setText(const CString& text)
{
    IButton::setText(text);
    m_textParams.text = m_text;

    int minw = Game::fontManager->getTextSize(m_textParams).X + 2 * ButtonMargin;
    setMinWidth((minw < ButtonMinWidth ? ButtonMinWidth : minw));
}


/**
 * Dessine le bouton.
 *
 * \todo Pouvoir changer les paramètres d'affichage (police, taille, etc.).
 ******************************/

void CPushButton::draw()
{
    unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_time;
    m_time = time;

    const int tmp_x = getX();
    const int tmp_y = getY();
    const int tmp_w = getWidth();
    const int tmp_h = getHeight();

    TVector2UI txtdim = Game::fontManager->getTextSize(m_textParams);

    m_textParams.rec.setX(tmp_x + (tmp_w - static_cast<int>(txtdim.X)) / 2);
    m_textParams.rec.setY(tmp_y + (tmp_h - static_cast<int>(txtdim.Y)) / 2);

    // Le bouton est désactivé
    if (!isHierarchyEnable())
    {
        m_timeFocus += frameTime;

        if (m_timeFocus > ButtonTimeFocus)
        {
            m_timeFocus = ButtonTimeFocus;
        }

        // Fond du bouton
        Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(49, 60, 83));

        // Bordure
        Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(28, 38, 60));

        m_textParams.color = CColor(160, 160, 160);

        // Texte du bouton
        Game::guiEngine->drawText(m_textParams);
    }
    // Si le bouton est pointé par le curseur de la souris, on change sa couleur de fond
    else if (Game::guiEngine->isPointed(this))
    {
        // Fond du bouton
        if (Game::guiEngine->isSelected(this))
        {
            m_timeFocus = ButtonTimeFocus / 2;

            Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(69, 82, 104));
            m_textParams.color = CColor(120, 220, 0);
        }
        else
        {
            if (m_timeFocus > frameTime)
            {
                m_timeFocus -= frameTime;
            }
            else
            {
                m_timeFocus = 0;
            }

            Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(89, 103, 124));
            m_textParams.color = CColor(156, 255, 0);
        }

        // Bordure
        Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(28, 38, 60));

        // Texte du bouton
        Game::guiEngine->drawText(m_textParams);
    }
    else
    {
        m_timeFocus += frameTime;

        if (m_timeFocus > ButtonTimeFocus)
        {
            m_timeFocus = ButtonTimeFocus;
        }

        float focusPercentage = m_timeFocus / static_cast<float>(ButtonTimeFocus);
        CColor colorBackground = CColor(89, 103, 124).interpolate(CColor(49, 60, 83), focusPercentage);
        CColor colorText = CColor(156, 255, 0).interpolate(CColor::White, focusPercentage);

        // Fond du bouton
        Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), colorBackground);

        // Bordure
        Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h), CColor(28, 38, 60));

        m_textParams.color = colorText;

        // Texte du bouton
        Game::guiEngine->drawText(m_textParams);
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CPushButton::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    const int tmp_x = getX();
    const int tmp_y = getY();

    if (event.button == MouseButtonLeft &&
        static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + getHeight())
    {
        if (event.type == ButtonReleased)
        {
            if (Game::guiEngine->isSelected(this))
            {
                onClicked();
            }

            onReleased();
            setDown(false);
        }
        else if (event.type == ButtonPressed)
        {
            onPressed();
            setDown(true);
        }
    }
}


/**
 * Modifie la largeur du bouton.
 *
 * \param width Largeur du bouton.
 ******************************/

void CPushButton::setWidth(int width)
{
    IButton::setWidth(width);
    m_textParams.rec.setWidth(getWidth());
}


/**
 * Modifie la hauteur du bouton.
 *
 * \param height Hauteur du bouton.
 ******************************/

void CPushButton::setHeight(int height)
{
    IButton::setHeight(height);
    m_textParams.rec.setHeight(getHeight());
}

} // Namespace Ted

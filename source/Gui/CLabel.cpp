/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLabel.cpp
 * \date 18/01/2010 Création de la classe GuiLabel.
 * \date 23/07/2010 L'affichage du texte est limité dans un rectangle.
 * \date 23/01/2011 Utilisation d'un TTextParams pour enregistrer les paramètres du texte.
 * \date 08/05/2011 On peut modifier la taille du texte.
 * \date 29/05/2011 La classe est renommée en CLabel.
 * \date 03/04/2012 On peut modifier la police de caractère.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CLabel.hpp"
#include "Gui/CGuiEngine.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param text   Texte du label.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLabel::CLabel(const CString& text, IWidget * parent) :
IWidget (parent),
m_align (AlignLeft)
{
    m_params.text  = text;
    m_params.font  = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
    m_params.size  = 13;
#else
    m_params.size  = 16;
#endif // T_USE_FREETYPE

    m_params.color = CColor::Black;

    // On modifie les dimensions minimales du label
    TVector2UI minsize = Game::fontManager->getTextSize(m_params);
    setMinSize(minsize.X, minsize.Y);
}


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLabel::CLabel(IWidget * parent) :
IWidget (parent),
m_align (AlignLeft)
{
    m_params.text  = CString();
    m_params.font  = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
    m_params.size  = 13;
#else
    m_params.size  = 16;
#endif // T_USE_FREETYPE

    m_params.color = CColor::Black;

    // On modifie les dimensions minimales du label
    TVector2UI minsize = Game::fontManager->getTextSize(m_params);
    setMinSize(minsize.X, minsize.Y);
}


/**
 * Donne le texte du label.
 *
 * \return Texte du label.
 *
 * \sa CLabel::setText
 ******************************/

CString CLabel::getText() const
{
    return m_params.text;
}


/**
 * Donne l'alignement du texte.
 *
 * \return Alignement du texte.
 *
 * \sa CLabel::setAlign
 ******************************/

THorizontalAlignment CLabel::getTextAlignment() const
{
    return m_align;
}


/**
 * Donne la couleur du texte.
 *
 * \return Couleur du texte.
 *
 * \sa CLabel::setColor
 ******************************/

CColor CLabel::getTextColor() const
{
    return m_params.color;
}


/**
 * Donne la police du texte.
 *
 * \return Police du texte.
 *
 * \sa CLabel::setFont
 ******************************/

CString CLabel::getTextFont() const
{
    return Game::fontManager->getFontName(m_params.font);
}


/**
 * Donne la taille du texte.
 *
 * \return Taille du texte.
 *
 * \sa CLabel::setFontSize
 ******************************/

int CLabel::getFontSize() const
{
    return m_params.size;
}


/**
 * Modifie le texte du label.
 *
 * \param text Texte du label.
 *
 * \sa CLabel::getText
 ******************************/

void CLabel::setText(const CString& text)
{
    m_params.text = text;

    // On modifie les dimensions minimales du label
    TVector2UI minsize = Game::fontManager->getTextSize(m_params);
    setMinSize(minsize.X, minsize.Y);
}


/**
 * Modifie l'alignement du texte.
 *
 * \param align Alignement du texte.
 *
 * \sa CLabel::getAlignement
 ******************************/

void CLabel::setTextAlignment(THorizontalAlignment align)
{
    m_align = align;
}


/**
 * Modifie la couleur du texte.
 *
 * \param color Couleur du texte.
 *
 * \sa CLabel::getColor
 ******************************/

void CLabel::setTextColor(const CColor& color)
{
    m_params.color = color;
}


/**
 * Modifie la police du texte.
 *
 * \param font Police du texte.
 *
 * \sa CLabel::getFont
 ******************************/

void CLabel::setTextFont(const CString& font)
{
    m_params.font = Game::fontManager->getFontId(font);

    // On modifie les dimensions minimales du label
    TVector2UI minsize = Game::fontManager->getTextSize(m_params);
    setMinSize(minsize.X, minsize.Y);
}


/**
 * Modifie la taille du texte.
 *
 * \param fontSize Taille du texte (entre 8 et 72).
 *
 * \sa CLabel::getFontSize
 ******************************/

void CLabel::setFontSize(int fontSize)
{
    if (fontSize >= 8 && fontSize <= 72)
    {
        m_params.size = fontSize;

        // On modifie les dimensions minimales du label
        TVector2UI minsize = Game::fontManager->getTextSize(m_params);
        setMinSize(minsize.X, minsize.Y);
    }
}


/**
 * Dessine le texte.
 ******************************/

void CLabel::draw()
{
    m_params.rec = CRectangle(getX(), getY(), getWidth(), getHeight());

    // Affichage du texte
    switch (m_align)
    {
        default:
        case AlignLeft:
            break;

        case AlignCenter:
        {
            TVector2UI txtdim = Game::fontManager->getTextSize(m_params);
            m_params.rec.setX(m_params.rec.getX() + (getWidth() - txtdim.X) / 2);
            break;
        }

        case AlignRight:
        {
            TVector2UI txtdim = Game::fontManager->getTextSize(m_params);
            m_params.rec.setX(m_params.rec.getX() + getWidth() - txtdim.X);
            break;
        }
    }

    Game::guiEngine->drawText(m_params);
}

} // Namespace Ted

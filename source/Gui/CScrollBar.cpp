/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CScrollBar.cpp
 * \date 14/03/2010 Création de la classe GuiScrollBar.
 * \date 14/07/2010 Affichage de la barre.
 * \date 07/11/2010 Ajout des boutons pour monter et descendre l'ascenseur, des
 * \date 07/11/2010 inputs, et de la gestion des évènements de la souris.
 * \date 07/11/2010 Ajout du paramètre single_step.
 * \date 22/01/2011 On peut déplacer la barre avec la souris.
 * \date 30/04/2011 Amélioration du déplacement avec la souris.
 * \date 29/05/2011 La classe est renommée en CScrollBar.
 * \date 03/05/2012 Correction d'un bug lors du déplacement à la souris.
 * \date 03/05/2012 La barre se déplace lorsqu'un bouton reste enfoncé.
 * \date 15/06/2014 Correction d'erreurs dans le calcul de la position et des dimensions de la barre.
 * \date 15/06/2014 Affichage de triangles dans les boutons.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CScrollBar.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CPushButton.hpp"
#include "Core/Events.hpp"
#include "Core/Time.hpp"
#include "Core/CApplication.hpp"
#include "Core/Maths/CRectangle.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

const int ScrollBarSize = 14; ///< Largeur par défaut en pixels.
const int ScrollBarNoMove = -32768;
const unsigned int ScrollBarTimeBeforeRepeat = 500; ///< Durée avant de répéter la modification de la valeur après qu'un des boutons soit enfoncé.
const unsigned int ScrollBarTimeRepeat       = 100; ///< Durée entre deux répétitions de la modification de la valeur si un bouton est enfoncé.
const int ScrollBarDistEndMove = 140; /**< Lorsqu'on glisse la barre de
                                        *  l'ascenseur et que le curseur
                                        *  s'éloigne trop, la barre revient
                                        *  à sa position initiale.
                                        */


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CScrollBar::CScrollBar(IWidget * parent) :
IWidget      (parent),
m_value      (0),
m_valueTmp   (0),
m_total      (0),
m_singleStep (0),
m_step       (0),
m_clic       (ScrollBarNoMove),
m_timeLast   (getElapsedTime()),
m_timeRepeat (ScrollBarTimeBeforeRepeat),
m_vertical   (true),
m_bup        (nullptr),
m_bdw        (nullptr)
{
    setSize(ScrollBarSize, ScrollBarSize);

    m_bup = new CPushButton(CString::fromUTF8("▲"), this);
    T_ASSERT(m_bup != nullptr);

    m_bup->setMinSize(ScrollBarSize, ScrollBarSize);
    m_bup->setMaxSize(ScrollBarSize, ScrollBarSize);
    m_bup->onClicked.connect(sigc::mem_fun(this, &CScrollBar::stepUp));

    m_bdw = new CPushButton(CString::fromUTF8("▼"), this);
    T_ASSERT(m_bdw != nullptr);

    m_bdw->setMinSize(ScrollBarSize, ScrollBarSize);
    m_bdw->setMaxSize(ScrollBarSize, ScrollBarSize);
    m_bdw->onClicked.connect(sigc::mem_fun(this, &CScrollBar::stepDown));
}


/**
 * Destructeur.
 ******************************/

CScrollBar::~CScrollBar()
{
    delete m_bup;
    delete m_bdw;
}


/**
 * Donne le numéro de la ligne affichée.
 *
 * \return Numéro de la ligne affichée.
 *
 * \sa CScrollBar::setValue
 ******************************/

int CScrollBar::getValue() const
{
    return m_value;
}


/**
 * Donne la valeur totale.
 *
 * \return Valeur totale.
 *
 * \sa CScrollBar::setTotal
 ******************************/

int CScrollBar::getTotal() const
{
    return m_total;
}


/**
 * Donne la taille d'une ligne.
 *
 * \return Taille d'une ligne.
 *
 * \sa CScrollBar::setSingleStep
 ******************************/

int CScrollBar::getSingleStep() const
{
    return m_singleStep;
}


/**
 * Donne le nombre de lignes d'une page.
 *
 * \return Nombre de lignes.
 *
 * \sa CScrollBar::setStep
 ******************************/

int CScrollBar::getStep() const
{
    return m_step;
}


/**
 * Indique si la barre est verticale.
 *
 * \return Booléen.
 *
 * \sa CScrollBar::isHorizontal
 * \sa CScrollBar::setVertical
 ******************************/

bool CScrollBar::isVertical() const
{
    return m_vertical;
}


/**
 * Indique si la barre est horizontale.
 *
 * \return Booléen.
 *
 * \sa CScrollBar::isVertical
 * \sa CScrollBar::setHorizontal
 ******************************/

bool CScrollBar::isHorizontal() const
{
    return !m_vertical;
}


/**
 * Modifie la valeur affichée.
 *
 * \param value Nouvelle valeur (doit être positive).
 *
 * \sa CScrollBar::getValue
 ******************************/

void CScrollBar::setValue(int value)
{
    if (value < 0)
        return;

    const int oldValue = m_value;
    const int max = getMax();

    if (value > max)
        m_value = max;
    else
        m_value = value;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Modifie la valeur totale.
 *
 * \param total Valeur totale (doit être positive).
 *
 * \sa CScrollBar::getTotal
 ******************************/

void CScrollBar::setTotal(int total)
{
    if (total < 0 || total == m_total)
        return;

    m_total = total;

    const int max = getMax();

    if (m_value > max)
    {
        m_value = max;
        onValueChange(m_value);
    }
}


/**
 * Modifie la taille d'une ligne.
 *
 * \param step Taille d'une ligne (doit être positive).
 *
 * \sa CScrollBar::setSingleStep
 ******************************/

void CScrollBar::setSingleStep(int step)
{
    if (step < 0)
        m_singleStep = 0;
    else
        m_singleStep = step;
}


/**
 * Modifie le nombre de lignes par page.
 *
 * \param step Nombre de lignes (doit être positif).
 *
 * \sa CScrollBar::getStep
 ******************************/

void CScrollBar::setStep(int step)
{
    if (step < 0)
    {
        m_step = 0;
    }
    else
    {
        m_step = step;
    }

    const int max = getMax();

    if (m_value > max)
    {
        m_value = max;
        onValueChange(m_value);
    }
}


/**
 * Modifie la valeur de vertical.
 *
 * \param vertical Indique si la barre doit être verticale.
 *
 * \sa CScrollBar::setHorizontal
 * \sa CScrollBar::isVertical
 ******************************/

void CScrollBar::setVertical(bool vertical)
{
    if (m_vertical != vertical)
    {
        m_vertical = vertical;

        if (m_vertical)
        {
            m_bup->setText(CString::fromUTF8("▲"));
            m_bdw->setText(CString::fromUTF8("▼"));
        }
        else
        {
            m_bup->setText(CString::fromUTF8("◄"));
            m_bdw->setText(CString::fromUTF8("►"));
        }
    }
}


/**
 * Modifie la valeur de vertical.
 *
 * \param horizontal Indique si la barre doit être horizontale.
 *
 * \sa CScrollBar::setVertical
 * \sa CScrollBar::isHorizontal
 ******************************/

void CScrollBar::setHorizontal(bool horizontal)
{
    setVertical(!horizontal);
}


/**
 * Donne la valeur maximale possible.
 *
 * \return Valeur maximale.
 ******************************/

int CScrollBar::getMax() const
{
    if (m_total <= m_step)
    {
        return 0;
    }

    return (m_total - m_step);
}


/**
 * Dessine la barre.
 *
 * \todo Déplacer la gestion de la position et de la taille des boutons dans
 *       les méthodes setWidth et setHeight.
 ******************************/

void CScrollBar::draw()
{
    T_ASSERT(m_bup != nullptr);
    T_ASSERT(m_bdw != nullptr);

    const int tmp_x = getX();
    const int tmp_y = getY();

    const float tmp = (m_total > 0 ? 1.0f / m_total : 0.0f); // Inverse de la longueur totale
    int buttonDim = ScrollBarSize;
    const int barSize = computeBarSize();

    // Barre verticale
    if (m_vertical)
    {
        if (m_height < ScrollBarSize + ScrollBarSize)
        {
            buttonDim = m_height / 2;
            m_bup->setHeight(buttonDim);
            m_bdw->setHeight(buttonDim);
        }

        m_bdw->setY(m_height - buttonDim);

        // Boutons
        m_bup->draw();
        m_bdw->draw();

        // Fond de la barre
        Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y + buttonDim, m_width - 1, m_height - buttonDim - buttonDim),
                                       CColor(220, 220, 220));
        Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y + buttonDim, m_width, m_height - buttonDim - buttonDim),
                                    CColor(28, 38, 60));

        // Ascenseur
        if (barSize > 0)
        {
            // Déplacement de la barre à la souris
            if (m_clic != ScrollBarNoMove && CApplication::getApp()->getButtonState(MouseButtonLeft))
            {
                m_value = m_valueTmp;

                const TVector2I cursor_position = CApplication::getApp()->getCursorPosition();
                const int cursor = cursor_position.X - tmp_x;

                if (cursor > -ScrollBarDistEndMove && cursor < static_cast<int>(m_width) + ScrollBarDistEndMove)
                {
                    int rel = static_cast<int>(static_cast<float>(cursor_position.Y - m_clic) *
                                               static_cast<float>(m_total) /
                                               static_cast<float>(m_height - buttonDim - buttonDim));

                         if (m_value + rel <= 0)        m_value = 0;
                    else if (m_value + rel >= getMax()) m_value = getMax();
                    else                                m_value += rel;
                }

                Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1,
                                                          tmp_y + buttonDim + (m_height - buttonDim - buttonDim) * m_value * tmp,
                                                          m_width - 2, barSize),
                                               CColor(120, 220, 0));
            }
            else
            {
                const unsigned int time = getElapsedTime();
                unsigned int frame = time - m_timeLast;
                m_timeLast = time;

                if (m_bup->isDown())
                {
                    while (frame >= m_timeRepeat)
                    {
                        frame -= m_timeRepeat;
                        m_timeRepeat = ScrollBarTimeRepeat;
                        stepUp();
                    }

                    m_timeRepeat -= frame;
                }
                else if (m_bdw->isDown())
                {
                    while (frame >= m_timeRepeat)
                    {
                        frame -= m_timeRepeat;
                        m_timeRepeat = ScrollBarTimeRepeat;
                        stepDown();
                    }

                    m_timeRepeat -= frame;
                }
                else
                {
                    m_timeRepeat = ScrollBarTimeBeforeRepeat;
                }

                Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1,
                                                          tmp_y + buttonDim + (m_height - buttonDim - buttonDim) * m_value * tmp,
                                                          m_width - 2, barSize),
                                               CColor(156, 255, 0));
            }
        }
    }
    // Barre horizontale
    else
    {
        if (m_width < ScrollBarSize * 2)
        {
            buttonDim = m_height / 2;
            m_bup->setWidth(buttonDim);
            m_bdw->setWidth(buttonDim);
        }

        m_bdw->setX(m_width - buttonDim);

        // Boutons
        m_bup->draw();
        m_bdw->draw();

        // Fond de la barre
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + buttonDim, tmp_y, m_width - buttonDim - buttonDim, m_height - 1),
                                       CColor(220, 220, 220));
        Game::guiEngine->drawBorder(CRectangle(tmp_x + buttonDim - 1, tmp_y, m_width - buttonDim - buttonDim + 2, m_height),
                                    CColor(28, 38, 60));

        // Ascenseur
        if (barSize > 0)
        {
            // Déplacement de la barre à la souris
            if (m_clic != ScrollBarNoMove && CApplication::getApp()->getButtonState(MouseButtonLeft))
            {
                m_value = m_valueTmp;

                const TVector2I cursor_position = CApplication::getApp()->getCursorPosition();
                const int cursor = cursor_position.Y - tmp_y;

                if (cursor > -ScrollBarDistEndMove && cursor < static_cast<int>(m_height) + ScrollBarDistEndMove)
                {
                    int rel = static_cast<int>(static_cast<float>(cursor_position.X - m_clic) *
                                               static_cast<float>(m_total) /
                                               static_cast<float>(m_width - buttonDim - buttonDim));

                         if (m_value + rel < 0)        m_value = 0;
                    else if (m_value + rel > getMax()) m_value = getMax();
                    else                               m_value += rel;
                }

                Game::guiEngine->drawRectangle(CRectangle(tmp_x + buttonDim + (m_width - buttonDim - buttonDim) * m_value * tmp,
                                                          tmp_y, barSize, m_height - 1),
                                               CColor(120, 220, 0));
            }
            else
            {
                const unsigned int time = getElapsedTime();
                unsigned int frame = time - m_timeLast;
                m_timeLast = time;

                if (m_bup->isDown())
                {
                    while (frame >= m_timeRepeat)
                    {
                        frame -= m_timeRepeat;
                        m_timeRepeat = ScrollBarTimeRepeat;
                        stepUp();
                    }

                    m_timeRepeat -= frame;
                }
                else if (m_bdw->isDown())
                {
                    while (frame >= m_timeRepeat)
                    {
                        frame -= m_timeRepeat;
                        m_timeRepeat = ScrollBarTimeRepeat;
                        stepDown();
                    }

                    m_timeRepeat -= frame;
                }
                else
                {
                    m_timeRepeat = ScrollBarTimeBeforeRepeat;
                }

                Game::guiEngine->drawRectangle(CRectangle(tmp_x + buttonDim + (m_width - buttonDim - buttonDim) * m_value * tmp,
                                                          tmp_y, barSize, m_height - 1),
                                               CColor(156, 255, 0));
            }
        }
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CScrollBar::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    T_ASSERT(m_bup != nullptr);
    T_ASSERT(m_bdw != nullptr);

    const int tmp_x = getX();
    const int tmp_y = getY();

    // Bouton pour descendre
    int bdw_x = m_bdw->getX();
    int bdw_y = m_bdw->getY();

    if (static_cast<int>(event.x) >= bdw_x &&
        static_cast<int>(event.x) <= bdw_x + static_cast<int>(m_bdw->getWidth()) &&
        static_cast<int>(event.y) >= bdw_y &&
        static_cast<int>(event.y) <= bdw_y + static_cast<int>(m_bdw->getHeight()))
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_bdw);
            }

            Game::guiEngine->setFocus(m_bdw);
        }

        Game::guiEngine->setPointed(m_bdw);

        m_bdw->onEvent(event);

        if (event.type != MoveMouse)
        {
            m_clic = ScrollBarNoMove;
        }

        return;
    }


    // Bouton pour monter
    int bup_x = m_bup->getX();
    int bup_y = m_bup->getY();

    if (static_cast<int>(event.x) >= bup_x &&
        static_cast<int>(event.x) <= bup_x + static_cast<int>(m_bup->getWidth()) &&
        static_cast<int>(event.y) >= bup_y &&
        static_cast<int>(event.y) <= bup_y + static_cast<int>(m_bup->getHeight()))
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_bup);
            }

            Game::guiEngine->setFocus(m_bup);
        }

        Game::guiEngine->setPointed(m_bup);

        m_bup->onEvent(event);

        if (event.type != MoveMouse)
        {
            m_clic = ScrollBarNoMove;
        }

        return;
    }


    if (event.type == ButtonPressed && m_total > 0)
    {
        const float tmp = 1.0f / m_total; // Inverse de la longueur totale

        if (m_vertical)
        {
            const int buttonDim = m_bup->getHeight();

            // Au-dessus de la barre
            if (static_cast<int>(event.y) > tmp_y + buttonDim &&
                static_cast<int>(event.y) < tmp_y + buttonDim + (m_height - buttonDim - buttonDim) * m_value * tmp)
            {
                pageUp();
                m_clic = ScrollBarNoMove;
            }
            // En-dessous de la barre
            else if (m_value < getMax() &&
                     static_cast<int>(event.y) > tmp_y + buttonDim + (m_height - buttonDim - buttonDim) * tmp * (m_value + m_step))
            {
                pageDown();
                m_clic = ScrollBarNoMove;
            }
            // Sur la barre
            else
            {
                m_clic = event.y;
                m_valueTmp = m_value; // Sauvegarde de l'ancienne valeur
            }
        }
        else
        {
            const int buttonDim = m_bup->getWidth();

            // À gauche de la barre
            if (static_cast<int>(event.x) > tmp_x + buttonDim &&
                static_cast<int>(event.x) < tmp_x + buttonDim + (m_width - buttonDim - buttonDim) * m_value * tmp)
            {
                pageUp();
                m_clic = ScrollBarNoMove;
            }
            // À droite de la barre
            else if (m_value < getMax() &&
                     static_cast<int>(event.x) > tmp_x + buttonDim + (m_width - buttonDim - buttonDim) * tmp * (m_value + m_step))
            {
                pageDown();
                m_clic = ScrollBarNoMove;
            }
            // Sur la barre
            else
            {
                m_clic = event.x;
                m_valueTmp = m_value; // Sauvegarde de l'ancienne valeur
            }
        }
    }
    else if (event.type == ButtonReleased)
    {
        m_clic = ScrollBarNoMove;
    }
}


/**
 * Fait monter l'ascenseur d'un pas.
 *
 * \sa CScrollBar::StepDown
 ******************************/

void CScrollBar::stepUp()
{
    const int oldValue = m_value;

    if (m_value <= m_singleStep)
        m_value = 0;
    else
        m_value -= m_singleStep;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Fait descendre l'ascenseur d'un pas.
 *
 * \sa CScrollBar::StepUp
 ******************************/

void CScrollBar::stepDown()
{
    const int oldValue = m_value;
    const int max = getMax();

    if (m_value + m_singleStep >= max)
        m_value = max;
    else
        m_value += m_singleStep;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Fait monter l'ascenseur d'une page.
 *
 * \sa CScrollBar::PageDown
 ******************************/

void CScrollBar::pageUp()
{
    const int oldValue = m_value;

    if (m_value <= m_step)
        m_value = 0;
    else
        m_value -= m_step;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Fait descendre l'ascenseur d'une page.
 *
 * \sa CScrollBar::PageUp
 ******************************/

void CScrollBar::pageDown()
{
    const int oldValue = m_value;
    const int max = getMax();

    if (m_value + m_step >= max)
        m_value = max;
    else
        m_value += m_step;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Donne la taille de l'ascenseur en pixels.
 *
 * \return Taille de l'ascenseur.
 ******************************/

int CScrollBar::computeBarSize() const
{
    float s;

    if (m_step >= m_total)
    {
        s = 1.0;
    }
    else
    {
        s = static_cast<float>(m_step) / m_total;
    }

    // Barre verticale
    if (m_vertical)
    {
        int buttonDim = (m_height < ScrollBarSize * 2 ? m_height / 2 : ScrollBarSize);
        s *= (m_height - buttonDim - buttonDim);
    }
    // Barre horizontale
    else
    {
        int buttonDim = (m_width < ScrollBarSize * 2 ? m_height / 2 : ScrollBarSize);
        s *= (m_width - buttonDim - buttonDim);
    }

    return static_cast<int>(s);
}

} // Namespace Ted

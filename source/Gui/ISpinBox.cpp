/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ISpinBox.cpp
 * \date 24/02/2010 Création de la classe GuiBaseSpinBox.
 * \date 08/07/2010 Correction d'un bug avec les boutons.
 * \date 10/07/2010 Correction d'un problème dans les dimensions du widget.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 26/01/2011 Lorsque le texte est modifié, la valeur est changée.
 * \date 29/05/2011 La classe est renommée en ISpinBox.
 * \date 01/05/2012 La valeur est modifiée lorsqu'un bouton reste enfoncé.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/ISpinBox.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"
#include "Core/Time.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

const int SpinBoxButtonWidth               =  20; ///< Largeur des boutons en pixels.
const unsigned int SpinBoxTimeBeforeRepeat = 500; ///< Durée avant de répéter la modification de la valeur après qu'un des boutons soit enfoncé.
const unsigned int SpinBoxTimeRepeat       = 100; ///< Durée entre deux répétitions de la modification de la valeur si un bouton est enfoncé.


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

ISpinBox::ISpinBox(IWidget * parent) :
IWidget       (parent),
m_timeLast   (getElapsedTime()),
m_timeRepeat (SpinBoxTimeBeforeRepeat),
m_wrapping    (true),
m_prefix      (),
m_suffix      (),
m_line        (nullptr),
m_buttonUp      (nullptr),
m_buttonDown    (nullptr)
{
    m_line = new CLineEdit(this);
    T_ASSERT(m_line != nullptr);

    int minh = m_line->getMinHeight() / 2;
    int maxh = m_line->getMaxHeight();

    // Boutons
    m_buttonUp = new CPushButton("+", this);
    T_ASSERT(m_buttonUp != nullptr);

    m_buttonUp->setX(m_line->getWidth());
    m_buttonUp->setMinSize(SpinBoxButtonWidth, minh);
    m_buttonUp->setMaxSize(SpinBoxButtonWidth, maxh / 2);
    m_buttonUp->onClicked.connect(sigc::mem_fun(this, &ISpinBox::stepUp));

    m_buttonDown = new CPushButton("-", this);
    T_ASSERT(m_buttonDown != nullptr);

    m_buttonDown->setPosition(m_line->getWidth() - 1, minh);
    m_buttonDown->setMinSize(SpinBoxButtonWidth, minh);
    m_buttonDown->setMaxSize(SpinBoxButtonWidth, maxh / 2);
    m_buttonDown->onClicked.connect(sigc::mem_fun(this, &ISpinBox::stepDown));

    m_line->onTextChange.connect(sigc::mem_fun(this, &ISpinBox::updateValue));

    setMaxHeight(maxh);
}


/**
 * Destructeur.
 ******************************/

ISpinBox::~ISpinBox()
{
    delete m_line;
    delete m_buttonUp;
    delete m_buttonDown;
}


/**
 * Rend la spinbox circulaire ou non.
 *
 * \param wrapping Indique si la spinbox est circulaire.
 *
 * \sa ISpinBox::getWrapping
 ******************************/

void ISpinBox::setWrapping(bool wrapping)
{
    m_wrapping = wrapping;
}


/**
 * Modifie le préfixe utilisé dans le champ texte.
 *
 * \param prefix Préfixe.
 *
 * \sa ISpinBox::getPrefix
 ******************************/

void ISpinBox::setPrefix(const CString& prefix)
{
    m_prefix = prefix;
}


/**
 * Modifie le suffixe utilisé dans le champ texte.
 *
 * \param suffix Suffixe.
 *
 * \sa ISpinBox::getSuffix
 ******************************/

void ISpinBox::setSuffix(const CString& suffix)
{
    m_suffix = suffix;
}


/**
 * Modifie la largeur du widget.
 *
 * \param width Largeur du widget en pixels.
 ******************************/

void ISpinBox::setWidth(int width)
{
    if (width < 0)
        return;

    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_buttonUp != nullptr);
    T_ASSERT(m_buttonDown != nullptr);

         if (width < m_minWidth) m_width = m_minWidth;
    else if (width > m_maxWidth) m_width = m_maxWidth;
    else                         m_width = width;

    m_line->setWidth(m_width - SpinBoxButtonWidth);
    const unsigned int tmpw = m_line->getWidth() - 1;
    m_buttonUp->setX(tmpw);
    m_buttonDown->setX(tmpw);
}


/**
 * Dessine la spinbox.
 ******************************/

void ISpinBox::draw()
{
    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_buttonUp != nullptr);
    T_ASSERT(m_buttonDown != nullptr);

    const unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_timeLast;
    m_timeLast = time;

    if (m_buttonUp->isDown())
    {
        while (frameTime >= m_timeRepeat)
        {
            frameTime -= m_timeRepeat;
            m_timeRepeat = SpinBoxTimeRepeat;
            stepUp();
        }

        m_timeRepeat -= frameTime;
    }
    else if (m_buttonDown->isDown())
    {
        while (frameTime >= m_timeRepeat)
        {
            frameTime -= m_timeRepeat;
            m_timeRepeat = SpinBoxTimeRepeat;
            stepDown();
        }

        m_timeRepeat -= frameTime;
    }
    else
    {
        m_timeRepeat = SpinBoxTimeBeforeRepeat;
    }

    // Affichage des widgets
    m_line->draw();
    m_buttonUp->draw();
    m_buttonDown->draw();
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void ISpinBox::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_buttonUp != nullptr);
    T_ASSERT(m_buttonDown != nullptr);

    int tmp_x = m_line->getX();
    int tmp_y = m_line->getY();

    if (static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + m_line->getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + m_line->getHeight())
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_line);
            }

            Game::guiEngine->setFocus(m_line);
        }

        Game::guiEngine->setPointed(m_line);

        m_line->onEvent(event);
        return;
    }

    tmp_x = m_buttonUp->getX();
    tmp_y = m_buttonUp->getY();

    if (static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + m_buttonUp->getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + m_buttonUp->getHeight())
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_buttonUp);
            }

            Game::guiEngine->setFocus(m_buttonUp);
        }

        Game::guiEngine->setPointed(m_buttonUp);

        m_buttonUp->onEvent(event);
        return;
    }

    tmp_x = m_buttonDown->getX();
    tmp_y = m_buttonDown->getY();

    if (static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + m_buttonDown->getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + m_buttonDown->getHeight())
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_buttonDown);
            }

            Game::guiEngine->setFocus(m_buttonDown);
        }

        Game::guiEngine->setPointed(m_buttonDown);

        m_buttonDown->onEvent(event);
        return;
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CSpinBox.cpp
 * \date 28/05/2009 Création de la classe GuiSpinBox.
 * \date 08/07/2010 Le texte du champ est modifié.
 * \date 09/07/2010 Modification des valeurs maximales et minimales par défaut.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en CSpinBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>

#include "Gui/CSpinBox.hpp"
#include "Gui/CLineEdit.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param value  Valeur par défaut.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CSpinBox::CSpinBox(int value, IWidget * parent) :
ISpinBox (parent),
m_value  (value),
m_min    (0),
m_max    (99),
m_step   (1)
{
    m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CSpinBox::CSpinBox(IWidget * parent) :
ISpinBox (parent),
m_value  (0),
m_min    (0),
m_max    (99),
m_step   (1)
{
    m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);
}


/**
 * Donne valeur de la spinbox.
 *
 * \return Valeur de la spinbox.
 *
 * \sa CSpinBox::setValue
 ******************************/

int CSpinBox::getValue() const
{
    return m_value;
}


/**
 * Donne la valeur minimale de la spinbox.
 *
 * \return Valeur minimale.
 *
 * \sa CSpinBox::setMinimum
 ******************************/

int CSpinBox::getMinimum() const
{
    return m_min;
}


/**
 * Donne la valeur maximale de la spinbox.
 *
 * \return Valeur maximale.
 *
 * \sa CSpinBox::setMaximum
 ******************************/

int CSpinBox::getMaximum() const
{
    return m_max;
}


/**
 * Accesseur pour step.
 *
 * \return Valeur à ajouter à chaque incrémentation.
 *
 * \sa CSpinBox::setStep
 ******************************/

int CSpinBox::getStep() const
{
    return m_step;
}


/**
 * Modifie la valeur de la spinbox.
 *
 * \param value Valeur de la spinbox.
 *
 * \sa CSpinBox::getValue
 ******************************/

void CSpinBox::setValue(int value)
{
    T_ASSERT(m_line != nullptr);

    const int old_value = m_value;

         if (value < m_min) m_value = m_min;
    else if (value > m_max) m_value = m_max;

    if (old_value != m_value)
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);
        onChange(m_value);
    }
}

/**
 * Mutateur pour min.
 *
 * \param minimum Valeur minimale.
 *
 * \sa CSpinBox::getMinimum
 ******************************/

void CSpinBox::setMinimum(int minimum)
{
    T_ASSERT(m_line != nullptr);

    if (minimum < m_max)
    {
        m_min = minimum;
    }

    if (m_value < m_min)
    {
        m_value = m_min;
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Mutateur pour max.
 *
 * \param maximum Valeur maximale.
 *
 * \sa CSpinBox::getMaximum
 ******************************/

void CSpinBox::setMaximum(int maximum)
{
    T_ASSERT(m_line != nullptr);

    if (maximum > m_min)
    {
        m_max = maximum;
    }

    if (m_value > m_max)
    {
        m_value = m_max;
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Modifie les valeurs minimales et maximales.
 *
 * \param minimum Valeur minimale.
 * \param maximum Valeur maximale.
 *
 * \sa CSpinBox::getMinimum
 * \sa CSpinBox::getMaximum
 ******************************/

void CSpinBox::setRange(int minimum, int maximum)
{
    T_ASSERT(m_line != nullptr);

    if (minimum < maximum)
    {
        m_min = minimum;
        m_max = maximum;
    }

    if (m_value > m_max)
    {
        m_value = m_max;
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
    else if (m_value < m_min)
    {
        m_value = m_min;
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Mutateur pour step.
 *
 * \param step Valeur à ajouter à chaque incrémentation.
 *
 * \sa CSpinBox::getStep
 ******************************/

void CSpinBox::setStep(int step)
{
    m_step = step;
}


/**
 * Incrémente la spinbox.
 ******************************/

void CSpinBox::stepUp()
{
    T_ASSERT(m_line != nullptr);

    int old_value = m_value;

    if (m_value + m_step > m_max)
    {
        m_value = (m_wrapping ? m_min : m_max);
    }
    else
    {
        m_value += m_step;
    }

    if (old_value != m_value)
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Décrémente la spinbox.
 ******************************/

void CSpinBox::stepDown()
{
    T_ASSERT(m_line != nullptr);

    int old_value = m_value;

    if (m_value - m_step < m_min)
    {
        m_value = (m_wrapping ? m_max : m_min);
    }
    else
    {
        m_value -= m_step;
    }

    if (old_value != m_value)
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Méthode appelée lorsque le texte du champ est modifié.
 ******************************/

void CSpinBox::updateValue()
{
    T_ASSERT(m_line != nullptr);

    bool ok;
    int32_t value = m_line->getText().toInt32(&ok);

    if (!ok)
    {
        value = m_min;
    }
    else
    {
        if (value < m_min) value = m_min;
        else if (value > m_max) value = m_max;
    }

    if (value != m_value)
    {
        m_value = value;

        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);

        onChange(m_value);
    }
    else
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value) + m_suffix);
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CSpacer.cpp
 * \date 07/03/2010 Création de la classe GuiSpacer.
 * \date 29/05/2011 La classe est renommée en CSpacer.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CSpacer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CSpacer::CSpacer(IWidget * parent) :
IWidget (parent)
{ }


/**
 * Dessine le widget
 ******************************/

void CSpacer::draw()
{

}

} // Namespace Ted

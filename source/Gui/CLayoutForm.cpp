/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutForm.cpp
 * \date 15/07/2010 Création de la classe GuiLayoutForm.
 * \date 29/05/2011 La classe est renommée en CLayoutForm.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CLayoutForm.hpp"
#include "Gui/CLayoutGrid.hpp"
#include "Gui/CLabel.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLayoutForm::CLayoutForm(IWidget * parent) :
ILayout       (parent),
m_layout      (nullptr),
m_labelAlign (AlignMiddleLeft),
m_nbr_rows    (0)
{
    m_layout = new CLayoutGrid(this);
    T_ASSERT(m_layout != nullptr);
}


/**
 * Destructeur.
 ******************************/

CLayoutForm::~CLayoutForm()
{
    delete m_layout;
}


/**
 * Donne l'alignement des label.
 *
 * \return Alignement des label.
 *
 * \sa CLayoutForm::setLabelAlignement
 ******************************/

TAlignment CLayoutForm::getLabelAlignement() const
{
    return m_labelAlign;
}


/**
 * Donne les valeurs des marges internes.
 *
 * \return Marges internes de chaque case en pixels.
 *
 * \sa CLayoutForm::setPadding
 ******************************/

CMargins CLayoutForm::getPadding() const
{
    return m_padding;
}


/**
 * Donne les valeurs des marges extérieures.
 *
 * \return Marges extérieures en pixels.
 *
 * \sa CLayoutForm::setMargin
 ******************************/

CMargins CLayoutForm::getMargin() const
{
    return m_margin;
}


/**
 * Modifie l'alignement des label.
 *
 * \todo Modifier l'alignement des labels déjà insérés.
 *
 * \param align Alignement des label.
 *
 * \sa CLayoutForm::getLabelAlignement
 ******************************/

void CLayoutForm::setLabelAlignement(TAlignment align)
{
    m_labelAlign = align;
}


/**
 * Modifie la valeur de la marge interne.
 *
 * \param padding Marges internes de chaque case en pixels.
 *
 * \sa CLayoutForm::getPadding
 ******************************/

void CLayoutForm::setPadding(const CMargins& padding)
{
    T_ASSERT(m_layout != nullptr);
    m_layout->setPadding(padding);
}


/**
 * Modifie la valeur de la marge extérieure.
 *
 * \param margin Marges extérieures en pixels.
 *
 * \sa CLayoutForm::getMargin
 ******************************/

void CLayoutForm::setMargin(const CMargins& margin)
{
    T_ASSERT(m_layout != nullptr);
    m_layout->setMargin(margin);
}


/**
 * Ajoute une ligne au layout.
 *
 * \param label    Texte du label.
 * \param widget   Pointeur sur le widget à insérer dans la deuxième colonne.
 * \param position Numéro de la ligne (dernière ligne par défaut).
 ******************************/

void CLayoutForm::addRow(const CString& label, IWidget * widget, int position)
{
    CLabel * lbl = new CLabel(label);
    addRow(lbl, widget, position);
}


/**
 * Ajoute une ligne au layout.
 *
 * \param label    Pointeur sur le label à utiliser.
 * \param widget   Pointeur sur le widget à insérer dans la deuxième colonne.
 * \param position Numéro de la ligne (dernière ligne par défaut).
 ******************************/

void CLayoutForm::addRow(CLabel * label, IWidget * widget, int position)
{
    // Insertion à la fin
    if (position < 0)
    {
        position = m_nbr_rows;
    }

    m_layout->addChild(label, position, 0, m_labelAlign);
    m_layout->addChild(widget, position, 1);

    if (position >= m_nbr_rows)
    {
        m_nbr_rows = position + 1;
    }
}


/**
 * Donne le nombre de lignes du layout.
 *
 * \return Nombre de lignes.
 ******************************/

int CLayoutForm::getNumRows() const
{
    return m_nbr_rows;
}


/**
 * Dessine le layout.
 ******************************/

void CLayoutForm::draw()
{
    T_ASSERT(m_layout != nullptr);
    m_layout->draw();
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CLayoutForm::onEvent(const CMouseEvent& event)
{
    T_ASSERT(m_layout != nullptr);
    m_layout->onEvent(event);
}


/**
 * Calcule les dimensions du layout.
 ******************************/

void CLayoutForm::computeSize()
{
    T_ASSERT(m_layout != nullptr);
    m_layout->computeSize();
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWidgetDebugTextures.cpp
 * \date 13/06/2014 Création de la classe CWidgetDebugTextures.
 * \date 14/06/2014 Affichage des cases.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWidgetDebugTextures.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CTextureManager.hpp"


namespace Ted
{

const int TextureWidth  = 120;
const int TextureHeight = 120;
const int CaseWidth     = 130;
const int CaseHeight    = 165;
const int CaseMargin    = 5;


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWidgetDebugTextures::CWidgetDebugTextures(IWidget * parent) :
IWidget (parent)
{

}


/**
 * Dessine le widget.
 ******************************/

void CWidgetDebugTextures::draw()
{
    CTextureManager::TTextureInfoVector textures = Game::textureManager->getTextures();
    int numTextures = textures.size();

    const int width = getWidth();
    int numCol = width / (CaseWidth + 2 * CaseMargin);

    CRectangle region(0, 0, width, getHeight());

    if (numCol == 0)
    {
        numCol = 1;
    }

    int numRow = numTextures / numCol;

    if (numRow * numCol < numTextures)
    {
        ++numRow;
    }

    const int posX = getX();
    const int posY = getY();

    const int offsetTexture = (CaseWidth - TextureWidth) / 2;
    const float textureRatio = TextureWidth / TextureHeight;

    int row = 0;
    int col = 0;

    int colWidth = (CaseWidth + 2 * CaseMargin);
    int rowHeight = (CaseHeight + 2 * CaseMargin);
    int skipLines = region.getX() / rowHeight;
    int skipCases = skipLines * numCol;

    for (int c = 0; c < numTextures; ++c)
    {
        if (c < skipCases)
        {
            continue;
        }

        if (rowHeight * (c - skipCases) / numCol > getHeight())
        {
            continue;
        }

        // Fond de la case
        Game::guiEngine->drawRectangle(CRectangle(posX - region.getX() + CaseMargin + col * colWidth,
                                                  posY - region.getY() + CaseMargin + row * rowHeight,
                                                  CaseWidth, CaseHeight),
                                       CColor(49, 60, 83, 210));

        // Texture
        Game::guiEngine->drawRectangle(CRectangle(posX - region.getX() + CaseMargin + offsetTexture + col * colWidth,
                                                  posY - region.getY() + CaseMargin + offsetTexture + row * rowHeight,
                                                  TextureWidth, TextureHeight),
                                       CColor::White);

        int textureWidth = textures[c].width;
        int textureHeight = textures[c].height;

        // Réduction de la taille de la texture
        if (textureWidth > TextureWidth || textureHeight > TextureHeight)
        {
            if (textureRatio > static_cast<float>(textures[c].width) / static_cast<float>(textures[c].height))
            {
                float div = static_cast<float>(textures[c].height) / static_cast<float>(TextureHeight);
                textureWidth = static_cast<float>(textures[c].width) / div;
                textureHeight = TextureHeight;
            }
            else
            {
                float div = static_cast<float>(textures[c].width) / static_cast<float>(TextureWidth);
                textureWidth = TextureWidth;
                textureHeight = static_cast<float>(textures[c].height) / div;
            }
        }

        Game::guiEngine->drawImage(CRectangle(posX - region.getX() + CaseMargin + offsetTexture + col * (CaseWidth + 2 * CaseMargin) + (TextureWidth - textureWidth) / 2,
                                              posY - region.getY() + CaseMargin + offsetTexture + row * (CaseHeight + 2 * CaseMargin) + (TextureHeight - textureHeight) / 2,
                                              textureWidth, textureHeight),
                                   textures[c].id);

        // Informations
        TTextParams params;
        params.color = CColor::White;
        params.font = Game::fontManager->getFontId("Verdana");
        params.size = 10;
        params.rec.setX(posX - region.getX() + CaseMargin + col * (CaseWidth + 2 * CaseMargin) + 5);
        params.rec.setY(posY - region.getY() + CaseMargin + row * (CaseHeight + 2 * CaseMargin) + offsetTexture + TextureHeight + 5);
        params.rec.setWidth(CaseWidth - 10);
        params.rec.setHeight(CaseHeight - TextureHeight - offsetTexture - 10);
        params.text = CString("%1\n%2 x %3").arg(textures[c].name).arg(textures[c].width).arg(textures[c].height);
        Game::guiEngine->drawText(params);

        if (col == numCol - 1)
        {
            col = 0;
            ++row;
        }
        else
        {
            ++col;
        }
    }
}

} // Namespace Ted

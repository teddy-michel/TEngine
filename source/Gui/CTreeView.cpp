/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CTreeView.cpp
 * \date 26/02/2010 Création de la classe GuiTreeView.
 * \date 18/01/2011 Héritage de GuiFrame.
 * \date 29/05/2011 La classe est renommée en CTreeView.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CTreeView.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CTreeView::CTreeView(IWidget * parent) :
IScrollArea (parent)
{ }


/**
 * Donne la racine de l'arbre.
 *
 * \return Pointeur sur la racine de l'arbre.
 *
 * \sa CTreeView::setHeadNode
 ******************************/

CTreeNode * CTreeView::getHeadNode() const
{
    return m_headnode;
}


/**
 * Donne l'élément sélectionné.
 *
 * \return Pointeur sur le nœud de l'arbre sélectionné.
 ******************************/

CTreeNode * CTreeView::getSelectedNode() const
{
    return m_selected;
}


/**
 * Modifie l'arbre.
 *
 * \param headnode Pointeur sur la racine de l'arbre.
 *
 * \sa CTreeView::getHeadNode
 ******************************/

void CTreeView::setHeadNode(CTreeNode * headnode)
{
    m_headnode = headnode;
}


/**
 * Dessine l'arbre.
 *
 * \todo Implémentation.
 ******************************/

void CTreeView::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    // Fond du champ
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::White);
    Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::Black);
}

} // Namespace Ted

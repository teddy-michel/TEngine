/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CComboBox.cpp
 * \date 02/03/2010 Création de la classe GuiComboBox.
 * \date 17/07/2010 Utilisation des assertions.
 * \date 04/01/2011 Utilisation d'une ListView.
 * \date 15/01/2011 Création de la méthode SelectItem.
 * \date 29/05/2011 La classe est renommée en CComboBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CGuiEngine.hpp"
#include "Gui/CComboBox.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CListView.hpp"
#include "Core/Events.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

const int ComboBoxMaxItems = 10; ///< Nombre maximal d'items affichés en même temps.


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CComboBox::CComboBox(IWidget * parent) :
IWidget     (parent),
m_show_list (false),
m_line      (nullptr),
m_button    (nullptr),
m_list      (nullptr)
{
    // Ligne de texte
    m_line = new CLineEdit( this);
    T_ASSERT(m_line != nullptr);
    m_line->setEditable(false);

    // Bouton
    m_button = new CPushButton("+", this);
    T_ASSERT(m_button != nullptr);

    m_list = new CListView(this);
    T_ASSERT(m_list != nullptr);

    int size = m_line->getMinHeight();

    m_button->setX(m_line->getWidth() - 1);
    m_button->setMinSize(size, size);
    m_button->setMaxSize(size, size);

    setMinSize(m_line->getMinWidth() + size - 1, size);
    setMaxHeight(m_minHeight);

    m_list->setY(size);
    m_list->setWidth(m_width);
    m_list->setPadding(4);

    m_button->onClicked.connect(sigc::mem_fun(this, &CComboBox::toggleList));
    m_list->onItemSelected.connect(sigc::mem_fun(this, &CComboBox::selectItem));
}


/**
 * Destructeur.
 ******************************/

CComboBox::~CComboBox()
{
    delete m_button;
    delete m_line;
    delete m_list;
}


/**
 * Donne le nombre d'items de la liste.
 *
 * \return Nombre d'items.
 ******************************/

int CComboBox::getNumItems() const
{
    T_ASSERT(m_list != nullptr);
    return m_list->getNumItems();
}


/**
 * Donne le numéro de l'item sélectionné.
 *
 * \return Numéro de l'item sélectionné.
 *
 * \sa CComboBox::setSelect
 ******************************/

int CComboBox::getSelect() const
{
    T_ASSERT(m_list != nullptr);
    return m_list->getCurrentItem();
}


/**
 * Donne la valeur de l'item sélectionné.
 *
 * \return Valeur de l'item sélectionné.
 ******************************/

CString CComboBox::getItemSelected() const
{
    T_ASSERT(m_list != nullptr);
    return m_list->getItem(m_list->getCurrentItem());
}


/**
 * Change le numéro de l'item sélectionné.
 * Si le numéro est trop grand, aucun item n'est sélectionné.
 *
 * \param select Numéro de l'item sélectionné.
 *
 * \sa CComboBox::getSelect
 ******************************/

void CComboBox::setSelect(int select)
{
    T_ASSERT(m_list != nullptr);
    m_list->setCurrentItem(select);
}


/**
 * Modifie la largeur du widget.
 *
 * \param width Largeur du widget en pixels.
 ******************************/

void CComboBox::setWidth(int width)
{
    if (width < 0)
        width = 0;

    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_list != nullptr);
    T_ASSERT(m_button != nullptr);

         if (width < m_minWidth) m_width = m_minWidth;
    else if (width > m_maxWidth) m_width = m_maxWidth;
    else                         m_width = width;

    m_line->setWidth(m_width - m_button->getWidth());
    m_button->setX(m_line->getWidth() - 1);

    m_list->setWidth(m_width);
}


/**
 * Modifie le type de liste : éditable ou non.
 *
 * \param editable Booléen indiquand si on peut modifier le texte.
 ******************************/

void CComboBox::setEditable(bool editable)
{
    m_line->setEditable(editable);
}


/**
 * Ajoute un item à la liste.
 *
 * \param item Valeur de l'item.
 * \param pos  Position de l'item (par défaut en dernier).
 ******************************/

void CComboBox::addItem(const CString& item, int pos)
{
    T_ASSERT(m_list != nullptr);
    m_list->addItem(item, pos);

    const int num_items = getNumItems();

    if (num_items <= ComboBoxMaxItems)
    {
        m_list->setHeight(num_items * m_list->getLineHeight());
    }
}


/**
 * Enlève un item de la liste.
 *
 * \param pos Position de l'item.
 ******************************/

void CComboBox::removeItem(int pos)
{
    T_ASSERT(m_list != nullptr);
    m_list->removeItem(pos);

    const int num_items = getNumItems();

    if (num_items <= ComboBoxMaxItems)
    {
        m_list->setHeight(num_items * m_list->getLineHeight());
    }
}


/**
 * Change la valeur du champ en sélectionnant un item de la liste.
 *
 * \param pos Position de l'item.
 ******************************/

void CComboBox::selectItem(int pos)
{
    T_ASSERT(m_list != nullptr);
    T_ASSERT(m_line != nullptr);

    CString val = m_list->getItem(pos);

    m_line->setText(val);

    // On masque la liste
    Game::guiEngine->setForeground(nullptr);
    m_show_list = false;

    onValueChange(val);
    onSelectedChange(pos);
}


/**
 * Supprime tous les items de la liste.
 ******************************/

void CComboBox::clearItems()
{
    T_ASSERT(m_list != nullptr);
    m_list->clear();
}


/**
 * Dessine le widget.
 ******************************/

void CComboBox::draw()
{
    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_button != nullptr);
    T_ASSERT(m_list != nullptr);

    m_line->draw();
    m_button->draw();

    if (m_show_list && !Game::guiEngine->isForeground(m_list))
    {
        m_show_list = false;
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CComboBox::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    T_ASSERT(m_line != nullptr);
    T_ASSERT(m_button != nullptr);

    int tmp_x = m_line->getX();
    int tmp_y = m_line->getY();

    // On transmet l'évènement à la ligne de texte
    if (static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + m_line->getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + m_line->getHeight())
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_line);
            }

            Game::guiEngine->setFocus(m_line);
        }

        Game::guiEngine->setPointed(m_line);

        m_line->onEvent(event);
        return;
    }

    tmp_x = m_button->getX();
    tmp_y = m_button->getY();

    // On transmet l'évènement au bouton
    if (static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= tmp_x + m_button->getWidth() &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= tmp_y + m_button->getHeight())
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_button);
            }

            if (m_show_list && !Game::guiEngine->isForeground(m_list))
            {
                Game::guiEngine->setForeground(m_list);
                Game::guiEngine->setFocus(m_list);
            }

            Game::guiEngine->setFocus(m_button);
        }

        Game::guiEngine->setPointed(m_button);

        m_button->onEvent(event);
        return;
    }
}


/**
 * Affiche ou masque la liste des items.
 ******************************/

void CComboBox::toggleList()
{
    if (m_show_list)
    {
        Game::guiEngine->setForeground(nullptr);
        m_show_list = false;
    }
    else
    {
        Game::guiEngine->setForeground(m_list);
        Game::guiEngine->setFocus(m_list);
        m_show_list = true;
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow_Options.cpp
 * \date 27/05/2009 Création de la classe GuiWindow_Options.
 * \date 08/07/2010 Ajout de la spin box et de la double box.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 19/01/2011 Test de GuiGroupBox.
 * \date 12/03/2011 Affichage de la liste des résolutions supportées.
 * \date 09/04/2011 Changement des dimensions de la fenêtre.
 * \date 29/05/2011 La classe est renommée en CWindow_Options.
 * \date 10/12/2011 On peut switcher en plein écran.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindow_Options.hpp"
#include "Gui/CLayoutTab.hpp"
#include "Gui/CLayoutHorizontal.hpp"
#include "Gui/CLayoutVertical.hpp"
#include "Gui/CComboBox.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CLabel.hpp"
#include "Gui/CRadioButton.hpp"
#include "Gui/CButtonGroup.hpp"
#include "Gui/CSpacer.hpp"
#include "Gui/CCheckBox.hpp"
#include "Gui/CDateTimeEdit.hpp"
#include "Gui/CSpinBox.hpp"
#include "Gui/CSlider.hpp"
#include "Gui/CDoubleBox.hpp"
#include "Gui/CGroupBox.hpp"
#include "Core/CApplication.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow_Options::CWindow_Options(IWidget * parent) :
CWindow ("Options", WindowCenter, parent)
{
    setMinSize(400, 400);

    CLayoutVertical * layout_win = new CLayoutVertical(this);
    layout_win->setMargin(CMargins(5));

    // Barre d'onglets
    CLayoutTab * tab = new CLayoutTab(layout_win);
    tab->setPadding(CMargins(5, 5, 5, 5));
    layout_win->addChild(tab);

    // Bouton Appliquer
    CPushButton * button_valid = new CPushButton("Appliquer", layout_win);
    button_valid->setMinSize(80, 20);
    button_valid->setMaxSize(150, 25);
    button_valid->onClicked.connect(sigc::mem_fun(this, &CWindow_Options::valid));

    // Bouton Fermer
    CPushButton * button_close = new CPushButton("Fermer", layout_win);
    button_close->setMinSize(80, 20);
    button_close->setMaxSize(150, 25);
    button_close->onClicked.connect(sigc::mem_fun(this, &CWindow::close));

    CSpacer * button_spacer = new CSpacer();
    button_spacer->setMinWidth(120);
    button_spacer->setMaxHeight(1);

    CLayoutHorizontal * layout_buttons = new CLayoutHorizontal(layout_win);
    layout_win->addChild(layout_buttons);
    layout_buttons->addChild(button_spacer);
    layout_buttons->addChild(button_valid);
    layout_buttons->addChild(button_close);

    // Onglet 1 : Affichage
    CLayoutVertical * layout = new CLayoutVertical(tab);
    layout->setPadding(CMargins(8));
    tab->addTab("Affichage", layout);

    // Liste des résolutions supportées
    CLayoutHorizontal * resol_layout = new CLayoutHorizontal(layout);
    layout->addChild(resol_layout);

    std::vector<TVector2UI> resolutions;
    CApplication::getSupportedResolutions(resolutions);

    m_resolutions = new CComboBox(layout);

    for (std::vector<TVector2UI>::const_iterator it = resolutions.begin(); it != resolutions.end(); ++it)
    {
        m_resolutions->addItem(CString::fromNumber(it->X) + " x " + CString::fromNumber(it->Y));
    }

    CLabel * resol_label = new CLabel(CString::fromUTF8("Résolution"));
    resol_label->setMaxHeight(m_resolutions->getMaxHeight());

    resol_layout->addChild(resol_label);
    resol_layout->addChild(m_resolutions);

    // Plein écran
    m_fullscreen = new CCheckBox(CString::fromUTF8("Plein écran"), layout);
    layout->addChild(m_fullscreen);


    // Onglet 2
    CLayoutVertical * layout2 = new CLayoutVertical(tab);
    layout2->setPadding(CMargins(5, 5, 5, 5));

    tab->addTab("Page 2", layout2);

    CComboBox * combo = new CComboBox(layout2);
    combo->addItem("Item 1");
    combo->addItem("Item 2");
    combo->addItem("Item 3");
    combo->addItem("Item 4");
    combo->addItem("Item 5");
    combo->addItem("Item 6");
    combo->addItem("Item 7");
    combo->addItem("Item 8");
    combo->addItem("Item 9");
    combo->addItem("Item 10");
    combo->addItem("Item 11");
    combo->addItem("Item 12");
    combo->addItem("Item 13");
    combo->addItem("Item 14");
    layout2->addChild(combo);

    CCheckBox * chbox = new CCheckBox(CString::fromUTF8("Case à cocher"));
    layout2->addChild(chbox);

    CDateTimeEdit * datetime = new CDateTimeEdit();
    layout2->addChild(datetime);

    CSlider * slider = new CSlider();
    slider->setStep(10);

    CSpinBox * sp1 = new CSpinBox();
    layout2->addChild(sp1);
    //sp1->onChange.connect(sigc::mem_fun(slider, &GuiSlider::setValue));
    //slider->onValueChange.connect(sigc::mem_fun(sp1, &GuiSpinBox::setValue));

    // GROUP BOX START
        CGroupBox * grb = new CGroupBox("GroupBox");
        layout2->addChild(grb);
        CLayoutVertical * layout3 = new CLayoutVertical();
        grb->setChild(layout3);

        CButtonGroup * gr = new CButtonGroup(grb);

        CRadioButton * radio1 = new CRadioButton("Bouton radio 1", grb);
        gr->addButton(radio1);
        layout3->addChild(radio1);

        CRadioButton * radio2 = new CRadioButton("Bouton radio 2", grb);
        gr->addButton(radio2);
        layout3->addChild(radio2);

        CDoubleBox * sp2 = new CDoubleBox(grb);
        layout3->addChild(sp2);
    // GROUP BOX END

    slider->setTotal(100);
    slider->setValue(20);
    layout2->addChild(slider);

    // Onglet 3
    CPushButton * button_test2 = new CPushButton("Test 2");
    button_test2->setMaxSize(300, 100);

    tab->addTab("Page 3", button_test2, AlignTopCenter);

    // Onglet 4
    CPushButton * button_test3 = new CPushButton("Test 3");

    tab->addTab("Page 4", button_test3);

    setLayout(layout_win); // On indique à la fenêtre d'utiliser ce widget comme layout
}


/**
 * Applique les changements de paramètres.
 ******************************/

void CWindow_Options::valid() const
{
    // Changement de résolution
    std::istringstream iss(m_resolutions->getItemSelected().toCharArray());

    unsigned int x, y;
    char c;

    if ((iss >> x) && (iss >> c) && (iss >> y) &&
         x > 100 && x < 10000 &&
         y > 100 && y < 10000)
    {
        CApplication::getApp()->setSize(x, y);
    }

    // Plein écran
    bool fullscreen = m_fullscreen->isDown();

    if (CApplication::getApp()->isFullScreen() != fullscreen)
    {
        CApplication::getApp()->switchFullScreen(fullscreen);
    }
}

} // Namespace Ted

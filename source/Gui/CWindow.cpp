/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow.cpp
 * \date    04/2009 Création de la classe GuiWindow.
 * \date 08/07/2010 On peut choisir d'afficher ou pas la barre de titre et la bordure.
 * \date 10/07/2010 Modification des paramètres pour afficher ou pas la bordure.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 23/07/2010 L'affichage du texte du titre de la fenêtre est limité dans un rectangle.
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 29/05/2011 La classe est renommée en CWindow.
 * \date 03/04/2013 Mémorisation des paramètres du texte pour le titre.
 * \date 03/04/2013 Ajout d'attributs pour définir les couleurs de la fenêtre.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindow.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/ILayout.hpp"
#include "Core/Time.hpp"
#include "Core/CApplication.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CColor.hpp"
#include "Core/Maths/CRectangle.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

const int WindowNoResize = -999999;           ///< Valeur des variables resize_* pour lesquelles on ne redimensionne pas la fenêtre.
const int WindowTitleBarHeight = 22;          ///< Hauteur de la barre de titre en pixels.
const int WindowBorder = 5;                   ///< Largeur des bordures des fenêtres en pixels.
const int WindowGlu = 10;                     ///< Si une fenêtre se trouve à cette distance d'un bord, on la colle à ce bord.
const int WindowMinWidth = 100;               ///< Largeur minimale d'une fenêtre en pixels.
const int WindowMinHeight = 50;               ///< Hauteur minimale d'une fenêtre en pixels.
const float WindowTransparencyTime = 500.0f;  ///< Durée pour qu'une fenêtre devienne transparente en millisecondes.


/**
 * Constructeur.
 *
 * \param title  Titre de la fenêtre.
 * \param flags  Paramètres supplémentaires.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow::CWindow(const CString& title, TWindowFlags flags, IWidget * parent) :
IWidget                   (parent),
m_moveX                   (0),
m_moveY                   (0),
m_resizeLeft              (WindowNoResize),
m_resizeRight             (WindowNoResize),
m_resizeTop               (WindowNoResize),
m_resizeBottom            (WindowNoResize),
m_backgroundColorActive   (182, 192, 210),
m_backgroundColorInactive (182, 192, 210),
m_borderColorActive       (69, 82, 104),
m_borderColorInactive     (69, 82, 104, 150),
m_closed                  (1),
m_centerX                 (0),
m_centerY                 (0),
m_resizable               (1),
m_movable                 (1),
m_buttonClose             (1),
m_border                  (1),
m_focus                   (0),
m_time                    (getElapsedTime()),
m_transparency            (0.0f),
m_child                   (nullptr),
m_layout                  (nullptr)
{
    initWindow();
    m_textParams.text = title;

    // Centrée en largeur
    if (flags & WindowCenterX)
    {
        m_centerX = 1;
        m_x = (CApplication::getApp()->getWidth() - m_width) / 2;
    }

    // Centrée en hauteur
    if (flags & WindowCenterY)
    {
        m_centerY = 1;
        m_y = (CApplication::getApp()->getHeight() - m_height - WindowTitleBarHeight) / 2;
    }

    if (flags & WindowNotResizable)
    {
        m_resizable = 0;
    }

    if (flags & WindowNotMovable)
    {
        m_movable = 0;
    }

    if (flags & WindowNoButtonClose)
    {
        m_buttonClose = 0;
    }

    if (flags & WindowNoBorder)
    {
        m_border = 0;
    }
}


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow::CWindow(IWidget * parent) :
IWidget                   (parent),
m_moveX                   (0),
m_moveY                   (0),
m_resizeLeft              (WindowNoResize),
m_resizeRight             (WindowNoResize),
m_resizeTop               (WindowNoResize),
m_resizeBottom            (WindowNoResize),
m_backgroundColorActive   (182, 192, 210),
m_backgroundColorInactive (182, 192, 210),
m_borderColorActive       (69, 82, 104),
m_borderColorInactive     (69, 82, 104, 150),
m_closed                  (1),
m_centerX                 (0),
m_centerY                 (0),
m_resizable               (1),
m_movable                 (1),
m_buttonClose             (1),
m_border                  (1),
m_focus                   (0),
m_time                    (getElapsedTime()),
m_transparency            (0.0f),
m_child                   (nullptr),
m_layout                  (nullptr)
{
    initWindow();
}


/**
 * Destructeur. Supprime l'objet enfant de la mémoire.
 ******************************/

CWindow::~CWindow()
{
    delete m_child;
}


/**
 * Retourne la position horizontale de la fenêtre en tenant compte des bordures.
 *
 * \return Position horizontale réelle.
 ******************************/

int CWindow::getRealX() const
{
    if (m_border)
    {
        return (getX() - WindowBorder);
    }
    else
    {
        return getX();
    }
}


/**
 * Retourne la position verticale de la fenêtre en tenant compte des bordures.
 *
 * \return Position verticale réelle.
 ******************************/

int CWindow::getRealY() const
{
    if (m_border)
    {
        return (getY() - WindowBorder - WindowTitleBarHeight);
    }
    else
    {
        return getY();
    }
}


/**
 * Retourne la largeur de la fenêtre en tenant compte des bordures.
 *
 * \return Largeur réelle en pixels.
 ******************************/

int CWindow::getRealWidth() const
{
    if (m_border)
    {
        return (m_width + 2 * WindowBorder);
    }
    else
    {
        return m_width;
    }
}


/**
 * Retourne la hauteur de la fenêtre en tenant compte des bordures.
 *
 * \return Hauteur réelle en pixels.
 ******************************/

int CWindow::getRealHeight() const
{
    if (m_border)
    {
        return (m_height + 2 * WindowBorder + WindowTitleBarHeight);
    }
    else
    {
        return m_height;
    }
}


CColor CWindow::getBackgroundColorActive() const
{
    return m_backgroundColorActive;
}


void CWindow::setBackgroundColorActive(const CColor& color)
{
    m_backgroundColorActive = color;
}


CColor CWindow::getBackgroundColorInactive() const
{
    return m_backgroundColorInactive;
}


void CWindow::setBackgroundColorInactive(const CColor& color)
{
    m_backgroundColorInactive = color;
}

CColor CWindow::getBorderColorActive() const
{
    return m_borderColorActive;
}

void CWindow::setBorderColorActive(const CColor& color)
{
    m_borderColorActive = color;
}

CColor CWindow::getBorderColorInactive() const
{
    return m_borderColorInactive;
}

void CWindow::setBorderColorInactive(const CColor& color)
{
    m_borderColorInactive = color;
}


/**
 * Modifie le titre de la fenêtre.
 *
 * \param title Titre de la fenêtre.
 *
 * \sa CWindow::getTitle
 ******************************/

void CWindow::setTitle(const CString& title)
{
    m_textParams.text = title;
}


/**
 * Modifie la largeur minimale de la fenêtre.
 *
 * \param width Largeur minimale de la fenêtre.
 ******************************/

void CWindow::setMinWidth(int width)
{
    if (width < 0)
        return;

    IWidget::setMinWidth(width < WindowMinWidth ? WindowMinWidth : width);
}


/**
 * Modifie la hauteur minimale de la fenêtre.
 *
 * \param height Hauteur minimale de la fenêtre.
 ******************************/

void CWindow::setMinHeight(int height)
{
    if (height < 0)
        return;

    IWidget::setMinHeight(height < WindowMinHeight ? WindowMinHeight : height);
}


/**
 * Centre la fenêtre horizontalement.
 *
 * \param centerX Fenêtre centrée horizontalement.
 ******************************/

void CWindow::setCenterX(bool centerX)
{
    m_centerX = centerX;
    m_moveX = 0;
    m_x = (CApplication::getApp()->getWidth() - m_width) / 2;
}


/**
 * Centre la fenêtre verticalement.
 *
 * \param centerY Fenêtre centrée verticalement.
 ******************************/

void CWindow::setCenterY(bool centerY)
{
    m_centerY = centerY;
    m_moveY = 0;

    if (m_border)
    {
        m_y = (CApplication::getApp()->getHeight() - m_height - WindowTitleBarHeight) / 2;
    }
    else
    {
        m_y = (CApplication::getApp()->getHeight() - m_height) / 2;
    }
}


/**
 * Mutateur pour resizable.
 *
 * \param resizable Indique si la fenêtre est redimensionnable.
 ******************************/

void CWindow::setResizable(bool resizable)
{
    m_resizable = resizable;
}


/**
 * Mutateur pour movable.
 *
 * \param movable Indique si la fenêtre est déplaçable.
 ******************************/

void CWindow::setMovable(bool movable)
{
    m_movable = movable;
}


/**
 * Indique si on doit dessiner une croix pour fermer la fenêtre.
 *
 * \param c Booléen
 ******************************/

void CWindow::setButtonClose(bool c)
{
    m_buttonClose = c;
}


/**
 * Pour afficher ou masquer la bordure.
 *
 * \param border Indique si on doit afficher la bordure.
 ******************************/

void CWindow::setBorder(bool border)
{
    m_border = border;
}


/**
 * Change la position de la fenêtre.
 * La fenêtre perd l'obligation d'être centrée.
 *
 * \param x Coordonnée horizontale.
 * \param y Coordonnée verticale.
 ******************************/

void CWindow::setPosition(int x, int y)
{
    m_x = x;
    m_y = y;

    m_centerX = false;
    m_centerY = false;
}


/**
 * Mutateur pour posx.
 *
 * \param x Coordonnée horizontale.
 ******************************/

void CWindow::setX(int x)
{
    m_x = x;
    m_centerX = false;
}


/**
 * Mutateur pour posy.
 *
 * \param y Coordonnée verticale.
 ******************************/

void CWindow::setY(int y)
{
    m_y = y;
    m_centerY = false;
}


/**
 * Modifie la largeur de la fenêtre.
 *
 * \param width Largeur de la fenêtre en pixels.
 ******************************/

void CWindow::setWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width == m_width)
        return;

    int oldWidth = m_width;
    IWidget::setWidth(width);

    if (oldWidth != m_width)
    {
        onResize();

        // On centre la fenêtre
        if (m_centerX)
        {
            int tmp = getX();
            m_x = (static_cast<int>(CApplication::getApp()->getWidth()) - m_width) / 2;

            if (m_x != tmp)
            {
                onMove();
            }
        }

        // On modifie la taille de l'objet enfant
        if (m_child)
        {
            m_child->setWidth(m_width);
        }
    }
}


/**
 * Modifie la hauteur de la fenêtre.
 *
 * \param height Hauteur de la fenêtre en pixels.
 ******************************/

void CWindow::setHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height == m_height)
        return;

    int oldHeight = m_height;
    IWidget::setHeight(height);

    if (oldHeight != m_height)
    {
        onResize();

        // On centre la fenêtre
        if (m_centerY)
        {
            int tmp = getY();

            if (m_border)
            {
                m_y = (static_cast<int>(CApplication::getApp()->getHeight()) - m_height - WindowTitleBarHeight) / 2;
            }
            else
            {
                m_y = (static_cast<int>(CApplication::getApp()->getHeight()) - m_height) / 2;
            }

            if (m_y != tmp)
            {
                onMove();
            }
        }

        // On modifie la taille de l'objet enfant
        if (m_child)
        {
            m_child->setHeight(m_height);
        }
    }
}


/**
 * Modifie la valeur de focus.
 *
 * \param focus Indique si la fenêtre a le focus.
 *
 * \sa CWindow::hasFocus
 ******************************/

void CWindow::setFocus(bool focus)
{
    m_focus = focus;
}


/**
 * Modifie l'objet enfant.
 *
 * \param child Pointeur sur l'objet enfant.
 ******************************/

void CWindow::setChild(IWidget * child)
{
    m_child = child;
    m_layout = nullptr;

    if (m_child)
    {
        m_child->setParent(this);
        m_child->setSize(m_width, m_height);
    }
}


/**
 * Modifie le layout.
 *
 * \param layout Pointeur sur le layout.
 ******************************/

void CWindow::setLayout(ILayout * layout)
{
    m_child = layout;
    m_layout = layout;

    if (m_child)
    {
        m_child->setParent(this);
        m_child->setSize(m_width, m_height);
    }
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void CWindow::onEvent(const CResizeEvent& event)
{
    int oldX = getX();
    int oldY = getY();

    // On centre la fenêtre
    if (m_centerX)
    {
        m_x = (static_cast<int>(CApplication::getApp()->getWidth()) - m_width) / 2;
    }

    if (m_centerY)
    {
        if (m_border)
        {
            m_y = (static_cast<int>(CApplication::getApp()->getHeight()) - m_height - WindowTitleBarHeight) / 2;
        }
        else
        {
            m_y = (static_cast<int>(CApplication::getApp()->getHeight()) - m_height) / 2;
        }
    }

    if (m_x != oldX || m_y != oldY)
    {
        onMove();
    }
}


/**
 * Gestion des évènements liés à la souris.
 *
 * \param event Évènement.
 ******************************/

void CWindow::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    // Si la fenêtre est fermée, on ne fait rien
    if (m_closed)
        return;

    computeSize();

    const int tmpX = getX();
    const int tmpY = getY();

    // Clic sur la croix
    if (m_buttonClose &&
        static_cast<int>(event.x) <= (tmpX + getWidth()) &&
        static_cast<int>(event.x) >= (tmpX + getWidth() - WindowTitleBarHeight) &&
        static_cast<int>(event.y) <= tmpY &&
        static_cast<int>(event.y) >= (tmpY - WindowTitleBarHeight))
    {
        if (event.type == ButtonReleased && event.button == MouseButtonLeft)
        {
            close();
        }

        return;
    }

    if (m_border)
    {
        // Clic en haut de la fenêtre
        if (static_cast<int>(event.x) <= (tmpX + getWidth()) &&
            static_cast<int>(event.x) >= tmpX &&
            static_cast<int>(event.y) <= tmpY &&
            static_cast<int>(event.y) >= (tmpY - WindowTitleBarHeight))
        {
            if (event.button == MouseButtonLeft && m_movable)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_moveX = static_cast<int>(event.x) - tmpX;
                    m_moveY = static_cast<int>(event.y) - tmpY;
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_moveX = 0;
                    m_moveY = 0;
                }
            }

            return;
        }

        // Coin haut gauche
        if (static_cast<int>(event.x) >= (tmpX - WindowBorder) &&
            static_cast<int>(event.x) <= tmpX &&
            static_cast<int>(event.y) <= (tmpY - WindowTitleBarHeight) &&
            static_cast<int>(event.y) >= (tmpY - WindowTitleBarHeight - WindowBorder))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeLeft = static_cast<int>(event.x);
                    m_resizeTop = static_cast<int>(event.y);
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeLeft = WindowNoResize;
                    m_resizeTop = WindowNoResize;
                }
            }

            return;
        }
        // Coin haut droit
        else if (static_cast<int>(event.x) <= (tmpX + getWidth() + WindowBorder) &&
                 static_cast<int>(event.x) >= (tmpX + getWidth()) &&
                 static_cast<int>(event.y) <= (tmpY - WindowTitleBarHeight) &&
                 static_cast<int>(event.y) >= (tmpY - WindowTitleBarHeight - WindowBorder))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeRight = static_cast<int>(event.x) - getWidth();
                    m_resizeTop = static_cast<int>(event.y);
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeRight = WindowNoResize;
                    m_resizeTop = WindowNoResize;
                }
            }

            return;
        }
        // Coin bas gauche
        else if (static_cast<int>(event.x) >= (tmpX - WindowBorder) &&
                 static_cast<int>(event.x) <= tmpX &&
                 static_cast<int>(event.y) <= (tmpY + m_height + WindowBorder) &&
                 static_cast<int>(event.y) >= (tmpY + m_height))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeLeft = static_cast<int>(event.x);
                    m_resizeBottom = static_cast<int>(event.y) - m_height;
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeLeft = WindowNoResize;
                    m_resizeBottom = WindowNoResize;
                }
            }

            return;
        }
        // Coin bas droit
        else if (static_cast<int>(event.x) <= (tmpX + getWidth() + WindowBorder) &&
                 static_cast<int>(event.x) >= (tmpX + getWidth()) &&
                 static_cast<int>(event.y) <= (tmpY + m_height + WindowBorder) &&
                 static_cast<int>(event.y) >= (tmpY + m_height))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeRight = static_cast<int>(event.x) - getWidth();
                    m_resizeBottom = static_cast<int>(event.y) - m_height;
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeRight = WindowNoResize;
                    m_resizeBottom = WindowNoResize;
                }
            }

            return;
        }
        // Bordure gauche
        else if (static_cast<int>(event.x) <= tmpX &&
                 static_cast<int>(event.x) >= (tmpX - WindowBorder))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeLeft = static_cast<int>(event.x);
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeLeft = WindowNoResize;
                }
            }

            return;
        }
        // Bordure droite
        else if (static_cast<int>(event.x) <= (tmpX + getWidth() + WindowBorder) &&
                 static_cast<int>(event.x) >= (tmpX + getWidth()))
        {
            if (m_resizable && event.button == MouseButtonLeft )
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeRight = static_cast<int>(event.x) - getWidth();
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeRight = WindowNoResize;
                }
            }

            return;
        }
        // Bordure haute
        else if (static_cast<int>(event.y) <= (tmpY - WindowTitleBarHeight) &&
                 static_cast<int>(event.y) >= (tmpY - WindowTitleBarHeight - WindowBorder))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeTop = static_cast<int>(event.y);
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeTop = WindowNoResize;
                }
            }

            return;
        }
        // Bordure basse
        else if (static_cast<int>(event.y) <= (tmpY + m_height + WindowBorder) &&
                 static_cast<int>(event.y) >= (tmpY + m_height))
        {
            if (m_resizable && event.button == MouseButtonLeft)
            {
                // Si on commence à cliquer
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    m_resizeBottom = static_cast<int>(event.y) - m_height;
                }
                // Si on arrête de cliquer
                else if (event.type == ButtonReleased)
                {
                    m_resizeBottom = WindowNoResize;
                }
            }

            return;
        }
    }

    if (m_child)
    {
        if (event.type == ButtonPressed || event.type == DblClick)
        {
            if (event.button == MouseButtonLeft)
            {
                Game::guiEngine->setSelected(m_child);
            }

            Game::guiEngine->setFocus(m_child);
        }

        Game::guiEngine->setPointed(m_child);

        // On transmet l'évènement à l'objet enfant
        return m_child->onEvent(event);
    }
}


#ifdef T_USE_PIXMAP

void IWidget::getPixmap(CImage& pixmap) const
{
    // Si la fenêtre est fermée, on ne l'affiche pas
    if (m_closed)
    {
        return;
    }

    const int tmp_x = getX();
    const int tmp_y = getY();
    const int tmp_w = getWidth();
    const int tmp_h = getHeight();

    // Bordure
    if (m_border)
    {
        CColor borderColor = m_borderColorInactive.interpolate(m_borderColorActive, m_transparency);

        pixmap->drawRectangle(CRectangle(tmp_x - WindowBorder,
                                         tmp_y - WindowTitleBarHeight - WindowBorder,
                                         tmp_w + 2 * WindowBorder,
                                         tmp_h + WindowTitleBarHeight + 2 * WindowBorder),
                              borderColor);
    }

    // Zone utilisateur
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h),
                                   m_backgroundColorInactive.interpolate(m_backgroundColorActive, m_transparency)
                                  );

    // Titre
    if (m_border)
    {
        m_textParams.rec.setX(tmp_x + 10);
        m_textParams.rec.setY(tmp_y - WindowTitleBarHeight + 3);
        m_textParams.rec.setWidth(tmp_w - 2 * WindowBorder);

        Game::guiEngine->drawText(m_textParams);

        // Croix pour fermer la fenêtre
        if (m_buttonClose)
        {
            const int tmpX0 = tmp_x + tmp_w;
            const int tmpY0 = tmp_y - WindowTitleBarHeight;

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 18, tmpY0 + 7),
                                      TVector2S(tmpX0 - 7 , tmpY0 + 18),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 7 , tmpY0 + 7),
                                      TVector2S(tmpX0 - 18, tmpY0 + 18),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 18, tmpY0 + 8),
                                      TVector2S(tmpX0 - 8 , tmpY0 + 18),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 7 , tmpY0 + 8),
                                      TVector2S(tmpX0 - 17, tmpY0 + 18),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 17, tmpY0 + 7),
                                      TVector2S(tmpX0 - 7 , tmpY0 + 17),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2S(tmpX0 - 8 , tmpY0 + 7),
                                      TVector2S(tmpX0 - 18, tmpY0 + 17),
                                      CColor::Black);
        }
    }

    // Contenu
    if (m_child)
    {
        m_child->draw();
    }

/*
    // Zone pour redimensionner la fenêtre
    if (m_resizable)
    {
        Game::guiEngine->drawLine(tmp_x + tmp_w - 16, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 16, CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 15, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 15, CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 14, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 14, CColor(175, 175, 175));

        Game::guiEngine->drawLine(tmp_x + tmp_w - 12, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 12, CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 11, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 11, CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 10, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 10, CColor(175, 175, 175));

        Game::guiEngine->drawLine(tmp_x + tmp_w - 8 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 8 , CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 7 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 7 , CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 6 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 6 , CColor(175, 175, 175));
    }
*/
}


void IWidget::paint(CImage& pixmap)
{
    // TODO: affichage de la décoration
    //...

    IWidget::paint(pixmap);
}


void CWindow::update(unsigned int frameTime)
{
    // Pourcentage de transparence
    if (m_focus && m_transparency < 1.0f)
    {
        m_transparency += frameTime / WindowTransparencyTime;
        if (m_transparency > 1.0f)
            m_transparency = 1.0f;
    }
    else if (!m_focus && m_transparency > 0.0f)
    {
        m_transparency -= frameTime / WindowTransparencyTime;
        if (m_transparency < 0.0f)
            m_transparency = 0.0f;
    }

    computeSize();

    IWidget::update(frameTime);
}

#else

/**
 * Dessine la fenêtre.
 *
 * \todo Ne plus appeler computeSize à chaque frame.
 ******************************/

void CWindow::draw()
{
    unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_time;
    m_time = time;

    // Si la fenêtre est fermée, on ne l'affiche pas
    if (m_closed)
        return;

    computeSize();

    const int tmp_x = getX();
    const int tmp_y = getY();
    const int tmp_w = getWidth();
    const int tmp_h = getHeight();

    // Pourcentage de transparence
    if (m_focus && m_transparency < 1.0f)
    {
        m_transparency += frameTime / WindowTransparencyTime;
        if (m_transparency > 1.0f)
            m_transparency = 1.0f;
    }
    else if (!m_focus && m_transparency > 0.0f)
    {
        m_transparency -= frameTime / WindowTransparencyTime;
        if (m_transparency < 0.0f)
            m_transparency = 0.0f;
    }

    // Bordure
    if (m_border)
    {
        Game::guiEngine->drawRectangle(CRectangle(tmp_x - WindowBorder,
                                                  tmp_y - WindowTitleBarHeight - WindowBorder,
                                                  tmp_w + 2 * WindowBorder,
                                                  tmp_h + WindowTitleBarHeight + 2 * WindowBorder),
                                       m_borderColorInactive.interpolate(m_borderColorActive, m_transparency)
                                      );
    }

    // Zone utilisateur
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, tmp_w, tmp_h),
                                   m_backgroundColorInactive.interpolate(m_backgroundColorActive, m_transparency)
                                  );

    // Titre
    if (m_border)
    {
        m_textParams.rec.setX(tmp_x + 10);
        m_textParams.rec.setY(tmp_y - WindowTitleBarHeight);
        m_textParams.rec.setWidth(tmp_w - 2 * WindowBorder);

        Game::guiEngine->drawText(m_textParams);

        // Croix pour fermer la fenêtre
        if (m_buttonClose)
        {
            const int tmpX0 = tmp_x + tmp_w - WindowBorder;
            const int tmpY0 = tmp_y - WindowTitleBarHeight + WindowBorder;

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 11, tmpY0 + 0),
                                      TVector2F(tmpX0 - 0 , tmpY0 + 11),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 0 , tmpY0 + 0),
                                      TVector2F(tmpX0 - 11, tmpY0 + 11),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 11, tmpY0 + 1),
                                      TVector2F(tmpX0 - 1 , tmpY0 + 11),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 0 , tmpY0 + 1),
                                      TVector2F(tmpX0 - 10, tmpY0 + 11),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 10, tmpY0 + 0),
                                      TVector2F(tmpX0 - 0 , tmpY0 + 10),
                                      CColor::Black);

            Game::guiEngine->drawLine(TVector2F(tmpX0 - 1 , tmpY0 + 0),
                                      TVector2F(tmpX0 - 11, tmpY0 + 10),
                                      CColor::Black);
        }
    }

    // Contenu
    if (m_child)
    {
        m_child->draw();
    }

/*
    // Zone pour redimensionner la fenêtre
    if (m_resizable)
    {
        Game::guiEngine->drawLine(tmp_x + tmp_w - 16, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 16, CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 15, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 15, CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 14, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 14, CColor(175, 175, 175));

        Game::guiEngine->drawLine(tmp_x + tmp_w - 12, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 12, CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 11, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 11, CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 10, tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 10, CColor(175, 175, 175));

        Game::guiEngine->drawLine(tmp_x + tmp_w - 8 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 8 , CColor::Black);
        Game::guiEngine->drawLine(tmp_x + tmp_w - 7 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 7 , CColor(175, 175, 175));
        Game::guiEngine->drawLine(tmp_x + tmp_w - 6 , tmp_y + tmp_h - 4, tmp_x + tmp_w - 4, tmp_y + tmp_h - 6 , CColor(175, 175, 175));
    }
*/
}

#endif


/**
 * Indique le curseur à utiliser en fonction de la position du curseur de la souris.
 *
 * \param x Coordonnée horizontale du curseur.
 * \param y Coordonnée verticale du curseur.
 * \return Curseur à utiliser (flèche).
 ******************************/

TCursor CWindow::getCursorType(int x, int y) const
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    if (m_child &&
        x >= m_child->getX() &&
        y >= m_child->getY() &&
        x <= m_child->getX() + m_child->getWidth() &&
        y <= m_child->getY() + m_child->getHeight())
    {
        return m_child->getCursorType(x, y);
    }

    if (m_resizable)
    {
        // Coin haut gauche
        if (x >= (tmp_x - WindowBorder) &&
            x <= tmp_x &&
            y <= (tmp_y - WindowTitleBarHeight) &&
            y >= (tmp_y - WindowTitleBarHeight - WindowBorder))
        {
            return CursorResizeNWSE;
        }
        // Coin haut droit
        else if (x <= (tmp_x + m_width + WindowBorder) &&
                 x >= (tmp_x + m_width) &&
                 y <= (tmp_y - WindowTitleBarHeight) &&
                 y >= (tmp_y - WindowTitleBarHeight - WindowBorder))
        {
            return CursorResizeNESW;
        }
        // Coin bas gauche
        else if (x >= (tmp_x - WindowBorder) &&
                 x <= tmp_x &&
                 y <= (tmp_y + m_height + WindowBorder) &&
                 y >= (tmp_y + m_height))
        {
            return CursorResizeNESW;
        }
        // Coin bas droit
        else if (x <= (tmp_x + m_width + WindowBorder) &&
                 x >= (tmp_x + m_width) &&
                 y <= (tmp_y + m_height + WindowBorder) &&
                 y >= (tmp_y + m_height))
        {
            return CursorResizeNWSE;
        }
        // Bordure gauche
        else if (x <= tmp_x && x >= (tmp_x - WindowBorder))
        {
            return CursorResizeWE;
        }
        // Bordure droite
        else if (x <= (tmp_x + m_width + WindowBorder) && x >= (tmp_x + m_width))
        {
            return CursorResizeWE;
        }
        // Bordure haute
        else if (y < (tmp_y - WindowTitleBarHeight) &&
                 y >= (tmp_y - WindowTitleBarHeight - WindowBorder))
        {
            return CursorResizeNS;
        }
        // Bordure basse
        else if (y <= (tmp_y + m_height + WindowBorder) &&
                 y >= (tmp_y + m_height))
        {
            return CursorResizeNS;
        }
    }

    return CursorArrow;
}


/**
 * Calcul les dimensions et la position de la fenêtre.
 ******************************/

void CWindow::computeSize()
{
    // Dimensions de la fenêtre
    const int app_w = CApplication::getApp()->getWidth();
    const int app_h = CApplication::getApp()->getHeight();

    // Largeur minimale
    if (m_width < m_minWidth)
    {
        m_width = m_minWidth;

        if (m_child)
        {
            m_child->setWidth(m_width);
/*
            // On recalcule la taille du layout
            if (m_layout)
            {
                m_layout->computeSize();
            }
*/
        }

        // Centrée en largeur
        if (m_centerX)
        {
            m_x = (app_w - m_width) / 2;
        }
    }

    // Hauteur minimale
    if (m_height < m_minHeight)
    {
        m_height = m_minHeight;

        if (m_child)
        {
            m_child->setHeight(m_height);
/*
            // On recalcule la taille du layout
            if (m_layout)
            {
                m_layout->computeSize();
            }
*/
        }

        // Centrée en hauteur
        if (m_centerY)
        {
            if (m_border)
            {
                m_y = (app_h - m_height - WindowTitleBarHeight) / 2;
            }
            else
            {
                m_y = (app_h - m_height) / 2;
            }
        }
    }


    const TVector2I cursor = CApplication::getApp()->getCursorPosition();
    const int cursorX = cursor.X;
    const int cursorY = cursor.Y;

    // Déplacement de la fenêtre
    if (m_movable && (m_moveX > 0 || m_moveY > 0))
    {
        // On vérifie que le clic gauche est actif
        if (CApplication::getApp()->getButtonState(MouseButtonLeft))
        {
            const int oldX = m_x;
            const int oldY = m_y;

            // La fenêtre suit le curseur
            m_x = cursorX - m_moveX;
            m_y = cursorY - m_moveY;

            if (WindowGlu > 0)
            {
                int border = (m_border ? WindowBorder : 0);
                int titleBar = (m_border ? WindowTitleBarHeight : 0);

                // On colle la fenêtre aux bords de la fenêtre parente si on s'en rapproche
                if ((m_x - border) < WindowGlu && (m_x - border) > - WindowGlu)
                {
                    m_x = border;
                }
                else if ((m_x + m_width + border) > app_w - WindowGlu && (m_x + m_width + border) < (app_w + WindowGlu))
                {
                    m_x = app_w - m_width - border;
                }

                if ((m_y - border - titleBar) < WindowGlu && (m_y - border - titleBar) > -WindowGlu)
                {
                    m_y = border + titleBar;
                }
                else if ((m_y + border) > (app_h - m_height - WindowGlu) && (m_y + border) < (app_h - m_height + WindowGlu))
                {
                    m_y = app_h - m_height - border;
                }
            }

            // Si la fenêtre a bougé, on envoit un output
            if (oldX != m_x || oldY != m_y)
            {
                onMove();
            }
        }
        else
        {
            m_moveX = 0;
            m_moveY = 0;
        }
    }

    // Redimensionnement de la fenêtre
    if (m_resizable && (
        m_resizeLeft != WindowNoResize || m_resizeRight != WindowNoResize ||
        m_resizeTop != WindowNoResize || m_resizeBottom != WindowNoResize))
    {
        // On vérifie que le clic gauche est actif
        if (CApplication::getApp()->getButtonState(MouseButtonLeft))
        {
            const int old_w = m_width;
            const int old_h = m_height;

            // Redimensionnement à gauche
            if (m_resizeLeft != WindowNoResize &&
                cursorX < m_width + getX() - (m_border ? WindowBorder : 0) - m_minWidth)
            {
                m_centerX = false;

                m_x -= m_resizeLeft - cursorX;
                m_width += m_resizeLeft - cursorX;
                m_resizeLeft = cursorX;
            }

            // Redimensionnement à droite
            if (m_resizeRight != WindowNoResize && m_resizeRight < cursorX)
            {
                m_centerX = false;

                m_width = cursorX - m_resizeRight;
            }

            // Redimensionnement en haut
            if (m_resizeTop != WindowNoResize &&
                cursorY < m_height + getY() - (m_border ? WindowTitleBarHeight + WindowBorder : 0) - m_minHeight)
            {
                m_centerY = false;

                m_y -= m_resizeTop - cursorY;
                m_height += m_resizeTop - cursorY;
                m_resizeTop = cursorY;
            }

            // Redimensionnement en bas
            if (m_resizeBottom != WindowNoResize && m_resizeBottom < cursorY)
            {
                m_centerY = false;

                m_height = cursorY - m_resizeBottom;
            }

            // Hauteur minimale et maximale
            if (m_height < m_minHeight)
                m_height = m_minHeight;
            else if (m_height > m_maxHeight)
                m_height = m_maxHeight;

            // Largeur minimale et maximale
            if (m_width < m_minWidth)
                m_width = m_minWidth;
            else if (m_width > m_maxWidth)
                m_width = m_maxWidth;

            // Les dimensions de la fenêtre ont changées
            if (old_w != m_width || old_h != m_height)
            {
                onResize();

                if (m_child)
                {
                    m_child->setSize(m_width, m_height);
/*
                    // On recalcule la taille du layout
                    if (m_layout)
                    {
                        m_layout->computeSize();
                    }
*/
                }
            }
        }
        else
        {
            // On réinitialise les variables
            m_resizeLeft   = WindowNoResize;
            m_resizeRight  = WindowNoResize;
            m_resizeTop    = WindowNoResize;
            m_resizeBottom = WindowNoResize;
        }
    }
}


/**
 * Supprime l'objet enfant et libère la mémoire.
 ******************************/

void CWindow::removeChild()
{
    delete m_child;
    m_child = nullptr;
    m_layout = nullptr;
}


/**
 * Ouvre la fenêtre.
 ******************************/

void CWindow::open()
{
    if (m_closed)
    {
        m_closed = 0;
        onOpen();
    }

    Game::guiEngine->onWindowOpen(this);
}


/**
 * Ferme la fenêtre (mais ne la détruit pas).
 ******************************/

void CWindow::close()
{
    if (!m_closed)
    {
        m_closed = 1;
        onClose();
    }

    Game::guiEngine->onWindowClose(this);
}


/**
 * Initialise les paramètres du widget.
 ******************************/

void CWindow::initWindow()
{
    setMinSize(WindowMinWidth, WindowMinHeight);

    m_textParams.font  = Game::fontManager->getFontId("Arial");
#ifdef T_USE_FREETYPE
    m_textParams.size  = 15;
#else
    m_textParams.size  = 18;
#endif
    m_textParams.color = CColor::Black;
    m_textParams.rec.setHeight(WindowTitleBarHeight - 3);
}

} // Namespace Ted

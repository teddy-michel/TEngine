/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutHorizontal.cpp
 * \date 18/01/2010 Création de la classe GuiLayoutHorizontal.
 * \date 29/05/2011 La classe est renommée en CLayoutHorizontal.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 * \date 14/06/2014 Dérivation de la classe ILayoutBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>
#include <vector>

#include "Gui/CLayoutHorizontal.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLayoutHorizontal::CLayoutHorizontal(IWidget * parent) :
ILayoutBox (parent)
{ }


/**
 * Ajoute un objet à la fin de la liste.
 *
 * \param widget  Pointeur sur l'objet à ajouter.
 * \param align   Alignement du widget dans la case.
 * \param pos     Position où on souhaite insérer l'item (commence à 0, par défaut à la fin).
 * \param stretch Facteur d'étirement.
 ******************************/

void CLayoutHorizontal::addChild(IWidget * widget, TAlignment align, int pos, int stretch)
{
    if (widget && !isChild(widget))
    {
        widget->setParent(this);

        int i = 0;
        int x = 0;

        TLayoutItem tmp;
        tmp.widget  = widget;
        tmp.align   = align;
        tmp.stretch = stretch;
        tmp.size    = widget->getMinWidth() + m_padding.getLeft() + m_padding.getRight();

        // On insère l'objet à la fin de la liste
        if (pos < 0 || pos >= getNumChildren())
        {
            m_children.push_back(tmp);
        }
        else
        {
            for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
            {
                if (i == pos)
                {
                    it = m_children.insert(it, tmp);
                    x += tmp.size;
                }

                x += it->widget->getMinWidth();
            }
        }

        if (x > m_width)
        {
            setWidth(x);
        }
        else
        {
            computeSize();
        }
    }
}


/**
 * Calcul les dimensions du layout.
 ******************************/

void CLayoutHorizontal::computeSize()
{
    // Nombre d'objets enfants
    const int nbr_children = getNumChildren();

    // Dimensions minimales et maximales du layout
    int min_height = 0;
    int max_height = 0;
    int min_width = 0;
    int max_width = 0;

    // Hauteur et largeur de chaque widget
    std::vector<int> widgets_h_min;
    std::vector<int> widgets_w_min;
    std::vector<int> widgets_h_max;
    std::vector<int> widgets_w_max;

    widgets_h_min.reserve(nbr_children);
    widgets_w_min.reserve(nbr_children);
    widgets_h_max.reserve(nbr_children);
    widgets_w_max.reserve(nbr_children);

    int totalStretch = 0;

    // On cherche la taille de chaque objet
    for (std::list<TLayoutItem>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        totalStretch += it->stretch;

        // Dimensions minimales
        int tmp_w = it->widget->getMinWidth();
        widgets_w_min.push_back(tmp_w);

        int tmp_h = it->widget->getMinHeight();
        widgets_h_min.push_back(tmp_h);

        tmp_h = SumInt(tmp_h, m_padding.getTop() + m_padding.getBottom());

        if (tmp_h > min_height)
        {
            min_height = tmp_h;
        }

        min_width = SumInt(min_width, SumInt(tmp_w, m_padding.getLeft() + m_padding.getRight()));

        // Dimensions maximales
        tmp_w = it->widget->getMaxWidth();
        widgets_w_max.push_back(tmp_w);

        tmp_h = it->widget->getMaxHeight();
        widgets_h_max.push_back(tmp_h);

        tmp_h = SumInt(tmp_h, m_padding.getTop() + m_padding.getBottom() );

        if (tmp_h > max_height)
        {
            max_height = tmp_h;
        }

        max_width = SumInt(max_width, SumInt(tmp_w, m_padding.getLeft() + m_padding.getRight()));
    }

    min_width = SumInt(min_width, m_margin.getLeft() + m_margin.getRight());
    max_width = SumInt(max_width, m_margin.getLeft() + m_margin.getRight());

    min_height = SumInt(min_height, m_margin.getTop() + m_margin.getBottom());
    max_height = SumInt(max_height, m_margin.getTop() + m_margin.getBottom());

    setMinSize(min_width, min_height);
    setMaxSize(max_width, max_height);


    // Espace à répartir
    int residu = 0;

    if (min_width < m_width)
    {
        residu = m_width - min_width;
    }


    // Dimensions de chaque objet enfant
    std::vector<int> widgets_w;
    std::vector<int> widgets_h;

    widgets_w.reserve(nbr_children);
    widgets_h.reserve(nbr_children);

    int growingStretch = totalStretch;
    int growingChildren = nbr_children;

    // Calcul des dimensions de chaque objet enfant
    for (int i = 0; i < nbr_children; ++i)
    {
        if (widgets_h_min[i] < widgets_h_max[i])
        {
            if (widgets_h_max[i] < m_height - m_padding.getTop() - m_padding.getBottom())
                widgets_h[i] = widgets_h_min[i];
            else
                widgets_h[i] = m_height - m_padding.getTop() - m_padding.getBottom();
        }
        else
        {
            widgets_h[i] = widgets_h_min[i];
        }

        widgets_w[i] = widgets_w_min[i];

        if (widgets_w[i] >= widgets_w_max[i])
        {
            --growingChildren;
        }
    }


    while (residu > 0)
    {
        if (growingStretch == 0)
        {
            // Tous les widgets ont leur taille maximale
            if (growingChildren == 0)
            {
                int nbr_first = residu % nbr_children;
                int add_pixel = residu / nbr_children;
                residu = 0;

                int i = 0;

                // On modifie la taille des cases
                for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
                {
                    it->size = widgets_w[i] + m_padding.getLeft() + m_padding.getRight() + add_pixel;
                    if (i < nbr_first)
                        ++(it->size);
                }
            }
            else
            {
                float pixelPerStretch = static_cast<float>(residu) / static_cast<float>(growingChildren);
                int pixelAdd = pixelPerStretch;

                if (pixelAdd == 0)
                {
                    growingChildren = 0;
                    continue;
                }

                int i = 0;

                // Pour chaque objet enfant
                for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
                {
                    // On peut agrandir le widget
                    if (widgets_w[i] < widgets_w_max[i])
                    {
                        int pixelCanAdd = widgets_w_max[i] - widgets_w[i];

                        if (pixelCanAdd >= pixelAdd)
                        {
                            widgets_w[i] += pixelAdd;
                            residu -= pixelAdd;

                            if (pixelCanAdd == pixelAdd)
                            {
                                --growingChildren;
                            }
                        }
                        else
                        {
                            widgets_w[i] += pixelCanAdd;
                            residu -= pixelCanAdd;
                            --growingChildren;
                        }

                        it->size = widgets_w[i] + m_padding.getLeft() + m_padding.getRight();
                    }
                }
            }
        }
        else
        {
            float pixelPerStretch = static_cast<float>(residu) / static_cast<float>(growingStretch);

            if (pixelPerStretch < 1.0f)
            {
                growingStretch = 0;
                growingChildren = 0;
                continue;
            }

            bool noChange = true;
            int i = 0;

            // Pour chaque objet enfant
            for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
            {
                // On peut agrandir le widget
                if (widgets_w[i] < widgets_w_max[i])
                {
                    int pixelAdd = pixelPerStretch * it->stretch;
                    int pixelCanAdd = widgets_w_max[i] - widgets_w[i];

                    if (pixelCanAdd >= pixelAdd)
                    {
                        widgets_w[i] += pixelAdd;
                        residu -= pixelAdd;

                        if (pixelCanAdd == pixelAdd)
                        {
                            --growingChildren;
                            growingStretch -= it->stretch;
                        }
                    }
                    else
                    {
                        widgets_w[i] += pixelCanAdd;
                        residu -= pixelCanAdd;

                        --growingChildren;
                        growingStretch -= it->stretch;
                    }

                    it->size = widgets_w[i] + m_padding.getLeft() + m_padding.getRight();

                    noChange = false;
                }
            }

            // Tous les widgets ont leur taille maximale
            if (noChange)
            {
                i = 0;

                // Pour chaque objet enfant
                for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
                {
                    int pixelAdd = pixelPerStretch * it->stretch;
                    residu -= pixelAdd;
                    it->size = widgets_w[i] + m_padding.getLeft() + m_padding.getRight() + pixelAdd;
                }

                growingStretch = 0;
                growingChildren = 0;
            }
        }
    }


    int offset = m_margin.getLeft();
    int i = 0;

    // On modifie la liste des objets
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it, ++i)
    {
        // Dimensions de l'objet
        it->widget->setSize(widgets_w[i], widgets_h[i]);

        switch (it->align)
        {
            case AlignTopLeft:
                it->widget->setPosition(offset + m_padding.getLeft(), m_margin.getTop() + m_padding.getTop());
                break;

            case AlignTopCenter:
                it->widget->setPosition(offset + (it->size - widgets_w[i]) / 2, m_margin.getTop() + m_padding.getTop());
                break;

            case AlignTopRight:
                it->widget->setPosition(offset + it->size - m_padding.getRight() - widgets_w[i], m_margin.getTop() + m_padding.getTop());
                break;

            case AlignMiddleLeft:
                it->widget->setPosition(offset + m_padding.getLeft(), (m_height - widgets_h[i]) / 2);
                break;

            default:
            case AlignMiddleCenter:
                it->widget->setPosition(offset + (it->size - widgets_w[i]) / 2, (m_height - widgets_h[i]) / 2);
                break;

            case AlignMiddleRight:
                it->widget->setPosition(offset + it->size - m_padding.getRight() - widgets_w[i], (m_height - widgets_h[i]) / 2);
                break;

            case AlignBottomLeft:
                it->widget->setPosition(offset + m_padding.getLeft(), m_height - widgets_h[i] - m_padding.getBottom());
                break;

            case AlignBottomCenter:
                it->widget->setPosition(offset + (it->size - widgets_w[i]) / 2, m_height - widgets_h[i] - m_padding.getBottom());
                break;

            case AlignBottomRight:
                it->widget->setPosition(offset + it->size - m_padding.getRight() - widgets_w[i], m_height - widgets_h[i] - m_padding.getBottom());
                break;
        }

        offset += it->size;
    }
}

} // Namespace Ted

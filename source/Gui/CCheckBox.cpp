/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CCheckBox.cpp
 * \date       2008 Création de la classe GuiCheckBox.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CCheckBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CCheckBox.hpp"
#include "Gui/CLabel.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

const int CheckBoxCase = 12; ///< Dimension de la case à cocher en pixels.
const int CheckBoxSize = 20; ///< Hauteur du widget en pixels.
const int CheckBoxDecal = (CheckBoxSize - CheckBoxCase) / 2;


/**
 * Constructeur par défaut.
 *
 * \param text   Texte du bouton.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CCheckBox::CCheckBox(const CString& text, IWidget * parent) :
IButton (text, parent),
m_label (nullptr)
{
    m_label = new CLabel(text, this);
    m_label->setPosition(CheckBoxSize, 0);

    setMinSize(CheckBoxSize + m_label->getWidth(), CheckBoxSize);
    setMaxHeight(CheckBoxSize);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CCheckBox::CCheckBox(IWidget * parent) :
IButton (CString(), parent),
m_label (nullptr)
{
    m_label = new CLabel(CString(), this);
    m_label->setPosition(CheckBoxSize, 0);

    setMinSize(CheckBoxSize + m_label->getWidth(), CheckBoxSize);
    setMaxHeight(CheckBoxSize);
}


/**
 * Destructeur.
 ******************************/

CCheckBox::~CCheckBox()
{
    delete m_label;
}


/**
 * Donne le texte du bouton.
 *
 * \return Texte du bouton.
 *
 * \sa CCheckBox::setText
 ******************************/

CString CCheckBox::getText() const
{
    return (m_label ? m_label->getText() : CString());
}


/**
 * Modifie le texte du bouton.
 *
 * \param text Texte du bouton.
 *
 * \sa CCheckBox::getText
 ******************************/

void CCheckBox::setText(const CString& text)
{
    IButton::setText(text);

    if (m_label)
    {
        m_label->setText(text);
        setMinWidth(CheckBoxSize + m_label->getWidth());
    }
}


/**
 * Dessine le bouton.
 ******************************/

void CCheckBox::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    // Bordure du bouton
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y + CheckBoxDecal, CheckBoxCase, CheckBoxCase), CColor(28, 38, 60));

    // Fond du bouton
    if (Game::guiEngine->isPointed(this))
    {
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1, tmp_y + CheckBoxDecal + 1, CheckBoxCase - 2, CheckBoxCase - 2), CColor(225, 225, 225));
    }
    else
    {
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1, tmp_y + CheckBoxDecal + 1, CheckBoxCase - 2, CheckBoxCase - 2), CColor::White);
    }

    // Carré intérieur
    if (m_down)
    {
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + 2, tmp_y + CheckBoxDecal + 2, CheckBoxCase - 4, CheckBoxCase - 4), CColor(156, 255, 0));
    }

    // Affichage du texte
    if (m_label)
    {
        m_label->draw();
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CCheckBox::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    const int tmp_x = getX();
    const int tmp_y = getY();

    if (event.button == MouseButtonLeft && event.type == ButtonReleased &&
        static_cast<int>(event.x) >= tmp_x &&
        static_cast<int>(event.x) <= (tmp_x + m_width) &&
        static_cast<int>(event.y) >= tmp_y &&
        static_cast<int>(event.y) <= (tmp_y + m_height) &&
        Game::guiEngine->isSelected(this))
    {
        onClicked();
        toggleDown();
    }
}

} // Namespace Ted

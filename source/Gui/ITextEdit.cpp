/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ITextEdit.cpp
 * \date 04/11/2010 Création de la classe GuiBaseTextEdit.
 * \date 13/11/2010 On peut préciser la taille du texte.
 * \date 17/01/2011 Cette classe n'hérite plus de GuiWidget.
 * \date 19/01/2011 Utilisation d'une structure TTextParams pour stocker les paramètres du texte.
 * \date 26/01/2011 Le signal onTextChange n'est émis que quand le texte est réellement modifié.
 * \date 29/05/2011 La classe est renommée en ITextEdit.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/ITextEdit.hpp"
#include "Core/Time.hpp"
#include "Core/Events.hpp"

#ifdef T_SYSTEM_WINDOWS
#  include <windows.h>
#endif // T_SYSTEM_WINDOWS


namespace Ted
{

const unsigned int TextDefaultMaxLength = 1024*1024; ///< Longueur maximale par défaut.


/**
 * Constructeur par défaut.
 *
 * \param text Texte par défaut.
 ******************************/

ITextEdit::ITextEdit(const CString& text) :
m_oldText   (text),
m_editable  (true),
m_maxlength (TextDefaultMaxLength),
m_cursor    (0),
m_select    (0),
m_time      (getElapsedTime()),
m_timeBlink (0)
{
    m_params.text  = text;
    m_params.font  = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
    m_params.size  = 13;
#else
    m_params.size  = 15;
#endif // T_USE_FREETYPE

    m_params.color = CColor::Black;
}


/**
 * Retourne le texte.
 *
 * \return Texte.
 *
 * \sa ITextEdit::setText
 ******************************/

CString ITextEdit::getText() const
{
    return m_params.text;
}


/**
 * Retourne le texte avant la dernière modification.
 *
 * \return Ancien texte.
 *
 * \sa ITextEdit::getText
 ******************************/

CString ITextEdit::getOldText() const
{
    return m_oldText;
}


/**
 * Donne la longueur du texte.
 *
 * \return Longueur du texte.
 *
 * \todo Gérer les caractères spéciaux, l'encodage, etc.
 ******************************/

unsigned int ITextEdit::getTextLength() const
{
    return m_params.text.getSize();
}


/**
 * Indique si le texte est modifiable.
 *
 * \return Booléen.
 ******************************/

bool ITextEdit::isEditable() const
{
    return m_editable;
}


/**
 * Donne la longueur maximale du texte.
 *
 * \return Longueur maximale du texte.
 *
 * \sa ITextEdit::setMaxLength
 ******************************/

unsigned int ITextEdit::getMaxLength() const
{
    return m_maxlength;
}


/**
 * Donne la position du curseur.
 *
 * \return Position du curseur.
 *
 * \sa ITextEdit::setCursor
 ******************************/

unsigned int ITextEdit::getCursor() const
{
    return m_cursor;
}


/**
 * Donne la longueur du texte sélectionné depuis le curseur.
 *
 * \return Longueur du texte sélectionné.
 *
 * \sa ITextEdit::setSelect
 ******************************/

int ITextEdit::getSelect() const
{
    return m_select;
}


/**
 * Donne la taille du texte.
 *
 * \return Taille du texte (nombre de caractères).
 *
 * \sa ITextEdit::setTextSize
 ******************************/

unsigned int ITextEdit::getTextSize() const
{
    return m_params.size;
}


/**
 * Modifie le texte du champ.
 *
 * \param text Texte.
 *
 * \sa ITextEdit::getText
 ******************************/

void ITextEdit::setText(const CString& text)
{
    m_oldText = m_params.text;
    m_params.text = (text.getSize() <= m_maxlength ? text : text.subString(0, m_maxlength));

    m_cursor = m_params.text.getSize();
    m_select = 0;
    m_timeBlink = timeBlink;

    if (m_params.text != m_oldText)
    {
        onTextChange();
    }
}


/**
 * Mutateur pour editable.
 *
 * \param editable Indique si le texte est modifiable.
 *
 * \sa ITextEdit::isEditable
 ******************************/

void ITextEdit::setEditable(bool editable)
{
    m_editable = editable;
}


/**
 * Modifie la longueur maximale du texte.
 *
 * \param maxlength Longueur maximale du texte.
 *
 * \sa ITextEdit::getMaxLength
 ******************************/

void ITextEdit::setMaxLength(unsigned int maxlength)
{
    m_maxlength = maxlength;

    if (m_params.text.getSize() > m_maxlength)
    {
        m_params.text = m_params.text.subString(0, m_maxlength);
    }
}


/**
 * Modifie la position du curseur.
 *
 * \param cursor Position du curseur.
 *
 * \sa ITextEdit::getCursor
 ******************************/

void ITextEdit::setCursor(unsigned int cursor)
{
    const unsigned int text_length = m_params.text.getSize();
    m_cursor = (cursor <= text_length ? cursor : text_length);
}


/**
 * Mutateur pour select.
 *
 * \param select Longueur du texte sélectionné.
 *
 * \sa ITextEdit::getSelect
 ******************************/

void ITextEdit::setSelect(int select)
{
    const unsigned int text_length = m_params.text.getSize();

    if (static_cast<int>(select + m_cursor) < 0)
        select = -m_cursor;
    else if (static_cast<int>(select + m_cursor) > static_cast<int>(text_length))
        select = m_params.size - m_cursor;

    m_select = select;
}


/**
 * Modifie la taille du texte.
 *
 * \param size Taille du texte (entre 6 et 48).
 *
 * \sa ITextEdit::getTextSize
 ******************************/

void ITextEdit::setTextSize(unsigned int size)
{
         if (size < 6)  m_params.size = 6;
    else if (size > 48) m_params.size = 48;
    else                m_params.size = size;
}


/**
 * Annule la dernière modification.
 ******************************/

void ITextEdit::undoLastModification()
{
    m_params.text = m_oldText;
    m_cursor = 0;
    m_select = 0;

    onTextChange();
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void ITextEdit::onTextEvent(const CTextEvent& event)
{
    const char character = static_cast<char>(event.unicode);

    // On n'ajoute pas les caractères spéciaux
    if (character < 32 && (character != '\n' && character != '\r' && character != '\t' && character != '\v'))
    {
        return;
    }

    if (m_editable)
    {
        m_oldText = m_params.text;

        if (m_select < 0)
        {
            m_params.text = m_params.text.subString(0, m_cursor + m_select) + character + m_params.text.subString(m_cursor);
            m_cursor += 1 + m_select;
            m_select = 0;
        }
        else if (m_select > 0)
        {
            m_params.text = m_params.text.subString(0, m_cursor) + character + m_params.text.subString(m_cursor + m_select);
            ++m_cursor;
            m_select = 0;
        }
        else
        {
            m_params.text = m_params.text.subString(0, m_cursor) + character + m_params.text.subString(m_cursor);
            ++m_cursor;
        }

        m_timeBlink = timeBlink;

        onTextChange();
        onCursorMove(m_cursor);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \todo Gérer le copier-coller.
 *
 * \param event Évènement.
 ******************************/

void ITextEdit::onKeyboardEvent(const CKeyboardEvent& event)
{
/* THIS IS NOT A WIDGET !
    if (!isHierarchyEnable())
        return;
*/
    if (event.type == KeyReleased)
        return;

    // Ctrl + ...
    if (event.modif & ControlModifier)
    {
        switch (event.code)
        {
            default:
                break;
            case Key_A:
                selectAll();
                break;
            case Key_C:
                copySelectedText();
                break;
            case Key_X:
                copySelectedText();
                removeSelectedText();
                break;
            case Key_V:
                paste();
                break;
            case Key_Z:
                undoLastModification();
                break;
        }
    }
    // Shift + ...
    else if (event.modif & ShiftModifier)
    {
        switch (event.code)
        {
            // Shift + Début
            case Key_Home:
            case Key_PageUp:
                m_select = m_cursor;
                m_cursor = 0;
                break;

            // Shift + Fin
            case Key_End:
            case Key_PageDown:
                m_select = static_cast<int>(m_cursor) - static_cast<int>(m_params.text.getSize());
                m_cursor = m_params.text.getSize();
                break;

            // Shift + Flèche gauche
            case Key_Left:

                if (m_cursor != 0)
                {
                    --m_cursor;
                    if (static_cast<int>(m_cursor + m_select) < static_cast<int>(m_params.text.getSize()))
                        ++m_select;

                    onCursorMove(m_cursor);
                }

                break;

            // Shift + Flèche droite
            case Key_Right:

                if (m_cursor < m_params.text.getSize())
                {
                    ++m_cursor;
                    if (static_cast<int>(m_cursor + m_select) > 0)
                        --m_select;

                    onCursorMove(m_cursor);
                }

                break;

            // Shift + Lettres et caractères spéciaux
            default: break;
        }
    }
    else
    {
        switch (event.code)
        {
            // Touches spéciales
            case Key_Unknown:    break;
            case Key_LShift:     break;
            case Key_RShift:     break;
            case Key_LControl:   break;
            case Key_RControl:   break;
            case Key_LAlt:       break;
            case Key_RAlt:       break;
            case Key_LMeta:      break;
            case Key_RMeta:      break;
            case Key_Print:      break;
            case Key_Insert:     break;
            case Key_Escape:     break;

            // Fonctions
            case Key_F1:         break;
            case Key_F2:         break;
            case Key_F3:         break;
            case Key_F4:         break;
            case Key_F5:         break;
            case Key_F6:         break;
            case Key_F7:         break;
            case Key_F8:         break;
            case Key_F9:         break;
            case Key_F10:        break;
            case Key_F11:        break;
            case Key_F12:        break;
            case Key_F13:        break;
            case Key_F14:        break;
            case Key_F15:        break;

            case Key_Up:         break;
            case Key_Down:       break;

            // Début
            case Key_Home:
            case Key_PageUp:

                if (m_cursor > 0)
                {
                    onCursorMove(m_cursor);
                }

                m_cursor = 0;
                m_select = 0;
                m_timeBlink = timeBlink;

                break;

            // Fin
            case Key_End:
            case Key_PageDown:

                if (m_cursor < m_params.text.getSize())
                {
                    onCursorMove(m_cursor);
                }

                m_cursor = m_params.text.getSize();
                m_select = 0;
                m_timeBlink = timeBlink;

                break;

            // Supprimer
            case Key_Delete:

                if (m_editable && m_params.text.getSize() > 0)
                {
                    if (m_select != 0)
                    {
                        if (m_select < 0)
                        {
                            m_oldText = m_params.text;
                            m_params.text = m_params.text.subString(0, m_cursor + m_select) + m_params.text.subString(m_cursor);
                            m_cursor += m_select;

                            onCursorMove(m_cursor);
                        }
                        else
                        {
                            m_oldText = m_params.text;
                            m_params.text = m_params.text.subString(0, m_cursor) + m_params.text.subString(m_cursor + m_select);
                        }

                        m_timeBlink = timeBlink;
                        m_select = 0;

                        onTextChange();
                    }
                    else if (m_cursor < m_params.text.getSize())
                    {
                        m_oldText = m_params.text;
                        m_params.text = m_params.text.subString(0, m_cursor) + m_params.text.subString(m_cursor + 1);
                        m_timeBlink = timeBlink;

                        onTextChange();
                    }
                }

                break;

            // Effacer
            case Key_Backspace:

                if (m_editable && m_params.text.getSize() > 0)
                {
                    if (m_select)
                    {
                        if (m_select < 0)
                        {
                            m_oldText = m_params.text;
                            m_params.text = m_params.text.subString(0, m_cursor + m_select) + m_params.text.subString(m_cursor);
                            m_cursor += m_select;

                            onCursorMove(m_cursor);
                        }
                        else
                        {
                            m_oldText = m_params.text;
                            m_params.text = m_params.text.subString(0, m_cursor) + m_params.text.subString(m_cursor + m_select);
                        }

                        m_timeBlink = timeBlink;
                        m_select = 0;

                        onTextChange();
                    }
                    else if (m_cursor > 0)
                    {
                        m_oldText = m_params.text;
                        m_params.text = m_params.text.subString(0, m_cursor - 1) + m_params.text.subString(m_cursor);
                        m_timeBlink = timeBlink;
                        --m_cursor;

                        onTextChange();
                        onCursorMove(m_cursor);
                    }
                }

                break;

            // Flèche gauche
            case Key_Left:

                if (m_select)
                {
                    if (m_select < 0)
                    {
                        m_cursor += m_select;
                        onCursorMove(m_cursor);
                    }

                    m_select = 0;
                }
                else if (m_cursor > 0)
                {
                    --m_cursor;
                    onCursorMove(m_cursor);
                }

                m_timeBlink = timeBlink;
                break;

            // Flèche droite
            case Key_Right:

                if (m_select)
                {
                    if (m_select > 0)
                    {
                        m_cursor += m_select;
                        onCursorMove(m_cursor);
                    }

                    m_select = 0;
                }
                else if (m_cursor < m_params.text.getSize())
                {
                    ++m_cursor;
                    onCursorMove(m_cursor);
                }

                m_timeBlink = timeBlink;
                break;

            // Touche Entrée
            case Key_Enter:
                onEnter();
                return;

            // Autres touches
            default: break;
        }
    }
}


/**
 * Efface le texte.
 ******************************/

void ITextEdit::clear()
{
    m_oldText = m_params.text;
    m_params.text.clear();

    m_cursor = 0;
    m_select = 0;

    onTextChange();
    onClear();
}


/**
 * Sélectionne tout le texte et place le curseur au début.
 ******************************/

void ITextEdit::selectAll()
{
    m_cursor = 0;
    m_select = m_params.text.getSize();
}


/**
 * Copie le texte sélectionné dans le presse-papier.
 *
 * \todo Implémentation sous Linux.
 ******************************/

void ITextEdit::copySelectedText() const
{
    if (m_select == 0)
    {
        return;
    }

    CString selectedText;

    if (m_select < 0)
    {
        selectedText = m_params.text.subString(m_cursor + m_select, m_cursor);
    }
    else
    {
        selectedText = m_params.text.subString(m_cursor, m_cursor + m_select);
    }

    const char * selectedTextUTF8 = selectedText.toUTF8();
    unsigned int textLen = strlen(selectedTextUTF8);

#ifdef T_SYSTEM_WINDOWS

    if (OpenClipboard(NULL))
    {
        EmptyClipboard();

        // Allocate a global memory object for the text.
        HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (textLen + 1) * sizeof(TCHAR));

        if (hglbCopy != NULL)
        {
            // Lock the handle and copy the text to the buffer.
            LPTSTR lptstrCopy = (LPTSTR) GlobalLock(hglbCopy);
            memcpy(lptstrCopy, selectedTextUTF8, textLen * sizeof(TCHAR));
            lptstrCopy[textLen] = (TCHAR) 0; // null character
            GlobalUnlock(hglbCopy);

            // Place the handle on the clipboard.
            SetClipboardData(CF_TEXT, hglbCopy);
        }

        CloseClipboard();
    }

#endif // T_SYSTEM_WINDOWS

}


/**
 * Efface le texte sélectionné.
 ******************************/

void ITextEdit::removeSelectedText()
{
    if (m_editable && m_params.text.getSize() > 0 && m_select != 0)
    {
        if (m_select < 0)
        {
            m_oldText = m_params.text;
            m_params.text = m_params.text.subString(0, m_cursor + m_select) + m_params.text.subString(m_cursor);
            m_cursor += m_select;

            onCursorMove(m_cursor);
        }
        else
        {
            m_oldText = m_params.text;
            m_params.text = m_params.text.subString(0, m_cursor) + m_params.text.subString(m_cursor + m_select);
        }

        m_timeBlink = timeBlink;
        m_select = 0;

        onTextChange();
    }
}


/**
 * Colle le texte du presse-papier au niveau du curseur.
 *
 * \todo Implémentation sous Linux.
 ******************************/

void ITextEdit::paste()
{
    removeSelectedText();

#ifdef T_SYSTEM_WINDOWS

    if (OpenClipboard(NULL))
    {
        HANDLE handle = GetClipboardData(CF_TEXT);

        if (handle != NULL)
        {
            CString newText = reinterpret_cast<char *>(handle);
            m_params.text = m_params.text.subString(0, m_cursor) + newText + m_params.text.subString(m_cursor);
            m_cursor += newText.getSize();
        }

        CloseClipboard();
    }

#endif // T_SYSTEM_WINDOWS

}

} // Namespace Ted

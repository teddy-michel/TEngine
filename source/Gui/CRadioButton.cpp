/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CRadioButton.cpp
 * \date       2008 Création de la classe GuiRadioButton.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CRadioButton.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CRadioButton.hpp"
#include "Gui/CLabel.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

const unsigned int RadioButtonRadius = 5; ///< Rayon du bouton en pixels.
const unsigned int RadioButtonSize = 10;  ///< Hauteur du widget en pixels (doit être supérieure au diamètre du bouton).


/**
 * Constructeur par défaut.
 *
 * \param text   Texte du bouton.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CRadioButton::CRadioButton(const CString& text, IWidget * parent) :
IButton (text, parent),
m_label (nullptr)
{
    m_label = new CLabel(text, this);
    m_label->setPosition(RadioButtonSize * 2, 2);

    setMinSize(RadioButtonSize * 2 + m_label->getWidth(), RadioButtonSize * 2);
    setMaxHeight(RadioButtonSize * 2);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CRadioButton::CRadioButton(IWidget * parent) :
IButton (CString(), parent),
m_label (nullptr)
{
    m_label = new CLabel(CString(), this);
    m_label->setPosition(RadioButtonSize * 2, 2);

    setMinSize(RadioButtonSize * 2 + m_label->getWidth(), RadioButtonSize * 2);
    setMaxHeight(RadioButtonSize * 2);
}


/**
 * Destructeur.
 ******************************/

CRadioButton::~CRadioButton()
{
    delete m_label;
}


/**
 * Modifie le texte du bouton.
 *
 * \param text Texte du bouton.
 ******************************/

void CRadioButton::setText(const CString& text)
{
    IButton::setText(text);

    if (m_label)
    {
        m_label->setText(text);
        setMinWidth(RadioButtonSize * 2 + m_label->getWidth());
    }
}


/**
 * Dessine le bouton.
 ******************************/

void CRadioButton::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    // Bordure du bouton
    Game::guiEngine->drawDisc(tmp_x + RadioButtonSize, tmp_y + RadioButtonSize, RadioButtonRadius, CColor(28, 38, 60));

    // Fond du bouton
    if (Game::guiEngine->isPointed(this))
    {
        Game::guiEngine->drawDisc(tmp_x + RadioButtonSize, tmp_y + RadioButtonSize, RadioButtonRadius - 1, CColor(225, 225, 225));
    }
    else
    {
        Game::guiEngine->drawDisc(tmp_x + RadioButtonSize, tmp_y + RadioButtonSize, RadioButtonRadius - 1, CColor::White);
    }

    // Disque intérieur
    if (m_down)
    {
        Game::guiEngine->drawDisc(tmp_x + RadioButtonSize, tmp_y + RadioButtonSize, RadioButtonRadius - 2, CColor(156, 255, 0));
    }

    // Affichage du texte
    if (m_label)
    {
        m_label->draw();
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CRadioButton::onEvent(const CMouseEvent& event)
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    if (event.button == MouseButtonLeft && event.type == ButtonReleased &&
        static_cast<int >(event.x) >= tmp_x &&
        static_cast<int >(event.x) <= tmp_x + static_cast<int >(m_width) &&
        static_cast<int >(event.y) >= tmp_y &&
        static_cast<int >(event.y) <= tmp_y + static_cast<int >(m_height) &&
        Game::guiEngine->isSelected(this ))
    {
        onClicked();
        toggleDown();
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IWidget.cpp
 * \date       2008 Création de la classe GuiWidget.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 11/05/2011 On peut activer et désactiver un objet.
 * \date 29/05/2011 La classe est renommée en IWidget.
 * \date 20/04/2013 Les widgets possèdent une liste d'objets enfants.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IWidget.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/ILogger.hpp"
#include "Core/Events.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

IWidget::IWidget(IWidget * parent) :
m_enable    (true),

#ifdef T_USE_PIXMAP
m_needRepaint (true),
#endif

m_x         (0),
m_y         (0),
m_width     (0),
m_height    (0),
m_minWidth  (0),
m_minHeight (0),
m_maxWidth  (std::numeric_limits<int>::max()),
m_maxHeight (std::numeric_limits<int>::max()),
m_parent    (parent)
{
    if (m_parent)
    {
        m_parent->addChild(this);
    }
}


/**
 * Détruit le widget et tous ses descendants.
 ******************************/

IWidget::~IWidget()
{
    for (std::list<IWidget *>::iterator it = m_children.begin(); it != m_children.end(); /*++it*/)
    {
        IWidget * child = *it;
        it = m_children.erase(it);
        child->m_parent = nullptr;
        delete child;
    }

    if (m_parent)
    {
        m_parent->removeChild(this);
    }
}


/**
 * Donne la position horizontale de l'objet.
 *
 * \return Coordonnée horizontale.
 *
 * \sa IWidget::setX
 ******************************/

int IWidget::getX() const
{
    if (m_parent)
    {
        return m_parent->getX() + m_x;
    }
    else
    {
        return m_x;
    }
}


/**
 * Donne la position verticale de l'objet.
 *
 * \return Coordonnée verticale.
 *
 * \sa IWidget::setY
 ******************************/

int IWidget::getY() const
{
    if (m_parent)
    {
        return m_parent->getY() + m_y;
    }
    else
    {
        return m_y;
    }
}


/**
 * Indique si le widget et ses ancêtres sont actifs ou pas.
 *
 * \return Booléen.
 *
 * \sa IWidget::setEnable
 * \sa IWidget::isEnable
 ******************************/

bool IWidget::isHierarchyEnable() const
{
    return (m_parent ? m_enable && m_parent->isHierarchyEnable() : m_enable);
}


/**
 * Modifie la position de l'objet.
 *
 * \param x Coordonnée horizontale.
 * \param y Coordonnée verticale.
 *
 * \sa IWidget::setX
 * \sa IWidget::setY
 ******************************/

void IWidget::setPosition(int x, int y)
{
    setX(x);
    setY(y);
}


/**
 * Modifie la position horizontale du widget.
 *
 * \param x Coordonnée horizontale.
 *
 * \sa IWidget::getX
 * \sa IWidget::setY
 * \sa IWidget::setPosition
 ******************************/

void IWidget::setX(int x)
{
    m_x = x;
}


/**
 * Modifie la position verticale du widget.
 *
 * \param y Coordonnée verticale.
 *
 * \sa IWidget::getY
 * \sa IWidget::setX
 * \sa IWidget::setPosition
 ******************************/

void IWidget::setY(int y)
{
    m_y = y;
}


/**
 * Modifie la largeur du widget.
 *
 * \param width Largeur de l'objet en pixels.
 *
 * \sa IWidget::setHeight
 * \sa IWidget::setSize
 ******************************/

void IWidget::setWidth(int width)
{
    if (width < 0)
        width = 0;

         if (width < m_minWidth) m_width = m_minWidth;
    else if (width > m_maxWidth) m_width = m_maxWidth;
    else                         m_width = width;
}


/**
 * Modifie la hauteur du widget.
 *
 * \param height Hauteur de l'objet en pixels.
 *
 * \sa IWidget::setWidth
 * \sa IWidget::setSize
 ******************************/

void IWidget::setHeight(int height)
{
    if (height < 0)
        height = 0;

         if (height < m_minHeight) m_height = m_minHeight;
    else if (height > m_maxHeight) m_height = m_maxHeight;
    else                           m_height = height;
}


/**
 * Modifie les dimensions de l'objet.
 *
 * \param height Hauteur de l'objet en pixels.
 * \param width  Largeur de l'objet en pixels.
 *
 * \sa IWidget::setWidth
 * \sa IWidget::setHeight
 ******************************/

void IWidget::setSize(int width, int height)
{
    setWidth(width);
    setHeight(height);
}


/**
 * Modifie la largeur minimale de l'objet.
 *
 * \param width Largeur minimale de l'objet en pixels.
 *
 * \sa IWidget::setMinHeight
 * \sa IWidget::setMinSize
 ******************************/

void IWidget::setMinWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width > m_maxWidth)
    {
        m_minWidth = width;
        m_maxWidth = width;
    }
    else
    {
        m_minWidth = width;
    }

    if (m_width < m_minWidth)
    {
        setWidth(m_minWidth);
    }
}


/**
 * Modifie la hauteur minimale de l'objet.
 *
 * \param height Hauteur minimale de l'objet en pixels.
 *
 * \sa IWidget::setMinWidth
 * \sa IWidget::setMinSize
 ******************************/

void IWidget::setMinHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height > m_maxHeight)
    {
        m_minHeight = height;
        m_maxHeight = height;
    }
    else
    {
        m_minHeight = height;
    }

    if (m_height < m_minHeight)
    {
        setHeight(m_minHeight);
    }
}


/**
 * Modifie les dimensions minimales de l'objet.
 *
 * \param width  Largeur minimale de l'objet en pixels.
 * \param height Hauteur minimale de l'objet en pixels.
 *
 * \sa IWidget::setMinWidth
 * \sa IWidget::setMinHeight
 ******************************/

void IWidget::setMinSize(int width, int height)
{
    setMinWidth(width);
    setMinHeight(height);
}


/**
 * Modifie la largeur maximale de l'objet.
 *
 * \param width Largeur maximale de l'objet en pixels.
 *
 * \sa IWidget::setMaxHeight
 * \sa IWidget::setMaxSize
 ******************************/

void IWidget::setMaxWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width < m_minWidth)
    {
        m_minWidth = width;
        m_maxWidth = width;
    }
    else
    {
        m_maxWidth = width;
    }

    if (m_width > m_maxWidth)
    {
        m_width = m_maxWidth;
    }
}


/**
 * Modifie la hauteur maximale de l'objet.
 *
 * \param height Hauteur maximale de l'objet en pixels.
 *
 * \sa IWidget::setMaxWidth
 * \sa IWidget::setMaxSize
 ******************************/

void IWidget::setMaxHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height < m_minHeight)
    {
        m_minHeight = height;
        m_maxHeight = height;
    }
    else
    {
        m_maxHeight = height;
    }

    if (m_height > m_maxHeight)
    {
        m_height = m_maxHeight;
    }
}


/**
 * Modifie les dimensions maximales de l'objet.
 *
 * \param width  Largeur maximale de l'objet en pixels.
 * \param height Hauteur maximale de l'objet en pixels.
 *
 * \sa IWidget::setMaxWidth
 * \sa IWidget::setMaxHeight
 ******************************/

void IWidget::setMaxSize(int width, int height)
{
    setMaxWidth(width);
    setMaxHeight(height);
}


/**
 * Modifie l'objet parent.
 *
 * \param parent Pointeur sur l'objet parent.
 *
 * \sa IWidget::getParent
 ******************************/

void IWidget::setParent(IWidget * const parent)
{
    // Rien à faire
    if (parent == this || parent == m_parent)
        return;

    // parent est un descendant
    if (isAncestorOf(parent))
        return;

    // Modification de l'ancien parent
    if (m_parent)
    {
        IWidget * oldParent = m_parent;
        m_parent = nullptr;
        oldParent->removeChild(this);
    }

    m_parent = parent;

    // Modification du nouveau parent
    if (m_parent)
    {
        m_parent->addChild(this);
    }
}


/**
 * Ajoute un objet enfant au widget.
 *
 * \param widget Pointeur sur l'objet à ajouter.
 ******************************/

void IWidget::addChild(IWidget * const widget)
{
    if (widget == this || widget == nullptr)
        return;

    // Rien à faire
    if (hasChild(widget))
        return;

    // widget est un ancêtre
    if (widget->isAncestorOf(this))
        return;

    // On retire le widget de la hiérarchie
    widget->setParent(nullptr);

    m_children.push_back(widget);
    widget->m_parent = this; //widget->setParent(this) amène pleins d'instructions inutiles
}


/**
 * Enlève un objet enfant au widget.
 *
 * \param widget Pointeur sur l'objet à enlever.
 ******************************/

void IWidget::removeChild(IWidget * const widget)
{
    if (widget == nullptr)
        return;

    std::list<IWidget *>::iterator it = std::find(m_children.begin(), m_children.end(), widget);

    if (it != m_children.end())
    {
        m_children.erase(it);
        widget->m_parent = nullptr; //widget->setParent(nullptr) amène pleins d'instructions inutiles
    }
}


/**
 * Indique si ce widget est un ancêtre d'un autre.
 *
 * \param widget Widget à rechercher parmi les descendants.
 * \return Booléen.
 ******************************/

bool IWidget::isAncestorOf(IWidget * const widget) const
{
    if (widget == nullptr)
        return false;

    if (widget == this)
        return true;

    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if ((*it)->isAncestorOf(widget))
            return true;
    }

    return false;
}


/**
 * Recherche un widget dans la liste des widgets enfants.
 *
 * \param widget Widget à rechercher parmi les enfants.
 * \return Booléen.
 ******************************/

bool IWidget::hasChild(IWidget * const widget) const
{
    if (widget == nullptr || widget == this)
        return false;

    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (*it == widget)
            return true;
    }

    return false;
}


/**
 * Active ou désactive l'objet.
 *
 * \param enable Booléen indiquant si l'objet est actif ou pas.
 *
 * \sa IWidget::isEnable
 ******************************/

void IWidget::setEnable(bool enable)
{
    m_enable = enable;
}


#ifdef T_USE_PIXMAP

void IWidget::getPixmap(CImage& pixmap) const
{
    //...
}


void IWidget::paint(CImage& pixmap)
{
    CImage subPixmap;
    getPixmap(subPixmap);
    // TODO: Fusion de subPixmap dans pixmap
    //...

    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        subPixmap.clear();
        it->getPixmap(subPixmap);
        // TODO: Fusion de subPixmap dans pixmap
        //...
    }
}


void IWidget::update(unsigned int frameTime)
{
    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        it->update(frameTime);
    }
}


void IWidget::repaint()
{
    m_needRepaint = true;

    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        it->repaint();
    }
}


bool IWidget::needRepaint() const
{
    if (!m_needRepaint)
    {
        for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
        {
            if (it->needRepaint())
            {
                return true;
            }
        }
    }

    return m_needRepaint;
}

#endif



/**
 * Indique le curseur à utiliser en fonction de la position du curseur de la souris.
 *
 * \param x Coordonnée horizontale du curseur.
 * \param y Coordonnée verticale du curseur.
 * \return Curseur à utiliser (flèche).
 ******************************/

TCursor IWidget::getCursorType(int x, int y) const
{
    T_UNUSED(x);
    T_UNUSED(y);

    return CursorArrow;
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void IWidget::onEvent(const CResizeEvent& event)
{
    for (std::list<IWidget *>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        (*it)->onEvent(event);
    }
}

} // Namespace Ted

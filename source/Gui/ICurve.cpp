/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ICurve.cpp
 * \date 19/04/2013 Création de la classe ICurve.
 * \date 20/04/2013 Améliorations.
 * \date 13/06/2014 Correction d'un bug d'affichage des barres verticales.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/ICurve.hpp"
#include "Gui/CGuiEngine.hpp"


namespace Ted
{

/**
 * Construit une courbe.
 *
 * \param parent Pointeur sur le widget parent.
 ******************************/

ICurve::ICurve(IWidget * parent) :
IWidget (parent),
m_color (CColor::Black),
m_mode  (Point),
m_minX  (0.0f),
m_maxX  (1.0f),
m_minY  (0.0f),
m_maxY  (1.0f)
{

}


/**
 * Détruit la courbe.
 ******************************/

ICurve::~ICurve()
{

}


/**
 * Modifie la couleur de la courbe.
 *
 * \param color Couleur de la courbe.
 ******************************/

void ICurve::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Modifie le mode d'affichage de la courbe.
 *
 * \param mode Mode d'affichage de la courbe.
 ******************************/

void ICurve::setMode(TMode mode)
{
    m_mode = mode;
}


void ICurve::setCurveRect(float minX, float maxX, float minY, float maxY)
{
    m_minX = (minX < maxX ? minX : maxX);
    m_maxX = (minX < maxX ? maxX : minX);

    m_minY = (minY < maxY ? minY : maxY);
    m_maxY = (minY < maxY ? maxY : minY);
}


/**
 * Dessine la courbe.
 ******************************/

#ifdef T_USE_PIXMAP
void ICurve::draw(CImage& pixmap)
#else
void ICurve::draw()
#endif
{
#ifdef T_USE_PIXMAP
    const float ratioX = (m_maxX - m_minX) / pixmap.getWidth();
    const float ratioY = pixmap.getHeight() / (m_maxY - m_minY);

    int pixelX = 0;
#else
    //Game::guiEngine->drawRectangle(CRectangle(getX(), getY(), getWidth(), getHeight()), CColor(0, 0, 255, 128));

    const float ratioX = (m_maxX - m_minX) / getWidth();
    const float ratioY = getHeight() / (m_maxY - m_minY);

    int pixelX = getX();
#endif

    for (float x = m_minX; x < m_maxX; x += ratioX, ++pixelX)
    {
        float y = getCurveYFromX(x);

        if (y < m_minY || y > m_maxY)
            continue;

#ifdef T_USE_PIXMAP
        int pixelY = (y - minY) * ratioY;
#else
        int bottom = getY() + getHeight();
        int pixelY = bottom - (y - m_minY) * ratioY;
#endif

        switch (m_mode)
        {
            default:
            case Point:
#ifdef T_USE_PIXMAP
                pixmap.setPixel(pixelX, pixelY, m_color);
#else
                Game::guiEngine->drawPoint(TVector2F(pixelX, pixelY), m_color);
#endif
                break;

            case VLine:

#ifdef T_USE_PIXMAP
                for (int pixel = pixelY; pixel <= getHeight(); ++pixel)
                {
                    pixmap.setPixel(pixelX, pixel, m_color);
                }
#else
                Game::guiEngine->drawLine(TVector2F(pixelX, bottom), TVector2F(pixelX, pixelY), m_color);
#endif

                break;
        }
    }
}

} // Namespace Ted

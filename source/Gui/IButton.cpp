/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IButton.cpp
 * \date       2008 Création de la classe GuiBaseButton.
 * \date 29/05/2011 La classe est renommée en IButton.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IButton.hpp"
#include "Gui/CButtonGroup.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param text   Texte du bouton.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

IButton::IButton(const CString& text, IWidget * parent) :
IWidget   (parent),
m_down    (false),
m_text    (text),
m_group   (nullptr)
{ }


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

IButton::IButton(IWidget * parent) :
IWidget   (parent),
m_down    (false),
m_text    (),
m_group   (nullptr)
{ }


/**
 * Destructeur.
 ******************************/

IButton::~IButton()
{ }


/**
 * Indique si le bouton est enfoncé ou pas.
 *
 * \return Booléen.
 *
 * \sa IButton::setDown
 ******************************/

bool IButton::isDown() const
{
    return m_down;
}


/**
 * Donne le texte du bouton.
 *
 * \return Texte du bouton.
 *
 * \sa IButton::setText
 ******************************/

CString IButton::getText() const
{
    return m_text;
}


/**
 * Donne le groupe contenant le bouton.
 *
 * \return Pointeur sur le groupe.
 *
 * \sa IButton::setGroup
 ******************************/

CButtonGroup * IButton::getGroup() const
{
    return m_group;
}


/**
 * Mutateur pour down.
 *
 * \param down Indique si le bouton doit être enfoncé ou pas.
 *
 * \sa IButton::ToggleDown
 * \sa IButton::isDown
 ******************************/

void IButton::setDown(bool down)
{
    if (m_down != down)
    {
        m_down = down;

        if (m_group)
        {
            m_group->buttonState(this);
        }
    }
}


/**
 * Inverse la valeur de down.
 *
 * \sa IButton::setDown
 * \sa IButton::isDown
 ******************************/

void IButton::toggleDown()
{
    m_down = !m_down;

    if (m_group)
    {
        m_group->buttonState(this);
    }
}


/**
 * Modifie le texte du bouton.
 *
 * \param text Texte du bouton.
 *
 * \sa IButton::getText
 ******************************/

void IButton::setText(const CString& text)
{
    m_text = text;
}


/**
 * Change le groupe contenant le bouton.
 *
 * \param group Pointeur sur le groupe.
 *
 * \sa IButton::getGroup
 ******************************/

void IButton::setGroup(CButtonGroup * group)
{
    m_group = group;
}

} // Namespace Ted

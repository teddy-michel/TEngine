/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CScrollArea.cpp
 * \date 27/06/2014 Création de la classe CScrollArea.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CScrollArea.hpp"
#include "Gui/CScrollBar.hpp"
#include "Gui/CGuiEngine.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CScrollArea::CScrollArea(IWidget * parent) :
IScrollArea (parent),
m_widget    (nullptr)
{
    m_vScroll->setHeight(m_height);
    m_vScroll->setStep(m_height);
    m_vScroll->setSingleStep(1);

    m_hScroll->setWidth(m_width);
    m_hScroll->setStep(m_width);
    m_hScroll->setSingleStep(1);

    setFrameSize(m_width, m_height);
}


void CScrollArea::setWidget(IWidget * widget)
{
    if (m_widget != nullptr)
    {
        removeChild(m_widget);
    }

    m_widget = widget;
    addChild(widget);
}


IWidget * CScrollArea::getWidget() const
{
    return m_widget;
}


/**
 * Dessine le conteneur.
 ******************************/

void CScrollArea::draw()
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    const int posX = getX();
    const int posY = getY();

    const TVector2I size = getWidgetSize();

    // Fond du champ
    Game::guiEngine->drawRectangle(CRectangle(posX, posY, size.X, size.Y), CColor::White);
    Game::guiEngine->drawBorder(CRectangle(posX, posY, size.X, size.Y), CColor(28, 38, 60));

    // Affichage des ascenseurs
    if (isShowVerticalScrollBar())
    {
        m_vScroll->draw();
    }

    if (isShowHorizontalScrollBar())
    {
        m_hScroll->draw();
    }

    // Affichage du widget interne
    if (m_widget != nullptr)
    {
        // TODO : limiter la zone à dessiner avec un CRectangle
        m_widget->draw();
    }
}


/**
 * Indique le curseur à utiliser en fonction de la position du curseur de la souris.
 *
 * \param x Coordonnée horizontale du curseur.
 * \param y Coordonnée verticale du curseur.
 * \return Curseur à utiliser.
 ******************************/

TCursor CScrollArea::getCursorType(int x, int y) const
{
    // Affichage des ascenseurs
    if (isShowVerticalScrollBar() && x > m_vScroll->getX())
    {
        return CursorArrow;
    }

    if (isShowHorizontalScrollBar() && y > m_hScroll->getY())
    {
        return CursorArrow;
    }

    if (m_widget != nullptr)
    {
        return m_widget->getCursorType(x, y);
    }

    return CursorArrow;
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CScrollArea::onEvent(const CTextEvent& event)
{
    if (m_widget != nullptr)
    {
        m_widget->onEvent(event);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CScrollArea::onEvent(const CKeyboardEvent& event)
{
    if (m_widget != nullptr)
    {
        m_widget->onEvent(event);
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CScrollArea::onEvent(const CMouseEvent& event)
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    // Ascenseurs
    if (isShowVerticalScrollBar())
    {
        const int posX = m_vScroll->getX();
        const int posY = m_vScroll->getY();

        if (static_cast<int>(event.x) >= posX &&
            static_cast<int>(event.x) <= posX + m_vScroll->getWidth() &&
            static_cast<int>(event.y) >= posY &&
            static_cast<int>(event.y) <= posY + m_vScroll->getHeight())
        {
            m_vScroll->onEvent(event);
            return;
        }
    }

    if (isShowHorizontalScrollBar())
    {
        const int posX = m_hScroll->getX();
        const int posY = m_hScroll->getY();

        if (static_cast<int>(event.x) >= posX &&
            static_cast<int>(event.x) <= posX + m_hScroll->getWidth() &&
            static_cast<int>(event.y) >= posY &&
            static_cast<int>(event.y) <= posY + m_hScroll->getHeight())
        {
            m_hScroll->onEvent(event);
            return;
        }
    }

    if (m_widget != nullptr)
    {
        m_widget->onEvent(event);
    }
}

} // Namespace Ted

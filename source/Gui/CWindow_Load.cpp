/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow_Load.cpp
 * \date 28/05/2009 Création de la classe GuiWindow_Load.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CWindow_Load.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindow_Load.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CLayoutGrid.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow_Load::CWindow_Load(IWidget * parent) :
CWindow ("Charger une partie", WindowCenter, parent)
{
    setMinSize(400, 240);

    m_button_close = new CPushButton("Fermer");
    m_button_close->onClicked.connect(sigc::mem_fun(this, &CWindow::close));

    // Layout
    CLayoutGrid * layout = new CLayoutGrid(this);
    layout->addChild(m_button_close, 0, 0, AlignMiddleLeft);

    CPushButton * button2 = new CPushButton("Test 1");
    button2->setMaxWidth(300);
    button2->setEnable(false);
    layout->addChild(button2, 2, 1, 2, 1);

    CPushButton * button3 = new CPushButton("Test 2");
    button3->setMaxWidth(260);
    layout->addChild(button3, 2, 0);

    setLayout(layout);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutGrid.cpp
 * \date 18/01/2010 Création de la classe GuiLayoutGrid.
 * \date 03/07/2010 Modification de la gestion des évènements.
 * \date 15/07/2010 Ajout de l'alignement des widgets.
 * \date 24/07/2010 Amélioration de la méthode ComputeSize.
 * \date 25/07/2010 Amélioration de la méthode ComputeSize.
 * \date 26/07/2010 Gestion des évènements.
 * \date 29/05/2011 La classe est renommée en CLayoutGrid.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>

#include "Gui/CLayoutGrid.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Events.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLayoutGrid::CLayoutGrid(IWidget * parent) :
ILayout     (parent),
m_nbr_rows  (0),
m_nbr_cols  (0)
{ }


/**
 * Donne le nombre de lignes de la grille.
 *
 * \return Nombre de lignes.
 ******************************/

int CLayoutGrid::getNumRows() const
{
    return m_nbr_rows;
}


/**
 * Donne le nombre de colonnes de la grille.
 *
 * \return Nombre de colonnes.
 ******************************/

int CLayoutGrid::getNumCols() const
{
    return m_nbr_cols;
}


/**
 * Donne la hauteur minimale d'une ligne.
 *
 * \param row Numéro de la ligne.
 * \return Hauteur minimale de la ligne.
 ******************************/

int CLayoutGrid::getRowMinHeight(int row) const
{
    return (row >= 0 && row < m_nbr_rows ? m_rows_min_height[row] : 0);
}


/**
 * Donne la largeur minimale d'une colonne.
 *
 * \param col Numéro de la colonne.
 * \return Largeur minimale de la colonne.
 ******************************/

int CLayoutGrid::getColumnMinWidth(int col) const
{
    return (col >= 0 && col < m_nbr_cols ? m_cols_min_width[col] : 0);
}


/**
 * Modifie la hauteur minimale d'une ligne.
 *
 * \param row Numéro de la ligne.
 * \param min Hauteur minimale de la ligne.
 ******************************/

void CLayoutGrid::setRowMinHeight(int row, int min)
{
    if (min < 0)
        min = 0;

    if (row >= 0 && row < m_nbr_rows)
    {
        m_rows_min_height[row] = min;
        computeSize();
    }
}


/**
 * Modifie la largeur minimale d'une colonne.
 *
 * \param col Numéro de la colonne.
 * \param min Largeur minimale de la colonne.
 ******************************/

void CLayoutGrid::setColMinWidth(int col, int min)
{
    if (min < 0)
        min = 0;

    if (col >= 0 && col < m_nbr_cols)
    {
        m_cols_min_width[col] = min;
        computeSize();
    }
}


/**
 * Dessine le layout.
 *
 * \todo Implémentation.
 ******************************/

void CLayoutGrid::draw()
{
    computeSize();

    // Affichage du contenu
    for (std::list<TLayoutItem>::const_iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        it->widget->draw();
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CLayoutGrid::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    // On parcourt la liste des objets enfants
    for (std::list<TLayoutItem>::iterator it = m_children.begin() ; it != m_children.end() ; ++it)
    {
        int tmp_x = it->widget->getX();
        int tmp_y = it->widget->getY();

        if (static_cast<int>(event.x) >= tmp_x &&
            static_cast<int>(event.x) <= tmp_x + it->widget->getWidth() &&
            static_cast<int>(event.y) >= tmp_y &&
            static_cast<int>(event.y) <= tmp_y + it->widget->getHeight())
        {
            if (event.type == ButtonPressed || event.type == DblClick)
            {
                if (event.button == MouseButtonLeft)
                {
                    Game::guiEngine->setSelected(it->widget);
                }

                Game::guiEngine->setFocus(it->widget);
            }

            Game::guiEngine->setPointed(it->widget);

            it->widget->onEvent(event);
            return;
        }
    }
}


/**
 * Ajoute un objet à la grille.
 *
 * \param widget  Pointeur sur l'objet à ajouter.
 * \param row     Numéro de la ligne (commence à 0).
 * \param col     Numéro de la colonne (commence à 0).
 * \param rowspan Nombre de lignes occupées par l'objet.
 * \param colspan Nombre de colonnes occupées par l'objet.
 * \param align   Alignement de l'objet dans la case du layout.
 ******************************/

void CLayoutGrid::addChild(IWidget * widget, int row, int col, int rowspan, int colspan, TAlignment align)
{
    if (widget == nullptr)
        return;

    if (row < 0 || col < 0)
        return;

    if (rowspan < 1)
        rowspan = 1;

    if (colspan < 1)
        colspan = 1;

    // On parcourt le tableau pour vérifier que les cases sont libres
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        // Le widget est déjà dans le layout
        if (it->widget == widget)
        {
            return;
        }

        for (int i = 0; i < rowspan; ++i)
        {
            for (int j = 0; j < colspan; ++j)
            {
                if (row + i >= it->row && row + i < it->row + it->rowspan &&
                    col + j >= it->col && col + j < it->col + it->colspan)
                {
                    return;
                }
            }
        }
    }

    widget->setParent(this);

    // On ajoute l'objet à la liste
    TLayoutItem item;

    item.widget  = widget;
    item.row     = row;
    item.col     = col;
    item.rowspan = rowspan;
    item.colspan = colspan;
    item.align   = align;

    m_children.push_back(item);

    if (row + rowspan > m_nbr_rows)
    {
        for (int i = m_nbr_rows; i < row + rowspan; ++i)
        {
             m_rows_min_height.push_back(0);
        }

        m_nbr_rows = row + rowspan;
    }

    if (col + colspan > m_nbr_cols)
    {
        for (int i = m_nbr_cols; i < col + colspan; ++i)
        {
             m_cols_min_width.push_back(0);
        }

        m_nbr_cols = col + colspan;
    }

    computeSize();
}


/**
 * Ajoute un objet à la grille.
 *
 * \param widget Pointeur sur l'objet à ajouter.
 * \param row    Numéro de la ligne (commence à 0).
 * \param col    Numéro de la colonne (commence à 0).
 * \param align  Alignement de l'objet dans la case du layout.
 ******************************/

void CLayoutGrid::addChild(IWidget * widget, int row, int col, TAlignment align)
{
    addChild(widget, row, col, 1, 1, align);
}


/**
 * Enlève un objet de la grille.
 *
 * \param widget Pointeur sur l'objet à enlever.
 ******************************/

void CLayoutGrid::removeChild(IWidget * widget)
{
    if (widget == nullptr)
        return;

    // On parcourt la liste des objets enfants
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (it->widget == widget)
        {
            m_children.erase(it);
            computeSize();
            return;
        }
    }
}


/**
 * Enlève un objet de la grille et le supprime.
 * Si un objet occupe plusieurs lignes ou plusieurs colonnes, cette méthode
 * supprime l'objet si on indique n'importe quelle ligne ou colonne occupée
 * par l'objet.
 *
 * \param row Numéro de la ligne.
 * \param col Numéro de la colonne.
 ******************************/

void CLayoutGrid::removeChild(int row, int col)
{
    if (row < 0 || col < 0 || row >= m_nbr_rows || col >= m_nbr_cols)
    {
        return;
    }

    // On cherche l'objet se trouvant à cette position
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if (row >= it->row && row < it->row + it->rowspan &&
            col >= it->col && col < it->col + it->colspan)
        {
            m_children.erase(it);
            computeSize();
            return;
        }
    }
}


/**
 * Calcule les dimensions du layout.
 ******************************/

void CLayoutGrid::computeSize()
{
    // Dimensions minimales et maximales de chaque ligne et colonne
    std::vector<int> rows_min;
    std::vector<int> cols_min;
    std::vector<int> rows_max;
    std::vector<int> cols_max;

    rows_min.reserve(m_nbr_rows);
    cols_min.reserve(m_nbr_cols);
    rows_max.reserve(m_nbr_rows);
    cols_max.reserve(m_nbr_cols);

    for (int i = 0; i < m_nbr_rows; ++i)
    {
        rows_min[i] = m_rows_min_height[i];
        rows_max[i] = std::numeric_limits<int>::max();
    }

    for (int i = 0; i < m_nbr_cols; ++i)
    {
        cols_min[i] = m_cols_min_width[i];
        cols_max[i] = std::numeric_limits<int>::max();
    }

    // On cherche les dimensions minimales et maximales de chaque ligne et colonne
    for (std::list<TLayoutItem>::const_iterator it = m_children.begin() ; it != m_children.end() ; ++it)
    {
        int tmp_min_h = it->widget->getMinHeight() / it->rowspan;
        int tmp_max_h = it->widget->getMaxHeight() / it->rowspan;

        int tmp_min_w = it->widget->getMinWidth() / it->colspan;
        int tmp_max_w = it->widget->getMaxWidth() / it->colspan;

        for (int i = it->row; i < it->row + it->rowspan; ++i)
        {
            if (rows_min[i] < tmp_min_h)
                rows_min[i] = tmp_min_h;
            if (rows_max[i] > tmp_max_h)
                rows_max[i] = tmp_max_h;
        }

        for (int i = it->col; i < it->col + it->colspan; ++i)
        {
            if (cols_min[i] < tmp_min_w)
                cols_min[i] = tmp_min_w;
            if (cols_max[i] > tmp_max_w)
                cols_max[i] = tmp_max_w;
        }
    }


    // Dimensions minimales et maximales du layout
    int min_height = 0;
    int max_height = 0;
    int min_width = 0;
    int max_width = 0;

    for (int i = 0; i < m_nbr_rows; ++i)
    {
        min_height = SumInt(min_height, rows_min[i]);
        max_height = SumInt(max_height, rows_max[i]);
    }

    for (int i = 0; i < m_nbr_cols; ++i)
    {
        min_width = SumInt(min_width, cols_min[i]);
        max_width = SumInt(max_width, cols_max[i]);
    }

    min_width = SumInt(min_width, m_margin.getLeft() + m_margin.getRight() + (m_padding.getLeft() + m_padding.getRight()) * m_nbr_rows);
    max_width = SumInt(max_width, m_margin.getLeft() + m_margin.getRight() + (m_padding.getLeft() + m_padding.getRight()) * m_nbr_rows);

    min_height = SumInt(min_height, m_margin.getTop() + m_margin.getBottom() + (m_padding.getTop() + m_padding.getBottom()) * m_nbr_rows);
    max_height = SumInt(max_height, m_margin.getTop() + m_margin.getBottom() + (m_padding.getTop() + m_padding.getBottom()) * m_nbr_rows);

    setMinSize(min_width, min_height);
    setMaxSize(max_width, max_height);


    // Espace à répartir
    int residu_w = 0;
    int residu_h = 0;

    if (min_width < m_width)
    {
        residu_w = m_width - min_width;
    }

    if (min_height < m_height)
    {
        residu_h = m_height - min_height;
    }


    while (residu_h > 0)
    {
        bool no_change = true;

        // On parcourt chaque ligne pour répartir le résidu
        for (int i = 0; i < m_nbr_rows; ++i)
        {
            if (rows_min[i] < rows_max[i])
            {
                ++rows_min[i];
                --residu_h;
                no_change = false;

                if (residu_h == 0)
                    break;
            }
        }

        // Toutes les lignes sont à leur hauteur maximale
        if (no_change)
        {
            int nbr_first = residu_h % m_nbr_rows;
            int add_pixel = residu_h / m_nbr_rows;

            for (int i = 0; i < m_nbr_rows; ++i)
            {
                rows_min[i] += add_pixel;
                if (i < nbr_first)
                    ++rows_min[i];
            }

            residu_h = 0;
        }
    }


    while (residu_w > 0)
    {
        bool no_change = true;

        // On parcourt chaque colonne pour répartir le résidu
        for (int i = 0; i < m_nbr_cols; ++i)
        {
            if (cols_min[i] < cols_max[i])
            {
                ++cols_min[i];
                --residu_w;
                no_change = false;

                if (residu_w == 0)
                    break;
            }
        }

        // Toutes les colonnes sont à leur largeur maximale
        if (no_change)
        {
            int nbr_first = residu_w % m_nbr_cols;
            int add_pixel = residu_w / m_nbr_cols;

            for (int i = 0; i < m_nbr_cols; ++i)
            {
                cols_min[i] += add_pixel;
                if (i < nbr_first)
                    ++cols_min[i];
            }

            residu_w = 0;
        }
    }


    // On modifie la position et les dimensions des widgets
    for (std::list<TLayoutItem>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        // On calcule les dimensions du widget
        int size_x = 0;
        int size_y = 0;

        for (int i = it->col; i < it->col + it->colspan; ++i)
        {
            size_x += cols_min[i];
        }

        for (int i = it->row; i < it->row + it->rowspan; ++i)
        {
            size_y += rows_min[i];
        }

        it->widget->setSize(size_x, size_y);

        // On calcule la position du widget
        int pos_x = m_margin.getLeft();
        int pos_y = m_margin.getTop();

        for (int i = 0; i < it->col; ++i)
        {
            pos_x += cols_min[i] + m_padding.getLeft() + m_padding.getRight();
        }

        for (int i = 0; i < it->row; ++i)
        {
            pos_y += rows_min[i] + m_padding.getTop() + m_padding.getBottom();
        }

        switch (it->align)
        {
            case AlignTopLeft:
                it->widget->setPosition(pos_x + m_padding.getLeft(), pos_y + m_padding.getTop());
                break;

            case AlignTopCenter:
                it->widget->setPosition(pos_x + m_padding.getLeft() + (size_x - it->widget->getWidth()) / 2, pos_y + m_padding.getTop());
                break;

            case AlignTopRight:
                it->widget->setPosition(pos_x + m_padding.getLeft() + size_x - it->widget->getWidth(), pos_y + m_padding.getTop());
                break;

            case AlignMiddleLeft:
                it->widget->setPosition(pos_x + m_padding.getLeft(), pos_y + m_padding.getTop() + (size_y - it->widget->getHeight()) / 2);
                break;

            default:
            case AlignMiddleCenter:
                it->widget->setPosition(pos_x + m_padding.getLeft() + (size_x - it->widget->getWidth()) / 2, pos_y + m_padding.getTop() + (size_y - it->widget->getHeight()) / 2);
                break;

            case AlignMiddleRight:
                it->widget->setPosition(pos_x + m_padding.getLeft() + size_x - it->widget->getWidth(), pos_y + m_padding.getTop() + (size_y - it->widget->getHeight()) / 2);
                break;

            case AlignBottomLeft:
                it->widget->setPosition(pos_x + m_padding.getLeft(), pos_y + m_padding.getTop() + size_y - it->widget->getHeight());
                break;

            case AlignBottomCenter:
                it->widget->setPosition(pos_x + m_padding.getLeft() + (size_x - it->widget->getWidth()) / 2, pos_y + m_padding.getTop() + size_y - it->widget->getHeight());
                break;

            case AlignBottomRight:
                it->widget->setPosition(pos_x + m_padding.getLeft() + size_x - it->widget->getWidth(), pos_y + m_padding.getTop() + size_y - it->widget->getHeight());
                break;
        }
    }

    return;
}

} // Namespace Ted

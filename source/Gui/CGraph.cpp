/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGraph.cpp
 * \date 19/04/2013 Création de la classe CGraph.
 * \date 20/04/2013 Améliorations.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Gui/CGraph.hpp"
#include "Gui/ICurve.hpp"
#include "Gui/CGuiEngine.hpp"


namespace Ted
{

/**
 * Construit un nouveau graphe.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CGraph::CGraph(IWidget * parent) :
IWidget                 (parent),
m_updateDelay           (1000),
m_lastUpdateTime        (0),
m_title                 (),
m_horizontalGridSize    (20),
m_horizontalGridEnabled (0),
m_verticalGridSize      (20),
m_verticalGridEnabled   (0),
m_chartMinX             (0.0f),
m_chartMaxX             (100.0f),
m_chartMinY             (0.0f),
m_chartMaxY             (100.0f),
m_autoX                 (true),
m_autoY                 (true),
m_backgroundColor       (0, 0, 0, 0),
m_gridColor             (CColor::Grey),
m_graphMargins          (10)
{
    setMinSize(m_graphMargins.getLeft() + 20 + 50 + m_graphMargins.getRight(), m_graphMargins.getTop() + 20 + 50 + 20 + m_graphMargins.getBottom());
}


/**
 * Construit un nouveau graphe.
 *
 * \param title  Titre du graphe.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CGraph::CGraph(const CString& title, IWidget * parent) :
IWidget                 (parent),
m_updateDelay           (1000),
m_lastUpdateTime        (0),
m_title                 (title),
m_horizontalGridSize    (20),
m_horizontalGridEnabled (0),
m_verticalGridSize      (20),
m_verticalGridEnabled   (0),
m_chartMinX             (0.0f),
m_chartMaxX             (100.0f),
m_chartMinY             (0.0f),
m_chartMaxY             (100.0f),
m_autoX                 (true),
m_autoY                 (true),
m_backgroundColor       (0, 0, 0, 0),
m_gridColor             (CColor::Grey),
m_graphMargins          (10)
{
    setMinSize(m_graphMargins.getLeft() + 20 + 50 + m_graphMargins.getRight(), m_graphMargins.getTop() + 20 + 50 + 20 + m_graphMargins.getBottom());
}


/**
 * Détruit le graphique.
 ******************************/

CGraph::~CGraph()
{

}


/**
 * Ajoute une courbe du graphe.
 *
 * \param curve Pointeur sur la courbe à ajouter.
 ******************************/

void CGraph::addCurve(ICurve * curve)
{
    if (curve == nullptr)
        return;

    std::list<ICurve *>::const_iterator it = std::find(m_curves.begin(), m_curves.end(), curve);

    if (it == m_curves.end())
    {
        m_curves.push_back(curve);
        curve->setParent(this);
        curve->setSize(getWidth() - 20 - m_graphMargins.getLeft() - m_graphMargins.getRight(), getHeight() - 20 - 20 - m_graphMargins.getTop() - m_graphMargins.getBottom());
        curve->setPosition(m_graphMargins.getLeft() + 20, m_graphMargins.getTop() + 20);
    }
}


/**
 * Enlève une courbe du graphe.
 *
 * \param curve Pointeur sur la courbe à enlever.
 ******************************/

void CGraph::removeCurve(ICurve * curve)
{
    std::list<ICurve *>::iterator it = std::find(m_curves.begin(), m_curves.end(), curve);

    if (it != m_curves.end())
    {
        m_curves.erase(it);
        curve->setPosition(0, 0);

        if (curve->getParent() == this)
            curve->setParent(nullptr);
    }
}


/**
 * Retourne la liste des courbes du graphe
 *
 * \return Liste des courbes.
 ******************************/

std::list<ICurve *> CGraph::getCurves() const
{
    return m_curves;
}


/**
 * Supprime toutes les courbes du graphe.
 ******************************/

void CGraph::clearCurves()
{
    for (std::list<ICurve *>::iterator it = m_curves.begin(); it != m_curves.end(); /*++it*/)
    {
        ICurve * curve = *it;
        it = m_curves.erase(it);
        delete curve;
    }
}


/**
 * Retourne le délai de mise à jour des courbes.
 *
 * \return Délai de mise à jour des courbes.
 ******************************/

unsigned int CGraph::getUpdateDelay() const
{
    return m_updateDelay;
}


/**
 * Modifie le délai de mise à jour des courbes.
 *
 * \param delay Délai de mise à jour des courbes.
 ******************************/

void CGraph::setUpdateDelay(unsigned int delay)
{
    m_updateDelay = delay;
}


/**
 * Retourne le titre du graphique.
 *
 * \return Titre du graphique.
 ******************************/

CString CGraph::getTitle() const
{
    return m_title;
}


/**
 * Modifie le titre du graphique.
 *
 * \param title Titre du graphique.
 ******************************/

void CGraph::setTitle(const CString& title)
{
    m_title = title;
}


/**
 * Retourne la légende horizontale du graphe.
 *
 * \return Légende horizontale.
 ******************************/

CString CGraph::getHorizontalLegend() const
{
    return m_horizontalLegend;
}


/**
 * Modifie la légende horizontale du graphe.
 *
 * \param legend Légende horizontale.
 ******************************/

void CGraph::setHorizontalLegend(const CString& legend)
{
    m_horizontalLegend = legend;
}


/**
 * Retourne la légende verticale du graphe.
 *
 * \return Légende verticale.
 ******************************/

CString CGraph::getVerticalLegend() const
{
    return m_verticalLegend;
}


/**
 * Modifie la légende verticale du graphe.
 *
 * \param legend Légende verticale.
 ******************************/

void CGraph::setVerticalLegend(const CString& legend)
{
    m_verticalLegend = legend;
}


/**
 * Retourne la taille de la grille horizontale.
 *
 * \return Taille de la grille horizontale.
 ******************************/

int CGraph::getHorizontalGridSize() const
{
    return m_horizontalGridSize;
}


/**
 * Modifie la taille de la grille horizontale.
 * Si \c size est négatif ou nul, la grille horizontale est masquée.
 *
 * \param size Taille de la grille horizontale.
 ******************************/

void CGraph::setHorizontalGridSize(int size)
{
    if (size <= 0)
    {
        m_horizontalGridEnabled = 0;
        m_horizontalGridSize = 0;
    }
    else
    {
        m_horizontalGridSize = size;
    }
}


/**
 * Retourne la taille de la grille verticale.
 *
 * \return Taille de la grille verticale.
 ******************************/

int CGraph::getVerticalGridSize() const
{
    return m_verticalGridSize;
}


/**
 * Modifie la taille de la grille verticale.
 * Si \c size est négatif ou nul, la grille verticale est masquée.
 *
 * \param size Taille de la grille verticale.
 ******************************/

void CGraph::setVerticalGridSize(int size)
{
    if (size <= 0)
    {
        m_verticalGridEnabled = 0;
        m_verticalGridSize = 0;
    }
    else
    {
        m_verticalGridSize = size;
    }
}


/**
 * Modifie la taille de la grille.
 * Si \c size est négatif ou nul, la grille est masquée.
 *
 * \param size Taille de la grille.
 ******************************/

void CGraph::setGridSize(int size)
{
    if (size <= 0)
    {
        m_horizontalGridEnabled = 0;
        m_horizontalGridSize = 0;
        m_verticalGridEnabled = 0;
        m_verticalGridSize = 0;
    }
    else
    {
        m_horizontalGridSize = size;
        m_verticalGridSize = size;
    }
}


/**
 * Indique si la grille horizontale est affichée.
 *
 * \return Booléen.
 ******************************/

bool CGraph::isHorizontalGridEnabled() const
{
    return m_horizontalGridEnabled;
}


void CGraph::setHorizontalGridEnabled(bool enabled)
{
    if (enabled)
    {
        if (m_horizontalGridSize > 0)
            m_horizontalGridEnabled = 1;
    }
    else
    {
        m_horizontalGridEnabled = enabled;
    }
}


/**
 * Indique si la grille verticale est affichée.
 *
 * \return Booléen.
 ******************************/

bool CGraph::isVerticalGridEnabled() const
{
    return m_verticalGridEnabled;
}


void CGraph::setVerticalGridEnabled(bool enabled)
{
    if (enabled)
    {
        if (m_verticalGridSize > 0)
            m_verticalGridEnabled = 1;
    }
    else
    {
        m_verticalGridEnabled = enabled;
    }
}


float CGraph::getChartMinX()
{
    if (m_autoX)
        computeSize();

    return m_chartMinX;
}


void CGraph::setChartMinX(float minX)
{
    if (m_autoX)
        return;

    m_chartMinX = minX;
}


float CGraph::getChartMaxX()
{
    if (m_autoX)
        computeSize();

    return m_chartMaxX;
}


void CGraph::setChartMaxX(float maxX)
{
    if (m_autoX)
        return;

    m_chartMaxX = maxX;
}


float CGraph::getChartMinY()
{
    if (m_autoY)
        computeSize();

    return m_chartMinY;
}


void CGraph::setChartMinY(float minY)
{
    if (m_autoY)
        return;

    m_chartMinY = minY;
}


float CGraph::getChartMaxY()
{
    if (m_autoY)
        computeSize();

    return m_chartMaxY;
}


void CGraph::setChartMaxY(float maxY)
{
    if (m_autoY)
        return;

    m_chartMaxY = maxY;
}


bool CGraph::isAutoX() const
{
    return m_autoX;
}


void CGraph::setAutoX(bool autoX)
{
    m_autoX = autoX;
}


bool CGraph::isAutoY() const
{
    return m_autoY;
}


void CGraph::setAutoY(bool autoY)
{
    m_autoY = autoY;
}


/**
 * Dessine le graphe.
 *
 * \todo Afficher les légendes.
 ******************************/

void CGraph::draw()
{
    computeSize();

    // Fond du graphe
    Game::guiEngine->drawRectangle(CRectangle(getX(), getY(), getWidth(), getHeight()), m_backgroundColor);

    // Affichage du cadre
    Game::guiEngine->drawLine(TVector2F(getX() + m_graphMargins.getLeft() + 20, getY() + getHeight() - m_graphMargins.getBottom() - 20),
                              TVector2F(getX() + getWidth() - m_graphMargins.getRight(), getY() + getHeight() - m_graphMargins.getBottom() - 20),
                              m_gridColor); // Ligne du bas
    Game::guiEngine->drawLine(TVector2F(getX() + m_graphMargins.getLeft() + 20, getY() + m_graphMargins.getTop() + 20),
                              TVector2F(getX() + getWidth() - m_graphMargins.getRight(), getY() + m_graphMargins.getTop() + 20),
                              m_gridColor); // Ligne du haut
    Game::guiEngine->drawLine(TVector2F(getX() + m_graphMargins.getLeft() + 20, getY() + m_graphMargins.getTop() + 20),
                              TVector2F(getX() + m_graphMargins.getLeft() + 20, getY() + getHeight() - m_graphMargins.getBottom() - 20),
                              m_gridColor); // Ligne de gauche
    Game::guiEngine->drawLine(TVector2F(getX() + getWidth() - m_graphMargins.getRight(), getY() + m_graphMargins.getTop() + 20),
                              TVector2F(getX() + getWidth() - m_graphMargins.getRight(), getY() + getHeight() - m_graphMargins.getBottom() - 20),
                              m_gridColor); // Ligne de droite
    //...

#ifdef T_USE_PIXMAP
    CImage pixmap(getWidth() - 60, getHeight() - 80);
#endif

    // Affichage de la grille
    if (m_horizontalGridEnabled)
    {
        for (int y = getY() + getHeight() - 20 - m_graphMargins.getBottom(); y >= getY() + m_graphMargins.getTop() + 20; y -= m_horizontalGridSize)
        {
            Game::guiEngine->drawLine(TVector2F(getX() + m_graphMargins.getLeft() + 20, y),
                                      TVector2F(getX() + getWidth() - m_graphMargins.getRight(), y),
                                      m_gridColor);
        }
    }

    if (m_verticalGridEnabled)
    {
        for (int x = getX() + m_graphMargins.getLeft() + 20; x <= getX() + getWidth() - m_graphMargins.getRight(); x += m_verticalGridSize)
        {
            Game::guiEngine->drawLine(TVector2F(x, getY() + 20 + m_graphMargins.getTop()),
                                      TVector2F(x, getY() + getHeight() - 20 - m_graphMargins.getBottom()),
                                      m_gridColor);
        }
    }

    //...

    //const float iteration = (m_maxX - m_minX) / (getWidth() - 60); //60px pour les marges du graphe

    // Affichage des courbes
    for (std::list<ICurve *>::const_iterator it = m_curves.begin(); it != m_curves.end(); ++it)
    {
        (*it)->setCurveRect(m_chartMinX, m_chartMaxX, m_chartMinY, m_chartMaxY);
#ifdef T_USE_PIXMAP
        (*it)->draw(pixmap);
#else
        (*it)->draw();
#endif
/*
        CColor curveColor = (*it)->getColor();

        for (float x = m_minX; x < m_maxX; x += iteration)
        {
            float y = (*it)->getY(x);
            drawPoint(x, y, curveColor);
        }
*/
    }
}


/**
 * Calcule les valeurs à afficher dans le graphe.
 ******************************/

void CGraph::computeSize()
{
    TTime currentTime = getElapsedTime();

    if (currentTime - m_lastUpdateTime < m_updateDelay)
        return;

    if (m_autoX)
    {
        m_chartMinX = std::numeric_limits<float>::max();
        m_chartMaxX = -std::numeric_limits<float>::max();

        for (std::list<ICurve *>::const_iterator it = m_curves.begin(); it != m_curves.end(); ++it)
        {
            float minX = (*it)->getCurveMinX();

            if (minX < m_chartMinX)
                m_chartMinX = minX;

            float maxX = (*it)->getCurveMaxX();

            if (maxX > m_chartMaxX)
                m_chartMaxX = maxX;
        }
    }

    // Le calcul des bornes en Y doit se faire après celui en X
    if (m_autoY)
    {
        m_chartMinY = std::numeric_limits<float>::max();
        m_chartMaxY = -std::numeric_limits<float>::max();

        for (std::list<ICurve *>::const_iterator it = m_curves.begin(); it != m_curves.end(); ++it)
        {
            float minY = (*it)->getCurveMinY(m_chartMinX, m_chartMaxX);

            if (minY < m_chartMinY)
                m_chartMinY = minY;

            float maxY = (*it)->getCurveMaxY(m_chartMinX, m_chartMaxX);

            if (maxY > m_chartMaxY)
                m_chartMaxY = maxY;
        }
    }

    m_lastUpdateTime = currentTime;
}

} // Namespace Ted

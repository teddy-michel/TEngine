/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGroupBox.cpp
 * \date 19/11/2010 Création de la classe GuiGroupBox.
 * \date 19/01/2011 Affichage correct, gestion des évènements.
 * \date 29/05/2011 La classe est renommée en CGroupBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CGroupBox.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/Events.hpp"


namespace Ted
{

const unsigned int GroupBoxMinSize = 100; ///< Dimension minimale en pixels.


/**
 * Constructeur par défaut.
 *
 * \param title  Titre du groupe.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CGroupBox::CGroupBox(const CString& title, IWidget * parent) :
IWidget     (parent),
m_title     (title),
m_align     (AlignLeft),
m_checkable (false),
m_checked   (true),
m_child     (nullptr)
{
    setMinSize(GroupBoxMinSize, GroupBoxMinSize);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CGroupBox::CGroupBox(IWidget * parent) :
IWidget     (parent),
m_title     (CString()),
m_align     (AlignLeft),
m_checkable (false),
m_checked   (true),
m_child     (nullptr)
{
    setMinSize(GroupBoxMinSize, GroupBoxMinSize);
}


/**
 * Destructeur. Supprime l'objet à l'intérieur du widget.
 ******************************/

CGroupBox::~CGroupBox()
{
    delete m_child;
}


/**
 * Donne le titre du groupe.
 *
 * \return Titre du groupe.
 *
 * \sa CGroupBox::setTitle
 ******************************/

CString CGroupBox::getTitle() const
{
    return m_title;
}


/**
 * Donne l'alignement du titre.
 *
 * \return Alignement du titre.
 *
 * \sa CGroupBox::setAlign
 ******************************/

THorizontalAlignment CGroupBox::getAlignment() const
{
    return m_align;
}


/**
 * Accesseur pour checkable.
 *
 * \return Booléen indiquant si le groupe peut être désactivé.
 *
 * \sa CGroupBox::setCheckable
 ******************************/

bool CGroupBox::isCheckable() const
{
    return m_checkable;
}


/**
 * Modifie le widget contenu dans le groupe.
 *
 * \return Pointeur sur le widget contenu dans le groupe.
 *
 * \sa CGroupBox::setChild
 ******************************/

IWidget * CGroupBox::getChild() const
{
    return m_child;
}


/**
 * Modifie la largeur du groupe
 *
 * \param width Largeur du groupe en pixels.
 ******************************/

void CGroupBox::setWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width == m_width)
        return;

    IWidget::setWidth(width);

    if (m_child)
    {
        m_child->setWidth(m_width);
    }
}


/**
 * Modifie la hauteur du groupe
 *
 * \param height Hauteur du groupe en pixels.
 ******************************/

void CGroupBox::setHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height == m_height)
        return;

    IWidget::setHeight(height);

    if (m_child)
    {
        m_child->setHeight(m_height - 15);
    }
}


/**
 * Modifie le titre du groupe.
 *
 * \param title titre du groupe.
 *
 * \sa CGroupBox::getTitle
 ******************************/

void CGroupBox::setTitle(const CString& title)
{
    m_title = title;
}


/**
 * Modifie l'alignement du titre.
 *
 * \param align Alignement du titre.
 *
 * \sa CGroupBox::getAlignement
 ******************************/

void CGroupBox::setAlignment(THorizontalAlignment align)
{
    m_align = align;
}


/**
 * Place une case à cocher pour activer ou désactiver le groupe.
 *
 * \param checkable Indique si on peut désactiver le groupe avec une case à cocher.
 *
 * \sa CGroupBox::getCheckable
 ******************************/

void CGroupBox::setCheckable(bool checkable)
{
    m_checkable = checkable;
}


/**
 * Modifie l'objet à l'intérieur du widget.
 *
 * \param child Pointeur sur l'objet à placer dans le widget.
 *
 * \sa CGroupBox::getChild
 ******************************/

void CGroupBox::setChild(IWidget * child)
{
    m_child = child;

    if (m_child)
    {
        m_child->setParent(this);
        m_child->setY(16);
    }
}


/**
 * Dessine le widget.
 *
 * \todo Dessiner la case à cocher.
 * \todo Garder en mémoire la variable params et la variable txtdim ?
 ******************************/

void CGroupBox::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    TTextParams params;
    params.text  = m_title;
    params.font  = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
    params.size  = 13;
#else
    params.size  = 15;
#endif // T_USE_FREETYPE

    params.color = CColor::Black;
    params.rec.setX(tmp_x + 10);
    params.rec.setY(tmp_y);
    params.rec.setWidth(getWidth() - 10);
    params.rec.setHeight(getHeight());

    const unsigned int title_len = Game::fontManager->getTextSize(params).X;

    // Affichage de la bordure
    Game::guiEngine->drawLine(TVector2F(tmp_x + 1, tmp_y + 8), TVector2F(tmp_x + 1, tmp_y + getHeight()), CColor(28, 38, 60));
    Game::guiEngine->drawLine(TVector2F(tmp_x + getWidth(), tmp_y + 8), TVector2F(tmp_x + getWidth(), tmp_y + getHeight()), CColor(28, 38, 60));
    Game::guiEngine->drawLine(TVector2F(tmp_x, tmp_y + getHeight()), TVector2F(tmp_x + getWidth(), tmp_y + getHeight()), CColor(28, 38, 60));
    Game::guiEngine->drawLine(TVector2F(tmp_x, tmp_y + 8), TVector2F(tmp_x + 8, tmp_y + 8), CColor(28, 38, 60));
    Game::guiEngine->drawLine(TVector2F(tmp_x + 12 + title_len, tmp_y + 8), TVector2F(tmp_x + getWidth(), tmp_y + 8), CColor(28, 38, 60));

    // Affichage du titre
    switch (m_align)
    {
        default:
        case AlignLeft:
            break;

        case AlignCenter:
        {
            TVector2UI txtdim = Game::fontManager->getTextSize(params);
            params.rec.setX(params.rec.getX() + (getWidth() - static_cast<int>(txtdim.X)) / 2);
            break;
        }

        case AlignRight:
        {
            TVector2UI txtdim = Game::fontManager->getTextSize(params);
            params.rec.setX(params.rec.getX() + m_width - static_cast<int>(txtdim.X));
            break;
        }
    }

    Game::guiEngine->drawText(params);

    if (m_child)
    {
        m_child->draw();
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CGroupBox::onEvent(const CMouseEvent& event)
{
    const int tmp_y = getY();

    if (m_child &&
        static_cast<int>(event.y) >= tmp_y + 16 &&
        static_cast<int>(event.y) <= tmp_y + getHeight())
    {
        m_child->onEvent(event);
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CButtonGroup.cpp
 * \date 12/03/2010 Création de la classe GuiButtonGroup.
 * \date 29/05/2011 La classe est renommée en CButtonGroup.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Gui/CButtonGroup.hpp"
#include "Gui/IButton.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CButtonGroup::CButtonGroup(IWidget * parent) :
IWidget     (parent),
m_exclusive (true)
{ }


/**
 * Indique si un seul bouton peut être enfoncé en même temps.
 *
 * \return Booléen.
 ******************************/

bool CButtonGroup::isExclusive() const
{
    return m_exclusive;
}


/**
 * Donne le bouton se trouvant à une certaine position.
 *
 * \param pos Position du bouton.
 * \return Pointeur sur le bouton.
 ******************************/

IButton * CButtonGroup::getButton(int pos) const
{
    if (pos < 0 || pos >= getNumButtons())
    {
        return nullptr;
    }

    std::list<IButton *>::const_iterator it = m_buttons.begin();
    std::advance(it, pos);

    return (it == m_buttons.end() ? nullptr : *it);
}


/**
 * Donne le nombre de boutons du groupe.
 *
 * \return Nombre de boutons.
 ******************************/

int CButtonGroup::getNumButtons() const
{
    return m_buttons.size();
}


/**
 * Rend le groupe exclusif.
 *
 * \param exclusive Indique qu'un seul bouton est enfoncé en même temps.
 ******************************/

void CButtonGroup::setExclusive(bool exclusive)
{
    if (exclusive != m_exclusive)
    {
        m_exclusive = exclusive;

        if (m_exclusive)
        {
            bool ex = false;

            // On change l'état de chaque bouton
            for (std::list<IButton *>::const_iterator it = m_buttons.begin(); it != m_buttons.end(); ++it)
            {
                if ((*it)->isDown())
                {
                    if (ex)
                    {
                        (*it)->setDown(false);
                    }
                    else
                    {
                        ex = true;
                    }
                }
            }

            // Aucun bouton n'est enfoncé, on change l'état du premier bouton
            if (!ex)
            {
                std::list<IButton *>::const_iterator it = m_buttons.begin();
                (*it)->setDown();
            }
        }
    }
}


/**
 * Ajoute un bouton au groupe.
 *
 * \param button Pointeur sur le bouton à ajouter.
 * \param pos Position du nouveau bouton (par défaut en dernière position).
 ******************************/

void CButtonGroup::addButton(IButton * button, int pos)
{
    if (button == nullptr)
        return;

    int nbr = getNumButtons();

    // On vérifie que le bouton n'est pas déjà dans la liste
    std::list<IButton *>::iterator it = std::find(m_buttons.begin(), m_buttons.end(), button);

    if (it == m_buttons.end())
    {
        if (pos < 0 || pos >= nbr)
        {
            m_buttons.push_back(button);
        }
        else
        {
            it = m_buttons.begin();
            std::advance(it, pos);
            m_buttons.insert(it, button);
        }

        button->setGroup(this);

        // Si c'est le seul bouton et que le groupe est exclusif, il doit être enfoncé
        if (nbr == 0 && m_exclusive)
        {
            button->setDown();
        }
    }
}


/**
 * Enlève un bouton du groupe.
 *
 * \param button Pointeur sur le bouton à enlever.
 ******************************/

void CButtonGroup::removeButton(IButton * button)
{
    if (button == nullptr)
        return;

    std::list<IButton *>::iterator it = std::find(m_buttons.begin(), m_buttons.end(), button);

    if (it != m_buttons.end())
    {
        (*it)->setGroup(nullptr);
        m_buttons.erase(it);
    }
}


/**
 * Enlève un bouton du groupe.
 *
 * \param pos Position du bouton à enlever.
 ******************************/

void CButtonGroup::removeButton(int pos)
{
    if (pos < 0 || pos >= getNumButtons())
        return;

    std::list<IButton *>::iterator it = m_buttons.begin();
    std::advance(it, pos);

    if (it != m_buttons.end())
    {
        (*it)->setGroup(nullptr);
        m_buttons.erase(it);
    }
}


/**
 * Enlève tous les boutons du groupe (la mémoire n'est pas libérée).
 ******************************/

void CButtonGroup::clearButtons()
{
    for (std::list<IButton *>::iterator it = m_buttons.begin(); it != m_buttons.end(); ++it)
    {
        (*it)->setGroup(nullptr);
    }

    m_buttons.clear();
}


/**
 * Change l'état d'un bouton.
 *
 * \param button Pointeur sur le bouton.
 ******************************/

void CButtonGroup::buttonState(IButton * button)
{
    if (m_exclusive && button)
    {
        // Le bouton n'appartient pas au groupe
        if (std::find(m_buttons.begin(), m_buttons.end(), button) == m_buttons.end())
        {
            return;
        }

        if (button->isDown())
        {
            // On parcourt la liste des boutons
            for (std::list<IButton *>::iterator it = m_buttons.begin(); it != m_buttons.end(); ++it)
            {
                if ((*it)->isDown() && *it != button)
                {
                    (*it)->setDown(false);
                }
            }
        }
        else
        {
            bool one = false;

            // On parcourt la liste des boutons
            for (std::list<IButton *>::iterator it = m_buttons.begin(); it != m_buttons.end(); ++it)
            {
                if ((*it)->isDown())
                {
                    one = true;
                }
            }

            if (!one)
            {
                button->setDown(true);
            }
        }
    }
}


/**
 * Cette méthode est définie mais ne fait rien et ne doit pas être appellée.
 ******************************/

void CButtonGroup::draw()
{

}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindowDebug.cpp
 * \date 01/04/2013 Création de la classe CWindowDebug.
 * \date 13/06/2014 Ajout du widget pour afficher les textures.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindowDebug.hpp"
#include "Gui/CLayoutHorizontal.hpp"
#include "Gui/CMenu.hpp"
//#include "Gui/CWidgetDebugMemory.hpp"
#include "Gui/CWidgetDebugTextures.hpp"
#include "Gui/CWidgetDebugFPS.hpp"
#include "Core/CApplication.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindowDebug::CWindowDebug(IWidget * parent) :
CWindow          ("Debug", WindowCenter | WindowNoBorder | WindowNotMovable | WindowNotResizable, parent),
//m_widgetMemory   (nullptr),
m_widgetTextures (nullptr),
m_widgetFPS      (nullptr),
m_layoutH        (nullptr)
{
    setMinSize(400, 300);
    setBackgroundColorActive(CColor(255, 255, 255, 200));
    setBackgroundColorInactive(CColor(255, 255, 255, 100));

    m_layoutH = new CLayoutHorizontal(this);
    m_layoutH->setPadding(CMargins(0));
    setLayout(m_layoutH);

    CMenu * menu = new CMenu(this);
    menu->setItemMargin(CMargins(5));
    menu->setItemPadding(CMargins(5));
    menu->setPadding(CMargins(5));
    menu->setBackgroundColor(CColor(182, 192, 210, 200));
    menu->setItemColorActive(CColor(69, 82, 104));
    menu->setItemColorInactive(CColor(49, 60, 83));
    menu->setFontColorActive(CColor::White);
    menu->setFontColorInactive(CColor(204, 204, 204));
    menu->setTextAlignment(AlignMiddleCenter);
    m_layoutH->addChild(menu, AlignMiddleCenter, -1, 0);

    //menu->addItem(CString::fromUTF8("Mémoire"), sigc::mem_fun(this, &CWindowDebug::displayMemory));
    menu->addItem("Textures", sigc::mem_fun(this, &CWindowDebug::displayTextures));
    menu->addItem("FPS", sigc::mem_fun(this, &CWindowDebug::displayFPS));

    //m_widgetMemory = new CWidgetDebugMemory(this);
    m_widgetTextures = new CWidgetDebugTextures(this);
    m_widgetFPS = new CWidgetDebugFPS(this);

    //CScrollArea * areaTextures = new CScrollArea(this);
    //areaTextures->setWidget(m_widgetTextures);

    displayFPS();
    //displayMemory();

    // On doit redimensionner après avoir ajouter tous les widgets
    setSize(CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight());
}


/**
 * Détruit la fenêtre.
 ******************************/

CWindowDebug::~CWindowDebug()
{
    //delete m_widgetMemory;
    //delete m_widgetTextures;
    //delete m_widgetFPS;
}

/*
void CWindowDebug::displayMemory()
{
    m_layoutH->removeChild(m_widgetTextures);
    m_layoutH->removeChild(m_widgetFPS);
    m_layoutH->addChild(m_widgetMemory, AlignMiddleCenter, -1, 1);
}
*/

/**
 * Affiche des informations sur les textures.
 ******************************/

void CWindowDebug::displayTextures()
{
    //m_layoutH->removeChild(m_widgetMemory);
    m_layoutH->removeChild(m_widgetFPS);
    m_layoutH->addChild(m_widgetTextures, AlignMiddleCenter, -1, 1);
}


/**
 * Affiche des informations sur les FPS.
 ******************************/

void CWindowDebug::displayFPS()
{
    m_layoutH->removeChild(m_widgetTextures);
    //m_layoutH->removeChild(m_widgetMemory);
    m_layoutH->addChild(m_widgetFPS, AlignMiddleCenter, -1, 1);
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void CWindowDebug::onEvent(const CResizeEvent& event)
{
    setSize(event.width, event.height);
    CWindow::onEvent(event);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CCurveCircularBuffer.cpp
 * \date 19/04/2013 Création de la classe CCurveCircularBuffer.
 * \date 13/06/2014 Mauvaise interpollation avec la dernière valeur.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Gui/CCurveCircularBuffer.hpp"


namespace Ted
{

CCurveCircularBuffer::CCurveCircularBuffer(CCircularBuffer<float> * buffer, IWidget * parent) :
IDiscreteCurve (parent),
m_data         (buffer)
{

}


CCurveCircularBuffer::~CCurveCircularBuffer()
{

}


CCircularBuffer<float> * CCurveCircularBuffer::getBuffer() const
{
    return m_data;
}


void CCurveCircularBuffer::setBuffer(CCircularBuffer<float> * buffer)
{
    m_data = buffer;
}


float CCurveCircularBuffer::getCurveYFromX(int x) const
{
    if (x < 0)
    {
        return m_data->at(0);
    }
    else if (static_cast<unsigned int>(x) >= m_data->size())
    {
        return m_data->at(m_data->size() - 1);
    }
    else
    {
        return m_data->at(x);
    }
}


float CCurveCircularBuffer::getCurveMinY(int minX, int maxX) const
{
    T_UNUSED(minX);
    T_UNUSED(maxX);
/*
    CCircularBuffer<float>::const_iterator it = std::min_element(m_data->cbegin(), m_data->cend());
    return (it == m_data->cend() ? 0.0f : *it);
*/
    float minY = std::numeric_limits<float>::max();

    for (CCircularBuffer<float>::const_iterator it = m_data->cbegin(); it != m_data->cend(); ++it)
    {
        if (*it < minY)
            minY = *it;
    }

    return minY;

}


float CCurveCircularBuffer::getCurveMaxY(int minX, int maxX) const
{
    T_UNUSED(minX);
    T_UNUSED(maxX);
/*
    CCircularBuffer<float>::const_iterator it = std::max_element(m_data->cbegin(), m_data->cend());
    return (it == m_data->cend() ? 0.0f : *it);
*/
    float maxY = -std::numeric_limits<float>::max();

    for (CCircularBuffer<float>::const_iterator it = m_data->cbegin(); it != m_data->cend(); ++it)
    {
        if (*it > maxY)
            maxY = *it;
    }

    return maxY;

}


float CCurveCircularBuffer::getCurveMinX() const
{
    return 0;
}


float CCurveCircularBuffer::getCurveMaxX() const
{
    return (m_data->size() - 1);
}

} // Namespace Ted

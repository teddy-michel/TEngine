/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CActivityVideo.cpp
 * \date 02/06/2014 Création de la classe CActivityVideo.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CActivityVideo.hpp"
#include "Gui/CLoaderAVI.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Time.hpp"


namespace Ted
{

/**
 * Constructeur de l'activité.
 *
 * \param activityName Nom de l'activité à créer.
 ******************************/

CActivityVideo::CActivityVideo(const CString& activityName) :
CActivity          (activityName),
m_video            (nullptr),
m_videoFileName    (),
m_playMode         (PlayEachTime),
m_interruptActions (AllKeys),
m_nextActivity     (),
m_timeLast         (0)
{

}


/**
 * Détruit l'activité.
 ******************************/

CActivityVideo::~CActivityVideo()
{
    delete m_video;
}


void CActivityVideo::setVideoFileName(const CString& fileName)
{
    if (m_videoFileName != fileName)
    {
        m_videoFileName = fileName;

        if (m_video != nullptr)
        {
            m_timeLast = getElapsedTime();
            m_video->loadFromFile(m_videoFileName);
        }
    }
}


CString CActivityVideo::getVideoFileName() const
{
    return m_videoFileName;
}


void CActivityVideo::setPlayMode(TPlayMode playMode)
{
    m_playMode = playMode;
}


CActivityVideo::TPlayMode CActivityVideo::getPlayMode() const
{
    return m_playMode;
}


void CActivityVideo::setInterruptActions(TInterruptActions actions)
{
    m_interruptActions = actions;
}


CActivityVideo::TInterruptActions CActivityVideo::getInterruptActions() const
{
    return m_interruptActions;
}


void CActivityVideo::setNextActivityName(const CString& activityName)
{
    m_nextActivity = activityName;
}


CString CActivityVideo::getNextActivityName() const
{
    return m_nextActivity;
}


void CActivityVideo::onEvent(const CMouseEvent& event)
{
    if (event.type != TEventType::ButtonPressed)
    {
        return;
    }

    bool canInterrupt = (m_playMode == PlayEachTime);

    if (m_playMode == PlayAtLeastOnce)
    {
        int alreadyPlayed = CApplication::getApp()->getSettingValue("ActivityVideoPlayed", m_activityName, "0").toInt32();

        if (alreadyPlayed)
        {
            canInterrupt = true;
        }
    }

    if (!canInterrupt)
    {
        return;
    }

    if (m_interruptActions.testFlag(AllKeys) || m_interruptActions.testFlag(MouseButtons))
    {
        nextActivity();
    }
}


void CActivityVideo::onEvent(const CKeyboardEvent& event)
{
    if (event.type != TEventType::KeyPressed)
    {
        return;
    }

    bool canInterrupt = (m_playMode == PlayEachTime);

    if (m_playMode == PlayAtLeastOnce)
    {
        int alreadyPlayed = CApplication::getApp()->getSettingValue("ActivityVideoPlayed", m_activityName, "0").toInt32();

        if (alreadyPlayed)
        {
            canInterrupt = true;
        }
    }

    if (!canInterrupt)
    {
        return;
    }

    if (m_interruptActions.testFlag(AllKeys))
    {
        nextActivity();
    }
    else if (m_interruptActions.testFlag(KeySpace) && event.code == Key_Space)
    {
        nextActivity();
    }
    else if (m_interruptActions.testFlag(KeyEnter) && event.code == Key_Enter)
    {
        nextActivity();
    }
    else if (m_interruptActions.testFlag(KeyEscape) && event.code == Key_Escape)
    {
        nextActivity();
    }
}


void CActivityVideo::start()
{
    if (m_video == nullptr)
    {
        m_video = new CLoaderAVI();
    }
    else
    {
        m_video->stop();
    }

    // On ne joue la vidéo que la première fois
    if (m_playMode == PlayOnlyFirstTime)
    {
        int alreadyPlayed = CApplication::getApp()->getSettingValue("ActivityVideoPlayed", m_activityName, "0").toInt32();

        if (alreadyPlayed)
        {
            nextActivity();
            return;
        }
    }

    if (m_video->loadFromFile(m_videoFileName))
    {
        CApplication::getApp()->showMouseCursor(false);
        m_timeLast = getElapsedTime();
        m_video->play();
        Game::guiEngine->drawImage(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_video->getTextureId());
    }
}


void CActivityVideo::update()
{
    if (m_video != nullptr)
    {
        const unsigned int time = getElapsedTime();
        unsigned int frameTime = time - m_timeLast;
        m_timeLast = time;

        m_video->update(frameTime);
        Game::guiEngine->drawImage(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_video->getTextureId());

        // Fin de la vidéo
        if (!m_video->isPlaying())
        {
            CApplication::getApp()->setSettingValue("ActivityVideoPlayed", m_activityName, "1");
            nextActivity();
        }
    }
}


void CActivityVideo::stop()
{
    if (m_video != nullptr)
    {
        m_video->stop();
    }
}


/**
 * Passe à l'activité suivante.
 ******************************/

void CActivityVideo::nextActivity()
{
    if (m_nextActivity.isEmpty())
    {
        CApplication::getApp()->log(CString::fromUTF8("L'activité suivante n'a pas été définie"), ILogger::Warning);
        return;
    }

    CActivity * activity = Game::guiEngine->getActivity(m_nextActivity);

    if (activity == nullptr)
    {
        CApplication::getApp()->log(CString::fromUTF8("L'activité suivante (%1) n'existe pas").arg(m_nextActivity), ILogger::Warning);
        return;
    }

    Game::guiEngine->setCurrentActivity(activity);
}

} // Namespace Ted

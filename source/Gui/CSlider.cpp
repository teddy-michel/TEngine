/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CSlider.cpp
 * \date 12/12/2010 Création de la classe GuiSlider.
 * \date 13/01/2011 Amélioration et affichage.
 * \date 06/05/2011 Implémentation en utilisant le code des scrollbars.
 * \date 28/05/2011 Amélioration du déplacement du curseur à la souris.
 * \date 29/05/2011 La classe est renommée en CSlider.
 * \date 14/06/2014 Correction d'un bug : le slider se déplaçait sans raison.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>

#include "Gui/CSlider.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/CApplication.hpp"
#include "Core/Events.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

const int SliderNoMove = -32768;
const int SliderSize = 6; ///< Largeur du curseur.
const int SliderDistEndMove = 140; /**< Lorsqu'on glisse la barre de
                                     *  l'ascenseur et que le curseur
                                     *  s'éloigne trop, la barre revient
                                     *  à sa position initiale.
                                     */


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CSlider::CSlider(IWidget * parent) :
IWidget    (parent),
m_value    (0),
m_valueTmp (0),
m_total    (0),
m_step     (0),
m_clic     (SliderNoMove),
m_vertical (false)
{
    setMinSize(10, 20);
    setMaxHeight(20);
}


/**
 * Modifie la valeur affichée.
 *
 * \param value Nouvelle valeur.
 *
 * \sa CSlider::getValue
 ******************************/

void CSlider::setValue(int value)
{
    const int oldValue = m_value;

         if (value < 0)       m_value = 0;
    else if (value > m_total) m_value = m_total;
    else                      m_value = value;

    if (oldValue != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Modifie la valeur totale.
 *
 * \param total Valeur totale (doit être positive).
 *
 * \sa CSlider::getTotal
 ******************************/

void CSlider::setTotal(int total)
{
    m_total = (total < 0 ? 0 : total);

    if (m_value > m_total)
    {
        m_value = m_total;
        onValueChange(m_value);
    }
}


/**
 * Modifie le nombre de lignes par page.
 *
 * \param step Nombre de lignes (doit être positif).
 *
 * \sa CSlider::getStep
 ******************************/

void CSlider::setStep(int step)
{
    m_step = (step < 0 ? 0 : step);
}


/**
 * Modifie la valeur de vertical.
 *
 * \param vertical Indique si la barre doit être verticale.
 *
 * \sa CSlider::setHorizontal
 * \sa CSlider::isVertical
 ******************************/

void CSlider::setVertical(bool vertical)
{
    m_vertical = vertical;

    if (m_vertical)
        setMaxSize(20, std::numeric_limits<int>::max());
    else
        setMaxSize(std::numeric_limits<int>::max(), 20);
}


/**
 * Modifie la valeur de vertical.
 *
 * \param horizontal Indique si la barre doit être horizontale.
 *
 * \sa CSlider::setVertical
 * \sa CSlider::isHorizontal
 ******************************/

void CSlider::setHorizontal(bool horizontal)
{
    setVertical(!horizontal);
}


/**
 * Dessine le slider.
 ******************************/

void CSlider::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    if (m_vertical)
    {
        // Barre
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + (m_width - 4) / 2, tmp_y + SliderSize / 2, 4, m_height - SliderSize),
                                       CColor(28, 38, 60));

        // Déplacement de la barre à la souris
        if (m_clic != SliderNoMove && CApplication::getApp()->getButtonState(MouseButtonLeft))
        {
            m_value = m_valueTmp;

            const TVector2I cursor_position = CApplication::getApp()->getCursorPosition();
            const int cursor = cursor_position.X - tmp_x;

            if (cursor > -SliderDistEndMove && cursor < m_width + SliderDistEndMove)
            {
                const int oldValue = m_value;

                int rel = static_cast<int>(static_cast<float>(cursor_position.Y - m_clic) *
                                           static_cast<float>(m_total) /
                                           static_cast<float>(m_height - SliderSize));

                     if (m_value + rel <= 0)       m_value = 0;
                else if (m_value + rel >= m_total) m_value = m_total;
                else                               m_value += rel;

                if (oldValue != m_value)
                {
                    onValueChange(m_value);
                }
            }
        }

        // Curseur
        Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y + static_cast<float>(m_value) / m_total * (m_height - SliderSize), m_width, SliderSize), CColor::Orange );
    }
    else
    {
        // Barre
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + SliderSize / 2, tmp_y + (m_height - 4) / 2, m_width - SliderSize, 4),
                                       CColor(28, 38, 60));

        // Déplacement de la barre à la souris
        if (m_clic != SliderNoMove && CApplication::getApp()->getButtonState(MouseButtonLeft))
        {
            m_value = m_valueTmp;

            const TVector2I cursor_position = CApplication::getApp()->getCursorPosition();
            const int cursor_y = cursor_position.Y - tmp_y;

            if (cursor_y > -SliderDistEndMove && cursor_y < m_height + SliderDistEndMove)
            {
                const int oldValue = m_value;

                int rel = static_cast<int>(static_cast<float>(cursor_position.X - m_clic) *
                                           static_cast<float>(m_total) /
                                           static_cast<float>(m_width - SliderSize));

                     if (m_value + rel < 0)       m_value = 0;
                else if (m_value + rel > m_total) m_value = m_total;
                else                              m_value += rel;

                if (oldValue != m_value)
                {
                    onValueChange(m_value);
                }
            }
        }

        // Curseur
        Game::guiEngine->drawRectangle(CRectangle(tmp_x + static_cast<float>(m_value) / m_total * (m_width - SliderSize), tmp_y, SliderSize, m_height),
                                       CColor(120, 220, 0));
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CSlider::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    const int tmp_x = getX();
    const int tmp_y = getY();

    if (event.type == ButtonPressed)
    {
        if (m_vertical)
        {
            // À gauche de la barre
            if (m_value > 0 &&
                static_cast<int>(event.y) > tmp_y + SliderSize / 2 &&
                static_cast<int>(event.y) < tmp_y + (m_height - SliderSize) * static_cast<float>(m_value) / m_total)
            {
                pageUp();
                m_clic = SliderNoMove;
            }
            // À droite de la barre
            else if (m_value < m_total &&
                     static_cast<int>(event.y) > tmp_y + SliderSize + (m_height - SliderSize) * static_cast<float>(m_value) / m_total &&
                     static_cast<int>(event.y) < tmp_y + m_height - SliderSize / 2)
            {
                pageDown();
                m_clic = SliderNoMove;
            }
            // Sur la barre
            else
            {
                m_clic = event.y;
                m_valueTmp = m_value; // Sauvegarde de l'ancienne valeur
            }
        }
        else
        {
            // À gauche de la barre
            if (m_value > 0 &&
                static_cast<int>(event.x) > tmp_x + SliderSize / 2 &&
                static_cast<int>(event.x) < tmp_x + (m_width - SliderSize) * static_cast<float>(m_value) / m_total)
            {
                pageUp();
                m_clic = SliderNoMove;
            }
            // À droite de la barre
            else if (m_value < m_total &&
                     static_cast<int>(event.x) > tmp_x + SliderSize + (m_width - SliderSize) * static_cast<float>(m_value) / m_total &&
                     static_cast<int>(event.x) < tmp_x + m_width - SliderSize / 2)
            {
                pageDown();
                m_clic = SliderNoMove;
            }
            // Sur la barre
            else
            {
                m_clic = event.x;
                m_valueTmp = m_value; // Sauvegarde de l'ancienne valeur
            }
        }
    }
    else if (event.type == ButtonReleased)
    {
        m_clic = SliderNoMove;
    }
}


/**
 * Fait monter l'ascenseur d'une page.
 *
 * \sa CSlider::pageDown
 ******************************/

void CSlider::pageUp()
{
    const int old_value = m_value;

    if (m_value - m_step < 0)
        m_value = 0;
    else
        m_value -= m_step;

    if (old_value != m_value)
    {
        onValueChange(m_value);
    }
}


/**
 * Fait descendre l'ascenseur d'une page.
 *
 * \sa CSlider::pageUp
 ******************************/

void CSlider::pageDown()
{
    const int old_value = m_value;

    if (m_value + m_step > m_total)
        m_value = m_total;
    else
        m_value += m_step;

    if (old_value != m_value)
    {
        onValueChange(m_value);
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDateTimeEdit.cpp
 * \date 14/03/2010 Création de la classe GuiDateTimeEdit.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 26/01/2011 Création de la méthode UpdateValue.
 * \date 29/05/2011 La classe est renommée en CDateTimeEdit.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CDateTimeEdit.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CPushButton.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDateTimeEdit::CDateTimeEdit(IWidget * parent) :
ISpinBox   (parent),
m_datetime (CDateTime()),
m_min      (CDateTime(1900,1,1)),
m_max      (CDateTime(2100,12,31)),
m_format   ("%d/%m/%Y %H:%i:%s"),
m_select   (Day)
{
    m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);
}


/**
 * Constructeur.
 *
 * \param datetime Date à utiliser.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDateTimeEdit::CDateTimeEdit(const CDateTime& datetime, IWidget * parent) :
ISpinBox   (parent),
m_datetime (datetime),
m_min      (CDateTime(1900,1,1)),
m_max      (CDateTime(2100,12,31)),
m_format   ("%d/%m/%Y %H:%i:%s"),
m_select   (Day)
{
    m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);
}


/**
 * Donne la date.
 *
 * \return Date.
 ******************************/

CDateTime CDateTimeEdit::getDateTime() const
{
    return m_datetime;
}


/**
 * Donne la date minimale.
 *
 * \return Date minimale.
 ******************************/

CDateTime CDateTimeEdit::getMinDateTime() const
{
    return m_min;
}


/**
 * Donne la date maximale.
 *
 * \return Date maximale.
 ******************************/

CDateTime CDateTimeEdit::getMaxDateTime() const
{
    return m_max;
}


/**
 * Donne le format d'affichage de la date.
 *
 * \return Format d'affichage.
 *
 * \sa CDateTimeEdit::setFormat
 * \sa CDateTime::toString
 ******************************/

CString CDateTimeEdit::getFormat() const
{
    return m_format;
}


/**
 * Modifie la valeur de la date.
 *
 * \param datetime Date.
 ******************************/

void CDateTimeEdit::setDateTime(const CDateTime& datetime)
{
    if (datetime < m_min)
    {
        m_datetime = m_min;
    }
    else if (datetime > m_max)
    {
        m_datetime = m_max;
    }
    else
    {
        m_datetime = datetime;
    }

    m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);

    onChange(m_datetime);
}


/**
 * Modifie la valeur de la date minimale.
 *
 * \param datetime Date minimale.
 ******************************/

void CDateTimeEdit::setMinDateTime(const CDateTime& datetime)
{
    m_min = datetime;

    if (m_min > m_max)
    {
        m_max = m_min;
    }

    if (m_datetime < m_min)
    {
        m_datetime = m_min;
        m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);

        onChange(m_datetime);
    }
}


/**
 * Modifie la valeur de la date maximale.
 *
 * \param datetime Date maximale.
 ******************************/

void CDateTimeEdit::setMaxDateTime(const CDateTime& datetime)
{
    m_max = datetime;

    if (m_max < m_min)
    {
        m_min = m_max;
    }

    if (m_datetime > m_max)
    {
        m_datetime = m_max;
        m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);

        onChange(m_datetime);
    }
}


/**
 * Modifie le format d'affichage de la date.
 *
 * \param format Format d'affichage.
 *
 * \sa CDateTimeEdit::getFormat
 * \sa CDateTime::toString
 ******************************/

void CDateTimeEdit::setFormat(const CString& format)
{
    m_format = format;
    m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);
}


/**
 * Incrémente la spinbox.
 ******************************/

void CDateTimeEdit::stepUp()
{
    CDateTime old(m_datetime);

    switch (m_select)
    {
        case Year:   m_datetime.addYears  (1); break;
        case Month:  m_datetime.addMonths (1); break;
        case Week:   m_datetime.addDays   (7); break;
        case Day:    m_datetime.addDays   (1); break;
        case Hour:   m_datetime.addHours  (1); break;
        case Minute: m_datetime.addMinutes(1); break;
        case Second: m_datetime.addSeconds(1); break;
    }

    if (m_datetime > m_max)
    {
        m_datetime = (m_wrapping ? m_min : m_max);
    }

    if (old != m_datetime)
    {
        m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);
        onChange(m_datetime);
    }
}


/**
 * Décrémente la spinbox.
 ******************************/

void CDateTimeEdit::stepDown()
{
    CDateTime old(m_datetime);

    switch (m_select)
    {
        case Year:   m_datetime.addYears  (-1); break;
        case Month:  m_datetime.addMonths (-1); break;
        case Week:   m_datetime.addDays   (-7); break;
        case Day:    m_datetime.addDays   (-1); break;
        case Hour:   m_datetime.addHours  (-1); break;
        case Minute: m_datetime.addMinutes(-1); break;
        case Second: m_datetime.addSeconds(-1); break;
    }

    if (m_datetime < m_min)
    {
        m_datetime = (m_wrapping ? m_max : m_min);
    }

    if (old != m_datetime)
    {
        m_line->setText(m_prefix + m_datetime.toString(m_format) + m_suffix);

        onChange(m_datetime);
    }
}


/**
 * Méthode appelée lorsque le texte du champ est modifié.
 *
 * \todo Implémentation.
 ******************************/

void CDateTimeEdit::updateValue()
{
    T_ASSERT(m_line != nullptr);

    //CDateTime value;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CGuiEngine.cpp
 * \date       2008 Création de la classe GuiEngine.
 * \date 11/07/2010 La texture du réticule est chargée à l'initialisation.
 * \date 17/07/2010 Utilisation des assertions et des signaux.
 * \date 19/07/2010 La méthode ShowCursor est déplacée vers la classe CApplication.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 30/10/2010 Gestion des éléments au premier-plan (menus, infobulles, et combobox).
 * \date 23/01/2011 Les méthodes d'affichage prennent un CRectangle en paramètre à la place de quatre.
 * \date 23/01/2011 Amélioration de l'utilisation des buffers graphiques.
 * \date 24/01/2011 Les buffers graphiques sont opérationnels.
 * \date 29/01/2011 Ajout d'une liste de HUD à afficher.
 * \date 14/02/2011 Utilisation de la couleur ou de l'image de fond.
 * \date 27/02/2011 Les méthodes ne dessinent plus les formes simples directement mais appellent les méthodes du buffer.
 * \date 27/02/2011 Suppression d'un bloc de test.
 * \date 02/03/2011 Création des méthodes getCursor, setCursor.
 * \date 06/03/2011 Gestion du curseur.
 * \date 29/05/2011 La classe est renommée en CGuiEngine.
 * \date 10/12/2011 Gestion des curseurs de la souris.
 * \date 07/06/2014 La classe CActivity s'occupe de la gestion des objets de l'interface.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CGuiEngine.hpp"
#include "Gui/CActivity.hpp"
#include "Gui/CCursor.hpp"
#include "Gui/CDialog.hpp"
#include "Gui/CWindow_Save.hpp"
#include "Gui/CWindow_Load.hpp"
#include "Gui/CWindow_Options.hpp"
#include "Core/Events.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"
#include "Graphic/CFontManager.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/ICamera.hpp"
#include "Graphic/IHUD.hpp"
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

namespace Game
{
    CGuiEngine * guiEngine = nullptr;
}


//const CColor colorWindowBackground();


/**
 * Constructeur par défaut.
 ******************************/

CGuiEngine::CGuiEngine() :
m_backgroundColor (CColor(0, 0, 0, 128)),
m_backgroundImage (0),
m_cursor          (CursorArrow),
m_buffer          (nullptr),
m_currentActivity (nullptr)
{
    if (Game::guiEngine != nullptr)
        exit(-1);

    Game::guiEngine = this;
}


/**
 * Destructeur.
 * Supprime les fenêtres spéciales et les autres fenêtres, et les HUD.
 ******************************/

CGuiEngine::~CGuiEngine()
{
    Game::guiEngine = nullptr;

    delete m_buffer;

    // On supprime tous les HUD
    for (std::list<IHUD *>::iterator it = m_hud.begin(); it != m_hud.end(); ++it)
    {
        delete *it;
    }

    CApplication::getApp()->log("Fermeture du gestionnaire de l'interface graphique");
}


/**
 * Indique si un objet a le focus.
 *
 * \param  widget Objet à tester.
 * \return Booléen valant true si l'objet a le focus.
 *
 * \sa CGuiEngine::getFocus
 * \sa CGuiEngine::setFocus
 ******************************/

bool CGuiEngine::hasFocus(const IWidget * widget) const
{
    if (m_currentActivity == nullptr)
    {
        return false;
    }

    return (m_currentActivity->getFocus() == widget);
}


/**
 * Donne l'objet ayant le focus.
 *
 * \return Pointeur sur l'objet ayant le focus.
 *
 * \sa CGuiEngine::hasFocus
 * \sa CGuiEngine::setFocus
 ******************************/

IWidget * CGuiEngine::getFocus() const
{
    if (m_currentActivity == nullptr)
    {
        return nullptr;
    }

    return m_currentActivity->getFocus();
}


/**
 * Indique si un objet est pointé par le curseur.
 *
 * \param widget Objet à tester.
 * \return Booléen valant true si l'objet est pointé par le curseur de la souris.
 *
 * \sa CGuiEngine::getPointed
 * \sa CGuiEngine::setPointed
 ******************************/

bool CGuiEngine::isPointed(const IWidget * widget) const
{
    if (m_currentActivity == nullptr)
    {
        return false;
    }

    return (m_currentActivity->getPointed() == widget);
}


/**
 * Donne l'objet pointé par le curseur de la souris.
 *
 * \return Pointeur sur l'objet pointé par le curseur de la souris.
 *
 * \sa CGuiEngine::isPointed
 * \sa CGuiEngine::setPointed
 ******************************/

IWidget * CGuiEngine::getPointed() const
{
    if (m_currentActivity == nullptr)
    {
        return nullptr;
    }

    return m_currentActivity->getPointed();
}


/**
 * Indique si un objet est sélectionné par la souris.
 *
 * \param widget Objet à tester.
 * \return Booléen valant true si l'objet est sélectionné par la souris.
 *
 * \sa CGuiEngine::getSelected
 * \sa CGuiEngine::setSelected
 ******************************/

bool CGuiEngine::isSelected(const IWidget * widget) const
{
    if (m_currentActivity == nullptr)
    {
        return false;
    }

    return (m_currentActivity->getSelected() == widget);
}


/**
 * Donne l'objet sélectionné par la souris.
 *
 * \return Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa CGuiEngine::isSelected
 * \sa CGuiEngine::setSelected
 ******************************/

IWidget * CGuiEngine::getSelected() const
{
    if (m_currentActivity == nullptr)
    {
        return nullptr;
    }

    return m_currentActivity->getSelected();
}


/**
 * Indique si un objet est au premier plan.
 *
 * \param widget Objet à tester.
 * \return Booléen.
 *
 * \sa CGuiEngine::getForeground
 * \sa CGuiEngine::setForeground
 ******************************/

bool CGuiEngine::isForeground(const IWidget * widget) const
{
    if (m_currentActivity == nullptr)
    {
        return false;
    }

    return (m_currentActivity->getForeground() == widget);
}


/**
 * Donne l'objet au premier plan.
 *
 * \return Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa CGuiEngine::isForeground
 * \sa CGuiEngine::setForeground
 ******************************/

IWidget * CGuiEngine::getForeground() const
{
    if (m_currentActivity == nullptr)
    {
        return nullptr;
    }

    return m_currentActivity->getForeground();
}


/**
 * Donne la couleur de fond du menu.
 *
 * \return Couleur de fond.
 *
 * \sa CGuiEngine::setBackgroundColor
 ******************************/

CColor CGuiEngine::getBackgroundColor() const
{
    return m_backgroundColor;
}


/**
 * Donne l'image de fond du menu.
 *
 * \return Identifiant de la texture utilisée comme image de fond du menu.
 *
 * \sa CGuiEngine::setBackgroundImage
 ******************************/

TTextureId CGuiEngine::getBackgroundImage() const
{
    return m_backgroundImage;
}


/**
 * Donne le curseur actuellement utilisé.
 *
 * \return Curseur utilisé.
 *
 * \sa CGuiEngine::setCursor
 ******************************/

TCursor CGuiEngine::getCursor() const
{
    return m_cursor;
}


/**
 * Change l'objet ayant le focus.
 *
 * \param widget Pointeur sur l'objet qui a le focus.
 *
 * \sa CGuiEngine::isFocus
 * \sa CGuiEngine::getFocus
 ******************************/

void CGuiEngine::setFocus(IWidget * widget)
{
    if (m_currentActivity == nullptr)
    {
        return;
    }

    m_currentActivity->setFocus(widget);
}


/**
 * Change l'objet pointé par le curseur de la souris.
 *
 * \param widget Pointeur sur l'objet pointé par le curseur de la souris.
 *
 * \sa CGuiEngine::isPointed
 * \sa CGuiEngine::getPointed
 ******************************/

void CGuiEngine::setPointed(IWidget * widget)
{
    if (m_currentActivity == nullptr)
    {
        return;
    }

    m_currentActivity->setPointed(widget);
}


/**
 * Change l'objet sélectionné par la souris.
 *
 * \param widget Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa CGuiEngine::isSelected
 * \sa CGuiEngine::getSelected
 ******************************/

void CGuiEngine::setSelected(IWidget * widget)
{
    if (m_currentActivity == nullptr)
    {
        return;
    }

    m_currentActivity->setSelected(widget);
}


/**
 * Change l'objet au premier plan.
 *
 * \param widget Pointeur sur l'objet à mettre au premier plan.
 *
 * \sa CGuiEngine::isForeground
 * \sa CGuiEngine::getForeground
 ******************************/

void CGuiEngine::setForeground(IWidget * widget)
{
    if (m_currentActivity == nullptr)
    {
        return;
    }

    m_currentActivity->setForeground(widget);
}


/**
 * Définit la couleur d'arrière-plan du menu.
 *
 * \param color Couleur de fond.
 *
 * \sa CGuiEngine::getBackgroundColor
 ******************************/

void CGuiEngine::setBackgroundColor(const CColor& color)
{
    m_backgroundColor = color;
}


/**
 * Définit l'image d'arrière-plan du menu. Si vous ne souhaitez pas utiliser
 * d'image, utiliser la texture 0.
 *
 * \param texture Identifiant de la texture à utiliser.
 *
 * \sa CGuiEngine::getBackgroundImage
 ******************************/

void CGuiEngine::setBackgroundImage(const TTextureId texture)
{
    m_backgroundImage = texture;
}


/**
 * Modifie le curseur à utiliser.
 *
 * \param cursor Curseur à utiliser.
 *
 * \sa CGuiEngine::getCursor
 ******************************/

void CGuiEngine::setCursor(TCursor cursor)
{
    m_cursor = cursor;
}


/**
 * Initialise le CGuiEngine.
 *
 * \todo Création des curseurs sous Linux.
 ******************************/

void CGuiEngine::init()
{
    static bool initialized = false;

    // Le CGuiEngine a déjà été initialisé
    if (initialized)
        return;

    initialized = true;

    CApplication::getApp()->log("Initialisation du gestionnaire de l'interface graphique");

    m_buffer = new CBuffer2D();
    T_ASSERT(m_buffer != nullptr);

    // Création des curseurs
    m_cursorArrow = CCursor(
        "X                       "
        "XX                      "
        "X.X                     "
        "X..X                    "
        "X...X                   "
        "X....X                  "
        "X.....X                 "
        "X......X                "
        "X.......X               "
        "X........X              "
        "X.........X             "
        "X..........X            "
        "X......XXXXX            "
        "X...X..X                "
        "X..X X..X               "
        "X.X  X..X               "
        "XX    X..X              "
        "      X..X              "
        "       XX               "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        " , 0 , 0 );

    m_cursorHand = CCursor(
        "     XX                 "
        "    X..X                "
        "    X..X                "
        "    X..X                "
        "    X..X                "
        "    X..XXX              "
        "    X..X..XXX           "
        "    X..X..X..XX         "
        "    X..X..X..X.X        "
        "XXX X..X..X..X..X       "
        "X..XX........X..X       "
        "X...X...........X       "
        " X..............X       "
        "  X.............X       "
        "  X.............X       "
        "   X............X       "
        "   X...........X        "
        "    X..........X        "
        "    X..........X        "
        "     X........X         "
        "     X........X         "
        "     XXXXXXXXXX         "
        "                        "
        "                        " , 5 , 0 );

    m_cursorText = CCursor(
        "XXX XXX                 "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "   X                    "
        "XXX XXX                 "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        " , 3 , 7 );

    m_cursorResizeNE_SW = CCursor(
        "                  XXXXXX"
        "                 X.....X"
        "                  X....X"
        "                   X...X"
        "                  X.X..X"
        "                 X.X X.X"
        "                X.X   X "
        "               X.X      "
        "              X.X       "
        "             X.X        "
        "        X   X.X         "
        "       X.X X.X          "
        "       X..X.X           "
        "       X...X            "
        "       X....X           "
        "       X.....X          "
        "       XXXXXX           "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        " , 15 , 8 );

    m_cursorResizeN_S = CCursor(
        "    X                   "
        "   X.X                  "
        "  X...X                 "
        " X.....X                "
        "X.......X               "
        "XXXX.XXXX               "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "   X.X                  "
        "XXXX.XXXX               "
        "X.......X               "
        " X.....X                "
        "  X...X                 "
        "   X.X                  "
        "    X                   "
        "                        " , 4 , 11 );

    m_cursorResizeNW_SE = CCursor(
        "XXXXXX                  "
        "X.....X                 "
        "X....X                  "
        "X...X                   "
        "X..X.X                  "
        "X.X X.X                 "
        " X   X.X                "
        "      X.X               "
        "       X.X              "
        "        X.X             "
        "         X.X   X        "
        "          X.X X.X       "
        "           X.X..X       "
        "            X...X       "
        "           X....X       "
        "          X.....X       "
        "           XXXXXX       "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        " , 8 , 8 );

    m_cursorResizeW_E = CCursor(
        "    XX           XX     "
        "   X.X           X.X    "
        "  X..X           X..X   "
        " X...XXXXXXXXXXXXX...X  "
        "X.....................X "
        " X...XXXXXXXXXXXXX...X  "
        "  X..X           X..X   "
        "   X.X           X.X    "
        "    XX           XX     "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        "
        "                        " , 11 , 4 );

    // Réticule
    /*unsigned int reticule = */Game::textureManager->loadTexture("divers/reticule_00.png");
}


/**
 * Dessine un rectangle coloré aligné sur les axes.
 *
 * \param rec   Dimensions du rectangle à afficher.
 * \param color Couleur du rectangle.
 ******************************/

void CGuiEngine::drawRectangle(const CRectangle& rec, const CColor& color) const
{
    m_buffer->addRectangle(rec, color);
}


/**
 * Dessine un rectangle aux coins arrondis (4px).
 *
 * \todo Pouvoir dessiner des rectangles arrondis de petites dimensions.
 *
 * \param rec   Dimensions du rectangle.
 * \param color Couleur du rectangle.
 ******************************/

void CGuiEngine::drawRectangleRounded(const CRectangle& rec, const CColor& color) const
{
    m_buffer->addRectangleRounded(rec, color);
}


/**
 * Dessine la bordure d'un rectangle.
 *
 * \todo Supprimer le paramètre thickness.
 * \todo Ajouter un paramètre corner pour donner le rayon des angles (fusion avec DrawBorderRounded).
 *
 * \param rec       Dimensions du rectangle.
 * \param color     Couleur de la bordure.
 * \param thickness Épaisseur de la bordure.
 ******************************/

void CGuiEngine::drawBorder(const CRectangle& rec, const CColor& color, unsigned int thickness) const
{
    m_buffer->addBorder(rec, color, thickness);
}


/**
 * Dessine la bordure d'un rectangle arrondi.
 *
 * \todo Dessin d'une bordure épaisse.
 * \todo Pouvoir préciser le rayon des angles.
 *
 * \param rec       Dimensions du rectangle.
 * \param color     Couleur de la bordure.
 * \param thickness Épaisseur de la bordure.
 ******************************/

void CGuiEngine::drawBorderRounded(const CRectangle& rec, const CColor& color, unsigned int thickness) const
{
    m_buffer->addBorderRounded(rec, color, thickness);
}


/**
 * Dessine un disque.
 *
 * \todo Améliorer cette méthode pour que chaque pixel ne soit dessiné qu'une fois.
 * \todo Dessiner des ellipses.
 *
 * \param x      Position horizontale du centre du disque.
 * \param y      Position verticale du centre du disque.
 * \param radius Rayon du disque.
 * \param color  Couleur du disque.
 ******************************/

void CGuiEngine::drawDisc(int x, int y, unsigned int radius, const CColor& color) const
{
    m_buffer->addDisc(x, y, radius, color);
}


/**
 * Dessine un cercle.
 *
 * \todo Améliorer cette méthode pour que chaque pixel ne soit dessiné qu'une fois.
 * \todo Dessiner des ellipses.
 *
 * \param x      Position horizontale du centre du cercle.
 * \param y      Position verticale du centre du cercle.
 * \param radius Rayon du cercle.
 * \param color  Couleur du cercle.
 ******************************/

void CGuiEngine::drawCircle(int x, int y, unsigned int radius, const CColor& color) const
{
    m_buffer->addCircle(x, y, radius, color);
}


/**
 * Affiche du texte dans une certaine zone.
 *
 * \param params Paramètres du texte à afficher.
 ******************************/

void CGuiEngine::drawText(const TTextParams& params) const
{
    m_buffer->addText(params);
}


/**
 * Affiche une image à l'écran.
 *
 * \param rec     Dimensions du rectangle.
 * \param texture Identifiant de la texture à utiliser.
 ******************************/

void CGuiEngine::drawImage(const CRectangle& rec, TTextureId texture) const
{
    m_buffer->addImage(rec, texture);
}


/**
 * Dessine une ligne.
 *
 * \param from  Position départ.
 * \param to    Position d'arrivée.
 * \param color Couleur de la ligne.
 ******************************/

void CGuiEngine::drawLine(const TVector2F& from, const TVector2F& to, const CColor& color) const
{
    m_buffer->addLine(from, to, color);
}


/**
 * Dessine un point.
 *
 * \param pos   Position du point.
 * \param color Couleur du point.
 ******************************/

void CGuiEngine::drawPoint(const TVector2F& pos, const CColor& color) const
{
    m_buffer->addPoint(pos, color);
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CGuiEngine::onEvent(const CMouseEvent& event)
{
    if (!CApplication::getApp()->isGameActive() && m_currentActivity != nullptr)
    {
        m_currentActivity->onEvent(event);
    }
/*
    // Déplacement du curseur
    if (event.type == MoveMouse)
    {
        m_pointed = nullptr;

        // État du jeu
        switch (CApplication::getApp()->getState())
        {
            default:
            case AppMenu:
            {
                // On commence par tester l'objet au premier-plan
                if (m_foreground)
                {
                    int tmp_x = m_foreground->getX();
                    int tmp_y = m_foreground->getY();

                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_foreground->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_foreground->getHeight()))
                    {
                        m_pointed = m_foreground;

                        // On transmet l'évènement à l'objet
                        m_foreground->onEvent(event);
                        return;
                    }
                }

                // On parcourt la liste des fenêtres pour trouver celle sous le curseur
                for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
                {
                    int tmp_x = (*it)->getRealX();
                    int tmp_y = (*it)->getRealY();

                    // Si la fenêtre est ouverte et que le curseur est dans la zone de la fenêtre
                    if ((*it)->isOpen() &&
                        static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + (*it)->getRealWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + (*it)->getRealHeight()))
                    {
                        m_pointed = (*it);

                        // On transmet l'évènement à la fenêtre
                        (*it)->onEvent(event);
                        return;
                    }
                }

                // Une partie est lancée
                if (m_menuInGame && CApplication::getApp()->inGame())
                {
                    int tmp_x = m_menuInGame->getX();
                    int tmp_y = m_menuInGame->getY();

                    // On cherche dans le menu
                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_menuInGame->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_menuInGame->getHeight()))
                    {
                        m_pointed = m_menuInGame;
                        m_menuInGame->onEvent(event);
                    }
                }
                // Aucune partie n'est lancée
                else if (m_menu)
                {
                    int tmp_x = m_menu->getX();
                    int tmp_y = m_menu->getY();

                    // On cherche dans le menu
                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_menu->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_menu->getHeight()))
                    {
                        m_pointed = m_menu;
                        m_menu->onEvent(event);
                    }
                }

                break;
            }

            case AppGame:
                break;

            case AppLoad:
            {
                int tmp_x = m_windowLoadProgress->getRealX();
                int tmp_y = m_windowLoadProgress->getRealY();

                if (m_windowLoadProgress && m_windowLoadProgress->isOpen() &&
                    static_cast<int>(event.x) >= tmp_x &&
                    static_cast<int>(event.x) <= (tmp_x + m_windowLoadProgress->getRealWidth()) &&
                    static_cast<int>(event.y) >= tmp_y &&
                    static_cast<int>(event.y) <= (tmp_y + m_windowLoadProgress->getRealHeight()))
                {
                    m_pointed = m_windowLoadProgress;

                    // On transmet l'évènement à la fenêtre
                    m_windowLoadProgress->onEvent(event);
                }

                break;
            }


            //case AppConsole:
                //if (m_windowGameConsole)
                    //m_windowGameConsole->onEvent(event);
                //break;


#ifdef T_ACTIVE_DEBUG_MODE
            case AppDebug:
                if (m_windowDebug)
                {
                    m_pointed = m_windowDebug;
                    m_windowDebug->onEvent(event);
                }

                break;
#endif

            case AppExit:
            {
                int tmp_x = m_windowExit->getRealX();
                int tmp_y = m_windowExit->getRealY();

                if (m_windowExit && m_windowExit->isOpen() &&
                    static_cast<int>(event.x) >= tmp_x &&
                    static_cast<int>(event.x) <= (tmp_x + m_windowExit->getRealWidth()) &&
                    static_cast<int>(event.y) >= tmp_y &&
                    static_cast<int>(event.y) <= (tmp_y + m_windowExit->getRealHeight()))
                {
                    m_pointed = m_windowExit;

                    // On transmet l'évènement à la fenêtre
                    m_windowExit->onEvent(event);
                }

                break;
            }
        }
    }
    // Bouton de la souris
    else
    {
        // État du jeu
        switch (CApplication::getApp()->getState())
        {
            default:
            case AppMenu:
            {
                // On commence par tester l'objet au premier-plan
                if (m_foreground)
                {
                    int tmp_x = m_foreground->getX();
                    int tmp_y = m_foreground->getY();

                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_foreground->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_foreground->getHeight()))
                    {
                        // On transmet l'évènement à l'objet
                        m_foreground->onEvent(event);
                        return;
                    }
                    // L'objet au premier plan perd le focus
                    else
                    {
                        m_foreground = nullptr;
                    }
                }

                // On parcourt la liste des fenêtres
                for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
                {
                    int tmp_x = (*it)->getRealX();
                    int tmp_y = (*it)->getRealY();

                    // Si la fenêtre est ouverte et que le curseur est dans la zone de la fenêtre
                    if ((*it)->isOpen() &&
                        static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + (*it)->getRealWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + (*it)->getRealHeight()))
                    {
                        // La fenêtre n'est pas au sommet de la liste
                        if (m_windows.front() != *it)
                        {
                            // On lui donne le focus
                            m_focus = *it;

                            if (event.button == MouseButtonLeft && event.type == ButtonPressed)
                            {
                                m_selected = *it;
                            }

                            // On place la fenêtre en haut de la pile
                            onWindowOpen(*it);
                        }

                        // On envoie l'évènement à la fenêtre
                        (*it)->onEvent(event);

                        if (event.button == MouseButtonLeft && event.type == ButtonReleased)
                        {
                            m_selected = nullptr;
                        }

                        return;
                    }
                }

                // Aucune fenêtre n'a le focus, on cherche dans le menu

                // Le jeu est lancé
                if (m_menuInGame && CApplication::getApp()->inGame())
                {
                    int tmp_x = m_menuInGame->getX();
                    int tmp_y = m_menuInGame->getY();

                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_menuInGame->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_menuInGame->getHeight()))
                    {
                        m_menuInGame->onEvent(event);
                    }
                }
                else if (m_menu)
                {
                    int tmp_x = m_menu->getX();
                    int tmp_y = m_menu->getY();

                    if (static_cast<int>(event.x) >= tmp_x &&
                        static_cast<int>(event.x) <= (tmp_x + m_menu->getWidth()) &&
                        static_cast<int>(event.y) >= tmp_y &&
                        static_cast<int>(event.y) <= (tmp_y + m_menu->getHeight()))
                    {
                        m_menu->onEvent(event);
                    }
                }

                break;
            }

            case AppGame:
                m_focus = nullptr;
                m_pointed = nullptr;
                m_selected = nullptr;
                break;

            case AppLoad:
            {
                int tmp_x = m_windowLoadProgress->getRealX();
                int tmp_y = m_windowLoadProgress->getRealY();

                // Le curseur est sur la fenêtre de chargement
                if (m_windowLoadProgress && m_windowLoadProgress->isOpen() &&
                    static_cast<int>(event.x) >= tmp_x &&
                    static_cast<int>(event.x) <= (tmp_x + m_windowLoadProgress->getRealWidth()) &&
                    static_cast<int>(event.y) >= tmp_y &&
                    static_cast<int>(event.y) <= (tmp_y + m_windowLoadProgress->getRealHeight()))
                {
                    // On lui donne le focus
                    m_focus = m_windowLoadProgress;

                    // On transmet l'évènement à la fenêtre
                    m_windowLoadProgress->onEvent(event);
                }

                break;
            }

            //case AppConsole:
                //if (m_windowGameConsole)
                    //m_windowGameConsole->onEvent(event);
                //break;

#ifdef T_ACTIVE_DEBUG_MODE
            case AppDebug:
                if (m_windowDebug)
                {
                    m_focus = m_windowDebug;
                    m_windowDebug->onEvent(event);
                }

                break;
#endif
            case AppExit:
                if (m_windowExit)
                    m_windowExit->onEvent(event);
                break;
        }

        if (event.button == MouseButtonLeft && event.type == ButtonReleased)
        {
            m_selected = nullptr;
        }
    }
*/
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CGuiEngine::onEvent(const CTextEvent& event)
{
    if (m_currentActivity != nullptr && !CApplication::getApp()->isGameActive())
    {
        m_currentActivity->onEvent(event);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CGuiEngine::onEvent(const CKeyboardEvent& event)
{
    if (m_currentActivity != nullptr && !CApplication::getApp()->isGameActive())
    {
        m_currentActivity->onEvent(event);
    }
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void CGuiEngine::onEvent(const CResizeEvent& event)
{
    if (m_currentActivity != nullptr && !CApplication::getApp()->isGameActive())
    {
        m_currentActivity->onEvent(event);
    }
}


/**
 * Méthode appelée au début de chaque frame.
 *
 * \todo Déplacer vers le renderer ou les buffers.
 ******************************/

void CGuiEngine::beginFrame()
{
    // On désactive les textures et le test de profondeur
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

    // Sauvegarde de la matrice
    glPushMatrix();

    // On met en projection orthogonale
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight(), 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    T_ASSERT(m_buffer != nullptr);
    m_buffer->clear();

    // On réinitialise le curseur
    m_cursor = CursorArrow;
}


/**
 * Méthode appelée à chaque frame pour afficher l'interface graphique.
 ******************************/

void CGuiEngine::displayFrame()
{
    if (CApplication::getApp()->isGameActive())
    {
        frameGame();
        return;
    }

    const unsigned int win_w = CApplication::getApp()->getWidth();
    const unsigned int win_h = CApplication::getApp()->getHeight();

    // Fond de l'écran
    if (m_backgroundImage == 0)
    {
        drawRectangle(CRectangle(0, 0, win_w, win_h), m_backgroundColor);
    }
    else
    {
        drawImage(CRectangle(0, 0, win_w, win_h), m_backgroundImage);
    }

    // Mise à jour de l'activité courante
    if (m_currentActivity)
    {
        m_currentActivity->updateActivity();
    }

    if (getPointed())
    {
        TVector2I cur = CApplication::getApp()->getCursorPosition();
        m_cursor = getPointed()->getCursorType(cur.X, cur.Y);
    }
/*
    switch (state)
    {
        default:
            break;

        case AppMenu:
            frameMenu();
            break;

        case AppConsole:
            frameConsole();
            break;

#ifdef T_ACTIVE_DEBUG_MODE
        case AppDebug:
            frameDebug();
            break;
#endif // T_ACTIVE_DEBUG_MODE

        case AppGame:
            frameGame();
            break;

        case AppLoad:
            frameLoad();
            break;

        case AppExit:
            frameExit();
            break;
    }
*/
}


/**
 * Méthode appelée à chaque frame pour l'état MENU.
 ******************************/
/*
void CGuiEngine::frameMenu()
{
    const unsigned int win_w = CApplication::getApp()->getWidth();
    const unsigned int win_h = CApplication::getApp()->getHeight();

    // Fond de l'écran
    if (m_backgroundImage == 0)
    {
        drawRectangle(CRectangle(0, 0, win_w, win_h), m_backgroundColor);
    }
    else
    {
        drawImage(CRectangle(0, 0, win_w, win_h), m_backgroundImage);
    }

    TTextParams params;
    params.text  = CApplication::getApp()->getWindowTitle();
    params.font  = Game::fontManager->getFontId("Verdana");
    params.size  = 40;
    params.color = CColor::White;

    const int txtpos_x = (win_w -  Game::fontManager->getTextSize(params).X) / 2;
    params.rec = CRectangle(txtpos_x, 60);

    // Titre
    drawText(params);

    // Affichage du menu
    if (m_menuInGame && CApplication::getApp()->inGame())
    {
        m_menuInGame->draw();
    }
    else if (m_menu)
    {
        m_menu->draw();
    }
*
    // Informations
    drawRectangle(CRectangle(20, 20, 280, 90), CColor(128, 0, 255, 128));
    drawBorder(CRectangle(20, 20, 280, 90), CColor::Blue, 2);

    TTextParams params2;
    params2.text  = "TEngine 0.3";
    params2.font  = "Verdana";
    params2.size  = 20;
    params2.color = CColor::White;
    params2.rec   = CRectangle(25, 25);

    drawText(params2);

    TTextParams params3;
    params3.text  = "FPS : " + CString::fromNumber(CApplication::getApp()->getFPS());
    params3.font  = "Arial";
    params3.size  = 17;
    params3.color = CColor::White;
    params3.rec.x = 30;
    params3.rec.y = 50;

    drawText(params3);

    TTextParams params4;
    params4.text  = "Surfaces : " + CString::fromNumber(CRenderer::Instance().getNumSurfaces());
    params4.font  = "Arial";
    params4.size  = 17;
    params4.color = CColor::White;
    params4.rec.x = 30;
    params4.rec.y = 65;

    drawText(params4);

    if (CApplication::getApp()->inGame())
    {
        TTextParams params5;
        params5.text  = "Position : " + CRenderer::Instance().getCamera()->getPosition().toString();
        params5.font  = "Arial";
        params5.size  = 17;
        params5.color = CColor::White;
        params5.rec   = CRectangle(30, 80);

        drawText(params5);
    }
*
    // Affichage des fenêtres
    for (std::list<CWindow *>::reverse_iterator it =  m_windows.rbegin(); it != m_windows.rend(); ++it)
    {
        if ((*it)->isOpen())
        {
            (*it)->draw();
        }
    }

    // Affichage de l'élément au premier plan
    if (m_foreground)
    {
        m_foreground->draw();
    }

    if (m_pointed)
    {
        TVector2I cur = CApplication::getApp()->getCursorPosition();
        m_cursor = m_pointed->getCursorType(cur.X, cur.Y);
    }
}
*/

/**
 * Méthode appelée à chaque frame pour l'état GAME.
 ******************************/

void CGuiEngine::frameGame()
{
    m_cursor = CursorEmpty;

    // Mise-à-jour et affichage des HUD
    for (std::list<IHUD *>::iterator it = m_hud.begin(); it != m_hud.end(); ++it)
    {
        (*it)->update();
        (*it)->paint();
    }
}


/**
 * Méthode appelée à chaque frame pour afficher la console dans le jeu.
 ******************************/
/*
void CGuiEngine::frameConsole()
{
    // Rectangle gris sur la moitié haute de l'écran
    drawRectangle(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_backgroundColor);
    drawRectangle(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight() / 2), CColor(0, 0, 0, 192));

    //...
}
*/
#ifdef T_ACTIVE_DEBUG_MODE

/**
 * Méthode appelée à chaque frame pour afficher l'interface de debug.
 ******************************/
/*
void CGuiEngine::frameDebug()
{
    // Rectangle gris sur tout l'écran
    drawRectangle(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_backgroundColor);

    m_windowDebug->draw();
}
*/
#endif // T_ACTIVE_DEBUG_MODE

/**
 * Méthode appelée à chaque frame pour l'état LOAD.
 ******************************/
/*
void CGuiEngine::frameLoad()
{
    // Fond noir
    drawRectangle(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), CColor::Black);

    // Afficage de la boite de dialogue
    if (m_windowLoadProgress)
    {
        m_windowLoadProgress->draw();

        TVector2I cur = CApplication::getApp()->getCursorPosition();
        m_cursor = m_windowLoadProgress->getCursorType(cur.X, cur.Y);
    }
}
*/

/**
 * Méthode appelée à chaque frame pour l'état EXIT.
 ******************************/
/*
void CGuiEngine::frameExit()
{
    // Fond de l'écran
    if (m_backgroundImage == 0)
    {
        drawRectangle(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_backgroundColor);
    }
    else
    {
        drawImage(CRectangle(0, 0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight()), m_backgroundImage);
    }

*
    // Informations
    drawRectangle(CRectangle(20, 20, 280, 90), CColor(128, 0, 255, 128));
    drawBorder(CRectangle(20, 20, 280, 90), CColor::Blue, 2);

    TTextParams params1;
    params1.text  = "TEngine 0.2";
    params1.font  = "Verdana";
    params1.size  = 20;
    params1.color = CColor::White;
    params1.rec   = CRectangle(25, 25);

    drawText(params1);

    TTextParams params2;
    params2.text  = "FPS : " + toString(CApplication::getApp()->getFPS());
    params2.font  = "Arial";
    params2.size  = 17;
    params2.color = CColor::White;
    params2.rec   = CRectangle(30, 50);

    drawText (params2);

    TTextParams params3;
    params3.text  = "Surfaces : " + toString(CRenderer::Instance().getNumSurfaces());
    params3.font  = "Arial";
    params3.size  = 17;
    params3.color = CColor::White;
    params3.rec   = CRectangle(30, 65);

    drawText(params3);

    if (CApplication::getApp()->inGame())
    {
        TTextParams params4;
        params4.text  = "Position : " + toString(CRenderer::Instance().getCamera()->getPosition());
        params4.font  = "Arial";
        params4.size  = 17;
        params4.color = CColor::White;
        params4.rec   = CRectangle(30, 80);

        drawText(params4);
    }
*
    // Affichage de la boite de dialogue
    if (m_windowExit)
    {
        m_windowExit->draw();

        TVector2I cur = CApplication::getApp()->getCursorPosition();
        m_cursor = m_windowExit->getCursorType(cur.X, cur.Y);
    }
}
*/

/**
 * Méthode appelée à la fin de chaque frame.
 *
 * \todo Déplacer vers le renderer ou les buffers.
 * \todo Afficher le curseur de la souris.
 ******************************/

void CGuiEngine::endFrame()
{
    T_ASSERT(m_buffer != nullptr);

    // Gestion du curseur
    if (CApplication::getApp()->isCursorVisible())
    {
        TVector2I pos = CApplication::getApp()->getCursorPosition();

        switch (m_cursor)
        {
            default:
            case CursorArrow:
                m_buffer->addImage(CRectangle(pos.X - m_cursorArrow.getPointerX(),
                                              pos.Y - m_cursorArrow.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorArrow.getTextureId());
                break;

            case CursorHand:
                m_buffer->addImage(CRectangle(pos.X - m_cursorHand.getPointerX(),
                                              pos.Y - m_cursorHand.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorHand.getTextureId());
                break;

            case CursorText:
                m_buffer->addImage(CRectangle(pos.X - m_cursorText.getPointerX(),
                                              pos.Y - m_cursorText.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorText.getTextureId());
                break;

            case CursorResizeNESW:
                m_buffer->addImage(CRectangle(pos.X - m_cursorResizeNE_SW.getPointerX(),
                                              pos.Y - m_cursorResizeNE_SW.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorResizeNE_SW.getTextureId());
                break;

            case CursorResizeNS:
                m_buffer->addImage(CRectangle(pos.X - m_cursorResizeN_S.getPointerX(),
                                              pos.Y - m_cursorResizeN_S.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorResizeN_S.getTextureId());
                break;

            case CursorResizeNWSE:
                m_buffer->addImage(CRectangle(pos.X - m_cursorResizeNW_SE.getPointerX(),
                                              pos.Y - m_cursorResizeNW_SE.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorResizeNW_SE.getTextureId());
                break;

            case CursorResizeWE:
                m_buffer->addImage(CRectangle(pos.X - m_cursorResizeW_E.getPointerX(),
                                              pos.Y - m_cursorResizeW_E.getPointerY(),
                                              CCursor::CursorSize, CCursor::CursorSize),
                                   m_cursorResizeW_E.getTextureId());
                break;
        }
    }


    m_buffer->update();
    m_buffer->draw();

    // On revient à une projection en perspective
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    Game::renderer->perpective();

    // Restauration de la matrice précedemment enregistrée
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
}


/**
 * Méthode appelée par une fenêtre lors de son ouverture, place la fenêtre au
 * début de la liste.
 *
 * \param window Pointeur sur la fenêtre.
 *
 * \sa CGuiEngine::onWindowClose
 ******************************/

void CGuiEngine::onWindowOpen(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    // On avertit chaque activité
    for (std::map<CString, CActivity *>::const_iterator it = m_activities.begin(); it != m_activities.end(); ++it)
    {
        it->second->onWindowOpen(window);
    }
}


/**
 * Méthode appelée par une fenêtre lors de sa fermeture, place la fenêtre à la
 * fin de la liste.
 *
 * \param window Pointeur sur la fenêtre.
 *
 * \sa CGuiEngine::onWindowOpen
 ******************************/

void CGuiEngine::onWindowClose(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    // On avertit chaque activité
    for (std::map<CString, CActivity *>::const_iterator it = m_activities.begin(); it != m_activities.end(); ++it)
    {
        it->second->onWindowClose(window);
    }
}


/**
 * Ajoute un HUD à la liste.
 *
 * \param hud Pointeur sur le HUD à ajouter.
 ******************************/

void CGuiEngine::addHUD(IHUD * hud)
{
    if (hud == nullptr)
    {
        return;
    }

    std::list<IHUD *>::const_iterator it = std::find(m_hud.begin(), m_hud.end(), hud);

    if (it == m_hud.end())
    {
        m_hud.insert(m_hud.end(), hud);
    }
}


/**
 * Enlève un HUD de la liste.
 *
 * \param hud Pointeur sur le HUD à enlever.
 ******************************/

void CGuiEngine::removeHUD(IHUD * hud)
{
    if (hud == nullptr)
        return;

    std::list<IHUD *>::iterator it = std::find(m_hud.begin(), m_hud.end(), hud);

    if (it != m_hud.end())
    {
        m_hud.erase(it);
    }
}


/**
 * Donne le nombre de HUD à afficher.
 *
 * \return Nombre de HUD à afficher.
 ******************************/

unsigned int CGuiEngine::getNumHUD() const
{
    return m_hud.size();
}


/*/*
 * Input utilisé pour ouvrir la fenêtre Console.
 *
 * \todo Supprimer cette méthode et utiliser les activités à la place.
 ******************************/
/*
void CGuiEngine::inputWindowConsole()
{
    // La fenêtre n'est pas au sommet de la liste
    if (m_windowConsole)
    {
        // On ouvre la fenêtre et on lui donne le focus
        m_windowConsole->open();
        setFocus(m_windowConsole);
    }
}
*/

/**
 * Input utilisé pour ouvrir la fenêtre Options.
 *
 * \todo Supprimer cette méthode et utiliser les activités à la place.
 ******************************/
/*
void CGuiEngine::inputWindowOptions()
{
    // La fenêtre n'est pas au sommet de la liste
    if (m_windowOptions)
    {
        // On ouvre la fenêtre et on lui donne le focus
        m_windowOptions->open();
        setFocus(m_windowOptions);
    }
}
*/

/**
 * Input utilisé pour ouvrir la fenêtre Chargement.
 *
 * \todo Supprimer cette méthode et utiliser les activités à la place.
 ******************************/
/*
void CGuiEngine::inputWindowLoad()
{
    // La fenêtre n'est pas au sommet de la liste
    if (m_windowLoad)
    {
        // On ouvre la fenêtre et on lui donne le focus
        m_windowLoad->open();
        setFocus(m_windowLoad);
    }
}
*/

/**
 * Input utilisé pour ouvrir la fenêtre Sauvegarde.
 *
 * \todo Supprimer cette méthode et utiliser les activités à la place.
 ******************************/
/*
void CGuiEngine::inputWindowSave()
{
    // La fenêtre n'est pas au sommet de la liste
    if (m_windowSave)
    {
        // On ouvre la fenêtre et on lui donne le focus
        m_windowSave->open();
        setFocus(m_windowSave);
    }
}
*/

/**
 * Input utilisé pour ouvrir la boite de dialogue Quitter.
 ******************************/
/*
void CGuiEngine::inputWindowExit()
{
    CApplication::getApp()->setState(AppExit);

    setFocus(nullptr);
    setPointed(nullptr);
    setSelected(nullptr);
}
*/

/**
 * Méthode utilisée pour fermer l'application.
 ******************************/

void CGuiEngine::inputExit()
{
    CApplication::getApp()->setInactive();
}


/**
 * Input utilisé pour reprendre la partie en cours.
 ******************************/

void CGuiEngine::inputResume()
{
    if (CApplication::getApp()->inGame())
    {
        CApplication::getApp()->setGameActive(true);
        CApplication::getApp()->showMouseCursor(false);

        setFocus(nullptr);
        setPointed(nullptr);
        setSelected(nullptr);
    }
}


/**
 * Input utilisé pour fermer la boite de dialogue Quitter.
 *
 * \todo Supprimer cette méthode et utiliser les activités à la place.
 ******************************/
/*
void CGuiEngine::inputEndStateExit()
{
    CApplication::getApp()->setState(AppMenu);

    // On réinitialise les paramètres de la fenêtre
    m_windowExit->open();
    m_windowExit->setCenterX();
    m_windowExit->setCenterY();
    m_windowExit->setFocus();
}
*/

/**
 * Ajoute une activité à l'application.
 * Si une activité porte le même nom, rien n'est fait.
 *
 * \param activity Pointeur sur l'activité à ajouter.
 ******************************/

void CGuiEngine::addActivity(CActivity * activity)
{
    if (activity == nullptr)
    {
        return;
    }

    CString activityName = activity->getActivityName();

    // On cherche si l'activité est déjà dans la liste ou si une activité porte le même nom
    for (std::map<CString, CActivity *>::iterator it = m_activities.begin(); it != m_activities.end(); ++it)
    {
        if (it->first == activityName)
        {
            CApplication::getApp()->log(CString::fromUTF8("Une activité porte déjà ce nom (%1)").arg(activityName), ILogger::Warning);
            return;
        }

        if (it->second == activity)
        {
            return;
        }
    }

    m_activities[activityName] = activity;
}


/**
 * Enlève une activité à l'application.
 *
 * \param activity Pointeur sur l'activité à enlever.
 */

void CGuiEngine::removeActivity(CActivity * activity)
{
    if (activity == nullptr)
    {
        return;
    }

    // On cherche l'activité dans la liste
    for (std::map<CString, CActivity *>::iterator it = m_activities.begin(); it != m_activities.end(); ++it)
    {
        if (it->second == activity)
        {
            m_activities.erase(it);
            return;
        }
    }
}


/**
 * Recherche une activité à partir de son nom.
 *
 * \param activityName Nom de l'activité à rechercher.
 ******************************/

CActivity * CGuiEngine::getActivity(const CString& activityName) const
{
    // On cherche l'activité dans la liste
    for (std::map<CString, CActivity *>::const_iterator it = m_activities.begin(); it != m_activities.end(); ++it)
    {
        if (it->first == activityName)
        {
            return it->second;
        }
    }

    return nullptr;
}


CActivity * CGuiEngine::getCurrentActivity() const
{
    return m_currentActivity;
}


/**
 * Change l'activité courante.
 *
 * \param activityName Nom de l'activité à démarrer.
 ******************************/

void CGuiEngine::setCurrentActivity(const CString& activityName)
{
    setCurrentActivity(getActivity(activityName));
}


/**
 * Change l'activité courante.
 *
 * \param activity Pointeur sur l'activité à démarrer.
 ******************************/

void CGuiEngine::setCurrentActivity(CActivity * activity)
{
    if (activity == nullptr || activity == m_currentActivity)
    {
        return;
    }

    // Arrêt de l'ancienne activité
    if (m_currentActivity != nullptr)
    {
        m_currentActivity->stop();
    }

    // Démarrage de la nouvelle activité.
    m_currentActivity = activity;
    m_currentActivity->start();
}

} // Namespace Ted

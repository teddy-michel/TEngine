/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDialog.cpp
 * \date       2008 Création de la classe GuiDialog.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CDialog.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CDialog.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CLabel.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CLayoutHorizontal.hpp"
#include "Gui/CLayoutVertical.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param title  Titre de la boite de dialogue.
 * \param text   Texte de la boite de dialogue.
 * \param flags  Paramètres supplémentaires (boutons).
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDialog::CDialog(const CString& title, const CString& text, TDialogButtons flags, IWidget * parent) :
CWindow (title, WindowCenter | WindowNotResizable, parent),
m_label (nullptr),
m_text  (text)
{
    // Remarque : on n'a pas besoin de garder en mémoire les boutons, ils seront
    //            supprimés par le layout qui les contient.

    CLayoutVertical * layout0 = new CLayoutVertical(this);
    layout0->setPadding(CMargins(5, 5, 5, 5));
    m_child = layout0;

    CLayoutHorizontal * layout = new CLayoutHorizontal(this);
    layout->setPadding(CMargins(5, 5, 5, 5));

    // Texte
    m_label = new CLabel(text, this);
    m_label->setTextAlignment(AlignCenter);

    layout0->addChild(m_label);
    layout0->addChild(layout);

    // Bouton "Oui"
    if (flags & ButtonYes)
    {
        CPushButton * button = new CPushButton("Oui", this);
        button->setMinWidth(80);
        button->onClicked.connect(onButtonYes);

        layout->addChild(button);
    }

    // Bouton "Non"
    if (flags & ButtonNo)
    {
        CPushButton * button = new CPushButton("Non", this);
        button->setMinWidth(80);
        button->onClicked.connect(onButtonNo);

        layout->addChild(button);
    }

    // Bouton "OK"
    if (flags & ButtonOK)
    {
        CPushButton * button = new CPushButton("OK", this);
        button->setMinWidth(80);
        button->onClicked.connect(onButtonOK);

        layout->addChild(button);
    }

    // Bouton "Annuler"
    if (flags & ButtonCancel)
    {
        CPushButton * button = new CPushButton("Annuler", this);
        button->setMinWidth(80);
        button->onClicked.connect(onButtonCancel);

        layout->addChild(button);
    }

    setMinHeight(80);
}


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDialog::CDialog(IWidget * parent) :
CWindow (CString(), WindowCenter | WindowNotResizable, parent),
m_label (nullptr)
{
    // Remarque : on n'a pas besoin de garder en mémoire les boutons, ils seront
    //            supprimés par le layout qui les contient.

    CLayoutVertical * layout0 = new CLayoutVertical(this);
    layout0->setPadding(CMargins(5, 5, 5, 5));
    m_child = layout0;

    CLayoutHorizontal * layout = new CLayoutHorizontal(this);
    layout->setPadding(CMargins(5, 5, 5, 5));

    m_label = new CLabel(this);
    m_label->setTextAlignment(AlignCenter);

    layout0->addChild(m_label);
    layout0->addChild(layout);

    setMinHeight(80);
}


/**
 * Donne le texte de la boite de dialogue.
 *
 * \return Texte de la boite de dialogue.
 *
 * \sa CDialog::setText
 ******************************/

CString CDialog::getText() const
{
    return m_label->getText();
}


/**
 * Modifie le texte de la boite de dialogue.
 *
 * \param text Texte de la boite de dialogue.
 *
 * \sa CDialog::getText
 ******************************/

void CDialog::setText(const CString& text)
{
    m_label->setText(text);
}

} // Namespace Ted

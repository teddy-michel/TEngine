/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLineEdit.cpp
 * \date 26/04/2009 Création de la classe GuiLineEdit.
 * \date 16/07/2010 Utilisation possible des signaux.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 20/07/2010 Utilisation des codes clavier internes.
 * \date 22/07/2010 Modification des codes spéciaux pour surligner le texte.
 * \date 23/07/2010 L'affichage du texte est limité dans un rectangle.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 30/10/2010 Corrections de bugs liés à la sélection du texte.
 * \date 30/10/2010 On peut annuler la dernière modification.
 * \date 04/11/2010 Héritage de la classe GuiBaseTextEdit.
 * \date 11/11/2010 On peut sélectionner le texte à la souris.
 * \date 16/12/2010 Correction d'un bug lors de la sélection du texte à la souris.
 * \date 17/01/2011 Héritage de la classe GuiWidget (car GuiBaseTextEdit n'en hérite plus).
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 26/03/2011 Correction d'un problème d'affichage du texte et du curseur en dehors du widget.
 * \date 29/05/2011 La classe est renommée en CLineEdit.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Time.hpp"
#include "Core/Events.hpp"


namespace Ted
{

const unsigned int LineEditMinWidth = 100; ///< Largeur minimale en pixels.
const unsigned int LineEditHeight = 20;    ///< Hauteur du widget en pixels.


/**
 * Constructeur par défaut.
 *
 * \param text   Texte par défaut.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLineEdit::CLineEdit(const CString& text, IWidget * parent) :
IWidget    (parent),
ITextEdit  (text),
m_password (false),
m_left     (0)
{
    setMinSize(LineEditMinWidth, LineEditHeight);
    setMaxHeight(LineEditHeight);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLineEdit::CLineEdit(IWidget * parent) :
IWidget    (parent),
ITextEdit  (CString()),
m_password (false),
m_left     (0)
{
    setMinSize(LineEditMinWidth, LineEditHeight);
    setMaxHeight(LineEditHeight);
}


/**
 * Indique si le texte doit être masqué, pour rentrer un mot de passe par exemple.
 *
 * \return Indique si le texte doit être masqué.
 *
 * \sa CLineEdit::setPassword
 ******************************/

bool CLineEdit::isPassword() const
{
    return m_password;
}


/**
 * Masque ou affiche le texte, pour rentrer un mot de passe par exemple.
 *
 * \param password Indique si le texte doit être masqué.
 *
 * \sa CLineEdit::isPassword
 ******************************/

void CLineEdit::setPassword(bool password)
{
    m_password = password;
}


/**
 * Dessine le champ texte.
 *
 * \bug Si on affiche un mot de passe, les caractères spéciaux sont aussi transformés en étoile.
 * \todo Remplacer correctement les caractères dans un mot de passe.
 ******************************/

void CLineEdit::draw()
{
    const unsigned int time = getElapsedTime();
    const unsigned int frameTime = time - m_time;
    m_time = time;

    m_timeBlink += frameTime;

    const int tmp_x = getX();
    const int tmp_y = getY();

    const bool hasfocus = Game::guiEngine->hasFocus(this);

    // Fond du champ et bordure
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::White);
    Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor(28, 38, 60));

    CString tmp_txt;

    // Sélection du texte si le champ a le focus
    if (hasfocus && m_select < 0)
    {
        tmp_txt = m_params.text.subString(0, m_cursor + m_select) +
                  static_cast<char>(12) +
                  m_params.text.subString(m_cursor + m_select, -m_select) +
                  static_cast<char>(12) +
                  m_params.text.subString(m_cursor);
    }
    else if (hasfocus && m_select > 0)
    {
        tmp_txt = m_params.text.subString(0, m_cursor) +
                  static_cast<char>(12) +
                  m_params.text.subString(m_cursor, m_select) +
                  static_cast<char>(12) +
                  m_params.text.subString(m_cursor + m_select);
    }
    else
    {
        tmp_txt = m_params.text;
    }

    TTextParams params;
    params.text  = tmp_txt;
    params.font  = Game::fontManager->getFontId("Arial");
    params.size  = m_params.size;
    params.color = m_params.color;
    params.rec.setX(tmp_x + 5);
    params.rec.setY(tmp_y);
    params.rec.setWidth(m_width - 10);
    params.rec.setHeight(m_height);
    params.left  = m_left;

    // Affichage du texte
    if (m_password)
    {
        params.text = CString('*', m_params.text.getSize()); //BUG: Les caractères non affichables sont aussi transformés en étoile
    }

    Game::guiEngine->drawText(params);

    // Affichage de la barre verticale clignotante si le champ a le focus
    if (hasfocus)
    {
        TTextParams params2;
        params2.text = m_params.text.subString(0, m_cursor);
        params2.font = Game::fontManager->getFontId("Arial");
        params2.size = m_params.size;

        int pos_cursor = Game::fontManager->getTextSize(params2).X + 5;

        // Curseur
        if (pos_cursor < m_width && (m_timeBlink / timeBlink) % 2)
        {
            Game::guiEngine->drawLine(TVector2F(tmp_x + pos_cursor, tmp_y + 2),
                                      TVector2F(tmp_x + pos_cursor, tmp_y + m_height - 2),
                                      CColor::Black);
        }
    }
}


/**
 * Indique le curseur à utiliser en fonction de la position du curseur de la souris.
 *
 * \param x Coordonnée horizontale du curseur.
 * \param y Coordonnée verticale du curseur.
 * \return Curseur à utiliser (barre de texte).
 ******************************/

TCursor CLineEdit::getCursorType(int x, int y) const
{
    T_UNUSED(x);
    T_UNUSED(y);

    return CursorText;
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CLineEdit::onEvent(const CTextEvent& event)
{
    const char character = static_cast<char>(event.unicode);

    // On n'ajoute pas les caractères multi-lignes
    if (character == '\n' || character == '\r' || character == '\v')
    {
        return;
    }

    ITextEdit::onTextEvent(event);
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CLineEdit::onEvent(const CKeyboardEvent& event)
{
    ITextEdit::onKeyboardEvent(event);
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CLineEdit::onEvent(const CMouseEvent& event)
{
    // Double-clic : on sélectionne tout le texte
    if (event.type == DblClick && event.button == MouseButtonLeft)
    {
        selectAll();
        return;
    }

    // Clic gauche
    if (event.button == MouseButtonLeft && event.type == ButtonPressed)
    {
        onClick();

        unsigned int i = 0;
        int pos = 0, pos_old = 0;

        const unsigned int text_length = m_params.text.getSize();
        const int pos_max = static_cast<int>(event.x) - getX() - 5;

        while (i < text_length && pos < pos_max)
        {
            pos_old = pos;

            TTextParams params;
            params.text = m_params.text.subString(0, ++i);
            params.font = Game::fontManager->getFontId("Arial");
            params.size = m_params.size;

            pos = Game::fontManager->getTextSize(params).X;
        }

        m_cursor = ((pos_max - pos_old) < (pos - pos_max) && i != 0 ? i - 1 : i);
        m_select = 0; // On désélectionne le texte
        m_timeBlink = timeBlink;

        return;
    }

    // Déplacement du curseur si on on a cliqué sur le widget
    if (event.type == MoveMouse && Game::guiEngine->isSelected(this))
    {
        unsigned int i = 0;
        int pos = 0, pos_old = 0;

        const unsigned int text_length = m_params.text.getSize();
        const int pos_max = static_cast<int>(event.x) - getX() - 5;

        while (i < text_length && pos < pos_max)
        {
            pos_old = pos;

            TTextParams params;
            params.text = m_params.text.subString(0, ++i);
            params.font = Game::fontManager->getFontId("Arial");
            params.size = m_params.size;

            pos = Game::fontManager->getTextSize(params).X;
        }

        int new_cursor = ((pos_max - pos_old) < (pos - pos_max) && i != 0 ? i - 1 : i);

        m_select += static_cast<int>(m_cursor) - new_cursor;
        if (new_cursor + m_select < 0)
            m_select = -new_cursor;

        m_cursor = new_cursor;
    }
}

} // Namespace Ted

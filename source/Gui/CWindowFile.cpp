/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindowFile.cpp
 * \date 18/02/2010 Création de la classe GuiWindowFile.
 * \date 29/05/2011 La classe est renommée en CWindowFile.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindowFile.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CLineEdit.hpp"
#include "Gui/CLayoutVertical.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param dir    Répertoire à afficher (par défaut le répertoire courant).
 * \param filter Filtres à appliquer aux noms de fichier et de répertoire.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindowFile::CWindowFile(const CString& dir, const CString& filter, IWidget * parent) :
CWindow         (parent),
m_dirs_only     (false),
m_mode          (ExistingFileOpen),
m_suffix        (CString()),
m_dir           (dir),
m_filter        (filter),
m_button_accept (nullptr),
m_button_cancel (nullptr),
m_line          (nullptr)
{
    m_child = new CLayoutVertical();

    m_button_accept = new CPushButton("Ouvrir", m_child);
    m_button_cancel = new CPushButton("Annuler", m_child);
    m_line = new CLineEdit(m_child);
}


/**
 * Destructeur.
 ******************************/

CWindowFile::~CWindowFile()
{
    delete m_button_accept;
    delete m_button_cancel;
    delete m_line;
}


/**
 * Indique qu'on doit afficher uniquement les répertoires.
 *
 * \return Booléen.
 ******************************/

bool CWindowFile::isDirsOnly() const
{
    return m_dirs_only;
}


/**
 * Donne le mode de sélection des fichiers.
 *
 * \return Mode de sélection.
 ******************************/

TFileMode CWindowFile::getMode() const
{
    return m_mode;
}


/**
 * Donne le suffixe à ajouter aux noms des fichiers sélectionnés.
 *
 * \return Suffixe à ajouter aux noms de fichier.
 *
 * \sa CWindowFile::setSuffix
 ******************************/

CString CWindowFile::getSuffix() const
{
    return m_suffix;
}


/**
 * Donne le répertoire affiché.
 *
 * \return Répertoire affiché.
 ******************************/

CString CWindowFile::getDirectory() const
{
    return m_dir;
}


/**
 * Donne le filtre à appliquer aux noms de fichiers et répertoires.
 *
 * \return Filtre à appliquer aux noms de fichiers et répertoires.
 *
 * \sa CWindowFile::setFilter
 ******************************/

CString CWindowFile::getFilter() const
{
    return m_filter;
}


/**
 * Modifie le type d'affichage : tous les fichiers ou seulement les dossiers.
 *
 * \param value Indique qu'on doit afficher uniquement les répertoires.
 ******************************/

void CWindowFile::setDirsOnly(bool value)
{
    m_dirs_only = value;
}


/**
 * Modifie le mode de sélection.
 *
 * \param mode Mode de sélection.
 ******************************/

void CWindowFile::setMode(TFileMode mode)
{
    m_mode = mode;
}


/**
 * Modifie le suffixe.
 *
 * \param suffix Suffixe à ajouter aux noms de fichier.
 *
 * \sa CWindowFile::getSuffix
 ******************************/

void CWindowFile::setSuffix(const CString& suffix)
{
    m_suffix = suffix;
}


/**
 * Change le répertoire affiché.
 *
 * \todo Implémentation.
 *
 * \param dir Nom du répertoire.
 ******************************/

void CWindowFile::setDirectory(const CString& dir)
{
    m_dir = dir;

    //...
}


/**
 * Modifie le filtre.
 *
 * \param filter Filtre à appliquer aux noms de fichiers et répertoires.
 ******************************/

void CWindowFile::setFilter(const CString& filter)
{
    m_filter = filter;
}


/**
 * Affiche le répertoire parent.
 * Équivalent à \code setDirectory("../"); \endcode
 ******************************/

void CWindowFile::parentDirectory()
{
    setDirectory("../");
}

} // Namespace Ted

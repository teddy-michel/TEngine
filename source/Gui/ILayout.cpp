/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/ILayout.cpp
 * \date 29/05/2009 Création de la classe GuiBaseLayout.
 * \date 29/05/2011 La classe est renommée en ILayout.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/ILayout.hpp"
#include "Gui/CGuiEngine.hpp"


namespace Ted
{

const int LayoutMaxPadding = 10; ///< Marge intérieure maximale.
const int LayoutMaxMargin  = 20; ///< Marge extérieure maximale.


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

ILayout::ILayout(IWidget * parent) :
IWidget   (parent),
m_padding (CMargins(2)),
m_margin  (CMargins(0))
{
    if (parent)
    {
        setSize(parent->getWidth(), parent->getHeight());
    }
}


/**
 * Destructeur.
 ******************************/

ILayout::~ILayout()
{ }


/**
 * Donne les valeurs des marges internes.
 *
 * \return Marges internes de chaque case en pixels.
 *
 * \sa ILayout::setPadding
 ******************************/

CMargins ILayout::getPadding() const
{
    return m_padding;
}


/**
 * Donne les valeurs des marges extérieures.
 *
 * \return Marges extérieures en pixels.
 *
 * \sa ILayout::setMargin
 ******************************/

CMargins ILayout::getMargin() const
{
    return m_margin;
}


/**
 * Modifie la valeur de la marge interne.
 *
 * \param padding Marge interne de chaque case en pixels.
 *
 * \sa ILayout::getPadding
 ******************************/

void ILayout::setPadding(const CMargins& padding)
{
    m_padding.setLeft  (padding.getLeft()   > LayoutMaxPadding ? LayoutMaxPadding : padding.getLeft()  );
    m_padding.setTop   (padding.getTop()    > LayoutMaxPadding ? LayoutMaxPadding : padding.getTop()   );
    m_padding.setRight (padding.getRight()  > LayoutMaxPadding ? LayoutMaxPadding : padding.getRight() );
    m_padding.setBottom(padding.getBottom() > LayoutMaxPadding ? LayoutMaxPadding : padding.getBottom());
}


/**
 * Modifie la valeur de la marge extérieure.
 *
 * \param margin Marge extérieure en pixels.
 *
 * \sa ILayout::getMargin
 ******************************/

void ILayout::setMargin(const CMargins& margin)
{
    m_margin.setLeft  (margin.getLeft()   > LayoutMaxPadding ? LayoutMaxMargin : margin.getLeft()  );
    m_margin.setTop   (margin.getTop()    > LayoutMaxPadding ? LayoutMaxMargin : margin.getTop()   );
    m_margin.setRight (margin.getRight()  > LayoutMaxPadding ? LayoutMaxMargin : margin.getRight() );
    m_margin.setBottom(margin.getBottom() > LayoutMaxPadding ? LayoutMaxMargin : margin.getBottom());
}

} // Namespace Ted

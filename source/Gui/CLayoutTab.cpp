/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CLayoutTab.cpp
 * \date 27/05/2009 Création de la classe GuiTab.
 * \date 16/07/2010 Utilisation possible des signaux, création du signal onTabChange.
 * \date 23/07/2010 L'affichage du texte du nom de l'onglet est limité dans un rectangle.
 * \date 29/05/2011 La classe est renommée en CTab.
 * \date 04/04/2012 La classe est renommée en CLayoutTab.
 * \date 04/04/2012 On peut modifier la position de la barre d'onglets.
 * \date 11/04/2012 Utilisation de la classe CMargins pour gérer les marges.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CLayoutTab.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/Maths/CVector2.hpp"
#include "Core/Events.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

const int TabHeight = 20; ///< Hauteur de la barre d'onglets.
const int TabMargin = 10; ///< Marge dans la barre d'onglets.
const int TabSpacing = 2; ///< Espace entre chaque onglet.


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CLayoutTab::CLayoutTab(IWidget * parent) :
ILayout    (parent),
m_open     (-1),
m_position (Top)
{
    computeSize();
}


/**
 * Destructeur.
 ******************************/

CLayoutTab::~CLayoutTab()
{
    clearTabs();
}


/**
 * Donne le numéro de l'onglet ouvert.
 *
 * \return Numéro de l'onglet ouvert.
 *
 * \sa CLayoutTab::setOpenTab
 ******************************/

int CLayoutTab::getOpenTab() const
{
    return m_open;
}


/**
 * Donne la position de la barre d'onglets.
 *
 * \return Position de la barre d'onglets.
 ******************************/

CLayoutTab::TTabPosition CLayoutTab::getTabPosition() const
{
    return m_position;
}


/**
 * Donne le nombre d'onglets de la barre.
 *
 * \return Nombre d'onglets.
 ******************************/

int CLayoutTab::getNumTabs() const
{
    return m_tabs.size();
}


/**
 * Donne le widget d'un onglet.
 *
 * \param pos Position de l'onglet.
 * \return Pointeur sur le widget.
 ******************************/

IWidget * CLayoutTab::getTab(int pos) const
{
    if (pos < 0 || pos >= getNumTabs())
        return nullptr;

    std::list<TabItem>::const_iterator it = m_tabs.begin();
    std::advance(it, pos);

    return (it == m_tabs.end() ? nullptr : it->tab);
}


/**
 * Change le numéro de l'onglet affiché.
 *
 * \param pos Position de l'onglet à afficher.
 *
 * \sa CLayoutTab::getOpenTab
 ******************************/

void CLayoutTab::setOpenTab(int pos)
{
    int temp = m_open;

    if (pos < 0 || pos >= getNumTabs())
    {
        m_open = 0;
    }
    else
    {
        std::list<TabItem>::const_iterator it = m_tabs.begin();
        std::advance(it, pos);

        m_open = (it == m_tabs.end() ? 0 : pos);
    }

    if (m_open != temp)
    {
        onTabChange();
    }
}


/**
 * Modifie la position de la barre d'onglet.
 *
 * \param position Position de la barre d'onglet.
 *
 * \sa CLayoutTab::getTabPosition
 ******************************/

void CLayoutTab::setTabPosition(TTabPosition position)
{
    if (m_position != position)
    {
        m_position = position;
        computeSize();
    }
}


/**
 * Mutateur pour width.
 *
 * \param width Largeur du layout en pixels.
 *
 * \sa CLayoutTab::setHeight
 ******************************/

void CLayoutTab::setWidth(int width)
{
    if (width < 0)
        width = 0;

    m_width = width;

    if (m_parent && m_parent->getWidth() < width)
    {
        m_parent->setWidth(width);
    }

    computeSize();
}


/**
 * Mutateur pour height.
 *
 * \param height Hauteur du layout en pixels.
 *
 * \sa CLayoutTab::setWidth
 ******************************/

void CLayoutTab::setHeight(int height)
{
    if (height < 0)
        height = 0;

    m_height = height;

    if (m_parent && m_parent->getHeight() < height)
    {
        m_parent->setHeight(height);
    }

    computeSize();
}


/**
 * Ajoute un onglet à la barre.
 *
 * \param name  Nom de l'onglet.
 * \param tab   Pointeur sur l'objet de l'onglet.
 * \param pos   Position de l'onglet (par défaut en dernière position).
 * \param align Alignement de l'objet à l'intérieur de l'onglet.
 ******************************/

void CLayoutTab::addTab(const CString& name, IWidget * tab, int pos, TAlignment align)
{
    if (tab)
    {
        tab->setParent(this);

        TabItem item;

        item.name = name;
        item.tab = tab;
        item.align = align;

        if (pos < 0 || pos >= getNumTabs())
        {
            m_tabs.push_back(item);
        }
        else
        {
            std::list<TabItem>::iterator it = m_tabs.begin();
            std::advance(it, pos);
            m_tabs.insert(it, item);
        }

        if (m_open < 0)
            m_open = 0;

        computeSize();
    }
}


/**
 * Ajoute un onglet à la barre.
 *
 * \param name  Nom de l'onglet.
 * \param tab   Pointeur sur l'objet de l'onglet.
 * \param align Alignement de l'objet à l'intérieur de l'onglet.
 ******************************/

void CLayoutTab::addTab(const CString& name, IWidget * tab, TAlignment align)
{
    addTab(name, tab, -1, align);
}


/**
 * Enlève un onglet de la barre.
 * L'objet \a tab stocké dans l'onglet n'est pas supprimé.
 *
 * \param tab Pointeur sur l'objet à enlever.
 ******************************/

void CLayoutTab::removeTab(IWidget * tab)
{
    // On parcourt la liste des onglets
    for (std::list<TabItem>::iterator it = m_tabs.begin(); it != m_tabs.end(); )
    {
        if (it->tab == tab)
        {
            tab->setParent(nullptr);
            it = m_tabs.erase(it);

            if (m_open >= getNumTabs())
            {
                --m_open;
                onTabChange();
            }

            computeSize();
        }
        else
        {
            ++it;
        }
    }
}


/**
 * Enlève un onglet de la barre.
 * L'objet stocké dans l'onglet n'est pas supprimé.
 *
 * \param pos Position de l'onglet à enlever.
 ******************************/

void CLayoutTab::removeTab(int pos)
{
    if (pos < 0 || pos >= getNumTabs())
        return;

    std::list<TabItem>::iterator it = m_tabs.begin();
    std::advance(it, pos);

    if (it != m_tabs.end())
    {
        it->tab->setParent(nullptr);
        m_tabs.erase(it);

        if (m_open >= getNumTabs())
        {
            --m_open;
            onTabChange();
        }

        computeSize();
    }
}


/**
 * Supprime tous les onglets et leur contenu.
 ******************************/

void CLayoutTab::clearTabs()
{
    // On supprime chaque widget un par un
    for (std::list<TabItem>::iterator it = m_tabs.begin(); it != m_tabs.end(); ++it)
    {
        delete it->tab;
    }

    m_tabs.clear();
    m_open = -1;

    computeSize();
}


/**
 * Dessine la barre d'onglets.
 *
 * \todo Pouvoir changer les paramètres d'affichage.
 * \todo Pouvoir changer la position de la barre d'onglets.
 ******************************/

void CLayoutTab::draw()
{
    int i = 0;
    int ofs_tab = getX();

    // Bordure
    Game::guiEngine->drawLine(TVector2F(getX(), getY() + m_height),
                              TVector2F(getX() + m_width, getY() + m_height),
                              CColor(28, 38, 60));

    Game::guiEngine->drawLine(TVector2F(getX() + 1, getY() + TabHeight),
                              TVector2F(getX() + 1, getY() + m_height),
                              CColor(28, 38, 60));

    Game::guiEngine->drawLine(TVector2F(getX() + m_width, getY() + TabHeight),
                              TVector2F(getX() + m_width, getY() + m_height) ,
                              CColor(28, 38, 60));

    // Intérieur
    Game::guiEngine->drawRectangle(CRectangle(getX() + 1, getY() + TabHeight, m_width - 2, m_height - TabHeight - 1),
                                   CColor(217, 222, 232));

    for (std::list<TabItem>::const_iterator it = m_tabs.begin(); it != m_tabs.end(); ++it, ++i)
    {
        TTextParams params;
        params.text = it->name;
        params.font = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
        params.size = 12;
#else
        params.size = 16;
#endif // T_USE_FREETYPE

        params.color = CColor::White;
        params.rec.setX(ofs_tab + TabMargin);
        params.rec.setY(getY() + 3);
        params.rec.setWidth(0);
        params.rec.setHeight(TabHeight);

        // Barre d'onglets
        TVector2UI txtdim = Game::fontManager->getTextSize(params);

        // Affichage de l'onglet ouvert
        if (i == m_open)
        {
            Game::guiEngine->drawRectangle(CRectangle(ofs_tab, getY(), txtdim.X + 2 * TabMargin, TabHeight),
                                           CColor(28, 38, 60));
            Game::guiEngine->drawRectangle(CRectangle(ofs_tab + 1, getY() + 1, txtdim.X + 2 * TabMargin - 2, TabHeight - 1),
                                           CColor(217, 222, 232));

            // Titre
            params.color = CColor::Black;
            Game::guiEngine->drawText(params);

            // Affichage du contenu de l'onglet
            if (it->tab)
            {
                it->tab->draw();
            }
        }
        else
        {
            Game::guiEngine->drawRectangle(CRectangle(ofs_tab, getY(), txtdim.X + 2 * TabMargin, TabHeight),
                                           CColor(28, 38, 60));
            Game::guiEngine->drawRectangle(CRectangle(ofs_tab + 1, getY() + 1, txtdim.X + 2 * TabMargin - 2, TabHeight - 2),
                                           CColor(69, 82, 104));

            // Titre
            Game::guiEngine->drawText(params);
        }

        ofs_tab += txtdim.X + 2 * TabMargin;

        Game::guiEngine->drawLine(TVector2F(ofs_tab, getY() + TabHeight),
                                  TVector2F(ofs_tab + TabSpacing, getY() + TabHeight),
                                  CColor(28, 38, 60));

        ofs_tab += TabSpacing;
    }

    Game::guiEngine->drawLine(TVector2F(ofs_tab, getY() + TabHeight),
                              TVector2F(getX() + m_width, getY() + TabHeight),
                              CColor(28, 38, 60));
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CLayoutTab::onEvent(const CMouseEvent& event)
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    // Barre d'onglets
    if (static_cast<int>(event.y) >= tmp_y + m_margin.getTop() &&
        static_cast<int>(event.y) <= tmp_y + TabHeight + m_margin.getTop())
    {
        int offset = 0;
        unsigned int i = 0;

        for (std::list<TabItem>::const_iterator it = m_tabs.begin(); it != m_tabs.end(); ++it, ++i)
        {
            TTextParams params;
            params.text = it->name;
            params.font = Game::fontManager->getFontId("Arial");

#ifdef T_USE_FREETYPE
        params.size = 12;
#else
        params.size = 16;
#endif // T_USE_FREETYPE

            // Barre d'onglets
            TVector2UI txtdim = Game::fontManager->getTextSize(params);

            // Clic sur l'onglet
            if (static_cast<int>(event.x) > tmp_x + offset &&
                static_cast<int>(event.x) < tmp_x + offset + static_cast<int>(txtdim.X) + 2 * TabMargin &&
                event.type == ButtonPressed && event.button == MouseButtonLeft)
            {
                m_open = i;
                break;
            }

            offset += txtdim.X + 2 * TabMargin;
            offset += TabSpacing;
        }
    }
    // Contenu de l'onglet
    else
    {
        if (m_open < 0)
            return;

        std::list<TabItem>::iterator it = m_tabs.begin();
        std::advance(it, m_open);

        if (it->tab)
        {
            if (static_cast<int>(event.y) >= it->tab->getY() &&
                static_cast<int>(event.y) <= it->tab->getY() + it->tab->getHeight() &&
                static_cast<int>(event.x) >= it->tab->getX() &&
                static_cast<int>(event.x) <= it->tab->getX() + it->tab->getWidth())
            {
                if (event.type == ButtonPressed || event.type == DblClick)
                {
                    if (event.button == MouseButtonLeft)
                    {
                        Game::guiEngine->setSelected(it->tab);
                    }

                    Game::guiEngine->setFocus(it->tab);
                }

                Game::guiEngine->setPointed(it->tab);

                it->tab->onEvent(event);
            }
        }
    }
}


/**
 * Calcul des dimensions du layout.
 *
 * \todo Pouvoir changer la position de la barre d'onglets.
 ******************************/

void CLayoutTab::computeSize()
{
    int min_w = 0;
    int min_h = 0;

    // On parcourt la liste des objets enfants
    for (std::list<TabItem>::const_iterator it = m_tabs.begin(); it != m_tabs.end(); ++it)
    {
        if (it->tab)
        {
            int tmp_w = it->tab->getMinWidth();
            int tmp_h = it->tab->getMinHeight();

            if (tmp_w > min_w)
                min_w = tmp_w;
            if (tmp_h > min_h)
                min_h = tmp_h;
        }
    }

    min_w += m_padding.getLeft() + m_padding.getRight()  + m_margin.getLeft() + m_margin.getRight();
    min_h += m_padding.getTop()  + m_padding.getBottom() + m_margin.getTop()  + m_margin.getBottom() + TabHeight;

    // On agrandit le layout en largeur
    if (min_w > m_width && m_minWidth < m_maxWidth)
    {
        setMinWidth(min_w);
    }

    // On agrandit le layout en hauteur
    if (min_h > m_height && m_minHeight < m_maxHeight)
    {
        setMinHeight(min_h);
    }

    // On modifie la taille des objets de chaque onglet
    for (std::list<TabItem>::const_iterator it = m_tabs.begin(); it != m_tabs.end(); ++it)
    {
        if (it->tab)
        {
            it->tab->setSize(m_width  - m_padding.getLeft() - m_padding.getRight()  - m_margin.getLeft() - m_margin.getRight() ,
                             m_height - m_padding.getTop()  - m_padding.getBottom() - m_margin.getTop()  - m_margin.getBottom() - TabHeight);

            switch (it->align)
            {
                case AlignTopLeft:
                    it->tab->setPosition(m_margin.getLeft() + m_padding.getLeft(),
                                         m_margin.getTop() + m_padding.getTop() + TabHeight);
                    break;

                case AlignTopCenter:
                    it->tab->setPosition((m_width - it->tab->getWidth()) / 2,
                                          m_margin.getTop() + m_padding.getTop() + TabHeight );
                    break;

                case AlignTopRight:
                    it->tab->setPosition(m_width - it->tab->getWidth() - m_margin.getRight() - m_padding.getRight(),
                                         m_margin.getTop() + m_padding.getTop() + TabHeight);
                    break;

                case AlignMiddleLeft:
                    it->tab->setPosition(m_margin.getLeft() + m_padding.getLeft(),
                                         (m_height - (m_margin.getTop() + TabHeight) - it->tab->getHeight()) / 2 + TabHeight);
                    break;

                default:
                case AlignMiddleCenter:
                    it->tab->setPosition((m_width - it->tab->getWidth() ) / 2,
                                         (m_height - (m_margin.getTop() + TabHeight) - it->tab->getHeight()) / 2 + TabHeight);
                    break;

                case AlignMiddleRight:
                    it->tab->setPosition(m_width - it->tab->getWidth() - m_margin.getRight() - m_padding.getRight(),
                                        (m_height - (m_margin.getTop() + TabHeight) - it->tab->getHeight()) / 2 + TabHeight);
                    break;

                case AlignBottomLeft:
                    it->tab->setPosition(m_margin.getLeft() + m_padding.getLeft(),
                                         m_height - it->tab->getHeight() - m_margin.getBottom() - m_padding.getBottom());
                    break;

                case AlignBottomCenter:
                    it->tab->setPosition((m_width - it->tab->getWidth() ) / 2,
                                          m_height - it->tab->getHeight() - m_margin.getBottom() - m_padding.getBottom());
                    break;

                case AlignBottomRight:
                    it->tab->setPosition(m_width - it->tab->getWidth() - m_margin.getRight() - m_padding.getRight(),
                                         m_height - it->tab->getHeight() - m_margin.getBottom() - m_padding.getBottom());
                    break;
            }
        }
    }
}

} // Namespace Ted

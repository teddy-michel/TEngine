/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CMenu.cpp
 * \date 06/05/2009 Création de la classe GuiMenu.
 * \date 03/07/2010 Les évènements sont gérés avec les outputs.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CMenu.
 * \date 04/04/2013 Nouveaux paramètres : itemHeight, itemMargin, padding, fontId, backgroundColor.
 * \date 06/04/2013 Nouveaux paramètres : fontSize, itemColorActive, itemColorInactive, fontColorActive, fontColorInactive.
 * \date 13/04/2013 Les éléments d'un menu ont tous la même largeur.
 * \date 13/06/2014 Nouveau paramètre : itemPadding.
 * \date 14/06/2014 Ajout d'un paramètre pour aligner le texte.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CMenu.hpp"
#include "Gui/CMenuItem.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/Events.hpp"


namespace Ted
{

const int MenuItemMinHeight     = 10; ///< Hauteur minimale d'un élément du menu.
const int MenuItemDefaultHeight = 30; ///< Hauteur par défaut d'un élément du menu.


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CMenu::CMenu(IWidget * parent) :
IWidget             (parent),
m_itemHeight        (MenuItemDefaultHeight),
m_fontId            (CFontManager::InvalidFont),
#ifdef T_USE_FREETYPE
m_fontSize          (18),
#else
m_fontSize          (20),
#endif // T_USE_FREETYPE
m_backgroundColor   (0, 0, 0, 0),
m_itemColorActive   (0, 0, 0, 0),
m_itemColorInactive (0, 0, 0, 0),
m_fontColorActive   (CColor::Yellow),
m_fontColorInactive (CColor::White),
m_textAlign         (AlignMiddleLeft),
m_currentItem       (-1)
{
    m_fontId = Game::fontManager->getFontId("Arial");
}


/**
 * Destructeur.
 ******************************/

CMenu::~CMenu()
{
    for (std::list<CMenuItem *>::iterator it = m_items.begin(); it != m_items.end(); /*++it*/)
    {
        CMenuItem * item = *it;
        it = m_items.erase(it);
        delete item;
    }
}


/**
 * Donne le nombre d'items dans le menu.
 *
 * \return Nombre d'items.
 ******************************/

int CMenu::getNumItems() const
{
    return m_items.size();
}


/**
 * Ajoute un item au menu.
 *
 * \param item Pointeur sur l'item à ajouter.
 ******************************/

void CMenu::addItem(CMenuItem * item)
{
    if (!item)
        return;

    item->setParent(this);

    m_currentItem = -1;
    addItemPrivate(item);
}


/**
 * Ajoute un item au menu avec une action associée.
 *
 * \param name Nom de l'item.
 * \param slot Slot à appeler lorsque l'on clique sur l'item.
 ******************************/

void CMenu::addItem(const CString& name, const sigc::slot<void>& slot)
{
    CMenuItem * item = new CMenuItem(name, this);
    item->onClick.connect(slot);

    m_currentItem = -1;
    addItemPrivate(item);
}


/**
 * Ajoute un séparateur au menu.
 ******************************/

void CMenu::addSeparator()
{
    CMenuItem * item = new CMenuItem(true, this);
    item->setMenu(this);
    item->setPosition(m_padding.getLeft(), m_padding.getTop() + getNumItems() * (m_itemMargin.getTop() + m_itemHeight + m_itemMargin.getBottom()));
    m_items.push_back(item);
    m_height += m_itemMargin.getTop() + m_itemHeight + m_itemMargin.getBottom();

    m_currentItem = -1;

    // Dimensions du séparateur
    item->setSize(m_width - m_padding.getLeft() - m_padding.getRight(), m_itemMargin.getTop() + m_itemHeight + m_itemMargin.getBottom());
}


/**
 * Enlève un item du menu (mais ne supprime pas l'item).
 *
 * \param item Pointeur sur l'item.
 ******************************/

void CMenu::removeItem(CMenuItem * item)
{
    if (item == nullptr)
        return;

    m_items.remove(item);
    item->setMenu(nullptr);

    if (item->getParent() == this)
        item->setParent(nullptr);

    m_currentItem = -1;
    updateItemsSize();
}


/**
 * Supprime un item du menu.
 *
 * \param item Numéro de l'élément à supprimer.
 ******************************/

void CMenu::removeItem(int item)
{
    if (item < 0 || item >= getNumItems())
        return;

    std::list<CMenuItem *>::iterator it = m_items.begin();
    std::advance(it, item);

    if (it == m_items.end())
        return;

    delete (*it);
    m_items.erase(it);

    m_currentItem = -1;
    updateItemsSize();
}


/**
 * Supprime tous les items du menu.
 ******************************/

void CMenu::clearItems()
{
    for (std::list<CMenuItem *>::iterator it = m_items.begin(); it != m_items.end(); /*++it*/)
    {
        CMenuItem * item = *it;
        it = m_items.erase(it);
        delete item;
    }

    setWidth(m_padding.getLeft() + m_padding.getRight());
    setHeight(m_padding.getTop() + m_padding.getBottom());
    m_currentItem = -1;
}


void CMenu::setWidth(int width)
{
    int oldWidth = m_width;
    IWidget::setWidth(width);

    if (oldWidth != m_width)
    {
        int itemWidth = m_width - (m_padding.getLeft() + m_padding.getRight()) - (m_itemMargin.getLeft() + m_itemMargin.getRight());

        // Mise à jour de la largeur des éléments
        for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            (*it)->setWidth(itemWidth);
        }
    }
}


/**
 * Modifie la hauteur de chaque élément du menu.
 *
 * \param height Hauteur des éléments du menu en pixels.
 ******************************/

void CMenu::setItemHeight(int height)
{
    if (height < MenuItemMinHeight)
        height = MenuItemMinHeight;

    if (m_itemHeight != height)
    {
        m_itemHeight = height;
        updateItemsSize();
    }
}


/**
 * Modifie la valeur des marges internes du menu.
 *
 * \param margin Marges internes du menu.
 ******************************/

void CMenu::setPadding(const CMargins& margin)
{
    if (m_padding != margin)
    {
        m_padding = margin;
        updateItemsSize();
    }
}


/**
 * Modifie la valeur des marges externes des éléments du menu.
 *
 * \param margin Marges externes des éléments du menu.
 ******************************/

void CMenu::setItemMargin(const CMargins& margin)
{
    if (m_itemMargin != margin)
    {
        m_itemMargin = margin;
        updateItemsSize();
    }
}


/**
 * Modifie la valeur des marges internes des éléments du menu.
 *
 * \param margin Marges internes des éléments du menu.
 ******************************/

void CMenu::setItemPadding(const CMargins& margin)
{
    if (m_itemPadding != margin)
    {
        m_itemPadding = margin;
/*
        // Mise à jour de chaque élément
        for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it, ++itemId)
        {
            (*it)->setPadding(m_itemPadding);
        }
*/
        updateItemsSize();
    }
}


/**
 * Modifie la police à utiliser pour le texte des éléments.
 *
 * \param font Identifiant de la police.
 ******************************/

void CMenu::setFontId(TFontId font)
{
    if (m_fontId != font)
    {
        m_fontId = font;
        updateItemsSize();
    }
}


/**
 * Modifie la taille du texte des éléments du menu.
 *
 * \param fontSize Taille du texte.
 ******************************/

void CMenu::setFontSize(int fontSize)
{
    if (fontSize < 8 || fontSize > 72)
        return;

    if (fontSize != m_fontSize)
    {
        m_fontSize = fontSize;
        updateItemsSize();
    }
}


/**
 * Modifie la couleur de fond du menu.
 *
 * \param color Couleur de fond du menu.
 ******************************/

void CMenu::setBackgroundColor(const CColor& color)
{
    m_backgroundColor = color;
}


/**
 * Modifie la couleur de fond des éléments actifs du menu.
 *
 * \param color Couleur de fond des éléments.
 ******************************/

void CMenu::setItemColorActive(const CColor& color)
{
    m_itemColorActive = color;
}


/**
 * Modifie la couleur de fond des éléments inactifs du menu.
 *
 * \param color Couleur de fond des éléments.
 ******************************/

void CMenu::setItemColorInactive(const CColor& color)
{
    m_itemColorInactive = color;
}


/**
 * Modifie la couleur du texte des éléments actifs du menu.
 *
 * \param color Couleur du texte des éléments.
 ******************************/

void CMenu::setFontColorActive(const CColor& color)
{
    m_fontColorActive = color;
}


/**
 * Modifie la couleur du texte des éléments inactifs du menu.
 *
 * \param color Couleur du texte des éléments.
 ******************************/

void CMenu::setFontColorInactive(const CColor& color)
{
    m_fontColorInactive = color;
}


void CMenu::setTextAlignment(TAlignment align)
{
    m_textAlign = align;
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CMenu::onEvent(const CMouseEvent& event)
{
    if (!isHierarchyEnable())
        return;

    // On cherche l'item pointé par le curseur
    for (std::list<CMenuItem *>::iterator it = m_items.begin(); it != m_items.end(); ++it)
    {
        T_ASSERT(*it != nullptr);

        int tmp_x = (*it)->getX();
        int tmp_y = (*it)->getY();

        if (static_cast<int>(event.x) >= tmp_x &&
            static_cast<int>(event.x) <= tmp_x + (*it)->getWidth() &&
            static_cast<int>(event.y) >= tmp_y &&
            static_cast<int>(event.y) <= tmp_y + (*it)->getHeight())
        {
            Game::guiEngine->setPointed(*it);
            (*it)->onEvent(event); // On envoie l'évènement à l'item
            m_currentItem = std::distance(m_items.begin(), it);
            return;
        }
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \todo Implémentation.
 *
 * \param event Évènement.
 ******************************/

void CMenu::onEvent(const CKeyboardEvent& event)
{
    // Flèche du haut
    if (event.code == Key_Up)
    {
        if (m_currentItem > 0)
            --m_currentItem;
    }
    // Flèche du bas
    else if(event.code == Key_Down)
    {
        if (m_currentItem + 1 < getNumItems())
            ++m_currentItem;
    }
    // Touche Entrée
    else if(event.code == Key_Enter || event.code == Key_NumEnter)
    {
        if (m_currentItem >= 0 && m_currentItem < getNumItems())
            ++m_currentItem;
        //m_currentItem
        //->onClick();
    }
}


/**
 * Dessine le menu.
 ******************************/

void CMenu::draw()
{
    Game::guiEngine->drawRectangle(CRectangle(getX(), getY(), getWidth(), getHeight()), m_backgroundColor);

    // Dessine chaque élément du menu
    for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it)
    {
        T_ASSERT(*it != nullptr);
        (*it)->draw();
    }
}


void CMenu::addItemPrivate(CMenuItem * item)
{
    T_ASSERT(item != nullptr);

    item->setMenu(this);
    m_items.push_back(item);

    int itemTotalHeight = m_itemMargin.getTop() + m_itemPadding.getTop() + m_itemHeight + m_itemPadding.getBottom() + m_itemMargin.getBottom();
    int height = m_height + itemTotalHeight;

    TTextParams params;
    params.text = item->getName();
    params.font = m_fontId;
    params.size = m_fontSize;

    // Longueur du texte
    int itemWidth = Game::fontManager->getTextSize(params).X;
    itemWidth += m_itemPadding.getLeft() + m_itemPadding.getRight();

    int currentItemWidth = m_width - (m_padding.getLeft() + m_padding.getRight()) - (m_itemMargin.getLeft() + m_itemMargin.getRight());

    if (currentItemWidth > itemWidth)
    {
        itemWidth = currentItemWidth;
    }

    int itemId = 0;

    // Mise à jour de la hauteur et de la position des éléments
    for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it, ++itemId)
    {
        (*it)->setSize(itemWidth, m_itemPadding.getTop() + m_itemHeight + m_itemPadding.getBottom());
        (*it)->setPosition(m_padding.getLeft() + m_itemMargin.getLeft(), m_padding.getTop() + itemId * itemTotalHeight + m_itemMargin.getTop());
    }

    int width = itemWidth;
    width += m_padding.getLeft() + m_padding.getRight();
    width += m_itemMargin.getLeft() + m_itemMargin.getRight();

    setMinSize(width, height);
}


/**
 * Met à jour les dimensions et la position des éléments du menu.
 ******************************/

void CMenu::updateItemsSize()
{
    int itemTotalHeight = m_itemMargin.getTop() + m_itemPadding.getTop() + m_itemHeight + m_itemPadding.getBottom() + m_itemMargin.getBottom();

    // Mise à jour des dimensions du menu
    int height = m_padding.getTop() + m_padding.getBottom() + getNumItems() * itemTotalHeight;
    int itemWidth = 0;

    int itemId = 0;

    // Calcule de la largeur des éléments
    for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it, ++itemId)
    {
        if (!(*it)->isSeparator())
        {
            TTextParams params;
            params.text = (*it)->getName();
            params.font = m_fontId;
            params.size = m_fontSize;

            int itemWidthTmp = Game::fontManager->getTextSize(params).X;

            if (itemWidthTmp > itemWidth)
            {
                itemWidth = itemWidthTmp;
            }
        }
    }

    itemWidth += m_itemPadding.getLeft() + m_itemPadding.getRight();
    itemId = 0;

    // Mise à jour de la hauteur et de la position des éléments
    for (std::list<CMenuItem *>::const_iterator it = m_items.begin(); it != m_items.end(); ++it, ++itemId)
    {
        (*it)->setSize(itemWidth, m_itemPadding.getTop() + m_itemHeight + m_itemPadding.getBottom());
        (*it)->setPosition(m_padding.getLeft() + m_itemMargin.getLeft(), m_padding.getTop() + itemId * itemTotalHeight + m_itemMargin.getTop());
    }

    int width = itemWidth;
    width += m_padding.getLeft() + m_padding.getRight();
    width += m_itemMargin.getLeft() + m_itemMargin.getRight();

    setMinSize(width, height);
}

} // Namespace Ted

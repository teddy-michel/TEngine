/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CTextEdit.cpp
 * \date 26/04/2009 Création de la classe GuiTextEdit.
 * \date 16/07/2010 Utilisation possible des signaux, ajout de l'input Clear.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 20/07/2010 Utilisation des codes clavier internes.
 * \date 23/07/2010 L'affichage du texte est limité dans un rectangle.
 * \date 04/11/2010 Héritage de la classe GuiBaseTextEdit.
 * \date 05/11/2010 Modification de la gestion du texte et ajout de l'ascenseur.
 * \date 06/11/2010 Affichage de l'ascenseur.
 * \date 08/01/2011 Ajout de la méthode setTop.
 * \date 17/01/2011 Héritage de la classe GuiFrame.
 * \date 19/01/2011 La position du curseur lors d'un clic est calculée correctement.
 * \date 06/03/2011 Création de la méthode getCursorType.
 * \date 26/03/2011 Correction d'un problème d'affichage du texte en dehors du widget.
 * \date 29/05/2011 La classe est renommée en CTextEdit.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CTextEdit.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CScrollBar.hpp"
#include "Core/Time.hpp"
#include "Core/Events.hpp"
#include "Core/Maths/CRectangle.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

const int TextEditMinWidth = 150; ///< Largeur minimale en pixels.
const int TextEditMinHeight = 50; ///< Hauteur minimale en pixels.


/**
 * Constructeur par défaut.
 *
 * \param text   Texte du champ.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CTextEdit::CTextEdit(const CString& text, IWidget * parent) :
IScrollArea (parent),
ITextEdit   (text)
{
    setMinSize(TextEditMinWidth, TextEditMinHeight);

    m_vScroll->setHeight(m_height);
    m_vScroll->setStep(m_height);
    m_vScroll->setSingleStep(m_params.size * 1.4f);

    m_hScroll->setWidth(m_width);
    m_hScroll->setStep(m_width);
    m_hScroll->setSingleStep(5);

    setFrameSize(m_width, m_height);
}

/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CTextEdit::CTextEdit(IWidget * parent) :
IScrollArea (parent),
ITextEdit   ()
{
    setMinSize(TextEditMinWidth, TextEditMinHeight);

    m_vScroll->setHeight(m_height);
    m_vScroll->setStep(m_height);
    m_vScroll->setSingleStep(5);

    m_hScroll->setWidth(m_width);
    m_hScroll->setStep(m_width);
    m_hScroll->setSingleStep(5);

    setFrameSize(m_width, m_height);
}


/**
 * Modifie le texte.
 *
 * \param text Texte.
 *
 * \sa GuiBaseTextEdit::getText
 ******************************/

void CTextEdit::setText(const CString& text)
{
    T_ASSERT(m_vScroll != nullptr);

    ITextEdit::setText(text);

    const TVector2UI text_size = Game::fontManager->getTextSize(m_params);

    // Modification de l'ascenseur
    m_hScroll->setTotal(text_size.X);
    m_vScroll->setTotal(text_size.Y);

    const int scrollmax = m_vScroll->getMax();

    // Si on est à la fin du texte, on descent l'ascenseur
    if (m_vScroll->getValue() >= scrollmax)
    {
        m_vScroll->setValue(scrollmax);
    }

    computeScrollSize();
}


/**
 * Ajoute une ligne de texte.
 *
 * \todo Gérer le curseur et la sélection du texte.
 *
 * \param text Texte à ajouter.
 ******************************/

void CTextEdit::addText(const CString& text)
{
    setText(m_params.text + text);
}


/**
 * Modifie la largeur du champ.
 *
 * \param width Largeur de l'objet en pixels.
 ******************************/

void CTextEdit::setWidth(int width)
{
    if (width < 0)
        width = 0;

    if (width < TextEditMinWidth)
    {
        width = TextEditMinWidth;
    }

    IScrollArea::setWidth(width);
}


/**
 * Modifie la hauteur du champ.
 *
 * \param height Hauteur de l'objet en pixels.
 ******************************/

void CTextEdit::setHeight(int height)
{
    if (height < 0)
        height = 0;

    if (height < TextEditMinHeight)
    {
        height = TextEditMinHeight;
    }

    IScrollArea::setHeight(height);
}


/**
 * Donne la hauteur totale du texte en pixels.
 *
 * \return Hauteur du texte en pixels.
 ******************************/

unsigned int CTextEdit::getTextHeight() const
{
    return Game::fontManager->getTextSize(m_params).Y;
}


/**
 * Dessine le champ texte.
 *
 * \todo Gérer le curseur.
 * \todo Gérer la sélection.
 * \todo Transférer l'affichage des barres de navigation à GuiFrame.
 ******************************/

void CTextEdit::draw()
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    const unsigned int time = getElapsedTime();
    const unsigned int frameTime = time - m_time;
    m_time = time;

    m_timeBlink += frameTime;


    const int tmp_x = getX();
    const int tmp_y = getY();

    const TVector2I size = getWidgetSize();


    // Dimensions du texte
    m_params.rec.setX(tmp_x + 5);
    m_params.rec.setY(tmp_y + 5);
    m_params.rec.setWidth(size.X - 10);
    m_params.rec.setHeight(size.Y - 10);

    m_params.top  = m_vScroll->getValue();
    m_params.left = m_hScroll->getValue();

    // Fond du champ
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, size.X, size.Y), CColor::White);
    Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, size.X, size.Y), CColor(28, 38, 60));


    // Affichage des ascenseurs
    if (isShowVerticalScrollBar())
    {
        m_vScroll->draw();
    }

    if (isShowHorizontalScrollBar())
    {
        m_hScroll->draw();
    }


    // Affichage du texte
    CString old_txt = m_params.text;

    // Sélection du texte
    if (m_select < 0)
    {
        m_params.text = m_params.text.subString(0, m_cursor + m_select) +
                        static_cast<char>(12) +
                        m_params.text.subString(m_cursor + m_select, -m_select) +
                        static_cast<char>(12) +
                        m_params.text.subString(m_cursor);
    }
    else if (m_select > 0)
    {
        m_params.text = m_params.text.subString(0, m_cursor) +
                        static_cast<char>(12) +
                        m_params.text.subString(m_cursor, m_select) +
                        static_cast<char>(12) +
                        m_params.text.subString(m_cursor + m_select);
    }

    Game::guiEngine->drawText(m_params);
    m_params.text = old_txt;


    // Affichage de la barre verticale clignotante
    if (Game::guiEngine->hasFocus(this))
    {
        //unsigned int pos_cursor = 0;

        // Curseur
        if ((m_timeBlink / timeBlink) % 2)
        {
            //Game::guiEngine->drawLine(tmp_x + pos_cursor, tmp_y + 2, tmp_x + pos_cursor, tmp_y + m_height - 2, CColor::Black);
        }
    }
}


/**
 * Indique le curseur à utiliser en fonction de la position du curseur de la souris.
 *
 * \param x Coordonnée horizontale du curseur.
 * \param y Coordonnée verticale du curseur.
 * \return Curseur à utiliser (flèche ou barre de texte).
 ******************************/

TCursor CTextEdit::getCursorType(int x, int y) const
{
    // Affichage des ascenseurs
    if (isShowVerticalScrollBar() && x > m_vScroll->getX())
    {
        return CursorArrow;
    }

    if (isShowHorizontalScrollBar() && y > m_hScroll->getY())
    {
        return CursorArrow;
    }

    return CursorText;
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CTextEdit::onEvent(const CTextEvent& event)
{
    ITextEdit::onTextEvent(event);
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CTextEdit::onEvent(const CKeyboardEvent& event)
{
    ITextEdit::onKeyboardEvent(event);
}


/**
 * Gestion des évènements de la souris.
 *
 * \todo Utiliser la méthode getTextLinesSize.
 * \todo Gérer la sélection de texte.
 * \todo Reléguer la gestion des ascenseurs à GuiFrame.
 *
 * \param event Évènement.
 ******************************/

void CTextEdit::onEvent(const CMouseEvent& event)
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    // Ascenseurs
    if (isShowVerticalScrollBar())
    {
        const int tmp_x = m_vScroll->getX();
        const int tmp_y = m_vScroll->getY();

        if (static_cast<int>(event.x) >= tmp_x &&
            static_cast<int>(event.x) <= tmp_x + m_vScroll->getWidth() &&
            static_cast<int>(event.y) >= tmp_y &&
            static_cast<int>(event.y) <= tmp_y + m_vScroll->getHeight())
        {
            m_vScroll->onEvent(event);
            return;
        }
    }

    if (isShowHorizontalScrollBar())
    {
        const int tmp_x = m_hScroll->getX();
        const int tmp_y = m_hScroll->getY();

        if (static_cast<int>(event.x) >= tmp_x &&
            static_cast<int>(event.x) <= tmp_x + m_hScroll->getWidth() &&
            static_cast<int>(event.y) >= tmp_y &&
            static_cast<int>(event.y) <= tmp_y + m_hScroll->getHeight())
        {
            m_hScroll->onEvent(event);
            return;
        }
    }


    // Double-clic : on sélectionne tout le texte
    if (event.type == DblClick && event.button == MouseButtonLeft)
    {
        selectAll();
    }


    // Clic gauche
    if (event.button == MouseButtonLeft && event.type == ButtonPressed)
    {
        onClick();

        // --------------------------------------------------
        // NOUVELLE VERSION AMELIORÉE
        // --------------------------------------------------

        std::vector<TTextLine> lines;
        Game::fontManager->getTextLinesSize(m_params, lines);

        if (lines.size() == 0)
        {
            m_cursor = 0;
        }
        else
        {
            // Numéro de la ligne sur laquelle on a cliqué
            unsigned int line_number = (event.y - getY()) / m_params.size;

            // On a cliqué après la dernière ligne
            if (line_number >= lines.size())
            {
                line_number = lines.size() - 1;
                m_cursor = m_params.text.getSize();
            }
            else
            {
                m_cursor = lines[line_number].offset;
                float offset = 0.0f;
                float clic = static_cast<float>(event.x) - static_cast<float>(getX());

                for (std::vector<float>::iterator it = lines[line_number].len.begin(); it != lines[line_number].len.end(); ++it)
                {
                    offset += *it;

                    if (offset >= clic)
                    {
                        if (offset - *it * 0.5f > clic)
                        {
                            ++m_cursor;
                        }

                        break;
                    }
                    else
                    {
                        ++m_cursor;
                    }
                }
            }
        }

        // --------------------------------------------------
        // SUPPRIMER CE BLOC ET REMPLACER PAR CELUI DU DESSUS
        // --------------------------------------------------
/*
        unsigned int i = 0;
        TVector2UI pos, pos_old;

        const unsigned int text_length = m_params.text.size();
        const TVector2UI pos_max(event.x - getX(), event.y - getY());

        while (i < text_length && pos < pos_max)
        {
            pos_old = pos;

            TTextParams params;
            params.text = m_params.text.substr(0, ++i);
            params.font = "Arial";
            params.size = m_params.size;

            pos = CFontManager::Instance().getTextSize(params);
        }

        m_cursor = (pos_max.X - pos_old.X < pos.X - pos_max.X ? i - 1 : i);
*/
        // --------------------------------------------------
        // FIN DU BLOC
        // --------------------------------------------------

        m_select = 0; // On désélectionne le texte
        m_timeBlink = timeBlink;
    }
}

} // Namespace Ted

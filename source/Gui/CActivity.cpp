/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CActivity.cpp
 * \date 02/06/2014 Création de la classe IActivity.
 * \date 04/06/2014 Déplacement du module Core vers le module Gui.
 * \date 07/06/2014 La classe CActivity s'occupe de la gestion des objets de l'interface.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CActivity.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CWindow.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Constructeur de l'activité.
 *
 * \param activityName Nom de l'activité à créer.
 ******************************/

CActivity::CActivity(const CString& activityName) :
m_activityName (activityName),
m_menu         (nullptr),
m_focus        (nullptr),
m_pointed      (nullptr),
m_selected     (nullptr),
m_foreground   (nullptr)
{
    Game::guiEngine->addActivity(this);
}


/**
 * Détruit l'activité.
 ******************************/

CActivity::~CActivity()
{
    Game::guiEngine->removeActivity(this);
}


/**
 * Indique si un objet a le focus.
 *
 * \param  widget Objet à tester.
 * \return Booléen valant true si l'objet a le focus.
 *
 * \sa IActivity::getFocus
 * \sa IActivity::setFocus
 ******************************/

bool CActivity::hasFocus(const IWidget * widget) const
{
    return (m_focus == widget);
}


/**
 * Donne l'objet ayant le focus.
 *
 * \return Pointeur sur l'objet ayant le focus.
 *
 * \sa IActivity::hasFocus
 * \sa IActivity::setFocus
 ******************************/

IWidget * CActivity::getFocus() const
{
    return m_focus;
}


/**
 * Indique si un objet est pointé par le curseur.
 *
 * \param widget Objet à tester.
 * \return Booléen valant true si l'objet est pointé par le curseur de la souris.
 *
 * \sa IActivity::getPointed
 * \sa IActivity::setPointed
 ******************************/

bool CActivity::isPointed(const IWidget * widget) const
{
    return (m_pointed == widget);
}


/**
 * Donne l'objet pointé par le curseur de la souris.
 *
 * \return Pointeur sur l'objet pointé par le curseur de la souris.
 *
 * \sa IActivity::isPointed
 * \sa IActivity::setPointed
 ******************************/

IWidget * CActivity::getPointed() const
{
    return m_pointed;
}


/**
 * Indique si un objet est sélectionné par la souris.
 *
 * \param widget Objet à tester.
 * \return Booléen valant true si l'objet est sélectionné par la souris.
 *
 * \sa IActivity::getSelected
 * \sa IActivity::setSelected
 ******************************/

bool CActivity::isSelected(const IWidget * widget) const
{
    return (m_selected == widget);
}


/**
 * Donne l'objet sélectionné par la souris.
 *
 * \return Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa IActivity::isSelected
 * \sa IActivity::setSelected
 ******************************/

IWidget * CActivity::getSelected() const
{
    return m_selected;
}


/**
 * Indique si un objet est au premier plan.
 *
 * \param widget Objet à tester.
 * \return Booléen.
 *
 * \sa IActivity::getForeground
 * \sa IActivity::setForeground
 ******************************/

bool CActivity::isForeground(const IWidget * widget) const
{
    return (m_foreground == widget);
}


/**
 * Donne l'objet au premier plan.
 *
 * \return Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa IActivity::isForeground
 * \sa IActivity::setForeground
 ******************************/

IWidget * CActivity::getForeground() const
{
    return m_foreground;
}


/**
 * Change l'objet ayant le focus.
 *
 * \param widget Pointeur sur l'objet qui a le focus.
 *
 * \sa IActivity::isFocus
 * \sa IActivity::getFocus
 ******************************/

void CActivity::setFocus(IWidget * widget)
{
    m_focus = widget;
}


/**
 * Change l'objet pointé par le curseur de la souris.
 *
 * \param widget Pointeur sur l'objet pointé par le curseur de la souris.
 *
 * \sa IActivity::isPointed
 * \sa IActivity::getPointed
 ******************************/

void CActivity::setPointed(IWidget * widget)
{
    m_pointed = widget;
}


/**
 * Change l'objet sélectionné par la souris.
 *
 * \param widget Pointeur sur l'objet sélectionné par la souris.
 *
 * \sa IActivity::isSelected
 * \sa IActivity::getSelected
 ******************************/

void CActivity::setSelected(IWidget * widget)
{
    m_selected = widget;
}


/**
 * Change l'objet au premier plan.
 *
 * \param widget Pointeur sur l'objet à mettre au premier plan.
 *
 * \sa IActivity::isForeground
 * \sa IActivity::getForeground
 ******************************/

void CActivity::setForeground(IWidget * widget)
{
    m_foreground = widget;
}


/**
 * Donne le nom de l'activité.
 *
 * \return Nom de l'activité.
 ******************************/

CString CActivity::getActivityName() const
{
    return m_activityName;
}


void CActivity::addWindow(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    if (std::find(m_windows.begin(), m_windows.end(), window) == m_windows.end())
    {
        m_windows.push_front(window);
        window->setFocus(true);
        m_focus = window;
    }
}


void CActivity::removeWindow(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    m_windows.remove(window);
}


std::list<CWindow *> CActivity::getWindows() const
{
    return m_windows;
}


/**
 * Méthode appelée par une fenêtre lors de son ouverture, place la fenêtre au
 * début de la liste.
 *
 * \param window Pointeur sur la fenêtre.
 *
 * \sa IActivity::onWindowClose
 ******************************/

void CActivity::onWindowOpen(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    // La fenêtre n'est pas au début de la liste
    if (window != m_windows.front())
    {
        bool windowFound = false;

        // On cherche dans la liste des fenêtres
        for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
        {
            if (*it == window)
            {
                // On enlève la fenêtre de la liste
                it = m_windows.erase(it);
                windowFound = true;
                break;
            }
        }

        if (!windowFound)
        {
            return;
        }

        // On enlève le focus de la première fenêtre
        m_windows.front()->setFocus(false);

        // On place la fenêtre au début de la liste
        m_windows.push_front(window);
    }

    // On lui donne le focus
    window->setFocus();
    setFocus(window);
}


/**
 * Méthode appelée par une fenêtre lors de sa fermeture, place la fenêtre à la
 * fin de la liste.
 *
 * \param window Pointeur sur la fenêtre.
 *
 * \sa IActivity::onWindowOpen
 ******************************/

void CActivity::onWindowClose(CWindow * window)
{
    if (window == nullptr)
    {
        return;
    }

    // La fenêtre n'est pas à la fin de la liste
    if (window != m_windows.back())
    {
        bool windowFound = false;

        // On cherche dans la liste des fenêtres
        for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
        {
            if (*it == window)
            {
                // On enlève la fenêtre de la liste
                it = m_windows.erase(it);
                windowFound = true;
                break;
            }
        }

        if (!windowFound)
        {
            return;
        }

        // On donne le focus à première fenêtre
        m_windows.front()->setFocus();
        setFocus(m_windows.front());

        // On place la fenêtre à la fin de la liste
        m_windows.push_back(window);
    }

    // On lui enlève le focus
    window->setFocus(false);
}


void CActivity::setMenu(CMenu * menu)
{
    m_menu = menu;
}


CMenu * CActivity::getMenu() const
{
    return m_menu;
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CActivity::onEvent(const CMouseEvent& event)
{
    // Déplacement du curseur
    if (event.type == MoveMouse)
    {
        m_pointed = nullptr;

        // On commence par tester l'objet au premier-plan
        if (m_foreground)
        {
            int tmp_x = m_foreground->getX();
            int tmp_y = m_foreground->getY();

            if (static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + m_foreground->getWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + m_foreground->getHeight()))
            {
                m_pointed = m_foreground;

                // On transmet l'évènement à l'objet
                m_foreground->onEvent(event);
                return;
            }
        }

        // On parcourt la liste des fenêtres pour trouver celle sous le curseur
        for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
        {
            int tmp_x = (*it)->getRealX();
            int tmp_y = (*it)->getRealY();

            // Si la fenêtre est ouverte et que le curseur est dans la zone de la fenêtre
            if ((*it)->isOpen() &&
                static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + (*it)->getRealWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + (*it)->getRealHeight()))
            {
                m_pointed = (*it);

                // On transmet l'évènement à la fenêtre
                (*it)->onEvent(event);
                return;
            }
        }

        if (m_menu)
        {
            int tmp_x = m_menu->getX();
            int tmp_y = m_menu->getY();

            // On cherche dans le menu
            if (static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + m_menu->getWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + m_menu->getHeight()))
            {
                m_pointed = m_menu;
                m_menu->onEvent(event);
            }
        }
    }
    // Bouton de la souris
    else
    {
        // On commence par tester l'objet au premier-plan
        if (m_foreground)
        {
            int tmp_x = m_foreground->getX();
            int tmp_y = m_foreground->getY();

            if (static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + m_foreground->getWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + m_foreground->getHeight()))
            {
                // On transmet l'évènement à l'objet
                m_foreground->onEvent(event);
                return;
            }
            // L'objet au premier plan perd le focus
            else
            {
                m_foreground = nullptr;
            }
        }

        // On parcourt la liste des fenêtres
        for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
        {
            int tmp_x = (*it)->getRealX();
            int tmp_y = (*it)->getRealY();

            // Si la fenêtre est ouverte et que le curseur est dans la zone de la fenêtre
            if ((*it)->isOpen() &&
                static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + (*it)->getRealWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + (*it)->getRealHeight()))
            {
                // La fenêtre n'est pas au sommet de la liste
                if (m_windows.front() != *it)
                {
                    // On lui donne le focus
                    m_focus = *it;

                    if (event.button == MouseButtonLeft && event.type == ButtonPressed)
                    {
                        m_selected = *it;
                    }

                    // On place la fenêtre en haut de la pile
                    onWindowOpen(*it);
                }

                // On envoie l'évènement à la fenêtre
                (*it)->onEvent(event);

                if (event.button == MouseButtonLeft && event.type == ButtonReleased)
                {
                    m_selected = nullptr;
                }

                return;
            }
        }

        // Aucune fenêtre n'a le focus, on cherche dans le menu
        if (m_menu)
        {
            int tmp_x = m_menu->getX();
            int tmp_y = m_menu->getY();

            if (static_cast<int>(event.x) >= tmp_x &&
                static_cast<int>(event.x) <= (tmp_x + m_menu->getWidth()) &&
                static_cast<int>(event.y) >= tmp_y &&
                static_cast<int>(event.y) <= (tmp_y + m_menu->getHeight()))
            {
                m_menu->onEvent(event);
            }
        }

        if (event.button == MouseButtonLeft && event.type == ButtonReleased)
        {
            m_selected = nullptr;
        }
    }
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CActivity::onEvent(const CTextEvent& event)
{
    // On transfère l'évènement à l'objet qui a le focus
    if (m_focus)
    {
        m_focus->onEvent(event);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CActivity::onEvent(const CKeyboardEvent& event)
{
    // On transfère l'évènement à l'objet qui a le focus
    if (m_focus)
    {
        m_focus->onEvent(event);
    }
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void CActivity::onEvent(const CResizeEvent& event)
{
    if (m_menu)
    {
        m_menu->onEvent(event);
    }

    for (std::list<CWindow *>::iterator it = m_windows.begin(); it != m_windows.end(); ++it)
    {
        (*it)->onEvent(event);
    }
}


/**
 * Met à jour l'activité.
 ******************************/

void CActivity::updateActivity()
{
    const unsigned int win_w = CApplication::getApp()->getWidth();
    //const unsigned int win_h = CApplication::getApp()->getHeight();
/*
    // Fond de l'écran
    if (m_backgroundImage == 0)
    {
        drawRectangle(CRectangle(0, 0, win_w, win_h), m_backgroundColor);
    }
    else
    {
        drawImage(CRectangle(0, 0, win_w, win_h), m_backgroundImage);
    }
*/
    TTextParams params;
    params.text  = CApplication::getApp()->getWindowTitle();
    params.font  = Game::fontManager->getFontId("Verdana");
    params.size  = 40;
    params.color = CColor::White;

    const int txtpos_x = (win_w -  Game::fontManager->getTextSize(params).X) / 2;
    params.rec = CRectangle(txtpos_x, 60);

    // Titre
    Game::guiEngine->drawText(params);

    // Affichage du menu
    if (m_menu)
    {
        m_menu->draw();
    }

    // Affichage des fenêtres
    for (std::list<CWindow *>::reverse_iterator it = m_windows.rbegin(); it != m_windows.rend(); ++it)
    {
        if ((*it)->isOpen())
        {
            (*it)->draw();
        }
    }

    // Affichage de l'élément au premier plan
    if (m_foreground)
    {
        m_foreground->draw();
    }

    update();
}


void CActivity::start()
{
    CApplication::getApp()->showMouseCursor(true);
}


void CActivity::update()
{

}


void CActivity::stop()
{

}

} // Namespace Ted

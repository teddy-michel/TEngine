/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CWindow_Save.cpp
 * \date 20/06/2010 Création de la classe GuiWindow_Save.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 29/05/2011 La classe est renommée en CWindow_Save.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/CWindow_Save.hpp"
#include "Gui/CPushButton.hpp"
#include "Gui/CLayoutVertical.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CWindow_Save::CWindow_Save(IWidget * parent) :
CWindow ("Sauvegarde", WindowCenter, parent)
{
    setMinSize(400, 240);

    // Afficher la liste des sauvegardes existantes
    // Afficher un bouton pour supprimer une sauvegarde
    // Afficher un bouton/lien pour créer une nouvelle sauvegarde

    CPushButton * button_close = new CPushButton("Fermer");

    button_close->onClicked.connect(sigc::mem_fun(this, &CWindow::close));

    // Layout
    CLayoutVertical * layout = new CLayoutVertical(this);
    layout->addChild(button_close);

    setLayout(layout);
}

} // Namespace Ted

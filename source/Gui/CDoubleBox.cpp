/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CDoubleBox.cpp
 * \date 28/05/2009 Création de la classe GuiDoubleBox.
 * \date 08/07/2010 Le texte du champ est modifié.
 * \date 09/07/2010 Modification des valeurs maximales et minimales par défaut.
 * \date 17/07/2010 Utilisation possible des signaux.
 * \date 14/08/2010 Précision du nombre de décimales à afficher.
 * \date 29/05/2011 La classe est renommée en CDoubleBox.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>
#include <cmath>

#include "Gui/CDoubleBox.hpp"
#include "Gui/CLineEdit.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param value  Valeur par défaut.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDoubleBox::CDoubleBox(double value, IWidget * parent) :
ISpinBox   (parent),
m_value    (value),
m_min      (0.0f),
m_max      (99.99f),
m_step     (0.1f),
m_decimals (2)
{
    m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);
}


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CDoubleBox::CDoubleBox(IWidget * parent) :
ISpinBox   (parent),
m_value    (0.0f),
m_min      (0.0f),
m_max      (99.99f),
m_step     (0.1f),
m_decimals (2)
{
    m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);
}


/**
 * Accesseur pour value.
 *
 * \return Valeur de la doublebox.
 *
 * \sa CDoubleBox::setValue
 ******************************/

double CDoubleBox::getValue() const
{
    return m_value;
}


/**
 * Accesseur pour min.
 *
 * \return Valeur minimale.
 *
 * \sa CDoubleBox::setMinimum
 ******************************/

double CDoubleBox::getMinimum() const
{
    return m_min;
}


/**
 * Accesseur pour max.
 *
 * \return Valeur maximale.
 *
 * \sa CDoubleBox::setMaximum
 ******************************/

double CDoubleBox::getMaximum() const
{
    return m_max;
}


/**
 * Accesseur pour step.
 *
 * \return Valeur à ajouter à chaque incrémentation.
 *
 * \sa CDoubleBox::setStep
 ******************************/

double CDoubleBox::getStep() const
{
    return m_step;
}


/**
 * Donne le nombre de décimales à afficher.
 *
 * \return Nombre de décimales.
 *
 * \sa CDoubleBox::setDecimals
 ******************************/

unsigned int CDoubleBox::getDecimals() const
{
    return m_decimals;
}


/**
 * Mutateur pour value.
 *
 * \param value Valeur de la doublebox.
 *
 * \sa CDoubleBox::getValue
 ******************************/

void CDoubleBox::setValue(double value)
{
    T_ASSERT(m_line != nullptr);

    if (value < m_min)
        value = m_min;
    else if (value > m_max)
        value = m_max;

    if (std::abs(value - m_value) >= std::numeric_limits<float>::epsilon())
    {
        m_value = value;
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Mutateur pour min.
 *
 * \param minimum Valeur minimale.
 *
 * \sa CDoubleBox::getMinimum
 ******************************/

void CDoubleBox::setMinimum(double minimum)
{
    T_ASSERT(m_line != nullptr);

    if (minimum < m_max)
    {
        m_min = minimum;
    }

    if (m_value < m_min)
    {
        m_value = m_min;
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Mutateur pour max.
 *
 * \param maximum Valeur maximale.
 *
 * \sa CDoubleBox::getMaximum
 ******************************/

void CDoubleBox::setMaximum(double maximum)
{
    T_ASSERT(m_line != nullptr);

    if (maximum > m_min)
    {
        m_max = maximum;
    }

    if (m_value > m_max)
    {
        m_value = m_max;
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Modifie les valeurs minimales et maximales.
 *
 * \param minimum Valeur minimale.
 * \param maximum Valeur maximale.
 *
 * \sa CDoubleBox::setMinimum
 * \sa CDoubleBox::setMaximum
 ******************************/

void CDoubleBox::setRange(double minimum, double maximum)
{
    T_ASSERT(m_line != nullptr);

    if (minimum < maximum)
    {
        m_min = minimum;
        m_max = maximum;
    }

    double tmp = m_value;

    if (m_value > m_max)
    {
        m_value = m_max;
    }
    else if (m_value < m_min)
    {
        m_value = m_min;
    }

    if (std::abs(m_value - tmp) < std::numeric_limits<double>::epsilon())
    {
        onChange(m_value);

        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);
    }
}


/**
 * Mutateur pour step.
 *
 * \param step Valeur à ajouter à chaque incrémentation.
 *
 * \sa CDoubleBox::getStep
 ******************************/

void CDoubleBox::setStep(double step)
{
    m_step = step;
}


/**
 * Modifie le nombre de décimales à afficher.
 *
 * \param decimals Nombre de décimales à afficher.
 *
 * \sa CDoubleBox::getDecimals
 ******************************/

void CDoubleBox::setDecimals(unsigned int decimals)
{
    m_decimals = decimals;
}


/**
 * Incrémente la doublebox.
 *
 * \sa CDoubleBox::StepDown
 ******************************/

void CDoubleBox::stepUp()
{
    T_ASSERT(m_line != nullptr);

    double old_value = m_value;

    if (m_value + m_step > m_max)
    {
        m_value = (m_wrapping ? m_min : m_max);
    }
    else
    {
        m_value += m_step;
    }

    if (std::abs(old_value - m_value) > std::numeric_limits<double>::epsilon() )
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Décrémente la doublebox.
 *
 * \sa CDoubleBox::StepUp
 ******************************/

void CDoubleBox::stepDown()
{
    T_ASSERT(m_line != nullptr);

    double old_value = m_value;

    if (m_value - m_step < m_min)
    {
        m_value = (m_wrapping ? m_max : m_min);
    }
    else
    {
        m_value -= m_step;
    }

    if (std::abs(old_value - m_value) > std::numeric_limits<double>::epsilon())
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
}


/**
 * Méthode appelée lorsque le texte du champ est modifié.
 ******************************/

void CDoubleBox::updateValue()
{
    T_ASSERT(m_line != nullptr);

    bool ok;
    double value = m_line->getText().toDouble(&ok);

    if (!ok)
    {
        value = m_min;
    }
    else
    {
        if (value < m_min)
            value = m_min;
        else if (value > m_max)
            value = m_max;
    }

    if (std::abs(value - m_value) >= std::numeric_limits<float>::epsilon())
    {
        m_value = value;

        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);

        onChange(m_value);
    }
    else
    {
        m_line->setText(m_prefix + CString::fromNumber(m_value, m_decimals) + m_suffix);
    }
}

} // Namespace Ted

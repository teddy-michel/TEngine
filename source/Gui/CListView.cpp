/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CListView.cpp
 * \date 24/02/2010 Création de la classe GuiListView.
 * \date 04/01/2011 Améliorations.
 * \date 14/01/2011 Amélioration de l'affichage et du calcul des dimensions.
 * \date 14/01/2011 Utilisation des signaux.
 * \date 15/01/2011 Création des méthodes SelectItem et ToggleItem.
 * \date 15/01/2011 On peut sélectionner les items en cliquant dessus.
 * \date 17/01/2011 L'item pointé par le curseur est surligné.
 * \date 17/01/2011 On peut sélectionner un item avec le clavier.
 * \date 17/01/2011 On peut faire défiler tous les items de la liste.
 * \date 17/01/2011 Héritage de GuiFrame à la place de GuiWidget.
 * \date 18/01/2011 Modification de la barre de navigation verticale.
 * \date 29/05/2011 La classe est renommée en CListView.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CRenderer.hpp"
#include "Gui/CListView.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Gui/CScrollBar.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/CApplication.hpp"
#include "Core/Events.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

const int ListViewMinHeight = 10;  ///< Hauteur minimale du widget.
const int ListViewLineHeight = 16; ///< Hauteur d'une ligne en pixels.

#ifdef T_USE_FREETYPE
const int ListViewTextSize = 13;
#else
const int ListViewTextSize = 16;
#endif // T_USE_FREETYPE


/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CListView::CListView(IWidget * parent) :
IScrollArea    (parent),
m_current_item (-1),
m_line_top     (0),
m_num_items    (0),
m_mode         (Single),
m_padding      (0)
{
    setMinHeight(ListViewMinHeight);
}


/**
 * Donne le numéro de l'item sélectionné.
 *
 * \return Numéro de l'item ou -1.
 *
 * \sa CListView::setCurrentItem
 ******************************/

int CListView::getCurrentItem() const
{
    return m_current_item;
}


/**
 * Donne le mode de sélection des items.
 *
 * \return Mode de sélection.
 *
 * \sa CListView::setSelectionMode
 ******************************/

CListView::TSelectionMode CListView::getSelectionMode() const
{
    return m_mode;
}


/**
 * Donne l'espacement entre deux items.
 *
 * \return Espacement entre deux items en pixels.
 *
 * \sa CListView::setPadding
 ******************************/

int CListView::getPadding() const
{
    return m_padding;
}


/**
 * Donne la valeur d'un item.
 *
 * \param pos Position de l'item.
 * \return Valeur de l'item, ou une chaine vide si le numéro est supérieur au
 *         nombre d'items.
 ******************************/

CString CListView::getItem(int pos) const
{
    if (pos < 0 || pos >= m_num_items)
        return CString();

    TListItemList::const_iterator it = m_items.begin();
    std::advance(it, pos);

    return (it != m_items.end() ? it->text : CString());
}


/**
 * Change l'item sélectionné.
 * Si le numéro est supérieur au nombre d'item, le dernier item est sélectionné.
 *
 * \param select Numéro de l'item à sélectionner.
 *
 * \sa CListView::getCurrentItem
 ******************************/

void CListView::setCurrentItem(int select)
{
    if (m_num_items == 0)
        return;

    if (select < 0 || select >= m_num_items)
    {
        select = m_num_items - 1;
    }

    if (m_current_item != select)
    {
        m_current_item = select;

        onCurrentItemChange(m_current_item);

        if (m_current_item < m_line_top)
        {
            m_line_top = m_current_item;
            m_vScroll->setValue(m_line_top);
        }
        else
        {
            const int nbr_items = m_height / getLineHeight();

            if (m_current_item >= m_line_top + nbr_items)
            {
                m_line_top = m_current_item - nbr_items + 1;
                m_vScroll->setValue(m_line_top);
            }
        }
    }
}


/**
 * Change le mode de sélection.
 *
 * \param mode Mode de sélection.
 *
 * \sa CListView::getSelectionMode
 ******************************/

void CListView::setSelectionMode(TSelectionMode mode)
{
    m_mode = mode;
}


/**
 * Modifie l'espacement entre deux items.
 *
 * \param padding Espacement entre deux items en pixels.
 *
 * \sa CListView::getPadding
 ******************************/

void CListView::setPadding(int padding)
{
    if (padding < 0)
        padding = 0;

    m_padding = padding;
}


/**
 * Donne le nombre d'items de la liste.
 *
 * \return Nombre d'items.
 ******************************/

int CListView::getNumItems() const
{
    return m_num_items;
}


/**
 * Donne la hauteur d'une ligne.
 *
 * \return Hauteur d'une ligne en pixels.
 ******************************/

int CListView::getLineHeight() const
{
    return (m_padding + ListViewLineHeight + m_padding);
}


/**
 * Ajoute un item.
 *
 * \param text Chaine de caractères de l'item.
 * \param pos  Position de l'item (par défaut en dernière position).
 ******************************/

void CListView::addItem(const CString& text, int pos)
{
    TListItem item = { false, text };

    if (pos < 0 || pos >= m_num_items)
    {
        m_items.push_back(item);
    }
    else
    {
        TListItemList::iterator it = m_items.begin();
        std::advance(it, pos);
        m_items.insert(it, item);
    }

    ++m_num_items;
    m_vScroll->setTotal(m_num_items);
}


/**
 * Enlève un item de la liste.
 *
 * \todo Modifier l'attribut m_line_top si nécessaire.
 *
 * \param pos Position de l'item à enlever.
 ******************************/

void CListView::removeItem(int pos)
{
    if (pos < 0 || pos >= m_num_items)
        return;

    TListItemList::iterator it = m_items.begin();
    std::advance(it, pos);

    if (it != m_items.end())
    {
        m_items.erase(it);
        --m_num_items;
        m_vScroll->setTotal(m_num_items);

        if (m_current_item >= m_num_items)
        {
            m_current_item = m_num_items - 1;

            onCurrentItemChange(m_current_item);
        }
    }
}


/**
 * Modifie la sélection d'un item.
 *
 * \param pos    Position de l'item à sélectionner.
 * \param select Booléen indiquant s'il faut sélectionner ou déselectionner l'item.
 ******************************/

void CListView::selectItem(int pos, bool select)
{
    if (pos < 0 || pos >= m_num_items || m_mode == None)
    {
        return;
    }

    TListItemList::iterator it = m_items.begin();
    std::advance(it, pos);

    if (it == m_items.end())
    {
        return;
    }

    if (m_mode == Single || m_mode == SingleOrNone)
    {
        // L'item a déjà cette sélection
        if (it->selected ==select)
        {
            return;
        }

        // On parcourt la liste pour déselectionner tous les items
        for (TListItemList::iterator it0 = m_items.begin(); it0 != m_items.end(); ++it0)
        {
            it0->selected = false;
        }
    }

    it->selected = select;

    onSelectionChange();
    onItemSelected(pos);
}


/**
 * Inverse la sélection d'un item.
 *
 * \param pos Position de l'item à sélectionner.
 ******************************/

void CListView::toggleItem(int pos)
{
    if (pos < 0 || pos >= m_num_items || m_mode == None)
    {
        return;
    }

    TListItemList::iterator it = m_items.begin();
    std::advance(it, pos);

    if (it == m_items.end())
    {
        return;
    }

    if (m_mode == SingleOrNone)
    {
        // On désélectionne tous les autres items
        for (TListItemList::iterator it0 = m_items.begin(); it0 != m_items.end(); ++it0)
        {
            if (it != it0)
                it0->selected = false;
        }

        it->selected = !it->selected;
    }
    else if (m_mode == Single)
    {
        // L'item est sélectionné et ne peut pas être désélectionné
        if (it->selected)
        {
            onItemSelected(pos);

            return;
        }

        // On désélectionne tous les autres items
        for (TListItemList::iterator it0 = m_items.begin(); it0 != m_items.end(); ++it0)
        {
            it0->selected = false;
        }

        it->selected = true;
    }
    // Sélection multiple
    else
    {
        it->selected = !it->selected;
    }

    onSelectionChange();
    if (it->selected)
        onItemSelected(pos);
}


/**
 * Dessine la liste.
 *
 * \todo Afficher les barres de navigation si nécessaire.
 ******************************/

void CListView::draw()
{
    const int tmp_x = getX();
    const int tmp_y = getY();

    // Fond du champ
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::White);
    Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::Black);

    int i = 0;
    const int line_height = getLineHeight();

    // Affichage du texte
    for (TListItemList::const_iterator it = m_items.begin(); it != m_items.end(); ++it, ++i)
    {
        // Au-dessus de la ligne à afficher
        if (i < m_line_top)
        {
            continue;
        }

        // En-dessous de la dernière ligne affichable
        if (line_height * (i + 1 - m_line_top) > m_height)
        {
            break;
        }

        // Cet item est sélectionné
        if (it->selected)
        {
            Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1, tmp_y + line_height * (i - m_line_top) ,
                                                      m_width - 2, line_height), CColor::Orange);
        }
        // Cet item est pointé par le curseur de la souris
        else if (Game::guiEngine->isPointed(this))
        {
            TVector2I mouse = CApplication::getApp()->getCursorPosition();

            if (mouse.Y >= tmp_y + line_height * (static_cast<int>(i) - static_cast<int>(m_line_top)) &&
                mouse.Y < tmp_y + line_height * (1 + static_cast<int>(i) - static_cast<int>(m_line_top)))
            {
                Game::guiEngine->drawRectangle(CRectangle(tmp_x + 1, tmp_y + line_height * (i - m_line_top) ,
                                                          m_width - 2, line_height), CColor(255, 128, 0, 200));
            }
        }

        // Ligne sélectionnée
        if (i == m_current_item)
        {
            Game::guiEngine->drawBorder(CRectangle(tmp_x + 1, tmp_y + 1 + line_height * (i - m_line_top),
                                                   m_width - 2, line_height - 1), CColor::Grey);
        }

        TTextParams params;
        params.text  = CString(it->text);
        params.font  = Game::fontManager->getFontId("Arial");
        params.size  = ListViewTextSize;
        params.color = CColor::Black;
        params.rec   = CRectangle(tmp_x + 5, tmp_y + m_padding + line_height * (i - m_line_top));

        Game::guiEngine->drawText(params);
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CListView::onEvent(const CMouseEvent& event)
{
    if (event.button != MouseButtonLeft || event.type != ButtonPressed)
    {
        return;
    }

    int offset = getY();
    const int line_height = getLineHeight();

    for (int i = 0; (i < m_num_items - m_line_top) && (i * line_height < m_height); ++i)
    {
        if (static_cast<int>(event.y) >= offset &&
            static_cast<int>(event.y) <= offset + line_height)
        {
            toggleItem(i + m_line_top);
            setCurrentItem(i + m_line_top);
            break;
        }

        offset += line_height;
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \todo Lorsqu'on appuie sur une touche, on cherche le premier item correspondant.
 *
 * \param event Évènement.
 ******************************/

void CListView::onEvent(const CKeyboardEvent& event)
{
    if (!isHierarchyEnable())
        return;

    if (event.type == KeyReleased)
        return;

    switch (event.code)
    {
        default:
            break;

        // On appuie sur Entrée ou sur Espace
        case Key_Enter:
        case Key_Space:

            if (m_current_item >= 0)
            {
                toggleItem(m_current_item);
            }

            break;

        // On sélectionne l'item au-dessus
        case Key_Up:

            if (m_num_items > 0)
            {
                if (m_current_item < 0)
                    setCurrentItem(0);
                else if (m_current_item > 0)
                    setCurrentItem(m_current_item - 1);
            }

            break;

        // On sélectionne l'item en-dessous
        case Key_Down:

            if (m_num_items > 0)
            {
                if (m_current_item < 0)
                    setCurrentItem(0);
                else if (m_current_item + 1 < m_num_items)
                    setCurrentItem(m_current_item + 1);
            }

            break;

        // On sélectionne le premier item de la liste
        case Key_Home:
            setCurrentItem(m_num_items == 0 ? -1 : 0);
            break;

        // On sélectionne le dernier item de la liste
        case Key_End:
            setCurrentItem(m_num_items == 0 ? -1 : m_num_items - 1);
            break;

        // Page précédente
        case Key_PageUp:
        {
            int num_items = m_height / getLineHeight();
            setCurrentItem(m_num_items == 0 ? -1 : (m_current_item > num_items ? m_current_item - num_items : 0));
            break;
        }

        // Page suivante
        case Key_PageDown:
        {
            int num_items = m_height / getLineHeight();
            setCurrentItem(m_num_items == 0 ? -1 : (m_current_item + num_items < m_num_items ? m_current_item + num_items : m_num_items - 1));
            break;
        }
    }
}


/**
 * Supprime tous les items de la liste.
 ******************************/

void CListView::clear()
{
    m_items.clear();

    m_current_item = -1;
    m_line_top = 0;
    m_num_items = 0;

    m_vScroll->setTotal(m_num_items);
}


/**
 * Déselectionne tous les items.
 ******************************/

void CListView::clearSelection()
{
    if (m_mode != Single)
    {
        for (TListItemList::iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            it->selected = false;
        }
    }
}


/**
 * Inverse la sélection.
 ******************************/

void CListView::invertSelection()
{
    if (m_mode == Multiple || m_mode == Extended)
    {
        // On inverse tous les items
        for (TListItemList::iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            it->selected = !(it->selected);
        }
    }
    else if (m_mode == Single || m_mode == SingleOrNone)
    {
        // On ne sélectionne que le premier item
        for (TListItemList::iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            it->selected = false;
        }

        m_items.begin()->selected = true;
    }
}


/**
 * Sélectionne tous les items.
 ******************************/

void CListView::selectAll()
{
    if (m_mode == Multiple || m_mode == Extended)
    {
        // On sélectionne tous les items
        for (TListItemList::iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            it->selected = true;
        }
    }
    else if (m_mode == Single || m_mode == SingleOrNone)
    {
        // On ne sélectionne que le premier item
        for (TListItemList::iterator it = m_items.begin(); it != m_items.end(); ++it)
        {
            it->selected = false;
        }

        m_items.begin()->selected = true;
    }
}

} // Namespace Ted

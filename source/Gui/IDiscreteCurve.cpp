/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IDiscreteCurve.cpp
 * \date 19/04/2013 Création de la classe IDiscreteCurve.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IDiscreteCurve.hpp"

#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Construit une courbe dont les valeurs sont discrètes.
 *
 * \param parent Pointeur sur le widget parent.
 ******************************/

IDiscreteCurve::IDiscreteCurve(IWidget * parent) :
ICurve (parent)
{

}


/**
 * Détruit la courbe.
 ******************************/

IDiscreteCurve::~IDiscreteCurve()
{

}


float IDiscreteCurve::getCurveYFromX(float x) const
{
    int integerPart = static_cast<int>(x);
    float decimalPart = x - integerPart;

    if (decimalPart > 0.0f)
        return (getCurveYFromX(integerPart + 1) * decimalPart) + (getCurveYFromX(integerPart) * (1.0f - decimalPart)); // Interpollation
    else
        return getCurveYFromX(integerPart);
}


float IDiscreteCurve::getCurveMinY(float minX, float maxX) const
{
    return getCurveMinY(static_cast<int>(minX), static_cast<int>(maxX) + 1);
}


float IDiscreteCurve::getCurveMaxY(float minX, float maxX) const
{
    return getCurveMaxY(static_cast<int>(minX), static_cast<int>(maxX) + 1);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/IScrollArea.cpp
 * \date 17/01/2011 Création de la classe GuiFrame.
 * \date 20/01/2011 Création des méthodes getWidgetSize et ComputeScrollSize.
 * \date 14/04/2011 La classe est renommée en GuiBaseScrollArea.
 * \date 29/05/2011 La classe est renommée en IScrollArea.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Gui/IScrollArea.hpp"
#include "Gui/CScrollBar.hpp"
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

IScrollArea::IScrollArea(IWidget * parent) :
IWidget        (parent),
m_showVScroll (true),
m_showHScroll (true),
m_vScroll      (nullptr),
m_hScroll      (nullptr)
{
    m_vScroll = new CScrollBar(this);
    T_ASSERT(m_vScroll != nullptr);
    m_vScroll->setTotal(m_height);
    m_vScroll->setStep(m_height);

    m_hScroll = new CScrollBar(this);
    T_ASSERT(m_hScroll != nullptr);
    m_hScroll->setHorizontal();
    m_hScroll->setTotal(m_width);
    m_hScroll->setStep(m_width);
}


/**
 * Destructeur.
 ******************************/

IScrollArea::~IScrollArea()
{
    delete m_vScroll;
    delete m_hScroll;
}


/**
 * Donne la largeur de la frame.
 *
 * \return Largeur de la frame en pixels.
 *
 * \sa IScrollArea::setFrameWidth
 ******************************/

int IScrollArea::getFrameWidth() const
{
    T_ASSERT(m_hScroll != nullptr);
    return m_hScroll->getTotal();
}


/**
 * Donne la hauteur de la frame.
 *
 * \return Hauteur de la frame en pixels.
 *
 * \sa IScrollArea::setFrameHeight
 ******************************/

int IScrollArea::getFrameHeight() const
{
    T_ASSERT(m_vScroll != nullptr);
    return m_vScroll->getTotal();
}


/**
 * Donne le pointeur sur la barre de navigation verticale.
 *
 * \return Pointeur sur la barre de navigation verticale.
 ******************************/

CScrollBar * IScrollArea::getVerticalScrollBar() const
{
    return m_vScroll;
}


/**
 * Donne le pointeur sur la barre de navigation horizontale.
 *
 * \return Pointeur sur la barre de navigation horizontale.
 ******************************/

CScrollBar * IScrollArea::getHorizontalScrollBar() const
{
    return m_hScroll;
}


/**
 * Indique si la barre verticale doit toujours être affichée.
 *
 * \return Booléen.
 *
 * \sa IScrollArea::setShowVerticalScrollBar
 ******************************/

bool IScrollArea::isShowVerticalScrollBar() const
{
    return m_showVScroll;
}


/**
 * Indique si la barre horizontale doit toujours être affichée.
 *
 * \return Booléen.
 *
 * \sa IScrollArea::setShowHorizontalScrollBar
 ******************************/

bool IScrollArea::isShowHorizontalScrollBar() const
{
    return m_showHScroll;
}


/**
 * Modifie la largeur du champ.
 *
 * \param width Largeur de l'objet en pixels.
 ******************************/

void IScrollArea::setWidth(int width)
{
    if (width < 0)
        return;

    int oldWidth = m_width;

    IWidget::setWidth(width);

    if (oldWidth != m_width)
    {
        computeScrollSize();
    }
}


/**
 * Modifie la hauteur du champ.
 *
 * \param height Hauteur de l'objet en pixels.
 ******************************/

void IScrollArea::setHeight(int height)
{
    if (height < 0)
        return;

    const int oldHeight = m_height;

    IWidget::setHeight(height);

    if (oldHeight != m_height)
    {
        computeScrollSize();
    }
}


/**
 * Modifie la largeur de la frame
 *
 * \param frameWidth Largeur de la frame en pixels.
 *
 * \sa IScrollArea::getFrameWidth
 ******************************/

void IScrollArea::setFrameWidth(int frameWidth)
{
    if (frameWidth < 0)
        return;

    T_ASSERT(m_hScroll != nullptr);
    m_hScroll->setTotal(frameWidth);

    computeScrollSize();
}


/**
 * Modifie la hauteur de la frame
 *
 * \param frameHeight Hauteur de la frame en pixels.
 *
 * \sa IScrollArea::getFrameHeight
 ******************************/

void IScrollArea::setFrameHeight(int frameHeight)
{
    if (frameHeight < 0)
        return;

    T_ASSERT(m_vScroll != nullptr);
    m_vScroll->setTotal(frameHeight);

    computeScrollSize();
}


/**
 * Modifie les dimensions de la frame.
 *
 * \param frameWidth  Hauteur de la frame en pixels.
 * \param frameHeight Hauteur de la frame en pixels.
 *
 * \sa IScrollArea::setFrameWidth
 * \sa IScrollArea::setFrameHeight
 ******************************/

void IScrollArea::setFrameSize(int frameWidth, int frameHeight)
{
    if (frameWidth < 0)
        return;

    if (frameHeight < 0)
        return;

    T_ASSERT(m_hScroll != nullptr);
    m_hScroll->setTotal(frameWidth);

    T_ASSERT(m_vScroll != nullptr);
    m_vScroll->setTotal(frameHeight);

    computeScrollSize();
}


/**
 * Modifie la visibilité de l'ascenseur vertical.
 *
 * \param show Indique si l'ascenseur vertical doit toujours être affiché.
 *
 * \sa IScrollArea::isShowVerticalScrollBar
 ******************************/

void IScrollArea::setShowVerticalScrollBar(bool show)
{
    m_showVScroll = show;
    computeScrollSize();
}


/**
 * Modifie la visibilité de l'ascenseur horizontal.
 *
 * \param show Indique si l'ascenseur horizontal doit toujours être affiché.
 *
 * \sa IScrollArea::isShowHorizontalScrollBar
 ******************************/

void IScrollArea::setShowHorizontalScrollBar(bool show)
{
    m_showHScroll = show;
    computeScrollSize();
}


/**
 * Indique si l'ascenseur vertical doit être affiché.
 *
 * \return Booléen.
 ******************************/

bool IScrollArea::shouldShowVerticalScrollBar() const
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    const int tmp = m_vScroll->getTotal();

    return (m_showVScroll || tmp > m_height ||
           (tmp > m_height - static_cast<int>(m_hScroll->getHeight()) && m_hScroll->getTotal() > m_width));
}


/**
 * Indique si l'ascenseur horizontal doit être affiché.
 *
 * \return Booléen.
 ******************************/

bool IScrollArea::shouldShowHorizontalScrollBar() const
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    const int tmp = m_hScroll->getTotal();

    return (m_showHScroll || tmp > m_width ||
           (tmp > m_width - static_cast<int>(m_vScroll->getHeight()) && m_vScroll->getTotal() > m_height));
}


/**
 * Calcule les dimensions et les positions des ascenseurs.
 * Cette méthode doit être appellée à chaque fois que les dimensions du widget
 * changent, ou que les paramètres des ascenseurs sont modifiés.
 ******************************/

void IScrollArea::computeScrollSize()
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    m_vScroll->setX(m_width - m_vScroll->getWidth() - 1);
    m_vScroll->setStep(m_height);

    m_hScroll->setY(m_height - m_hScroll->getHeight());
    m_hScroll->setStep(m_width);

    const bool visvscroll = shouldShowVerticalScrollBar();   //( m_show_vscroll || m_vscroll->getTotal() > m_vscroll->getStep() );
    const bool vishscroll = shouldShowHorizontalScrollBar(); //( m_show_hscroll || m_hscroll->getTotal() > m_hscroll->getStep() );

    if (visvscroll)
    {
        // Affichage des deux barres
        if (vishscroll)
        {
            m_vScroll->setHeight(m_height - m_hScroll->getHeight());
            m_hScroll->setWidth(m_width - m_vScroll->getWidth());
        }
        // Affichage de la barre verticale uniquement
        else
        {
            m_vScroll->setHeight(m_height);
        }
    }
    // Affichage de la barre horizontale uniquement
    else if (vishscroll)
    {
        m_hScroll->setWidth(m_width);
    }
}


/**
 * Calcule les dimensions de la zone réservée au widget lui-même.
 *
 * \return Dimensions du widget.
 ******************************/

TVector2I IScrollArea::getWidgetSize() const
{
    T_ASSERT(m_vScroll != nullptr);
    T_ASSERT(m_hScroll != nullptr);

    TVector2I size(m_width, m_height);

    const bool visvscroll = (m_showVScroll || m_vScroll->getTotal() > m_vScroll->getStep());
    const bool vishscroll = (m_showHScroll || m_hScroll->getTotal() > m_hScroll->getStep());

    if (visvscroll)
    {
        size.X -= m_vScroll->getWidth();
    }

    if (vishscroll)
    {
        size.Y -= m_hScroll->getHeight();
    }

    return size;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Gui/CProgressBar.cpp
 * \date       2008 Création de la classe GuiProgressBar.
 * \date 13/11/2010 La barre défile de gauche à droite si total vaut 0.
 * \date 29/05/2011 La classe est renommée en CProgressBar.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <limits>

#include "Gui/CProgressBar.hpp"
#include "Gui/CGuiEngine.hpp"
#include "Core/Maths/CRectangle.hpp"
#include "Core/Time.hpp"


namespace Ted
{

const unsigned int ProgressBarHeight = 15;    ///< Hauteur de la barre en pixels.
const unsigned int ProgressBarMinWidth = 100; ///< Largeur de la barre en pixels.


/**
 * Constructeur par défaut.
 *
 * \param total Nombre total d'unités.
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CProgressBar::CProgressBar(float total, IWidget * parent) :
IWidget (parent),
m_value (0.0f),
m_total (total)
{
    if (m_total < 0.0f)
        m_total = 0.0f;

    setMinSize(ProgressBarMinWidth, ProgressBarHeight);
    setMaxHeight(ProgressBarHeight);
}


/**
 * Constructeur.
 *
 * \param parent Pointeur sur l'objet parent.
 ******************************/

CProgressBar::CProgressBar(IWidget * parent) :
IWidget (parent),
m_value (0.0f),
m_total (0.0f)
{
    setMinSize(ProgressBarMinWidth, ProgressBarHeight);
    setMaxHeight(ProgressBarHeight);
}


/**
 * Calcul le pourcentage effectué.
 *
 * \return Pourcentage effectué.
 *
 * \sa CProgressBar::setPourcentage
 ******************************/

float CProgressBar::getPourcentage() const
{
    return (100.0f * m_value / m_total);
}


/**
 * Accesseur pour valeur.
 *
 * \return Nombre d'unités effectuées.
 *
 * \sa CProgressBar::setValue
 ******************************/

float CProgressBar::getValue() const
{
    return m_value;
}


/**
 * Accesseur pour total.
 *
 * \return Nombre total d'unités.
 *
 * \sa CProgressBar::setTotal
 ******************************/

float CProgressBar::getTotal() const
{
    return m_total;
}


/**
 * Définie le pourcentage effectué. Si la valeur n'est pas comprise entre 0 et 100,
 * rien n'est modifié.
 *
 * \param pourcentage Pourcentage effectué.
 *
 * \sa CProgressBar::getPourcentage
 ******************************/

void CProgressBar::setPourcentage(float pourcentage)
{
    if (pourcentage >= 0.0f && pourcentage <= 100.0f)
    {
        m_value = pourcentage * m_total * 0.01f;
    }
}


/**
 * Modifie le nombre d'unités effectuées. Si ce nombre est négatif ou supérieur
 * au nombre total d'unités, rien n'est modifié.
 *
 * \param valeur Nombre d'unités effectuées.
 *
 * \sa CProgressBar::getValue
 ******************************/

void CProgressBar::setValue(float valeur)
{
    if (valeur > 0.0f && valeur < m_total)
    {
        m_value = valeur;
    }
}


/**
 * Modifie le nombre total d'unité. Ce nombre doit être positif.
 * Si la nouvelle valeur est inférieure au nombre actuelle d'unités effectuées,
 * on considère que toutes les unités sont effectuées.
 *
 * \param total Nombre total d'unités.
 *
 * \sa CProgressBar::getTotal
 ******************************/

void CProgressBar::setTotal(float total)
{
    if (total > 0.0f)
    {
        m_total = total;

        if (m_value > m_total)
        {
            m_value = m_total;
        }
    }
}


/**
 * Dessine le barre de progression.
 ******************************/

void CProgressBar::draw()
{
    static unsigned int time = 0;
    static int busy = m_width * -0.5f;

    unsigned int ntime = getElapsedTime();

    const int tmp_x = getX();
    const int tmp_y = getY();


    // Bordure de la barre
    Game::guiEngine->drawBorder(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor(50, 50, 50));

    // Intérieur de la barre
    Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, m_width, m_height), CColor::White);

    // Barre de progression (orange)
    if (m_total > 0.0f)
    {
        Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y,
                                                  static_cast<unsigned int>(m_width * (m_value / m_total)),
                                                  m_height),
                                       CColor::Orange);
    }
    else
    {
        const unsigned int bar_width = m_width * 0.3f;

        if (ntime > time)
        {
            busy += (ntime - time) * static_cast<float>(m_width) * 0.0015f;
        }

        if (busy > m_width * 1.5f - bar_width)
        {
            busy = m_width * -0.5f;
        }

        if (busy > static_cast<int>(-bar_width) && busy < 0)
        {
            Game::guiEngine->drawRectangle(CRectangle(tmp_x, tmp_y, bar_width + busy, m_height), CColor::Orange);
        }
        else if (busy > 0 && busy < static_cast<int>(m_width) - static_cast<int>(bar_width))
        {
            Game::guiEngine->drawRectangle(CRectangle(tmp_x + busy, tmp_y, bar_width, m_height), CColor::Orange);
        }
        else if (busy > static_cast<int>(m_width) - static_cast<int>(bar_width) && busy < static_cast<int>(m_width))
        {
            Game::guiEngine->drawRectangle(CRectangle(tmp_x + busy, tmp_y, m_width - busy, m_height), CColor::Orange);
        }
    }

    time = ntime;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/Socket.cpp
 * \date      2008 Création des fonctions utiles aux sockets.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Network/Socket.hpp"

#if defined T_SYSTEM_LINUX
#  include <errno.h>
#endif

namespace Ted
{

namespace Socket
{

/**
 * Initialise la socket
 ******************************/

void initialise()
{
#if defined T_SYSTEM_WINDOWS
    WSADATA WSAData;
    WSAStartup(MAKEWORD(2, 0), &WSAData);
#endif
}


/**
 * Destructeur.
 ******************************/

void clean()
{
#if defined T_SYSTEM_WINDOWS
    WSACleanup();
#endif
}


/**
 * Get the last socket error status.
 *
 * \return Status corresponding to the last socket error.
 ******************************/

Status getErrorStatus()
{
#if defined T_SYSTEM_WINDOWS

    switch (WSAGetLastError())
    {
        case WSAEWOULDBLOCK:  return Socket::NotReady;
        case WSAECONNABORTED: return Socket::Disconnected;
        case WSAECONNRESET:   return Socket::Disconnected;
        case WSAETIMEDOUT:    return Socket::Disconnected;
        case WSAENETRESET:    return Socket::Disconnected;
        case WSAENOTCONN:     return Socket::Disconnected;
        default:              return Socket::Error;
    }

#elif defined T_SYSTEM_LINUX

    switch (errno)
    {
        case EWOULDBLOCK:     return Socket::NotReady;
        case ECONNABORTED:    return Socket::Disconnected;
        case ECONNRESET:      return Socket::Disconnected;
        case ETIMEDOUT:       return Socket::Disconnected;
        case ENETRESET:       return Socket::Disconnected;
        case ENOTCONN:        return Socket::Disconnected;
        default:              return Socket::Error;
    }

#endif
}

} // Namespace Socket

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CSocketUDP.cpp
 * \date       2008 Création de la classe CSocketUDP.
 * \date 23/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 15/03/2011 Inclusion de trois fichiers pour Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "os.h"

#if defined(T_SYSTEM_LINUX)
#  include <fcntl.h>
#  include <cstring>
#  include <netinet/tcp.h>
#endif

#include "Network/CIPAddress.hpp"
#include "Network/CSocketUDP.hpp"
#include "Network/CPacket.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut
 ******************************/

CSocketUDP::CSocketUDP() :
m_socket              (INVALID_SOCKET),
m_port                (0),
m_pending_packet_size (0),
m_blocking            (true)
{
    create();
}


/**
 * Accesseur pour port
 *
 * \return Current port (0 means the socket is not bound)
 ******************************/

uint16_t CSocketUDP::getPort() const
{
    return m_port;
}


/**
 * Change the blocking state of the socket.
 *
 * \param blocking Pass true to set the socket as blocking, or false for non-blocking.
 ******************************/
void CSocketUDP::setBlocking(bool blocking)
{
    // Make sure our socket is valid
    if (!isValid())
    {
        create();
    }

#if defined T_SYSTEM_WINDOWS

    unsigned long block = blocking ? 0 : 1;
    ioctlsocket(m_socket, FIONBIO, &block);

#elif defined T_SYSTEM_LINUX

    int status = fcntl(m_socket, F_GETFL);

    if (blocking)
    {
        fcntl(m_socket, F_SETFL, status & ~O_NONBLOCK);
    }
    else
    {
        fcntl(m_socket, F_SETFL, status | O_NONBLOCK);
    }

#endif

    m_blocking = blocking;
}


/**
 * Bind the socket to a specific port
 *
 * \param port Port to bind the socket to
 * \return True if operation has been successful
 ******************************/

bool CSocketUDP::bind(uint16_t port)
{
    // Check if the socket is already bound to the specified port
    if (m_port != port)
    {
        // If the socket was previously bound to another port, we need to recreate it
        if (m_port != 0)
        {
            close();
            create();
        }

        if (port != 0)
        {
            // Build an address with the specified port
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(port);
            addr.sin_addr.s_addr = INADDR_ANY;
            memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

            // Bind the socket to the port
            if (::bind(m_socket, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) == -1)
            {
                CApplication::getApp()->log(CString("Failed to bind the socket to port %1").arg(port), ILogger::Error);
                m_port = 0;
                return false;
            }
        }

        // Save the new port
        m_port = port;
    }

    return true;
}


/**
 * Unbind the socket to its previous port
 *
 * \return True if operation has been successful
 ******************************/

bool CSocketUDP::unbind()
{
    // To unbind the socket, we just recreate it
    if (m_port != 0)
    {
        close();
        create();
        m_port = 0;
    }

    return true;
}


/**
 * Send an array of bytes.
 *
 * \param data Pointer to the bytes to send
 * \param size Number of bytes to send
 * \param addr Address of the computer to send the packet to
 * \param port Port to send the data to
 * \return Status code.
 ******************************/

Socket::Status CSocketUDP::send(const char * data, std::size_t size, const CIPAddress& addr, uint16_t port)
{
    // Make sure the socket is valid
    if (!isValid())
    {
        create();
    }

    // Check parameters
    if (data && size)
    {
        // Build the target address
        sockaddr_in target;
        target.sin_family = AF_INET;
        target.sin_port = htons(port);
        target.sin_addr.s_addr = inet_addr(addr.toString().toCharArray());
        memset(target.sin_zero, 0, sizeof(target.sin_zero));

        // Loop until every byte has been sent
        int sent = 0;
        int sizeToSend = static_cast<int>(size);

        for (int length = 0; length < sizeToSend; length += sent)
        {
            // Send a chunk of data
            sent = sendto(m_socket, data + length, sizeToSend - length, 0, reinterpret_cast<sockaddr *>(&target), sizeof(target));

            // Check errors
            if (sent <= 0)
            {
                return Socket::getErrorStatus();
            }
        }

        return Socket::Done;
    }
    else
    {
        // Error...
        return Socket::Error;
    }
}


/**
 * Send a packet of data.
 *
 * \param packet Packet to send.
 * \param addr   Address of the computer to send the packet to.
 * \param port   Port to send the data to.
 * \return Status code.
 ******************************/

Socket::Status CSocketUDP::send(CPacket& packet, CIPAddress& addr, uint16_t port)
{
    // Get the data to send from the packet
    std::size_t dataSize = 0;
    const char * data = packet.onSend(dataSize);

    // Send the packet size
    uint32_t PacketSize = htonl(static_cast<unsigned long>(dataSize));
    send(reinterpret_cast<const char *>(&PacketSize), sizeof(PacketSize), addr, port);

    // Send the packet data
    if (PacketSize > 0)
    {
        return send(data, dataSize, addr, port);
    }
    else
    {
        return Socket::Done;
    }
}


/**
 * Receive an array of bytes.
 *  This function will block if the socket is blocking
 *
 * \param data         Pointer to a byte array to fill (make sure it is big enough)
 * \param maxsize      Maximum number of bytes to read
 * \param sizereceived Number of bytes received
 * \param addr         Address of the computer which sent the data
 * \return Status code
 ******************************/

Socket::Status CSocketUDP::receive(char * data, std::size_t maxsize, std::size_t& sizereceived, CIPAddress& addr)
{
    // First clear the size received
    sizereceived = 0;

    // Make sure the socket is bound to a port
    if (m_port == 0)
    {
        CApplication::getApp()->log("Failed to receive data ; the UDP socket first needs to be bound to a port.", ILogger::Error);
        return Socket::Error;
    }

    // Make sure the socket is valid
    if (!isValid())
    {
        create();
    }

    // Check parameters
    if (data && maxsize)
    {
        // Data that will be filled with the other computer's address
        sockaddr_in sender;
        sender.sin_family = AF_INET;
        sender.sin_port = htons(m_port);
        sender.sin_addr.s_addr = INADDR_ANY;
        memset(sender.sin_zero, 0, sizeof(sender.sin_zero));

#if defined T_SYSTEM_WINDOWS
        int SenderSize = sizeof(sender);
#elif defined T_SYSTEM_LINUX
        socklen_t SenderSize = sizeof(sender);
#endif

        // Receive a chunk of bytes
        int received = recvfrom(m_socket, data, static_cast<int>(maxsize), 0, reinterpret_cast<sockaddr*>(&sender), &SenderSize);

        // Check the number of bytes received
        if (received > 0)
        {
            addr = CIPAddress(inet_ntoa(sender.sin_addr));
            sizereceived = static_cast<std::size_t>(received);
            return Socket::Done;
        }
        else
        {
            addr = CIPAddress();
            return received == 0 ? Socket::Disconnected : Socket::getErrorStatus();
        }
    }
    else
    {
        CApplication::getApp()->log("Cannot receive data from the network (invalid parameters).", ILogger::Error);
        return Socket::Error;
    }
}


/**
 * Receive a packet. This function will block if the socket is blocking.
 *
 * \param packet Packet to fill with received data.
 * \param addr   Address of the computer which sent the packet.
 * \return Status code.
 ******************************/

Socket::Status CSocketUDP::receive(CPacket& packet, CIPAddress& addr)
{
    // This is not safe at all, as data can be lost, duplicated, or arrive in a different order.
    // So if a packet is split into more than one chunk, nobody knows what could happen...
    // Conclusion : we shouldn't use packets with UDP, unless we build a more complex protocol on top of it.

    // We start by getting the size of the incoming packet
    uint32_t packet_size = 0;
    std::size_t received   = 0;

    if (m_pending_packet_size < 0)
    {
        Socket::Status status = receive(reinterpret_cast<char *>(&packet_size), sizeof(packet_size), received, addr);

        if (status != Socket::Done)
        {
            return status;
        }

        packet_size = ntohl(packet_size);
    }
    else
    {
        // There is a pending packet : we already know its size
        packet_size = m_pending_packet_size;
    }

    // Clear the user packet
    packet.clear();

    // Use another address instance for receiving the packet data ;
    // chunks of data coming from a different sender will be discarded (and lost...)
    CIPAddress sender;

    // Then loop until we receive all the packet data
    char buffer[1024];

    while (m_pending_packet.size() < packet_size)
    {

#if defined(T_SYSTEM_WINDOWS) && defined(min)
#  undef min
#endif

        // Receive a chunk of data
        std::size_t SizeToGet = std::min(static_cast<std::size_t>(packet_size - m_pending_packet.size()), sizeof(buffer));
        Socket::Status status = receive(buffer, SizeToGet, received, sender);

        if (!status)
        {
            // We must save the size of the pending packet until we can receive its content
            if (status == Socket::NotReady)
            {
                m_pending_packet_size = packet_size;
            }

            return status;
        }

        // Append it into the packet
        if (sender == addr && received > 0)
        {
            m_pending_packet.resize(m_pending_packet.size() + received);
            char * begin = &m_pending_packet[0] + m_pending_packet.size() - received;
            memcpy(begin, buffer, received);
        }
    }

    // We have received all the datas : we can copy it to the user packet, and clear our internal packet
    packet.clear();

    if (!m_pending_packet.empty())
    {
        packet.onReceive(&m_pending_packet[0], m_pending_packet.size());
    }

    m_pending_packet.clear();
    m_pending_packet_size = -1;

    return Socket::Done;
}


/**
 * Ferme le socket.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CSocketUDP::close()
{
    if (isValid())
    {
#if defined T_SYSTEM_WINDOWS
        if (closesocket(m_socket) == -1)
#elif defined T_SYSTEM_LINUX
        if (::close(m_socket) == -1)
#endif
        {
            return false;
        }

        m_socket = INVALID_SOCKET;
    }

    m_port = 0;
    m_blocking = true;

    return true;
}


/**
 * Check if the socket is in a valid state ; this function can be called any time to
 * check if the socket is OK.
 *
 * \return Booléen.
 ******************************/

bool CSocketUDP::isValid() const
{
    return (m_socket != INVALID_SOCKET);
}


/**
 * Create the socket.
 *
 * \param sock System socket descriptor to use (0 by default -- create a new socket).
 ******************************/

void CSocketUDP::create(SOCKET sock)
{
    // Use the given socket descriptor, or get a new one
    m_socket = sock ? sock : socket(PF_INET, SOCK_DGRAM, 0);
    m_blocking = true;

    // Clear the last port used
    m_port = 0;

    // Reset the pending packet
    m_pending_packet.clear();
    m_pending_packet_size = -1;

    // Setup default options
    if (isValid())
    {
        // To avoid the "Address already in use" error message when trying to bind to the same port
        int yes = 1;

        if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1)
        {
            CApplication::getApp()->log("Failed to set socket option \"reuse address\" ; binding to a same port may fail if too fast.", ILogger::Warning);
        }

        // Enable broadcast by default
        if (setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1)
        {
            CApplication::getApp()->log("Failed to enable broadcast on UDP socket.", ILogger::Warning);
        }

        // Set blocking by default (should always be the case anyway)
        setBlocking(true);
    }
}

} // Namespace Ted

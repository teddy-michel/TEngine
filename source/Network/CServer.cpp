/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CServer.cpp
 * \date 20/05/2009 Création de la classe CServer.
 * \date 28/02/2011 Correction d'un bug dans DeleteClient lié à l'utilisation des itérateurs.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Network/CServer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CServer::CServer()
{ }


/**
 * Destructeur.
 ******************************/

CServer::~CServer()
{ }


/**
 * Donne le nombre de clients connectés au serveur.
 *
 * \return Nombre de clients.
 ******************************/

unsigned int CServer::getNumClients() const
{
    return m_clients.size();
}


/**
 * Ajoute un client au serveur.
 *
 * \param client Client à ajouter.
 ******************************/

void CServer::addClient(int client)
{
    m_clients.push_back(client);
}


/**
 * Enlève un client au serveur.
 *
 * \param client Client à enlever.
 ******************************/

void CServer::deleteClient(int client)
{
    for (std::vector<int>::iterator it = m_clients.begin(); it != m_clients.end(); )
    {
        if (*it == client)
        {
            it = m_clients.erase(it);
        }
        else
        {
            ++it;
        }
    }
}


/**
 * Supprime tous les clients du serveur.
 ******************************/

void CServer::clearClients()
{
    m_clients.clear();
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/CPacket.cpp
 * \date 25/05/2009 Création de la classe CPacket.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cstring>

#include "Network/CPacket.hpp"
#include "Network/Socket.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CPacket::CPacket() :
m_cursor (0),
m_valid  (true)
{ }


/**
 * Destructeur.
 ******************************/

CPacket::~CPacket()
{ }


/**
 * Append data to the end of the packet.
 *
 * \param data        Pointer to the bytes to append.
 * \param SizeInBytes Number of bytes to append.
 ******************************/

void CPacket::append(const void * data, std::size_t SizeInBytes)
{
    if (data && SizeInBytes > 0)
    {
        std::size_t start = m_data.size();
        m_data.resize(start + SizeInBytes);
        memcpy(&m_data[start], data, SizeInBytes);
    }
}


/**
 * Clear the packet data.
 ******************************/

void CPacket::clear()
{
    m_data.clear();
    m_cursor = 0;
    m_valid = true;
}


/**
 * Get a pointer to the data contained in the packet.
 *
 * \remarks The returned pointer may be invalid after you append data to the packet.
 *
 * \return Pointeur sur les données du packet.
 ******************************/

const char * CPacket::getData() const
{
    return (!m_data.empty() ? &m_data[0] : nullptr);
}


/**
 * Get the size of the data contained in the packet.
 *
 * \return Data size in bytes.
 ******************************/

std::size_t CPacket::getDataSize() const
{
    return m_data.size();
}


/**
 * Tell if the reading position has reached the end of the packet.
 *
 * \return Booléen.
 ******************************/

bool CPacket::endOfPacket() const
{
    return (m_cursor >= m_data.size());
}


/**
 * Tell if the packet is valid for reading.
 *
 * \return True if last data extraction from packet was successful.
 ******************************/

CPacket::operator bool() const
{
    return m_valid;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(bool& data)
{
    uint8_t value;

    if (*this >> value)
    {
        data = (value != 0);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(int8_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = *reinterpret_cast<const int8_t*>(getData() + m_cursor);
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(uint8_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = *reinterpret_cast<const uint8_t*>(getData() + m_cursor);
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(int16_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = ntohs (*reinterpret_cast<const int16_t*>(getData() + m_cursor));
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(uint16_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = ntohs(*reinterpret_cast<const uint16_t*>(getData() + m_cursor));
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(int32_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = ntohl(*reinterpret_cast<const int32_t*>(getData() + m_cursor));
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(uint32_t& data)
{
    if (checkSize(sizeof(data)))
    {
        data = ntohl(*reinterpret_cast<const uint32_t*>(getData() + m_cursor));
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(float& data)
{
    if (checkSize(sizeof(data)))
    {
        data = *reinterpret_cast<const float*>(getData() + m_cursor);
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(double& data)
{
    if (checkSize(sizeof(data)))
    {
        data = *reinterpret_cast<const double*>(getData() + m_cursor);
        m_cursor += sizeof(data);
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(char * data)
{
    // First extract string length
    uint32_t length;
    *this >> length;

    if (length > 0 && checkSize(length))
    {
        // Then extract characters
        memcpy(data, getData() + m_cursor, length);
        data[length] = '\0';

        // Update reading position
        m_cursor += length;
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(std::string& data)
{
    // First extract string length
    uint32_t length;
    *this >> length;

    data.clear();

    if (length > 0 && checkSize(length))
    {
        // Then extract characters
        data.assign(getData() + m_cursor, length);

        // Update reading position
        m_cursor += length;
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(wchar_t * data)
{
    // First extract string length
    uint32_t length;
    *this >> length;

    if (length > 0 && checkSize(length * sizeof(int32_t)))
    {
        // Then extract characters
        for (uint32_t i = 0; i < length; ++i)
        {
            uint32_t c;
            *this >> c;
            data[i] = static_cast<wchar_t>(c);
        }

        data[length] = L'\0';
    }

    return *this;
}


/**
 * Operator >> overloads to extract data from the packet.
 *
 * \param data Données à extraire du paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator>>(std::wstring& data)
{
    // First extract string length
    uint32_t length;
    *this >> length;

    data.clear();

    if (length > 0 && checkSize(length * sizeof(int32_t)))
    {
        // Then extract characters
        for (uint32_t i = 0; i < length; ++i)
        {
            uint32_t c;
            *this >> c;
            data += static_cast<wchar_t>(c);
        }
    }

    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(bool data)
{
    *this << static_cast<uint8_t>(data);
    return *this;
}


/**
 *  Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(int8_t data)
{
    append(&data, sizeof(data));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(uint8_t data)
{
    append(&data, sizeof(data));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(int16_t data)
{
    int16_t toWrite = htons(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(uint16_t data)
{
    uint16_t toWrite = htons(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(int32_t data)
{
    int32_t toWrite = htonl(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(uint32_t data)
{
    uint32_t toWrite = htonl(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(float data)
{
    append(&data, sizeof(data));
    return *this;
}

/**
 *  Operator << overloads to put data into the packet
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(double data)
{
    append(&data, sizeof(data));
    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(const char * data)
{
    // First insert string length
    uint32_t length = 0;

    for (const char* c = data; *c != '\0'; ++c)
    {
        ++length;
    }

    *this << length;

    // Then insert characters
    append(data, length * sizeof(char));

    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(const std::string& data)
{
    // First insert string length
    uint32_t length = static_cast<uint32_t>(data.size());
    *this << length;

    // Then insert characters
    if (length > 0)
    {
        append(data.c_str(), length * sizeof(std::string::value_type));
    }

    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<<(const wchar_t * data)
{
    // First insert string length
    uint32_t length = 0;

    for (const wchar_t* c = data; *c != L'\0'; ++c)
    {
        ++length;
    }

    *this << length;

    // Then insert characters
    for (const wchar_t * c = data; *c != L'\0'; ++c)
    {
        *this << static_cast<int32_t>(*c);
    }

    return *this;
}


/**
 * Operator << overloads to put data into the packet.
 *
 * \param data Données à ajouter au paquet.
 * \return Référence sur le paquet.
 ******************************/

CPacket& CPacket::operator<< (const std::wstring& data)
{
    // First insert string length
    uint32_t length = static_cast<uint32_t>(data.size());
    *this << length;

    // Then insert characters
    if (length > 0)
    {
        for (std::wstring::const_iterator c = data.begin(); c != data.end(); ++c)
        {
            *this << static_cast<int32_t>(*c);
        }
    }

    return *this;
}


/**
 * Check if the packet can extract a given size of bytes.
 *
 * \param size Size to check.
 * \return Booléen.
 ******************************/

bool CPacket::checkSize(std::size_t size)
{
    m_valid = m_valid && (m_cursor + size <= m_data.size());
    return m_valid;
}


/**
 * Called before the packet is sent to the network.
 *
 * \param dataSize Variable to fill with the size of data to send.
 * \return Pointer to the array of bytes to send.
 ******************************/

const char * CPacket::onSend(std::size_t& dataSize)
{
    dataSize = getDataSize();
    return getData();
}


/**
 * Called after the packet has been received from the network.
 *
 * \param data     Pointer to the array of received bytes.
 * \param dataSize Size of the array of bytes.
 ******************************/

void CPacket::onReceive(const char * data, std::size_t dataSize)
{
    append(data, dataSize);
}

} // Namespace Ted

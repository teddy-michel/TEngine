/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/CIPAddress.cpp
 * \date 25/05/2009 Création de la casse CIPAddress.
 * \date 14/03/2011 Inclusion de netdb.h sous Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "os.h"

#ifdef T_SYSTEM_LINUX
#  include <netdb.h>
#endif

#include <cstring>

#include "Network/CIPAddress.hpp"
#include "Network/HTTP.hpp"


namespace Ted
{

/// Static member
const CIPAddress CIPAddress::LocalHost("127.0.0.1");


/**
 * Constructeur par défaut.
 ******************************/

CIPAddress::CIPAddress() :
m_addr (INADDR_NONE)
{ }


/**
 * Construct the address from a string.
 *
 * \param addr IP address ("xxx.xxx.xxx.xxx") or network name.
 ******************************/

CIPAddress::CIPAddress(const CString& addr)
{
    // First try to convert it as a byte representation ("xxx.xxx.xxx.xxx")
    m_addr = inet_addr(addr.toCharArray());

    // If not successful, try to convert it as a host name
    if (!isValid())
    {
        hostent * host = gethostbyname(addr.toCharArray());

        if (host)
        {
            // Host found, extract its IP address
            m_addr = reinterpret_cast<in_addr *>(host->h_addr)->s_addr;
        }
        else
        {
            // Host name not found on the network
            m_addr = INADDR_NONE;
        }
    }
}


/**
 * Construct the address from a C-style string.
 * Needed for implicit conversions from literal strings to IPAddress to work.
 *
 * \param addr IP address ("xxx.xxx.xxx.xxx") or network name.
 ******************************/

CIPAddress::CIPAddress(const char * addr)
{
    // First try to convert it as a byte representation ("xxx.xxx.xxx.xxx")
    m_addr = inet_addr(addr);

    // If not successful, try to convert it as a host name
    if (!isValid())
    {
        hostent * host = gethostbyname(addr);

        if (host)
        {
            // Host found, extract its IP address
            m_addr = reinterpret_cast<in_addr *>(host->h_addr)->s_addr;
        }
        else
        {
            // Host name not found on the network
            m_addr = INADDR_NONE;
        }
    }
}


/**
 * Construct the address from 4 bytes.
 *
 * \param byte0 First byte of the address.
 * \param byte1 Second byte of the address.
 * \param byte2 Third byte of the address.
 * \param byte3 Fourth byte of the address.
 ******************************/

CIPAddress::CIPAddress(unsigned char byte0, unsigned char byte1, unsigned char byte2, unsigned char byte3)
{
    m_addr = htonl((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | byte3);
}


/**
 * Construct the address from a 32-bits integer.
 *
 * \param addr 4 bytes of the address packed into a 32-bits integer.
 ******************************/

CIPAddress::CIPAddress(uint32_t addr)
{
    m_addr = htonl(addr);
}


/**
 * Tell if the address is a valid one.
 *
 * \return Booléen.
 ******************************/

bool CIPAddress::isValid() const
{
    return m_addr != INADDR_NONE;
}


/**
 *  Get a string representation of the address.
 *
 * \return String representation of the IP address ("xxx.xxx.xxx.xxx").
 ******************************/

CString CIPAddress::toString() const
{
    in_addr InAddr;
    InAddr.s_addr = m_addr;

    return inet_ntoa(InAddr);
}


/**
 * Get an integer representation of the address.
 *
 * \return 32-bits integer containing the 4 bytes of the address, in system endianness.
 ******************************/

uint32_t CIPAddress::toInteger() const
{
    return ntohl(m_addr);
}


/**
 * Get the computer's local IP address (from the LAN point of view).
 *
 * \return Local IP address.
 ******************************/

CIPAddress CIPAddress::getLocalAddress()
{
    // The method here is to connect a UDP socket to anyone (here to localhost),
    // and get the local socket address with the getsockname function.
    // UDP connection will not send anything to the network, so this function won't cause any overhead

    CIPAddress LocalAddress;

    // Create the socket
    SOCKET Socket = socket(PF_INET , SOCK_DGRAM , 0 );

    if (Socket == INVALID_SOCKET)
    {
        return LocalAddress;
    }

    // Build the host address (use a random port)
    sockaddr_in SockAddr;
    memset(SockAddr.sin_zero, 0, sizeof(SockAddr.sin_zero));
    SockAddr.sin_addr.s_addr = INADDR_LOOPBACK;
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(4567);

    // Connect the socket
    if (connect(Socket, reinterpret_cast<sockaddr *>(&SockAddr), sizeof(SockAddr)) == -1)
    {
#if defined T_SYSTEM_WINDOWS
        closesocket(Socket);
#elif defined T_SYSTEM_LINUX
        close(Socket);
#endif
        return LocalAddress;
    }

    // Get the local address of the socket connection
#if defined T_SYSTEM_WINDOWS
    int size = sizeof(SockAddr);
#elif defined T_SYSTEM_LINUX
    socklen_t size = sizeof(SockAddr);
#endif

    if (getsockname(Socket, reinterpret_cast<sockaddr *>(&SockAddr), &size) == -1)
    {
#if defined T_SYSTEM_WINDOWS
        closesocket(Socket);
#elif defined T_SYSTEM_LINUX
        close(Socket);
#endif
        return LocalAddress;
    }

    // Close the socket
#if defined T_SYSTEM_WINDOWS
    closesocket(Socket);
#elif defined T_SYSTEM_LINUX
    close(Socket);
#endif

    // Finally build the IP address
    LocalAddress.m_addr = SockAddr.sin_addr.s_addr;

    return LocalAddress;
}


/**
 * Get the computer's public IP address (from the web point of view).
 * The only way to get a public address is to ask it to a distant website ; as
 * a consequence, this function may be very slow : use it as few as possible !
 *
 * \return Public IP address.
 ******************************/

CIPAddress CIPAddress::getPublicAddress()
{
    // The trick here is more complicated, because the only way
    // to get our public IP address is to get it from a distant computer.
    // Here we get the web page from http://www.whatismyip.org
    // and parse the result to extract our IP address
    // (not very hard : the web page contains only our IP address)

    CIPAddress PublicAddress;

    // Connect to the web server and get its index page
    HTTP Server("www.whatismyip.org");
    HTTP::Request Request(HTTP::Request::Get, "/");
    HTTP::Response Page = Server.SendRequest (Request);

    // If the request was successful, we can extract
    // the address from the body of the web page
    if (Page.getStatus() == HTTP::Response::OK)
    {
        PublicAddress = Page.getBody();
    }

    return PublicAddress;
}


/**
 * Comparison operator ==.
 *
 * \param addr Address to compare.
 * \return True if *this == Other.
 ******************************/

bool CIPAddress::operator==(const CIPAddress& addr) const
{
    return m_addr == addr.m_addr;
}


/**
 * Comparison operator !=.
 *
 * \param addr Address to compare.
 * \return True if *this != Other.
 ******************************/

bool CIPAddress::operator!=(const CIPAddress& addr) const
{
    return m_addr != addr.m_addr;
}


/**
 * Comparison operator <.
 *
 * \param addr Address to compare.
 * \return True if *this < Other.
 ******************************/

bool CIPAddress::operator<(const CIPAddress& addr) const
{
    return m_addr < addr.m_addr;
}


/**
 * Comparison operator >.
 *
 * \param addr Address to compare.
 * \return True if *this > Other.
 ******************************/

bool CIPAddress::operator>(const CIPAddress& addr) const
{
    return m_addr > addr.m_addr;
}


/**
 * Comparison operator <=.
 *
 * \param addr Address to compare.
 * \return True if *this <= Other.
 ******************************/

bool CIPAddress::operator<=(const CIPAddress& addr) const
{
    return m_addr <= addr.m_addr;
}


/**
 * Comparison operator >=.
 *
 * \param addr Address to compare.
 * \return True if *this >= Other.
 ******************************/

bool CIPAddress::operator>=(const CIPAddress& addr) const
{
    return m_addr >= addr.m_addr;
}


/**
 * Operator >> overload to extract an address from an input stream.
 *
 * \relates CIPAddress
 *
 * \param stream Input stream.
 * \param addr   Address to extract.
 * \return Reference to the input stream.
 ******************************/

std::istream& operator>>(std::istream& stream, CIPAddress& addr)
{
    std::string str;
    stream >> str;
    addr = CIPAddress(str.c_str());

    return stream;
}


/**
 * Operator << overload to print an address to an output stream.
 *
 * \relates CIPAddress
 *
 * \param stream Output stream.
 * \param addr   Address to print.
 * \return Reference to the output stream.
 ******************************/

std::ostream& operator<<(std::ostream& stream, const CIPAddress& addr)
{
    return stream << addr.toString();
}

} // Namespace Ted

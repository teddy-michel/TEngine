/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Network/HTTP.cpp
 * \date 25/05/2009 Création de la classe HTTP.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <ctype.h>
#include <sstream>

#include "Network/HTTP.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut
 *
 * \param method Method to use for the request (Get by default)
 * \param URI    Target URI ("/" by default -- index page)
 * \param body   Content of the request's body (empty by default)
 ******************************/

HTTP::Request::Request(TMethod method, const CString& URI, const CString& body) :
m_method        (method),
m_URI           (URI),
m_major_version (1),
m_minor_version (0),
m_body          (body)
{ }


/**
 * Set the value of a field; the field is added if it doesn't exist
 *
 * \param field Name of the field to set (case-insensitive)
 * \param value Value of the field
 ******************************/

void HTTP::Request::setField(const CString& field, const CString& value)
{
    m_fields[field.toLowerCase()] = value;
}


/**
 * Set the request method.
 * This parameter is Get by default
 *
 * \param method Method to use for the request
 ******************************/

void HTTP::Request::setMethod(HTTP::Request::TMethod method)
{
    m_method = method;
}


/**
 * Set the target URI of the request.
 * This parameter is "/" by default
 *
 * \param URI URI to request, local to the host
 ******************************/

void HTTP::Request::setURI(const CString& URI)
{
    m_URI = URI;

    // Make sure it starts with a '/'
    if (m_URI.isEmpty() || m_URI[0] != '/')
    {
        m_URI = "/" + m_URI;
    }
}


/**
 * Set the HTTP version of the request.
 * This parameter is 1.0 by default
 *
 * \param major Major version number
 * \param minor Minor version number
 ******************************/

void HTTP::Request::setHTTPVersion(unsigned int major, unsigned int minor)
{
    m_major_version = major;
    m_minor_version = minor;
}


/**
 * Set the body of the request. This parameter is optional and
 * makes sense only for POST requests.
 * This parameter is empty by default
 *
 * \param body Content of the request body
 ******************************/

void HTTP::Request::setBody(const CString& body)
{
    m_body = body;
}


/**
 * Get the string representation of a request header
 *
 * \return String containing the request
 ******************************/

CString HTTP::Request::toString() const
{
    std::ostringstream out;

    // Convert the method to its string representation
    CString requestMethod;

    switch (m_method)
    {
        default:
        case Get:  requestMethod = "GET";  break;
        case Post: requestMethod = "POST"; break;
        case Head: requestMethod = "HEAD"; break;
    }

    // Write the first line containing the request type
    out << requestMethod << " " << m_URI << " ";
    out << "HTTP/" << m_major_version << "." << m_minor_version << "\r\n";

    // Write fields
    for (std::map<CString, CString>::const_iterator i = m_fields.begin() ; i != m_fields.end() ; ++i)
    {
        out << i->first << ": " << i->second << "\r\n";
    }

    // Use an extra \r\n to separate the header from the body
    out << "\r\n";

    // Add the body
    out << m_body;

    return out.str().c_str();
}


/**
 * Check if the given field has been defined
 *
 * \param field Name of the field to check (case-insensitive)
 * \return Booléen
 ******************************/

bool HTTP::Request::hasField(const CString& field) const
{
    return m_fields.find(field) != m_fields.end();
}


/**
 * Constructeur par défaut
 ******************************/

HTTP::Response::Response() :
m_status        (ConnectionFailed),
m_major_version (0),
m_minor_version (0)
{ }


/**
 * Get the value of a field
 *
 * \param field Field
 * \return Value of the field
 ******************************/

const CString& HTTP::Response::getField(const CString& field) const
{
    std::map<CString, CString>::const_iterator it = m_fields.find(field);

    if (it != m_fields.end())
    {
        return it->second;
    }
    else
    {
        static const CString Empty = CString();
        return Empty;
    }
}


/**
 * Get the header's status code
 *
 * \return Status code
 ******************************/

HTTP::Response::Status HTTP::Response::getStatus() const
{
    return m_status;
}


/**
 * Get the major HTTP version number of the response
 *
 * \return Major HTTP version number
 ******************************/

unsigned int HTTP::Response::getMajorHTTPVersion() const
{
    return m_major_version;
}


/**
 * Get the minor HTTP version number of the response
 *
 * \return Minor HTTP version number
 ******************************/

unsigned int HTTP::Response::getMinorHTTPVersion () const
{
    return m_minor_version;
}


/**
 * Get the body of the response. The body can contain :
 *  - the requested page (for GET requests)
 *  - a response from the server (for POST requests)
 *  - nothing (for HEAD requests)
 *  - an error message (in case of an error)
 *
 * \return Body of the response
 ******************************/

const CString& HTTP::Response::getBody() const
{
    return m_body;
}


/**
 * Construct the header from a response string
 *
 * \param data Response string
 ******************************/

void HTTP::Response::FromString(const CString& data)
{
    std::istringstream in(data.toCharArray());

    // Extract the HTTP version from the first line
    std::string version;

    if (in >> version)
    {
        if (version.size() >= 8 && version[6] == '.' &&
            toLower(version.substr(0, 5)) == "http/" &&
            isdigit(version[5]) && isdigit(version[7]))
        {
            m_major_version = version[5] - '0';
            m_minor_version = version[7] - '0';
        }
        else
        {
            // Invalid HTTP version
            m_status = InvalidResponse;
            return;
        }
    }

    // Extract the status code from the first line
    int statusCode;

    if (in >> statusCode)
    {
        m_status = static_cast<Status>(statusCode);
    }
    else
    {
        // Invalid status code
        m_status = InvalidResponse;
        return;
    }

    // Ignore the end of the first line
    in.ignore(10000, '\n');

    // Parse the other lines, which contain fields, one by one
    std::string line;

    while (std::getline(in, line) && (line.size() > 2))
    {
        CString lineStr = line.c_str();
        std::ptrdiff_t pos = lineStr.indexOf(": ");

        if (pos != -1)
        {
            // Extract the field name and its value
            CString field = lineStr.subString(0, pos);
            CString value = lineStr.subString(pos + 2);

            // Remove any trailing \r
            if (value.endsWith('\r'))
            {
                value.resize(value.getSize() - 1);
            }

            // Add the field
            m_fields[field.toLowerCase()] = value;
        }
    }

    // Finally extract the body
    m_body.clear();

    while (std::getline(in, line))
    {
        m_body += CString(line.c_str()) + "\n";
    }
}


/**
 * Constructeur par défaut.
 ******************************/

HTTP::HTTP() :
m_host (),
m_port (0)
{ }


/**
 * Construct the HTTP instance with the target host.
 *
 * \param host Host.
 * \param port Port.
 ******************************/

HTTP::HTTP(const CString& host, uint16_t port)
{
    setHost(host, port);
}


/**
 * Set the target host.
 *
 * \param host Host.
 * \param port Port.
 ******************************/

void HTTP::setHost(const CString& host, uint16_t port)
{
    // Detect the protocol used
    CString protocol = host.subString(0, 8).toLowerCase();

    if (protocol.subString(0, 7) == "http://")
    {
        // HTTP protocol
        m_host_name = host.subString(7);
        m_port = (port != 0 ? port : 80);
    }
    else if (protocol == "https://")
    {
        // HTTPS protocol
        m_host_name = host.subString(8);
        m_port = (port != 0 ? port : 443);
    }
    else
    {
        // Undefined protocol - use HTTP
        m_host_name = host;
        m_port = (port != 0 ? port : 80);
    }

    // Remove any trailing '/' from the host name
    if (m_host_name.endsWith('/'))
    {
        m_host_name.resize(m_host_name.getSize() - 1);
    }

    m_host = CIPAddress(m_host_name);
}


/**
 * Send a HTTP request and return the server's response.
 * You must be connected to a host before sending requests.
 * Any missing mandatory header field will be added with an appropriate value.
 *
 * \warning This function waits for the server's response and may not return
 *          instantly; use a thread if you don't want to block your application.
 *
 * \param req Requête.
 * \return Réponse HTTP.
 ******************************/

HTTP::Response HTTP::SendRequest(const HTTP::Request& req)
{
    // First make sure the request is valid -- add missing mandatory fields
    Request ToSend(req);

    if (!ToSend.hasField("From"))
    {
        ToSend.setField("From", "user@server.org");
    }
    if (!ToSend.hasField("User-Agent"))
    {
        ToSend.setField("User-Agent", "ted_engine/0.1");
    }
    if (!ToSend.hasField("Host"))
    {
        ToSend.setField("Host", m_host_name);
    }
    if (!ToSend.hasField("Content-Length"))
    {
        std::ostringstream out;
        out << ToSend.m_body.getSize();
        ToSend.setField("Content-Length", out.str().c_str());
    }

    // Prepare the response
    Response received;

    // Connect the socket to the host
    if (m_connection.connect(m_port, m_host) == Socket::Done)
    {
        // Convert the request to string and send it through the connected socket
        CString RequestStr = ToSend.toString();

        if (!RequestStr.isEmpty())
        {
            // Send it through the socket
            if (m_connection.send(RequestStr.toCharArray(), RequestStr.getSize()) == Socket::Done)
            {
                // Wait for the server's response
                CString receivedStr;
                std::size_t size = 0;
                char buffer[1024 + 1];

                while (m_connection.receive(buffer, 1024, size) == Socket::Done)
                {
                    buffer[size] = '\0';
                    receivedStr += CString(buffer);
                }

                // Build the Response object from the received data
                received.FromString(receivedStr);
            }
        }

        // Close the connection
        m_connection.close();
    }

    return received;
}

} // Namespace Ted

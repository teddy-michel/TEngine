/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Network/CSocketTCP.cpp
 * \date       2008 Création de la classe CSocketTCP.
 * \date 23/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 14/03/2011 Inclusion de trois fichiers pour Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>
#include <iostream>

#include "os.h"

#if defined(T_SYSTEM_LINUX)
#  include <fcntl.h>
#  include <cstring>
#  include <netinet/tcp.h>
#endif

#include "Network/CIPAddress.hpp"
#include "Network/CPacket.hpp"
#include "Network/CSocketTCP.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Default constructor.
 ******************************/

CSocketTCP::CSocketTCP() :
m_socket              (INVALID_SOCKET),
m_pending_packet_size (0),
m_blocking            (true)
{
    create();
}


/**
 * Change the blocking state of the socket
 ******************************/

void CSocketTCP::setBlocking(bool blocking)
{
    // Make sure our socket is valid
    if (!isValid())
    {
        create();
    }

#if defined T_SYSTEM_WINDOWS

    unsigned long block = blocking ? 0 : 1;
    ioctlsocket(m_socket, FIONBIO, &block);

#elif defined T_SYSTEM_LINUX

    int status = fcntl(m_socket, F_GETFL);

    if (blocking)
    {
        fcntl(m_socket, F_SETFL, status & ~O_NONBLOCK);
    }
    else
    {
        fcntl(m_socket, F_SETFL, status | O_NONBLOCK);
    }

#endif

    m_blocking = blocking;
}


/**
 * Connect to another computer on a specified port
 */

Socket::Status CSocketTCP::connect(uint16_t port, const CIPAddress& host, float timeout)
{
    // Make sure our socket is valid
    if (!isValid())
    {
        create();
    }

    // Build the host address
    sockaddr_in SockAddr;
    memset(SockAddr.sin_zero, 0, sizeof(SockAddr.sin_zero));
    SockAddr.sin_addr.s_addr = inet_addr(host.toString().toCharArray());
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(port);

    if (timeout <= 0)
    {
        // ----- We're not using a timeout : just try to connect -----

        if (::connect(m_socket, reinterpret_cast<sockaddr *>(&SockAddr), sizeof(SockAddr)) == -1)
        {
            // Failed to connect
            return Socket::getErrorStatus();
        }

        // Connection succeeded
        return Socket::Done;
    }
    else
    {
        // ----- We're using a timeout : we'll need a few tricks to make it work -----

        // Save the previous blocking state
        bool IsBlocking = m_blocking;

        // Switch to non-blocking to enable our connection timeout
        if (IsBlocking)
        {
            setBlocking(false);
        }

        // Try to connect to host
        if (::connect(m_socket, reinterpret_cast<sockaddr *>(&SockAddr), sizeof(SockAddr)) >= 0)
        {
            // We got instantly connected! (it may no happen a lot...)
            return Socket::Done;
        }

        // Get the error status
        Socket::Status status = Socket::getErrorStatus();

        // If we were in non-blocking mode, return immediatly
        if (!IsBlocking)
        {
            return status;
        }

        // Otherwise, wait until something happens to our socket (success, timeout or error)
        if (status == Socket::NotReady)
        {
            // Setup the selector
            fd_set Selector;
            FD_ZERO(&Selector);
            FD_SET(m_socket, &Selector);

            // Setup the timeout
            timeval Time;
            Time.tv_sec  = static_cast<long>(timeout);
            Time.tv_usec = (static_cast<long>(timeout * 1000) % 1000) * 1000;

            // Wait for something to write on our socket (would mean the connection has been accepted)
            if (select(static_cast<int>(m_socket + 1), nullptr, &Selector, nullptr, &Time) > 0)
            {
                // Connection succeeded
                status = Socket::Done;
            }
            else
            {
                // Failed to connect before timeout is over
                status = Socket::getErrorStatus();
            }
        }

        // Switch back to blocking mode
        setBlocking(true);

        return status;
    }
}


/**
 * Listen to a specified port for incoming data or connections.
 ******************************/

bool CSocketTCP::listen(uint16_t port)
{
    // Make sure our socket is valid
    if (!isValid())
        create();

    // Build the address
    sockaddr_in SockAddr;
    memset(SockAddr.sin_zero, 0, sizeof(SockAddr.sin_zero));
    SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(port);

    // Bind the socket to the specified port
    if (bind(m_socket, reinterpret_cast<sockaddr *>(&SockAddr), sizeof(SockAddr)) == -1)
    {
        CApplication::getApp()->log(CString("Failed to bind socket to port %1").arg(port), ILogger::Error);
        return false;
    }

    // Listen to the bound port
    if (::listen(m_socket, 0) == -1)
    {
        CApplication::getApp()->log(CString("Failed to listen to port %1").arg(port), ILogger::Error);
        return false;
    }

    return true;
}


/**
 * Wait for a connection (must be listening to a port).
 * This function will block if the socket is blocking
 */

Socket::Status CSocketTCP::accept(CSocketTCP& connected, CIPAddress * addr)
{
    // Address that will be filled with client informations
    sockaddr_in ClientAddress;

#if defined T_SYSTEM_WINDOWS
    int length = sizeof(ClientAddress);
#elif defined T_SYSTEM_LINUX
    socklen_t length = sizeof(ClientAddress);
#endif

    // Accept a new connection
    connected = ::accept(m_socket, reinterpret_cast<sockaddr *>(&ClientAddress), &length);

    // Check errors
    if (!connected.isValid())
    {
        if (addr)
        {
            *addr = CIPAddress();
        }

        return Socket::getErrorStatus();
    }

    // Fill address if requested
    if (addr)
    {
        *addr = CIPAddress(inet_ntoa(ClientAddress.sin_addr));
    }

    return Socket::Done;
}


/**
 * Send an array of bytes to the host (must be connected first).
 ******************************/

Socket::Status CSocketTCP::send(const char * data, std::size_t size)
{
    // First check that socket is valid
    if (!isValid())
    {
        return Socket::Error;
    }

    // Check parameters
    if (data && size)
    {
        // Loop until every byte has been sent
        int sent = 0;
        int sizeToSend = static_cast<int>(size);

        for (int length = 0; length < sizeToSend; length += sent)
        {
            // Send a chunk of data
            sent = ::send(m_socket, data + length, sizeToSend - length, 0);

            // Check if an error occured
            if (sent <= 0)
            {
                return Socket::getErrorStatus();
            }
        }

        return Socket::Done;
    }
    else
    {
        CApplication::getApp()->log("Cannot send data over the network (invalid parameters).", ILogger::Error);
        return Socket::Error;
    }
}


/**
 * Receive an array of bytes from the host (must be connected first).
 * This function will block if the socket is blocking.
 ******************************/

Socket::Status CSocketTCP::receive(char * data, std::size_t max_size, std::size_t& size_received)
{
    // First clear the size received
    size_received = 0;

    // Check that socket is valid
    if (!isValid())
    {
        return Socket::Error;
    }

    // Check parameters
    if (data && max_size)
    {
        // Receive a chunk of bytes
        int received = recv(m_socket, data, static_cast<int>(max_size), 0);

        // Check the number of bytes received
        if (received > 0)
        {
            size_received = static_cast<std::size_t>(received);
            return Socket::Done;
        }
        else if (received == 0)
        {
            return Socket::Disconnected;
        }
        else
        {
            return Socket::getErrorStatus();
        }
    }
    else
    {
        CApplication::getApp()->log("Cannot receive data from the network (invalid parameters).", ILogger::Error);
        return Socket::Error;
    }
}


/**
 * Send a packet of data to the host (must be connected first).
 ******************************/

Socket::Status CSocketTCP::send(CPacket& packet)
{
    // Get the data to send from the packet
    std::size_t dataSize = 0;
    const char * data = packet.onSend(dataSize);

    // Send the packet size
    uint32_t packetSize = htonl(static_cast<unsigned long>(dataSize));
    send(reinterpret_cast<const char *>(&packetSize), sizeof(packetSize));

    // Send the packet data
    if (packetSize > 0)
    {
        return send(data, dataSize);
    }
    else
    {
        return Socket::Done;
    }
}

/**
 * Receive a packet from the host (must be connected first).
 * This function will block if the socket is blocking.
 */

Socket::Status CSocketTCP::receive(CPacket& packet)
{
    // We start by getting the size of the incoming packet
    uint32_t packetSize = 0;
    std::size_t received = 0;

    if (m_pending_packet_size < 0)
    {
        Socket::Status status = receive(reinterpret_cast<char *>(&packetSize), sizeof(packetSize), received);

        if (status != Socket::Done)
        {
            return status;
        }

        packetSize = ntohl(packetSize);
    }
    else
    {
        // There is a pending packet : we already know its size
        packetSize = m_pending_packet_size;
    }

    // Then loop until we receive all the packet data
    char Buffer[1024];

    while (m_pending_packet.size() < packetSize)
    {

#if defined(T_SYSTEM_WINDOWS) && defined(min)
#   undef min
#endif

        // Receive a chunk of data
        std::size_t SizeToGet = std::min(static_cast<std::size_t>(packetSize - m_pending_packet.size()), sizeof(Buffer));
        Socket::Status status = receive(Buffer, SizeToGet, received);

        if (status != Socket::Done)
        {
            // We must save the size of the pending packet until we can receive its content
            if (status == Socket::NotReady)
            {
                m_pending_packet_size = packetSize;
            }

            return status;
        }

        // Append it into the packet
        if (received > 0)
        {
            m_pending_packet.resize(m_pending_packet.size() + received);
            char * Begin = &m_pending_packet[0] + m_pending_packet.size() - received;
            memcpy(Begin, Buffer, received);
        }
    }

    // We have received all the datas : we can copy it to the user packet, and clear our internal packet
    packet.clear();

    if (!m_pending_packet.empty())
    {
        packet.onReceive(&m_pending_packet[0], m_pending_packet.size());
    }

    m_pending_packet.clear();
    m_pending_packet_size = -1;

    return Socket::Done;
}


/**
 * Close the socket.
 */

bool CSocketTCP::close()
{
    if (isValid())
    {
#if defined T_SYSTEM_WINDOWS
        if (closesocket(m_socket) == -1)
#elif defined T_SYSTEM_LINUX
        if (close(m_socket) == -1)
#endif
        {
            return false;
        }

        m_socket = INVALID_SOCKET;
    }

    m_blocking = true;

    return true;
}


/**
 * Check if the socket is in a valid state ; this function
 * can be called any time to check if the socket is OK
 */

bool CSocketTCP::isValid() const
{
    return (m_socket != INVALID_SOCKET);
}


/**
 * Comparison operator ==
 */

bool CSocketTCP::operator==(const CSocketTCP& socket) const
{
    return (m_socket == socket.m_socket);
}


/**
 * Comparison operator !=
 */

bool CSocketTCP::operator!=(const CSocketTCP& socket) const
{
    return (m_socket != socket.m_socket);
}


/**
 * Comparison operator <.
 * Provided for compatibility with standard containers, as
 * comparing two sockets doesn't make much sense...
 */

bool CSocketTCP::operator<(const CSocketTCP& socket) const
{
    return (m_socket < socket.m_socket);
}


/**
 * Construct the socket from a socket descriptor (for internal use only).
 */

CSocketTCP::CSocketTCP(SOCKET sock)
{
    create(sock);
}


/**
 * Create the socket.
 */

void CSocketTCP::create(SOCKET sock)
{
    // Use the given socket descriptor, or get a new one
    m_socket = (sock ? sock : socket(PF_INET, SOCK_STREAM, 0));
    m_blocking = true;

    // Reset the pending packet
    m_pending_packet.clear();
    m_pending_packet_size = -1;

    // Setup default options
    if (isValid())
    {
        // To avoid the "Address already in use" error message when trying to bind to the same port
        int yes = 1;

        if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1)
        {
            CApplication::getApp()->log("Failed to set socket option \"SO_REUSEADDR\" ; binding to a same port may fail if too fast.", ILogger::Warning);
        }

        // Disable the Nagle algorithm (ie. removes buffering of TCP packets)
        if (setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1)
        {
            CApplication::getApp()->log("Failed to set socket option \"TCP_NODELAY\" ; all your TCP packets will be buffered.", ILogger::Warning);
        }

        // Set blocking by default (should always be the case anyway)
        setBlocking(true);
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILoaderArchive.cpp
 * \date 12/01/2010 Création de la classe ILoaderArchive.
 * \date 02/03/2011 Ajout des méthodes statiques getArchiveLoader et AddArchiveLoader.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/ILoaderArchive.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"

#ifdef T_DEBUG
#  include "Core/ILogger.hpp"
#endif


namespace Ted
{

std::list<ILoaderArchive::CreateInstanceType> ILoaderArchive::s_loaders;


/**
 * Constructeur par défaut.
 ******************************/

ILoaderArchive::ILoaderArchive() :
ILoader()
{ }


/**
 * Destructeur.
 ******************************/

ILoaderArchive::~ILoaderArchive()
{ }


/**
 * Donne le nombre de fichiers de l'archive.
 *
 * \return Nombre de fichiers.
 ******************************/

unsigned int ILoaderArchive::getNumFiles() const
{
    return m_files.size();
}


/**
 * Donne la liste des noms de fichiers de l'archive.
 *
 * \return Liste des fichiers.
 ******************************/

std::vector<CString> ILoaderArchive::getFilesList()
{
    return m_files;
}


/**
 * Indique si un fichier est présent dans l'archive.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen.
 ******************************/

bool ILoaderArchive::isFileExists(const CString& fileName)
{
    CString name = CFileSystem::convertName(fileName);

    // On met la chaine en minuscules
#ifdef T_SYSTEM_WINDOWS
    name = name.toLowerCase();
#endif

    // On cherche le fichier dans la liste des fichiers
    for (std::vector<CString>::iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        // On a trouvé le fichier
#ifdef T_SYSTEM_WINDOWS
        if (it->toLowerCase() == name)
#else
        if (*it == name)
#endif
        {
            return true;
        }
    }

    return false;
}


#ifdef T_DEBUG

/**
 * Log la liste des fichiers de l'archive.
 ******************************/

void ILoaderArchive::Debug_LogFiles() const
{
    CApplication * app = CApplication::getApp();

    app->log(CString("\nListe des fichiers de l'archive %1 :").arg(m_fileName), ILogger::Debug);

    for (std::vector<CString>::const_iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        app->log(CString(" - %1").arg(*it), ILogger::Debug);
    }

    app->log("\n", ILogger::Debug);
}

#endif


/**
 * Instancie un chargeur d'archives à partir d'un fichier à charger, et charge
 * le fichier demandé si possible.
 *
 * \param fileName Adresse du fichier à charger.
 * \return Pointeur sur l'instance du chargeur créée, ou un pointeur invalide si le fichier ne
 *         peut pas être chargé.
 ******************************/

ILoaderArchive * ILoaderArchive::getArchiveLoader(const CString& fileName)
{
    // On parcourt la liste des chargeurs
    for (std::list<CreateInstanceType>::iterator it = s_loaders.begin(); it != s_loaders.end(); ++it)
    {
        ILoaderArchive * loader = (*it) (fileName.toCharArray());

        if (loader)
        {
            loader->loadFromFile(fileName);
            return loader;
        }
    }

    // Aucun chargeur ne prend en charge ce type de fichier
    return nullptr;
}


/**
 * Ajoute un chargeur d'archives.
 *
 * \param loader Chargeur à ajouter.
 ******************************/

void ILoaderArchive::addArchiveLoader(CreateInstanceType loader)
{
    s_loaders.insert(s_loaders.end(), loader);
}

} // Namespace Ted

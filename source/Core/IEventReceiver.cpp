/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IEventReceiver.cpp
 * \date 11/04/2009 Création de la classe IEventReceiver.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 06/04/2013 Ajout de la méthode onEvent avec un évènement de type CResizeEvent.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/IEventReceiver.hpp"
#include "Core/Events.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

IEventReceiver::IEventReceiver()
{ }


/**
 * Destructeur.
 ******************************/

IEventReceiver::~IEventReceiver()
{ }


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void IEventReceiver::onEvent(const CMouseEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void IEventReceiver::onEvent(const CTextEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void IEventReceiver::onEvent(const CKeyboardEvent& event)
{
    T_UNUSED(event);
}


/**
 * Gestion des évènements de redimensionnement de la fenêtre.
 *
 * \param event Évènement.
 ******************************/

void IEventReceiver::onEvent(const CResizeEvent& event)
{
    T_UNUSED(event);
}

} // Namespace Ted

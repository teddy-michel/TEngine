/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CTreeNode.cpp
 * \date 26/02/2010 Création de la classe CTreeNode.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Core/CTreeNode.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param text Texte du nœud.
 ******************************/

CTreeNode::CTreeNode(const CString& text) :
m_text       (text),
m_expanded   (false),
m_expandable (false)
{ }


/**
 * Appelle le destructeur des nœuds enfants.
 ******************************/

CTreeNode::~CTreeNode()
{
    ClearChildren();
}


/**
 * Donne le texte du nœud.
 *
 * \return Texte du nœud.
 *
 * \sa CTreeNode::setText
 ******************************/

CString CTreeNode::getText() const
{
    return m_text;
}


/**
 * Indique si le nœud est développé.
 *
 * \return Booléen.
 *
 * \sa CTreeNode::setExpanded
 ******************************/

bool CTreeNode::isExpanded() const
{
    return m_expanded;
}


/**
 * Indique si le nœud peut être développé.
 *
 * \return Booléen.
 *
 * \sa CTreeNode::setExpandable
 ******************************/

bool CTreeNode::isExpandable() const
{
    return m_expandable;
}


/**
 * Donne le nombre de nœuds enfants.
 *
 * \return Nombre de nœuds enfants.
 ******************************/

unsigned int CTreeNode::getNumChildren() const
{
    return m_children.size();
}


/**
 * Modifie la valeur du texte du nœud.
 *
 * \param text Texte du nœud.
 *
 * \sa CTreeNode::getText
 ******************************/

void CTreeNode::setText(const CString& text)
{
    m_text = text;
}


/**
 * Développe ou réduit le nœud.
 *
 * \param expanded Indique si les éléments enfants sont visibles.
 *
 * \sa CTreeNode::isExpanded
 * \sa CTreeNode::setExpandable
 ******************************/

void CTreeNode::setExpanded(bool expanded)
{
    if (m_expandable)
        m_expanded = expanded;
}


/**
 * Rend le nœud développable.
 *
 * \param expandable Indique si le nœud peut être développé.
 *
 * \sa CTreeNode::isExpandable
 * \sa CTreeNode::setExpanded
 ******************************/

void CTreeNode::setExpandable(bool expandable)
{
    m_expandable = expandable;
}


/**
 * Ajoute un nœud à la liste des enfants.
 *
 * \param child Pointeur sur un nœud.
 ******************************/

void CTreeNode::AddChild(CTreeNode * child)
{
    if (child == nullptr)
        return;

    std::list<CTreeNode *>::iterator it = std::find(m_children.begin(), m_children.end(), child);
    if (it == m_children.end())
        m_children.push_back(child);
}


/**
 * Ajoute un nœud à la liste des enfants.
 *
 * \param text Texte du nœud enfant.
 ******************************/

void CTreeNode::AddChild(const CString& text)
{
    CTreeNode * node = new CTreeNode(text);
    m_children.push_back(node);
}


/**
 * Enlève un nœud de la liste des enfants.
 *
 * \param child Pointeur sur un nœud.
 ******************************/

void CTreeNode::RemoveChild(CTreeNode * child)
{
    if (child == nullptr)
        return;

    std::list<CTreeNode *>::iterator it = std::find(m_children.begin(), m_children.end(), child);

    if (it != m_children.end())
    {
        m_children.erase(it);
    }
}


/**
 * Supprime tous les nœuds de la liste des enfants.
 ******************************/

void CTreeNode::ClearChildren()
{
    for (std::list<CTreeNode *>::iterator it = m_children.begin() ; it != m_children.end() ; ++it)
    {
        delete *it;
    }

    m_children.clear();
}

} // Namespace Ted

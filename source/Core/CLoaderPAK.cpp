/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderPAK.cpp
 * \date 30/12/2009 Création de la classe CLoaderPAK.
 * \date 08/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CLoaderPAK.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderPAK::CLoaderPAK() :
ILoaderArchive ()
{ }


/**
 * Donne le contenu d'un fichier.
 *
 * \param fileName Nom du fichier.
 * \param data     Tableau contenant les données du fichier.
 * \param size     Taille du tableau (0 si le fichier n'existe pas).
 * \return Booléen indiquant si le fichier a pu être chargé.
 ******************************/

bool CLoaderPAK::getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size)
{
    *size = 0;

    if (!m_file)
    {
        data.clear();
        return false;
    }

    CString name = CFileSystem::convertName(fileName);

    std::size_t filenum = 0;

    // On met la chaine en minuscules
#if defined (T_SYSTEM_WINDOWS)
    name = name.toLowerCase();
#endif

    // On cherche le fichier dans la liste des fichiers
    const std::size_t num_files = m_files.size();

    for (std::size_t i = 0; i < num_files; ++i)
    {
        // On a trouvé le fichier
#if defined (T_SYSTEM_WINDOWS)
        if (m_files[i].toLowerCase() == name)
#else
        if (m_files[i] == name)
#endif
        {
            filenum = i + 1;
            break;
        }
    }

    // Le fichier n'existe pas
    if (filenum == 0)
    {
        return false;
    }

    *size = m_lengths[filenum - 1];
    data.clear();
    data.resize(*size);

    // On se rend à l'offset du fichier
    m_file.seekg(m_offsets[filenum - 1]);

    // On lit caractère par caractère
    for (std::size_t i = 0 ; i < *size ; ++i)
    {
        char data_n;
        m_file.read(reinterpret_cast<char *>(&data_n), 1);
        data.push_back(data_n);
    }

    return true;
}


/**
 * Donne la taille d'un fichier de l'archive.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets.
 ******************************/

uint64_t CLoaderPAK::getFileSize(const CString& fileName)
{
    CString name = CFileSystem::convertName(fileName);

    // On met la chaine en minuscules
#ifdef T_SYSTEM_WINDOWS
    name = name.toLowerCase();
#endif

    unsigned int size = 0;

    // On cherche le fichier dans la liste des fichiers
    for (std::vector<CString>::iterator it = m_files.begin() ; it != m_files.end() ; ++it)
    {
        // On a trouvé le fichier
#ifdef T_SYSTEM_WINDOWS
        if (it->toLowerCase() == name)
#else
        if (*it == name)
#endif
        {
            size = m_lengths[std::distance(m_files.begin(), it)];

        }
    }

    return size;
}


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderPAK::loadFromFile(const CString& fileName)
{
    m_file.open(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!m_file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderPAK::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // Lecture de l'en-tête
    int32_t ident;
    m_file.read(reinterpret_cast<char *>(&ident), sizeof(int32_t));

    // Vérification de l'identifiant du fichier
    if (ident != ('P' + ('A'<<8) + ('C'<<16) + ('K'<<24)))
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderPAK::loadFromFile : l'identifiant est incorrect (%1)").arg(ident), ILogger::Error);
        return false;
    }

    uint32_t offset = 0;
    uint32_t length = 0;

    m_file.read(reinterpret_cast<char *>(&offset), sizeof(uint32_t));
    m_file.read(reinterpret_cast<char *>(&length), sizeof(uint32_t));

    m_file.seekg(offset);

    // Liste des fichiers
    for (unsigned int i = 0; i < length / 64; ++i)
    {
        char file_name[57] = "";
        uint32_t file_offset = 0;
        uint32_t file_length = 0;

        // Lecture des informations
        m_file.read(reinterpret_cast<char *>(&file_name), 56);
        m_file.read(reinterpret_cast<char *>(&file_offset), sizeof(uint32_t));
        m_file.read(reinterpret_cast<char *>(&file_length), sizeof(uint32_t));

        m_files.push_back(file_name);
        m_offsets.push_back(file_offset);
        m_lengths.push_back(file_length);
    }

    return true;
}


/**
 * Indique si le fichier contient un modèle pouvant être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CLoaderPAK::isCorrectFormat(const char * fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName, std::ios::in | std::ios::binary);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    // Lecture de l'en-tête
    int32_t ident;
    file.read(reinterpret_cast<char *>(&ident), sizeof(int32_t));

    if (!file.good())
    {
        file.close();
        return false;
    }

    file.close();

    // Vérification de l'identifiant du fichier
    return (ident == ('P' + ('A'<<8) + ('C'<<16) + ('K'<<24)));
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
ILoaderArchive * CLoaderPAK::createInstance(const char * fileName)
#else
CLoaderPAK * CLoaderPAK::createInstance(const char * fileName)
#endif
{
    return (CLoaderPAK::isCorrectFormat(fileName) ? new CLoaderPAK() : nullptr);
}

} // Namespace Ted

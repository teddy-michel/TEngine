
/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IResourceManager.cpp
 * \date 03/06/2014 Gestion des répertoires de recherche.
 * \date 04/06/2014 Ajout du répertoire courant par défaut à tous les gestionnaires.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/IResourceManager.hpp"
#include <algorithm>


namespace Ted
{

/**
 * Constructeur du gestionnaire de ressources.
 ******************************/

IResourceManager::IResourceManager()
{
    m_paths.push_back(CString());
}


/**
 * Détruit le gestionnaire et toutes les ressources chargées.
 ******************************/

IResourceManager::~IResourceManager()
{
    freeMemory();
}


/**
 * Ajoute un répertoire à la liste des répertoires du gestionnaire.
 *
 * \param pathName Chemin du répertoire à ajouter.
 ******************************/

void IResourceManager::addPath(const CString& pathName)
{
    if (std::find(m_paths.begin(), m_paths.end(), pathName) == m_paths.end())
    {
        m_paths.push_back(pathName);
    }
}


/**
 * Enlève un répertoire de la liste des répertoires du gestionnaire.
 *
 * \param pathName Chemin du répertoire à enlever.
 ******************************/

void IResourceManager::removePath(const CString& pathName)
{
    m_paths.remove(pathName);
}


/**
 * Donne la liste des répertoires du gestionnaire.
 *
 * \return Liste des répertoires.
 ******************************/

std::list<CString> IResourceManager::getPaths() const
{
    return m_paths;
}

} // Namespace Ted

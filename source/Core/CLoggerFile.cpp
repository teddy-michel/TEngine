/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoggerFile.cpp
 * \date       2008 Création de la classe CLoggerFile.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CLoggerFile.hpp"
#include "Core/CDateTime.hpp"


namespace Ted
{

/**
 * Constructeur par défaut
 *
 * \param file Adresse du fichier de log
 ******************************/

CLoggerFile::CLoggerFile(const CString& file) :
m_file (file.toCharArray())
{
    // Écriture de l'en-tête du fichier
    m_file << "  ================================================" << std::endl;
    m_file << "   Lancement du programme - " << date() << " - " << time() << std::endl;
    m_file << "  ================================================" << std::endl;
    m_file << std::endl;
}


/**
 * Destructeur.
 ******************************/

CLoggerFile::~CLoggerFile()
{
    // Fermeture du fichier
    m_file << std::endl;
    m_file << "  ================================================" << std::endl;
    m_file << "   Fermeture du programme - " << date() << " - " << time() << std::endl;
    m_file << "  ================================================" << std::endl;
}


/**
 * Log un message.
 *
 * \param message Message à inscrire
 ******************************/

void CLoggerFile::write(const CString& message, int level)
{
    if (m_file.is_open())
    {
        m_file << CDateTime::getCurrentTime() << " " << level << " " << message << std::endl << std::flush;
    }
}

} // Namespace

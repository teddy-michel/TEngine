/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CDirectory.cpp
 * \date 21/05/2009 Création de la classe CDirectory.
 * \date 18/01/2011 Le nom du répertoire est convertit par le système de fichier.
 * \date 14/03/2011 Inclusion de sys/stat.h sous Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdint.h>
#include "os.h"

#if defined T_SYSTEM_WINDOWS
#  include <windows.h>
#elif defined T_SYSTEM_LINUX
#  include <dirent.h>
#  include <sys/stat.h>
#else
#  error This operating system is not supported
#endif

#include "Core/CDirectory.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/CTreeNode.hpp"

#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param dir Adresse du répertoire.
 ******************************/

CDirectory::CDirectory(const CString& dir) :
m_directory (dir),
m_filter    ("*.*"),
m_sort      (SortByName),
m_asc       (true)
{
    m_directory = CFileSystem::Instance().getAbsolutePath(m_directory);
    listFiles();
}


/**
 * Accesseur pour directory.
 *
 * \return Adresse du répertoire.
 *
 * \sa CDirectory::setDirectory
 ******************************/

CString CDirectory::getDirectory() const
{
    return m_directory;
}


/**
 * Accesseur pour filter.
 *
 * \return Filtre de recherche.
 *
 * \sa CDirectory::setFilter
 ******************************/

CString CDirectory::getFilter() const
{
    return m_filter;
}


/**
 * Donne la liste des fichiers du répertoire.
 *
 * \return Liste des fichiers.
 ******************************/

std::vector<TFileInfos> CDirectory::getFilesList() const
{
    return m_files;
}


/**
 * Donne la liste des dossiers du répertoire.
 *
 * \return Liste des dossiers.
 ******************************/

std::vector<CString> CDirectory::getDirsList() const
{
    return m_dirs;
}


/**
 * Donne l'arbre du répertoire.
 *
 * \return Pointeur sur la racine de l'arbre.
 ******************************/

CTreeNode * CDirectory::getTree() const
{
    CTreeNode * tree = new CTreeNode(m_directory);

    // Liste des dossiers
    for (std::vector<CString>::const_iterator it = m_dirs.begin(); it != m_dirs.end(); ++it)
    {
        CTreeNode * node = new CTreeNode(*it);
        node->setExpandable();
        tree->AddChild(node);
    }

    // Liste des fichiers
    for (std::vector<TFileInfos>::const_iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        tree->AddChild(it->name);
    }

    return tree;
}


/**
 * Modifie l'adresse du répertoire.
 *
 * \param dir Adresse absolue du répertoire.
 *
 * \sa CDirectory::getDirectory
 ******************************/

void CDirectory::setDirectory(const CString& dir)
{
    m_directory = CFileSystem::Instance().getAbsolutePath(dir);
    listFiles();
}


/**
 * Modifie l'adresse du répertoire à partir du répertoire actuelle.
 *
 * \param dir Adresse relative du répertoire.
 *
 * \sa CDirectory::getDirectory
 ******************************/

void CDirectory::setRelativeDirectory(const CString& dir)
{
    m_directory = CFileSystem::Instance().getAbsolutePath(m_directory + CFileSystem::separator + dir);
    listFiles();
}


/**
 * Mutateur pour filter.
 *
 * \param filter Filtre de recherche.
 *
 * \sa CDirectory::getFilter
 ******************************/

void CDirectory::setFilter(const CString& filter)
{
    m_filter = filter;
    listFiles();
}


/**
 *Mutateur pour sort.
 *
 * \param sort Ordre de tri.
 * \param asc  Booléen indiquant si l'ordre est croissant.
 ******************************/

void CDirectory::setSorting(TSort sort, bool asc)
{
    m_sort = sort;
    m_asc = asc;
    sortFiles();
}


/**
 * Donne le nombre de fichier contenus dans le répertoire.
 *
 * \return Nombre de fichiers.
 ******************************/

unsigned int CDirectory::getNumFiles() const
{
    return m_files.size();
}


/**
 * Donne le nombre de dossiers contenus dans le répertoire.
 *
 * \return Nombre de dossiers.
 ******************************/

unsigned int CDirectory::getNumDirs() const
{
    return m_dirs.size();
}


/**
 * Donne la taille totale des fichiers du répertoire.
 *
 * \return Taille du répertoire.
 ******************************/

unsigned int CDirectory::getSize() const
{
    unsigned int size = 0;

    for (std::vector<TFileInfos>::const_iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        size += it->size;
    }

    return size;
}


/**
 * Charge la liste des fichiers et des dossiers.
 ******************************/

void CDirectory::listFiles()
{
    m_files.clear();
    m_dirs.clear();

#if defined T_SYSTEM_WINDOWS

    HANDLE hFind;
    WIN32_FIND_DATA FindData;

    // Change de dossier
    char old_dir[556];
    GetCurrentDirectory(sizeof(old_dir), old_dir);
    SetCurrentDirectory(m_directory.toCharArray());

    // Début de la recherche
    hFind = FindFirstFile(m_filter.toCharArray(), &FindData);

    if (hFind != INVALID_HANDLE_VALUE)
    {
        // Fichiers suivants
        do
        {
            // Dossier
            if ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                m_dirs.push_back(FindData.cFileName);

                // Dossiers spéciaux
                if (m_dirs.back() == "." || m_dirs.back() == "..")
                {
                    m_dirs.pop_back();
                }
            }
            // Fichier
            else
            {
                TFileInfos file;
                file.name = FindData.cFileName;
                file.size = (FindData.nFileSizeHigh * (MAXDWORD + 1)) + FindData.nFileSizeLow;

                // Conversion de la date
                uint64_t tmp = 116444736;
                tmp *= 1000000000;
                uint64_t time = ((static_cast<uint64_t>(FindData.ftLastWriteTime.dwLowDateTime) << 32) | (FindData.ftLastWriteTime.dwHighDateTime << 0)) - tmp;
                file.time = time / 10000000;

                // Flags
                file.flags = 0;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE   ) file.flags |= ARCHIVE;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_COMPRESSED) file.flags |= COMPRESSED;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_ENCRYPTED ) file.flags |= ENCRYPTED;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN    ) file.flags |= HIDDEN;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_READONLY  ) file.flags |= READONLY;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM    ) file.flags |= SYSTEM;
                if (FindData.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY ) file.flags |= TEMPORARY;

                m_files.push_back(file);
            }
        } while (FindNextFile(hFind, &FindData));

        // Fin de la recherche
        FindClose(hFind);

        // Change de dossier
        SetCurrentDirectory(old_dir);

        // Tri des fichiers et dossiers
        std::sort(m_dirs.begin(), m_dirs.end());
        sortFiles();
    }

#elif defined T_SYSTEM_LINUX

    DIR * rep = opendir(m_filter.toCharArray());

    if (rep)
    {
        struct dirent * ent;

        while ((ent = readdir(rep)))
        {
            struct stat entrystat;

            if (stat(ent->d_name, &entrystat))
            {
                break;
            }

            // Répertoire
            if (S_ISDIR(entrystat.st_mode))
            {
                m_dirs.push_back(ent->d_name);

                // Répertoires spéciaux
                if (m_dirs.back() == "." || m_dirs.back() == "..")
                {
                    m_dirs.pop_back();
                }
            }
            // Fichier
            else
            {
                TFileInfos file;
                file.name = ent->d_name;
                file.size = entrystat.st_size;
                file.time = entrystat.st_mtime;

                // Flags
                file.flags = 0;
                if (file.name[0] == '.')
                    file.flags |= HIDDEN; // Les fichiers cachés commencent par un point
                if (S_ISFIFO (entrystat.st_mode) || S_ISSOCK(entrystat.st_mode) || S_ISBLK(entrystat.st_mode))
                    file.flags |= SYSTEM;

                m_files.push_back(file);
            }
        }

        closedir(rep);

        // Tri des fichiers et dossiers
        std::sort(m_dirs.begin(), m_dirs.end());
        SortFiles();
    }

#else
#error This operating system is not yet supported
#endif
}


/**
 * Tri la liste des fichiers et des dossiers.
 ******************************/

void CDirectory::sortFiles()
{
    switch (m_sort)
    {
        default:
        case SortByName:

            if (m_asc)
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByNameASC());
            }
            else
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByNameDESC());
            }

            break;

        case SortBySize:

            if (m_asc)
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesBySizeASC());
            }
            else
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesBySizeDESC());
            }

            break;

        case SortByType:

            if (m_asc)
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByTypeASC());
            }
            else
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByTypeDESC());
            }

            break;

        case SortByTime:

            if (m_asc)
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByTimeASC());
            }
            else
            {
                std::sort(m_files.begin(), m_files.end(), SortFilesByTimeDESC());
            }

            break;
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CAStar.cpp
 * \date 31/01/2011 Création de la classe CAStar.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CAStar.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param from Nœud de départ.
 * \param to   Nœud d'arrivée.
 ******************************/

CAStar::CAStar(TNode * from, TNode * to) :
m_current_node (from),
m_final_node   (to)
{ }


/**
 * Modifie le nœud actuel.
 *
 * \param node Nœud actuel.
 ******************************/

void CAStar::setCurrentNode(TNode * node)
{
    m_current_node = node;

    // Le nœud est dans la liste
    if (std::find(m_path.begin(), m_path.end(), node) != m_path.end())
    {
        // On supprime les nœuds du chemin tant qu'on ne tombe pas sur node
        for (std::list<TNode *>::iterator it = m_path.begin(); it != m_path.end() && *it != node; )
        {
            it = m_path.erase(it);
        }
    }
    else
    {
        computePath();
    }
}


/**
 * Modifie le nœud d'arrivée.
 *
 * \param node Nœud d'arrivée.
 ******************************/

void CAStar::setDestination(TNode * node)
{
    m_final_node = node;
    computePath();
}


/**
 * Donne le nœud suivant le nœud actuel.
 *
 * \return Pointeur sur le nœud suivant.
 ******************************/

TNode * CAStar::nextNode() const
{
    std::list<TNode *>::const_iterator it = std::find(m_path.begin(), m_path.end(), m_current_node);

    // On est déjà arrivé à destination
    if (*it == m_final_node)
        return nullptr;

    // On cherche le point suivant
    ++it;

    return *it;
}


/**
 * Donne la distance entre le nœud actuel et le nœud final.
 *
 * \return Distance jusqu'au nœud final.
 ******************************/

float CAStar::getDistance()
{
    return m_final_node->g;
}


/**
 * Calcule la distance entre deux nœuds.
 *
 * \param a Premier nœud.
 * \param b Second nœud.
 * \return Distance entre les deux nœuds.
 ******************************/

float CAStar::nodeDistance(TNode * a, TNode * b)
{
    T_ASSERT(a != nullptr);
    T_ASSERT(b != nullptr);

    TVector3F v = a->pos - b->pos;
    return v.norm();
}


/**
 * Cherche le chemin entre le nœud de départ et le nœud d'arrivée.
 ******************************/

void CAStar::computePath()
{
    m_path.clear();

    T_ASSERT(m_current_node != nullptr);
    T_ASSERT(m_final_node != nullptr);

    std::list<TNode *> lst_open;
    //std::list<TNode *> lst_close;

    lst_open.push_back(m_current_node);

    while (true)
    {
        // La liste ouverte est vide, il n'y a pas de solution
        if (lst_open.empty())
            return;


        TNode * node = nullptr;
        float f = std::numeric_limits<float>::max();

        for (std::list<TNode *>::const_iterator it = lst_open.begin(); it != lst_open.end(); ++it)
        {
            float tmp = (*it)->g + (*it)->h;

            if (tmp < f)
            {
                f = tmp;
                node = *it;
            }
        }


        // On est arrivé à destination
        if (node == m_final_node)
            break;

        // On passe le nœud de la liste ouverte à la liste fermée
        //lst_close.push_front(node);

        std::list<TNode *>::iterator it0 = std::find(lst_open.begin(), lst_open.end(), node);

        if (it0 != lst_open.end())
        {
            lst_open.erase(it0);
        }


        // On parcourt les nœuds adjacents
        for (std::list<TNode *>::iterator it = node->adj.begin(); it != node->adj.end(); ++it)
        {
            // Le nœud a déjà été visité
            if (std::find(lst_open.begin(), lst_open.end(), *it) != lst_open.end())
            {
                float g = node->g + nodeDistance(node, *it);

                if (g < (*it)->g)
                {
                    (*it)->g = g;
                    (*it)->parent = node;
                }
            }
            else
            {
                lst_open.push_front(*it);
                (*it)->parent = node;
                (*it)->g = node->g + nodeDistance(node, *it);
                (*it)->h = nodeDistance(*it, m_final_node);
            }
        }
    }

    // Construction de la liste
    TNode * node = m_final_node;
    m_path.push_back(node);

    while (node != m_current_node)
    {
        m_path.push_front(node->parent);
        if (node->parent == m_current_node)
            break;
        node = node->parent;
    }
}

} // Namespace

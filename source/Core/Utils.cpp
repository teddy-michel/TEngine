/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Utils.cpp
 * \date       2008 Création des fonctions utilitaires.
 * \date 14/08/2010 Création de la fonction DoubleToString.
 * \date 02/06/2011 Ajout de la fonction PointerToString.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <ctime>
#include <iomanip>

#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Vérifie qu'une chaine peut être convertie en short.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isShort(const std::string& str)
{
    std::istringstream iss(str);
    short tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Vérifie qu'une chaine peut être convertie en unsigned short.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isUShort(const std::string& str)
{
    std::istringstream iss(str);
    unsigned short tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Vérifie qu'une chaine peut être convertie en int.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isInt(const std::string& str)
{
    std::istringstream iss(str);
    int tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Vérifie qu'une chaine peut être convertie en unsigned int.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isUInt(const std::string& str)
{
    std::istringstream iss(str);
    unsigned int tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Vérifie qu'une chaine peut être convertie en float.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isFloat(const std::string& str)
{
    std::istringstream iss(str);
    float tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Vérifie qu'une chaine peut être convertie en double.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine de caractères.
 * \return Booléen.
 ******************************/

bool isDouble(const std::string& str)
{
    std::istringstream iss(str);
    double tmp;

    return (iss >> tmp) && (iss.eof());
}


/**
 * Convertit une chaine de caractères en minuscules.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine à convertir.
 * \return Chaine convertie.
 ******************************/

std::string toLower(const std::string& str)
{
    std::string ret = str;

    for (std::string::iterator i = ret.begin(); i != ret.end(); ++i)
    {
        *i = static_cast<char>(tolower(*i));
    }

    return ret;
}


/**
 * Convertit une chaine de caractères en majuscules.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine à convertir.
 * \return Chaine convertie.
 ******************************/

std::string toUpper(const std::string& str)
{
    std::string ret = str;

    for (std::string::iterator i = ret.begin(); i != ret.end(); ++i)
    {
        *i = static_cast<char>(toupper(*i));
    }

    return ret;
}


/**
 * Convertit une chaine de caractères au format UTF8 vers le format Latin1.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param str Chaine à convertir.
 * \return Chaine convertie.
 ******************************/

std::string UTF8toLatin1(const std::string& str)
{
    std::string nstr;

    // On parcourt la chaine caractère par caractère
    for (std::string::const_iterator it = str.begin(); it != str.end(); ++it)
    {
        // 1xxxxxxx
        if (*it & 128)
        {
            // 1100001x
            //if ((*it & 66) && !(*it & 4) && !(*it & 8) && !(*it & 16) && !(*it & 32))
            if ((*it & 66) && !(*it & 60))
            {
                char tmp = *it << 6;

                if (++it == str.end())
                {
                    return nstr;
                }

                // 10xxxxxx
                if ((*it & 128) && !(*it & 64))
                {
                    tmp += (*it & ~128);
                    nstr += tmp;
                }
            }
        }
        // 0xxxxxxx
        else
        {
            nstr += *it;
        }
    }

    return nstr;
}


/**
 * Convertit un nombre en chaine de caractères, en précisant le nombre de décimales.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param nbr      Nombre à convertir.
 * \param decimals Nombre de décimales.
 * \return Chaine de caractères.
 ******************************/

std::string DoubleToString(double nbr, unsigned int decimals)
{
    std::ostringstream oss;
    oss << std::setprecision(decimals) << std::fixed << nbr;
    return oss.str();
}


/**
 * Convertit un pointeur en chaine de caractères.
 *
 * \todo Intégrer à la classe CString.
 *
 * \param ptr Adresse à convertir.
 * \return Chaine de caractères.
 ******************************/

std::string PointerToString(void * ptr)
{
    std::ostringstream oss;
    oss << ptr;
    return oss.str();
}

} // Namespace Ted

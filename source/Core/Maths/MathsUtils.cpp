/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/MathsUtils.cpp
 * \date 30/03/2013 Création du fichier.
 * \date 06/10/2013 Correction d'un dépassement de valeur dans la fonction RandInt.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <ctime>
#include <cstdlib>

#include "Core/Maths/MathsUtils.hpp"


namespace Ted
{

uint64_t TRand();
uint64_t TRand()
{
    static bool init = false;

    if (!init)
    {
        srand(time(nullptr));
        init = true;
    }

    return rand();
}


/**
 * Génère un nombre aléatoire compris entre deux valeurs.
 *
 * \param min Valeur minimale.
 * \param max Valeur maximale.
 * \return Nombre aléatoire.
 ******************************/

unsigned int RandInt(int32_t min, int32_t max)
{
    return (static_cast<int32_t>(min + (static_cast<float>(TRand() * (max - min + 1) / RAND_MAX))));
}


/**
 * Génère un nombre aléatoire compris entre deux valeurs.
 *
 * \param min Valeur minimale.
 * \param max Valeur maximale.
 * \return Nombre aléatoire.
 ******************************/

unsigned int RandUnsignedInt(uint32_t min, uint32_t max)
{
    return (static_cast<uint32_t>(min + (static_cast<float>(TRand() * (max - min + 1) / RAND_MAX))));
}


/**
 * Génère un nombre aléatoire compris entre deux valeurs réelles.
 *
 * \param min Valeur minimale.
 * \param max Valeur maximale.
 * \return Nombre aléatoire.
 ******************************/

float RandFloat(float min, float max)
{
    return min + static_cast<float>(RandInt(static_cast<unsigned int>(0), static_cast<unsigned int>(RAND_MAX))) * (max - min) / RAND_MAX;
}

} // Namespace Ted

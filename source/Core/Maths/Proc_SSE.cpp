/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/Proc_SSE.cpp
 * \date 24/12/2010 Création des fonctions.
 * \date 04/03/2011 Adaptation de la syntaxe à GCC.
 */

#if 0

/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Proc_SSE.hpp"
#include "os.h"


namespace Ted
{

/**
 * Calcule la racine carrée d'un nombre.
 *
 * \param x Nombre dont ont veut calculer la racine carrée.
 * \return Racine carrée du nombre.
 ******************************/

float SSE_Sqrt(float x)
{
	float root = 0.0f;

#ifdef T_SYSTEM_WINDOWS

	__asm
	(
		"sqrtss     xmm0, x                         \n"
		"movss      root, xmm0                      \n"
	);

#elif defined(T_SYSTEM_LINUX)

	__asm__ __volatile__
	(
		"movss %1,%%xmm2                            \n"
		"sqrtss %%xmm2,%%xmm1                       \n"
		"movss %%xmm1,%0                            \n"
       	: "=m" (root)
		: "m" (x)
	);

#endif

	return root;
}


void SSE_SinCos(float x, float * s, float * c)
{

#ifdef T_SYSTEM_WINDOWS

    float t4, t8, t12;

    __asm
    (
        "movss      xmm0, x                         \n"
        "movss      t12, xmm0                       \n"
        "movss      xmm1, _ps_am_inv_sign_mask      \n"
        "mov        eax, t12                        \n"
        "mulss      xmm0, _ps_am_2_o_pi             \n"
        "andps      xmm0, xmm1                      \n"
        "and        eax, 0x80000000                 \n"

        "cvttss2si  edx, xmm0                       \n"
        "mov        ecx, edx                        \n"
        "mov        t12, esi                        \n"
        "mov        esi, edx                        \n"
        "add        edx, 0x1                        \n"
        "shl        ecx, (31 - 1)                   \n"
        "shl        edx, (31 - 1)                   \n"

        "movss      xmm4, _ps_am_1                  \n"
        "cvtsi2ss   xmm3, esi                       \n"
        "mov        t8, eax                         \n"
        "and        esi, 0x1                        \n"

        "subss      xmm0, xmm3                      \n"
        "movss      xmm3, _sincos_inv_masks[esi * 4]\n"
        "minss      xmm0, xmm4                      \n"

        "subss      xmm4, xmm0                      \n"

        "movss      xmm6, xmm4                      \n"
        "andps      xmm4, xmm3                      \n"
        "and		ecx, 0x80000000                 \n"
        "movss      xmm2, xmm3                      \n"
        "andnps     xmm3, xmm0                      \n"
        "and        edx, 0x80000000                 \n"
        "movss      xmm7, t8                        \n"
        "andps      xmm0, xmm2                      \n"
        "mov        t8, ecx                         \n"
        "mov        t4, edx                         \n"
        "orps       xmm4, xmm3                      \n"

        "mov        eax, s                          \n" //mov eax, [esp + 4 + 16]
        "mov        edx, c                          \n" //mov edx, [esp + 4 + 16 + 4]

        "andnps     xmm2, xmm6                      \n"
        "orps       xmm0, xmm2                      \n"

        "movss      xmm2, t8                        \n"
        "movss      xmm1, xmm0                      \n"
        "movss      xmm5, xmm4                      \n"
        "xorps      xmm7, xmm2                      \n"
        "movss      xmm3, _ps_sincos_p3             \n"
        "mulss      xmm0, xmm0                      \n"
        "mulss      xmm4, xmm4                      \n"
        "movss      xmm2, xmm0                      \n"
        "movss      xmm6, xmm4                      \n"
        "orps       xmm1, xmm7                      \n"
        "movss      xmm7, _ps_sincos_p2             \n"
        "mulss      xmm0, xmm3                      \n"
        "mulss      xmm4, xmm3                      \n"
        "movss      xmm3, _ps_sincos_p1             \n"
        "addss      xmm0, xmm7                      \n"
        "addss      xmm4, xmm7                      \n"
        "movss      xmm7, _ps_sincos_p0             \n"
        "mulss      xmm0, xmm2                      \n"
        "mulss      xmm4, xmm6                      \n"
        "addss      xmm0, xmm3                      \n"
        "addss      xmm4, xmm3                      \n"
        "movss      xmm3, t4                        \n"
        "mulss      xmm0, xmm2                      \n"
        "mulss      xmm4, xmm6                      \n"
        "orps       xmm5, xmm3                      \n"
        "mov        esi, t12                        \n"
        "addss      xmm0, xmm7                      \n"
        "addss      xmm4, xmm7                      \n"
        "mulss      xmm0, xmm1                      \n"
        "mulss      xmm4, xmm5                      \n"

        // use full stores since caller might reload with full loads
        "movss      [eax], xmm0                     \n"
        "movss      [edx], xmm4                     \n"
    );

#else
#   error "Not Implemented"
#endif
}


float SSE_Cos(float x)
{

#ifdef T_SYSTEM_WINDOWS

    float temp;

    __asm
    (
        "movss      xmm0, x                         \n"
        "movss      xmm1, _ps_am_inv_sign_mask      \n"
        "andps      xmm0, xmm1                      \n"
        "addss      xmm0, _ps_am_pi_o_2             \n"
        "mulss      xmm0, _ps_am_2_o_pi             \n"

        "cvttss2si  ecx, xmm0                       \n"
        "movss      xmm5, _ps_am_1                  \n"
        "mov        edx, ecx                        \n"
        "shl        edx, (31 - 1)                   \n"
        "cvtsi2ss   xmm1, ecx                       \n"
        "and        edx, 0x80000000                 \n"
        "and        ecx, 0x1                        \n"

        "subss      xmm0, xmm1                      \n"
        "movss      xmm6, _sincos_masks[ecx * 4]    \n"
        "minss      xmm0, xmm5                      \n"

        "movss       xmm1, _ps_sincos_p3            \n"
        "subss       xmm5, xmm0                     \n"

        "andps       xmm5, xmm6                     \n"
        "movss       xmm7, _ps_sincos_p2            \n"
        "andnps      xmm6, xmm0                     \n"
        "mov         temp, edx                      \n"
        "orps        xmm5, xmm6                     \n"
        "movss       xmm0, xmm5                     \n"

        "mulss       xmm5, xmm5                     \n"
        "movss       xmm4, _ps_sincos_p1            \n"
        "movss       xmm2, xmm5                     \n"
        "mulss       xmm5, xmm1                     \n"
        "movss       xmm1, _ps_sincos_p0            \n"
        "addss       xmm5, xmm7                     \n"
        "mulss       xmm5, xmm2                     \n"
        "movss       xmm3, temp                     \n"
        "addss       xmm5, xmm4                     \n"
        "mulss       xmm5, xmm2                     \n"
        "orps        xmm0, xmm3                     \n"
        "addss       xmm5, xmm1                     \n"
        "mulss       xmm0, xmm5                     \n"

        "movss       x, xmm0                        \n"
    );

#else
#   error "Not Implemented"
#endif

    return x;
}


void SSE2_SinCos(float x, float * s, float * c)
{

#ifdef T_SYSTEM_WINDOWS

    __asm
    (
        "movss       xmm0, x                        \n"
        "movaps      xmm7, xmm0                     \n"
        "movss       xmm1, _ps_am_inv_sign_mask     \n"
        "movss       xmm2, _ps_am_sign_mask         \n"
        "movss       xmm3, _ps_am_2_o_pi            \n"
        "andps       xmm0, xmm1                     \n"
        "andps       xmm7, xmm2                     \n"
        "mulss       xmm0, xmm3                     \n"

        "pxor        xmm3, xmm3                     \n"
        "movd        xmm5, _epi32_1                 \n"
        "movss       xmm4, _ps_am_1                 \n"

        "cvttps2dq   xmm2, xmm0                     \n"
        "pand        xmm5, xmm2                     \n"
        "movd        xmm1, _epi32_2                 \n"
        "pcmpeqd     xmm5, xmm3                     \n"
        "movd        xmm3, _epi32_1                 \n"
        "cvtdq2ps    xmm6, xmm2                     \n"
        "paddd       xmm3, xmm2                     \n"
        "pand        xmm2, xmm1                     \n"
        "pand        xmm3, xmm1                     \n"
        "subss       xmm0, xmm6                     \n"
        "pslld       xmm2, (31 - 1)                 \n"
        "minss       xmm0, xmm4                     \n"

        "mov         eax, s                         \n" // mov eax, [esp + 4 + 16]
        "mov         edx, c                         \n" // mov edx, [esp + 4 + 16 + 4]

        "subss       xmm4, xmm0                     \n"
        "pslld       xmm3, (31 - 1)                 \n"

        "movaps      xmm6, xmm4                     \n"
        "xorps       xmm2, xmm7                     \n"
        "movaps      xmm7, xmm5                     \n"
        "andps       xmm6, xmm7                     \n"
        "andnps      xmm7, xmm0                     \n"
        "andps       xmm0, xmm5                     \n"
        "andnps      xmm5, xmm4                     \n"
        "movss       xmm4, _ps_sincos_p3            \n"
        "orps        xmm6, xmm7                     \n"
        "orps        xmm0, xmm5                     \n"
        "movss       xmm5, _ps_sincos_p2            \n"

        "movaps      xmm1, xmm0                     \n"
        "movaps      xmm7, xmm6                     \n"
        "mulss       xmm0, xmm0                     \n"
        "mulss       xmm6, xmm6                     \n"
        "orps        xmm1, xmm2                     \n"
        "orps        xmm7, xmm3                     \n"
        "movaps      xmm2, xmm0                     \n"
        "movaps      xmm3, xmm6                     \n"
        "mulss       xmm0, xmm4                     \n"
        "mulss       xmm6, xmm4                     \n"
        "movss       xmm4, _ps_sincos_p1            \n"
        "addss       xmm0, xmm5                     \n"
        "addss       xmm6, xmm5                     \n"
        "movss       xmm5, _ps_sincos_p0            \n"
        "mulss       xmm0, xmm2                     \n"
        "mulss       xmm6, xmm3                     \n"
        "addss       xmm0, xmm4                     \n"
        "addss       xmm6, xmm4                     \n"
        "mulss       xmm0, xmm2                     \n"
        "mulss       xmm6, xmm3                     \n"
        "addss       xmm0, xmm5                     \n"
        "addss       xmm6, xmm5                     \n"
        "mulss       xmm0, xmm1                     \n"
        "mulss       xmm6, xmm7                     \n"

        // use full stores since caller might reload with full loads
        "movss       [eax], xmm0                    \n"
        "movss       [edx], xmm6                    \n"
    );

#else
#   error "Not Implemented"
#endif

}

float SSE2_Cos(float x)
{

#ifdef T_SYSTEM_WINDOWS

    __asm
    (
        "movss       xmm0, x                        \n"
        "movss       xmm1, _ps_am_inv_sign_mask     \n"
        "movss       xmm2, _ps_am_pi_o_2            \n"
        "movss       xmm3, _ps_am_2_o_pi            \n"
        "andps       xmm0, xmm1                     \n"
        "addss       xmm0, xmm2                     \n"
        "mulss       xmm0, xmm3                     \n"

        "pxor        xmm3, xmm3                     \n"
        "movd        xmm5, _epi32_1                 \n"
        "movss       xmm4, _ps_am_1                 \n"
        "cvttps2dq   xmm2, xmm0                     \n"
        "pand        xmm5, xmm2                     \n"
        "movd        xmm1, _epi32_2                 \n"
        "pcmpeqd     xmm5, xmm3                     \n"
        "cvtdq2ps    xmm6, xmm2                     \n"
        "pand        xmm2, xmm1                     \n"
        "pslld       xmm2, (31 - 1)                 \n"

        "subss       xmm0, xmm6                     \n"
        "movss       xmm3, _ps_sincos_p3            \n"
        "minss       xmm0, xmm4                     \n"
        "subss       xmm4, xmm0                     \n"
        "andps       xmm0, xmm5                     \n"
        "andnps      xmm5, xmm4                     \n"
        "orps        xmm0, xmm5                     \n"

        "movaps      xmm1, xmm0                     \n"
        "movss       xmm4, _ps_sincos_p2            \n"
        "mulss       xmm0, xmm0                     \n"
        "movss       xmm5, _ps_sincos_p1            \n"
        "orps        xmm1, xmm2                     \n"
        "movaps      xmm7, xmm0                     \n"
        "mulss       xmm0, xmm3                     \n"
        "movss       xmm6, _ps_sincos_p0            \n"
        "addss       xmm0, xmm4                     \n"
        "mulss       xmm0, xmm7                     \n"
        "addss       xmm0, xmm5                     \n"
        "mulss       xmm0, xmm7                     \n"
        "addss       xmm0, xmm6                     \n"
        "mulss       xmm0, xmm1                     \n"
        "movss       x, xmm0                        \n"
    );

#else
#   error "Not Implemented"
#endif

    return x;
}

} // Namespace Ted

#endif

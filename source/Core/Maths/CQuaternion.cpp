/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Maths/CQuaternion.cpp
 * \date       2008 Création de la classe CQuaternion.
 * \date 12/07/2010 Création d'un quaternion depuis une matrice de rotation.
 *                  Création d'un quaternion depuis un axe de rotation et un angle.
 *                  Ajout d'une méthode pour normaliser un quaternion.
 * \date 15/12/2010 Modification des constructeurs, ajout de la conversion depuis un angle.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>
#include <limits>

#include "Core/Maths/CQuaternion.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CQuaternion::CQuaternion() :
A (0.0f) , B (0.0f) , C (0.0f) , D (0.0f)
{ }


/**
 * Constructeur à partir de quatres réels.
 *
 * \param a Premier nombre.
 * \param b Deuxième nombre.
 * \param c Troisième nombre.
 * \param d Quatrième nombre.
 ******************************/

CQuaternion::CQuaternion(float a, float b, float c, float d) :
A (a) , B (b) , C (c) , D (d)
{ }


/**
 * Opération unaire +.
 *
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator+() const
{
    return CQuaternion(A, B, C, D);
}


/**
 * Opération unaire -.
 *
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator-() const
{
    return CQuaternion(-A, -B, -C, -D);
}


/**
 * Opération binaire +.
 *
 * \param quaternion Quaternion à ajouter.
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator+(const CQuaternion &quaternion) const
{
    return CQuaternion(A + quaternion.A, B + quaternion.B, C + quaternion.C, D + quaternion.D);
}


/**
 * Opération binaire -.
 *
 * \param quaternion Quaternion à soustraire.
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator-(const CQuaternion &quaternion) const
{
    return CQuaternion(A - quaternion.A, B - quaternion.B, C - quaternion.C, D - quaternion.D);
}


/**
 * Opération +=.
 *
 * \param quaternion : Quaternion à ajouter.
 * \return Résultat de l'opération.
 ******************************/

const CQuaternion& CQuaternion::operator+=(const CQuaternion& quaternion)
{
    A += quaternion.A;
    B += quaternion.B;
    C += quaternion.C;
    D += quaternion.D;

    return *this;
}


/**
 * Opération -=.
 *
 * \param quaternion : Quaternion à soustraire.
 * \return Résultat de l'opération.
 ******************************/

const CQuaternion& CQuaternion::operator-=(const CQuaternion& quaternion)
{
    A -= quaternion.A;
    B -= quaternion.B;
    C -= quaternion.C;
    D -= quaternion.D;

    return *this;
}


/**
 * Multiplication par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator*(float k) const
{
    return CQuaternion(A * k, B * k, C * k, D * k);
}


/**
 *Division par un scalaire.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator/(float k) const
{
    return CQuaternion(A / k, B / k, C / k, D / k);
}


/**
 * Multiplication interne.
 *
 * \param quaternion Quaternion à multiplier.
 * \return Résultat de l'opération.
 ******************************/

CQuaternion CQuaternion::operator*(const CQuaternion& quaternion) const
{
    return CQuaternion(A * quaternion.A - B * quaternion.B - C * quaternion.C - D * quaternion.D,
                       A * quaternion.B + B * quaternion.A + C * quaternion.D - D * quaternion.C,
                       A * quaternion.C + C * quaternion.A + D * quaternion.B - B * quaternion.D,
                       A * quaternion.D + D * quaternion.A + B * quaternion.C - C * quaternion.B);
}


/**
 * Opération *=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

const CQuaternion& CQuaternion::operator*=(float k)
{
    A *= k;
    B *= k;
    C *= k;
    D *= k;

    return *this;
}


/**
 * Opération /=.
 *
 * \param k Scalaire.
 * \return Résultat de l'opération.
 ******************************/

const CQuaternion& CQuaternion::operator/=(float k)
{
    A /= k;
    B /= k;
    C /= k;
    D /= k;

    return *this;
}


/**
 * Comparaison ==.
 *
 * \param quaternion Quaternion à comparer.
 * \return Booléen.
 ******************************/

bool CQuaternion::operator==(const CQuaternion& quaternion) const
{
    return ((std::abs(A - quaternion.A) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(B - quaternion.B) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(C - quaternion.C) <= std::numeric_limits<float>::epsilon()) &&
            (std::abs(D - quaternion.D) <= std::numeric_limits<float>::epsilon()));
}


/**
 * Comparaison !=.
 *
 * \param quaternion Quaternion à comparer.
 * \return Booléen.
 ******************************/

bool CQuaternion::operator!=(const CQuaternion& quaternion) const
{
    return !(*this == quaternion);
}


/**
 * Transtypage en float *.
 ******************************/

CQuaternion::operator float*()
{
    return &A;
}


/**
 * Transtypage en const float *.
 ******************************/

CQuaternion::operator const float*() const
{
    return &A;
}


/**
 * Donne la matrice associée au quaternion.
 *
 * \return Matrice associée au quaternion.
 ******************************/

TMatrix4F CQuaternion::Matrice() const
{
    return TMatrix4F(A, -B, -C, -D, B, A, -D, C, C, D, A, -B, D, -C, B, A);
}


/**
 * Transforme le quaternion en une matrice de rotation.
 *
 * \return Matrice de rotation.
 ******************************/

TMatrix3F CQuaternion::toRotationMatrix() const
{
    float t1 = B * B;
    float t2 = C * C;
    float t3 = D * D;
    float t4 = B * C;
    float t5 = D * A;
    float t6 = C * A;
    float t7 = B * D;
    float t8 = C * D;
    float t9 = B * A;

    return TMatrix3F(1.0f - 2.0f * (t2 - t3) , 2.0f * (t4 - t5) , 2.0f * (t7 - t6) ,
                     2.0f * (t4 + t5) , 1.0f - 2.0f * (t1 - t3) , 2.0f * (t8 - t9) ,
                      2.0f * (t7 - t6) , 2.0f * (t8 + t9) , 1.0f - 2.0f * (t1 - t2));
}


/**
 * Transforme une matrice de rotation en un quaternion.
 *
 * \param matrix Matrice de rotation.
 ******************************/

void CQuaternion::FromRotationMatrix(const TMatrix3F& matrix)
{
    float tr = matrix.getTrace();

    if (tr > 0)
    {
        float s = 0.5f / std::sqrt(tr);

        A = s * (matrix.CB - matrix.BC);
        B = s * (matrix.AC - matrix.CA);
        C = s * (matrix.BA - matrix.AB);
        D = 0.25f / s;
    }
    else
    {
        int max = (matrix.AA > matrix.BB ? (matrix.CC > matrix.AA ? 3 : 1) : (matrix.CC > matrix.BB ? 3 : 2));

        if (max == 1)
        {
            float s = 0.5f * std::sqrt(1 + matrix.AA - matrix.BB - matrix.CC);

            A = 0.5f / s;
            B = (matrix.AB - matrix.BA) / s;
            C = (matrix.AC - matrix.CA) / s;
            D = (matrix.BC - matrix.CB) / s;
        }
        else if (max == 2)
        {
            float s = 0.5f * std::sqrt(1 - matrix.AA + matrix.BB - matrix.CC);

            A = (matrix.AB - matrix.BA) / s;
            B = 0.5f / s;
            C = (matrix.BC - matrix.CB) / s;
            D = (matrix.AC - matrix.CA) / s;
        }
        else if (max == 3)
        {
            float s = 0.5f * std::sqrt(1 - matrix.AA - matrix.BB + matrix.CC);

            A = (matrix.AC - matrix.CA) / s;
            B = (matrix.BC - matrix.CB) / s;
            C = 0.5f / s;
            D = (matrix.AB - matrix.BA) / s;
        }
    }

    return;
}


/**
 * Transforme un axe de rotation et un angle en un quaternion.
 *
 * \param axis  Axe de rotation.
 * \param angle Angle de rotation en radians.
 ******************************/

void CQuaternion::FromAxeRotation(const TVector3F& axis, float angle)
{
    float tmp = angle * 0.5f;
    float sin_a = std::sin(tmp);

    A = axis.X * sin_a;
    B = axis.Y * sin_a;
    C = axis.Z * sin_a;
    D = std::cos(tmp);

    Normalize();
}


/**
 * Transforme un angle de rotation (yaw, pitch, roll) en un quaternion.
 *
 * \param angles Angles de rotation.
 ******************************/

void CQuaternion::FromAngles(const TVector3F& angles)
{
    float tmp = angles.X * 0.5f;
    float sin0 = std::sin(tmp);
    float cos0 = std::cos(tmp);

    tmp = angles.Y * 0.5f;
    float sin1 = std::sin(tmp);
    float cos1 = std::cos(tmp);

    tmp = angles.Z * 0.5f;
    float sin2 = std::sin(tmp);
    float cos2 = std::cos(tmp);

    float tmp1 = sin0 * cos1;
    float tmp2 = cos0 * cos1;
    float tmp3 = sin1 * sin2;
    float tmp4 = sin1 * cos2;

    A = tmp1 * cos2 - cos0 * tmp3;
    B = cos0 * tmp4 + tmp1 * sin2;
    C = tmp2 * sin2 - sin0 * tmp4;
    D = tmp2 * cos2 + sin0 * tmp3;
}


/**
 * Donne le conjugué du quaternion.
 *
 * \return Conjuqué du quaternion.
 ******************************/

CQuaternion CQuaternion::Conjugue() const
{
    return CQuaternion(A, -B, -C, -D);
}


/**
 * Donne l'inverse du quaternion.
 *
 * \return Inverse du quaternion.
 ******************************/

CQuaternion CQuaternion::Inverse() const
{
    float tmp = A * A + B * B + C * C + D * D;

    if (std::abs(tmp) <= std::numeric_limits<float>::epsilon())
    {
        return CQuaternion();
    }

    return CQuaternion(A, -B, -C, -D) / tmp;
}


/**
 * Calcul la norme du quaternion.
 *
 * \return Norme du quaternion.
 ******************************/

float CQuaternion::Norm() const
{
    return std::sqrt(A * A + B * B + C * C + D * D);
}


/**
 * Normalise le quaternion.
 ******************************/

void CQuaternion::Normalize()
{
    float n = Norm();

    if (std::abs(n) > std::numeric_limits<float>::epsilon())
    {
        A /= n;
        B /= n;
        C /= n;
        D /= n;
    }
}

} // Namespace Ted

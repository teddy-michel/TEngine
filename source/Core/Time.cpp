/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Time.cpp
 * \date 18/07/2010 Création des fonctions de gestion du temps.
 * \date 15/03/2011 Inclusion de sys/time.h sous Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/Time.hpp"

#include "os.h"

#if defined T_SYSTEM_WINDOWS
#  include <windows.h>
#elif defined T_SYSTEM_LINUX
#  include <unistd.h>
#  include <sys/time.h>
#else
#  error This operating system is not yet supported.
#endif


namespace Ted
{

#if defined T_SYSTEM_LINUX

TTime getCurrentTime();

TTime getCurrentTime()
{
    struct timeval tmp;
    gettimeofday(&tmp, nullptr);

#ifdef T_TIME_64BITS
    return ((static_cast<uint32_t>(tmp.tv_sec) * 1000) + static_cast<uint32_t>(tmp.tv_usec / 1000));
#else
    return ((tmp.tv_sec * 1000) + (tmp.tv_usec / 1000));
#endif
}

#endif


/**
 * Donne le nombre de millisecondes écoulées depuis le lancement du programme.
 *
 * \return Nombre de millisecondes écoulées.
 ******************************/

TTime getElapsedTime()
{
#if defined T_SYSTEM_WINDOWS
    static TTime start = GetTickCount();
    return (GetTickCount() - start);
#elif defined T_SYSTEM_LINUX
    static TTime start = getCurrentTime();
    return (getCurrentTime() - start);
#else
#  error This operating system is not yet supported.
#endif
}


/**
 * Interrompt l'exécution du processus pendant une certaine durée.
 *
 * \param time Durée à attendre en millisecondes.
 ******************************/

void Delay(TTime time)
{
#if defined T_SYSTEM_WINDOWS
    Sleep(time);
#elif defined T_SYSTEM_LINUX
    usleep(1000 * time);
#else
#  error This operating system is not yet supported.
#endif
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/IAchievement.cpp
 * \date 01/07/2010 Création de la classe IAchievement.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/IAchievement.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param name Nom du succès.
 ******************************/

IAchievement::IAchievement(const CString& name) :
m_name      (name),
m_max_steps (1),
m_nbr_steps (0),
m_locked    (false)
{ }


/**
 * Modifie le nom du succès.
 *
 * \param name Nom du succès.
 ******************************/

void IAchievement::setName(const CString& name)
{
    m_name = name;
}


/**
 * Modifie le nombre de pas pour que le succès soit dévérouillé.
 *
 * \param steps Nombre de pas pour que le succès soit dévérouillé.
 ******************************/

void IAchievement::setMaxSteps(unsigned int steps)
{
    m_max_steps = steps;
}


/**
 * Modifie le nombre actuel de pas.
 *
 * \param steps Nombre actuel de pas.
 ******************************/

void IAchievement::setNumSteps(unsigned int steps)
{
    m_nbr_steps = steps;
}


/**
 * Bloque ou débloque le succès.
 *
 * \param locked Indique si le succès doit être bloqué.
 ******************************/

void IAchievement::setLocked(bool locked)
{
    m_locked = locked;
}

} // Namespace Ted

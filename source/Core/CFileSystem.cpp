/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CFileSystem.cpp
 * \date 12/01/2010 Création de la classe CFileSystem.
 * \date 08/07/2010 La classe FileSystem est renommée en CFileSystem.
 * \date 10/07/2010 Le chargement des fichiers utilisent maintenant une méthode LoadFromFile.
 * \date 27/07/2010 Ajout du paramètre separator.
 * \date 02/03/2011 Les chargeurs d'archives sont déclarés dans le constructeur.
 * \date 14/03/2011 Inclusion de dirent.h pour compiler sous Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <list>

#include "os.h"

#ifdef T_SYSTEM_WINDOWS
#  include <direct.h>
#  include <windows.h>
#else
#  include <dirent.h>
#  include <unistd.h>
#endif

#include "Core/CFileSystem.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/CFile.hpp"
#include "Core/Utils.hpp"

// Formats d'archive
#include "Core/ILoaderArchive.hpp"
#include "Core/CLoader7Z.hpp"
#include "Core/CLoaderPAK.hpp"
#include "Core/CLoaderTAR.hpp"
#include "Core/CLoaderZIP.hpp"

#include "Core/Allocation.hpp"


namespace Ted
{

#if defined(T_SYSTEM_WINDOWS)
const char CFileSystem::separator = '\\'; // Le caractère / peut aussi être utilisé.
#elif defined(T_SYSTEM_LINUX)
const char CFileSystem::separator = '/';
#elif defined(T_SYSTEM_MAC)
const char CFileSystem::separator = ':';
#else
const char CFileSystem::separator = '/';
#endif


T_SINGLETON_IMPL(CFileSystem)

/**
 * Constructeur par défaut.
 ******************************/

CFileSystem::CFileSystem()
{
    // Ajout des chargeurs d'archive du moteur
    ILoaderArchive::addArchiveLoader(&CLoader7Z::createInstance);
    ILoaderArchive::addArchiveLoader(&CLoaderPAK::createInstance);
    ILoaderArchive::addArchiveLoader(&CLoaderZIP::createInstance);
    ILoaderArchive::addArchiveLoader(&CLoaderTAR::createInstance);
}


/**
 * Destructeur.
 ******************************/

CFileSystem::~CFileSystem()
{
    // On libère la mémoire
    for (std::vector<ILoaderArchive *>::iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        delete (*it);
    }
}


/**
 * Ajoute une archive au système de fichier.
 *
 * \param fileName Adresse du fichier de l'archive.
 ******************************/

void CFileSystem::addArchive(const CString& fileName)
{
    // L'archive est déjà dans le système de fichier
    if (archiveExists(fileName))
    {
        return;
    }

    CString abs_name = getAbsolutePath(fileName);

    ILoaderArchive * file = ILoaderArchive::getArchiveLoader(abs_name);

    if (file && file->loadFromFile(abs_name))
    {
        m_archives.push_back(file);
    }
}


/**
 * Ajoute un répertoire au système de fichier.
 *
 * \param dir Nom du répertoire.
 ******************************/

void CFileSystem::addDirectory(const CString& dir)
{
    CString abs_name = getAbsolutePath(dir);

    // On parcourt la liste des répertoires
    for (std::vector<CString>::iterator it = m_dirs.begin() ; it != m_dirs.end() ; ++it)
    {
        // Le répertoire existe déjà
        if (*it == abs_name)
        {
            return;
        }
    }

    // On ajoute le répertoire à la liste
    m_dirs.push_back(abs_name);
}


/**
 * Enlève une archive du système de fichier.
 *
 * \param fileName Adresse du fichier de l'archive.
 ******************************/

void CFileSystem::removeArchive(const CString& fileName)
{
    CString abs_name = getAbsolutePath(fileName);

    for (std::vector<ILoaderArchive *>::iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if (getAbsolutePath((*it)->getFileName()) == abs_name)
        {
            delete (*it);
            m_archives.erase(it);
            return;
        }
    }
}


/**
 * Enlève une archive du système de fichier.
 *
 * \param archive Pointeur sur l'archive à enlever.
 ******************************/

void CFileSystem::removeArchive(ILoaderArchive * archive)
{
    for (std::vector<ILoaderArchive *>::iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if (*it == archive)
        {
            delete (*it);
            m_archives.erase(it);
            return;
        }
    }
}


/**
 * Enlève un répertoire du système de fichier.
 *
 * \param dir Nom du répertoire.
 ******************************/

void CFileSystem::removeDirectory(const CString& dir)
{
    // On garde toujours au moins un répertoire (?)
    if (m_dirs.size() <= 1)
    {
        return;
    }

    CString abs_name = getAbsolutePath(dir);

    // Parcourt de la liste des répertoires
    for (std::vector<CString>::iterator it = m_dirs.begin() ; it != m_dirs.end() ; ++it)
    {
        if (*it == abs_name)
        {
            m_dirs.erase(it);
            return;
        }
    }
}


/**
 * Indique si une archive est dans le système de fichier.
 *
 * \param fileName Adresse du fichier de l'archive.
 * \return Booléen.
 ******************************/

bool CFileSystem::archiveExists(const CString& fileName) const
{
    CString abs_name = getAbsolutePath(fileName);

    // On parcourt la liste des archives
    for(std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if (getAbsolutePath((*it)->getFileName()) == abs_name)
        {
            return true;
        }
    }

    return false;
}


/**
 * Indique si un fichier existe dans le système de fichier.
 *
 * \todo On recherche en priorité dans les dossiers avant de chercher dans les archive,
 *       peut-être faudrait-il faire l'inverse ?
 *
 * \param fileName Adresse du fichier.
 * \return Booléen.
 ******************************/

bool CFileSystem::fileExists(const CString& fileName) const
{
    // Recherche dans les répertoires
    if (fileExistsInDirs(fileName))
    {
        return true;
    }

    // Recherche dans les archives
    return fileExistsInArchives(fileName);
}


/**
 * Indique si un fichier existe dans les répertoires.
 *
 * \bug Cette méthode utilisée directement fonctionne correctement, mais pas si elle est utilisée
 *      depuis une autre méthode de la classe.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen.
 ******************************/

bool CFileSystem::fileExistsInDirs(const CString& fileName) const
{
    CString name = convertName(fileName);

    // Recherche dans les répertoires
    for (std::vector<CString>::const_iterator it = m_dirs.begin() ; it != m_dirs.end() ; ++it)
    {
        CString fullname = *it + separator + name;

        //changeDirectory(*it); // On change de répertoire
        std::ifstream file(fullname.toCharArray(), std::ios::in);
        //changeDirectory(m_dirs[0]); // On revient au répertoire courant

        if (file)
        {
            file.close();
            return true;
        }
    }

    return false;
}


/**
 * Indique si un fichier existe dans les archives.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen.
 ******************************/

bool CFileSystem::fileExistsInArchives(const CString& fileName) const
{
    CString name = convertName(fileName);

    // Recherche dans les archives
    for (std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if ((*it)->isFileExists(name))
        {
            return true;
        }
    }

    return false;
}


/**
 * Retourne le chemin du répertoire où se trouve le fichier, ou la chaine vide.
 * La chaine retournée est terminée par le caractère séparateur, ou si le
 * fichier n'existe pas, c'est la chaine vide.
 *
 * \param fileName Adresse du fichier.
 * \return Chemin du répertoire ou chaine vide.
 ******************************/

CString CFileSystem::getFileDirectory(const CString& fileName) const
{
    CString name = convertName(fileName);

    // Recherche dans les répertoires
    for (std::vector<CString>::const_iterator it = m_dirs.begin() ; it != m_dirs.end() ; ++it)
    {
        CString fullname = *it + separator + name;

        //changeDirectory(*it); // On change de répertoire
        std::fstream file(fullname.toCharArray(), std::ios::in);
        //changeDirectory(m_dirs[0]); // On revient au répertoire courant

        if (!file.fail())
        {
            file.close();
            return *it + separator;
        }
    }

    return CString();
}


/**
 * Retourne le chemin de l'archive où se trouve le fichier, ou la chaine vide.
 *
 * \param fileName Adresse du fichier.
 * \return Chemin de l'archive ou chaine vide.
 ******************************/

CString CFileSystem::getFileArchive(const CString& fileName) const
{
    CString name = convertName(fileName);

    // Recherche dans les archives
    for (std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if ((*it)->isFileExists(name))
        {
            return getAbsolutePath((*it)->getFileName());
        }
    }

    return CString();
}


/**
 * Donne la taille d'un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets.
 ******************************/

uint64_t CFileSystem::getFileSize(const CString& fileName) const
{
    CString name = convertName(fileName);

    // Recherche dans les répertoires
    for (std::vector<CString>::const_iterator it = m_dirs.begin() ; it != m_dirs.end() ; ++it)
    {
        CString fullname = *it + separator + name;

        //changeDirectory(*it); // On change de répertoire
        std::ifstream file(fullname.toCharArray(), std::ios::in | std::ios::binary);
        //changeDirectory(m_dirs[0]); // On revient au répertoire courant

        if (file)
        {
            uint64_t size = 0;

            // Taille du fichier
            file.seekg(0, std::ios_base::end);
            size = file.tellg();
            file.close();

            return size;
        }
    }

    // Recherche dans les archives
    for (std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if ((*it)->isFileExists(name))
        {
            return (*it)->getFileSize(name);
        }
    }

    return 0;
}


/**
 * Donne le répertoire de l'exécutable.
 *
 * \return Répertoire courant.
 ******************************/

CString CFileSystem::getCurrentDirectory() const
{

#if defined (T_SYSTEM_WINDOWS)

    int bufferLength = GetCurrentDirectory(0, nullptr);

    if (bufferLength > 0)
    {
        char * dir = new char[bufferLength];
        GetCurrentDirectory(bufferLength, dir);
        return dir;
    }

    return CString();

#elif defined (T_SYSTEM_LINUX)

    char * dir = new char[PATH_MAX];
    getcwd(PATH_MAX, dir);
    return dir;

#else

    return CString();

#endif

}


/**
 * Retourne le pointeur sur une archive.
 *
 * \param archive Adresse de l'archive.
 * \return Pointeur sur l'archive.
 ******************************/

ILoaderArchive * CFileSystem::getArchive(const CString& archive)
{
    CString abs_name = getAbsolutePath(archive);

    // Recherche dans les archives existantes
    for (std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin() ; it != m_archives.end() ; ++it)
    {
        if (getAbsolutePath((*it)->getFileName()) == abs_name)
        {
            return *it;
        }
    }

    // Ouverture de l'archive
    ILoaderArchive * file = ILoaderArchive::getArchiveLoader(abs_name);

    if (file && file->loadFromFile(abs_name))
    {
        m_archives.push_back(file);
        return file;
    }

    return nullptr;
}


/**
 * Inscrit la liste des archives dans le log.
 ******************************/

void CFileSystem::logArchivesList() const
{
    CApplication * app = CApplication::getApp();

    app->log(CString::fromUTF8("\nListe des archives du système de fichier :"));

    // Liste des archives
    for (std::vector<ILoaderArchive *>::const_iterator it = m_archives.begin(); it != m_archives.end(); ++it)
    {
        app->log(CString(" - %1").arg((*it)->getFileName()));
    }

    app->log("\n");
}


/**
 * Inscrit la liste des répertoires dans le log.
 ******************************/

void CFileSystem::logDirectoriesList() const
{
    CApplication * app = CApplication::getApp();

    app->log(CString::fromUTF8("\nListe des répertoires du système de fichier :"));

    // Liste des archives
    for (std::vector<CString>::const_iterator it = m_dirs.begin(); it != m_dirs.end(); ++it)
    {
        app->log(CString(" - %1").arg(*it));
    }

    app->log("\n");
}


/**
 * Retourne le chemin absolu d'un fichier.
 *
 * Si \a fileName est une adresse relative, le répertoire courant est utilisé comme point de
 * départ, le fichier n'est donc pas recherché dans le système de fichier.
 *
 * --------------
 *
 * Si \a fileName contient des caractères interdits par le système d'exploitation, une chaine
 * vide est retournée. En plus du caractère nul, Window interdit l'utilisation des caractères
 * suivants dans un nom de fichier :
 * \li \
 * \li /
 * \li :
 * \li *
 * \li ?
 * \li "
 * \li <
 * \li >
 * \li |
 *
 * --------------
 *
 * \param fileName Adresse du fichier.
 * \return Chemin absolu.
 ******************************/

CString CFileSystem::getAbsolutePath(const CString& fileName) const
{
    // Si l'adresse est vide, on retourne le répertoire courant
    if (fileName.isEmpty())
    {
        return getCurrentDirectory();
    }

    CString path = convertName(fileName);

#if defined (T_SYSTEM_LINUX)

    // L'adresse du fichier est déjà un chemin absolu si elle commence par un slash
    if (path[0] != '/')
    {
        path = getCurrentDirectory() + path;
    }

#elif defined (T_SYSTEM_WINDOWS)
/*
    if (path.Contains(':') || path.Contains('*') ||
        path.Contains('?') || path.Contains('"') ||
        path.Contains('<') || path.Contains('>') ||
        path.Contains('|'))
    {
        return CString();
    }
*/
    // L'adresse du fichier est déjà un chemin absolu si elle commence par une lettre suivie de deux points puis d'un backslash
    if (path.getSize() > 2)
    {
        if (!((path[0] >= 'a' && path[0] <= 'z') || (path[0] >= 'A' && path[0] <= 'Z') ) || path[1] != ':' || path[2] != separator)
        {
            path = getCurrentDirectory() + separator + path;
        }
    }
    else if (path.getSize() == 2)
    {
        if (((path[0] >= 'a' && path[0] <= 'z') || (path[0] >= 'A' && path[0] <= 'Z')) && path[1] == ':')
        {
            return (path + separator);
        }

        path = getCurrentDirectory() + separator + path;
    }
    else
    {
        path = getCurrentDirectory() + separator + path;
    }

#endif

    std::istringstream iss(path.toCharArray());
    std::string mot;
    std::list<CString> dossiers;

    // On découpe la chaine autour des séparateurs
    while (std::getline(iss, mot, separator))
    {
        dossiers.push_back(mot.c_str());
    }

    // On parcourt la liste des dossiers
    for (std::list<CString>::iterator it = dossiers.begin() ; it != dossiers.end() ; )
    {
        if (it->isEmpty() || *it == ".")
        {
            it = dossiers.erase(it);
        }
        else if (*it == "..")
        {
            // L'adresse est incorrecte
            if (it == dossiers.begin())
            {
                return CString();
            }

            it = dossiers.erase(it);
            --it;

            if (it == dossiers.begin())
            {
                return CString();
            }

            it = dossiers.erase(it);
        }
        else
        {
            ++it;
        }
    }

    path = CString();

    for (std::list<CString>::const_iterator it = dossiers.begin() ; it != dossiers.end() ; ++it)
    {
        if (it != dossiers.begin())
            path += separator;
        path += *it;
    }

    return path;
}


/**
 * Convertit une adresse au bon format selon le système d'exploitation.
 * Sous Windows, cette méthode remplace les anti-slash par des slash, sous les autres systèmes
 * d'exploitation, elle ne fait rien.
 *
 * \param fileName Adresse du fichier.
 * \return Adresse convertie au bon format.
 ******************************/

CString CFileSystem::convertName(const CString& fileName)
{
    CString name = fileName;

#if defined (T_SYSTEM_WINDOWS)

    // On remplace les slash par des backslash
    for(CString::iterator it = name.begin() ; it != name.end() ; ++it)
    {
        if (*it == '/')
        {
            *it = '\\';
        }
    }

#endif

    return name;
}


/**
 * Change le répertoire courant.
 *
 * \todo Revoir l'utilité de cette méthode.
 ******************************/

void CFileSystem::changeDirectory(const CString& dir)
{
#if defined (T_SYSTEM_WINDOWS)
    _chdir(dir.toCharArray());
#else
    chdir(dir.toCharArray());
#endif
}

} // Namespace Ted

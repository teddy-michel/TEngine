/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/* Copyright (C) 2004-2005 Laurent Gomila */

/**
 * \file Core/CMemoryManager.cpp
 * \date 29/12/2009 Création de la classe CMemoryManager.
 * \date 13/07/2010 Le gestionnaire de mémoire fait de nouveau partie du projet.
 */


#ifdef T_USING_MEMORY_MANAGER


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <iomanip>
#include <sstream>
#include <cstdlib>

#include "Core/CMemoryManager.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

#ifdef new
#error New est redéfini !
#endif

/**
 * Constructeur par défaut.
 ******************************/

CMemoryManager::CMemoryManager() :
m_file ("logs/memory.log")
{
    // On vérifie que le fichier est bien ouvert
    if (!m_file)
    {
        throw CLoadingFailed("memory.log", "Impossible d'accéder en écriture");
    }

    // Inscription de l'en-tête du fichier
    m_file << "  ========================================" << std::endl;
    m_file << "     TedEngine  -  Memory leak tracker" << std::endl;
    m_file << "  ========================================" << std::endl << std::endl;

    //m_nextDelete.size = 0;
    m_nextDeleteCurrent = -1;
}


/**
 * Destructeur.
 ******************************/

CMemoryManager::~CMemoryManager()
{
    reportLeaks();
}


/**
 * Renvoie l'instance de la classe.
 *
 * \return Référence sur l'instance unique de la classe.
 ******************************/

CMemoryManager& CMemoryManager::Instance()
{
    static CMemoryManager inst;
    return inst;
}


/**
 * Ajoute une allocation mémoire.
 *
 * \param size  Taille allouée.
 * \param file  Fichier contenant l'allocation.
 * \param line  Ligne de l'allocation.
 * \param array True si c'est une allocation de tableau.
 * \return Pointeur sur la mémoire allouée.
 ******************************/

void * CMemoryManager::allocate(std::size_t size, const std::string& file, int line, bool array)
{
    // Allocation de la mémoire
    void * ptr = ::malloc(size);

    if (size > 40000)
    {
        m_file << "ACHTUNG : gros bloc" << std::endl;
    }

    // Ajout du bloc à la liste des blocs alloués
    TBlock new_block;
    new_block.size = size;
    new_block.file = file;
    new_block.line = line;
    new_block.array = array;
    m_blocks[ptr] = new_block;

    // Log
    m_file << " + Alloc | " << std::left <<std::setw(10) << std::setfill(' ') << ptr << std::right
           << " | " << std::setw(6) << std::setfill(' ') << static_cast<int>(new_block.size) << " o"
           << " | " << new_block.file << " (" << new_block.line << ")" << std::endl;

    return ptr;
}


/**
 * Retire une allocation mémoire.
 *
 * \param ptr   Adresse de la mémoire desallouée.
 * \param array True si c'est une désallocation de tableau.
 ******************************/

void CMemoryManager::Free(void * ptr, bool array)
{
    //m_file << "CMemoryManager::Free" <<std::endl;

    // Recherche de l'adresse dans les blocs alloués
    TBlockMap::iterator it = m_blocks.find(ptr);

    // Si le bloc n'a pas été alloué, on génère une erreur
    if (it == m_blocks.end())
    {
        // En fait ça arrive souvent, du fait que le delete surchargé est pris en compte
        // même là où on n'inclue pas DebugNew.h, mais pas la macro pour le new.
        // Dans ce cas on détruit le bloc et on quitte immédiatement

        m_file << " - Free  | " << std::left <<std::setw(10) << std::setfill(' ') << ptr << std::right << " |      ? o | ?" << std::endl;

        ::free(ptr);
        return;
    }

    // Si le type d'allocation ne correspond pas, on génère une erreur
    if (it->second.array != array)
    {
        throw CBadDelete(ptr, it->second.file, it->second.line, !array);
    }

    if (m_nextDeleteCurrent < 0)
    {
        //m_file << "Erreur : m_nextDeleteCurrent < 0" <<std::endl;
        m_nextDeleteCurrent = 0;

        // Finalement, si tout va bien, on supprime le bloc et on log tout ça
        m_file << " - Free  | " << std::left << std::setw(10) << std::setfill(' ') << ptr << std::right
           << " | " << std::setw(6) << std::setfill(' ') << static_cast<int>(it->second.size) << " o"
           //<< " | " << m_nextDelete.file << " (" << m_nextDelete.line << ")" << std::endl;
           << " | ?" << std::endl;
    }
    else
    {
        // Finalement, si tout va bien, on supprime le bloc et on log tout ça
        m_file << " - Free  | " << std::left << std::setw(10) << std::setfill(' ') << ptr << std::right
           << " | " << std::setw(6) << std::setfill(' ') << static_cast<int>(it->second.size) << " o"
           //<< " | " << m_nextDelete.file << " (" << m_nextDelete.line << ")" << std::endl;
           << " | " << m_nextDelete[m_nextDeleteCurrent].file << " (" << m_nextDelete[m_nextDeleteCurrent].line << ")" << std::endl;

        --m_nextDeleteCurrent;
    }

    //m_file << "m_nextDeleteCurrent = " << m_nextDeleteCurrent <<std::endl;



    m_blocks.erase(it);
    //m_nextDelete.size = 0;

    // Libération de la mémoire
    ::free(ptr);
}


/**
 * Sauvegarde les infos sur la désallocation en cours.
 *
 * \param file Fichier contenant la désallocation.
 * \param line Ligne de la désallocation.
 ******************************/

void CMemoryManager::nextDelete(const std::string& file, int line)
{
    //m_file << "CMemoryManager::nextDelete" <<std::endl;
/*
    TBlock block;
    block.file = file;
    block.line = line;
    m_deleteStack.push(block);
*/
    if (m_nextDeleteCurrent >= nextDeleteCount - 1)
    {
        //if (m_nextDelete.size != 0)
        m_file << "Erreur : nextDelete en cours..." <<std::endl;
        m_nextDelete[m_nextDeleteCurrent].size = 1;
        m_nextDelete[m_nextDeleteCurrent].file = file;
        m_nextDelete[m_nextDeleteCurrent].line = line;
    }
    else
    {
        ++m_nextDeleteCurrent;
        m_nextDelete[m_nextDeleteCurrent].size = 1;
        m_nextDelete[m_nextDeleteCurrent].file = file;
        m_nextDelete[m_nextDeleteCurrent].line = line;
    }
/*
    m_nextDelete.size = 1;
    m_nextDelete.file = file;
    m_nextDelete.line = line;
*/
    //m_file << "m_nextDeleteCurrent = " << m_nextDeleteCurrent <<std::endl;
}


/**
 * Inscrit le rapport sur les fuites de mémoire.
 ******************************/

void CMemoryManager::reportLeaks()
{
    // Détail des fuites
    std::size_t total_size = 0;

    for (TBlockMap::iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
    {
        // Ajout de la taille du bloc au cumul
        total_size += it->second.size;

        // Inscription dans le fichier des informations sur le bloc courant
        m_file << " ! Leak  | " << std::left << std::setw(10) << std::setfill(' ') << it->first << std::right
               << " | " << std::setw(6) << std::setfill(' ') << static_cast<int>(it->second.size) << " o"
               << " | " << it->second.file << " (" << it->second.line << ")" << std::endl;

        // Libération de la mémoire
        //::free(it->first);
    }

    // Affichage du cumul des fuites
    m_file << std::endl << std::endl << "-- "
           << static_cast<int>(m_blocks.size()) << " blocs non-libéré(s), "
           << static_cast<int>(total_size)      << " octets --"
           << std::endl;
}

} // namespace Ted

#endif // T_USING_MEMORY_MANAGER

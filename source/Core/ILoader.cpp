/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILoader.cpp
 * \date       2008 Création de la classe ILoader.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 *                  Création de la méthode SaveToFile.
 *                  Les messages d'erreurs sont envoyés à la console.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/ILoader.hpp"
#include "Core/Exceptions.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

ILoader::ILoader() :
m_fileName ()
{ }


/**
 * Destructeur.
 ******************************/

ILoader::~ILoader()
{ }


/**
 * Retourne le nom du fichier.
 *
 * \return Nom du fichier.
 ******************************/

CString ILoader::getFileName() const
{
    return m_fileName;
}


/**
 * Sauvegarde les données dans un fichier.
 *
 * \param fileName Adresse du fichier.
 ******************************/

bool ILoader::saveToFile(const CString& fileName) const
{
    T_UNUSED(fileName);
    CApplication::getApp()->log(CString::fromUTF8("La sauvegarde de ce type de fichier n'est pas implémentée"), ILogger::Error);
    return false;
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/ILogger.cpp
 * \date       2008 Création de la classe ILogger.
 * \date 02/10/2011 Ajout de la méthode hasLogger.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <stdio.h>

#include "Core/ILogger.hpp"
#include "Core/CDateTime.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

ILogger::ILogger()
{ }


/**
 * Destructeur.
 ******************************/

ILogger::~ILogger()
{ }


/**
 * Renvoie la date courante.
 *
 * \return Date courante sous forme de chaîne de caractères.
 ******************************/

CString ILogger::date()
{
    return CDateTime::getCurrentDate();
}


/**
 * Renvoie l'heure courante.
 *
 * \return Heure courante sous forme de chaîne de caractères.
 ******************************/

CString ILogger::time()
{
    return CDateTime::getCurrentTime();
}

} // Namespace Ted

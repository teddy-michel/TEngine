/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoader7Z.cpp
 * \date 31/01/2011 Création de la classe CLoader7Z.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CLoader7Z.hpp"
#include "Core/ILogger.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Constructeur.
 ******************************/

CLoader7Z::CLoader7Z() :
ILoaderArchive()
{ }


/**
 * Donne le contenu d'un fichier.
 *
 * \todo Implémentation.
 *
 * \param fileName Nom du fichier.
 * \param data     Tableau contenant les données du fichier.
 * \param size     Taille du tableau (0 si le fichier n'existe pas).
 * \return Booléen indiquant si le fichier a pu être chargé.
 ******************************/

bool CLoader7Z::getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size)
{
    T_UNUSED_UNIMPLEMENTED(fileName);
    T_UNUSED_UNIMPLEMENTED(data);

    //CString name = CFileSystem::convertName(fileName);

    *size = 0;

    //...

    return false;
}


/**
 * Donne la taille d'un fichier de l'archive.
 *
 * \todo Implémentation.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets.
 ******************************/

uint64_t CLoader7Z::getFileSize(const CString& fileName)
{
    T_UNUSED_UNIMPLEMENTED(fileName);

    //CString name = CFileSystem::convertName(fileName);

    //...

    return 0;
}


/**
 * Chargement de la liste des fichiers.
 *
 * \todo Implémentation.
 *
 * \param fileName Adresse du fichier de l'archive.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoader7Z::loadFromFile(const CString& fileName)
{
#ifdef T_DEBUG
    CApplication::getApp()->log(CString::fromUTF8("CLoader7Z::loadFromFile : Lecture du fichier %1").arg(fileName));
#endif

    m_file.close();
    m_file.open(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!m_file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoader7Z::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // On efface toutes les données
    m_files.clear();

    // Lecture de l'en-tête
    TSignatureHeader sighead;
    m_file.read(reinterpret_cast<char *>(&sighead), sizeof(TSignatureHeader));

    // Signature incorrecte
    if (sighead.signature[0] != 0x37 || sighead.signature[1] != 0x7A ||
        sighead.signature[2] != 0xBC || sighead.signature[3] != 0xAF ||
        sighead.signature[4] != 0x27 || sighead.signature[5] != 0x1C)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoader7Z::loadFromFile : la signature du fichier %1 ne correspond pas à un fichier 7Z").arg(fileName), ILogger::Error);
        return false;
    }

    // Version incorrecte
    if (sighead.version_major != 0 || (sighead.version_minor != 2 && sighead.version_minor != 3))
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoader7Z::loadFromFile : La version du fichier %1 est incorrecte (%2.%3)").arg(fileName).arg(sighead.version_major).arg(sighead.version_minor), ILogger::Error);
        return false;
    }

#ifdef T_DEBUG
CApplication::getApp()->log(CString("CLoader7Z::loadFromFile : start_header_crc   = ").arg(sighead.start_header_crc), ILogger::Debug);
CApplication::getApp()->log(CString("CLoader7Z::loadFromFile : next_header_offset = ").arg(sighead.next_header_offset), ILogger::Debug);
CApplication::getApp()->log(CString("CLoader7Z::loadFromFile : next_header_size   = ").arg(sighead.next_header_size), ILogger::Debug);
CApplication::getApp()->log(CString("CLoader7Z::loadFromFile : next_header_crc    = ").arg(sighead.next_header_crc), ILogger::Debug);
#endif

    // Vérification du CRC
    //sighead.start_header_crc

    ReadBloc(sizeof(TSignatureHeader) + sighead.next_header_offset, sighead.next_header_size);
/*
    char * header = new char[sighead.next_header_size];

    m_file.seekg(sighead.next_header_offset, std::ios_base::cur);
    m_file.read(header, sighead.next_header_size);

    if (header[0] == kHeader)
    {
ILogger::log() << "Lecture d'un kHeader...\n";
    }
ILogger::log() << "header[0] = " << header[0] << "\n\n";

    delete[] header;
*/
    return true;
}


/**
 * Indique si le fichier contient un modèle pouvant être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CLoader7Z::isCorrectFormat(const char * fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName, std::ios::in | std::ios::binary);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    // Lecture de l'en-tête
    TSignatureHeader sighead;
    file.read(reinterpret_cast<char *>(&sighead), sizeof(TSignatureHeader));

    if (!file.good())
    {
        file.close();
        return false;
    }

    file.close();

    // Signature incorrecte
    if (sighead.signature[0] != 0x37 || sighead.signature[1] != 0x7A ||
        sighead.signature[2] != 0xBC || sighead.signature[3] != 0xAF ||
        sighead.signature[4] != 0x27 || sighead.signature[5] != 0x1C)
    {
        return false;
    }

    // Version incorrecte
    if (sighead.version_major != 0 || (sighead.version_minor != 2 && sighead.version_minor != 3))
    {
        return false;
    }

    return true;
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
ILoaderArchive * CLoader7Z::createInstance(const char * fileName)
#else
CLoader7Z * CLoader7Z::createInstance(const char * fileName)
#endif
{
    return (CLoader7Z::isCorrectFormat(fileName) ? new CLoader7Z() : nullptr);
}


/**
 * Lit un bloc dans le fichier.
 *
 * \param offset Offset depuis le début du fichier.
 * \param size   Nombre d'octets à lire dans le fichier.
 ******************************/

void CLoader7Z::ReadBloc(const std::streampos offset, const std::size_t size)
{
#ifdef T_DEBUG
    CApplication::getApp()->log(CString("Lecture de %1 octets depuis l'offset %2").arg(size).arg(offset), ILogger::Debug);
#endif

    if (size == 0)
        return;

    char * header = new char[size];

    m_file.seekg(offset, std::ios_base::beg);
    m_file.read(header, size);

    try
    {
        switch (header[0])
        {
            case kHeader:
                ReadHeader(header);
                break;

            case kEncodedHeader:
                ReadEncodedHeader(header);
                break;
        }
    }
    catch (...)
    {
        //...
    }

    delete[] header;
    return;
}


/**
 * Convertit un tableau de 4 caractères en un entier.
 *
 * \todo Implémentation.
 *
 * \param buf Tableau de 4 caractères.
 * \return Entier.
 ******************************/

uint32_t CLoader7Z::ConvertUint32(char buf[4])
{
    uint32_t result = 0;

//ILogger::log() << " " << buf[0] << " " << buf[1] << " " << buf[2] << " " << buf[3] << " " << buf[4] << " " << buf[5] << " " << buf[6] << " " << buf[7] << "\n";

    for (int i = 0; i < 4; ++i)
    {
        result <<= 8;
        result += buf[i];
    }

    return result;
}


/**
 * Convertit un tableau de 8 caractères en un entier long.
 *
 * \todo Implémentation.
 *
 * \param buf Tableau de 8 caractères.
 * \return Entier long.
 ******************************/

uint64_t CLoader7Z::ConvertUint64(char buf[8])
{
    uint64_t result = 0;

//ILogger::log() << " " << buf[0] << " " << buf[1] << " " << buf[2] << " " << buf[3] << " " << buf[4] << " " << buf[5] << " " << buf[6] << " " << buf[7] << "\n";

    for (int i = 0; i < 8; ++i)
    {
        result <<= 8;
        result += buf[i];
    }

    return result;
}


/**
 * Lit un bloc de type Header dans le fichier.
 *
 * \param buf Pointeur sur le buffer à lire.
 * \return Pointeur sur la fin du buffer.
 ******************************/

char * CLoader7Z::ReadHeader(char * buf)
{
//ILogger::log() << "ReadHeader()\n";

    if (*buf != kHeader)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x01]\n";

    ++buf;

    if (*buf == kArchiveProperties)
    {
        buf = ReadArchiveProperties(buf);
    }

    if (*buf == kAdditionalStreamsInfo)
    {
        buf = ReadAdditionalStreamsInfo(buf);
    }

    if (*buf == kMainStreamsInfo)
    {
        buf = ReadMainStreamsInfo(buf);
    }

    if (*buf == kFilesInfo)
    {
        buf = ReadFilesInfo(buf);
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x00]\n";

    ++buf;

    return buf;
}

char * CLoader7Z::ReadArchiveProperties(char * buf)
{
//ILogger::log() << "ReadArchiveProperties()\n";

    if (*buf != kArchiveProperties)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

    while (true)
    {
        if (*buf == 0) break;
        uint64_t property_size = ConvertUint64(buf);
        buf += 8;
//ILogger::log() << "[property_size="<<property_size<<"]\n";
        for (uint64_t i = 0; i < property_size; ++i)
        {
            //char propertyData = *buf;
//ILogger::log() << "["<<propertyData<<"]\n";
            ++buf;
        }
    }

    return buf;
}

char * CLoader7Z::ReadAdditionalStreamsInfo(char * buf)
{
//ILogger::log() << "ReadAdditionalStreamsInfo()\n";

    if (*buf != kAdditionalStreamsInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

    if (*buf == kPackInfo)
    {
        buf = ReadPackInfo(buf);
    }

    if (*buf == kUnPackInfo)
    {
        buf = ReadUnPackInfo(buf);
    }

    if (*buf == kSubStreamsInfo)
    {
        buf = ReadSubStreamsInfo(buf);
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    return buf;
}

char * CLoader7Z::ReadMainStreamsInfo(char * buf)
{
//ILogger::log() << "ReadMainStreamsInfo()\n";

    if (*buf != kMainStreamsInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x04]\n";

    ++buf;

    if (*buf == kPackInfo)
    {
        buf = ReadPackInfo(buf);
    }

    if (*buf == kUnPackInfo)
    {
        buf = ReadUnPackInfo(buf);
    }

    if (*buf == kSubStreamsInfo)
    {
        buf = ReadSubStreamsInfo(buf);
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x00]\n";

    ++buf;

    return buf;
}

char * CLoader7Z::ReadFilesInfo(char * buf)
{
//ILogger::log() << "ReadFilesInfo()\n";

    if (*buf != kFilesInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x05]\n";

    ++buf;

    /*uint64_t numFiles =*/ ConvertUint64(buf);
    buf += 8;

//ILogger::log() << "[num_files="<<numFiles<<"]\n";

/*
  for (;;)
  {
    BYTE PropertyType;
    if (aType == 0)
      break;

    UINT64 Size;

    switch(PropertyType)
    {
      kEmptyStream:   (0x0E)
        for(NumFiles)
          BIT IsEmptyStream

      kEmptyFile:     (0x0F)
        for(EmptyStreams)
          BIT IsEmptyFile

      kAnti:          (0x10)
        for(EmptyStreams)
          BIT IsAntiFile

      case kCTime: (0x12)
      case kATime: (0x13)
      case kMTime: (0x14)
        BYTE AllAreDefined
        if (AllAreDefined == 0)
        {
          for(NumFiles)
            BIT TimeDefined
        }
        BYTE External;
        if(External != 0)
          UINT64 DataIndex
        []
        for(Definded Items)
          UINT64 Time
        []

      kNames:     (0x11)
        BYTE External;
        if(External != 0)
          UINT64 DataIndex
        []
        for(Files)
        {
          wchar_t Names[NameSize];
          wchar_t 0;
        }
        []

      kAttributes:  (0x15)
        BYTE AllAreDefined
        if (AllAreDefined == 0)
        {
          for(NumFiles)
            BIT AttributesAreDefined
        }
        BYTE External;
        if(External != 0)
          UINT64 DataIndex
        []
        for(Definded Attributes)
          UINT32 Attributes
        []
    }
  }

*/

    return buf;
}

char * CLoader7Z::ReadPackInfo(char * buf)
{
//ILogger::log() << "ReadPackInfo()\n";

    if (*buf != kPackInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x06]\n";

    ++buf;

    /*uint64_t pack_pos =*/ ConvertUint64(buf);
//ILogger::log() << "[pack_pos="<<pack_pos<<"]\n";
    buf += 8;

    uint64_t num_pack_streams = ConvertUint64(buf);
//ILogger::log() << "[num_pack_streams="<<num_pack_streams<<"]\n";
    buf += 8;

    if (*buf == kSize)
    {
//ILogger::log() << "[0x09]\n";
        for (uint64_t i = 0; i < num_pack_streams; ++i)
        {
            //uint64_t pack_sizes[i] = ConvertUint64(buf);
            buf += 8;
        }
    }

    if (*buf == kCRC)
    {
//ILogger::log() << "[0x0A]\n";
        for (uint64_t i = 0; i < num_pack_streams; ++i)
        {
            uint8_t all_are_defined = *buf;
            ++buf;

            if (all_are_defined == 0)
            {
/*
                for (NumStreams)
                    BIT Defined
*/
            }

            unsigned int num_defined = 0; //???

            for (unsigned int j = 0; j < num_defined; ++j)
            {
                //uint32_t crc = ConvertUint32(buf);
                buf += 4; // ?
            }
        }
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x00]\n";

    ++buf;

    return buf;
}


char * CLoader7Z::ReadUnPackInfo(char * buf)
{
//ILogger::log() << "ReadUnPackInfo()\n";

    if (*buf != kUnPackInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

    if (*buf != kFolder)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

    uint64_t num_folders = ConvertUint64(buf);
    buf += 8;

    uint8_t external = *buf;
    ++buf;

    switch (external)
    {
        case 0:

            for (uint64_t i = 0; i < num_folders; ++i)
            {
                buf = ReadFolder(buf);
            }

            break;

        case 1:
        {
            //uint64_t data_stream_index = ConvertUint64(buf);
            buf += 8;
            break;
        }

        default:
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
            throw "Fichier incorrect";
            return buf;
    }

    if (*buf != kCodersUnPackSize)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

/*
  for(Folders)
    for(Folder.NumOutStreams)
     UINT64 UnPackSize;
*/

    if (*buf == kCRC)
    {
        //UnPackDigests[NumFolders]
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

    ++buf;

    return buf;
}

char * CLoader7Z::ReadSubStreamsInfo(char * buf)
{
//ILogger::log() << "ReadSubStreamsInfo()\n";

    if (*buf != kSubStreamsInfo)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x08]\n";

    ++buf;

    if (*buf == kNumUnPackStream)
    {
//ILogger::log() << "[0x0D]\n";

        ++buf;
/*
        for (unsigned int i = 0; i < num_folders; ++i)
        {
            uint64_t = ConvertUint64(buf);
            buf += 8;
        }
*/
    }

    if (*buf == kSize)
    {
//ILogger::log() << "[0x09]\n";

        ++buf;

        //UINT64 UnPackSizes[]
    }

    if (*buf == kCRC)
    {
//ILogger::log() << "[0x0A]\n";

        ++buf;

        //Digests[Number of streams with unknown CRC]

        if (*buf == 0)
        {
//ILogger::log() << "[0x00]\n";
            ++buf;

            unsigned int num_streams = 0; //?

            for (unsigned int i = 0; i < num_streams; ++i)
            {
                //BIT Defined ??
            }
        }
        else
        {
//ILogger::log() << "[0x??]\n";
            ++buf;
        }

        unsigned int num_defined = 1; //?

        for (unsigned int i = 0; i < num_defined; ++i)
        {
            //uint32_t crc = ConvertUint32(buf);
//ILogger::log() << "[0x??]\n";
//ILogger::log() << "[0x??]\n";
//ILogger::log() << "[0x??]\n";
//ILogger::log() << "[0x??]\n";
            buf += 4;
        }
    }

    if (*buf != kEnd)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x00]\n";

    ++buf;

    return buf;
}

char * CLoader7Z::ReadFolder(char * buf)
{
//ILogger::log() << "ReadFolder()\n";

    uint64_t num_coders = ConvertUint64(buf);
    buf += 8;

    uint64_t num_in_streams_total = 0;
    uint64_t num_out_streams_total = 0;

    for (uint64_t i = 0; i < num_coders; ++i)
    {
        uint8_t param = *buf;
        ++buf;

        uint8_t codec_id_size = (param << 4); //TODO: VERIFIER LE SENS

        for (uint8_t j = 0; j < codec_id_size; ++j)
        {
            //char codec_id = *buf;
            ++buf;
        }

        if (param & 0x10) //TODO: VERIFIER LA VALEUR
        {
            uint64_t num_in_streams = ConvertUint64(buf);
            buf += 8;
            uint64_t num_out_streams = ConvertUint64(buf);
            buf += 8;

            num_in_streams_total += num_in_streams;
            num_out_streams_total += num_out_streams;
        }

        if (param & 0x20) //TODO: VERIFIER LA VALEUR
        {
            uint64_t property_size = ConvertUint64(buf);
            buf += 8;

            for (uint64_t j = 0; j < property_size; ++j)
            {
                //uint8_t properties = *buf;
                ++buf;
            }
        }
    }

    uint64_t num_bind_pairs = num_out_streams_total - 1;

    for (uint64_t i = 0; i < num_bind_pairs; ++i)
    {
/*
        UINT64 InIndex;
        UINT64 OutIndex;
*/
    }

    uint64_t num_packed_streams = num_in_streams_total - num_bind_pairs;

    if (num_packed_streams > 1)
    {
        for (uint64_t i = 0; i < num_packed_streams; ++i)
        {
            //uint64_t index = ConvertUint64(buf);
            buf += 8;
        }
    }

    return buf;
}

char * CLoader7Z::ReadEncodedHeader(char * buf)
{
//ILogger::log() << "ReadEncodedHeader()\n";

    if (*buf != kEncodedHeader)
    {
//ILogger::log() << "Erreur ! ("<<__LINE__<<")\n";
        throw "Fichier incorrect";
        return buf;
    }

//ILogger::log() << "[0x17]\n";

    ++buf;

    //...

    return buf;
}

} // Namespace Ted

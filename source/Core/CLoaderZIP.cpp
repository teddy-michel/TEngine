/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderZIP.cpp
 * \date 10/01/2010 Création de la classe CLoaderZIP.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 * \date 12/04/2012 La méthode getFileContent vide le tableau avant de commencer.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

//#include <zlib.h>

#include "Core/CLoaderZIP.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/Utils.hpp"
#include "Core/Exceptions.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "os.h"


namespace Ted
{

/**
 * Constructeur.
 ******************************/

CLoaderZIP::CLoaderZIP() :
ILoaderArchive ()
{ }


/**
 * Donne le contenu d'un fichier.
 *
 * \todo Lire les fichiers compressés.
 *
 * \param fileName Nom du fichier.
 * \param data     Tableau contenant les données du fichier.
 * \param size     Taille du tableau (0 si le fichier n'existe pas).
 * \return Booléen indiquant si le fichier a pu être chargé.
 ******************************/

bool CLoaderZIP::getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size)
{
    *size = 0;
    data.clear();

    if (!m_file)
    {
        return false;
    }

    CString name = CFileSystem::convertName(fileName);

    std::size_t filenum = 0;

    // On met la chaine en minuscules
#if defined (T_SYSTEM_WINDOWS)
    name = name.toLowerCase();
#endif

    // On cherche le fichier dans la liste des fichiers
    const std::size_t num_files = m_files.size();

    for (std::size_t i = 0; i < num_files; ++i)
    {
        // On a trouvé le fichier
#if defined (T_SYSTEM_WINDOWS)
        if (m_files[i].toLowerCase() == name)
#else
        if (m_files[i] == name)
#endif
        {
            filenum = i + 1;
            break;
        }
    }

    T_ASSERT(filenum <= m_lengths.size());

    // Le fichier n'existe pas
    if (filenum == 0)
    {
        return false;
    }

    *size = m_lengths[filenum - 1];
    data.reserve(*size);

    // On se rend à l'offset du fichier
    m_file.seekg(m_offsets[filenum - 1]);

    // On lit caractère par caractère
    for (uint64_t i = 0; i < *size; ++i)
    {
        char data_n;
        m_file.read(reinterpret_cast<char *>(&data_n), 1);
        data.push_back(data_n);
    }

    return true;
}


/**
 * Donne la taille d'un fichier de l'archive.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets, 0 si le fichier n'existe pas.
 ******************************/

uint64_t CLoaderZIP::getFileSize(const CString& fileName)
{
    CString name = CFileSystem::convertName(fileName);

    // On met la chaine en minuscules
#if defined (T_SYSTEM_WINDOWS)
    name = name.toLowerCase();
#endif

    unsigned int filenum = 0;

    // On cherche le fichier dans la liste des fichiers
    for (std::vector<CString>::iterator it = m_files.begin() ; it != m_files.end() ; ++it)
    {
        // On a trouvé le fichier
#if defined (T_SYSTEM_WINDOWS)
        if (it->toLowerCase() == name)
#else
        if (*it == name)
#endif
        {
            filenum = std::distance(m_files.begin(), it) + 1;
        }
    }

    // Le fichier n'existe pas
    if (filenum == 0)
    {
        return 0;
    }

    if (m_lengths.size() > filenum)
    {
        return m_lengths[filenum - 1];
    }

    return 0;
}

/*
int unzLocateFile(unzFile file, const char * szFileName, int iCaseSensitivity)
{
    unz64_s* s;
    int err;

    // We remember the 'current' position in the file so that we can jump back there if we fail.
    unz_file_info64 cur_file_infoSaved;
    unz_file_info64_internal cur_file_info_internalSaved;
    ZPOS64_T num_fileSaved;
    ZPOS64_T pos_in_central_dirSaved;


    if (file == nullptr)
        return UNZ_PARAMERROR;

    if (strlen(szFileName) >= UNZ_MAXFILENAMEINZIP)
        return UNZ_PARAMERROR;

    s = (unz64_s*)file;
    if (!s->current_file_ok)
        return UNZ_END_OF_LIST_OF_FILE;

    // Save the current state
    num_fileSaved = s->num_file;
    pos_in_central_dirSaved = s->pos_in_central_dir;
    cur_file_infoSaved = s->cur_file_info;
    cur_file_info_internalSaved = s->cur_file_info_internal;

    err = unzGoToFirstFile(file);

    while (err == UNZ_OK)
    {
        char szCurrentFileName[UNZ_MAXFILENAMEINZIP+1];
        err = unzGetCurrentFileInfo64(file,nullptr,
                                    szCurrentFileName,sizeof(szCurrentFileName)-1,
                                    nullptr,0,nullptr,0);
        if (err == UNZ_OK)
        {
            if (unzStringFileNameCompare(szCurrentFileName, szFileName,iCaseSensitivity) == 0)
                return UNZ_OK;
            err = unzGoToNextFile(file);
        }
    }

    //We failed, so restore the state of the 'current file' to where we were.
    s->num_file = num_fileSaved ;
    s->pos_in_central_dir = pos_in_central_dirSaved ;
    s->cur_file_info = cur_file_infoSaved;
    s->cur_file_info_internal = cur_file_info_internalSaved;
    return err;
}
*/

/*
int do_extract_onefile(unzFile uf, const char * fileName, int opt_extract_without_path, int opt_overwrite, const char * password)
{
    int err = UNZ_OK;

    if (unzLocateFile(uf,fileName,CASESENSITIVITY) != UNZ_OK)
    {
        printf("file %s not found in the zipfile\n",fileName);
        return 2;
    }

    if (do_extract_currentfile(uf,&opt_extract_without_path, &opt_overwrite, password) == UNZ_OK)
        return 0;
    else
        return 1;
}
*/

/**
 * Chargement de la liste des fichiers.
 *
 * \param fileName Adresse du fichier de l'archive.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderZIP::loadFromFile(const CString& fileName)
{
    //char * uf = unzOpen64(fileName);
    //do_extract_onefile(uf, fileName_to_extract, opt_do_extract_withoutpath, opt_overwrite, password);

    m_file.open(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!m_file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderZIP::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // On efface toutes les données
    m_files.clear();
    m_offsets.clear();
    m_lengths.clear();

    // On calcule la taille du fichier
    m_file.seekg(0, std::ios_base::end);
    unsigned int size = m_file.tellg();

    // On lit le ZIP_EndOfCentralDirRecord
    ZIP_EndOfCentralDirRecord infos;
    m_file.seekg(-22, std::ios_base::end);
    m_file.read(reinterpret_cast<char *>(&infos), sizeof(ZIP_EndOfCentralDirRecord));

    // Vérification de la signature
    if (infos.signature != ('P' + ('K'<<8) + (5<<16) + (6<<24)))
    {
        CApplication::getApp()->log(CString("CLoaderZIP::loadFromFile : la signature du ZIP_EndOfCentralDirRecord est incorrecte (%1)").arg(infos.signature), ILogger::Warning);

        // On cherche le début de la structure un peu avant
        int i = 0;

        while (++i < (int) size - 22)
        {
            m_file.seekg(-22 - i, std::ios_base::end); // On revient un caractère avant
            m_file.read(reinterpret_cast<char *>(&infos), sizeof(ZIP_EndOfCentralDirRecord));

            if (infos.signature == ('P' + ('K'<<8) + (5<<16) + (6<<24)))
            {
                if (infos.comment_length != i)
                    return false;

                CApplication::getApp()->log(CString::fromUTF8("CLoaderZIP::loadFromFile : signature correcte trouvée %1 octets avant à cause d'un commentaire").arg(i), ILogger::Warning);

                i = 0;
                break;
            }
        }

        if (i != 0)
            return false;
    }

    m_offsets.reserve(infos.central_directory_entries_total);
    m_lengths.reserve(infos.central_directory_entries_total);

    unsigned int offset = infos.start_of_central_dir_offset;

    // Lecture de la liste des fichiers
    for (unsigned int i = 0; i < infos.central_directory_entries_total; ++i)
    {
        ZIP_FileHeader file_infos;

        m_file.seekg(offset, std::ios_base::beg);
        m_file.read(reinterpret_cast<char *>(&file_infos), sizeof(ZIP_FileHeader));

        // Vérification de la signature
        if (file_infos.signature != ('P' + ('K'<<8) + (1<<16) + (2<<24)))
        {
            CApplication::getApp()->log(CString("CLoaderZIP::loadFromFile : la signature du ZIP_FileHeader est incorrecte (%1)").arg(file_infos.signature), ILogger::Error);
            return false;
        }
/*
        // Vérification de la méthode de compression
        if (file_infos.compression_method != 0 || file_infos.compressed_size != file_infos.uncompressed_size)
        {
            CApplication::getApp()->log(CString::fromUTF8("La méthode de compression du fichier ZIP est incorrecte (%1).").arg(file_infos.compression_method), ILogger::Error);
            return false;
        }
*/
        // Longueur du nom du fichier
        if (file_infos.file_name_length >= 128)
        {
            CApplication::getApp()->log(CString("CLoaderZIP::loadFromFile : le nom du fichier de l'archive ZIP est trop long (%1 caractères).").arg(file_infos.file_name_length), ILogger::Error);
            return false;
        }

        offset += sizeof(ZIP_FileHeader);
        m_file.seekg(offset, std::ios_base::beg);

        // Nom du fichier
        char file_name[128] = "";
        m_file.read(reinterpret_cast<char *>(&file_name), file_infos.file_name_length);

        CString file_name_new = file_name;

        // Conversions spécifiques à Windows
#if defined (T_SYSTEM_WINDOWS)

        // On remplace les slash par des backslash
        /// \todo Utiliser la classe CDirectory ou CFile ou faire cela
        for (CString::iterator it = file_name_new.begin(); it != file_name_new.end(); ++it)
        {
            if (*it == '/')
                *it = '\\';
        }

        // On met la chaine en minuscules
        //file_name_new = toLower(file_name_new);
#endif

        m_files.push_back(file_name_new);
        m_lengths.push_back(file_infos.uncompressed_size);

        // Lecture des informations du fichier
        m_file.seekg(file_infos.relative_offset_of_local_header);
        ZIP_LocalFileHeader file_content;
        m_file.read(reinterpret_cast<char *>(&file_content), sizeof(ZIP_LocalFileHeader));

        // Vérification de la signature
        if (file_content.signature != ('P' + ('K'<<8) + (3<<16) + (4<<24)))
        {
            CApplication::getApp()->log(CString("CLoaderZIP::loadFromFile : la signature du ZIP_LocalFileHeader est incorrecte (%1)").arg(file_infos.signature), ILogger::Error);
            return false;
        }

        m_offsets.push_back(file_infos.relative_offset_of_local_header + sizeof(ZIP_LocalFileHeader) + file_content.file_name_length + file_content.extra_field_length);

        // On passe au fichier suivant
        offset += file_infos.file_name_length + file_infos.extra_field_length + file_infos.file_comment_length;
    }

    return true;
}


/**
 * Indique si le fichier est une archive ZIP correcte.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CLoaderZIP::isCorrectFormat(const char * fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName, std::ios::in | std::ios::binary);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    uint32_t signature;
    file.read(reinterpret_cast<char *>(&signature), 4);

    if (!file.good())
    {
        file.close();
        return false;
    }

    file.close();

    return (signature == ('P' + ('K'<<8) + (1<<16) + (2<<24) ) ||
            signature == ('P' + ('K'<<8) + (3<<16) + (4<<24) ) ||
            signature == ('P' + ('K'<<8) + (5<<16) + (6<<24) ) );
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le modèle ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
ILoaderArchive * CLoaderZIP::createInstance(const char * fileName)
#else
CLoaderZIP * CLoaderZIP::createInstance(const char * fileName)
#endif
{
    return (CLoaderZIP::isCorrectFormat(fileName) ? new CLoaderZIP() : nullptr);
}

} // Namespace Ted

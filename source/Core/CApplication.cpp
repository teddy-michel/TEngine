/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CApplication.cpp
 * \date       2008 Création de la classe Application.
 * \date 15/07/2010 La classe Application est renommée en CApplication.
 * \date 18/07/2010 Utilisation du fichier Time.
 * \date 19/07/2010 La méthode ShowCursor est déplacée du GuiEngine vers cette classe.
 * \date 23/07/2010 Modification de la gestion de la fenêtre.
 * \date 24/07/2010 Modification de la gestion des évènements du clavier.
 * \date 14/02/2011 Le chargement d'une map se fait avec le même IMap.
 * \date 27/02/2011 Suppression de la méthode NewGame.
 * \date 28/02/2011 Les données de la map sont correctement supprimées lorsqu'on quitte l'application.
 * \date 11/03/2011 Création de la méthode SetMouseCursor.
 * \date 14/03/2011 Implémentation de GetSupportedResolutions et GetSupportedVideoModes sous Linux.
 * \date 03/04/2011 Ajout des méthodes pour gérer les actions associées aux touches.
 * \date 28/09/2011 Le fichier de log a dans son nom la date et l'heure.
 * \date 02/10/2011 La logger n'est pas crée si un autre logger a été défini.
 *                  Lecture des paramètres depuis le fichier config.ini.
 *                  Création de la fonction getKeyCode.
 * \date 04/10/2011 Ajout de la méthode getSettingValue.
 * \date 05/10/2011 Le changement de map est fonctionnel.
 * \date 04/12/2011 Utilisation de la SFML.
 * \date 10/12/2011 Gestion des curseurs de la souris.
 * \date 14/10/2012 On peut afficher ou masquer les données de Bullet.
 * \date 16/03/2013 Séparation de la boucle principale dans deux méthodes virtuelles.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CApplication.hpp"
#include "Core/CDateTime.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/CFile.hpp"
#include "Core/CLoggerFile.hpp"
#include "Core/Utils.hpp"
#include "Core/Time.hpp"

#include "os.h"

// DEBUG
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

CApplication * CApplication::m_instance = NULL;


/**
 * Constructeur par défaut.
 ******************************/

CApplication::CApplication(const CString& title) :
IEventReceiver    (),
m_time            (0),
m_inGame          (false),
m_gameActive      (false),
m_active          (true),
m_fps             (50.0f),
m_window          (nullptr),
m_cursorPos       (Origin2US),
m_gameTime        (0),
m_cursorVisible   (true),
m_frameTimeList   (100),
m_fpsList         (100),

#ifndef T_NO_DEFAULT_LOGGER
m_defaultLogger   (nullptr),
#endif // T_NO_DEFAULT_LOGGER

m_receiver        (nullptr),
m_title           (title/*"TEngine Application"*/),
m_videoMode       (800, 600, 32),
m_fullscreen      (false)
{
    if (m_instance)
        exit(-1);

    m_instance = this;

    // Création du logger par défaut
#ifndef T_NO_DEFAULT_LOGGER
    m_defaultLogger = new CLoggerFile(CDateTime::getCurrentDateTime().toString("logs/log-%Y-%m-%d-%H-%i-%s.txt"));
    T_ASSERT(m_defaultLogger != nullptr);
    addLogger(m_defaultLogger);
#endif // T_NO_DEFAULT_LOGGER

    log("Chargement du moteur");

    // Lecture des paramètres
    m_settings.loadFromFile("../../config.ini");

    // Initialisation de l'état des boutons de la souris
    m_buttons[MouseButtonLeft]   = false;
    m_buttons[MouseButtonRight]  = false;
    m_buttons[MouseButtonMiddle] = false;
    m_buttons[MouseWheelUp]      = false;
    m_buttons[MouseWheelDown]    = false;

    // Touches par défaut
    m_keyActions[ActionForward]     = getKeyCode(m_settings.getValue("Keys", "forward"    , "Key_Z"));
    m_keyActions[ActionBackward]    = getKeyCode(m_settings.getValue("Keys", "backward"   , "Key_S"));
    m_keyActions[ActionStrafeLeft]  = getKeyCode(m_settings.getValue("Keys", "strafeleft" , "Key_Q"));
    m_keyActions[ActionStrafeRight] = getKeyCode(m_settings.getValue("Keys", "straferight", "Key_D"));
    m_keyActions[ActionTurnLeft]    = getKeyCode(m_settings.getValue("Keys", "turnleft"   , "Key_Left"));
    m_keyActions[ActionTurnRight]   = getKeyCode(m_settings.getValue("Keys", "turnright"  , "Key_Right"));
    m_keyActions[ActionJump]        = getKeyCode(m_settings.getValue("Keys", "jump"       , "Key_Space"));
    m_keyActions[ActionCrunch]      = getKeyCode(m_settings.getValue("Keys", "crunch"     , "Key_LShift"));
    m_keyActions[ActionZoom]        = getKeyCode(m_settings.getValue("Keys", "zoom"       , "Key_G"));

    m_window = new sf::Window();
    T_ASSERT(m_window != nullptr);
    m_window->setMouseCursorVisible(false);
}


/**
 * Indique si le curseur est affiché.
 *
 * \return Booléen.
 ******************************/

bool CApplication::isCursorVisible() const
{
    return m_cursorVisible;
}


/**
 * Active ou désactive la répétition automatique des touches.
 *
 * \param enabled Booléen.
 ******************************/

void CApplication::enableKeyRepeat(bool enabled)
{
    T_ASSERT(m_window != nullptr);
    m_window->setKeyRepeatEnabled(enabled);
}


/**
 * Donne le ratio de la fenêtre : width / height.
 *
 * \return Ratio de la fenêtre.
 *
 * \sa CApplication::getWidth
 * \sa CApplication::getHeight
 ******************************/

float CApplication::getRatio() const
{
    return (static_cast<float>(getWidth()) / static_cast<float>(getHeight()));
}


/**
 * Modifie le titre de l'application et de la fenêtre.
 *
 * \param title Titre de la fenêtre.
 *
 * \sa CApplication::getWindowTitle
 ******************************/

void CApplication::setWindowTitle(const CString& title)
{
    m_title = title;

    T_ASSERT(m_window != nullptr);
    m_window->setTitle(m_title.toCharArray());
}


/**
 * Indique si la fenêtre est en plein écran.
 *
 * \return Booléen.
 ******************************/

bool CApplication::isFullScreen() const
{
    return m_fullscreen;
}


/**
 * Donne la position du curseur de la souris
 *
 * \return Position du curseur.
 ******************************/

TVector2I CApplication::getCursorPosition() const
{
    T_ASSERT(m_window != nullptr);

    sf::Vector2i pos = sf::Mouse::getPosition(*m_window);
    return TVector2I(pos.x, pos.y);
}


/**
 * Ajoute un logger à l'application.
 *
 * \param logger Pointeur sur le logger à ajouter.
 */

void CApplication::addLogger(ILogger * logger)
{
    if (!logger)
        return;

    if (std::find(m_loggers.begin(), m_loggers.end(), logger) == m_loggers.end())
        m_loggers.push_back(logger);
}


/**
 * Enlève un logger à l'application.
 * Le logger n'est pas détruit.
 *
 * \param logger Pointeur sur le logger à enlever.
 */

void CApplication::removeLogger(ILogger * logger)
{
    if (!logger)
        return;

    std::list<ILogger *>::iterator it = std::find(m_loggers.begin(), m_loggers.end(), logger);

    if (it != m_loggers.end())
        m_loggers.erase(it);
}


/**
 * Indique si une partie est lancée ou pas.
 *
 * \return Booléen valant true si une partie est lancée.
 ******************************/

bool CApplication::inGame() const
{
    return m_inGame;
}


bool CApplication::isGameActive() const
{
    return m_gameActive;
}


void CApplication::setGameActive(bool active)
{
    if (m_inGame)
        m_gameActive = active;
}


/**
 * Indique si la résolution est valide.
 *
 * \param width  Largeur en pixels.
 * \param height Hauteur en pixels.
 * \return Booléen.
 *
 * \sa CApplication::getValidResolutions
 ******************************/

bool CApplication::isValidResolution(unsigned int width, unsigned int height)
{
    TVector2UI mode(width, height);
    std::vector<TVector2UI> modes;
    getSupportedResolutions(modes);

    for (std::vector<TVector2UI>::const_iterator it = modes.begin(); it != modes.end(); ++it)
    {
        if (*it == mode)
        {
            return true;
        }
    }

    return false;
}


/**
 * Affiche l'application en plein écran ou dans une fenêtre.
 *
 * \param fullScreen Indique si la fenêtre doit être en plein écran.
 ******************************/

void CApplication::switchFullScreen(bool fullScreen)
{
    T_ASSERT(m_window != nullptr);

    m_fullscreen = fullScreen;

    // Création de la nouvelle fenêtre
    if (m_fullscreen)
    {
        m_window->create(m_videoMode, m_title.toCharArray(), sf::Style::Fullscreen);
    }
    else
    {
        m_window->create(m_videoMode, m_title.toCharArray());
    }

    m_window->setMouseCursorVisible(false);
}


/**
 * Affiche la fenêtre et crée le contexte OpenGL avec les paramètres du
 * fichier de configuration.
 ******************************/

void CApplication::displayWindow()
{
    sf::VideoMode mode;
    mode.width = m_settings.getValue("Window", "width", "800").toUnsignedInt32();
    mode.height = m_settings.getValue("Window", "height", "600").toUnsignedInt32();
    mode.bitsPerPixel = 32;

    CString title = m_settings.getValue("TEngine", "title", "TEngine Application");
    bool fullScreen = m_settings.getBooleanValue("Window", "fullscreen" , false);

    displayWindow(mode, title, fullScreen);
}


/**
 * Destructeur.
 ******************************/

CApplication::~CApplication()
{
    m_instance = nullptr;

    log("Fermeture du moteur");

    // Fermeture de la fenêtre
    closeWindow();

#ifndef T_NO_DEFAULT_LOGGER
    delete m_defaultLogger;
#endif // T_NO_DEFAULT_LOGGER
}


/**
 * Donne la durée passée dans le jeu.
 *
 * \return Durée de jeu en millisecondes.
 ******************************/

unsigned int CApplication::getGameTime() const
{
    return m_gameTime;
}


/**
 * Donne le titre de la fenêtre.
 *
 * \return Titre de la fenêtre.
 *
 * \sa CApplication::setWindowTitle
 ******************************/

CString CApplication::getWindowTitle() const
{
    return m_title;
}


/**
 * Donne le nombre de FPS.
 *
 * \return Nombre de FPS.
 ******************************/

float CApplication::getFPS() const
{
    return m_fps;
}


/**
 * Donne l'état d'une touche du clavier.
 *
 * \param key Identifiant de la touche.
 * \return Booléen.
 ******************************/

bool CApplication::getKeyState(TKey key) const
{
    std::map<TKey, bool>::const_iterator it = m_keyStates.find(key);
    return (it != m_keyStates.end() && it->second);
}


/**
 * Donne l'état d'un bouton de la souris.
 *
 * \param button Code du bouton de la souris.
 * \return Booléen.
 ******************************/

bool CApplication::getButtonState(TMouseButton button) const
{
    std::map<TMouseButton, bool>::const_iterator it = m_buttons.find(button);
    return (it != m_buttons.end() && it->second);
}


/**
 * Indique si l'application est active.
 *
 * \return Booléen.
 ******************************/

bool CApplication::isActive() const
{
    return m_active;
}


/**
 * Donne le récepteur d'évènements lié à l'application.
 *
 * \return Pointeur sur le récepteur d'évènement.
 *
 * \sa CApplication::setEventReceiver
 ******************************/

IEventReceiver * CApplication::getEventReceiver() const
{
    return m_receiver;
}


/**
 * Indique si un argument est présent.
 *
 * \param arg Argument à chercher.
 * \return Booléen.
 ******************************/

bool CApplication::isArg(const CString& arg) const
{
    for (std::vector<CString>::const_iterator it = m_args.begin(); it != m_args.end(); ++it)
    {
        if (arg == *it)
        {
            return true;
        }
    }

    return false;
}


/**
 * Donne la position d'un argument dans le tableau des arguments.
 *
 * \return Position de l'argument ou -1 si non trouvé
 ******************************/

unsigned int CApplication::getArgPosition(const CString& arg) const
{
    // On parcourt le tableau des arguments
    for (std::vector<CString>::const_iterator it = m_args.begin(); it != m_args.end(); ++it)
    {
        if (arg == *it)
        {
            return std::distance(m_args.begin(), it);
        }
    }

    return static_cast<unsigned int>(-1);
}


/**
 * Donne la valeur d'un argument selon sa position.
 *
 * \param position Position de l'argument
 * \return Valeur de l'argument
 ******************************/

CString CApplication::getArg(unsigned int position) const
{
    if (m_args.size() > position)
    {
        return m_args[position];
    }

    return CString();
}


/**
 * Donne le nombre d'arguments envoyés à l'application.
 *
 * \return Nombre d'arguments.
 ******************************/

unsigned int CApplication::getNumArgs() const
{
    return m_args.size();
}


/**
 * Donne l'action associée à une touche.
 *
 * \param key Touche recherchée.
 * \return Action associée.
 ******************************/

TAction CApplication::getActionForKey(TKey key) const
{
    for (std::map<TAction, TKey>::const_iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            return it->first;
        }
    }

    return ActionNone;
}


/**
 * Donne la touche associée à une action.
 *
 * \param action Action recherchée.
 * \return Touche associée.
 ******************************/

TKey CApplication::getKeyForAction(TAction action) const
{
    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end())
    {
        return it->second;
    }

    return Key_Unknown;
}


/**
 * Indique si la touche associée à une action est actuellement enfoncée.
 *
 * \param action Action recherchée.
 * \return Booléen.
 ******************************/

bool CApplication::isActionActive(TAction action) const
{
    if (action == ActionNone)
    {
        return false;
    }

    std::map<TAction, TKey>::const_iterator it = m_keyActions.find(action);

    if (it != m_keyActions.end() && it->second != Key_Unknown)
    {
        return getKeyState(it->second);
    }

    return false;
}


/**
 * Modifie la position du curseur de la souris.
 *
 * \param pos Position du curseur
 ******************************/

void CApplication::setCursorPosition(const TVector2US& pos)
{
    T_ASSERT(m_window != nullptr);
    sf::Mouse::setPosition(sf::Vector2i(pos.X, pos.Y), *m_window);
}


/**
 * Affiche la fenêtre et crée le contexte OpenGL.
 *
 * \param mode       Mode vidéo.
 * \param title      Titre de la fenêtre.
 * \param fullScreen Indique si la fenêtre doit être en plein écran.
 ******************************/

void CApplication::displayWindow(const sf::VideoMode& mode, const CString& title, bool fullScreen)
{
    T_ASSERT(m_window != nullptr);

    m_title = title;
    m_fullscreen = fullScreen;
    m_videoMode = mode;

    // Création de la nouvelle fenêtre
    if (m_fullscreen)
    {
        m_window->create(m_videoMode, m_title.toCharArray(), sf::Style::Fullscreen);
    }
    else
    {
        m_window->create(m_videoMode, m_title.toCharArray());
    }

    m_window->setMouseCursorVisible(false);
}


/**
 * Modifie les dimensions de la fenêtre.
 *
 * \param width  Largeur de la fenêtre en pixels.
 * \param height Hauteur de la fenêtre en pixels.
 ******************************/

void CApplication::setSize(unsigned int width, unsigned int height)
{
    T_ASSERT(m_window != nullptr);

    m_videoMode.width = width;
    m_videoMode.height = height;

    if (m_fullscreen)
    {
        // Création du nouveau contexte
        switchFullScreen(true);
    }
    else
    {
        m_window->setSize(sf::Vector2u(width, height));
    }
}


/**
 * Modifie la position de la fenêtre à l'écran.
 *
 * \todo Utiliser un TVector2S
 *
 * \param left Coordonnée horizontale
 * \param top  Coordonnée verticale.
 ******************************/

void CApplication::setPosition(int left, int top)
{
    T_ASSERT(m_window != nullptr);
    m_window->setPosition(sf::Vector2i(left, top));
}


/**
 * Affiche ou masque le curseur de la souris.
 *
 * \param show Booléen.
 ******************************/

void CApplication::showMouseCursor(bool show)
{
    T_ASSERT(m_window != nullptr);

    m_cursorVisible = show;
    m_cursorPos.X = getWidth() / 2;
    m_cursorPos.Y = getHeight() / 2;

    sf::Mouse::setPosition(sf::Vector2i(m_cursorPos.X, m_cursorPos.Y), *m_window);
}


/**
 * Quitte l'application.
 * Cette méthode modifie uniquement la valeur d'une variable mais ne quitte pas
 * l'application directement. L'application sera fermée proprement à la fin de
 * la frame actuelle.
 *
 * \sa CApplication::isActive
 ******************************/

void CApplication::setInactive()
{
    m_active = false;
}


/**
 * Modifie le récepteur d'évènements.
 *
 * \param receiver Pointeur sur le nouveau récepteur d'évènements.
 *
 * \sa CApplication::getEventReceiver
 ******************************/

void CApplication::setEventReceiver(IEventReceiver * receiver)
{
    if (receiver != this)
        m_receiver = receiver;
}


/**
 * Log un message.
 *
 * \param message Message à logger.
 * \param level   Niveau du message.
 */

void CApplication::log(const CString& message, int level)
{
    for (std::list<ILogger *>::const_iterator it = m_loggers.begin(); it != m_loggers.end(); ++it)
    {
        (*it)->write(message, level);
    }
}


/**
 * Change la touche associée à une action.
 *
 * \param action Action à modifier.
 * \param key    Touche à utiliser.
 ******************************/

void CApplication::setKeyForAction(TAction action, TKey key)
{
    // On cherche si une autre action utilise cette touche
    for (std::map<TAction, TKey>::iterator it = m_keyActions.begin(); it != m_keyActions.end(); ++it)
    {
        if (it->second == key)
        {
            it->second = Key_Unknown;
        }
    }

    m_keyActions[action] = key;
}


/**
 * Boucle principale de l'application.
 ******************************/

void CApplication::mainLoop()
{
/*
    // Définition de la langue de l'application
    if (isArg("--lang"))
    {
        CString lang = getArg(getArgPosition("--lang") + 1);
        setLanguage(TLang::French);
    }
*/

    // Boucle de l'application
    while (eventLoop());
}


/**
 * Méthode appellée à chaque frame dans la boucle principale de l'application.
 * Gère les évènements et met à jour les données du jeu et l'affichage.
 * Cette méthode peut être redéfinie dans une classe dérivée pour changer le comportement
 * de l'application.
 *
 * \return Booléen indiquant si l'application est active.
 */

bool CApplication::eventLoop()
{
    // Déclaration des variables
    static unsigned int time_fps = 0;
    static unsigned int nbr_img = 0;
    sf::Event event;

    // Gestion du temps
    unsigned int time = getElapsedTime();
    unsigned int frameTime = time - m_time;
    m_time = time;

    // Boucle évènementielle
    while (m_window->pollEvent(event))
    {
        switch (event.type)
        {
            default:
                break;

            // Bouton Quitter (croix ou Alt-F4)
            case sf::Event::Closed:

                if (m_gameActive)
                {
                    showMouseCursor();
                    m_gameActive = false;
                }

                break;

            // Changement de taille de la fenêtre
            case sf::Event::Resized:
            {
                CResizeEvent ev;
                ev.width = event.size.width;
                ev.height = event.size.height;
                onEvent(ev);
                break;
            }

            // Texte provenant du clavier
            case sf::Event::TextEntered:
            {
                CTextEvent ev;
                ev.type = EventText;
                ev.unicode = event.text.unicode;
                onEvent(ev);
                break;
            }

            // Enfoncement d'une touche du clavier
            case sf::Event::KeyPressed:
            {
                CKeyboardEvent ev;

                ev.type  = KeyPressed;
                ev.code  = getKeyCode(event.key.code);
                ev.modif = NoModifier;

                if (event.key.alt    ) ev.modif |= AltModifier;
                if (event.key.control) ev.modif |= ControlModifier;
                if (event.key.shift  ) ev.modif |= ShiftModifier;

                onEvent(ev);
                break;
            }

            // Relachement d'une touche du clavier
            case sf::Event::KeyReleased:
            {
                CKeyboardEvent ev;

                ev.type  = KeyReleased;
                ev.code  = getKeyCode(event.key.code);
                ev.modif = NoModifier;

                if (event.key.alt    ) ev.modif |= AltModifier;
                if (event.key.control) ev.modif |= ControlModifier;
                if (event.key.shift  ) ev.modif |= ShiftModifier;

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonPressed:
            {
                CMouseEvent ev;

                ev.type = ButtonPressed;
                ev.x    = event.mouseButton.x;
                ev.y    = event.mouseButton.y;
                ev.xrel = static_cast<int>(event.mouseMove.x) - static_cast<int>(m_cursorPos.X);
                ev.yrel = static_cast<int>(event.mouseMove.y) - static_cast<int>(m_cursorPos.Y);

                switch (event.mouseButton.button)
                {
                    default:
                        ev.button = MouseNoButton;
                        break;

                    case sf::Mouse::Left:
                        ev.button = MouseButtonLeft;
                        break;

                    case sf::Mouse::Right:
                        ev.button = MouseButtonRight;
                        break;

                    case sf::Mouse::Middle:
                        ev.button = MouseButtonMiddle;
                        break;

                    case sf::Mouse::XButton1:
                        ev.button = MouseButtonX1;
                        break;

                    case sf::Mouse::XButton2:
                        ev.button = MouseButtonX2;
                        break;
                }

                m_cursorPos.set(ev.x, ev.y);

                // On place le curseur au centre de l'écran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), *m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseButtonReleased:
            {
                CMouseEvent ev;

                ev.type = ButtonReleased;
                ev.x    = event.mouseButton.x;
                ev.y    = event.mouseButton.y;
                ev.xrel = static_cast<int>(event.mouseMove.x) - static_cast<int>(m_cursorPos.X);
                ev.yrel = static_cast<int>(event.mouseMove.y) - static_cast<int>(m_cursorPos.Y);

                switch (event.mouseButton.button)
                {
                    default:
                        ev.button = MouseNoButton;
                        break;

                    case sf::Mouse::Left:
                        ev.button = MouseButtonLeft;
                        break;

                    case sf::Mouse::Right:
                        ev.button = MouseButtonRight;
                        break;

                    case sf::Mouse::Middle:
                        ev.button = MouseButtonMiddle;
                        break;

                    case sf::Mouse::XButton1:
                        ev.button = MouseButtonX1;
                        break;

                    case sf::Mouse::XButton2:
                        ev.button = MouseButtonX2;
                        break;
                }

                m_cursorPos.set(ev.x, ev.y);

                // On place le curseur au centre de l'écran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), *m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseWheelMoved:
            {
                CMouseEvent ev;

                ev.type   = MouseWheel;
                ev.x      = event.mouseWheel.x;
                ev.y      = event.mouseWheel.y;
                ev.xrel   = static_cast<int>(event.mouseMove.x) - static_cast<int>(m_cursorPos.X);
                ev.yrel   = static_cast<int>(event.mouseMove.y) - static_cast<int>(m_cursorPos.Y);
                ev.button = (event.mouseWheel.delta > 0 ? MouseWheelUp : MouseWheelDown);

                m_cursorPos.set(ev.x, ev.y);

                // On place le curseur au centre de l'écran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2 ), *m_window);
                }

                onEvent(ev);
                break;
            }

            case sf::Event::MouseMoved:
            {
                CMouseEvent ev;

                ev.type   = MoveMouse;
                ev.x      = event.mouseMove.x;
                ev.y      = event.mouseMove.y;
                ev.xrel   = static_cast<int>(event.mouseMove.x) - static_cast<int>(m_cursorPos.X);
                ev.yrel   = static_cast<int>(event.mouseMove.y) - static_cast<int>(m_cursorPos.Y);
                ev.button = MouseNoButton;

                if (m_cursorVisible)
                {
                    m_cursorPos.set(ev.x, ev.y);
                }

                // On place le curseur au centre de l'écran
                if (!m_cursorVisible && (ev.xrel != 0 || ev.yrel != 0))
                {
                    sf::Mouse::setPosition(sf::Vector2i(getWidth() / 2, getHeight() / 2), *m_window);
                }

                onEvent(ev);
                break;
            }

        }
    }

    if (m_window)
    {
        m_window->display();
    }

    // Calcul des FPS
    ++nbr_img;

    // À chaque seconde écoulée, on met à jour le compteur de FPS
    if (m_time - time_fps > 1000)
    {
        m_fps = (1000.0f * nbr_img / static_cast<float>(m_time - time_fps));
        nbr_img = 1;
        time_fps = m_time;

#ifdef T_ACTIVE_DEBUG_MODE
        // Enregistrement des dernières mesures de FPS
        m_fpsList.append(m_fps);
#endif // T_ACTIVE_DEBUG_MODE
    }

#ifdef T_ACTIVE_DEBUG_MODE
        // Enregistrement de la durée de chaque frame
        m_frameTimeList.append(frameTime);
#endif // T_ACTIVE_DEBUG_MODE

    return m_active;
}


/**
 * Gestion des arguments envoyés à l'application.
 *
 * \param argc Nombre d'arguments
 * \param argv Pointeur sur le tableau des arguments
 ******************************/

void CApplication::sendArgs(int argc, char * argv[])
{
    // Liste des arguments
    m_args.reserve(argc);

    for (int i = 0; i < argc; ++i)
    {
        m_args.push_back(argv[i]);
    }

    // On indique au système de fichier le répertoire courant
    CString path = m_args[0];
    std::ptrdiff_t pos = path.lastIndexOf(CFileSystem::separator);

    if (pos != -1)
    {
        CFileSystem::Instance().addDirectory(path.subString(0, pos + 1));
    }
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CApplication::onEvent(const CMouseEvent& event)
{
    // Modification de l'état du bouton
    if (event.type == ButtonPressed)
    {
        m_buttons[event.button] = true;
    }
    else if (event.type == ButtonReleased)
    {
        m_buttons[event.button] = false;
    }

    // On envoie l'évènement au récepteur d'évènements
    if (m_receiver && m_gameActive)
    {
        m_receiver->onEvent(event);
    }
}


/**
 * Gestion du texte provenant du clavier.
 *
 * \param event Évènement.
 ******************************/

void CApplication::onEvent(const CTextEvent& event)
{
    // On envoie l'évènement au récepteur d'évènements
    if (m_receiver && m_gameActive)
    {
        m_receiver->onEvent(event);
    }
}


/**
 * Gestion des évènements du clavier.
 *
 * \param event Évènement.
 ******************************/

void CApplication::onEvent(const CKeyboardEvent& event)
{
    // Modification de l'état de la touche
    m_keyStates[event.code] = (event.type == KeyPressed);

    // On envoie l'évènement au récepteur d'évènements
    if (m_receiver && m_gameActive)
    {
        m_receiver->onEvent(event);
    }
}


/**
 * Donne la valeur d'un paramètre défini dans le fichier de configuration.
 *
 * \param group  Nom du groupe.
 * \param key    Nom de la clé.
 * \param defval Valeur par défaut.
 * \return Valeur du paramètre.
 ******************************/

CString CApplication::getSettingValue(const CString& group, const CString& key, const CString& defval) const
{
    return m_settings.getValue(group, key, defval);
}


/**
 * Modifie la valeur d'un paramètre dans le fichier de configuration.
 *
 * \param group Nom du groupe.
 * \param key   Nom de la clé.
 * \param value Valeur de la clé.
 ******************************/

void CApplication::setSettingValue(const CString& group, const CString& key, const CString& value)
{
    m_settings.setValue(group, key, value);
    m_settings.saveToFile("../../config.ini");
}


/**
 * Ferme la fenêtre.
 ******************************/

void CApplication::closeWindow()
{
    // Suppression de l'ancienne fenêtre
    delete m_window;
    m_window = nullptr;
}


/**
 * Indique si la fenêtre est ouverte ou pas.
 *
 * \return Booléen.
 ******************************/

bool CApplication::isWindowOpened() const
{
    T_ASSERT(m_window != nullptr);
    return m_window->isOpen();
}


/**
 * Donne la largeur de la fenêtre.
 *
 * \return Largeur de la fenêtre en pixels.
 *
 * \sa CApplication::getHeight
 ******************************/

unsigned int CApplication::getWidth() const
{
    T_ASSERT(m_window != nullptr);
    sf::Vector2u windowSize = m_window->getSize();
    return windowSize.x;
}


/**
 * Donne la hauteur de la fenêtre.
 *
 * \return Hauteur de la fenêtre en pixels.
 *
 * \sa CApplication::getWidth
 ******************************/

unsigned int CApplication::getHeight() const
{
    T_ASSERT(m_window != nullptr);
    sf::Vector2u windowSize = m_window->getSize();
    return windowSize.y;
}


/**
 * Affiche ou masque la fenêtre.
 *
 * \param state Booléen.
 ******************************/

void CApplication::showWindow(bool state)
{
    T_ASSERT(m_window != nullptr);
    m_window->setActive(state);
}


/**
 * Récupère la liste des résolutions valides en plein écran.
 *
 * \param resolutions Tableau qui recevra les résolutions valides.
 *
 * \sa CApplication::isValidResolution
 ******************************/

void CApplication::getSupportedResolutions(std::vector<TVector2UI>& resolutions)
{
    resolutions.clear();

    // Liste des modes vidéos en plein écran
    std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();

    for (std::vector<sf::VideoMode>::const_iterator it = modes.begin(); it != modes.end(); ++it)
    {
        resolutions.push_back(TVector2UI(it->width, it->height));
    }
}


/**
 * Récupère la liste des modes vidéos valides en plein écran.
 *
 * \param modes Tableau qui recevra les modes vidéos valides.
 ******************************/

void CApplication::getSupportedVideoModes(std::vector<sf::VideoMode>& modes)
{
    modes = sf::VideoMode::getFullscreenModes();
}


/**
 * Donne le code correspondant à un nom de touche.
 *
 * \todo Trouver un moyen plus élégant de faire cela.
 *
 * \param keycode Nom de la touche (Key_???).
 * \return Code de la touche (défini dans l'énumération TKey).
 ******************************/

TKey CApplication::getKeyCode(const CString& keycode)
{
    // Ça commence mal...
    if (!keycode.startsWith("Key_"))
    {
        return Key_Unknown;
    }

    CString str = keycode.subString(4);

    // Chiffres
    if (str == "0") return Key_0;
    if (str == "1") return Key_1;
    if (str == "2") return Key_2;
    if (str == "3") return Key_3;
    if (str == "4") return Key_4;
    if (str == "5") return Key_5;
    if (str == "6") return Key_6;
    if (str == "7") return Key_7;
    if (str == "8") return Key_8;
    if (str == "9") return Key_9;

    // Chiffres (pavé numérique)
    if (str == "Num0") return Key_Num0;
    if (str == "Num1") return Key_Num1;
    if (str == "Num2") return Key_Num2;
    if (str == "Num3") return Key_Num3;
    if (str == "Num4") return Key_Num4;
    if (str == "Num5") return Key_Num5;
    if (str == "Num6") return Key_Num6;
    if (str == "Num7") return Key_Num7;
    if (str == "Num8") return Key_Num8;
    if (str == "Num9") return Key_Num9;

    // Lettres
    if (str == "A") return Key_A;
    if (str == "B") return Key_B;
    if (str == "C") return Key_C;
    if (str == "D") return Key_D;
    if (str == "E") return Key_E;
    if (str == "F") return Key_F;
    if (str == "G") return Key_G;
    if (str == "H") return Key_H;
    if (str == "I") return Key_I;
    if (str == "J") return Key_J;
    if (str == "K") return Key_K;
    if (str == "L") return Key_L;
    if (str == "M") return Key_M;
    if (str == "N") return Key_N;
    if (str == "O") return Key_O;
    if (str == "P") return Key_P;
    if (str == "Q") return Key_Q;
    if (str == "R") return Key_R;
    if (str == "S") return Key_S;
    if (str == "T") return Key_T;
    if (str == "U") return Key_U;
    if (str == "V") return Key_V;
    if (str == "W") return Key_W;
    if (str == "X") return Key_X;
    if (str == "Y") return Key_Y;
    if (str == "Z") return Key_Z;

    // Autres caractères ASCII
    if (str == "Space"       ) return Key_Space;
    if (str == "Exclam"      ) return Key_Exclam;
    if (str == "Paragraph"   ) return Key_Paragraph;
    if (str == "BracketRight") return Key_BracketRight;
    if (str == "ParenRight"  ) return Key_ParenRight;
    if (str == "Degree"      ) return Key_Degree;
    if (str == "Comma"       ) return Key_Comma;
    if (str == "Question"    ) return Key_Question;
    if (str == "Colon"       ) return Key_Colon;
    if (str == "Slash"       ) return Key_Slash;
    if (str == "Semicolon"   ) return Key_Semicolon;
    if (str == "Less"        ) return Key_Less;
    if (str == "Greater"     ) return Key_Greater;
    if (str == "Equal"       ) return Key_Equal;
    if (str == "Plus"        ) return Key_Plus;
    if (str == "BraceRight"  ) return Key_BraceRight;
    if (str == "Euro"        ) return Key_Euro;
    if (str == "TwoSuperior" ) return Key_TwoSuperior;
    if (str == "At"          ) return Key_At;
    if (str == "Ampersand"   ) return Key_Ampersand;
    if (str == "Tilde"       ) return Key_Tilde;
    if (str == "QuoteDbl"    ) return Key_QuoteDbl;
    if (str == "Apostrophe"  ) return Key_Apostrophe;
    if (str == "BraceLeft"   ) return Key_BraceLeft;
    if (str == "ParenLeft"   ) return Key_ParenLeft;
    if (str == "BracketLeft" ) return Key_BracketLeft;
    if (str == "Minus"       ) return Key_Minus;
    if (str == "Bar"         ) return Key_Bar;
    if (str == "Quote"       ) return Key_Quote;
    if (str == "Underscore"  ) return Key_Underscore;
    if (str == "Backslash"   ) return Key_Backslash;
    if (str == "Dollar"      ) return Key_Dollar;
    if (str == "Sterling"    ) return Key_Sterling;
    if (str == "Circum"      ) return Key_Circum;
    if (str == "Asterisk"    ) return Key_Asterisk;
    if (str == "Mu"          ) return Key_Mu;
    if (str == "Percent"     ) return Key_Percent;
    if (str == "Add"         ) return Key_Add;
    if (str == "Subtract"    ) return Key_Subtract;
    if (str == "Divide"      ) return Key_Divide;
    if (str == "Multiply"    ) return Key_Multiply;

    // Touches spéciales
    if (str == "LAlt"    ) return Key_LAlt;
    if (str == "RAlt"    ) return Key_RAlt;
    if (str == "LControl") return Key_LControl;
    if (str == "RControl") return Key_RControl;
    if (str == "LShift"  ) return Key_LShift;
    if (str == "RShift"  ) return Key_RShift;
    if (str == "LMeta"   ) return Key_LMeta;
    if (str == "RMeta"   ) return Key_RMeta;
    if (str == "Menu"    ) return Key_Menu;

    if (str == "Print"     ) return Key_Print;
    if (str == "Pause"     ) return Key_Pause;
    if (str == "Tab"       ) return Key_Tab;
    if (str == "Backspace" ) return Key_Backspace;
    if (str == "Enter"     ) return Key_Enter;
    if (str == "NumEnter"  ) return Key_NumEnter;
    if (str == "Insert"    ) return Key_Insert;
    if (str == "Delete"    ) return Key_Delete;
    if (str == "Home"      ) return Key_Home;
    if (str == "End"       ) return Key_End;
    if (str == "PageUp"    ) return Key_PageUp;
    if (str == "PageDown"  ) return Key_PageDown;
    if (str == "Left"      ) return Key_Left;
    if (str == "Up"        ) return Key_Up;
    if (str == "Right"     ) return Key_Right;
    if (str == "Down"      ) return Key_Down;
    if (str == "CapsLock"  ) return Key_CapsLock;
    if (str == "NumLock"   ) return Key_NumLock;
    if (str == "ScrollLock") return Key_ScrollLock;
    if (str == "Function"  ) return Key_Function;
    if (str == "Escape"    ) return Key_Escape;

    // Fonctions
    if (str == "F1" ) return Key_F1;
    if (str == "F2" ) return Key_F2;
    if (str == "F3" ) return Key_F3;
    if (str == "F4" ) return Key_F4;
    if (str == "F5" ) return Key_F5;
    if (str == "F6" ) return Key_F6;
    if (str == "F7" ) return Key_F7;
    if (str == "F8" ) return Key_F8;
    if (str == "F9" ) return Key_F9;
    if (str == "F10") return Key_F10;
    if (str == "F11") return Key_F11;
    if (str == "F12") return Key_F12;
    if (str == "F13") return Key_F13;
    if (str == "F14") return Key_F14;
    if (str == "F15") return Key_F15;

    return Key_Unknown;
}


/**
 * Donne le code correspondant à un code de touche SFML.
 * \todo Implémentation.
 *
 * \param keycode Code de touche SFML.
 * \return Code de la touche (défini dans l'énumération TKey).
 ******************************/

TKey CApplication::getKeyCode(const sf::Keyboard::Key& keycode)
{
    switch (keycode)
    {
        default: return Key_Unknown;

        // Lettres
        case sf::Keyboard::A: return Key_A;
        case sf::Keyboard::B: return Key_B;
        case sf::Keyboard::C: return Key_C;
        case sf::Keyboard::D: return Key_D;
        case sf::Keyboard::E: return Key_E;
        case sf::Keyboard::F: return Key_F;
        case sf::Keyboard::G: return Key_G;
        case sf::Keyboard::H: return Key_H;
        case sf::Keyboard::I: return Key_I;
        case sf::Keyboard::J: return Key_J;
        case sf::Keyboard::K: return Key_K;
        case sf::Keyboard::L: return Key_L;
        case sf::Keyboard::M: return Key_M;
        case sf::Keyboard::N: return Key_N;
        case sf::Keyboard::O: return Key_O;
        case sf::Keyboard::P: return Key_P;
        case sf::Keyboard::Q: return Key_Q;
        case sf::Keyboard::R: return Key_R;
        case sf::Keyboard::S: return Key_S;
        case sf::Keyboard::T: return Key_T;
        case sf::Keyboard::U: return Key_U;
        case sf::Keyboard::V: return Key_V;
        case sf::Keyboard::W: return Key_W;
        case sf::Keyboard::X: return Key_X;
        case sf::Keyboard::Y: return Key_Y;
        case sf::Keyboard::Z: return Key_Z;

        // Chiffres
        case sf::Keyboard::Num0: return Key_0;
        case sf::Keyboard::Num1: return Key_1;
        case sf::Keyboard::Num2: return Key_2;
        case sf::Keyboard::Num3: return Key_3;
        case sf::Keyboard::Num4: return Key_4;
        case sf::Keyboard::Num5: return Key_5;
        case sf::Keyboard::Num6: return Key_6;
        case sf::Keyboard::Num7: return Key_7;
        case sf::Keyboard::Num8: return Key_8;
        case sf::Keyboard::Num9: return Key_9;

        // Touches spéciales
        case sf::Keyboard::Escape:   return Key_Escape;
        case sf::Keyboard::LControl: return Key_LControl;
        case sf::Keyboard::LShift:   return Key_LShift;
        case sf::Keyboard::LAlt:     return Key_LAlt;
        case sf::Keyboard::LSystem:  return Key_LMeta;
        case sf::Keyboard::RControl: return Key_RControl;
        case sf::Keyboard::RShift:   return Key_RShift;
        case sf::Keyboard::RAlt:     return Key_RAlt;
        case sf::Keyboard::RSystem:  return Key_RMeta;
        case sf::Keyboard::Menu:     return Key_Menu;

        case sf::Keyboard::LBracket:  return Key_BracketLeft;
        case sf::Keyboard::RBracket:  return Key_BracketRight;
        case sf::Keyboard::SemiColon: return Key_Semicolon;
        case sf::Keyboard::Comma:     return Key_Comma;
        case sf::Keyboard::Period:    return Key_Period;
        case sf::Keyboard::Quote:     return Key_Quote;
        case sf::Keyboard::Slash:     return Key_Slash;
        case sf::Keyboard::BackSlash: return Key_Backslash;
        case sf::Keyboard::Tilde:     return Key_Tilde;
        case sf::Keyboard::Equal:     return Key_Equal;
        case sf::Keyboard::Dash:      return Key_Minus;
        case sf::Keyboard::Space:     return Key_Space;
        case sf::Keyboard::Return:    return Key_Enter;
        case sf::Keyboard::BackSpace: return Key_Backspace;
        case sf::Keyboard::Tab:       return Key_Tab;
        case sf::Keyboard::PageUp:    return Key_PageUp;
        case sf::Keyboard::PageDown:  return Key_PageDown;
        case sf::Keyboard::End:       return Key_End;
        case sf::Keyboard::Home:      return Key_Home;
        case sf::Keyboard::Insert:    return Key_Insert;
        case sf::Keyboard::Delete:    return Key_Delete;
        case sf::Keyboard::Add:       return Key_Add;
        case sf::Keyboard::Subtract:  return Key_Subtract;
        case sf::Keyboard::Multiply:  return Key_Multiply;
        case sf::Keyboard::Divide:    return Key_Divide;
        case sf::Keyboard::Left:      return Key_Left;
        case sf::Keyboard::Right:     return Key_Right;
        case sf::Keyboard::Up:        return Key_Up;
        case sf::Keyboard::Down:      return Key_Down;
        case sf::Keyboard::Pause:     return Key_Pause;

        // Chiffres (pavé numérique)
        case sf::Keyboard::Numpad0: return Key_Num0;
        case sf::Keyboard::Numpad1: return Key_Num1;
        case sf::Keyboard::Numpad2: return Key_Num2;
        case sf::Keyboard::Numpad3: return Key_Num3;
        case sf::Keyboard::Numpad4: return Key_Num4;
        case sf::Keyboard::Numpad5: return Key_Num5;
        case sf::Keyboard::Numpad6: return Key_Num6;
        case sf::Keyboard::Numpad7: return Key_Num7;
        case sf::Keyboard::Numpad8: return Key_Num8;
        case sf::Keyboard::Numpad9: return Key_Num9;

        // Fonctions
        case sf::Keyboard::F1:  return Key_F1;
        case sf::Keyboard::F2:  return Key_F2;
        case sf::Keyboard::F3:  return Key_F3;
        case sf::Keyboard::F4:  return Key_F4;
        case sf::Keyboard::F5:  return Key_F5;
        case sf::Keyboard::F6:  return Key_F6;
        case sf::Keyboard::F7:  return Key_F7;
        case sf::Keyboard::F8:  return Key_F8;
        case sf::Keyboard::F9:  return Key_F9;
        case sf::Keyboard::F10: return Key_F10;
        case sf::Keyboard::F11: return Key_F11;
        case sf::Keyboard::F12: return Key_F12;
        case sf::Keyboard::F13: return Key_F13;
        case sf::Keyboard::F14: return Key_F14;
        case sf::Keyboard::F15: return Key_F15;
    }

    return Key_Unknown;
}

} // Namespace Ted

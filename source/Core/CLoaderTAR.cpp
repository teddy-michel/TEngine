/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CLoaderTAR.cpp
 * \date 31/01/2010 Création de la classe CLoaderTAR.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 * \date 02/03/2011 Création des méthodes isCorrectFormat et CreateInstance.
 * \date 09/04/2012 La méthode getFileContent retourne un booléen.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Core/CLoaderTAR.hpp"
#include "Core/ILogger.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderTAR::CLoaderTAR() :
ILoaderArchive ()
{ }


/**
 * Donne le contenu d'un fichier.
 *
 * \param fileName Nom du fichier.
 * \param data     Tableau contenant les données du fichier.
 * \param size     Taille du tableau (0 si le fichier n'existe pas).
 * \return Booléen indiquant si le fichier a pu être chargé.
 ******************************/

bool CLoaderTAR::getFileContent(const CString& fileName, std::vector<char>& data, uint64_t * size)
{
    CString name = CFileSystem::convertName(fileName);

    *size = 0;
    unsigned int filenum = 0;

    // On met la chaine en minuscules
#if defined (T_SYSTEM_WINDOWS)
    name = name.toLowerCase();
#endif

    // On cherche le fichier dans la liste des fichiers
    for (std::vector<CString>::iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        // On a trouvé le fichier
#if defined (T_SYSTEM_WINDOWS)
        if (it->toLowerCase() == name)
#else
        if (*it == name)
#endif
        {
            filenum = std::distance(m_files.begin(), it) + 1;
        }
    }

    // Le fichier n'existe pas
    if (filenum == 0)
    {
        return false;
    }

    if (m_lengths.size() > filenum - 1)
    {
        *size = m_lengths[filenum - 1];
        data.reserve(*size);

        // On se rend à l'offset du fichier
        m_file.seekg(m_offsets[filenum - 1]);

        // On lit caractère par caractère
        for (std::size_t i = 0; i < *size; ++i)
        {
            char data_n;
            m_file.read(reinterpret_cast<char *>(&data_n), 1);
            data.push_back(data_n);
        }
    }

    return true;
}


/**
 * Donne la taille d'un fichier de l'archive.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets.
 ******************************/

uint64_t CLoaderTAR::getFileSize(const CString& fileName)
{
    CString name = CFileSystem::convertName(fileName);

    // On met la chaine en minuscules
#if defined (T_SYSTEM_WINDOWS)
    name = name.toLowerCase();
#endif

    unsigned int filenum = 0;

    // On cherche le fichier dans la liste des fichiers
    for (std::vector<CString>::iterator it = m_files.begin(); it != m_files.end(); ++it)
    {
        // On a trouvé le fichier
#if defined (T_SYSTEM_WINDOWS)
        if (it->toLowerCase() == name)
#else
        if (*it == name)
#endif
        {
            filenum = std::distance(m_files.begin(), it) + 1;
        }
    }

    // Le fichier n'existe pas
    if (filenum == 0)
    {
        return 0;
    }

    if (m_lengths.size() > filenum)
    {
        return m_lengths[filenum - 1];
    }

    return 0;
}


/**
 * Chargement de la liste des fichiers.
 *
 * \todo Vérifier le checksum.
 *
 * \param fileName Adresse du fichier de l'archive.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderTAR::loadFromFile(const CString& fileName)
{
    m_file.open(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!m_file)
    {
        CApplication::getApp()->log(CString::fromUTF8("CLoaderTAR::loadFromFile : Impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // On efface toutes les données
    m_files.clear();
    m_offsets.clear();
    m_lengths.clear();

    // On calcule la taille du fichier
    m_file.seekg(0, std::ios_base::end);
    unsigned int size = m_file.tellg();

    unsigned int offset = 0;

    // Lecture du fichier
    while (offset + sizeof(TARFileHeader) < size)
    {
        TARFileHeader file_header;

        m_file.seekg(offset, std::ios_base::beg);
        m_file.read(reinterpret_cast<char *>(&file_header), sizeof(TARFileHeader));

        // Calcul du checksum
        unsigned int checksum_ok = CString(file_header.checksum).toUnsignedInt32();
        unsigned int checksum = 256 + file_header.link;

        for (unsigned int i = 0; i < 100; ++i)
        {
            checksum += file_header.fileName[i];
            checksum += file_header.linkname[i];

            if (i < 12)
            {
                checksum += file_header.filesize[i];
                checksum += file_header.time[i];

                if (i < 8)
                {
                    checksum += file_header.filemode[i];
                    checksum += file_header.owner[i];
                    checksum += file_header.group[i];
                }
            }
        }

        if (checksum != checksum_ok)
        {
            CApplication::getApp()->log(CString::fromUTF8("CLoaderTAR::loadFromFile : checksum du fichier de l'archive TAR incorrect (%1 au lieu de %2)").arg(checksum).arg(checksum_ok), ILogger::Warning);
        }

        offset += 512;

        // Dossier
        if (file_header.link == 5)
        {
            continue;
        }

        // Nom du fichier
        m_files.push_back(CString(file_header.fileName));

        // Taille du fichier
        unsigned int filesize = CString(file_header.filesize).toUnsignedInt32();
        m_lengths.push_back(filesize);

        // Offset du fichier
        m_offsets.push_back(offset);

        if (filesize > 0)
        {
            offset += filesize + 512 - (filesize % 512);
        }
    }

    return true;
}


/**
 * Indique si le fichier contient une archive pouvant être chargée.
 *
 * \todo Améliorer la méthode de détection.
 *
 * \param fileName Adresse du fichier contenant le modèle.
 * \return Booléen.
 ******************************/

bool CLoaderTAR::isCorrectFormat(const char * fileName)
{
    // Ouverture du fichier
    std::ifstream file(fileName, std::ios::in | std::ios::binary);

    // Le fichier ne peut pas être ouvert
    if (!file)
    {
        return false;
    }

    // On calcule la taille du fichier
    file.seekg(0, std::ios_base::end);
    unsigned int size = file.tellg();

    file.close();

    return (size % 512 == 0);
}


/**
 * Crée une nouvelle instance du chargeur si le fichier peut être chargé.
 *
 * \param fileName Adresse du fichier contenant l'archive.
 * \return Pointeur sur une nouvelle instance du chargeur de modèles, ou un pointeur invalide si
 *         le fichier ne peut pas être chargé.
 ******************************/

#ifdef T_NO_COVARIANT_RETURN
ILoaderArchive * CLoaderTAR::createInstance(const char * fileName)
#else
CLoaderTAR * CLoaderTAR::createInstance(const char * fileName)
#endif
{
    return (CLoaderTAR::isCorrectFormat(fileName) ? new CLoaderTAR() : nullptr);
}

} // Namespace Ted

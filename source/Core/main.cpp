/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/main.cpp
 * \date 23/07/2010 Création de la fonction main.
 */

#include "os.h"

#if defined(T_SYSTEM_WINDOWS)

#undef __STRICT_ANSI__ // Pour définir __argc et __argv
#include <windows.h>

/*
#ifndef __argc
#   error "Macro __argc not defined"
#endif

#ifndef __argv
#   error "Macro __argv not defined"
#endif
*/

// On définie la fonction main classique
extern int main(int argc, char * argv[]);

// Point d'entrée du programme sous Windows.
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    int status = main(__argc, __argv);
    exit(status); // Appel des fonctions définies dans atexit

    return EXIT_SUCCESS; // Ce code n'est jamais exécuté
}

#endif // T_SYSTEM_WINDOWS

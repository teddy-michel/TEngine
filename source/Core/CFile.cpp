/*
Copyright (C) 2008-2016 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/CFile.cpp
 * \date 12/01/2010 Création de la classe CFile.
 * \date 27/07/2010 On ne change plus de répertoire pour charger un fichier.
 *                  Les messages d'erreur sont envoyés à la console.
 * \date 14/03/2011 Inclusion de dirent.h sous Linux.
 * \date 09/04/2012 Améliorations.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "os.h"

#ifdef T_SYSTEM_LINUX
#  include <dirent.h>
#endif

#include "Core/CFile.hpp"
#include "Core/CApplication.hpp"
#include "Core/CFileSystem.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"
#include "Core/ILoaderArchive.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 * Le fichier n'est pas ouvert.
 *
 * \param fileName Adresse du fichier.
 ******************************/

CFile::CFile(const CString& fileName) :
m_fileName (CFileSystem::convertName(fileName)),
m_size     (0),
m_type     (FileNone),
m_mode     (0)
{ }


/**
 * Destructeur. Ferme le fichier s'il est ouvert.
 ******************************/

CFile::~CFile()
{
    close();
}


/**
 * Retourne le chemin absolu du fichier.
 *
 * \return Chemin absolu.
 ******************************/

CString CFile::getAbsolutePath() const
{
    return CFileSystem::Instance().getAbsolutePath(m_fileName);
}


/**
 * Retourne le nom du fichier complet.
 *
 * \return Nom du fichier.
 ******************************/

CString CFile::getFullName() const
{
    return m_fileName;
}


/**
 * Retourne le nom du fichier (sans le chemin).
 *
 * \return Nom du fichier.
 ******************************/

CString CFile::getFileName() const
{
    std::ptrdiff_t pos = m_fileName.lastIndexOf(CFileSystem::separator);

    if (pos != -1)
    {
        return m_fileName.subString(pos + 1);
    }
    else
    {
        return m_fileName;
    }
}


/**
 * Retourne le nom du fichier sans l'extension.
 *
 * \return Nom du fichier.
 ******************************/

CString CFile::getName() const
{
    return getFileName().subString(0, getFileName().lastIndexOf('.'));
}


/**
 * Retourne l'extension du fichier.
 *
 * \return Extension du fichier.
 ******************************/

CString CFile::getExtension() const
{
    std::ptrdiff_t pos = m_fileName.lastIndexOf('.');

    if (pos != -1)
    {
        return m_fileName.subString(pos + 1);
    }
    else
    {
        return CString();
    }
}


/**
 * Modifie le nom du fichier à ouvrir.
 * Si le fichier a déjà été ouvert, il est fermé.
 *
 * \param fileName Adresse du fichier à ouvrir.
 ******************************/

void CFile::setFileName(const CString& fileName)
{
    if (isOpen())
    {
        CApplication::getApp()->log(CString("CFile::setFileName : le fichier %1 est déjà ouvert").arg(m_fileName), ILogger::Warning);
        close();
    }

    m_fileName = CFileSystem::convertName(fileName);
}


/**
 * Indique si le fichier est ouvert.
 *
 * \return Booléen.
 ******************************/

bool CFile::isOpen() const
{
    return (m_type != FileNone);
}


/**
 * Ouvre un fichier.
 *
 * \param fileName Adresse du fichier.
 * \param mode     Mode d'ouverture du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CFile::open(const CString& fileName, TOpenMode mode)
{
    m_fileName = CFileSystem::convertName(fileName);
    return open(mode);
}


/**
 * Ouvre un fichier.
 *
 * \param mode Mode d'ouverture du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CFile::open(TOpenMode mode)
{
    if (m_fileName.isEmpty())
    {
        CApplication::getApp()->log(CString::fromUTF8("CFile::Open : tentative d'ouverture d'un fichier sans spécifier son nom"), ILogger::Warning);
        return false;
    }

    if (isOpen())
    {
        CApplication::getApp()->log(CString::fromUTF8("CFile::Open : Le fichier %1 est déjà ouvert").arg(m_fileName), ILogger::Warning);
        return false;
    }

    if (mode.testFlag(Append))
    {
        mode |= WriteOnly;
    }

    if ((mode & ReadAndWrite) == 0)
    {
        CApplication::getApp()->log(CString::fromUTF8("CFile::Open : pas de mode d'ouverture spécifié pour le fichier %1").arg(m_fileName), ILogger::Warning);
        return false;
    }

    m_mode = mode;

    std::ios_base::openmode open_mode;
    if ( mode.testFlag(ReadOnly )) open_mode |= std::ios::in;
    if ( mode.testFlag(WriteOnly)) open_mode |= std::ios::out;
    if ( mode.testFlag(Append   )) open_mode |= std::ios::ate;
    if ( mode.testFlag(Truncate )) open_mode |= std::ios::trunc;
    if (!mode.testFlag(Text     )) open_mode |= std::ios::binary;

    CString path = CFileSystem::Instance().getFileDirectory(m_fileName);

    // Le fichier n'existe pas dans les répertoires
    if (path.isEmpty())
    {
        path = CFileSystem::Instance().getFileArchive(m_fileName);

        // Le fichier n'existe pas dans les archives
        if (path.isEmpty())
        {
            CApplication::getApp()->log(CString::fromUTF8("CFile::Open : le fichier %1 est introuvable").arg(m_fileName), ILogger::Warning);
            return false;
        }
        else
        {
            ILoaderArchive * archive = CFileSystem::Instance().getArchive(path);

            if (archive)
            {
                if (open(archive, m_fileName))
                {
                    m_type = FileMemory;
                    return true;
                }

                return false;
            }

            CApplication::getApp()->log(CString::fromUTF8("CFile::Open : le fichier %1 est introuvable.").arg(m_fileName), ILogger::Warning);
            return false;
        }
    }
    // Le fichier existe dans les répertoires
    else
    {
        CString fullname = path + m_fileName;

        //CFileSystem::ChangeDirectory(path); // On change de répertoire
        m_file.open(fullname.toCharArray(), open_mode);
        //CFileSystem::ChangeDirectory(CFileSystem::Instance().getCurrentDirectory()); // On revient au répertoire courant

        if (!m_file.fail())
        {
            CApplication::getApp()->log(CString::fromUTF8("Lecture du fichier %1").arg(fullname));
            m_fileName = fullname;
            m_type = FileDisk;

            // Taille du fichier
            m_file.seekg(0, std::ios_base::end);
            m_size = m_file.tellg();

            if (!m_mode.testFlag(Append))
            {
                m_file.seekg(0, std::ios_base::beg);
            }

            return true;
        }

        CApplication::getApp()->log(CString::fromUTF8("CFile::Open : impossible de lire le fichier %1").arg(fullname), ILogger::Warning);
        return false;
    }
}


/**
 * Ferme le fichier.
 ******************************/

void CFile::close()
{
    if (m_type == FileDisk)
    {
        m_file.flush();
        m_file.close();
    }
    else if (m_type == FileMemory)
    {
        m_buffer.str("");
    }

    m_type = FileNone;
    m_size = 0;
}


/**
 * Retourne la taille du fichier.
 *
 * \return Taille du fichier en octets, ou 0 si le fichier n'est pas ouvert.
 ******************************/

uint64_t CFile::getFileSize() const
{
    return m_size;
}


/**
 * Donne la taille d'un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Taille du fichier en octets.
 ******************************/

uint64_t CFile::getFileSize(const CString& fileName)
{
    return CFile(fileName).getFileSize();
}


/**
 * Déplace le curseur virtuel.
 *
 * \param off Integral value representing the offset to be applied relative to an absolute position specified in the dir parameter.
 * \param dir Seeking direction.
 * \return Référence sur ce fichier (*this).
 *
 * \sa CFile::TellCursor
 ******************************/

CFile& CFile::seekCursor(uint64_t off, TSeekDir dir)
{
    // Fichier réel
    if (m_type == FileDisk)
    {
        switch (dir)
        {
            case Begin:   m_file.seekg(off, std::ios_base::beg); break;
            case End:     m_file.seekg(off, std::ios_base::end); break;
            case Current: m_file.seekg(off, std::ios_base::cur); break;
        }
    }
    // Fichier virtuel
    else if (m_type == FileMemory)
    {
        switch (dir)
        {
            case Begin:   m_buffer.seekg(off, std::ios_base::beg); break;
            case End:     m_buffer.seekg(off, std::ios_base::end); break;
            case Current: m_buffer.seekg(off, std::ios_base::cur); break;
        }
    }

    return *this;
}


/**
 * Donne la position du curseur virtuel.
 *
 * \return Position du curseur.
 *
 * \sa CFile::SeekCursor
 ******************************/

uint64_t CFile::tellCursor()
{
    // Fichier réel
    if (m_type == FileDisk)
    {
        return m_file.tellg();
    }
    // Fichier virtuel
    else if (m_type == FileMemory)
    {
        return m_buffer.tellg();
    }

    return 0;
}


/**
 * Returns the stream buffer object associated with the stream.
 *
 * \return A pointer to the stream buffer object associated with the stream before the call.
 ******************************/

std::streambuf * CFile::readBuffer() const
{
    if (m_type == FileDisk)
    {
        return m_file.rdbuf();
    }
    else if (m_type == FileMemory)
    {
        return m_buffer.rdbuf();
    }

    return nullptr;
}


/**
 * Lit un certain nombre de caractères dans le fichier.
 *
 * \param s Pointeur sur un tableau de caractère contenant les caractères lus.
 * \param n Nombre maximal de caractères à lire.
 * \return Nombre de caractères lus, ou -1 en cas d'erreur.
 ******************************/

int64_t CFile::read(char * s, uint64_t n)
{
    if (m_type == FileNone || !m_mode.testFlag(ReadOnly))
    {
        return -1;
    }

    // Fichier réel
    if (m_type == FileDisk)
    {
        m_file.read(s, n);
        return m_file.gcount();
    }
    // Fichier virtuel
    else if (m_type == FileMemory)
    {
        m_buffer.read(s, n);
        return m_buffer.gcount();
    }

    return -1;
}


/**
 * Écrit un certain nombre de caractères dans le fichier.
 *
 * \param s Pointeur sur un tableau de caractère contenant les caractères à écrire.
 * \param n Nombre de caractères à écrire.
 * \return Nombre de caractères écrits, ou -1 en cas d'erreur.
 ******************************/

int64_t CFile::write(const char * s, uint64_t n)
{
    if (m_type == FileNone || !m_mode.testFlag(WriteOnly))
    {
        return -1;
    }

    // Fichier réel
    if (m_type == FileDisk)
    {
        m_file.write(s, n);
        return (m_file.bad() ? -1 : n);
    }
    // Fichier virtuel
    else if (m_type == FileMemory)
    {
        m_buffer.write(s, n);
        return (m_buffer.bad() ? -1 : n);
    }

    return -1;
}



/**
 * Ouvre un fichier qui se trouve dans une archive.
 *
 * \param archive  Adresse de l'archive.
 * \param fileName Adresse du fichier dans l'archive.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CFile::open(const CString& archive, const CString& fileName)
{
    ILoaderArchive * file_archive = CFileSystem::Instance().getArchive(archive);

    if (file_archive)
    {
        return open(file_archive, fileName);
    }

    return false;
}


/**
 * Ouvre un fichier qui se trouve dans une archive.
 *
 * \param archive  Pointeur sur l'archive.
 * \param fileName Adresse du fichier dans l'archive.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CFile::open(ILoaderArchive * archive, const CString& fileName)
{
    if (archive == nullptr)
    {
        return false;
    }

    if (archive->isFileExists(fileName))
    {
        m_fileName = fileName;
        m_type = FileMemory;
        std::vector<char> content;
        archive->getFileContent(m_fileName, content, &m_size);

        // On ajoute chaque caractère au buffer
        for (std::vector<char>::const_iterator it = content.begin(); it != content.end(); ++it)
        {
            m_buffer << *it;
        }

        return true;
    }

    CApplication::getApp()->log(CString::fromUTF8("CFile::Open : le fichier %1 ne se trouve pas dans l'archive %2").arg(fileName).arg(archive->getFileName()), ILogger::Warning);
    return false;
}

} // Namespace

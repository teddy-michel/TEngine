/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Core/Exceptions.cpp
 * \date       2008 Création des exceptions.
 * \date 03/12/2010 Ajout de l'exception CUncorrectValue.
 * \date 14/11/2011 Création de l'exception CBadArgument qui remplace CUncorrectValue.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sstream>

#include "Core/Exceptions.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
  Constructeur.
 *
 * \param message Message d'erreur.
 ******************************/

IException::IException(const std::string& message) :
m_message (message)
{ }


/**
 * Destructeur.
 ******************************/

IException::~IException() throw()
{ }


/**
 * Renvoie le message associé à l'exception.
 *
 * \return Pointeur sur le message.
 ******************************/

const char * IException::what() const throw()
{
    return m_message.c_str();
}


/**
 * Constructeur.
 *
 * \param file Fichier source contenant l'erreur.
 * \param line Ligne à laquelle se trouve l'erreur dans le fichier source.
 * \param message Message d'erreur.
 ******************************/

CAssertException::CAssertException(const std::string& file, int line, const std::string& message) :
IException (message + "\n" + file + " (" + toString(line) + ")")
{ }


/**
 * Constructeur.
 *
 * \param ptr       Adresse du bloc.
 * \param file      Fichier source.
 * \param line      Ligne dan sle fichier source.
 * \param new_array Indique si le [] se trouve sur le new ou le delete.
 ******************************/

CBadDelete::CBadDelete(const void * ptr, const std::string& file, int line, bool new_array) :
IException ("Anomalie " + toString(new_array ? "new[] / delete[]" : "new / delete") + " détectée\nAdresse : 0x" + toString(ptr) + "\nFichier source : " + file + " (" + toString(line) + ")" )
{ }


/**
 * Constructeur.
 *
 * \param file    Fichier.
 * \param message Message d'erreur.
 ******************************/

CLoadingFailed::CLoadingFailed(const std::string& file, const std::string& message) :
IException ("Erreur dans le chargement de " + file + "\n" + message)
{ }


/**
 * Constructeur.
 *
 * \param message Message d'erreur.
 ******************************/

COutOfMemory::COutOfMemory(const std::string& message) :
IException (message)
{ }


/**
 * Constructeur.
 *
 * \param feature Caractéristique non supportée.
 ******************************/

CUnsupported::CUnsupported(const std::string& feature) :
IException ("Fonctionnalité non supportée\n" + feature)
{ }


/**
 * Constructeur.
 *
 * \param error Message décrivant l'erreur.
 ******************************/

CBadConversion::CBadConversion(const std::string& error) :
IException (error)
{ }


/**
 * Constructeur.
 *
 * \param msg Message décrivant l'erreur.
 ******************************/

CBadArgument::CBadArgument(const std::string& msg) :
IException ("Argument incorrect : " + msg + ")\n")
{ }

} // Namespace Ted

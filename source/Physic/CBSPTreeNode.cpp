/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBSPTreeNode.cpp
 * \date 09/05/2009 Création de la classe CBSPTreeNode.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CBSPTreeNode.hpp"
#include "Physic/Impact.hpp"
#include "Physic/ICollisionVolume.hpp"

#ifdef T_DEBUG
#  include "Core/ILogger.hpp"
#  include "Core/CApplication.hpp"
#endif



namespace Ted
{

const float MinDistance = 0.1f ; ///< Épaisseur des parois.


/**
 * Constructeur par défaut.
 ******************************/

CBSPTreeNode::CBSPTreeNode() :
m_child_front (nullptr),
m_child_back  (nullptr)
{ }


/**
 * Destructeur.
 ******************************/

CBSPTreeNode::~CBSPTreeNode()
{
    delete m_child_front;
    delete m_child_back;
}


/**
 * Donne le plan lié au nœud.
 *
 * \return Plan du nœud.
 ******************************/

CPlane CBSPTreeNode::getPlane() const
{
    return m_plane;
}


/**
 * Donne le pointeur sur le nœud ou la feuille avant.
 *
 * \return Pointeur sur le nœud ou la feuille avant.
 *
 * \sa CBSPTreeNode::setChildFront
 ******************************/

IBSPTreeElement * CBSPTreeNode::getChildFront() const
{
    return m_child_front;
}


/**
 * Accesseur pour child_back.
 *
 * \return Pointeur sur le nœud ou la feuille arrière.
 *
 * \sa CBSPTreeNode::setChildBack
 ******************************/

IBSPTreeElement * CBSPTreeNode::getChildBack() const
{
    return m_child_back;
}


/**
 * Indique si l'élément de l'arbre est une feuille.
 *
 * \return false.
 ******************************/

bool CBSPTreeNode::isLeaf() const
{
    return false;
}


/**
 * Indique si l'élément de l'arbre est un nœud.
 *
 * \return true.
 ******************************/

bool CBSPTreeNode::isNode() const
{
    return true;
}


/**
 * Modifie le plan du nœud.
 *
 * \param plane Plan du nœud.
 ******************************/

void CBSPTreeNode::setPlane(const CPlane& plane)
{
    m_plane = plane;
}


/**
 * Modifie le nœud ou la feuille avant.
 *
 * \param child Pointeur sur le nœud ou la feuille avant.
 *
 * \sa CBSPTreeNode::getChildFront
 ******************************/

void CBSPTreeNode::setChildFront(IBSPTreeElement * child)
{
    m_child_front = child;
}


/**
 * Modifie le nœud ou la feuille arrière.
 *
 * \param child Pointeur sur le nœud ou la feuille arrière.
 *
 * \sa CBSPTreeNode::getChildBack
 ******************************/

void CBSPTreeNode::setChildBack(IBSPTreeElement * child)
{
    m_child_back = child;
}


/**
 * Cherche le type de contenu en fonction d'une position.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType CBSPTreeNode::getContentType(const TVector3F& position) const
{
    if (VectorDot(position, m_plane.getNormale()) > m_plane.getDistance())
    {
        return (m_child_front ? m_child_front->getContentType(position) : Empty);
    }
    else
    {
        return (m_child_back ? m_child_back->getContentType(position) : Empty);
    }
}


/**
 * Indique si un déplacement est correct.
 * Méthode récursive.
 *
 * \todo Ne marche pas du tout.
 *
 * \param from Point de départ.
 * \param to   Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CBSPTreeNode::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    if (getContentType(from) == Solid || getContentType(to) == Solid)
    {
        return false;
    }

    if (VectorDot(from, m_plane.getNormale()) > m_plane.getDistance())
    {
        // Le déplacement se fait dans l'espace avant
        if (VectorDot(to, m_plane.getNormale()) > m_plane.getDistance())
        {
            return m_child_front->isCorrectMovement(from, to);
        }
        // Le déplacement se fait de l'avant du plan vers l'arrière
        else
        {
            // Point d'intersection du segment avec le plan
            TVector3F pt = from + (m_plane.getDistance() + VectorDot(from, m_plane.getNormale())) /
                                        VectorDot(m_plane.getNormale(), from - to) * (to - from);

            // On découpe le segment en deux
            if (!m_child_front->isCorrectMovement(from, pt))
                return false;
            return m_child_back->isCorrectMovement(pt, to);
        }
    }
    else
    {
        // Le déplacement se fait de l'arrière du plan vers l'avant
        if (VectorDot(to, m_plane.getNormale()) > m_plane.getDistance())
        {
            // Point d'intersection du segment avec le plan
            TVector3F pt = from + (m_plane.getDistance() + VectorDot(from, m_plane.getNormale())) /
                                        VectorDot(from - to, m_plane.getNormale()) * (to - from);

            // On découpe le segment en deux
            if (!m_child_back->isCorrectMovement(from, pt))
                return false;
            return m_child_front->isCorrectMovement(pt, to);
        }
        // Le déplacement se fait dans l'espace arrière
        else
        {
            return m_child_back->isCorrectMovement(from, to);
        }
    }
}


/**
 * Recherche une collision avec un volume.
 *
 * \bug On peut traverser certains murs (bug lié au format BSP des moteurs Source et GoldSource).
 * \bug On peut ricocher sur certains murs (bug lié au format BSP des moteurs Source et GoldSource).
 * \bug On peut être bloqué sur un plan entre deux feuilles vides.
 * \bug On peut parfois se retrouver très loin de sa position de départ.
 * \todo Gérer correctement les arbres BSP du moteur Source, ou transformer ces arbres BSP.
 *
 * \param volume Volume.
 * \param impact Impact.
 * \param brush  Impact temporaire.
 ******************************/

void CBSPTreeNode::getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const
{
    // On calcule les distances entre les deux positions et le plan
    float d1 = m_plane.getDistance(impact.getPointFrom());
    float d2 = m_plane.getDistance(impact.getPointTo());

    // On récupère l'offset selon le plan
    float offset = volume.getOffset(m_plane);

    // Forme toujours devant le nœud
    if (d1 > offset && d2 > offset)
    {
        m_child_front->getCollision(volume, impact, brush);
    }
    // Forme toujours derrière le nœud
    else if (-d1 > offset && -d2 > offset)
    {
        m_child_back->getCollision(volume, impact, brush);
    }
    // La forme traverse le nœud (à partir de là ça bug)
    else
    {
        float frac_impact;
        float frac_reelle;

        // On calcule la fraction réelle
//if (d1 < 0) frac_reelle = (d2 - offset) / (d2 - d1); else // Tentative de correction du "bug" made in Source
        frac_reelle = (std::abs(d1 - d2) < std::numeric_limits<float>::epsilon() ? 1.0f : (d1 - offset) / (d1 - d2));

        // Collision si la forme va de l'avant vers l'arrière et si l'impact est plus près que celui déjà stocké
        if (
              //std::abs(d1) >= offset // Ajout personnel (à vérifier) : pour éviter de glisser sur un plan entre deux feuilles vides
              //+ MinDistance && // Deuxième ajout, pour éviter que frac_impact soit négatif
                                // (on peut traverser les murs pour revenir dans une feuille vide à certaines conditions)

             //d1 > 0 && d2 < 0 &&                  // Modification (?)
             //d1 > d2 &&
             //std::abs(d1) > std::abs(d2) && // Modification (?)

             ((
//d1 < 0 && d2 > d1) || (d1 > 0 && // Tentative de correction du "bug" made in Source
             d1 > d2)) &&

             frac_reelle < brush.getFracReelle() )
        {
            // On calcule la fraction impact
//if (d1 < 0) frac_impact = (d2 - MinDistance - offset) / (d2 - d1); else // Tentative de correction du "bug" made in Source
            frac_impact = (std::abs(d1 - d2) < std::numeric_limits<float>::epsilon() ? 1.0f : (d1 - MinDistance - offset) / (d1 - d2));

            // On stocke l'impact si fraction réelle plus près
            brush.setImpact(
//d1 < 0 ? -(m_plane.getNormale()) : // Tentative de correction du "bug" made in Source
                             m_plane.getNormale(), frac_impact, frac_reelle);
        }
        // Sinon, on mémorise les paramètres brush
        else
        {
            frac_impact = brush.getFracImpact();
            frac_reelle = brush.getFracReelle();
        }

        TVector3F normale = brush.getNormale();

        // On parcours l'espace arrière
//if (d1 < 0) m_child_front->getCollision(volume, impact, brush); else // Tentative de correction du "bug" made in Source
        m_child_back->getCollision(volume, impact, brush);

        // On restaure le brush à ce nœud
        brush.setImpact(normale, frac_impact, frac_reelle);

        // On parcours l'espace avant
//if (d1 < 0) m_child_back->getCollision(volume, impact, brush); else // Tentative de correction du "bug" made in Source
        m_child_front->getCollision(volume, impact, brush);
    }
}


/**
 * Indique s'il y a intersection entre l'arbre BSP et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CBSPTreeNode::hasImpact(const CRay& ray) const
{
    T_UNUSED_UNIMPLEMENTED(ray);

    //...

    return false;
}


/**
 * Cherche l'intersection entre l'arbre BSP et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray    Rayon à utiliser.
 * \param impact Impact du rayon avec l'arbre.
 * \return Booléen indiquant s'il y a impact.
 ******************************/

bool CBSPTreeNode::getRayImpact(const CRay& ray, CRayImpact& impact) const
{
    // On découpe le vecteur from/direction pour les deux côtés de chaque plan
    // Si on arrive sur un feuille vide, aucun problème
    // Si la feuille est solide, il y a eu intersection
    // À chaque nouveau plan, on stocke le point d'intersection et la normale
    // si ce point est plus près de l'ancien point stocké

    if (getContentType(ray.origin) == Solid)
    {
        return true;
    }

    // Espace avant
    if (m_plane.getDistance(ray.origin) > 0.0f)
    {
        // Vers l'espace avant
        if (VectorDot(ray.direction, m_plane.getNormale()) > 0.0f)
        {
            return m_child_front->getRayImpact(ray, impact);
        }

        // Point d'intersection du rayon avec le plan
        TVector3F pt = ray.origin - (m_plane.getDistance() + VectorDot(ray.origin, m_plane.getNormale())) / VectorDot(m_plane.getNormale(), ray.direction) * ray.direction;

        // Il y a eu intersection dans l'espace avant
        if (!m_child_front->getRayImpact(ray, impact))
        {
            TVector3F tmp1 = ray.origin - pt;
            TVector3F tmp2 = ray.origin - impact.point;

            // On stocke les informations du nouveau point d'intersection
            if (tmp1.norm() < tmp2.norm())
            {
                impact.point = pt;
                impact.normale = m_plane.getNormale();
            }

            return true;
        }

        CRay tmp(pt, ray.direction);

        // Il y a eu intersection dans l'espace arrière
        if (!m_child_back->getRayImpact(tmp, impact))
        {
            TVector3F tmp1 = ray.origin - pt;
            TVector3F tmp2 = ray.origin - impact.point;

            // On stocke les informations du nouveau point d'intersection
            if (tmp1.norm() < tmp2.norm())
            {
                impact.point = pt;
                impact.normale = m_plane.getNormale();
            }

            return true;
        }

        return false;
    }
    // Espace arrière
    else
    {
        // Vers l'espace avant
        if (VectorDot(ray.direction, m_plane.getNormale()) > 0.0f)
        {
            // Point d'intersection du rayon avec le plan
            TVector3F pt = ray.origin - (m_plane.getDistance() + VectorDot(ray.origin, m_plane.getNormale())) / VectorDot(m_plane.getNormale(), ray.direction) * ray.direction;

            // Il y a eu intersection dans l'espace avant
            if (!m_child_back->getRayImpact(ray, impact))
            {
                TVector3F tmp1 = ray.origin - pt;
                TVector3F tmp2 = ray.origin - impact.point;

                // On stocke les informations du nouveau point d'intersection
                if (tmp1.norm() < tmp2.norm())
                {
                    impact.point = pt;
                    impact.normale = m_plane.getNormale();
                }

                return true;
            }

            CRay tmp(pt, ray.direction);

            // Il y a eu intersection dans l'espace arrière
            if (!m_child_front->getRayImpact(tmp, impact))
            {
                TVector3F tmp1 = ray.origin - pt;
                TVector3F tmp2 = ray.origin - impact.point;

                // On stocke les informations du nouveau point d'intersection
                if (tmp1.norm() < tmp2.norm())
                {
                    impact.point = pt;
                    impact.normale = m_plane.getNormale();
                }

                return true;
            }

            return false;
        }

        // Vers l'espace arrière
        return m_child_back->getRayImpact(ray, impact);
    }
}


/**
 * Vérifie si l'arbre est correct.
 *
 * \return Booléen indiquant si le nœud contient une feuille vide.
 ******************************/

bool CBSPTreeNode::CheckTree()
{
    bool check_front = m_child_front->CheckTree();
    bool check_back = m_child_back->CheckTree();

    // L'espace avant est plein alors que l'espace arrière contient une feuille vide
    if (!check_front && check_back)
    {
        // On inverse le plan du nœud
        m_plane.setDistance(-(m_plane.getDistance()));
        m_plane.setNormale(-(m_plane.getNormale()));

        // On inverse les nœuds enfants
        IBSPTreeElement * tmp = m_child_front;
        m_child_front = m_child_back;
        m_child_back = tmp;
    }

    return (check_back || check_front);
}


#ifdef T_DEBUG

/**
 * Log le contenu de l'arbre BSP.
 *
 * \param offset Nombre d'espace à ajouter au début de la ligne.
 ******************************/

void CBSPTreeNode::Debug_LogTree(unsigned int offset) const
{
    CString str(' ', offset);
    str += " : " + m_plane.getNormale().toString() + " - " + CString::fromNumber(m_plane.getDistance());
    CApplication::getApp()->log(str, ILogger::Debug);

    if (m_child_front)
    {
        m_child_front->Debug_LogTree(offset + 2);
    }

    if (m_child_back)
    {
        m_child_back->Debug_LogTree(offset + 2);
    }
}

#endif

} // Namespace Ted

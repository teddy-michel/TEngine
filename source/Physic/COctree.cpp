/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/COctree.cpp
 * \date       2008 Création de la classe COctree.
 * \date 27/07/2010 Ajout du paramètre child.
 * \date 10/04/2011 Ajout de plusieurs attributs et méthodes.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/COctree.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

COctree::COctree() :
m_center (Origin3F),
m_type   (Empty)
{
    for (int i = 0; i < 8; ++i)
    {
        m_child[i] = nullptr;
    }
}


/**
 * Destructeur.
 ******************************/

COctree::~COctree()
{
    // Suppression des nœuds enfant
    for (int i = 0; i < 8; ++i)
    {
        delete m_child[i];
    }
}


/**
 * Donne les coordonnées du centre du nœud.
 *
 * \return Coordonnées du centre du nœud.
 ******************************/

TVector3F COctree::getCenter() const
{
    return m_center;
}


/**
 * Donne le type de contenu du nœud.
 *
 * \return Type de contenu de nœud.
 *
 * \sa COctree::setType
 ******************************/

TContentType COctree::getType() const
{
    return m_type;
}


/**
 * Indique si le nœud est une feuille.
 *
 * \return Booléen.
 ******************************/

bool COctree::isLeaf() const
{
    return (m_child[0] != nullptr);
}


/**
 * Modifie le type de contenu du nœud.
 *
 * \param type Type de contenu du nœud.
 *
 * \sa COctree::getType
 ******************************/

void COctree::setType(TContentType type)
{
    m_type = type;
}


/**
 * Donne le type de contenu pour un point.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType COctree::getContentType(const TVector3F& position) const
{
    if (m_child[0] == nullptr)
    {
        return m_type;
    }
    else
    {
        int indice = ((position.X < m_center.X) ?
                        ((position.Y < m_center.Y) ?
                            ((position.Z < m_center.Z) ? 6 : 2) :
                            ((position.Z < m_center.Z) ? 4 : 0)) :
                        ((position.Y < m_center.Y) ?
                            ((position.Z < m_center.Z) ? 7 : 3) :
                            ((position.Z < m_center.Z) ? 5 : 1)));

        return m_child[indice]->getContentType(position);
    }
}


/**
 * Divise le nœud.
 *
 * \param center Coordonnées du centre du nœud.
 ******************************/

void COctree::Divide(const TVector3F& center)
{
    m_center = center;

    if (m_child[0] == nullptr)
    {
        // Création des nœuds enfant
        for (int i = 0; i < 8; ++i)
        {
            m_child[i] = new COctree();
            T_ASSERT(m_child[i] != nullptr);
        }
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBSPTree.cpp
 * \date       2008 Création de la classe CBSPTree.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CBSPTree.hpp"
#include "Physic/Impact.hpp"
#include "Physic/CBSPTreeNode.hpp"
#include "Physic/ICollisionVolume.hpp"

// DEBUG
#include "Core/Allocation.hpp"


namespace Ted
{

const unsigned short MAX_IMPACT = 5; ///< Nombre maximal de recherches d'impact.
const float MIN_MOVE = 0.2f;         ///< Mouvement minimal.


/**
 * Constructeur par défaut.
 *
 * \param node Pointeur sur le node.
 ******************************/

CBSPTree::CBSPTree(CBSPTreeNode * node) :
m_node (node)
{ }


/**
 * Destructeur.
 ******************************/

CBSPTree::~CBSPTree()
{
    delete m_node;
}


/**
 * Accesseur pour node.
 *
 * \return Pointeur sur le node parent.
 ******************************/

CBSPTreeNode * CBSPTree::getHeadNode() const
{
    return m_node;
}


/**
 * Mutateur pour node.
 *
 * \param node Pointeur sur le node parent.
 ******************************/

void CBSPTree::setHeadNode(CBSPTreeNode * node)
{
    m_node = node;
}


/**
 * Donne le type de contenu pour un point.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType CBSPTree::getContentType(const TVector3F& position) const
{
    if (m_node)
    {
        return m_node->getContentType(position);
    }

    return Empty;
}


/**
 * Indique si un déplacement est correct.
 *
 * \param from Point de départ.
 * \param to Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CBSPTree::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    return (m_node && m_node->isCorrectMovement(from, to));
}


/**
 * Cherche un déplacement correct pour un volume.
 *
 * \todo Vérifier.
 *
 * \param volume Pointeur sur le volume à déplacer.
 * \param movement Vecteur de déplacement.
 * \return Booléen indiquant s'il y a eu collision.
 ******************************/

bool CBSPTree::Slide(ICollisionVolume& volume, const TVector3F& movement) const
{
    float dot;

    // Indique s'il a collision
    bool isCollision = false;

    // Objets de récupération d'impact
    Impact impact;
    Impact brush;

    TVector3F pos1 = volume.getPosition();
    TVector3F pos2 = Origin3F;
    TVector3F move = Origin3F;
    TVector3F edge = Origin3F;
    TVector3F velocity = movement;
    TVector3F trajet_restant = movement;

    TVector3F normals[MAX_IMPACT];
    unsigned int normals_index = 0;

    // Valeur représentant la fraction restante du mouvement de départ
    float frac = 1.0f;

    for (int loops = 0; loops < MAX_IMPACT; ++loops)
    {
        // On calcule le point d'arrivée de la forme
        pos2 = pos1 + trajet_restant;

        // Initialise les objets impact
        impact.Reset(pos1, pos2);
        brush.Reset(pos1, pos2);

        // Recherche d'impact sur le BSP
        m_node->getCollision(volume, impact, brush);

        // Si impact, on l'indique et on continue
        if (impact.isImpact())
        {
            isCollision = true;
        }
        // Sinon, on assigne le point d'arrivée à la forme et on stoppe net
        else
        {
            volume.setPosition(pos2);
            return isCollision;
        }


        //Si frac = 0
        //On augmente légèrement z jusqu'à une valeur maximale (pour franchir une marche d'escalier)
        //...


        // On met à jour la fraction restante
        frac -= frac * impact.getFracImpact();

        // On calcule le trajet jusqu'à l'impact
        move = (impact.getPointTo() - impact.getPointFrom()) * impact.getFracImpact();

        // Si la distance forme-impact est non négligeable, on remet les normales à 0
        if (move.norm() > MIN_MOVE)
        {
            normals_index = 0;
        }

        // On stocke la nouvelle normale à l'impact
        normals[normals_index] = impact.getNormale();
        ++normals_index;

        // On avance le point de départ jusqu'à l'impact
        pos1 += move;

        unsigned int i;

        // On calcule la nouvelle vélocité
        for (i = 0; i < normals_index; ++i)
        {
            unsigned int j;

            // On projette la vélocité sur le "mur" représenté par la normale, et on la stocke dans trajet_restant
            dot = VectorDot(velocity, normals[i]);

            trajet_restant = velocity - normals[i] * dot;

            // Si une autre normale est "en face" de trajet_restant, on sort de cette boucle
            for (j = 0; j < normals_index; ++j)
            {
                if (j != i && VectorDot(trajet_restant, normals[j]) < 0.0f)
                    break;
            }

            // Si pas toutes les normales en face de ce trajet_restant, on sort
            if (j == normals_index)
                break;
        }

        // Si toutes les normales peuvent être prises en compte pour la nouvelle vélocité
        if (i == normals_index)
        {
            // 2 normales
            if (normals_index == 2)
            {
                // On calcule l'arête entre les 2 murs
                edge = VectorCross(normals[0], normals[1]);
                edge.normalize();

                // On projette la vélocité dessus
                dot = VectorDot(edge, velocity);

                trajet_restant = edge * dot;
            }
            // 3 normales
            else
            {
                // On stoppe net
                volume.setPosition(pos1);
                return isCollision;
            }
        }

        // On raccourci le trajet restant selon la fraction de mouvement restante
        trajet_restant *= frac;

        // Si le trajet restant va "contre" la vélocité originale ou si sa longueur est trop petite
        if (VectorDot(velocity, trajet_restant) < 0.0f || trajet_restant.norm() < MIN_MOVE)
        {
            // On stoppe net
            volume.setPosition(pos1);
            return isCollision;
        }
    }

    // Retourne le résultat de collision, normalement, on n'atteint pas cette ligne
    return isCollision;
}


/**
 * Recherche une collision avec un volume.
 *
 * \param volume Volume.
 * \param impact Impact.
 * \param brush Impact temporaire.
 ******************************/

void CBSPTree::getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const
{
    if (m_node)
    {
        m_node->getCollision(volume, impact, brush);
    }
}


/**
 * Indique s'il y a intersection entre l'arbre BSP et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CBSPTree::hasImpact(const CRay& ray) const
{
    return (m_node && m_node->hasImpact(ray));
}


/**
 * Cherche l'intersection entre l'arbre BSP et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CBSPTree::getRayImpact(const CRay& ray) const
{
    CRayImpact impact;

    if (m_node)
    {
        m_node->getRayImpact(ray, impact);
    }

    return impact;
}


/**
 * Vérifie si l'arbre est correct.
 ******************************/

void CBSPTree::CheckTree()
{
    if (m_node)
    {
        m_node->CheckTree();
    }
}


#ifdef T_DEBUG

/**
 * Log le contenu de l'arbre BSP.
 ******************************/

void CBSPTree::Debug_LogTree() const
{
    if (m_node)
    {
        m_node->Debug_LogTree();
    }
}

#endif

} // Namespace Ted

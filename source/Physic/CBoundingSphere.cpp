/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBoundingSphere.cpp
 * \date 02/02/2010 Création de la classe CBoundingSphere.
 * \date 11/07/2010 La classe CSphereBox s'appelle désormais CBoundingSphere.
 *                  Test d'intersection avec une CBoundingBox ou un CTriangle.
 * \date 14/07/2010 Calcul de l'impact avec un rayon.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CBoundingSphere.hpp"
#include "Physic/CBoundingBox.hpp"
#include "Physic/CHitBox.hpp"
#include "Physic/CCollisionMesh.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param center Centre de la sphère.
 * \param radius Rayon de la sphère.
 ******************************/

CBoundingSphere::CBoundingSphere(const TVector3F& center, float radius) :
ICollisionVolume (),
m_center         (center),
m_radius         (radius)
{
    if (m_radius < 0.1f)
        m_radius = 0.1f;
}


/**
 * Accesseur pour center.
 *
 * \return Centre de la sphère.
 ******************************/

TVector3F CBoundingSphere::getPosition() const
{
    return m_center;
}


/**
 * Accesseur pour radius.
 *
 * \return Rayon de la sphère.
 ******************************/

float CBoundingSphere::getRadius() const
{
    return m_radius;
}


/**
 * Modifie la position de la sphère.
 *
 * \param position Centre de la pshère.
 ******************************/

void CBoundingSphere::setPosition(const TVector3F& position)
{
    m_center = position;
}


/**
 * Translate la sphère.
 *
 * \param v Vecteur de translation.
 ******************************/

void CBoundingSphere::translate(const TVector3F& v)
{
    m_center += v;
}


/**
 * Mutateur pour radius.
 *
 * \param radius Rayon de la pshère.
 ******************************/

void CBoundingSphere::setRadius(float radius)
{
    m_radius = (radius < 0.1f ? 0.1f : radius);
}


/**
 * Donne l'offset de la sphère par rapport à un plan.
 *
 * \return Offset de la sphère (c'est-à-dire son rayon).
 ******************************/

float CBoundingSphere::getOffset(const CPlane& plane) const
{
    T_UNUSED(plane);
    return m_radius;
}


/**
 * Donne le type de contenu pour un point.
 *
 * \param position Position du point.
 * \return Type de contenu (solide ou vide).
 ******************************/

TContentType CBoundingSphere::getContentType(const TVector3F& position) const
{
    TVector3F tmp = position - m_center;
    return (tmp.norm() > m_radius ? Empty : Solid);
}


/**
 * Indique si un déplacement est correct.
 *
 * \todo Implémentation.
 *
 * \param from Point de départ.
 * \param to Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    T_UNUSED_UNIMPLEMENTED(from);
    T_UNUSED_UNIMPLEMENTED(to);

    //...

    return true;
}


/**
 * Indique s'il y a intersection entre la sphère et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::hasImpact(const CRay& ray) const
{
    float beta  = VectorDot(ray.direction, ray.origin - m_center);

    return ((beta * beta - VectorDot(ray.direction, ray.direction) *
             (VectorDot(ray.origin, ray.origin) + VectorDot(m_center, m_center) -
               2 * (VectorDot(ray.origin, m_center)) - m_radius * m_radius)) > 0);
}


/**
 * Cherche l'intersection entre la sphère et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CBoundingSphere::getRayImpact(const CRay& ray) const
{
    CRayImpact impact;

    TVector3F ray_origin_sphere = ray.origin - m_center;

    float a = VectorDot(ray.direction, ray.direction);
    float b = 2 * VectorDot(ray.direction, ray_origin_sphere);
    float c = VectorDot(ray_origin_sphere, ray_origin_sphere) - m_radius * m_radius;

    // On résout : a * t² + b * t + c = 0
    float delta = b * b - 4 * a * c; // Discriminant

    // Intersection
    if (delta > 0)
    {
        float deltasqrt = std::sqrt(delta);
        float tmp = 0.5f / a;

        float t1 = (-b + deltasqrt) * tmp;
        float t2 = (-b - deltasqrt) * tmp;

        impact.delta = (t1 > 0 && (t1 < t2 || t2 < 0) ? t1 : t2);
        impact.point = ray.origin + ray.direction * impact.delta;

        impact.normale = impact.point - m_center;
        impact.normale.normalize();
    }

    return impact;
}


/**
 * Indique si une sphère englobante entre en collision avec une boite englobante.
 *
 * \param box Boite englobante à tester.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::isIntersecting(const CBoundingBox& box) const
{
    return box.isIntersecting(*this);
}


/**
 * Indique si une sphère englobante entre en collision avec une autre.
 *
 * \param sphere Sphère à tester.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::isIntersecting(const CBoundingSphere& sphere) const
{
    TVector3F tmp = m_center - sphere.getPosition();
    return (tmp.norm() < m_radius + sphere.getRadius());
}


/**
 * Indique si une sphère englobante entre en collision avec une boite orientée.
 *
 * \param box Boite orientée à tester.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::isIntersecting(const CHitBox& box) const
{
    return box.isIntersecting(*this);
}


/**
 * Indique si une sphère englobante entre en collision avec un triangle.
 *
 * \todo Implémentation.
 *
 * \param triangle Triangle à tester.
 * \return Booléen.
 ******************************/

bool CBoundingSphere::isIntersecting(const CTriangle& triangle) const
{
    T_UNUSED_UNIMPLEMENTED(triangle);

    //...

    return false;
}

} // Namespace Ted

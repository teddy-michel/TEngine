/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CCollisionMesh.cpp
 * \date 28/06/2010 Création de la classe CCollisionMesh.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CCollisionMesh.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CCollisionMesh::CCollisionMesh() :
ICollisionVolume ()
{ }


/**
 * Donne la position du maillage.
 *
 * \return Position du maillage.
 ******************************/

TVector3F CCollisionMesh::getPosition() const
{
    TVector3F p = Origin3F;

    for (std::vector<TVector3F>::const_iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        p += *it;
    }

    return p / m_vertices.size();
}


/**
 * Modifie la position du maillage.
 *
 * \param position Nouvelle position du maillage.
 ******************************/

void CCollisionMesh::setPosition(const TVector3F& position)
{
    translate(position - getPosition());
}


/**
 * Translate le maillage.
 *
 * \param v Vecteur de translation.
 ******************************/

void CCollisionMesh::translate(const TVector3F& v)
{
    // Pour chaque sommet
    for (std::vector<TVector3F>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        *it += v;
    }
}


/**
 * Donne l'offset du maillage par rapport à un plan.
 *
 * \todo Implémentation.
 *
 * \param plane Plan de référence.
 * \return Offset.
 ******************************/

float CCollisionMesh::getOffset(const CPlane& plane) const
{
    T_UNUSED_UNIMPLEMENTED(plane);

    //...

    return 1.0f;
}


/**
 * Donne le contenu pour un point.
 *
 * \todo Implémentation.
 *
 * \param position Position du point.
 * \return Type de contenu (solide ou vide).
 ******************************/

TContentType CCollisionMesh::getContentType(const TVector3F& position) const
{
    T_UNUSED_UNIMPLEMENTED(position);

    //...

    return Empty;
}


/**
 * Indique si un mouvement est correct par rapport au maillage.
 *
 * \todo Implémentation.
 *
 * \param from Point de départ.
 * \param to Point d'arrivé.
 * \return Booléen.
 ******************************/

bool CCollisionMesh::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    T_UNUSED_UNIMPLEMENTED(from);
    T_UNUSED_UNIMPLEMENTED(to);

    //...

    return true;
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBSPTreeLeaf.cpp
 * \date 09/05/2009 Création de la classe CBSPTreeLeaf.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CBSPTreeLeaf.hpp"
#include "Physic/ICollisionVolume.hpp"
#include "Physic/Impact.hpp"

#ifdef T_DEBUG
#  include "Core/ILogger.hpp"
#  include "Core/CApplication.hpp"
#endif


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CBSPTreeLeaf::CBSPTreeLeaf(TContentType type) :
m_type (type)
{ }


/**
 * Destructeur.
 ******************************/

CBSPTreeLeaf::~CBSPTreeLeaf()
{ }


/**
 * Indique si l'élément de l'arbre est une feuille.
 *
 * \return true.
 ******************************/

bool CBSPTreeLeaf::isLeaf() const
{
    return true;
}


/**
 * Indique si l'élément de l'arbre est un nœud.
 *
 * \return false.
 ******************************/

bool CBSPTreeLeaf::isNode() const
{
    return false;
}


/**
 * Donne le type de feuilles.
 *
 * \return Type de feuilles.
 ******************************/

TContentType CBSPTreeLeaf::getType() const
{
    return m_type;
}


/**
 * Mutateur pour type.
 *
 * \param type Type de feuilles.
 ******************************/

void CBSPTreeLeaf::setType(TContentType type)
{
    m_type = type;
}


/**
 * Cherche le type de contenu en fonction d'une position.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType CBSPTreeLeaf::getContentType(const TVector3F& position) const
{
    T_UNUSED(position);
    return m_type;
}


/**
 * Indique si un déplacement est correct.
 *
 * \param from Point de départ.
 * \param to Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CBSPTreeLeaf::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    T_UNUSED(from);
    T_UNUSED(to);

    return (m_type != Solid);
}


/**
 * Recherche une collision avec un volume.
 *
 * \param volume Volume.
 * \param impact Impact.
 * \param brush  Impact temporaire.
 ******************************/

void CBSPTreeLeaf::getCollision(const ICollisionVolume& volume, Impact& impact, Impact& brush) const
{
    T_UNUSED(volume);

    // Feuille solide
    if (m_type == Solid)
    {
        // On transfère brush dans impact si plus près
        impact.setNearerImpact(brush.getNormale(), brush.getFracImpact(), brush.getFracReelle());
    }

    // On réinitialise iBrush
    brush.clearImpact();
}


/**
 * Indique s'il y a intersection entre l'arbre BSP et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CBSPTreeLeaf::hasImpact(const CRay& ray) const
{
    T_UNUSED(ray);
    return (m_type == Solid);
}


/**
 * Cherche l'intersection entre l'arbre BSP et un rayon.
 *
 * \param ray    Rayon à utiliser.
 * \param impact Impact du rayon avec l'arbre.
 * \return Paramètres de l'impact.
 ******************************/

bool CBSPTreeLeaf::getRayImpact(const CRay& ray, CRayImpact& impact) const
{
    T_UNUSED(ray);
    T_UNUSED(impact);

    return (m_type == Solid);
}


/**
 * Vérifie si l'arbre est correct.
 *
 * \return Indique si la feuille est vide.
 ******************************/

bool CBSPTreeLeaf::CheckTree()
{
    return (m_type != Solid);
}


#ifdef T_DEBUG

/**
 * Log le contenu de l'arbre BSP.
 *
 * \param offset Espaces à insérer au début de la ligne.
 ******************************/

void CBSPTreeLeaf::Debug_LogTree(unsigned int offset) const
{
    CString str(' ', offset);
    str += "[" + CString::fromPointer(this) + "] : " + m_type;

    CApplication::getApp()->log(str);
}

#endif // T_DEBUG

} // Namespace Ted

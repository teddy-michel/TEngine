/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CBoundingBox.cpp
 * \date 02/02/2010 Création de la classe CBoundingBox.
 * \date 11/07/2010 Le test d'intersection avec une bounding box est correct.
 *                  Test d'intersection avec une sphère ou un triangle.
 * \date 14/07/2010 Implémentation du test d'intersection avec un triangle et avec un rayon.
 * \date 16/12/2010 Ajout de la méthode isInside.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CBoundingBox.hpp"
#include "Physic/CBoundingSphere.hpp"
#include "Physic/CHitBox.hpp"
#include "Physic/CCollisionMesh.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CBoundingBox::CBoundingBox() :
ICollisionVolume (),
m_min            (Origin3F),
m_max            (Origin3F)
{ }


/**
 * Constructeur.
 *
 * \param min Point minimal.
 * \param max Point maximal.
 ******************************/

CBoundingBox::CBoundingBox(const TVector3F& min, const TVector3F& max) :
ICollisionVolume (),
m_min            (min),
m_max            (max)
{ }


/**
 * Donne le centre de la boite.
 *
 * \return Centre de la boite.
 ******************************/

TVector3F CBoundingBox::getPosition() const
{
    return (m_max + m_min) * 0.5f;
}


/**
 * Accesseur pour min.
 *
 * \return Point minimal.
 ******************************/

TVector3F CBoundingBox::getMin() const
{
    return m_min;
}


/**
 * Accesseur pour max.
 *
 * \return Point maximal.
 ******************************/

TVector3F CBoundingBox::getMax() const
{
    return m_max;
}


/**
 * Modifie la position de la boite.
 *
 * \param position Position de la boite.
 ******************************/

void CBoundingBox::setPosition(const TVector3F& position)
{
    translate(position - getPosition());
}


/**
 * Translate la boite.
 *
 * \param v Vecteur de translation.
 ******************************/

void CBoundingBox::translate(const TVector3F& v)
{
    m_min += v;
    m_max += v;
}


/**
 * Mutateur pour min.
 *
 * \param min Point minimal.
 ******************************/

void CBoundingBox::setMin(const TVector3F& min)
{
    if (min.X < m_max.X)
    {
        m_min.X = min.X;
    }
    else
    {
        float tmp = m_max.X;
        m_max.X = min.X;
        m_min.X = tmp;
    }

    if (min.Y < m_max.Y)
    {
        m_min.Y = min.Y;
    }
    else
    {
        float tmp = m_max.Y;
        m_max.Y = min.Y;
        m_min.Y = tmp;
    }

    if (min.Z < m_max.Z)
    {
        m_min.Z = min.Z;
    }
    else
    {
        float tmp = m_max.Z;
        m_max.Z = min.Z;
        m_min.Z = tmp;
    }
}


/**
 * Mutateur pour max.
 *
 * \param max Point maximal.
 ******************************/

void CBoundingBox::setMax(const TVector3F& max)
{
    if (max.X > m_min.X)
    {
        m_max.X = max.X;
    }
    else
    {
        float tmp = m_min.X;
        m_min.X = max.X;
        m_max.X = tmp;
    }

    if (max.Y > m_min.Y)
    {
        m_max.Y = max.Y;
    }
    else
    {
        float tmp = m_min.Y;
        m_min.Y = max.Y;
        m_max.Y = tmp;
    }

    if (max.Z > m_min.Z)
    {
        m_max.Z = max.Z;
    }
    else
    {
        float tmp = m_min.Z;
        m_min.Z = max.Z;
        m_max.Z = tmp;
    }

    return;
}


/**
 * Ajoute un point à la boudning box.
 *
 * \param point Point à ajouter.
 ******************************/

void CBoundingBox::addPoint(const TVector3F& point)
{
    if (point.X < m_min.X)
        m_min.X = point.X;
    else if (point.X > m_max.X)
        m_max.X = point.X;

    if (point.Y < m_min.Y)
        m_min.Y = point.Y;
    else if (point.Y > m_max.Y)
        m_max.Y = point.Y;

    if (point.Z < m_min.Z)
        m_min.Z = point.Z;
    else if (point.Z > m_max.Z)
        m_max.Z = point.Z;
}


#ifdef T_DEBUG

#include <GL/glew.h>

/**
 * Dessine la bounding box.
 * Cette méthode ne doit pas être utilisée lors de l'utilisation normale de l'application.
 *
 * \todo Utiliser un buffer graphique.
 ******************************/

void CBoundingBox::Debug_Draw() const
{
    glColor4ub(255, 0, 0, 255);

    glBegin(GL_LINES);

    glVertex3f(m_min.X, m_min.Y, m_min.Z);
    glVertex3f(m_min.X, m_min.Y, m_max.Z);

    glVertex3f(m_min.X, m_min.Y, m_max.Z);
    glVertex3f(m_min.X, m_max.Y, m_max.Z);

    glVertex3f(m_min.X, m_max.Y, m_max.Z);
    glVertex3f(m_min.X, m_max.Y, m_min.Z);

    glVertex3f(m_min.X, m_max.Y, m_min.Z);
    glVertex3f(m_min.X, m_min.Y, m_min.Z);

    glVertex3f(m_max.X, m_min.Y, m_min.Z);
    glVertex3f(m_max.X, m_min.Y, m_max.Z);

    glVertex3f(m_max.X, m_min.Y, m_max.Z);
    glVertex3f(m_max.X, m_max.Y, m_max.Z);

    glVertex3f(m_max.X, m_max.Y, m_max.Z);
    glVertex3f(m_max.X, m_max.Y, m_min.Z);

    glVertex3f(m_max.X, m_max.Y, m_min.Z);
    glVertex3f(m_max.X, m_min.Y, m_min.Z);

    glVertex3f(m_min.X, m_min.Y, m_min.Z);
    glVertex3f(m_max.X, m_min.Y, m_min.Z);

    glVertex3f(m_min.X, m_min.Y, m_max.Z);
    glVertex3f(m_max.X, m_min.Y, m_max.Z);

    glVertex3f(m_min.X, m_max.Y, m_max.Z);
    glVertex3f(m_max.X, m_max.Y, m_max.Z);

    glVertex3f(m_min.X, m_max.Y, m_min.Z);
    glVertex3f(m_max.X, m_max.Y, m_min.Z);

    glEnd();

    glColor4ub(255, 255, 255, 255);
}

#endif


/**
 * Donne l'offset de la boite par rapport à un plan.
 *
 * \todo Vérifier.
 *
 * \return Offset de la boite.
 ******************************/

float CBoundingBox::getOffset(const CPlane& plane) const
{
    TVector3F pt;
    TVector3F n = plane.getNormale();

    if (n.X < 0)
    {
        if (n.Y < 0)
        {
            if (n.Z < 0)
            {
                pt = TVector3F(m_max.X, m_max.Y, m_max.Z);
            }
            else
            {
                pt = TVector3F(m_max.X, m_max.Y, m_min.Z);
            }
        }
        else
        {
            if (n.Z < 0)
            {
                pt = TVector3F(m_max.X, m_min.Y, m_max.Z);
            }
            else
            {
                pt = TVector3F(m_max.X, m_min.Y, m_min.Z);
            }
        }
    }
    else
    {
        if (n.Y < 0)
        {
            if (n.Z < 0)
            {
                pt = TVector3F(m_min.X, m_max.Y, m_max.Z);
            }
            else
            {
                pt = TVector3F(m_min.X, m_max.Y, m_min.Z);
            }
        }
        else
        {
            if (n.Z < 0)
            {
                pt = TVector3F(m_min.X, m_min.Y, m_max.Z);
            }
            else
            {
                pt = TVector3F(m_min.X, m_min.Y, m_min.Z);
            }
        }
    }

    CPlane p(n, VectorDot(n, pt));
    return std::abs(p.getDistance((m_max + m_min) * 0.5f));
}


/**
 * Donne le type de contenu pour un point.
 *
 * \param position Position du point.
 * \return Type de contenu (solide ou vide).
 ******************************/

TContentType CBoundingBox::getContentType(const TVector3F& position) const
{
    return (position.X < m_min.X && position.X > m_max.X &&
            position.Y < m_min.Y && position.Y > m_max.Y &&
            position.Z < m_min.Z && position.Z > m_max.Z ? Empty : Solid);
}


/**
 * Indique si un déplacement est correct.
 *
 * \todo Implémentation.
 *
 * \param from Point de départ.
 * \param to Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    T_UNUSED_UNIMPLEMENTED(from);
    T_UNUSED_UNIMPLEMENTED(to);

    //...

    return true;
}

// Il semblerait que ces macros soient définies quelque part
#ifdef near
#  undef near
#endif

#ifdef far
#  undef far
#endif

/**
 * Indique s'il y a intersection entre la boite et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CBoundingBox::hasImpact(const CRay& ray) const
{
    float t0 = 0.0f;
    float t1 = RAY_MAX_LENGTH;

    {
        float invRayDir = 1.0f / ray.direction.X;
        float near = (m_min.X - ray.origin.X) * invRayDir;
        float far = (m_max.X - ray.origin.X) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return false;
    }

    {
        float invRayDir = 1.0f / ray.direction.Y;
        float near = (m_min.Y - ray.origin.Y) * invRayDir;
        float far = (m_max.Y - ray.origin.Y) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return false;
    }

    {
        float invRayDir = 1.0f / ray.direction.Z;
        float near = (m_min.Z - ray.origin.Z) * invRayDir;
        float far = (m_max.Z - ray.origin.Z) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return false;
    }

    return true;
}


/**
 * Cherche l'intersection entre la boite et un rayon.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CBoundingBox::getRayImpact(const CRay& ray) const
{
    CRayImpact impact;

    float t0 = 0.0f;
    float t1 = RAY_MAX_LENGTH; // std::numeric_limits<float>::max()

    {
        float invRayDir = 1.0f / ray.direction.X;
        float near = (m_min.X - ray.origin.X) * invRayDir;
        float far = (m_max.X - ray.origin.X) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return impact; // Pas d'intersection
    }

    {
        float invRayDir = 1.0f / ray.direction.Y;
        float near = (m_min.Y - ray.origin.Y) * invRayDir;
        float far = (m_max.Y - ray.origin.Y) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return impact; // Pas d'intersection
    }

    {
        float invRayDir = 1.0f / ray.direction.Z;
        float near = (m_min.Z - ray.origin.Z) * invRayDir;
        float far = (m_max.Z - ray.origin.Z) * invRayDir;

        // On inverse near et far
        if (near > far)
        {
            float temp = near;
            near = far;
            far = temp;
        }

        t0 = (near > t0 ? near : t0);
        t1 = (far < t1 ? far : t1);

        if (t0 > t1)
            return impact; // Pas d'intersection
    }

    // Intersection
    impact.point = ray.origin + ray.direction * t0;
    impact.delta = t0;

         if (std::abs(impact.point.X - m_min.X) < std::numeric_limits<float>::epsilon()) impact.normale.set(-1.0f,  0.0f,  0.0f);
    else if (std::abs(impact.point.Y - m_min.Y) < std::numeric_limits<float>::epsilon()) impact.normale.set( 0.0f, -1.0f,  0.0f);
    else if (std::abs(impact.point.Z - m_min.Z) < std::numeric_limits<float>::epsilon()) impact.normale.set( 0.0f,  0.0f, -1.0f);
    else if (std::abs(impact.point.X - m_max.X) < std::numeric_limits<float>::epsilon()) impact.normale.set( 1.0f,  0.0f,  0.0f);
    else if (std::abs(impact.point.Y - m_max.Y) < std::numeric_limits<float>::epsilon()) impact.normale.set( 0.0f,  1.0f,  0.0f);
    else if (std::abs(impact.point.Z - m_max.Z) < std::numeric_limits<float>::epsilon()) impact.normale.set( 0.0f,  0.0f,  1.0f);

    return impact;
}


/**
 * Indique si une bounding box est à l'intérieur d'une autre.
 *
 * \todo Vérifier et utiliser la version optimisée.
 *
 * \param box Bounding box qui peut être à l'intérieur de l'autre.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isInside(const CBoundingBox& box) const
{
    TVector3F min = box.getMin();
    TVector3F max = box.getMax();

    return (max.X <= m_max.X && min.X >= m_min.X &&
            max.Y <= m_max.Y && min.Y >= m_min.Y &&
            max.Z <= m_max.Z && min.Z >= m_min.Z);
/*
    // Version optimisé, à vérifier
    return !(max.X > m_max.X || min.X < m_min.X &&
             max.Y > m_max.Y || min.Y < m_min.Y &&
             max.Z > m_max.Z || min.Z < m_min.Z);
*/
}


/**
 * Indique si une bounding box entre en collision avec une autre.
 *
 * \param box Bounding box à tester.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isIntersecting(const CBoundingBox& box) const
{
    TVector3F min = box.getMin();
    TVector3F max = box.getMax();

    return !(max.X < m_min.X || min.X > m_max.X ||
             max.Y < m_min.Y || min.Y > m_max.Y ||
             max.Z < m_min.Z || min.Z > m_max.Z);
}


/**
 * Indique si une bounding box entre en collision avec une sphère englobante.
 *
 * \param sphere Sphère englobante à tester.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isIntersecting(const CBoundingSphere& sphere) const
{
    // See Graphics Gems, box-sphere intersection
    float dmin = 0.0f;

    TVector3F center = sphere.getPosition();

    // Unrolled the loop.. this is a big cycle stealer...
    if (center.X < m_min.X)
    {
        float delta = center.X - m_min.X;
        dmin += delta * delta;
    }
    else if (center.X > m_max.X)
    {
        float delta = m_max.X - center.X;
        dmin += delta * delta;
    }

    if (center.Y < m_min.Y)
    {
        float delta = center.Y - m_min.Y;
        dmin += delta * delta;
    }
    else if (center.Y > m_max.Y)
    {
        float delta = m_max.Y - center.Y;
        dmin += delta * delta;
    }

    if (center.Z < m_min.Z)
    {
        float delta = center.Z - m_min.Z;
        dmin += delta * delta;
    }
    else if (center.Z > m_max.Z)
    {
        float delta = m_max.Z - center.Z;
        dmin += delta * delta;
    }

    float radius = sphere.getRadius();
    return (dmin < radius * radius);
}


/**
 * Indique si une bounding box entre en collision avec une boite orientée.
 *
 * \param box Boite orientée à tester.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isIntersecting(const CHitBox& box) const
{
    return box.isIntersecting(*this);
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeZ ?
 * \param flEdgeY ?
 * \param flAbsEdgeZ ?
 * \param flAbsEdgeY ?
 * \param p1 ?
 * \param p3 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossX2(float flEdgeZ, float flEdgeY, float flAbsEdgeZ, float flAbsEdgeY,
                                const TVector3F& p1, const TVector3F& p3, const TVector3F& vecExtents)
{
    // Cross Product(axialX(1,0,0) x edge): x = 0.0f, y = edge.z, z = -edge.y
    // Triangle Point Distances: dist(x) = normal.y * pt(x).y + normal.z * pt(x).z
    float dist1 = flEdgeZ * p1.Y - flEdgeY * p1.Z;
    float dist3 = flEdgeZ * p3.Y - flEdgeY * p3.Z;

    // Extents are symmetric: dist = abs(normal.y) * extents.y + abs(normal.z) * extents.z
    float flDistBox = flAbsEdgeZ * vecExtents.Y + flAbsEdgeY * vecExtents.Z;

    // Either dist1, dist3 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist1 < dist3)
    {
        if (dist1 > flDistBox || dist3 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist3 > flDistBox || dist1 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeZ ?
 * \param flEdgeY ?
 * \param flAbsEdgeZ ?
 * \param flAbsEdgeY ?
 * \param p1 ?
 * \param p2 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossX3(float flEdgeZ, float flEdgeY, float flAbsEdgeZ, float flAbsEdgeY,
                                const TVector3F& p1, const TVector3F& p2, const TVector3F& vecExtents)
{
    // Cross Product(axialX(1,0,0) x edge): x = 0.0f, y = edge.z, z = -edge.y
    // Triangle Point Distances: dist(x) = normal.y * pt(x).y + normal.z * pt(x).z
    float dist1 = flEdgeZ * p1.Y - flEdgeY * p1.Z;
    float dist2 = flEdgeZ * p2.Y - flEdgeY * p2.Z;

    // Extents are symmetric: dist = abs(normal.y) * extents.y + abs( normal.z ) * extents.z
    float flDistBox = flAbsEdgeZ * vecExtents.Y + flAbsEdgeY * vecExtents.Z;

    // Either dist1, dist2 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist1 < dist2)
    {
        if (dist1 > flDistBox || dist2 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist2 > flDistBox || dist1 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeZ ?
 * \param flEdgeX ?
 * \param flAbsEdgeZ ?
 * \param flAbsEdgeX ?
 * \param p1 ?
 * \param p3 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossY2(float flEdgeZ, float flEdgeX, float flAbsEdgeZ, float flAbsEdgeX,
                                const TVector3F& p1, const TVector3F& p3, const TVector3F& vecExtents)
{
    // Cross Product(axialY(0,1,0) x edge): x = -edge.z, y = 0.0f, z = edge.x
    // Triangle Point Distances: dist(x) = normal.x * pt(x).x + normal.z * pt(x).z
    float dist1 = -flEdgeZ * p1.X + flEdgeX * p1.Z;
    float dist3 = -flEdgeZ * p3.X + flEdgeX * p3.Z;

    // Extents are symmetric: dist = abs(normal.x) * extents.x + abs(normal.z) * extents.z
    float flDistBox = flAbsEdgeZ * vecExtents.X + flAbsEdgeX * vecExtents.Z;

    // Either dist1, dist3 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist1 < dist3)
    {
        if (dist1 > flDistBox || dist3 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist3 > flDistBox || dist1 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeZ ?
 * \param flEdgeX ?
 * \param flAbsEdgeZ ?
 * \param flAbsEdgeX ?
 * \param p1 ?
 * \param p2 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossY3(float flEdgeZ, float flEdgeX, float flAbsEdgeZ, float flAbsEdgeX,
                                const TVector3F& p1, const TVector3F& p2, const TVector3F& vecExtents)
{
    // Cross Product(axialY(0,1,0) x edge): x = -edge.z, y = 0.0f, z = edge.x
    // Triangle Point Distances: dist(x) = normal.x * pt(x).x + normal.z * pt(x).z
    float dist1 = -flEdgeZ * p1.X + flEdgeX * p1.Z;
    float dist2 = -flEdgeZ * p2.X + flEdgeX * p2.Z;

    // Extents are symmetric: dist = abs(normal.x) * extents.x + abs(normal.z) * extents.z
    float flDistBox = flAbsEdgeZ * vecExtents.X + flAbsEdgeX * vecExtents.Z;

    // Either dist1, dist2 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist1 < dist2)
    {
        if (dist1 > flDistBox || dist2 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist2 > flDistBox || dist1 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeY ?
 * \param flEdgeX ?
 * \param flAbsEdgeY ?
 * \param flAbsEdgeX ?
 * \param p2 ?
 * \param p3 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossZ1(float flEdgeY, float flEdgeX, float flAbsEdgeY, float flAbsEdgeX,
                                const TVector3F& p2, const TVector3F& p3, const TVector3F& vecExtents)
{
    // Cross Product(axialZ(0,0,1) x edge): x = edge.y, y = -edge.x, z = 0.0f
    // Triangle Point Distances: dist(x) = normal.x * pt(x).x + normal.y * pt(x).y
    float dist2 = flEdgeY * p2.X - flEdgeX * p2.Y;
    float dist3 = flEdgeY * p3.X - flEdgeX * p3.Y;

    // Extents are symmetric: dist = abs(normal.x) * extents.x + abs(normal.y) * extents.y
    float flDistBox = flAbsEdgeY * vecExtents.X + flAbsEdgeX * vecExtents.Y;

    // Either dist2, dist3 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist3 < dist2)
    {
        if (dist3 > flDistBox || dist2 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist2 > flDistBox || dist3 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param flEdgeY ?
 * \param flEdgeX ?
 * \param flAbsEdgeY ?
 * \param flAbsEdgeX ?
 * \param p1 ?
 * \param p3 ?
 * \param vecExtents ?
 * \return Booléen
 ******************************/

inline bool AxisTestEdgeCrossZ2(float flEdgeY, float flEdgeX, float flAbsEdgeY, float flAbsEdgeX,
                                const TVector3F& p1, const TVector3F& p3, const TVector3F& vecExtents)
{
    // Cross Product(axialZ(0,0,1) x edge): x = edge.y, y = -edge.x, z = 0.0f
    // Triangle Point Distances: dist(x) = normal.x * pt(x).x + normal.y * pt(x).y
    float dist1 = flEdgeY * p1.X - flEdgeX * p1.Y;
    float dist3 = flEdgeY * p3.X - flEdgeX * p3.Y;

    // Extents are symmetric: dist = abs(normal.x) * extents.x + abs(normal.y) * extents.y
    float flDistBox = flAbsEdgeY * vecExtents.X + flAbsEdgeX * vecExtents.Y;

    // Either dist1, dist3 is the closest point to the box, determine which and test of overlap with box(AABB).
    if (dist1 < dist3)
    {
        if (dist1 > flDistBox || dist3 < -flDistBox)
        {
            return false;
        }
    }
    else
    {
        if (dist3 > flDistBox || dist1 < -flDistBox)
        {
            return false;
        }
    }

    return true;
}


/**
 * ?
 * Cet algorithme est issu du Source Engine.
 *
 * \param emins ?
 * \param emaxs ?
 * \param p     ?
 * \return ?
 ******************************/

inline int BoxOnPlaneSide(const TVector3F& emins, const TVector3F& emaxs, const CPlane& p)
{
    TVector3F corners[2];
    TVector3F normale = p.getNormale();

    if (normale.X < 0)
    {
        corners[0].X = emins.X;
        corners[1].X = emaxs.X;
    }
    else
    {
        corners[1].X = emins.X;
        corners[0].X = emaxs.X;
    }

    if (normale.Y < 0)
    {
        corners[0].Y = emins.Y;
        corners[1].Y = emaxs.Y;
    }
    else
    {
        corners[1].Y = emins.Y;
        corners[0].Y = emaxs.Y;
    }

    if (normale.Z < 0 )
    {
        corners[0].Z = emins.Z;
        corners[1].Z = emaxs.Z;
    }
    else
    {
        corners[1].Z = emins.Z;
        corners[0].Z = emaxs.Z;
    }

    int sides = 0;

    if (VectorDot(normale, corners[0]) >= p.getDistance()) sides = 1;
    if (VectorDot(normale, corners[1]) < p.getDistance()) sides |= 2;

    return sides;
}


/**
 * Indique si une bounding box entre en collision avec un triangle.
 * Cet algorithme est issu du Source Engine.
 *
 * \todo Calculer le plan contenant le triangle.
 *
 * \param triangle Triangle à tester.
 * \return Booléen.
 ******************************/

bool CBoundingBox::isIntersecting(const CTriangle& triangle) const
{
    //-----------------------------------------------------------------------------
    // Triangle points are in counter-clockwise order with the normal facing "out."
    //
    // Using the "Separating-Axis Theorem" to test for intersections between
    // a triangle and an axial-aligned bounding box (AABB).
    // 1. 3 Axis Plane Tests - x, y, z
    // 2. 9 Edge Planes Tests - the 3 edges of the triangle crossed with all 3 axial
    //                          planes (x, y, z)
    // 3. 1 Face Plane Test - the plane the triangle resides in (cplane_t plane)
    //-----------------------------------------------------------------------------

    TVector3F vecBoxCenter = (m_max + m_min) * 0.5f;
    TVector3F vecBoxExtents = (m_max - m_min) * 0.5f;


	CPlane plane(triangle.s1, triangle.s2, triangle.s3); // Plan contenant le triangle


    // Test the axial planes (x,y,z) against the min, max of the triangle.
    TVector3F p1 = triangle.s1 - vecBoxCenter;
    TVector3F p2 = triangle.s2 - vecBoxCenter;
    TVector3F p3 = triangle.s3 - vecBoxCenter;

    // x plane
    float min = (p1.X < p2.X ? (p3.X < p1.X ? p3.X : p1.X) : (p3.X < p2.X ? p3.X : p2.X));
    float max = (p1.X > p2.X ? (p3.X > p1.X ? p3.X : p1.X) : (p3.X > p2.X ? p3.X : p2.X));

    if (min > vecBoxExtents.X || max < -vecBoxExtents.X)
    {
        return false;
    }

    // y plane
    min = (p1.Y < p2.Y ? (p3.Y < p1.Y ? p3.Y : p1.Y) : (p3.Y < p2.Y ? p3.Y : p2.Y));
    max = (p1.Y > p2.Y ? (p3.Y > p1.Y ? p3.Y : p1.Y) : (p3.Y > p2.Y ? p3.Y : p2.Y));

    if (min > vecBoxExtents.Y || max < -vecBoxExtents.Y)
    {
        return false;
    }

    // z plane
    min = (p1.Z < p2.Z ? (p3.Z < p1.Z ? p3.Z : p1.Z) : (p3.Z < p2.Z ? p3.Z : p2.Z));
    max = (p1.Z > p2.Z ? (p3.Z > p1.Z ? p3.Z : p1.Z) : (p3.Z > p2.Z ? p3.Z : p2.Z));

    if (min > vecBoxExtents.Z || max < -vecBoxExtents.Z)
    {
        return false;
    }


    // Test the 9 edge cases.

    // edge 0 (cross x,y,z)
    TVector3F vecEdge = p2 - p1;
    TVector3F vecAbsEdge(std::abs(vecEdge.X), std::abs(vecEdge.Y), std::abs(vecEdge.Z));

    if (!AxisTestEdgeCrossX2(vecEdge.Z, vecEdge.Y, vecAbsEdge.Z, vecAbsEdge.Y, p1, p3, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossY2(vecEdge.Z, vecEdge.X, vecAbsEdge.Z, vecAbsEdge.X, p1, p3, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossZ1(vecEdge.Y, vecEdge.X, vecAbsEdge.Y, vecAbsEdge.X, p2, p3, vecBoxExtents))
    {
        return false;
    }

    // edge 1 (cross x,y,z)
    vecEdge = p3 - p2;
    vecAbsEdge.set(std::abs(vecEdge.X), std::abs(vecEdge.Y), std::abs(vecEdge.Z));

    if (!AxisTestEdgeCrossX2(vecEdge.Z, vecEdge.Y, vecAbsEdge.Z, vecAbsEdge.Y, p1, p2, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossY2(vecEdge.Z, vecEdge.X, vecAbsEdge.Z, vecAbsEdge.X, p1, p2, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossZ2(vecEdge.Y, vecEdge.X, vecAbsEdge.Y, vecAbsEdge.X, p1, p3, vecBoxExtents))
    {
        return false;
    }

    // edge 2 (cross x,y,z)
    vecEdge = p1 - p3;
    vecAbsEdge.set(std::abs(vecEdge.X), std::abs(vecEdge.Y), std::abs(vecEdge.Z));

    if (!AxisTestEdgeCrossX3(vecEdge.Z, vecEdge.Y, vecAbsEdge.Z, vecAbsEdge.Y, p1, p2, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossY3(vecEdge.Z, vecEdge.X, vecAbsEdge.Z, vecAbsEdge.X, p1, p2, vecBoxExtents))
    {
        return false;
    }

    if (!AxisTestEdgeCrossZ1(vecEdge.Y, vecEdge.X, vecAbsEdge.Y, vecAbsEdge.X, p2, p3, vecBoxExtents))
    {
        return false;
    }

    // Test against the triangle face plane.
    return (BoxOnPlaneSide(m_min, m_max, plane) == 3);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CPlane.cpp
 * \date 08/02/2010 Création de la classe CPlane.
 * \date 14/07/2010 Constructeur à partir de trois points.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CPlane.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param normale Normale au plan.
 * \param distance Distance entre le plan et l'origine.
 ******************************/

CPlane::CPlane(const TVector3F& normale, float distance) :
m_normale  (normale),
m_distance (distance)
{
    m_normale.normalize();
}


/**
 * Constructeur à partir de trois points.
 *
 * \param pt1 Premier point.
 * \param pt2 Deuxième point.
 * \param pt3 Troisième point.
 ******************************/

CPlane::CPlane(const TVector3F& pt1, const TVector3F& pt2, const TVector3F& pt3)
{
    TVector3F v1 = pt2 - pt1;
    TVector3F v2 = pt3 - pt1;

    m_normale = VectorCross(v1, v2);
    m_normale.normalize();

    m_distance = VectorDot(pt1, m_normale);
}


/**
 * Donne la normale du plan.
 *
 * \return Normale du plan.
 * \sa CPlane::setNormale
 ******************************/

TVector3F CPlane::getNormale() const
{
    return m_normale;
}


/**
 * Donne la distance du plan à l'origine.
 *
 * \return Distance du plan à l'origine.
 * \sa CPlane::setDistance
 ******************************/

float CPlane::getDistance() const
{
    return m_distance;
}


/**
 * Modifie la normale du plan.
 *
 * \param normale Normale du plan.
 * \sa CPlane::getNormale
 ******************************/

void CPlane::setNormale(const TVector3F& normale)
{
    m_normale = normale;
    m_normale.normalize();
}


/**
 * Modifie la distance du plan à l'origine.
 *
 * \param distance Distance du plan à l'origine.
 * \sa CPlane::getDistance
 ******************************/

void CPlane::setDistance(float distance)
{
    m_distance = distance;
}


/**
 * Donne la distance d'un point par rapport au plan.
 * Si la valeur retournée est négative, le point est situé derrière le plan.
 *
 * \return Distance du point au plan.
 ******************************/

float CPlane::getDistance(const TVector3F& point) const
{
    return /*std::abs*/(m_normale.X * point.X + m_normale.Y * point.Y + m_normale.Z * point.Z - m_distance);
}

} // Namespace Ted

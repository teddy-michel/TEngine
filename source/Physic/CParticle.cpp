/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CParticle.cpp
 * \date       2008 Création de la classe CParticule.
 * \date 16/07/2010 Modification de la gestion de la durée de vie.
 * \date 05/01/2010 Le déplacement des particules est fait par l'émetteur.
 * \date 08/05/2011 La classe est renommée en CParticle.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CParticle.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param position Position initiale de la particule.
 * \param vitesse  Vitesse initiale de la particule.
 * \param color    Couleur de la particule.
 * \param life     Durée de vie de la particule en millisecondes.
 ******************************/

CParticle::CParticle(const TVector3F& position, const TVector3F& vitesse, const CColor& color, unsigned int life) :
m_mass     (1.0f),
m_position (position),
m_vitesse  (vitesse),
m_color    (color),
m_life     (life)
{ }


/**
 * Accesseur pour mass.
 *
 * \return Masse de la particule en kilogrammes.
 *
 * \sa CParticle::setMass
 ******************************/

float CParticle::getMass() const
{
    return m_mass;
}


/**
 * Accesseur pour position.
 *
 * \return Position de la particule.
 *
 * \sa CParticle::setPosition
 ******************************/

TVector3F CParticle::getPosition() const
{
    return m_position;
}


/**
 * Accesseur pour vitesse.
 *
 * \return Vitesse de la particule.
 *
 * \sa CParticle::setVitesse
 ******************************/

TVector3F CParticle::getVitesse() const
{
    return m_vitesse;
}


/**
 * Accesseur pour couleur.
 *
 * \return Couleur de la particule.
 *
 * \sa CParticle::setColor
 ******************************/

CColor CParticle::getColor() const
{
    return m_color;
}


/**
 * Donne la durée de vie restant à la particule en millisecondes.
 *
 * \return Durée de vie restant à la particule.
 *
 * \sa CParticle::setLife
 * \sa CParticle::isDead
 ******************************/

unsigned int CParticle::getLife() const
{
    return m_life;
}


/**
 * Accesseur pour dead.
 *
 * \return Booléen indiquant si la particule est morte.
 *
 * \sa CParticle::getLife
 ******************************/

bool CParticle::isDead() const
{
    return (m_life == 0);
}


/**
 * Modifie la masse de la particule.
 *
 * \param mass Masse de la particule.
 *
 * \sa CParticle::getMass
 ******************************/

void CParticle::setMass(float mass)
{
    if (mass < 0.0f) m_mass = 0.0f;
    else m_mass = mass;
}


/**
 * Mutateur pour position.
 *
 * \param position Nouvelle position de la particule.
 *
 * \sa CParticle::getPosition
 ******************************/

void CParticle::setPosition(const TVector3F& position)
{
    m_position = position;
}


/**
 * Mutateur pour vitesse.
 *
 * \param vitesse Nouvelle vitesse de la particule.
 *
 * \sa CParticle::getVitesse
 ******************************/

void CParticle::setVitesse(const TVector3F& vitesse)
{
    m_vitesse = vitesse;
}


/**
 * Mutateur pour couleur.
 *
 * \param color Nouvelle couleur de la particule.
 *
 * \sa CParticle::getColor
 ******************************/

void CParticle::setColor(const CColor& color)
{
    m_color = color;
}


/**
 * Modifie la durée de vie de la particule.
 *
 * \param life Durée de vie de la particule.
 *
 * \sa CParticle::getLife
 ******************************/

void CParticle::setLife(unsigned int life)
{
    m_life = life;
}


/**
 * Met à jour les données de la particule.
 *
 * \todo Pouvoir choisir la manière de déplacer les particules.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CParticle::update(unsigned int frameTime)
{
    if (m_life < frameTime)
    {
        m_life = 0;
    }
    else
    {
        m_life -= frameTime;
    }

    // Si la particule est morte, on ne fait rien
    if (m_life == 0)
    {
        return;
    }
/*
    // On met à jour la position et la vitesse de la particule
    TVector3F gravity = CPhysicEngine::Instance().getGravity() * 0.001f * frameTime;

    m_position += PARTICLES_FROTTEMENT * 0.001f * static_cast<float>(frameTime) * (m_vitesse + PARTICLES_FROTTEMENT * 0.5f * gravity);
    m_vitesse += gravity;
*/
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CConstraint.cpp
 * \date 05/07/2010 Création de la classe CConstraint.
 */


#ifndef T_USING_PHYSIC_BULLET

/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "CConstraint.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CConstraint::CConstraint () :
m_model1 (nullptr),
m_model2 (nullptr)
{ }


/**
 * Donne le pointeur sur le premier modèle de la liaison.
 *
 * \return Pointeur sur le premier modèle.
 ******************************/

IModel * CConstraint::getModel1() const
{
    return m_model1;
}


/**
 * Donne le pointeur sur le second modèle de la liaison.
 *
 * \return Pointeur sur le second modèle.
 ******************************/

IModel * CConstraint::getModel2() const
{
    return m_model2;
}

} // Namespace Ted

#endif

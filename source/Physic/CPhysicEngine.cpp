/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CPhysicEngine.cpp
 * \date       2008 Création de la classe CPhysicEngine.
 * \date 15/07/2010 Modification de la méthode getRayImpact.
 * \date 17/07/2010 Création de la méthode RemoveModel.
 * \date 12/04/2011 Ajout d'un début de code pour utiliser le moteur physique Bullet.
 * \date 12/12/2011 Ajout d'une liste de volumes statiques convexes.
 *                  Ajout d'un débugger pour Bullet.
 */

/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>

#include "Physic/CPhysicEngine.hpp"
#include "Physic/CStaticVolume.hpp"
#include "Physic/CBSPTree.hpp"
#include "Physic/Impact.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"
//#include "Physic/CBulletDebugDraw.hpp"
//#include "Game/IModel.hpp"
//#include "Game/IModelV2.hpp"
//#include "Game/Entities/IBaseAnimating.hpp"
//#include "Game/Entities/IBaseTrigger.hpp"
//#include "Graphic/CBuffer.hpp"


namespace Ted
{

namespace Game
{
    CPhysicEngine * physicEngine = nullptr;
}


/**
 * Constructeur par défaut.
 ******************************/

CPhysicEngine::CPhysicEngine() :
#ifdef T_USING_PHYSIC_BULLET
m_world                   (nullptr),
m_broadphase              (nullptr),
m_dispatcher              (nullptr),
m_collision_configuration (nullptr),
m_solver                  (nullptr),

#ifdef T_DEBUG
//m_buffer                  (nullptr),
#endif

#endif
m_bsp                     (nullptr)

{
    if (Game::physicEngine)
        exit(-1);

    CApplication::getApp()->log("Initialisation du moteur physique");

#ifdef T_USING_PHYSIC_BULLET
    m_collision_configuration = new btDefaultCollisionConfiguration();
    T_ASSERT(m_collision_configuration != nullptr);
    m_dispatcher = new btCollisionDispatcher(m_collision_configuration);
    T_ASSERT(m_dispatcher != nullptr);
    m_broadphase = new btDbvtBroadphase();
    T_ASSERT(m_broadphase != nullptr);

#undef new // Les classes btSequentialImpulseConstraintSolver et btDiscreteDynamicsWorld redéfinissent l'opérateur new

    m_solver = new btSequentialImpulseConstraintSolver;
    T_ASSERT(m_solver != nullptr);
    m_world = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collision_configuration);
    T_ASSERT(m_world != nullptr);

    //CBulletDebugDraw * debug_draw = new CBulletDebugDraw();
    //m_buffer = debug_draw->getBuffer();
    //m_world->setDebugDrawer(debug_draw);

#endif

    setGravity(TVector3F(0.0f, 0.0f, -512.0f));

    Game::physicEngine = this;
}


/**
 * Destructeur.
 ******************************/

CPhysicEngine::~CPhysicEngine()
{
    Game::physicEngine = nullptr;

    CApplication::getApp()->log("Fermeture du moteur physique");
    delete m_bsp;

#ifdef T_USING_PHYSIC_BULLET
    //delete m_world->getDebugDrawer();

    delete m_world;
    delete m_solver;
    delete m_broadphase;
    delete m_dispatcher;
    delete m_collision_configuration;
#endif
}


/**
 * Donne la valeur de la gravité.
 *
 * \return Accélération de la pesanteur en unités par seconde au carré.
 *
 * \sa CPhysicEngine::setGravity
 ******************************/

TVector3F CPhysicEngine::getGravity() const
{
    return m_gravity;
}


/**
 * Donne un pointeur sur l'arbre BSP principal.
 *
 * \return Pointeur sur l'arbre BSP principal.
 ******************************/

CBSPTree * CPhysicEngine::getBSPTree() const
{
    return m_bsp;
}


/**
 * Donne le nombre de modèles gérer par le moteur physique.
 *
 * \return Nombre de modèles.
 ******************************/

unsigned int CPhysicEngine::getNumBodys() const
{
    return m_bodys.size();
}


/*/*
 * Donne le nombre de triggers gérer par le moteur physique.
 *
 * \return Nombre de triggers.
 ******************************/
/*
unsigned int CPhysicEngine::getNumTriggers() const
{
    return m_triggers.size();
}
*/

/*/*
 * Donne le buffer graphique utilisé pour l'affichage des données physiques.
 *
 * \return Pointeur sur le buffer graphique.
 ******************************/
/*
CBuffer * CPhysicEngine::getDebugBuffer() const
{
    return m_buffer;
}
*/

/**
 * Modifie la gravité.
 *
 * \param gravity Accélération de la pesanteur en unités par seconde au carré.
 *
 * \sa CPhysicEngine::getGravity
 ******************************/

void CPhysicEngine::setGravity(const TVector3F gravity)
{
    m_gravity = gravity;

#ifdef T_USING_PHYSIC_BULLET
    m_world->setGravity(btVector3(m_gravity.X, m_gravity.Y, m_gravity.Z));
#endif
}


/**
 * Modifie l'arbre BSP principal.
 *
 * \param bsp Pointeur sur l'arbre BSP principal.
 ******************************/

void CPhysicEngine::setBSPTree(CBSPTree * bsp)
{
    m_bsp = bsp;
}


/**
 * Ajoute un modèle à la liste.
 * Si le modèle est déjà dans la liste ou que le pointeur est invalide, rien
 * n'est fait.
 *
 * \param body Pointeur sur le modèle à ajouter.
 ******************************/

void CPhysicEngine::addRigidBody(btRigidBody * body)
{
    if (body == nullptr)
        return;

    if (std::find(m_bodys.begin(), m_bodys.end(), body) == m_bodys.end())
    {
        m_bodys.push_back(body);

#ifdef T_USING_PHYSIC_BULLET
        T_ASSERT(m_world);
        body->setActivationState(ACTIVE_TAG);
        m_world->addRigidBody(body);
#endif
    }
}


#ifndef T_USE_MODELS_V2

/**
 * Ajoute un modèle à la liste.
 * Si le modèle est déjà dans la liste ou que le pointeur est invalide, rien n'est fait.
 *
 * \param model Pointeur sur le modèle à ajouter.
 ******************************/

void CPhysicEngine::addModel(IModelV2 * model)
{
    if (model == nullptr)
        return;

    if (std::find(m_modelsV2.begin(), m_modelsV2.end(), model) == m_modelsV2.end())
    {
        m_modelsV2.push_back(model);

        btRigidBody * body = model->getRigidBody();

        if (body)
        {
            body->setActivationState(ACTIVE_TAG);
            T_ASSERT(m_world);
            m_world->addRigidBody(body);
        }
    }
}
#endif

/**
 * Enlève un modèle de la liste.
 *
 * \param body Pointeur sur le modèle à enlever.
 ******************************/

void CPhysicEngine::removeRigidBody(btRigidBody * body)
{
    if (body == nullptr)
        return;

    // On parcourt la liste des modèles
    for (std::vector<btRigidBody *>::iterator it = m_bodys.begin(); it != m_bodys.end(); ++it)
    {
        if (*it == body)
        {
#ifdef T_USING_PHYSIC_BULLET
            T_ASSERT(m_world != nullptr);
            m_world->removeRigidBody(body);
#endif

            m_bodys.erase(it);
            return;
        }
    }
}


#ifndef T_USE_MODELS_V2

/**
 * Enlève un modèle de la liste.
 *
 * \param model Pointeur sur le modèle à enlever.
 ******************************/

void CPhysicEngine::removeModel(IModelV2 * model)
{
    if (model == nullptr)
        return;

    // On parcourt la liste des modèles
    for (std::vector<IModelV2 *>::iterator it = m_modelsV2.begin() ; it != m_modelsV2.end() ; ++it)
    {
        if (*it == model)
        {
            T_ASSERT(m_world != nullptr);
            m_world->removeRigidBody(model->getRigidBody());

            m_modelsV2.erase(it);
            return;
        }
    }
}

#endif

/**
 * Ajoute un volume statique convexe à la liste.
 * Si le volume est déjà dans la liste ou que le pointeur est invalide, rien
 * n'est fait.
 *
 * \param volume Pointeur sur le volume à ajouter.
 ******************************/

void CPhysicEngine::addStaticVolume(CStaticVolume * volume)
{
    if (volume == nullptr)
        return;

    if (std::find(m_volumes.begin(), m_volumes.end(), volume) == m_volumes.end())
    {
        m_volumes.push_back(volume);

#ifdef T_USING_PHYSIC_BULLET
        T_ASSERT(m_world);
        m_world->addRigidBody(volume->getRigidBody());
#endif
    }
}


/**
 * Enlève un volume statique convexe de la liste.
 *
 * \param volume Pointeur sur le volume à enlever.
 ******************************/

void CPhysicEngine::removeStaticVolume(CStaticVolume * volume)
{
    if (volume == nullptr)
        return;

    // On parcourt la liste des modèles
    for (std::vector<CStaticVolume *>::iterator it = m_volumes.begin() ; it != m_volumes.end() ; ++it)
    {
        if (*it == volume)
        {
#ifdef T_USING_PHYSIC_BULLET
            T_ASSERT(m_world != nullptr);
            m_world->removeRigidBody(volume->getRigidBody());
#endif

            m_volumes.erase(it);
            return;
        }
    }
}


/*/*
 * Ajoute un trigger dans la liste.
 * Si le trigger est déjà dans la liste ou que le pointeur est invalide, rien
 * n'est fait.
 * Les triggers ne sont pas censés être dans la liste des modèles.
 *
 * \param trigger Pointeur sur le trigger à ajouter.
 ******************************/
/*
void CPhysicEngine::addTrigger(IBaseTrigger * trigger)
{
    if (trigger != nullptr && std::find(m_triggers.begin(), m_triggers.end(), trigger) == m_triggers.end())
    {
        m_triggers.push_back(trigger);
    }
}
*/

/*/*
 * Enlève un trigger de la liste.
 *
 * \param trigger Pointeur sur le trigger à enlever.
 ******************************/
/*
void CPhysicEngine::removeTrigger(IBaseTrigger * trigger)
{
    if (trigger == nullptr)
        return;

    for (std::vector<IBaseTrigger *>::iterator it = m_triggers.begin() ; it != m_triggers.end() ; ++it)
    {
        if (*it == trigger)
        {
            m_triggers.erase(it);
            return;
        }
    }
}
*/

/**
 * Donne le type de contenu pour un point.
 *
 * \param position Position du point.
 * \return Type de contenu.
 ******************************/

TContentType CPhysicEngine::getContentType(const TVector3F& position) const
{
    TContentType type = Empty;

#ifdef T_USE_MODELS_V2
    T_UNUSED_UNIMPLEMENTED(position);
#else

    // On teste chaque modèle
    for (std::vector<IModel *>::const_iterator it = m_models.begin() ; it != m_models.end() ; ++it)
    {
        TContentType model_type = (*it)->getContentType(position);

        if (model_type == Solid)
            return Solid;

        if (model_type == Water)
        {
            switch (type)
            {
                default: break;
                case Empty:   type = Water;           break;
                case Trigger: type = WaterAndTrigger; break;
            }
        }
        else if (model_type == Trigger)
        {
            switch (type)
            {
                default: break;
                case Empty: type = Trigger;         break;
                case Water: type = WaterAndTrigger; break;
            }
        }
        else if (model_type == WaterAndTrigger && type != WaterAndTrigger)
        {
            type = WaterAndTrigger;
        }
    }

    // On teste chaque trigger
    if (type != Trigger && type != WaterAndTrigger)
    {
        for (std::vector<IBaseTrigger *>::const_iterator it = m_triggers.begin() ; it != m_triggers.end() ; ++it)
        {
            TContentType trigger_type = (*it)->getModel()->getContentType(position);

            // Si le contenu est solide, on considère qu'on est à l'intérieur du trigger (comportement du moteur GoldSource)
            if (trigger_type == Solid || trigger_type == Trigger || trigger_type == WaterAndTrigger)
            {
                type = (type == Water ? WaterAndTrigger : Trigger);
                return type;
            }
        }
    }

#endif

    return type;
}


/**
 * Indique si la position d'un point est correcte.
 *
 * \param position Position du point.
 * \return Booléen.
 ******************************/

bool CPhysicEngine::isCorrectPosition(const TVector3F& position) const
{
    return (getContentType(position) != Solid);
}


/**
 * Indique si un déplacement est correct.
 *
 * \param from Point de départ.
 * \param to   Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CPhysicEngine::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{

#ifdef T_USE_MODELS_V2
    T_UNUSED_UNIMPLEMENTED(from);
    T_UNUSED_UNIMPLEMENTED(to);
#else

    // On test les collisions avec chaque modèle
    for (std::vector<IModel *>::const_iterator it = m_models.begin() ; it != m_models.end() ; ++it)
    {
        if (!(*it)->isCorrectMovement(from, to))
        {
            return false;
        }
    }

#endif

    return true;
}


/**
 * Indique si un point est sous l'eau.
 *
 * \param position Position du point.
 * \return Booléen.
 ******************************/

bool CPhysicEngine::isUnderWater(const TVector3F& position) const
{
    bool underwater = false;

#ifdef T_USE_MODELS_V2
    T_UNUSED_UNIMPLEMENTED(position);
#else

    // On test les collisions avec chaque modèle
    for (std::vector<IModel *>::const_iterator it = m_models.begin(); it != m_models.end(); ++it)
    {
        switch ((*it)->getContentType(position))
        {
            default: break;

            case Solid :
                return false;

            case Water:
            case WaterAndTrigger:
                underwater = true;
                break;
        }
    }

#endif

    return underwater;
}


/**
 * Cherche la position correcte la plus proche.
 *
 * \todo Implémentation.
 *
 * \param position Position du point.
 * \return Position correcte.
 ******************************/

TVector3F CPhysicEngine::getCorrectPosition(const TVector3F& position)
{
    T_UNUSED_UNIMPLEMENTED(position);

    //...

    return Origin3F;
}


/**
 * Cherche le point d'arrivée correspondant à un déplacement correct.
 *
 * \todo Implémentation.
 *
 * \param from Point de départ.
 * \param to   Point d'arrivée.
 * \return Position d'arrivée correcte.
 ******************************/

TVector3F CPhysicEngine::getCorrectDeplacement(const TVector3F& from, const TVector3F& to)
{
    if (from == to || !isCorrectPosition(from))
    {
        return from;
    }

    //...

    return to;
}


/**
 * Cherche l'intersection entre un rayon et l'environnement.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CPhysicEngine::getRayImpact(const CRay& ray) const
{
    CRayImpact impact;

    // Collision avec l'arbre BSP
    if (m_bsp)
    {
        impact = m_bsp->getRayImpact(ray);
    }

#ifndef T_USE_MODELS_V2

    // Collision avec les modèles
    for (std::vector<IModel *>::const_iterator it = m_models.begin() ; it != m_models.end() ; ++it)
    {
        CRayImpact tmp_impact = (*it)->getRayImpact(ray);

        if (tmp_impact < impact)
        {
            impact = tmp_impact;
        }
    }

#endif

    return impact;
}


/**
 * Cherche un déplacement correct pour un volume.
 *
 * \param volume   Volume à déplacer.
 * \param movement Vecteur de déplacement.
 * \return Booléen indiquant s'il y a eu collision.
 ******************************/

bool CPhysicEngine::slide(ICollisionVolume& volume, const TVector3F& movement) const
{
    return (m_bsp && m_bsp->Slide(volume, movement));
}


/*/*
 * Cherche dans quels triggers une entité se trouve, et avertit ces triggers.
 *
 * \todo Vérifier si l'entité est complètement à l'intérieur du trigger.
 *
 * \param entity Pointeur sur l'entité à rechercher.
 ******************************/
/*
void CPhysicEngine::checkTriggers(IBaseAnimating * entity) const
{
    if (entity == nullptr)
        return;

    IModel * entity_model = entity->getModel();

    if (entity_model == nullptr)
        return;

    CBoundingBox entity_box = entity_model->getBoundingBox();

    // On parcourt la liste des triggers
    for (std::vector<IBaseTrigger *>::const_iterator it = m_triggers.begin(); it != m_triggers.end(); ++it)
    {
        // Si le trigger est actif
        if ((*it)->isEnable())
        {
            IModel * m = (*it)->getModel();

            if (m == nullptr)
            {
                CApplication::getApp()->log(CString::fromUTF8("Le trigger %1 n'a pas de modèle associé (model %2)").arg(CString::fromPointer(*it)).arg(CString::fromPointer(m)), ILogger::Error);
                continue;
            }

            CBoundingBox b = m->getBoundingBox();

            if (entity_box.isIntersecting(b))
            {
                (*it)->trigger(entity);
            }
        }
    }
}
*/

/**
 * Définit la durée de la dernière frame.
 *
 * \todo Pouvoir utiliser le mode Debug de Bullet.
 *
 * \param frameTime Durée de la dernière frame en millisecondes.
 ******************************/

void CPhysicEngine::setFrameTime(unsigned int frameTime)
{

#ifdef T_USING_PHYSIC_BULLET
    m_world->stepSimulation(static_cast<float>(frameTime) * 0.001f);

    //m_buffer->clear();
    //m_world->debugDrawWorld();

#else
    T_UNUSED(frameTime);
#endif

}

} // Namespace Ted

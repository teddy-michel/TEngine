/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/Impact.cpp
 * \date 11/05/2009 Création de la classe Impact.
 */

/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/Impact.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

Impact::Impact() :
m_point_from  (Origin3F),
m_point_to    (Origin3F),
m_normale     (Origin3F),
m_frac_impact (1.0f),
m_frac_reelle (1.0f)
{ }


/**
 * Donne la position du début du segment.
 *
 * \return Début du segment.
 *
 * \sa Impact::getPointTo
 ******************************/

TVector3F Impact::getPointFrom() const
{
    return m_point_from;
}


/**
 * Donne la position de la fin du segment.
 *
 * \return Fin du segment.
 *
 * \sa Impact::getPointFrom
 ******************************/

TVector3F Impact::getPointTo() const
{
    return m_point_to;
}


/**
 * Donne la normale de l'impact.
 *
 * \return Normale de l'impact.
 *
 * \sa Impact::setNormale
 ******************************/

TVector3F Impact::getNormale() const
{
    return m_normale;
}


/**
 * Accesseur pour frac_impact.
 *
 * \return Fraction de l'impact.
 ******************************/

float Impact::getFracImpact() const
{
    return m_frac_impact;
}


/**
 * Accesseur pour frac_reelle.
 *
 * \return Fraction réelle.
 ******************************/

float Impact::getFracReelle() const
{
    return m_frac_reelle;
}


/**
 * Mutateur pour point_debut.
 *
 * \param from Point initial.
 *
 * \sa Impact::getPointFrom
 * \sa Impact::setPointTo
 ******************************/

void Impact::setPointFrom(const TVector3F& from)
{
    m_point_from = from;
}


/**
 * Mutateur pour point_fin.
 *
 * \param to Point finale.
 *
 * \sa Impact::getPointTo
 * \sa Impact::setPointFrom
 ******************************/

void Impact::setPointTo(const TVector3F& to)
{
    m_point_to = to;
}


/**
 * Mutateur pour normale.
 *
 * \param normale Vecteur normal au plan de l'impact.
 *
 * \sa Impact::getNormale
 ******************************/

void Impact::setNormale(const TVector3F& normale)
{
    m_normale = normale;
}


/**
 * Mutateur pour frac_impact.
 *
 * \param frac_impact Fraction de l'impact.
 ******************************/

void Impact::setFracImpact(float frac_impact)
{
    m_frac_impact = frac_impact;
}


/**
 * Mutateur pour frac_reelle.
 *
 * \param frac_reelle Fraction réelle.
 ******************************/

void Impact::setFracReelle(float frac_reelle)
{
    m_frac_reelle = frac_reelle;
}


/**
 * Réinitialise les paramètres de l'impact.
 *
 * \param from Position initial.
 * \param to   Position finale.
 ******************************/

void Impact::Reset(const TVector3F& from, const TVector3F& to)
{
    // Stocke les deux positions
    m_point_from = from;
    m_point_to = to;

    clearImpact();
}


/**
 * Réinitialise les paramètres de l'impact.
 ******************************/

void Impact::clearImpact()
{
    m_frac_impact = 1.0f;
    m_frac_reelle = 1.0f;
}


/**
 * Indique s'il y a impact.
 *
 * \return Booléen.
 ******************************/

bool Impact::isImpact() const
{
    return (m_frac_reelle < 1.0f);
}


/**
 * Définit un impact.
 *
 * \param normaleImpact   Normale de l'impact
 * \param fractionImpact Fraction de l'impact
 * \param fractionReal Fraction réelle de l'impact
 ******************************/

void Impact::setImpact(const TVector3F& normaleImpact, float fractionImpact, float fractionReal)
{
    m_normale = normaleImpact;
    m_frac_impact = fractionImpact;
    m_frac_reelle = fractionReal;
}


/**
 * Définit un impact
 *
 * \param normaleImpact   Normale de l'impact
 * \param fractionImpact Fraction de l'impact
 * \param fractionReal Fraction réelle de l'impact
 ******************************/

void Impact::setNearerImpact(const TVector3F& normaleImpact, float fractionImpact, float fractionReal)
{
    if (fractionReal < m_frac_reelle)
    {
        m_normale = normaleImpact;
        m_frac_impact = fractionImpact;
        m_frac_reelle = fractionReal;
    }
}

} // Namespace Ted

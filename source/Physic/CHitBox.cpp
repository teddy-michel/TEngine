/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CHitBox.cpp
 * \date 12/02/2010 Création de la classe CHitBox.
 * \date 12/07/2010 L'orientation de la boite est représentée par un quaternion.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CHitBox.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CHitBox::CHitBox() :
ICollisionVolume (),
m_position       (Origin3F),
m_angles         (CQuaternion(0.0f, 0.0f, 0.0f, 1.0f)),
m_width          (0.0f),
m_length         (0.0f),
m_height         (0.0f)
{ }


/**
 * Destructeur.
 ******************************/

CHitBox::~CHitBox()
{ }


/**
 * Accesseur pour position.
 *
 * \return Centre de la boite.
 * \sa CHitBox::setPosition
 ******************************/

TVector3F CHitBox::getPosition() const
{
    return m_position;
}


/**
 * Donne l'orientation de la boite.
 *
 * \return Orientation de la boite.
 * \sa CHitBox::setAngles
 ******************************/

CQuaternion CHitBox::getAngles() const
{
    return m_angles;
}


/**
 * Accesseur pour width.
 *
 * \return Largeur de la boite.
 * \sa CHitBox::setWidth
 ******************************/

float CHitBox::getWidth() const
{
    return m_width;
}


/**
 * Accesseur pour length.
 *
 * \return Longueur de la boite.
 * \sa CHitBox::setLength
 ******************************/

float CHitBox::getLength() const
{
    return m_length;
}


/**
 * Accesseur pour height.
 *
 * \return Hauteur de la boite.
 * \sa CHitBox::setHeight
 ******************************/

float CHitBox::getHeight() const
{
    return m_height;
}


/**
 * Modifie la position de la boite
 *
 * \param position Centre de la boite.
 * \sa CHitBox::getPosition
 ******************************/

void CHitBox::setPosition(const TVector3F& position)
{
    m_position = position;
}


/**
 * Translate la boite
 *
 * \param v Vecteur de translation.
 ******************************/

void CHitBox::translate(const TVector3F& v)
{
    m_position += v;
}


/**
 * Modifie l'orientation de la boite.
 *
 * \param angles Orientation de la boite.
 * \sa CHitBox::getAngles
 ******************************/

void CHitBox::setAngles(const CQuaternion& angles)
{
    m_angles = angles;
}


/**
 * Mutteur pour width.
 *
 * \param width Largeur de la boite.
 * \sa CHitBox::getWidth
 ******************************/

void CHitBox::setWidth(float width)
{
    if (width > 0.0f)
        m_width = width;
}


/**
 * Mutateur pour length.
 *
 * \param length Longueur de la boite.
 * \sa CHitBox::getLength
 ******************************/

void CHitBox::setLength(float length)
{
    if (length > 0.0f)
        m_length = length;
}


/**
 * Mutateur pour height.
 *
 * \param height Hauteur de la boite.
 * \sa CHitBox::getHeight
 ******************************/

void CHitBox::setHeight(float height)
{
    if (height > 0.0f)
        m_height = height;
}


/**
 * Modifie les dimensions de la boite.
 *
 * \param width Largeur de la boite.
 * \param length Longueur de la boite.
 * \param height Hauteur de la boite.
 ******************************/

void CHitBox::setSize(float width, float length, float height)
{
    setWidth(width);
    setLength(length);
    setHeight(height);
}


/**
 * Donne l'offset de la boite par rapport à un plan.
 *
 * \todo Implémentation.
 *
 * \return Offset de la boite.
 ******************************/

float CHitBox::getOffset(const CPlane& plane) const
{
    T_UNUSED_UNIMPLEMENTED(plane);

    //...

    return 1.0f;
}


/**
 * Donne le type de contenu pour un point.
 *
 * \todo Implémentation.
 *
 * \param position Position du point.
 * \return Type de contenu (solide ou vide).
 ******************************/

TContentType CHitBox::getContentType(const TVector3F& position) const
{
    T_UNUSED_UNIMPLEMENTED(position);

    //...

    return Empty;
}


/**
 * Indique si un déplacement est correct.
 *
 * \todo Implémentation.
 *
 * \param from Point de départ.
 * \param to Point d'arrivée.
 * \return Booléen.
 ******************************/

bool CHitBox::isCorrectMovement(const TVector3F& from, const TVector3F& to) const
{
    T_UNUSED_UNIMPLEMENTED(from);
    T_UNUSED_UNIMPLEMENTED(to);

    //...

    return true;
}


/**
 * Indique s'il y a intersection entre la sphère et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Booléen.
 ******************************/

bool CHitBox::hasImpact(const CRay& ray) const
{
    T_UNUSED_UNIMPLEMENTED(ray);

    //...

    return false;
}


/**
 * Cherche l'intersection entre la boite et un rayon.
 *
 * \todo Implémentation.
 *
 * \param ray Rayon à utiliser.
 * \return Paramètres de l'impact.
 ******************************/

CRayImpact CHitBox::getRayImpact(const CRay& ray) const
{
    T_UNUSED_UNIMPLEMENTED(ray);

    CRayImpact impact;

    //...

    return impact;
}


/**
 * Indique si la boite entre en collision avec une boite englobante.
 *
 * \todo Implémentation.
 *
 * \param box Boite englobante à tester.
 * \return Booléen.
 ******************************/

bool CHitBox::isIntersecting(const CBoundingBox& box) const
{
    T_UNUSED_UNIMPLEMENTED(box);

    //...

    return false;
}


/**
 * Indique si la boite entre en collision avec une sphère englobante.
 *
 * \todo Implémentation.
 *
 * \param sphere Sphère englobante à tester.
 * \return Booléen.
 ******************************/

bool CHitBox::isIntersecting(const CBoundingSphere& sphere) const
{
    T_UNUSED_UNIMPLEMENTED(sphere);

    //...

    return false;
}


/**
 * Indique si la boite entre en collision avec une boite orientée.
 *
 * \todo Implémentation.
 *
 * \param box Boite orientée à tester.
 * \return Booléen.
 ******************************/

bool CHitBox::isIntersecting(const CHitBox& box) const
{
    T_UNUSED_UNIMPLEMENTED(box);

    //...

    return false;
}


/**
 * Indique si la boite entre en collision avec un triangle.
 *
 * \todo Implémentation.
 *
 * \param triangle Triangle à tester.
 * \return Booléen.
 ******************************/

bool CHitBox::isIntersecting(const CTriangle& triangle) const
{
    T_UNUSED_UNIMPLEMENTED(triangle);

    //...

    return false;
}

} // Namespace Ted

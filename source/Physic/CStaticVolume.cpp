/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Physic/CStaticVolume.cpp
 * \date 12/12/2011 Création de la classe CStaticVolume.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Physic/CStaticVolume.hpp"
#include "Physic/CPhysicEngine.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur. Le modèle est ajouté au moteur physique.
 *
 * \param vertices Liste des sommets du volume.
 ******************************/

CStaticVolume::CStaticVolume(const btAlignedObjectArray<btVector3>& vertices) :
#ifdef T_USING_PHYSIC_BULLET
m_body  (nullptr),
m_shape (nullptr)
#endif
{

#ifdef T_USING_PHYSIC_BULLET

    // Paramètres du corps
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(btVector3(0.0f, 0.0f, 0.0f));

    m_shape = new btConvexHullShape(&(vertices[0].getX()), vertices.size());
    btDefaultMotionState * motion_state = new btDefaultMotionState(transform); // éventuellement nullptr

    // Création du corps
    btRigidBody::btRigidBodyConstructionInfo construction_info(0.0f, motion_state, m_shape);
    m_body = new btRigidBody(construction_info);

    Game::physicEngine->addStaticVolume(this);

#endif

}


/**
 * Destructeur. Le modèle est enlevé du moteur physique.
 ******************************/

CStaticVolume::~CStaticVolume()
{
#ifdef T_USING_PHYSIC_BULLET
    Game::physicEngine->removeStaticVolume(this);

    delete m_shape;
    delete m_body->getMotionState();
    delete m_body;
#endif
}

#ifdef T_USING_PHYSIC_BULLET

/**
 * Donne le pointeur sur le corps rigide du modèle (pour Bullet).
 *
 * \return Pointeur sur le corps rigide.
 ******************************/

btRigidBody * CStaticVolume::getRigidBody() const
{
    return m_body;
}


/**
 * Donne le pointeur sur le volume du modèle (pour Bullet).
 *
 * \return Pointeur sur le volume.
 ******************************/

btCollisionShape * CStaticVolume::getShape() const
{
    return m_shape;
}

#endif

} // Namespace Ted

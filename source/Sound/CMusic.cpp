/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CMusic.cpp
 * \date 25/04/2009 Création de la classe CMusic.
 * \date 11/07/2010 On peut modifier le nom du fichier.
 * \date 16/07/2010 Conversion en booléen.
 * \date 18/07/2010 Correction d'une erreur lorsque le nombre d'échantillons lus est trop faible.
 * \date 03/12/2010 CSound hérite de la classe ISound.
 * \date 04/12/2010 On peut charger une musique depuis un fichier ou depuis une vidéo.
 * \date 08/12/2010 La lecture du son depuis une vidéo est fonctionnelle.
 *                  La lecture en boucle est fonctionnelle.
 * \date 10/12/2010 Correction d'un bug dans la vitesse de lecture des sons d'une vidéo.
 *                  Correction d'un bug lorsque la lecture du son est interrompue par une frame trop longue.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Sound/CMusic.hpp"
#include "Sound/CSoundEngine.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CMusic::CMusic() :
ISound         (),
m_file         (nullptr),
m_fromVideo    (false),
#ifdef T_SYSTEM_WINDOWS
m_aviStream    (nullptr),
m_sampleCount  (0)
#endif
{
    m_buffer[0] = 0;
    m_buffer[1] = 0;
}


/**
 * Destructeur.
 ******************************/

CMusic::~CMusic()
{
    ALenum error = alGetError();

    // Fermeture du fichier
    if (m_file)
    {
        sf_close(m_file);
    }

    stop();

    // On purge la file de tampons de la source
    if (m_source != 0)
    {
        ALint nbr_queued;
        ALuint buffer;

        alGetSourcei(m_source, AL_BUFFERS_QUEUED /*AL_BUFFERS_PROCESSED*/, &nbr_queued);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
            nbr_queued = 0;
        }

        for (ALint i = 0; i < nbr_queued; ++i)
        {
            alSourceUnqueueBuffers(m_source, 1, &buffer);

            // Traitement des erreurs
            if ((error = alGetError()) != AL_NO_ERROR)
            {
                CSoundEngine::displayOpenALError(error, "alSourceUnqueueBuffers", __LINE__);
            }
        }
    }

    if (m_buffer[0] != 0 || m_buffer[1] != 0)
    {
        alDeleteBuffers(2, m_buffer);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alDeleteBuffers", __LINE__);
        }
    }

    //CSoundEngine::Instance().RemoveMusic(this);
}


/**
 * Donne les buffers de la musique.
 *
 * \return Pointeur sur les deux buffers utilisés.
 ******************************/

const ALuint * CMusic::getBuffers() const
{
    return m_buffer;
}


/**
 * Charge une musique.
 *
 * \param fileName Adresse du fichier contenant la musique.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CMusic::loadFromFile(const CString& fileName)
{
    m_loaded = false;
    m_fileName = fileName;
    m_fromVideo = false;
    ALenum error = alGetError();

    // Ouverture du fichier audio avec libsndfile
    SF_INFO file_infos;
    m_file = sf_open(m_fileName.toCharArray(), SFM_READ, &file_infos);

    // Erreur lors du chargement du fichier
    if (!m_file)
    {
        CApplication::getApp()->log(CString("CMusic::loadFromFile : impossible d'ouvrir le fichier %1").arg(m_fileName), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString("Chargement de la musique %1").arg(m_fileName));

    // Lecture du nombre d'échantillons et du taux d'échantillonnage
    m_nbrSamples = static_cast<ALsizei>(file_infos.channels * file_infos.frames);
    m_sampleRate = static_cast<ALsizei>(file_infos.samplerate);

    // Détermination du format en fonction du nombre de canaux
    switch (file_infos.channels)
    {
        case 1:
            m_format = AL_FORMAT_MONO16;
            break;

        case 2:
            m_format = AL_FORMAT_STEREO16;
            break;

        case 4:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
                return false;

            m_format = alGetEnumValue("AL_FORMAT_QUAD16");
            break;

        case 6:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
                return false;

            m_format = alGetEnumValue("AL_FORMAT_51CHN16");
            break;

        case 7:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
                return false;

            m_format = alGetEnumValue("AL_FORMAT_61CHN16");
            break;

        case 8:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
                return false;

            m_format = alGetEnumValue("AL_FORMAT_71CHN16");
            break;

        default:
            return false;
    }

    // Création des buffers OpenAL
    if (m_buffer[0] == 0 || m_buffer[1] == 0)
    {
        alGenBuffers(2, m_buffer);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alGenBuffers", __LINE__);
            return false;
        }

        // Les buffers sont invalides
        if (m_buffer[0] == 0 || m_buffer[1] == 0)
        {
            CApplication::getApp()->log("Les buffers audio sont invalides", ILogger::Error);
            return false;
        }
    }

    // Création d'une source
    if (m_source == 0)
    {
        alGenSources(1, &m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alGenSources", __LINE__);
            return false;
        }

        // La source est invalide
        if (m_source == 0)
        {
            CApplication::getApp()->log("La source audio est invalide", ILogger::Error);
            return false;
        }
    }

    // On remplit les deux buffers
    readData(m_buffer[0], 44100);
    readData(m_buffer[1], 44100);

    // Remplissage avec les échantillons lus
    alSourceQueueBuffers(m_source, 2, m_buffer);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourceQueueBuffers", __LINE__);
        return false;
    }

    // Paramètres de la source
    alSourcei(m_source, AL_LOOPING, false);

    // Traitement des erreurs
    if ((error = alGetError() ) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alGetSourcei", __LINE__);
    }

    alSourcef(m_source, AL_PITCH, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSourcef(m_source, AL_GAIN, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSource3f(m_source, AL_POSITION, 0.0f, 0.0f, 0.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSource3f", __LINE__);
    }

    m_loaded = true;
    return true;
}


/**
 * Charge une musique depuis une vidéo.
 *
 * \param video Pointeur sur la vidéo à utiliser.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CMusic::loadFromVideo(const CString& fileName)
{
    m_loaded = false;
    m_fileName = fileName;
    m_file = nullptr;
    m_fromVideo = true;

#ifdef T_SYSTEM_WINDOWS

    m_sampleCount = 0;
    ALenum error = alGetError();

    CApplication::getApp()->log(CString::fromUTF8("Chargement de la musique de la vidéo %1").arg(m_fileName));

    // Ouverture du flux audio
    if (AVIStreamOpenFromFile(&m_aviStream, m_fileName.toCharArray(), streamtypeAUDIO, 0, OF_READ, nullptr))
    {
        CApplication::getApp()->log("AVIStreamOpenFromFile : impossible de lire le flux audio", ILogger::Error);
        return false;
    }


    LONG buffer_size;
    AVIStreamRead(m_aviStream, AVIStreamStart(m_aviStream), (-1L), nullptr, 0, &buffer_size, nullptr);

    PBYTE tmp_format = new BYTE[buffer_size];
    AVIStreamReadFormat(m_aviStream, AVIStreamStart(m_aviStream), tmp_format, &buffer_size);
    LPWAVEFORMATEX wave_format = reinterpret_cast<LPWAVEFORMATEX>(tmp_format);

    // Lecture du nombre d'échantillons et du taux d'échantillonnage
    m_nbrSamples = AVIStreamLength(m_aviStream);
    m_sampleRate = wave_format->nSamplesPerSec;

    // Détermination du format en fonction du nombre de canaux
    switch (wave_format->nChannels)
    {
        case 1:
            m_format = AL_FORMAT_MONO16;
            break;

        case 2:
            m_format = AL_FORMAT_STEREO16;
            break;

        case 4:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            m_format = alGetEnumValue("AL_FORMAT_QUAD16");
            break;

        case 6:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            m_format = alGetEnumValue("AL_FORMAT_51CHN16");
            break;

        case 7:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            m_format = alGetEnumValue("AL_FORMAT_61CHN16");
            break;

        case 8:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            m_format = alGetEnumValue("AL_FORMAT_71CHN16");
            break;

        default:
            return false;
    }

    // Création des buffers OpenAL
    if (m_buffer[0] == 0 || m_buffer[1] == 0)
    {
        alGenBuffers(2, m_buffer);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alGenBuffers", __LINE__);
            return false;
        }

        // Les buffers sont invalides
        if (m_buffer[0] == 0 || m_buffer[1] == 0)
        {
            CApplication::getApp()->log("Les buffers audio sont invalides", ILogger::Error);
            return false;
        }
    }

    // Création d'une source
    if (m_source == 0)
    {
        alGenSources(1, &m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alGenSources", __LINE__);
            return false;
        }

        // La source est invalide
        if (m_source == 0)
        {
            CApplication::getApp()->log("La source audio est invalide", ILogger::Error);
            return false;
        }
    }

    // On remplit les deux buffers
    readData(m_buffer[0], 44100);
    readData(m_buffer[1], 44100);

    // Remplissage avec les échantillons lus
    alSourceQueueBuffers(m_source, 2, m_buffer);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourceQueueBuffers", __LINE__);
        return false;
    }

    // Paramètres de la source
    alSourcei(m_source, AL_LOOPING, false);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alGetSourcei", __LINE__);
    }

    alSourcef(m_source, AL_PITCH, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSourcef(m_source, AL_GAIN, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError() ) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSource3f(m_source, AL_POSITION, 0.0f, 0.0f, 0.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSource3f", __LINE__);
    }

    m_loaded = true;

#endif

    return true;
}


/**
 * Met à jour les buffers.
 ******************************/

void CMusic::update()
{
    if (m_source == 0 || !m_play)
        return;

    ALenum error = alGetError();

    ALint status;
    alGetSourcei(m_source, AL_SOURCE_STATE, &status);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alGetSourcei", __LINE__);
        return;
    }

    //if (status == AL_PLAYING)
    {
        // On récupère le nombre de tampons qui ont été traités
        ALint nbr_processed;
        alGetSourcei(m_source, AL_BUFFERS_PROCESSED, &nbr_processed);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alGetSourcei", __LINE__);
            return;
        }

        // On les re-remplit et on les réinjecte dans la file
        for (ALint i = 0; i < nbr_processed; ++i)
        {
            // On sort le tampon de la file
            ALuint buffer;
            alSourceUnqueueBuffers(m_source, 1, &buffer);

            // Traitement des erreurs
            if ((error = alGetError()) != AL_NO_ERROR)
            {
                CSoundEngine::displayOpenALError(error, "alSourceUnqueueBuffers", __LINE__);
                return;
            }

            // On le remplit avec les données du fichier
            readData(buffer, 44100);

            // On le réinjecte dans la file
            alSourceQueueBuffers(m_source, 1, &buffer);

            // Traitement des erreurs
            if ((error = alGetError()) != AL_NO_ERROR)
            {
                CSoundEngine::displayOpenALError(error, "alSourceQueueBuffers", __LINE__);
                return;
            }
        }

        // Les deux buffers ont été lus, il faut relancer la lecture
        if (nbr_processed == 2)
        {
            alSourcePlay(m_source);

            // Traitement des erreurs
            if ((error = alGetError() ) != AL_NO_ERROR)
            {
                CSoundEngine::displayOpenALError(error, "alSourcePlay", __LINE__);
            }
        }
    }
}


/**
 * Lit un certain nombre d'échantillons et les place dans un buffer.
 *
 * \todo Le nombre d'échantillons lus dépend du format du son, adapter en conséquence.
 *
 * \param buffer Identifiant du buffer à remplir.
 * \param nbr_samples Nombre d'échantillons à lire.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CMusic::readData(ALuint buffer, ALsizei nbr_samples)
{
    ALenum error = alGetError();

    // Lecture des échantillons audio au format entier 16 bits signé
    std::vector<ALshort> samples(nbr_samples);

    // Lecture depuis un fichier
    if (m_file)
    {
        // On remplit le premier buffer
        if (sf_read_short(m_file, &samples[0], nbr_samples) == 0)
        {
            // Fin du fichier, on revient au début
            if (m_loop)
            {
                sf_seek(m_file, 0, SEEK_SET);
#ifdef T_SYSTEM_WINDOWS
                m_sampleCount = 0;
#endif
            }
            else
            {
                m_play = false;
            }

            return true;
        }

        // On le remplit avec les données du fichier
        alBufferData(buffer, m_format, &samples[0], nbr_samples * sizeof(ALushort), m_sampleRate);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alBufferData", __LINE__);
            return false;
        }

        return true;
    }

#ifdef T_SYSTEM_WINDOWS
    // Lecture depuis une vidéo
    else if (m_fromVideo && m_aviStream)
    {
        LONG samples_read;

        switch (AVIStreamRead(m_aviStream, AVIStreamStart(m_aviStream) + m_sampleCount, nbr_samples, &samples[0], nbr_samples * sizeof(ALshort), nullptr, &samples_read))
        {
            // Aucune erreur
            case 0: break;

            case AVIERR_BUFFERTOOSMALL:
                CApplication::getApp()->log("AVIStreamRead : The buffer size was smaller than a single sample of data.", ILogger::Error);
                return false;

            case AVIERR_MEMORY:
                CApplication::getApp()->log("AVIStreamRead : There was not enough memory to complete the read operation.", ILogger::Error);
                return false;

            case AVIERR_FILEREAD:
                CApplication::getApp()->log("AVIStreamRead : A disk error occurred while reading the file.", ILogger::Error);
                return false;

            default:
                CApplication::getApp()->log("AVIStreamRead : Unknown error", ILogger::Error);
                return false;
        }

        m_sampleCount += samples_read / 2; // On divise par la taille d'un échantillon

        // Le nombre d'échantillons lus est inférieur au nombre demandé : on revient au début
        //TODO: vérifier pour chaque format possible
        if ((m_format == AL_FORMAT_MONO16   && samples_read < nbr_samples) ||
            (m_format == AL_FORMAT_STEREO16 && samples_read * 2 < nbr_samples))
        {
            if (m_loop)
            {
                m_sampleCount = 0;
            }
            else
            {
                m_play = false;
            }
        }

        // On le remplit avec les données du fichier
        alBufferData(buffer, m_format, &samples[0], samples_read * sizeof(ALushort), m_sampleRate);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alBufferData", __LINE__);
            return false;
        }

        return true;
    }
#endif

    return false;
}

} // Namespace Ted

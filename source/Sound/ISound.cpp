/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/ISound.cpp
 * \date 03/12/2010 Création de la classe ISound.
 * \date 10/12/2010 Amélioration de la gestion de la lecture/pause d'un son.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>

#include "Sound/ISound.hpp"
#include "Sound/CSoundEngine.hpp"

// DEBUG
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

ISound::ISound() :
m_fileName   (),
m_loop       (false),
m_loaded     (false),
m_play       (false),
m_source     (0),
m_nbrSamples (0),
m_sampleRate (0)
{ }


/**
 * Destructeur.
 ******************************/

ISound::~ISound()
{
    ALenum error = alGetError();

    if (m_source != 0)
    {
        alSourcei(m_source, AL_BUFFER, 0);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
        }

        alDeleteSources(1, &m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alDeleteSources", __LINE__);
        }
    }
}


/**
 * Donne le nom du fichier contenant le son.
 *
 * \return Fichier contenant le son.
 ******************************/

CString ISound::getFileName() const
{
    return m_fileName;
}


/**
 * Accesseur pour source.
 *
 * \return Source.
 ******************************/

ALuint ISound::getSource() const
{
    return m_source;
}


/**
 * Accesseur pour nbr_samples.
 *
 * \return Nombre d'échantillons.
 ******************************/

ALsizei ISound::getNumSamples() const
{
    return m_nbrSamples;
}


/**
 * Accesseur pour sample_rate.
 *
 * \return Taux d'échantillonage.
 ******************************/

ALsizei ISound::getSampleRate() const
{
    return m_sampleRate;
}


/**
 * Indique si le son est en train d'être joué.
 *
 * \return True si le morceau est en cours de lecture, false sinon.
 ******************************/

bool ISound::isPlaying() const
{/*
    if (m_source != 0)
    {
        ALenum error = alGetError();

        ALint status;
        alGetSourcei(m_source, AL_SOURCE_STATE, &status);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::DisplayOpenALError(error, "alSourcei", __LINE__);
            return false;
        }

        return (status == AL_PLAYING);
    }
*/
    return m_play;
}


/**
 * Indique si la lecture du son est en pause.
 *
 * \todo Utiliser la position de la lecture.
 *
 * \return True si la lecture est en pause, false sinon.
 ******************************/

bool ISound::isPaused() const
{
    if (m_source != 0)
    {
        ALenum error = alGetError();

        ALint status;
        alGetSourcei(m_source, AL_SOURCE_STATE, &status);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
            return false;
        }

        return (status == AL_PAUSED);
    }

    return (!m_play);
}


/**
 * Indique si la lecture du son est stoppée.
 *
 * \todo Utiliser la position de la lecture.
 *
 * \return True si la lecture est arrêtée, false sinon.
 ******************************/

bool ISound::isStopping() const
{
    if (m_source != 0)
    {
        ALenum error = alGetError();

        ALint status;
        alGetSourcei(m_source, AL_SOURCE_STATE, &status);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
            return true;
        }

        return (status == AL_STOPPED || status == AL_INITIAL);
    }

    return (!m_play);
}


/**
 * Indique si le son est joué en boucle.
 *
 * \return Booléen.
 * \sa ISound::setLoop
 ******************************/

bool ISound::isLoop() const
{
/*
    ALenum error = alGetError();

    ALint loop;
    alGetSourcei(m_source, AL_LOOPING, &loop);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::DisplayOpenALError(error, "alSourcei", __LINE__);
        return false;
    }

    return (loop != 0);
*/
    return m_loop;
}


/**
 * Donne la valeur de pitch.
 *
 * \return Pitch.
 * \sa ISound::setPitch
 ******************************/

float ISound::getPitch() const
{
    ALenum error = alGetError();

    ALfloat pitch;
    alGetSourcef(m_source, AL_PITCH, &pitch);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        return 0.0f;
    }

    return pitch;
}


/**
 * Donne le volume de la source.
 *
 * \return Volume entre 0 et 100.
 * \sa ISound::setVolume
 ******************************/

float ISound::getVolume() const
{
    ALenum error = alGetError();

    ALfloat volume;
    alGetSourcef(m_source, AL_GAIN, &volume);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        return 0.0f;
    }

    return (volume * 100.f);
}


/**
 * Donne la position de la source.
 *
 * \return Position de la source.
 * \sa ISound::setPosition
 ******************************/

TVector3F ISound::getPosition() const
{
    ALenum error = alGetError();

    TVector3F position;
    alGetSource3f(m_source, AL_POSITION, &position.X, &position.Y, &position.Z);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSource3f", __LINE__);
        return Origin3F;
    }

    return position;
}


/**
 * Donne la distance minimale pour entendre la source.
 *
 * \return Distance minimale.
 * \sa ISound::setMinimaleDistance
 ******************************/

float ISound::getMinimaleDistance() const
{
    ALenum error = alGetError();

    ALfloat distance;
    alGetSourcef(m_source, AL_REFERENCE_DISTANCE, &distance);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        return 0.0f;
    }

    return distance;
}


/**
 * Donne la valeur de l'atténuation.
 *
 * \return Atténuation.
 * \sa ISound::setAttenuation
 ******************************/

float ISound::getAttenuation() const
{
    ALenum error = alGetError();

    ALfloat attenuation;
    alGetSourcef(m_source, AL_ROLLOFF_FACTOR, &attenuation);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        return 0.0f;
    }

    return attenuation;
}


/**
 * Indique la position de lecture (en secondes).
 *
 * \return Position de lecture.
 * \sa ISound::setPlayingOffset
 ******************************/

float ISound::getPlayingOffset() const
{
    ALenum error = alGetError();

    ALfloat seconds = 0.0f;
    alGetSourcef(m_source, AL_SEC_OFFSET, &seconds);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        return 0.0f;
    }

    return seconds;
}


/**
 * Modifie la valeur de loop.
 *
 * \param loop Indique si le son doit être joué en boucle.
 * \sa ISound::isLoop
 ******************************/

void ISound::setLoop(bool loop)
{
    m_loop = loop;
}


/**
 * Définit la valeur de pitch.
 *
 * \param pitch Pitch.
 * \sa ISound::getPitch
 ******************************/

void ISound::setPitch(float pitch)
{
    ALenum error = alGetError();

    alSourcef(m_source, AL_PITCH, pitch);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }
}


/**
 * Définit le volume de la source.
 *
 * \param volume Volume entre 0 et 100.
 * \sa ISound::getVolume
 ******************************/

void ISound::setVolume(float volume)
{
    if (m_source != 0)
    {
        ALenum error = alGetError();

        if (volume > 100.0f) volume = 100.0f;
        else if (volume < 0.0f) volume = 0.0f;

        alSourcef(m_source, AL_GAIN, volume * 0.01f);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
        }
    }
}


/**
 * Définit la position de la source.
 *
 * \param position Position de la source.
 * \sa ISound::getPosition
 ******************************/

void ISound::setPosition(const TVector3F& position)
{
    ALenum error = alGetError();

    alSource3f(m_source, AL_POSITION, position.X, position.Y, position.Z);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSource3f", __LINE__);
    }
}


/**
 * Définit la distance minimale pour entendre la source.
 *
 * \param distance Distance minimale.
 * \sa ISound::getMinimaleDistance
 ******************************/

void ISound::setMinimaleDistance(float distance)
{
    ALenum error = alGetError();

    alSourcef(m_source, AL_REFERENCE_DISTANCE, distance);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }
}


/**
 * Définit la valeur de l'atténuation.
 *
 * \param attenuation Atténuation.
 * \sa ISound::getAttenuation
 ******************************/

void ISound::setAttenuation(float attenuation)
{
    ALenum error = alGetError();

    alSourcef(m_source, AL_ROLLOFF_FACTOR, attenuation);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }
}


/**
 * Modifie la position de lecture.
 * \todo Utiliser aussi AL_SAMPLE_OFFSET.
 *
 * \param offset Position de lecture en secondes.
 * \sa CSound::getPlayingOffset
 ******************************/

void ISound::setPlayingOffset(float offset)
{
    ALenum error = alGetError();

    alSourcef(m_source, AL_SEC_OFFSET, offset);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }
}


/**
 * Joue le son.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool ISound::play()
{
    if (!m_play)
    {
        m_play = true;

        if (m_source == 0)
            return false;

        ALenum error = alGetError();

        alSourcePlay(m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcePlay", __LINE__);
            return false;
        }
    }

    return true;
}


/**
 * Met en pause la lecture du son.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool ISound::pause()
{
    if (m_play)
    {
        m_play = false;

        if (m_source == 0)
            return false;

        ALenum error = alGetError();

        alSourcePause(m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourcePlay", __LINE__);
            return false;
        }
    }

    return true;
}


/**
 * Arrête la lecture du son.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool ISound::stop()
{
    if (m_play)
    {
        m_play = false;

        if (m_source == 0)
            return false;

        ALenum error = alGetError();

        alSourceStop(m_source);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alSourceStop", __LINE__);
            return false;
        }
    }

    return true;
}

} // Namespace Ted

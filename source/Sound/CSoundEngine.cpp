/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CSoundEngine.cpp
 * \date       2008 Création de la classe CSoundEngine.
 * \date 11/07/2010 On peut ajouter ou enelver un son ou une musique.
 * \date 03/12/2010 PlaySound et PlayMusic retourne le pointeur sur la ressource.
 * \date 28/02/2011 Correction de plusieurs erreurs liées aux itérateurs.
 * \date 02/03/2011 Création de la méthode SaveCapture.
 * \date 20/03/2011 Inclusion de cstring.
 * \date 23/03/2011 Implémentation de la méthode Beep sous Linux.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <AL/al.h>
#include <cstring>

#include "os.h"

#if defined T_SYSTEM_WINDOWS
#  include <windows.h>
#else
#  include <fcntl.h>
#  include <signal.h>
#  include <sys/ioctl.h>
#  include <linux/kd.h>
#  include <unistd.h>
#endif

#include "Sound/CSoundEngine.hpp"
#include "Sound/CSound.hpp"
#include "Sound/CMusic.hpp"
#include "Sound/CCapture.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

#ifdef T_SYSTEM_LINUX

/**
 * This number represents the fixed frequency of the original PC XT's
 * timer chip (the 8254 AFAIR), which is approximately 1.193 MHz. This
 * number is divided with the desired frequency to obtain a counter value,
 * that is subsequently fed into the timer chip, tied to the PC speaker.
 * The chip decreases this counter at every tick (1.193 MHz) and when it
 * reaches zero, it toggles the state of the speaker (on/off, or in/out),
 * resets the counter to the original value, and starts over. The end
 * result of this is a tone at approximately the desired frequency.
 ******************************/

const unsigned int clock_tick_rate = 1193180;

int console_fd = -1;

void handle_signal(int signum);


/**
 * Traitement du signal SIGINT pour ne pas beeper en permanence.
 *
 * \param signum Numéro du signal.
 ******************************/

void handle_signal(int signum)
{
    if (signum == SIGINT)
    {
        if (console_fd >= 0)
        {
            /* Kill the sound, quit gracefully */
            ioctl(console_fd, KIOCSOUND, 0);
            close(console_fd);
            exit(signum);
        }
        else
        {
            /* Just quit gracefully */
            exit(signum);
        }
    }
}

#endif


namespace Game
{
    CSoundEngine * soundEngine = NULL;
}


/**
 * Constructeur par défaut.
 ******************************/

CSoundEngine::CSoundEngine() :
m_capture (nullptr),
m_device  (nullptr)
{
    if (Game::soundEngine)
        exit(-1);

    CApplication::getApp()->log("Initialisation du moteur de son");

#ifdef T_SYSTEM_LINUX
    signal(SIGINT, handle_signal);
#endif

    initOpenAL();

    Game::soundEngine = this;
}


/**
 * Destructeur.
 * Supprime tous les sons chargés en mémoire
 ******************************/

CSoundEngine::~CSoundEngine()
{
    Game::soundEngine = nullptr;

    CApplication::getApp()->log("Fermeture du moteur de son");

    // Suppression des sons et des musiques
    deleteSounds();
    deleteMusics();

    shutdownOpenAL();

    delete m_capture;
}


/**
 * Initialisation de OpenAL.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CSoundEngine::initOpenAL()
{
    static bool init = false;

    if (!init)
    {
        CApplication::getApp()->log("Initialisation de OpenAL");

        // Ouverture du device
        m_device = alcOpenDevice(nullptr);

        if (!m_device)
        {
            CApplication::getApp()->log(CString::fromUTF8("Impossible de créer le device OpenAL."), ILogger::Error);
            return false;
        }

        // Création du contexte
        ALCcontext * context = alcCreateContext(m_device, nullptr);

        if (!context)
        {
            CApplication::getApp()->log(CString::fromUTF8("Impossible de créer le contexte OpenAL."), ILogger::Error);
            return false;
        }

        // Activation du contexte
        if (!alcMakeContextCurrent(context))
        {
            CApplication::getApp()->log("Impossible d'activer le contexte OpenAL", ILogger::Error);
            return false;
        }

        // Création de la capture audio
        m_capture = new CCapture();
        T_ASSERT(m_capture != nullptr);

        if (!m_capture->initCapture(m_device))
        {
            delete m_capture;
            m_capture = nullptr;
        }

        init = true;
    }

    return true;
}


/**
 * Ajoute un son à la liste des sons.
 *
 * \param sound Pointeur sur le son à ajouter.
 ******************************/

void CSoundEngine::addSound(CSound * sound)
{
    if (sound == nullptr)
        return;

    if (std::find(m_sounds.begin(), m_sounds.end(), sound) == m_sounds.end())
    {
        m_sounds.push_back(sound);
    }
}


/**
 * Ajoute une musique à la liste des sons.
 *
 * \param music Pointeur sur la musique à ajouter.
 ******************************/

void CSoundEngine::addMusic(CMusic * music)
{
    if (music == nullptr)
        return;

    if (std::find(m_musics.begin(), m_musics.end(), music) == m_musics.end())
    {
        m_musics.push_back(music);
    }
}


/**
 * Enlève un son.
 *
 * \param sound Pointeur sur le son à enlever.
 ******************************/

void CSoundEngine::removeSound(CSound * sound)
{
    std::list<CSound *>::iterator it = std::find(m_sounds.begin(), m_sounds.end(), sound);
    if (it != m_sounds.end())
        m_sounds.erase(it);
}


/**
 * Enlève une musique.
 *
 * \param music Pointeur sur la musique à enlever.
 ******************************/

void CSoundEngine::removeMusic(CMusic * music)
{
    std::list<CMusic *>::iterator it = std::find(m_musics.begin(), m_musics.end(), music);
    if (it != m_musics.end())
        m_musics.erase(it);
}


/**
 * Récupère la liste des devices disponibles.
 *
 * \param devices Liste des devices.
 ******************************/

void CSoundEngine::getDevices(std::vector<CString>& devices)
{
    // Vidage de la liste
    devices.clear();

    // Récupération des devices disponibles
    const ALCchar * deviceList = alcGetString(nullptr, ALC_DEVICE_SPECIFIER);

    if (deviceList)
    {
        // Extraction des devices contenus dans la chaîne renvoyée
        while (strlen(deviceList) > 0)
        {
            devices.push_back(CString(deviceList));
            deviceList += strlen(deviceList) + 1;
        }
    }
}


/**
 * Récupère la liste des devices de capture disponibles.
 *
 * \param devices Liste des devices.
 ******************************/

void CSoundEngine::getCaptureDevices(std::vector<CString>& devices)
{
    // Vidage de la liste
    devices.clear();

    // Récupération des devices de capture disponibles
    const ALCchar * deviceList = alcGetString(nullptr, ALC_CAPTURE_DEVICE_SPECIFIER);

    if (deviceList)
    {
        // Extraction des devices contenus dans la chaîne renvoyée
        while (strlen(deviceList) > 0)
        {
            devices.push_back(deviceList);
            deviceList += strlen(deviceList) + 1;
        }
    }
}


/**
 * Fermeture de OpenAL.
 ******************************/

void CSoundEngine::shutdownOpenAL()
{
    CApplication::getApp()->log("Fermeture de OpenAL");

    // Récupération du contexte et du device
    ALCcontext * context = alcGetCurrentContext();
    ALCdevice * device  = alcGetContextsDevice(context);

    // Désactivation du contexte
    alcMakeContextCurrent(nullptr);

    // Destruction du contexte
    if (context)
        alcDestroyContext(context);

    // Fermeture du device
    if (device)
        alcCloseDevice(device);
}


/**
 * Vérifie l'existence d'une extension OpenAL.
 *
 * \param extension Extension dont on souhaite vérifier l'existence.
 * \return Booléen.
 ******************************/

bool CSoundEngine::isOALExtension(const CString& extension)
{
    return alIsExtensionPresent(extension.toCharArray());
}


/**
 * Génère un son.
 *
 * \todo Test pour Linux.
 * \todo Utiliser le logger à la place de fprintf.
 *
 * \param frequency Fréquence du son en hertz.
 * \param duration Durée du son en millisecondes.
 ******************************/

void CSoundEngine::beep(unsigned short frequency, unsigned short duration)
{
#if defined T_SYSTEM_WINDOWS
    ::Beep(frequency, duration);
#else
    // Try to snag the console
    if ((console_fd = open("/dev/console", O_WRONLY)) == -1)
    {
        fprintf(stderr, "Could not open /dev/console for writing.\n");
        printf("\a"); // Output the only beep we can, in an effort to fall back on usefulness
        return;
    }

    // Beep
    if (ioctl(console_fd, KIOCSOUND, (int)(clock_tick_rate / frequency)) < 0)
    {
        printf("\a"); // Output the only beep we can, in an effort to fall back on usefulness
    }

    usleep(1000 * duration); // wait...
    ioctl(console_fd, KIOCSOUND, 0); // stop beep

    close(console_fd);
#endif
}


/**
 * Modifie le volume de l'écouteur.
 *
 * \param volume Volume entre 0 et 100.
 ******************************/

void CSoundEngine::setVolume(float volume)
{
    alListenerf(AL_GAIN, volume * 0.01f);
}


/**
 * Modifie la position de l'écouteur.
 *
 * \param position Position.
 ******************************/

void CSoundEngine::setPosition(const TVector3F& position)
{
    alListener3f(AL_POSITION, position.X, position.Y, position.Z);
}


/**
 * Modifie la direction de l'écouteur.
 *
 * \param direction Direction.
 ******************************/

void CSoundEngine::setDirection(const TVector3F& direction)
{
    ALfloat orientation[] = { direction.X, direction.Y, direction.Z, 0.0f, 1.0f, 0.0f };
    alListenerfv(AL_ORIENTATION, orientation);
}


/**
 * Modifie la vitesse de l'écouteur.
 *
 * \param velocity Vitesse.
 ******************************/

void CSoundEngine::setVelocity(const TVector3F& velocity)
{
    alListener3f(AL_VELOCITY, velocity.X, velocity.Y, velocity.Z);
}


/**
 * Lit un son.
 *
 * \param fileName Adresse du fichier contenant le son.
 * \return Pointeur sur le son chargé.
 ******************************/

CSound * CSoundEngine::playSound(const CString& fileName)
{
    CSound * sound = new CSound();

    m_sounds.push_back(sound);

    sound->loadFromFile(fileName);
    sound->play();

    return sound;
}


/**
 * Lit une musique.
 *
 * \param fileName Adresse du fichier contenant la musique.
 * \return Pointeur sur la musique chargée.
 ******************************/

CMusic * CSoundEngine::playMusic(const CString& fileName)
{
    // On cherche si la musique est déjà en train d'être jouée
    for (std::list<CMusic *>::const_iterator it = m_musics.begin(); it != m_musics.end(); ++it)
    {
        if ((*it)->getFileName() == fileName)
        {
            return *it;
        }
    }

    CMusic * music = new CMusic();

    if (music->loadFromFile(fileName))
    {
        m_musics.push_back(music);
        music->play();
    }

    return music;
}


/**
 * Méthode appellée à chaque frame.
 *
 * \todo Supprimer les musiques terminées, qui ne sont pas jouées
 *       en boucle, et qui ont été ajoutées par la méthode PlayMusic.
 ******************************/

void CSoundEngine::frame()
{
    // Traitement des sons
    for (std::list<CSound *>::iterator it = m_sounds.begin(); it != m_sounds.end(); /*++it*/)
    {
        // Suppression des sons terminés
        if (!(*it)->isPlaying())
        {
            delete *it;
            it = m_sounds.erase(it);
        }
        else
        {
            ++it;
        }
    }

    // Traitement des musiques
    for (std::list<CMusic *>::iterator it = m_musics.begin(); it != m_musics.end(); /*++it*/)
    {
        // Mise-à-jour des musiques en cours de lecture
        if ((*it)->isPlaying())
        {
            (*it)->update();
            ++it;
        }
        // Suppression des musiques terminées
        else
        {
/*
            delete *it;
            it = m_musics.erase(it);
*/
            ++it;
        }

    }

    // Traitement de la capture audio
    if (m_capture)
    {
        m_capture->update();
    }
}


/**
 * Supprime tous les sons en cours de lecture.
 ******************************/

void CSoundEngine::deleteSounds()
{
    for (std::list<CSound *>::const_iterator it = m_sounds.begin(); it != m_sounds.end(); ++it)
    {
        delete *it;
    }

    m_sounds.clear();
}


/**
 * Supprime toutes les musiques en cours de lecture.
 ******************************/

void CSoundEngine::deleteMusics()
{
    for (std::list<CMusic *>::const_iterator it = m_musics.begin(); it != m_musics.end(); ++it)
    {
        delete *it;
    }

    m_musics.clear();
}


/**
 * Démarre la capture audio.
 ******************************/

void CSoundEngine::startCapture()
{
    if (m_capture)
        m_capture->start();
}


/**
 * Stop la capture audio.
 ******************************/

void CSoundEngine::stopCapture()
{
    if (m_capture)
        m_capture->stop();
}


/**
 * Stop la capture audio et l'enregistre dans un fichier.
 *
 * \param fileName Adresse du fichier dans lequel sauvegarder la capture audio.
 ******************************/

void CSoundEngine::saveCapture(const CString& fileName)
{
    if (m_capture)
    {
        m_capture->stop();
        m_capture->saveToFile(fileName);
    }
}


/**
 * Affiche un message d'erreur d'OpenAL dans la console et dans le log.
 *
 * \param error Numéro de l'erreur OpenAL.
 * \param msg   Message d'erreur facultatif.
 * \param line  Ligne à laquelle l'erreur se produit.
 ******************************/

void CSoundEngine::displayOpenALError(int error, const CString& msg, unsigned int line)
{
    CString str = (msg.isEmpty() ? "OpenAL" : msg);
    str += " donne ";

    switch (error)
    {
        default:                   str += CString::fromNumber(error); break;
        case AL_NO_ERROR:          str += "AL_NO_ERROR";              break;
        case AL_INVALID_OPERATION: str += "AL_INVALID_OPERATION";     break;
        case AL_INVALID_VALUE:     str += "AL_INVALID_VALUE";         break;
        case AL_INVALID_ENUM:      str += "AL_INVALID_ENUM";          break;
        case AL_INVALID_NAME:      str += "AL_INVALID_NAME";          break;
        case AL_OUT_OF_MEMORY:     str += "AL_OUT_OF_MEMORY";         break;

    }

    if (line > 0)
    {
        str += " ligne " + CString::fromNumber(line);
    }

    CApplication::getApp()->log(str, ILogger::Error);
}

} // Namespace Ted

/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CSound.cpp
 * \date       2008 Création de la classe CSound.
 * \date 11/07/2010 On peut modifier le nom du fichier.
 * \date 16/07/2010 Conversion en booléen.
 * \date 03/12/2010 CSound hérite de la classe ISound.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <vector>
#include <sndfile.h>

#include "Sound/CSound.hpp"
#include "Sound/CSoundEngine.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CSound::CSound() :
ISound   (),
m_buffer (0)
{ }


/**
 * Destructeur.
 ******************************/

CSound::~CSound()
{
    ALenum error;

    stop();

    if (m_buffer != 0)
    {
        alDeleteBuffers(1, &m_buffer);

        // Traitement des erreurs
        if ((error = alGetError()) != AL_NO_ERROR)
        {
            CSoundEngine::displayOpenALError(error, "alDeleteBuffers", __LINE__);
        }
    }
}


/**
 * Accesseur pour buffer.
 *
 * \return Buffer contenant le son.
 ******************************/

ALuint CSound::getBuffer() const
{
    return m_buffer;
}


/**
 * Modifie la valeur de loop.
 *
 * \param loop Indique si le son doit être joué en boucle.
 ******************************/

void CSound::setLoop(bool loop)
{
    m_loop = loop;

    ALenum error = alGetError();

    alSourcei(m_source, AL_LOOPING, loop);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
    }
}


/**
 * Charge un son.
 *
 * \param fileName Adresse du fichier contenant le son.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CSound::loadFromFile(const CString& fileName)
{
    m_loaded = false;
    m_fileName = fileName;
    ALenum error;

    // Ouverture du fichier audio avec libsndfile
    SF_INFO file_infos;
    SNDFILE * file = sf_open(m_fileName.toCharArray(), SFM_READ, &file_infos);

    if (!file)
    {
        CApplication::getApp()->log(CString("CSound::loadFromFile : impossible d'ouvrir le fichier %1").arg(m_fileName), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString("Chargement du son %1").arg(m_fileName));

    // Lecture du nombre d'échantillons et du taux d'échantillonnage
    m_nbrSamples = static_cast<ALsizei>(file_infos.channels * file_infos.frames);
    m_sampleRate = static_cast<ALsizei>(file_infos.samplerate);

    // Lecture des échantillons audio au format entier 16 bits signé
    std::vector<ALshort> samples(m_nbrSamples);

    if (sf_read_short(file, &samples[0], m_nbrSamples) < m_nbrSamples)
    {
        return false;
    }

    // Fermeture du fichier
    sf_close(file);

    // Détermination du format en fonction du nombre de canaux
    ALenum format;

    switch (file_infos.channels)
    {
        case 1:
            format = AL_FORMAT_MONO16;
            break;

        case 2:
            format = AL_FORMAT_STEREO16;
            break;

        case 4:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            format = alGetEnumValue("AL_FORMAT_QUAD16");
            break;

        case 6:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            format = alGetEnumValue("AL_FORMAT_51CHN16");
            break;

        case 7:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            format = alGetEnumValue("AL_FORMAT_61CHN16");
            break;

        case 8:

            if (!CSoundEngine::isOALExtension("AL_EXT_MCFORMATS"))
            {
                return false;
            }

            format = alGetEnumValue("AL_FORMAT_71CHN16");
            break;

        default:
            return false;
    }

    // Création du tampon OpenAL
    alGenBuffers(1, &m_buffer);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR || m_buffer == 0)
    {
        CSoundEngine::displayOpenALError(error, "alGenBuffers", __LINE__);
        return false;
    }

    // Remplissage avec les échantillons lus
    alBufferData(m_buffer, format, &samples[0], m_nbrSamples * sizeof(ALushort), m_sampleRate);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alBufferData", __LINE__);
        return false;
    }

    // Création d'une source
    alGenSources(1, &m_source);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR || m_source == 0)
    {
        CSoundEngine::displayOpenALError(error, "alGenSources", __LINE__);
        return false;
    }

    // Paramètres de la source
    alSourcei(m_source, AL_BUFFER, m_buffer);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
        return false;
    }

    alSourcei(m_source, AL_LOOPING, false);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcei", __LINE__);
    }

    alSourcef(m_source, AL_PITCH, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSourcef(m_source, AL_GAIN, 1.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSourcef", __LINE__);
    }

    alSource3f(m_source, AL_POSITION, 0.0f, 0.0f, 0.0f);

    // Traitement des erreurs
    if ((error = alGetError()) != AL_NO_ERROR)
    {
        CSoundEngine::displayOpenALError(error, "alSource3f", __LINE__);
    }

    m_loaded = true;
    return true;
}

} // Namespace Ted

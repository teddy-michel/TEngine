/*
Copyright (C) 2008-2014 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Sound/CCapture.cpp
 * \date 24/05/2009 Création de la classe CCapture.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <sndfile.h>

#include "Sound/CCapture.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CCapture::CCapture() :
m_device  (nullptr),
m_started (false)
{ }


/**
 * Destructeur.
 ******************************/

CCapture::~CCapture()
{
    alcCaptureCloseDevice(m_device);
}


/**
 * Indique si la capture est en cours.
 *
 * \return Booléen.
 ******************************/

bool CCapture::isStarted() const
{
    return m_started;
}


/**
 * Indique si la capture est en stoppée.
 *
 * \return Booléen.
 ******************************/

bool CCapture::isStoped() const
{
    return !m_started;
}


/**
 * Initialise la capture audio.
 *
 * \param device Device de lecture.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CCapture::initCapture(ALCdevice * device)
{
    // On commence par vérifier que la capture audio est supportée
    if (alcIsExtensionPresent(device, "ALC_EXT_CAPTURE") == AL_FALSE)
    {
        return false;
    }

    // Ouverture du device
    m_device = alcCaptureOpenDevice(nullptr, 44100, AL_FORMAT_MONO16, 44100);

    if (!m_device)
    {
        return false;
    }

    return true;
}


/**
 * Démarre la capture audio.
 ******************************/

void CCapture::start()
{
    m_samples.clear();

    // Lancement de la capture
    alcCaptureStart(m_device);
    m_started = true;
    update();
}


/**
 * Met à jour la capture audio.
 ******************************/

void CCapture::update()
{
    if (!m_started)
        return;

    // On récupère le nombre d'échantillons disponibles
    ALCint samplesAvailable;
    alcGetIntegerv(m_device, ALC_CAPTURE_SAMPLES, 1, &samplesAvailable);

    // On lit les échantillons et on les ajoute au tableau
    if (samplesAvailable > 0)
    {
        std::size_t s = m_samples.size();
        m_samples.resize(s + samplesAvailable);
        alcCaptureSamples(m_device, &m_samples[s], samplesAvailable);
    }
}


/**
 * Stop la capture audio.
 ******************************/

void CCapture::stop()
{
    // On stoppe la capture
    alcCaptureStop(m_device);
    m_started = false;

    // On récupère les derniers échantillons
    update();
}


/**
 * Sauvegarde la capture dans un fichier.
 *
 * \param fileName Adresse du fichier.
 ******************************/

void CCapture::saveToFile(const CString& fileName)
{
    // On renseigne les paramètres du fichier à créer
    SF_INFO fileInfos;
    fileInfos.channels = 1;
    fileInfos.samplerate = 44100;
    fileInfos.format = SF_FORMAT_PCM_16 | SF_FORMAT_WAV;

    // On ouvre le fichier en écriture
    SNDFILE * file = sf_open(fileName.toCharArray(), SFM_WRITE, &fileInfos);

    if (!file)
        return;

    // Ecriture des échantillons audio
    sf_write_short(file, &m_samples[0], m_samples.size());

    // Fermeture du fichier
    sf_close(file);
}

} // Namespace Ted

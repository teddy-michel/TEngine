/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderTedFont.cpp
 * \date 18/02/2011 Création de la classe CLoaderTedFont.
 * \date 19/02/2011 L'image est chargée correctement.
 * \date 28/02/2011 Correction d'une erreur dans le chargement des pixels de l'image.
 * \date 20/03/2011 Inclusion de cstring.
 * \date 08/04/2012 Nouvelle version du format de fichier.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>
#include <cstring>

#include "Graphic/CLoaderTedFont.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderTedFont::CLoaderTedFont() :
ILoader (),
m_size  (0)
{ }


/**
 * Donne la largeur d'un caractère dans l'image.
 *
 * \return Largeur d'un caractère en pixels.
 ******************************/

unsigned char CLoaderTedFont::getSize() const
{
    return m_size;
}


/**
 * Donne l'image de la police de caractères.
 *
 * \return Référence constante sur l'image de la police.
 ******************************/

const CImage& CLoaderTedFont::getImage() const
{
    return m_image;
}


/**
 * Donne les dimensions de chaque caractère.
 *
 * \param sizes Tableau qui contiendra les dimensions de chaque caractère.
 ******************************/

void CLoaderTedFont::getCharSizes(TCharSize sizes[256]) const
{
    std::copy(m_char_size, m_char_size + 256, sizes);
}


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderTedFont::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        CApplication::getApp()->log(CString("CLoaderTedFont::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // Lecture de l'en-tête
    THeader header;
    file.read(reinterpret_cast<char *>(&header), sizeof(THeader));

    // Vérification de l'identifiant du fichier
    if (header.ident != FileIdent)
    {
        CApplication::getApp()->log(CString("CLoaderTedFont::loadFromFile : l'identifiant du fichier de police du TEngine est incorrect (%1)").arg(header.ident), ILogger::Error);
        file.close();
        return false;
    }

    // Vérification de la version du fichier
    if (header.version != Version_1_0)
    {
        CApplication::getApp()->log(CString("CLoaderTedFont::loadFromFile : la version du fichier de police du TEngine est incorrecte (%1)").arg(header.version), ILogger::Error);
        file.close();
        return false;
    }

    strncpy(m_name, header.name, 31);
    std::copy(header.char_size, header.char_size + 256, m_char_size);
    m_size = header.size;

    const int size = 16 * header.size;
    const int size2 = size * size;
    uint8_t * pixels = new uint8_t[size2];

    file.seekg(header.offset);
    file.read(reinterpret_cast<char *>(pixels), size2);

    CColor * data = new CColor[size2];

    for (int i = 0; i < size2; ++i)
    {
        if (pixels[i] == 0)
        {
            data[i] = CColor(0xFF, 0xFF, 0xFF, 0x00);
        }
        else
        {
            //data[i] = CColor(pixels[i], pixels[i], pixels[i], 0x00);
            data[i] = CColor(pixels[i], pixels[i], pixels[i], pixels[i]);
        }
    }

    delete[] pixels;
    m_image = CImage(size, size, data);
    delete[] data;

    file.close();

    return true;
}

} // Namespace Ted

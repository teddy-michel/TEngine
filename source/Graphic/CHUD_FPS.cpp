/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CHUD_FPS.cpp
 * \date 25/01/2011 Création de la classe CHUD_FPS.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CHUD_FPS.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/CApplication.hpp"
#include "Core/Utils.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CHUD_FPS::CHUD_FPS() :
IHUD ()
{
    setSize(100, 26);
    setPosition(10, 10);
}


/**
 * Met à jour le contenu du HUD.
 ******************************/

void CHUD_FPS::update()
{
    T_ASSERT(m_buffer != nullptr);

    const float fps = CApplication::getApp()->getFPS();

    m_buffer->clear();

    m_buffer->addRectangle(m_rec, CColor(127, 127, 127, 127));


    // Texte
    TTextParams params;
    params.text  = CString("FPS : ") + CString::fromNumber(fps);
    params.font  = Game::fontManager->getFontId("Arial");
    params.size  = 16;
    params.color = CColor::White;
    params.rec.setX(m_rec.getX() + 5);
    params.rec.setY(m_rec.getY() + 5);
    params.rec.setWidth(m_rec.getWidth());
    params.rec.setHeight(m_rec.getHeight());

    Game::fontManager->drawText(m_buffer, params);


    m_buffer->update();
}


/**
 * Affiche le HUD.
 ******************************/

void CHUD_FPS::paint()
{
    m_buffer->draw();
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CMultiBuffer.cpp
 * \date 17/05/2009 Création de la classe CMultiBuffer.
 * \date 23/11/2010 On peut utiliser plus de deux unités de texture.
 * \date 09/12/2010 Suppression des méthodes pour ajouter une seule donnée aux tableaux.
 * \date 15/12/2010 Suppression des méthodes pour modifier ou accéder aux tableaux.
 * \date 15/12/2010 Création des méthodes qui retournent une référence sur un tableau.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 * \date 06/09/2015 Ajout du mode d'affichage des couleurs uniquement.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/CMultiBuffer.hpp"
#include "Graphic/CColor.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CMultiBuffer::CMultiBuffer() :
IBuffer       (),
m_primitive   (PrimTriangle),
m_numIndices  (0),
m_numVertices (0),
m_numNormales (0),
m_numColors   (0),
m_numTextures (0)
{
    for (unsigned short i = 0; i < NumUTUsed; ++i)
    {
        m_numCoords[i] = 0;
    }
}


/**
 * Donne le type de primitives à afficher.
 *
 * \return Type de primitive à afficher.
 *
 * \sa CMultiBuffer::setPrimitiveType
 ******************************/

TPrimitiveType CMultiBuffer::getPrimitiveType() const
{
    return m_primitive;
}


/**
 * Retourne le tableau des indices.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence sur le tableau des indices.
 ******************************/

std::vector<unsigned int>& CMultiBuffer::getIndices(unsigned short sub)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getIndices();
}


/**
 * Retourne le tableau des sommets.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence sur le tableau des sommets.
 ******************************/

std::vector<TVector3F>& CMultiBuffer::getVertices(unsigned short sub)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getVertices();
}


/**
 * Retourne le tableau des normales.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence sur le tableau des normales.
 ******************************/

std::vector<TVector3F>& CMultiBuffer::getNormales(unsigned short sub)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getNormales();
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \param sub  Numéro du SubBuffer (par défaut 0).
 * \param unit Unité de texture à utiliser (par défaut 0).
 * \return Référence sur le tableau des coordonnées de texture.
 ******************************/

std::vector<TVector2F>& CMultiBuffer::getTextCoords(unsigned short sub, unsigned short unit)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getTextCoords(unit);
}


/**
 * Retourne le tableau des couleurs.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence sur le tableau des couleurs.
 ******************************/

std::vector<float>& CMultiBuffer::getColors(unsigned short sub)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getColors();
}


/**
 * Retourne le tableau des textures.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence sur le tableau des textures.
 ******************************/

TBufferTextureVector& CMultiBuffer::getTextures(unsigned short sub)
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getTextures();
}


/**
 * Retourne le tableau des indices.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence constante sur le tableau des indices.
 ******************************/

const std::vector<unsigned int>& CMultiBuffer::getIndices(unsigned short sub) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getIndices();
}


/**
 * Retourne le tableau des sommets.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence constante sur le tableau des sommets.
 ******************************/

const std::vector<TVector3F>& CMultiBuffer::getVertices(unsigned short sub) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getVertices();
}


/**
 * Retourne le tableau des normales.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence constante sur le tableau des normales.
 ******************************/

const std::vector<TVector3F>& CMultiBuffer::getNormales(unsigned short sub) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getNormales();
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \param sub  Numéro du SubBuffer (par défaut 0).
 * \param unit Unité de texture à utiliser (par défaut 0).
 * \return Référence constante sur le tableau des coordonnées de texture.
 ******************************/

const std::vector<TVector2F>& CMultiBuffer::getTextCoords(unsigned short sub, unsigned short unit) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getTextCoords(unit);
}


/**
 * Retourne le tableau des couleurs.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence constante sur le tableau des couleurs.
 ******************************/

const std::vector<float>& CMultiBuffer::getColors(unsigned short sub) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getColors();
}


/**
 * Retourne le tableau des textures.
 *
 * \param sub Numéro du SubBuffer (par défaut 0).
 * \return Référence constante sur le tableau des textures.
 ******************************/

const TBufferTextureVector& CMultiBuffer::getTextures(unsigned short sub) const
{
    if (sub >= m_data.size())
    {
        throw CBadArgument("Subbuffer number is too big.");
    }

    TBufferDataList::const_iterator it = m_data.begin();
    std::advance(it, sub);

    return it->buffer.getTextures();
}


/**
 * Modifie les primitive.
 *
 * \param primitive Type de primitive à afficher.
 *
 * \sa CMultiBuffer::getPrimitiveType
 ******************************/

void CMultiBuffer::setPrimitiveType(TPrimitiveType primitive)
{
    m_primitive = primitive;
}


/**
 * Active un SubBuffer.
 *
 * \param sub Numéro du SubBuffer.
 ******************************/

void CMultiBuffer::enableSubBuffer(unsigned short sub)
{
    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);
    if (it != m_data.end())
        it->enable = true;
}


/**
 * Désactive un SubBuffer.
 *
 * \param sub Numéro du SubBuffer.
 ******************************/

void CMultiBuffer::disableSubBuffer(unsigned short sub)
{
    TBufferDataList::iterator it = m_data.begin();
    std::advance(it, sub);
    if (it != m_data.end())
        it->enable = false;
}


/**
 * Active tous les SubBuffers.
 ******************************/

void CMultiBuffer::enableAllSubBuffer()
{
    // Pour chaque SubBuffer
    for (TBufferDataList::iterator it = m_data.begin(); it != m_data.end(); ++it)
    {
        it->enable = true;
    }
}


/**
 * Translate tous les vertices.
 *
 * \param vector Vecteur de translation.
 ******************************/

void CMultiBuffer::translate(const TVector3F& vector)
{
    // Pour chaque SubBuffer
    for (TBufferDataList::iterator it = m_data.begin(); it != m_data.end(); ++it)
    {
        it->buffer.translate(vector);
    }
}


/**
 * Redimensionne l'ensemble des vertices.
 *
 * \param s Facteur de redimensionnement.
 ******************************/

void CMultiBuffer::scale(float s)
{
    // Pour chaque SubBuffer
    for (TBufferDataList::iterator it = m_data.begin(); it != m_data.end(); ++it)
    {
        it->buffer.scale(s);
    }
}


/**
 * Supprime toutes les données.
 ******************************/

void CMultiBuffer::clear()
{
    // Pour chaque SubBuffer
    for (TBufferDataList::iterator it = m_data.begin(); it != m_data.end(); ++it)
    {
        it->buffer.clear();
    }
}


/**
 * Dessine le contenu du buffer.
 * Doit être appelée depuis le thread principal.
 *
 * \return Nombre de triangles dessinés.
 ******************************/

unsigned int CMultiBuffer::draw() const
{
    if (m_vertexBuffer == 0 || m_indexBuffer == 0)
    {
        return 0;
    }

    unsigned int nbr = 0; // Nombre de triangles dessinés
    unsigned int vbo_offset = 0;

    TDisplayMode mode = Game::renderer->getMode();


    // Buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);


    // Activation des tableaux de vertices
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, T_BUFFER_OFFSET(vbo_offset));
    vbo_offset += m_numVertices * sizeof(TVector3F);


    // Activation des tableaux de coordonnées de textures si nécessaire
    if (mode != Wireframe && mode != WireframeCulling && mode != Grey && mode != Colored &&
        m_primitive != PrimLine && m_primitive != PrimPoint &&
        m_numTextures > 0 && m_numCoords[0] > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);

        glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(vbo_offset));
        vbo_offset += m_numCoords[0] * sizeof(TVector2F);

        // Multitexturing
        if (mode == Normal || mode == Lightmaps)
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                if (m_numCoords[i] > 0)
                {
                    glActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glClientActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                    glEnable(GL_TEXTURE_2D);

                    glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(vbo_offset));
                    vbo_offset += m_numCoords[i] * sizeof(TVector2F);
                }
            }
        }
        else
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                vbo_offset += m_numCoords[i] * sizeof(TVector2F);
            }
        }
    }
    else
    {
        for (unsigned int i = 0; i < NumUTUsed; ++i)
        {
            vbo_offset += m_numCoords[i] * sizeof(TVector2F);
        }
    }

    // Activation des tableaux de couleurs
    if (m_numColors > 0)
    {
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4, GL_FLOAT, 0, T_BUFFER_OFFSET(vbo_offset));
        vbo_offset += m_numColors * sizeof(float);
    }


    // Activation des tableaux de normales
    if (m_numNormales > 0)
    {
        glEnableClientState(GL_NORMAL_ARRAY );
        glNormalPointer(GL_FLOAT, 0, T_BUFFER_OFFSET(vbo_offset));
        //vbo_offset += m_size_norm * sizeof(float);
    }


    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    switch (mode)
    {
        default:
        case Normal:

            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                unsigned int offset = 0;

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Pour chaque texture différente
                        for (TBufferTextureVector::const_iterator it2 = it->buffer.getTextures().begin(); it2 != it->buffer.getTextures().end(); ++it2)
                        {
                            // Chargement des textures
                            if (m_numCoords[1] > 0)
                            {
                                Game::textureManager->bindTexture(it2->texture[0], 0);
                                Game::textureManager->bindTexture(it2->texture[1], 1);
                            }
                            else
                            {
                                Game::textureManager->bindTexture(it2->texture[0], 0);
                            }

                            // Affichage des primitives
                            glDrawElements(m_primitive, it2->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(offset * sizeof(unsigned int)));

                            nbr += it2->nbr;
                            offset += it2->nbr;
                        }
                    }
                    else
                    {
                        offset += it->nbr;
                    }
                }
            }
            else
            {
                // On n'utilise pas les textures
                Game::textureManager->bindTexture(0, 0);

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Affichage des primitives
                        glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                    }

                    nbr += it->nbr;
                }
            }

            glDisable(GL_CULL_FACE);

            break;

        // Affichage des textures sans les lightmaps
        case Textured:

            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                unsigned int offset = 0;
                Game::textureManager->bindTexture(0, 1);

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Pour chaque texture différente
                        for (TBufferTextureVector::const_iterator it2 = it->buffer.getTextures().begin(); it2 != it->buffer.getTextures().end(); ++it2)
                        {
                            // Chargement des textures
                            if (m_numCoords[1] > 0)
                            {
                                Game::textureManager->bindTexture(it2->texture[0], 0);
                            }
                            else
                            {
                                Game::textureManager->bindTexture(it2->texture[0], 0);
                            }

                            // Affichage des primitives
                            glDrawElements(m_primitive, it2->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(offset * sizeof(unsigned int)));

                            nbr += it2->nbr;
                            offset += it2->nbr;
                        }
                    }
                    else
                    {
                        offset += it->nbr;
                    }
                }
            }
            else
            {
                Game::textureManager->bindTexture(0, 0);

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Affichage des primitives
                        glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                    }

                    nbr += it->nbr;
                }
            }

            glDisable(GL_CULL_FACE);

            break;

        // Affichage des lightmaps
        case Lightmaps:

            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                unsigned int offset = 0;
                Game::textureManager->bindTexture(0, 0 );

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Pour chaque texture différente
                        for (TBufferTextureVector::const_iterator it2 = it->buffer.getTextures().begin(); it2 != it->buffer.getTextures().end(); ++it2)
                        {
                            // Chargement des textures
                            if (m_numCoords[1] > 0)
                            {
                                Game::textureManager->bindTexture(it2->texture[1], 1);
                            }
                            else
                            {
                                Game::textureManager->bindTexture(0, 0);
                            }

                            // Affichage des primitives
                            glDrawElements(m_primitive, it2->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(offset * sizeof(unsigned int)));

                            nbr += it2->nbr;
                            offset += it2->nbr;
                        }
                    }
                    else
                    {
                        offset += it->nbr;
                    }
                }
            }
            else
            {
                // On n'utilise pas les textures
                Game::textureManager->bindTexture(0, 0);

                for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
                {
                    // On dessine le SubBuffer
                    if (it->enable)
                    {
                        // Affichage des primitives
                        glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                    }

                    nbr += it->nbr;
                }
            }

            glDisable(GL_CULL_FACE);

            break;

        // Affichage des couleurs
        case Colored:
        {
            glEnable(GL_CULL_FACE);

            // On n'utilise pas les textures
            Game::textureManager->bindTexture(0, 0);

            for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
            {
                // On dessine le SubBuffer
                if (it->enable)
                {
                    // Affichage des primitives
                    glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                }

                nbr += it->nbr;
            }

            glDisable(GL_CULL_FACE);

            break;
        }

        // Mode fil de fer
        case Wireframe:

            Game::textureManager->bindTexture(0, 0);
            glColor4ub(255, 255, 255, 255);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
            {
                // On dessine le SubBuffer
                if (it->enable)
                {
                    // Affichage des primitives
                    glDrawElements(GL_TRIANGLES, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                }

                nbr += it->nbr;
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            break;

        // Mode fil de fer avec culling
        case WireframeCulling:

            glEnable(GL_CULL_FACE);

            Game::textureManager->bindTexture(0, 0);
            glColor4ub(255, 255, 255, 255);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            for (TBufferDataList::const_iterator it = m_data.begin(); it != m_data.end(); ++it)
            {
                // On dessine le SubBuffer
                if (it->enable)
                {
                    // Affichage des primitives
                    glDrawElements(GL_TRIANGLES, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr));
                }

                nbr += it->nbr;
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glDisable(GL_CULL_FACE);

            break;
    }

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);


    // Désactivation des tableaux de normales
    if (m_numNormales > 0)
    {
        glDisableClientState(GL_NORMAL_ARRAY);
    }

    // Désactivation des tableaux de couleurs
    if (m_numColors > 0)
    {
        glDisableClientState(GL_COLOR_ARRAY);
    }

    // Désactivation des tableaux de coordonnées de textures
    if (mode != Wireframe && mode != WireframeCulling && mode != Grey && mode != Colored &&
        m_primitive != PrimLine && m_primitive != PrimPoint &&
        m_numTextures > 0 && m_numCoords[0] > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        // Multitexturing
        if (mode == Normal || mode == Lightmaps)
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                if (m_numCoords[i] > 0)
                {
                    glActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glClientActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
                    glDisable(GL_TEXTURE_2D);
                }
            }

            glActiveTextureARB(GL_TEXTURE0_ARB);
            glClientActiveTextureARB(GL_TEXTURE0_ARB);
        }
    }

    // Désactivation des tableaux de vertices
    glDisableClientState(GL_VERTEX_ARRAY);

    return (nbr / 3);
}


/**
 * Met à jour les buffers.
 * Doit être appelée depuis le thread principal.
 ******************************/

void CMultiBuffer::update()
{
    IBuffer::update();

    // Tableaux
    std::vector<unsigned int> indices;
    std::vector<TVector3F> vertices;
    std::vector<TVector3F> normales;
    std::vector<TVector2F> coords[NumUTUsed];
    std::vector<float> colors;
    TBufferTextureVector textures;

    // Pour chaque SubBuffer
    for (TBufferDataList::iterator it = m_data.begin(); it != m_data.end(); ++it)
    {
        // On récupère les tableaux
        std::vector<unsigned int> tmp_indices = it->buffer.getIndices();
        std::vector<TVector3F> tmp_vertices   = it->buffer.getVertices();
        std::vector<TVector3F> tmp_normales   = it->buffer.getNormales();
        std::vector<float> tmp_colors         = it->buffer.getColors();
        TBufferTextureVector tmp_textures     = it->buffer.getTextures();

        for (unsigned short i = 0; i < NumUTUsed; ++i)
        {
            std::vector<TVector2F> tmp_coords = it->buffer.getTextCoords(i);

            for (std::vector<TVector2F>::iterator it2 = tmp_coords.begin() ; it2 != tmp_coords.end(); ++it2)
            {
                coords[i].push_back(*it2);
            }
        }


        for (std::vector<unsigned int>::iterator it2 = tmp_indices.begin(); it2 != tmp_indices.end(); ++it2)
        {
            indices.push_back(*it2);
        }

        for (std::vector<TVector3F>::iterator it2 = tmp_vertices.begin(); it2 != tmp_vertices.end(); ++it2)
        {
            vertices.push_back(*it2);
        }

        for (std::vector<TVector3F>::iterator it2 = tmp_normales.begin(); it2 != tmp_normales.end(); ++it2)
        {
            normales.push_back(*it2);
        }

        for (std::vector<float>::iterator it2 = tmp_colors.begin(); it2 != tmp_colors.end(); ++it2)
        {
            colors.push_back(*it2);
        }

        for (TBufferTextureVector::iterator it2 = tmp_textures.begin(); it2 != tmp_textures.end(); ++it2)
        {
            textures.push_back(*it2);
        }


        // Optimisation du tableau de textures
        unsigned int prevTexture = 0; // Identifiant de la dernière texture du tableau
        bool first = false;

        // On parcourt le tableau de textures
        for (TBufferTextureVector::iterator it2 = textures.begin(); it2 != textures.end(); ++it2)
        {
            // La texture de cette case est la même que celle de la case précédente, donc on les fusionne
            if (first && it2->texture[0] == prevTexture)
            {
                unsigned int nbr = it->nbr; // On conserve le nombre de faces à afficher pour cette texture
                it2 = textures.erase(it2); // On supprime la case actuelle du tableau

                // On modifie la case précédente du tableau
                --it2;
                it2->nbr += nbr;
            }
            else
            {
                prevTexture = it2->texture[0];
                first = true;
            }
        }
    }

    // Taille des tableaux
    m_numIndices  = indices.size();
    m_numVertices = vertices.size();
    m_numNormales = normales.size();
    m_numColors   = colors.size();
    m_numTextures = textures.size();

    for (unsigned short i = 0; i < NumUTUsed; ++i)
    {
        m_numCoords[i] = coords[i].size();
    }

    // Tailles des tableaux
    const unsigned int sz_vert = m_numVertices * sizeof(TVector3F);
    const unsigned int sz_colo = m_numColors * sizeof(float);
    const unsigned int sz_norm = m_numNormales * sizeof(TVector3F);
    unsigned int sz_total = sz_vert + sz_colo + sz_norm;
    unsigned int sz_txt[NumUTUsed];

    for (unsigned short i = 0; i < NumUTUsed; ++i)
    {
        sz_txt[i] = m_numCoords[i] * sizeof(TVector2F);
        sz_total += sz_txt[i];
    }


    // Modification du VBO
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER, sz_total, 0, GL_STATIC_DRAW);

    unsigned int sz = 0;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_vert, &vertices[0]);
    sz += sz_vert;

    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_txt[i], &coords[i][0]);
        sz += sz_txt[i];
    }

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_colo, &colors[0]);
    sz += sz_colo;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_norm, &normales[0]);
    //sz += sz_norm;

    // Modification du IBO
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, m_numIndices * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
}

} // Namespace Ted

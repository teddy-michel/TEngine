/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFormattedString.cpp
 * \date 11/05/2012 Création de la classe CFormattedString.
 * \date 08/04/2013 Modification complète de la classe.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CFormattedString.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param str           Chaine de caractères à formatter.
 * \param defaultFormat Formattage par défaut.
 ******************************/

CFormattedString::CFormattedString(const CString& str, const TFormat& defaultFormat) :
m_string        (str),
m_defaultFormat (defaultFormat)
{
    m_defaultFormat.offset = -1;
    m_defaultFormat.length = -1;
}


/**
 * Modifie le texte à formatter.
 *
 * \param str Chaine de caractère à formatter.
 ******************************/

void CFormattedString::setString(const CString& str)
{
    m_string = str;
}


/**
 * Modifie le formattage par défaut du texte.
 *
 * \param defaultFormat Formattage par défaut.
 ******************************/

void CFormattedString::setDefaultFormat(const TFormat& format)
{
    m_defaultFormat = format;
    m_defaultFormat.offset = -1;
    m_defaultFormat.length = -1;
}


CFormattedString::TFormat CFormattedString::getFormat(int offset) const
{
    if (offset < 0)
        return m_defaultFormat;

    for (TFormatList::const_iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
    {
        if (it->offset <= offset && it->offset + it->length >= offset)
        {
            return *it;
        }
        else if (it->offset > offset)
        {
            return m_defaultFormat;
        }
    }

    return m_defaultFormat;
}


void CFormattedString::setPartFormat(const TFormat& format)
{
    if (format.length <= 0)
        return;

    if (format.offset < 0)
        return;

    setPartFormat(format.offset, format.length, format);
}


void CFormattedString::setPartDefaultFormat(int offset, int length)
{
    if (length < 0)
        return;

    if (offset < 0)
        return;

    setPartFormat(offset, length, m_defaultFormat);
}


/**
 * Retourne le prochain format à partir d'un offset dans la chaine.
 *
 * \param offset Offset dans la chaine de caractères.
 * \return Prochain format.
 ******************************/

CFormattedString::TFormat CFormattedString::getNextFormat(int offset) const
{
    if (offset < 0)
        offset = 0;

    TFormat lastPart;

    for (TFormatList::const_iterator it = m_blocks.begin(); it != m_blocks.end(); ++it)
    {
        if (it->offset > offset)
        {
            return *it;
        }
    }

    return m_defaultFormat;
}


/**
 * Supprime tous les formattages de la chaine.
 ******************************/

void CFormattedString::clearFormat()
{
    m_blocks.clear();
}


void CFormattedString::setPartFormat(int offset, int length, const TFormat& format)
{
    T_ASSERT(offset >= 0);
    T_ASSERT(length > 0);

    int offsetEnd = offset + length - 1;
    bool needInsert = (format.offset != -1 && format.length != -1);
    bool canInsert = false;

    for (TFormatList::iterator it = m_blocks.begin(); it != m_blocks.end(); /*++it*/)
    {
        int itOffsetEnd = it->offset + it->length - 1;

        if (it->offset < offset)
        {
            if (itOffsetEnd < offset)
            {
                continue;
            }
            else if (itOffsetEnd == offset)
            {
                if (it->length > 1)
                {
                    --(it->length);
                    canInsert = true;
                }
                else
                {
                    it = m_blocks.erase(it);
                    canInsert = true;
                    continue;
                }
            }
            else
            {
                it->length = offset - it->offset;
                canInsert = true;
            }
        }
        else if (it->offset == offset)
        {
            int diff = it->length - length;

            if (diff < 0)
            {
                it = m_blocks.erase(it);
                canInsert = true;
            }
            else if (diff == 0)
            {
                it = m_blocks.erase(it);
                if (needInsert)
                    it = m_blocks.insert(it, format);
                return;
            }
            else
            {
                it->offset += length;
                it->length -= length;
                canInsert = true;
            }
        }
        else
        {
            if (offsetEnd < it->offset)
            {
                it = m_blocks.insert(it, format);
                return;
            }
            else
            {
                if (itOffsetEnd < offset)
                {
                    it = m_blocks.erase(it);
                    canInsert = true;
                }
                else if (itOffsetEnd == offset)
                {
                    it = m_blocks.erase(it);
                    if (needInsert)
                        it = m_blocks.insert(it, format);
                    return;
                }
                else
                {
                    it->offset = offsetEnd;
                    it->length -= offsetEnd - it->offset;
                    if (needInsert)
                        it = m_blocks.insert(it, format);
                    return;
                }
            }
        }

        ++it;
    }

    if (needInsert)
        m_blocks.push_back(format);
}

} // Namespace Ted

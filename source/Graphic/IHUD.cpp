/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/IHUD.cpp
 * \date 13/02/2010 Création de la classe CHUD.
 * \date 25/01/2011 Transformation de la classe CHUD en IHUD virtuelle.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/IHUD.hpp"
#include "Graphic/CBuffer2D.hpp"

// DEBUG
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

IHUD::IHUD() :
m_buffer (nullptr),
m_rec    ()
{
    m_buffer = new CBuffer2D();
    T_ASSERT(m_buffer != nullptr);
}


/**
 * Destructeur.
 ******************************/

IHUD::~IHUD()
{
    delete m_buffer;
}


/**
 * Donne le buffer graphique utilisé par le HUD.
 *
 * \return Pointeur sur le buffer graphique.
 ******************************/

CBuffer2D * IHUD::getBuffer() const
{
    return m_buffer;
}


/**
 * Donne les dimensions du rectangle pour afficher le HUD.
 *
 * \return Dimensions du rectangle.
 ******************************/

CRectangle IHUD::getRec() const
{
    return m_rec;
}


/**
 * Modifie la position horizontale du HUD.
 *
 * \param x Position horizontale.
 ******************************/

void IHUD::setX(int x)
{
    m_rec.setX(x);
}


/**
 * Modifie la position verticale du HUD.
 *
 * \param y Position verticale.
 ******************************/

void IHUD::setY(int y)
{
    m_rec.setY(y);
}


/**
 * Modifie la position du HUD.
 *
 * \param x Position horizontale.
 * \param y Position verticale.
 *
 * \sa IHUD::setX
 * \sa IHUD::setY
 ******************************/

void IHUD::setPosition(int x, int y)
{
    setX(x);
    setY(y);
}


/**
 * Modifie la largeur du HUD.
 *
 * \param width Largeur en pixels.
 ******************************/

void IHUD::setWidth(int width)
{
    m_rec.setWidth(width);
}


/**
 * Modifie la hauteur du HUD.
 *
 * \param height Hauteur en pixels.
 ******************************/

void IHUD::setHeight(int height)
{
    m_rec.setHeight(height);
}


/**
 * Modifie la largeur du HUD.
 *
 * \param width  Largeur en pixels.
 * \param height Hauteur en pixels.
 *
 * \sa IHUD::setWidth
 * \sa IHUD::setHeight
 ******************************/

void IHUD::setSize(int width, int height)
{
    setWidth(width);
    setHeight(height);
}

} // Namespace Ted

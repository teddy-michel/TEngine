/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderVMT.cpp
 * \date 10/01/2010 Création de la classe CLoaderVMT.
 * \date 10/07/2010 Le chargement se fait avec la méthode loadFromFile.
 * \date 05/06/2014 Utilisation des répertoires du gestionnaire de textures.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Graphic/CLoaderVMT.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Core/Utils.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderVMT::CLoaderVMT() :
ILoader()
{ }


/**
 * Accesseur pour texture.
 *
 * \return Nom de la texture.
 ******************************/

CString CLoaderVMT::getTexture() const
{
    return m_texture;
}


/**
 * Chargement du fichier.
 *
 * \todo Lire toutes les données possibles.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderVMT::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        //CApplication::getApp()->log(CString("CLoaderVMT::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    std::string texte;
    TShaderType type = UNKNOWN;

    file >> texte;
    texte = toLower(texte);

         // Type de fichier VMT
         if (texte == "\"cable\"")                 type = CABLE;
    else if (texte == "\"lightmappedgeneric\"")    type = LIGHT_MAPPED_GENERIC;
    else if (texte == "\"patch\"")                 type = PATCH;
    else if (texte == "\"unlitgeneric\"")          type = UNLIT_GENERIC;
    else if (texte == "\"unlittwotexture\"")       type = UNLIT_TWO_TEXTURE;
    else if (texte == "\"vertexlitgeneric\"")      type = VERTEXLIT_GENERIC;
    else if (texte == "\"water\"")                 type = WATER;
    else if (texte == "\"worldvertextransition\"") type = WORLD_VERTEX_TRANSITION;

    // On doit ensuite lire une accolade ouvrante
    file >> texte;

    if (texte != "{")
    {
        return false;
    }

    // On lit le reste du fichier mot par mot
    while ((file >> texte))
    {
        // Accolade fermante : fin des données
        if (texte == "}")
        {
            return true;
        }

        // Commentaire
        if (texte.size() > 2 && texte[0] == '/' && texte[1] == '/')
        {
            std::getline(file, texte);
            continue;
        }

        // Lecture du nom de la texture
        if ((type == LIGHT_MAPPED_GENERIC ||
             type == VERTEXLIT_GENERIC ||
             type == UNLIT_GENERIC) && toLower(texte) == "\"$basetexture\"")
        {
            file >> texte;

            if (texte.size() > 3)
            {
                // Suppression des guillemets
                std::string::size_type pos = texte.find_first_of('"');

                if (pos != std::string::npos)
                {
                    texte = texte.substr(pos + 1, std::string::npos);
                }

                pos = texte.find_last_of('"');

                if (pos != std::string::npos)
                {
                    texte = texte.substr(0, pos);
                }

                m_texture = texte.c_str();

                return true;
            }
        }
        // On doit inclure un autre fichier VMT
        else if (type == PATCH && toLower(texte) == "\"include\"")
        {
            file >> texte;

            if (texte.size() > 3)
            {
                std::string::size_type pos = texte.find_first_of('"');

                if (pos != std::string::npos)
                {
                    texte = texte.substr(pos + 1, std::string::npos);
                }

                pos = texte.find_last_of('"');

                if (pos != std::string::npos)
                {
                    texte = texte.substr(0, pos);
                }

                std::list<CString> texturePaths = Game::textureManager->getPaths();

                for (std::list<CString>::const_iterator it = texturePaths.begin(); it != texturePaths.end(); ++it)
                {
                    CLoaderVMT new_vmt_file;

                    // On tente de lire le fichier VMT
                    if (new_vmt_file.loadFromFile(*it + texte.c_str()))
                    {
                        m_texture = new_vmt_file.getTexture();
                        break;
                    }
                }

                return true;
            }
        }
    }

    // Il manque l'accolade fermante
    return false;
}

} // Namespace Ted

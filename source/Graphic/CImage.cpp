/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CImage.cpp
 * \date       2008 Création de la classe CImage.
 * \date 15/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 15/07/2010 Les images sont chargées et enregistrées au format RGBA.
 * \date 17/07/2010 Correction d'un dans la gestion des erreurs de DevIL.
 * \date 10/04/2013 Ajout de la méthode insertImage.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <il.h>
#include <fstream>

#include "Graphic/CImage.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur de copie.
 *
 * \param image Image à copier.
 ******************************/

CImage::CImage(const CImage& image) :
m_width  (image.m_width),
m_height (image.m_height),
m_pixels (image.m_pixels)
{

}


/**
 * Constructeur par défaut.
 ******************************/

CImage::CImage() :
m_width  (0),
m_height (0),
m_pixels ()
{

}


/**
 * Constructeur avec des dimensions.
 *
 * \param width  Largeur de l'image en pixels.
 * \param height Hauteur de l'image en pixels.
 ******************************/

CImage::CImage(int width, int height) :
m_width  (width < 0 ? 0 : width),
m_height (height < 0 ? 0 : height),
m_pixels (m_width * m_height, CColor())
{ }


/**
 * Constructeur.
 *
 * \param width  Largeur de l'image en pixels.
 * \param height Hauteur de l'image en pixels.
 * \param pixels Tableau de pixels.
 ******************************/

CImage::CImage(int width, int height, const CColor * pixels) :
m_width  (width < 0 ? 0 : width),
m_height (height < 0 ? 0 : height),
m_pixels (pixels, pixels + m_width * m_height)
{ }


/**
 * Accesseur pour width.
 *
 * \return Largeur de l'image
 ******************************/

int CImage::getWidth() const
{
    return m_width;
}


/**
 * Accesseur pour height.
 *
 * \return Hauteur de l'image.
 ******************************/

int CImage::getHeight() const
{
    return m_height;
}


/**
 * Accesseur pour pixels.
 *
 * \return Tableau de pixels.
 ******************************/

const CColor * CImage::getPixels() const
{
    return &m_pixels[0];
}


/**
 * Charge l'image depuis un fichier.
 *
 * \todo Utiliser les loaders d'images à la place de Devil.
 * \bug La fonction ilLoadImage plante pour certains fichiers VTF.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CImage::loadFromFile(const CString& fileName)
{
    // Test de l'existence du fichier
    std::ifstream file(fileName.toCharArray());
    if (file.fail())
    {
        return false;
    }

    // Génération d'une nouvelle texture
    ILuint texture;
    ilGenImages(1, &texture);
    ilBindImage(texture);

    // Chargement de l'image
    if (!ilLoadImage(fileName.toCharArray()))
    {
        CApplication::getApp()->log(CString("CImage::loadFromFile : impossible de charger l'image %1 (erreur %2)").arg(fileName).arg(ilGetError()), ILogger::Error);
        return false;
    }

    m_width  = ilGetInteger(IL_IMAGE_WIDTH);
    m_height = ilGetInteger(IL_IMAGE_HEIGHT);

    // Récupération des pixels de l'image
    CColor * firstPixel = reinterpret_cast<CColor *>(ilGetData());
    m_pixels.assign(firstPixel, firstPixel + m_width * m_height);

    // Suppression de la texture
    ilDeleteImages(1, &texture);

    CApplication::getApp()->log(CString("Chargement de l'image %1").arg(fileName));
    return true;
}


/**
 * Sauvegarde l'image dans un fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CImage::saveToFile(const CString& fileName) const
{
    // On crée une copie de l'image dans un format compatible avec DevIL (RGBA 32 bits)
    CImage image = *this;

    // On retourne l'image, sans quoi elle apparaîtrait à l'envers
    image.flip();

    // Génération d'une nouvelle texture
    ILuint texture;
    ilGenImages(1, &texture);
    ilBindImage(texture);

    // Dimensionnement et remplissage de la texture avec les pixels convertis
    if (!ilTexImage(image.getWidth(), image.getHeight(), 1, 4, IL_RGBA, IL_UNSIGNED_BYTE, const_cast<void *>(reinterpret_cast<const void *>(image.getPixels()))))
    {
        CApplication::getApp()->log(CString::fromUTF8("CImage::saveToFile : impossible de créer la texture de l'image dans %1 (erreur %2)").arg(fileName).arg(ilGetError()), ILogger::Error);
        return false;
    }

    // Sauvegarde de la texture
    if (!ilSaveImage(fileName.toCharArray()))
    {
        CApplication::getApp()->log(CString::fromUTF8("CImage::saveToFile : impossible de sauvegarder l'image %1 (erreur %2)").arg(fileName).arg(ilGetError()), ILogger::Error);
        return false;
    }

    CApplication::getApp()->log(CString("Sauvegarde de l'image dans %1").arg(fileName));

    // Suppression de la texture
    ilDeleteImages(1, &texture);

    return true;
}


/**
 * Remplit l'image d'une couleur.
 *
 * \param color Couleur à utiliser.
 ******************************/

void CImage::fill(const CColor& color)
{
    for (std::vector<CColor>::iterator it = m_pixels.begin(); it != m_pixels.end(); ++it)
    {
        *it = color;
    }
}


/**
 * Change la couleur d'un pixel.
 *
 * \param x     Coordonnée horizontale du pixel.
 * \param y     Coordonnée verticale du pixel.
 * \param color Couleur du pixel.
 ******************************/

void CImage::setPixel(int x, int y, const CColor& color)
{
    T_ASSERT(x >= 0 && x < m_width);
    T_ASSERT(y >= 0 && y < m_height);

    m_pixels[x + y * m_width] = color;
}


/**
 * Récupère la couleur d'un pixel.
 *
 * \param x Coordonnée horizontale du pixel.
 * \param y Coordonnée verticale du pixel.
 * \return color du pixel.
 ******************************/

CColor CImage::getPixel(int x, int y) const
{
    T_ASSERT(x >= 0 && x < m_width);
    T_ASSERT(y >= 0 && y < m_height);

    return m_pixels[x + y * m_width];
}


/**
 * Retourne l'image verticalement. Inverse le haut et le bas.
 ******************************/

void CImage::flip()
{
    for (int row = 0; row < m_height / 2; ++row)
    {
        for (int col = 0; col < m_width; ++col)
        {
            CColor tmp = m_pixels[row * m_width + col];
            m_pixels[row * m_width + col] = m_pixels[(m_height - row - 1) * m_width + col];
            m_pixels[(m_height - row - 1) * m_width + col] = tmp;
        }
    }
}


/**
 * Retourne l'image horizontalement. Inverse la gauche et la droite.
 ******************************/

void CImage::mirror()
{
    for (int row = 0; row < m_height; ++row)
    {
        for (int col = 0; col < m_width / 2; ++col)
        {
            CColor tmp = m_pixels[row * m_width + col];
            m_pixels[row * m_width + col] = m_pixels[row * m_width + (m_width - col - 1)];
            m_pixels[row * m_width + (m_width - col - 1)] = tmp;
        }
    }
}


/**
 * Change une couleur de l'image en une autre.
 *
 * \param from Couleur à changer.
 * \param to   Nouvelle couleur.
 ******************************/

void CImage::changeColor(const CColor& from, const CColor& to)
{
    // On parcourt l'image
    for (int i = 0; i < m_width; ++i)
    {
        for (int j = 0; j < m_height; ++j)
        {
            if (getPixel(i, j) == from)
            {
                setPixel(i, j, to);
            }
        }
    }
}


void CImage::insertImage(const CImage& image, const CRectangle& rec)
{
    // Rien à insérer
    if (rec.isEmpty() || rec.getX() > m_width || rec.getY() > m_height)
        return;

    const int numRows = std::min(rec.getHeight(), image.getHeight());
    const int numCols = std::min(rec.getWidth(), image.getWidth());

    if (numRows <= 0 || numCols <= 0)
        return;

    const int minRow = std::max(0, rec.getY());
    const int minCol = std::max(0, rec.getX());

    const int maxRow = std::min(rec.getY() + numRows, m_height);
    const int maxCol = std::min(rec.getX() + numCols, m_width);

    for (int row = minRow, x = 0; row < maxRow; ++row, ++x)
    {
        for (int col = minCol, y = 0; col < maxCol; ++col, ++y)
        {
            m_pixels[row * m_width + col] = image.m_pixels[x * image.m_width + y];
        }
    }
}


CImage& CImage::operator=(const CImage& image)
{
    m_width = image.m_width;
    m_height = image.m_height;
    m_pixels = image.m_pixels;

    return *this;
}

} // Namespace Ted

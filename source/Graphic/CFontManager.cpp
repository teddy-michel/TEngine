/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFontManager.cpp
 * \date       2008 Création de la classe CPoliceManager.
 * \date 08/07/2010 La classe CPoliceManager s'appelle désormais CFontManager.
 * \date 10/07/2010 Correction d'un problème avec les couleurs.
 * \date 15/07/2010 Correction d'un bug qui empêchait le chargement des polices.
 * \date 22/07/2010 La méthode DrawText prend un rectangle comme argument.
 * \date 22/07/2010 Création de la méthode LogFontList.
 * \date 23/07/2010 Le texte n'est affiché que dans le rectangle donné.
 * \date 30/10/2010 Corrections de bugs liés au surlignement du texte.
 * \date 04/11/2010 On peut décaler un texte en haut et à gauche.
 * \date 12/11/2010 Les méthodes DrawText prennent un TTextParams en argument.
 * \date 12/11/2010 Création des méthodes getTextLinesSize et getTextLineSize.
 * \date 23/01/2011 Adaptation de la méthode DrawText suite aux changements des buffers 2D.
 * \date 19/02/2011 Les polices sont chargées depuis un fichier de police TEF.
 * \date 26/03/2011 Les caractères en bordure du rectangle sont découpés.
 * \date 12/12/2011 Changement du nom de la texture.
 * \date 14/04/2012 Utilisation de CString à la place de std::string.
 * \date 19/04/2012 Les polices sont manipulées via un identifiant.
 * \date 13/04/2013 Utilisation possible de FreeType.
 * \date 15/06/2014 Les caractères partiellement visibles sont affichés.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <algorithm>
#include <GL/glew.h>

#include "Graphic/CFontManager.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CLoaderTedFont.hpp"
#include "Graphic/CFont.hpp"
#include "Core/ILogger.hpp"
#include "Core/CApplication.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

namespace Game
{
    CFontManager * fontManager = nullptr;
}


/**
 * Constructeur par défaut.
 ******************************/

CFontManager::CFontManager()
{
    if (Game::fontManager)
        exit(-1);

    Game::fontManager = this;

#ifdef T_USE_FREETYPE

    FT_Error error = FT_Init_FreeType(&m_library);

    if (error != 0)
    {
        CApplication::getApp()->log(CString("Erreur lors de l'initialisation de FreeType (code %1)").arg(error), ILogger::Error);
    }

#endif
}


/**
 * Destructeur.
 ******************************/

CFontManager::~CFontManager()
{
    Game::fontManager = nullptr;
    unloadFonts();

#ifdef T_USE_FREETYPE

    if (m_library)
    {
        FT_Error error = FT_Done_FreeType(m_library);

        if (error != 0)
        {
            CApplication::getApp()->log(CString("Erreur lors de la fermeture de FreeType (code %1)").arg(error), ILogger::Error);
        }
    }

#endif
}


/**
 * Retourne l'identifiant de la police à partir de son nom.
 *
 * \param name Nom de la police.
 * \return Identifiant de la police si elle a été chargée, sinon retourne InvalidFont.
 ******************************/

TFontId CFontManager::getFontId(const CString& name)
{
    TFontId id = 0;

    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it, ++id)
    {
#ifdef T_USE_FREETYPE
        if ((*it)->getFamilyName() == name)
#else
        if (it->name == name)
#endif
        {
            return id;
        }
    }

    // Si la police n'a pas été chargée, on le fait

#ifdef T_USE_FREETYPE
    return privLoadFont(name, CString("../../fonts/" + name + ".ttf"));
#endif

    return privLoadFont(name, CString("../../fonts/" + name + ".tef"));
}


/**
 * Retourne le nom de la police associée à un identifiant.
 *
 * \param id Identifiant de la police.
 * \return Nom de la police ou chaine vide si la police n'existe pas.
 ******************************/

CString CFontManager::getFontName(const TFontId id) const
{
    if (id < m_fonts.size())
    {
#ifdef T_USE_FREETYPE
        return m_fonts[id]->getFamilyName();
#else
        return m_fonts[id].name;
#endif
    }

    // La police n'a pas été trouvée
    return CString();
}


/**
 * Charge une police de caractère.
 * Si le nom est déjà utilisé, la police n'est pas chargée.
 *
 * \param name     Nom de la police.
 * \param fileName Adresse du fichier contenant la police à charger.
 * \return Identifiant de la police
 ******************************/

TFontId CFontManager::loadFont(const CString& name, const CString& fileName)
{
    TFontId id = 0;

    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it, ++id)
    {
#ifdef T_USE_FREETYPE
        if ((*it)->getFamilyName() == name)
#else
        if (it->name == name)
#endif
        {
            return id;
        }
    }

    // Si la police n'a pas été chargée, on le fait
    return privLoadFont(name, fileName);
}


/**
 * Supprime toutes les polices.
 ******************************/

void CFontManager::unloadFonts()
{
    CApplication::getApp()->log(CString::fromUTF8("Suppression des polices de caractère (%1 polices supprimées)").arg(m_fonts.size()));

#ifndef T_USE_FREETYPE
    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it)
    {
        Game::textureManager->deleteTexture(it->texture);
    }
#endif

    m_fonts.clear();
}


/**
 * Ajoute le texte dans un buffer graphique.
 *
 * \todo Aller automatiquement à la ligne si demandé.
 * \todo Vérifier les coordonnées de texture pour éviter de devoir retourner l'image.
 * \todo Utiliser la classe CFormattedString.
 *
 * \param buffer Pointeur sur le buffer graphique 2D à utiliser.
 * \param params Paramètres du texte à afficher.
 *
 * Les codes ASCII suivants permettent de changer la couleur du texte :
 * \li 1 : Couleur normale.
 * \li 2 : Blanc.
 * \li 3 : Noir.
 * \li 4 : Rouge.
 * \li 5 : Bleu.
 * \li 6 : Vert.
 * \li 7 : Jaune.
 * \li 8 : Orange.
 * \li 12 : Surligné.
 ******************************/

void CFontManager::drawText(CBuffer2D * buffer, const TTextParams& params)
{
    if (buffer == nullptr)
        return;

    if (params.text.isEmpty())
        return;

    if (params.font >= m_fonts.size())
        return;

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->drawText(buffer, params);
#else
    const TFont& font = m_fonts[params.font];

    // Variables
    const float ratio = params.size * 16.0f / font.width;
    float x = static_cast<float>(params.rec.getX()) - static_cast<float>(params.left);
    float y = static_cast<float>(params.rec.getY()) - static_cast<float>(params.top);
    float offset = 0.5f / font.width;

    bool highlight = false;
    bool draw_char = true;
    CColor color_actual = params.color;


    // On récupère les tableaux du buffer graphique
    std::vector<TPrimitiveType>& elements = buffer->getElements();
    std::vector<unsigned int>& indices = buffer->getIndices();
    std::vector<TVector2S>& vertices = buffer->getVertices();
    std::vector<float>& colors = buffer->getColors();
    std::vector<float>& coords = buffer->getTextCoords();
    std::vector<unsigned int>& textures = buffer->getTextures();


    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        // Gestion des caractères spéciaux
        switch (c)
        {
            // Couleur normale
            case 1:
                color_actual = params.color;
                continue;

            // Blanc
            case 2:
                color_actual = CColor::White;
                continue;

            // Noir
            case 3:
                color_actual = CColor::Black;
                continue;

            // Rouge
            case 4:
                color_actual = CColor::Red;
                continue;

            // Bleu
            case 5:
                color_actual = CColor::Blue;
                continue;

            // Vert
            case 6:
                color_actual = CColor::Green;
                continue;

            // Jaune
            case 7:
                color_actual = CColor::Yellow;
                continue;

            // Orange
            case 8:
                color_actual = CColor::Orange;
                continue;

            // Surlignement
            case 12:
                highlight = !highlight;
                continue;

            default:
                break;
        }

        drawChar = ((params.rec.getWidth()  == 0 || (x < params.rec.getX() + params.rec.getWidth() &&
                                                     x + charParams.rect.getWidth() > static_cast<float>(params.rec.getX()))) &&
                    (params.rec.getHeight() == 0 || (y < params.rec.getY() + params.rec.getHeight() &&
                                                     y + charParams.rect.getHeight() > static_cast<float>(params.rec.getY()))));

        // Pourcentages d'affichage du caractère
        float perX1 = 0.0f;
        float perX2 = 1.0f;
        float perY1 = 0.0f;
        float perY2 = 1.0f;

        if (params.rec.getWidth() > 0)
        {
            // Caractère découpé à gauche
            if (x < params.rec.getX() &&
                x + font.size[c].X * ratio > params.rec.getX())
            {
                perX1 = (params.rec.getX() - x) / (font.size[c].X * ratio);
            }

            // Caractère découpé à droite
            if (x < params.rec.getX() + params.rec.getWidth() &&
                x + font.size[c].X * ratio > params.rec.getX() + params.rec.getWidth())
            {
                perX2 = (params.rec.getX() + params.rec.getWidth() - x) / (font.size[c].X * ratio);
            }
        }

        if (params.rec.getHeight() > 0)
        {
            // Caractère découpé en bas
            if (y < params.rec.getY() &&
                y + font.size[c].Y * ratio > params.rec.getY())
            {
                perY1 = (params.rec.getY() - y) / (font.size[c].Y * ratio);
            }

            // Caractère découpé en bas
            if (y < params.rec.getY() + params.rec.getHeight() &&
                y + font.size[c].Y * ratio > params.rec.getY() + params.rec.getHeight())
            {
                perY2 = (params.rec.getY() + params.rec.getHeight() - y) / (font.size[c].Y * ratio);
            }
        }

        // Surlignement du texte
        if (highlight && drawChar)
        {
            // Couleur de surlignement
            float tmp_color[4] = { 1.0f, 0.5f, 0.0f, 0.9f };

            elements.push_back(PrimTriangle);
            textures.push_back(0);
            elements.push_back(PrimTriangle);
            textures.push_back(0);

            unsigned int indice = vertices.size();

            indices.push_back(indice);
            indices.push_back(indice + 1);
            indices.push_back(indice + 2);
            indices.push_back(indice);
            indices.push_back(indice + 2);
            indices.push_back(indice + 3);

            vertices.push_back(TVector2F(x, y));
            vertices.push_back(TVector2F(x + font.size[c].X * ratio * perX2, y));
            vertices.push_back(TVector2F(x + font.size[c].X * ratio * perX2, y + params.size * perY2));
            vertices.push_back(TVector2F(x, y + params.size * perY2));

            for (int i = 0; i < 4; ++i)
            {
                coords.push_back(0.0f);
                coords.push_back(0.0f);

                colors.push_back(tmp_color[0]);
                colors.push_back(tmp_color[1]);
                colors.push_back(tmp_color[2]);
                colors.push_back(tmp_color[3]);
            }
        }

        switch (c)
        {
            // Saut de ligne
            case '\n':
            case '\r':
                x = static_cast<float>(params.rec.getX()) - static_cast<float>(params.left);
                y += params.size;
                continue;

            // Espace
            case ' ':
                x += font.size[32].X * ratio;
                continue;

            // Tabulation horizontale
            case '\t':
                x += 4.0f * font.size[32].X * ratio;
                continue;

            default:

                if (drawChar)
                {
                    float tmp_color[4] = { color_actual.getRed()   / 255.0f ,
                                           color_actual.getGreen() / 255.0f ,
                                           color_actual.getBlue()  / 255.0f ,
                                           color_actual.getAlpha() / 255.0f };

                    elements.push_back(PrimTriangle);
                    textures.push_back(font.texture);
                    elements.push_back(PrimTriangle);
                    textures.push_back(font.texture);

                    unsigned int indice = vertices.size();

                    indices.push_back(indice);
                    indices.push_back(indice + 1);
                    indices.push_back(indice + 2);
                    indices.push_back(indice);
                    indices.push_back(indice + 2);
                    indices.push_back(indice + 3);

                    // Premier point
                    vertices.push_back(TVector2F(x, y));

                    coords.push_back(0.0625f * ((c % 16)) + offset);
                    coords.push_back(0.0625f * ((c / 16)) + offset);

                    // Deuxième point
                    vertices.push_back(TVector2F(x + params.size * perX2, y));

                    coords.push_back(0.0625f * ((c % 16) + perX2) - offset);
                    coords.push_back(0.0625f * ((c / 16)) + offset);

                    // Troisième point
                    vertices.push_back(TVector2F(x + params.size * perX2, y + params.size * perY2));

                    coords.push_back(0.0625f * ((c % 16) + perX2) - offset);
                    coords.push_back(0.0625f * ((c / 16) + perY2) - offset);

                    // Quatrième point
                    vertices.push_back(TVector2F(x, y + params.size * perY2));

                    coords.push_back(0.0625f * ((c % 16)) + offset);
                    coords.push_back(0.0625f * ((c / 16) + perY2) - offset);

                    for (int i = 0; i < 4; ++i)
                    {
                        colors.push_back(tmp_color[0]);
                        colors.push_back(tmp_color[1]);
                        colors.push_back(tmp_color[2]);
                        colors.push_back(tmp_color[3]);
                    }
                }

                break;
        }


        // Incrémentation de la position
        x += font.size[c].X * ratio;
    }

#endif
}


/**
 * Renvoie les dimensions d'une chaîne de caractère en pixels.
 *
 * \param params Paramètres du texte à afficher.
 * \return Dimensions du texte en pixels.
 ******************************/

TVector2UI CFontManager::getTextSize(const TTextParams& params)
{
    if (params.text.isEmpty() || params.font >= m_fonts.size())
    {
        return Origin2UI;
    }

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    return font->getTextSize(params);
#else
    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;

    std::vector<float> lengths;
    TVector2F sz(0.0f, static_cast<float>(params.size));

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lengths.push_back(sz.X);
                sz.Y += params.size;
                sz.X = 0.0f;
                break;

            // Tabulation horizontale
            case '\t':
                sz.X += 4 * font.size[32].X * ratio;
                break;

            // Défaut
            default:
                sz.X += font.size[c].X * ratio;
                break;
        }
    }

    // On prend la longueur de la ligne la plus longue
    lengths.push_back(sz.X);
    sz.X = *std::max_element(lengths.begin(), lengths.end());

    return TVector2UI(static_cast<unsigned int>(sz.X), static_cast<unsigned int>(sz.Y));

#endif
}


/**
 * Calcule la largeur de chaque caractère de chaque ligne d'un texte.
 * Seul le texte, la police, et la taille sont utilisés.
 *
 * \todo Tester.
 *
 * \param params Paramètres du texte à afficher.
 * \param lines  Structure qui contiendra la largeur de chaque caractère de
 *               chaque ligne du texte.
 ******************************/

void CFontManager::getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines)
{
    lines.clear();

    if (params.text.isEmpty() || params.font >= m_fonts.size())
    {
        return;
    }

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->getTextLinesSize(params, lines);
#else

    TTextLine currentLine;

    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;
    unsigned int offset = 0;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.cbegin(); it != params.text.cend(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lines.push_back(currentLine);
                currentLine.len.clear();
                currentLine.offset = ++offset;
                break;

            // Tabulation horizontale
            case '\t':
                ++offset;
                currentLine.len.push_back(4.0f * font.size[32].X * ratio);
                break;

            // Défaut
            default:
                ++offset;
                currentLine.len.push_back(font.size[c].X * ratio);
                break;
        }
    }

    lines.push_back(currentLine);
#endif
}


/**
 * Calcule la largeur de chaque caractère d'une ligne de texte.
 * Le calcul s'arrête si un retour à la ligne est rencontré.
 * Seul le texte, la police, et la taille sont utilisés.
 *
 * \todo Tester.
 *
 * \param params Paramètres du texte à afficher.
 * \return Largeur de chaque caractère de la ligne.
 ******************************/

TTextLine CFontManager::getTextLineSize(const TTextParams& params)
{
    if (params.text.isEmpty())
        return TTextLine();

    if (params.font >= m_fonts.size())
        return TTextLine();

    TTextLine line;

    // Récupération de la police à utiliser
#ifdef T_USE_FREETYPE
    CFont * font = m_fonts[params.font];
    font->getTextLineSize(params, line);
#else
    const TFont& font = m_fonts[params.font];

    const float ratio = params.size * 16.0f / font.width;

    std::vector<float> lengths;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator it = params.text.begin(); it != params.text.end(); ++it)
    {
        unsigned char c = it->toLatin1();

        switch (c)
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\r':
            case '\n':
                return line;

            // Tabulation horizontale
            case '\t':
                line.len.push_back(4.0f * font.size[32].X * ratio);
                break;

            // Défaut
            default:
                line.len.push_back(font.size[c].X * ratio);
                break;
        }
    }
#endif

    return line;
}


/**
 * Inscrit la liste des polices dans le log.
 ******************************/

void CFontManager::logFontList() const
{
    CString txt("Liste des polices :");
    //CApplication::getApp()->log("\nListe des polices :");

    // On parcourt le tableau des textures
    for (TFontVector::const_iterator it = m_fonts.begin(); it != m_fonts.end(); ++it)
    {
#ifdef T_USE_FREETYPE
        txt += CString("\n - ") + (*it)->getFontName();
#else
        txt += CString("\n - %1 (texture %2, %3 x %4)").arg(it->name).arg(it->texture).arg(it->width).arg(it->height);
        //CApplication::getApp()->log(CString(" - %1 (texture %2, %3 x %4)").arg(it->name).arg(it->texture).arg(it->width).arg(it->height));
#endif
    }

    CApplication::getApp()->log(txt);
    //CApplication::getApp()->log("\n");
}


/**
 * Charge une police de caractère.
 *
 * \param name     Nom de la police.
 * \param fileName Adresse du fichier contenant la police à charger.
 * \return Identifiant de la police
 ******************************/

TFontId CFontManager::privLoadFont(const CString& name, const CString& fileName)
{
    CApplication::getApp()->log(CString::fromUTF8("Chargement de la police de caractère %1").arg(name));


#ifdef T_USE_FREETYPE

    CFont * font = new CFont();

    if (!font->loadFromFile(fileName))
    {
        delete font;
        CApplication::getApp()->log(CString("CFontManager::privLoadFont : impossible de charger le fichier de police \"%1.ttf\"").arg(name), ILogger::Error);
        return InvalidFont;
    }

#else

    CLoaderTedFont loader;

    if (!loader.loadFromFile(fileName))
    {
        CApplication::getApp()->log(CString("CFontManager::privLoadFont : impossible de charger le fichier de police \"%1.tef\"").arg(name), ILogger::Error);
        return InvalidFont;
    }

    TFont font;
    font.name    = name;
    font.texture = Game::textureManager->loadTexture(":font:" + name, loader.getImage(), CTextureManager::FilterNone);
    font.width   = loader.getImage().getWidth();
    font.height  = loader.getImage().getHeight();

    CLoaderTedFont::TCharSize sizes[256];

    loader.getCharSizes(sizes);

    // Dimensions des caractères
    for (int i = 0; i < 256; ++i)
    {
        font.size[i].X = static_cast<unsigned int>(sizes[i].width);
        font.size[i].Y = static_cast<unsigned int>(sizes[i].height);
    }

#endif

    TFontId identifiant = m_fonts.size();
    m_fonts.push_back(font);
    return identifiant;
}

} // Namespace Ted

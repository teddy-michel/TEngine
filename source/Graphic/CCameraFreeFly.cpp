/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CCameraFreeFly.cpp
 * \date       2008 Création de la classe CCameraFreeFly.
 * \date 11/07/2010 On peut modifier le rayon de la sphère englobante.
 * \date 20/07/2010 Utilisation des assertions et des codes clavier internes.
 * \date 17/03/2011 Création de la méthode setKey.
 *                  Ajout des mouvements de rotation à gauche ou à droite.
 * \date 03/04/2011 Utilisation des touches et actions de la classe CApplication.
 * \date 19/10/2013 Ajout de la méthode anglesFromVectors.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <cmath>

#include "Graphic/CCameraFreeFly.hpp"
#include "Core/CApplication.hpp"

#ifdef T_CAMERA_FREEFLY_COLLISION
#  include "Physic/CPhysicEngine.hpp"
#  include "Physic/CBoundingSphere.hpp"
#endif

// DEBUG
#include "Core/Exceptions.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

const float CameraFreeFlyVelocity = 0.4f;    ///< Vitesse de déplacement de la caméra.
const unsigned int VerticalMotionTime = 250; ///< Durée de déplacement vertical en millisecondes (molette).
const float PiOver180 = 0.0174532925f;       ///< Pi sur 180.


/**
 * Constructeur par défaut.
 *
 * \todo Enlever le volume.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

CCameraFreeFly::CCameraFreeFly(const TVector3F& position, const TVector3F& direction) :
ICamera                   (position, direction),
m_forward                 (Origin3F),
m_left                    (Origin3F),
m_theta                   (0.0f),
m_phi                     (0.0f),
m_timeVerticalMotion      (0),
m_verticalMotionDirection (0)
#ifdef T_CAMERA_FREEFLY_COLLISION
,m_collisions             (true),
m_sphere                  (nullptr)
#endif
{
    anglesFromVectors();

#ifdef T_CAMERA_FREEFLY_COLLISION
    m_sphere = new CBoundingSphere(m_position, 16.0f);
    T_ASSERT(m_sphere != nullptr);
#endif
}


/**
 * Destructeur. Supprime la sphère englobante.
 ******************************/

CCameraFreeFly::~CCameraFreeFly()
{
#ifdef T_CAMERA_FREEFLY_COLLISION
    delete m_sphere;
#endif
}


/**
 * Accesseur pour theta.
 *
 * \return Angle Theta.
 ******************************/

float CCameraFreeFly::getTheta() const
{
    return m_theta;
}


/**
 * Accesseur pour phi.
 *
 * \return Angle Phi.
 ******************************/

float CCameraFreeFly::getPhi() const
{
    return m_phi;
}


/**
 * Modifie la direction de la caméra.
 *
 * \param direction Nouvelle direction.
 ******************************/

void CCameraFreeFly::setDirection(const TVector3F& direction)
{
    m_direction = direction;
    anglesFromVectors();
}

#ifdef T_CAMERA_FREEFLY_COLLISION

/**
 * Accesseur pour collisions.
 *
 * \return La caméra ne peut pas traverser les murs.
 *
 * \sa CCameraFreeFly::setCollisions
 ******************************/

bool CCameraFreeFly::isCollisions() const
{
    return m_collisions;
}

/**
 * Mutateur pour collisions.
 *
 * \param collisions La caméra ne peut pas traverser les murs.
 *
 * \sa CCameraFreeFly::isCollisions
 ******************************/

void CCameraFreeFly::setCollisions(bool collisions)
{
    m_collisions = collisions;
}

/**
 * Modifie le rayon de la sphère englobante.
 *
 * \param radius Rayon de la sphère englobante.
 ******************************/

void CCameraFreeFly::setSphereRadius(float radius)
{
    T_ASSERT(m_sphere != nullptr);
    m_sphere->setRadius(radius);
}

#endif

/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CCameraFreeFly::onEvent(const CMouseEvent& event)
{
    m_theta -= event.xrel * MOUSE_MOTION_SPEED;
    m_phi   -= event.yrel * MOUSE_MOTION_SPEED;
    vectorsFromAngles();

    if (event.type == ButtonPressed || event.type == ButtonReleased)
    {
        // Coup de molette vers le haut
        if (event.button == MouseWheelUp)
        {
            m_timeVerticalMotion = VerticalMotionTime;
            m_verticalMotionDirection = 1;

        }
        // Coup de molette vers le bas
        else if (event.button == MouseWheelDown)
        {
            m_timeVerticalMotion = VerticalMotionTime;
            m_verticalMotionDirection = -1;
        }
    }
}


/**
 * Déplace la caméra.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CCameraFreeFly::animate(unsigned int frameTime)
{
    TVector3F new_position = m_position;

    // Déplacement vers l'avant
    if (CApplication::getApp()->isActionActive(ActionForward))
    {
        new_position += m_forward * CameraFreeFlyVelocity * frameTime;
    }

    // Déplacement vers l'arrière
    if (CApplication::getApp()->isActionActive(ActionBackward))
    {
        new_position -= m_forward * CameraFreeFlyVelocity * frameTime;
    }

    // Déplacement vers la gauche
    if (CApplication::getApp()->isActionActive(ActionStrafeLeft))
    {
        new_position += m_left * CameraFreeFlyVelocity * frameTime;
    }

    // Déplacement vers la droite
    if (CApplication::getApp()->isActionActive(ActionStrafeRight))
    {
        new_position -= m_left * CameraFreeFlyVelocity * frameTime;
    }

    // Rotation vers la gauche
    if (CApplication::getApp()->isActionActive(ActionTurnLeft))
    {
        m_theta += frameTime * MOUSE_MOTION_SPEED;
        vectorsFromAngles();
    }

    // Rotation vers la droite
    if (CApplication::getApp()->isActionActive(ActionTurnRight))
    {
        m_theta -= frameTime * MOUSE_MOTION_SPEED;
        vectorsFromAngles();
    }

    // Déplacement vertical
    if (m_verticalMotionDirection)
    {
        new_position += TVector3F(0.0f, 0.0f, m_verticalMotionDirection * CameraFreeFlyVelocity * frameTime);

        if (frameTime > m_timeVerticalMotion)
        {
            m_verticalMotionDirection = 0;
        }
        else
        {
            m_timeVerticalMotion -= frameTime;
        }
    }

    // Zoom
    if (CApplication::getApp()->isActionActive(ActionZoom))
    {
        if (m_zoom > frameTime)
        {
            m_zoom -= frameTime;
        }
        else if (m_zoom > 0)
        {
            m_zoom = 0;
        }
    }
    else
    {
        if (m_zoom + frameTime < ZOOM_TIME)
        {
            m_zoom += frameTime;
        }
        else if (m_zoom < ZOOM_TIME)
        {
            m_zoom = ZOOM_TIME;
        }
    }

    // La caméra n'a pas bougé
    if (m_position == new_position)
    {
        return;
    }

#ifdef T_CAMERA_FREEFLY_COLLISION

    T_ASSERT(m_sphere != nullptr);

    if (m_collisions)
    {
        // Déplacement du volume englobant
        CPhysicEngine::Instance().slide(*m_sphere, new_position - m_position);
        m_position = m_sphere->getPosition();
    }
    else
#endif
    {
        m_position = new_position;
#ifdef T_CAMERA_FREEFLY_COLLISION
        m_sphere->setPosition(m_position);
#endif
    }

    m_direction = m_position + m_forward;
}


/**
 * Convertit les angles en direction.
 ******************************/

void CCameraFreeFly::vectorsFromAngles()
{
    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    double r_temp = cos(m_phi * PiOver180);
    m_forward.Z = sin(m_phi * PiOver180);
    m_forward.X = r_temp * cos(m_theta * PiOver180);
    m_forward.Y = r_temp * sin(m_theta * PiOver180);

    m_left = VectorCross(TVector3F(0.0f, 0.0f, 1.0f), m_forward);
    m_left.normalize();

    m_direction = m_position + m_forward;
}


/**
 * Convertit la position et la direction en angles.
 ******************************/

void CCameraFreeFly::anglesFromVectors()
{
    m_forward = (m_direction - m_position).getNormalized();
    m_left = VectorCross(TVector3F(0.0f, 0.0f, 1.0f), m_forward);
    m_left.normalize();
    m_phi = asin(m_forward.Z) / PiOver180;

    if (std::abs(m_forward.X) < std::numeric_limits<float>::epsilon())
    {
        m_theta = 90;
    }
    else
    {
        m_theta = atan(m_forward.Y / m_forward.X) / PiOver180;
    }

    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    m_direction = m_position + m_forward;
}


/**
 * Initialise les paramètres de la caméra.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

void CCameraFreeFly::initialize(const TVector3F& position, const TVector3F& direction)
{
#ifdef T_CAMERA_FREEFLY_COLLISION
    T_ASSERT(m_sphere != nullptr);
#endif

    m_position = position;
    m_direction = direction;
    m_forward = Origin3F;
    m_left = Origin3F;
    m_theta = 0.0f;
    m_phi = 0.0f;
    m_timeVerticalMotion = 0;
    m_verticalMotionDirection = 0;
    m_zoom = ZOOM_TIME;
#ifdef T_CAMERA_FREEFLY_COLLISION
    m_sphere->setPosition(m_position);
#endif

    anglesFromVectors();
}

} // Namespace Ted

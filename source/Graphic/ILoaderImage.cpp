/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/ILoaderImage.cpp
 * \date       2008 Création de la classe ILoaderImage.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/ILoaderImage.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

ILoaderImage::ILoaderImage() :
ILoader()
{ }


/**
 * Accesseur pour width.
 *
 * \return Largeur de l'image en pixels.
 ******************************/

unsigned int ILoaderImage::getWidth() const
{
    return m_width;
}


/**
 * Accesseur pour height.
 *
 * \return Hauteur de l'image en pixels.
 ******************************/

unsigned int ILoaderImage::getHeight() const
{
    return m_height;
}


/**
 * Retourne l'image lue dans le fichier.
 *
 * \return Image.
 ******************************/

CImage ILoaderImage::getImage()
{
    return CImage(m_width, m_height, reinterpret_cast<CColor *>(&m_pixels[0]));
}

} // Namespace Ted

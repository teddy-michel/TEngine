/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CTextureManager.cpp
 * \date       2008 Création de la classe CTextureManager.
 * \date 11/07/2010 Les messages d'erreur sont envoyés à la console.
 * \date 11/07/2010 Suppression de la méthode pour charger une image en précisant la transparence.
 * \date 13/07/2010 Les méthodes BindTexture et BindTextures sont déclarées constantes.
 * \date 14/07/2010 Création de la méthode ReloadTexture.
 * \date 15/07/2010 La méthode ReloadTexture est constante et renvoie un booléen.
 * \date 15/07/2010 Le format des images est désormais le RGBA.
 * \date 17/11/2010 Fusion des méthodes BindTexture et BindTextures en une seule.
 * \date 30/03/2011 On ne peut plus supprimer les deux premières textures.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 08/12/2011 Les fonctions OpenGL sont appelées dans la méthode doPendingTasks.
 * \date 16/03/2013 La génération des mipmaps ne passe plus par Glu.
 * \date 05/04/2013 La méthode unloadTextures peut être appelée dans n'importe quel thread.
 * \date 05/04/2013 Les dimensions de chaque texture sont gardées en mémoire.
 * \date 04/06/2014 Les textures sont recherchées dans la liste des répertoires du gestionnaire.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/CTextureManager.hpp"
#include "Graphic/CColor.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"

// DEBUG
#include "Core/Allocation.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

namespace Game
{
    CTextureManager * textureManager = nullptr;
}


/**
 * Constructeur par défaut.
 ******************************/

CTextureManager::CTextureManager() :
IResourceManager ()
{
    if (Game::textureManager)
        exit(-1);

    init();
    Game::textureManager = this;
}


/**
 * Destructeur.
 ******************************/

CTextureManager::~CTextureManager()
{
    Game::textureManager = nullptr;
    unloadTextures();
}


/**
 * Retourne l'identifiant de la texture à partir de son nom.
 *
 * \param name Nom de la texture.
 * \return Identifiant de la texture si elle a été chargée, sinon 0.
 ******************************/

TTextureId CTextureManager::getTextureId(const CString& name) const
{
    sf::Lock lock(m_mutex);

    for (TTextureInfoVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        if (it->name == name)
        {
            return std::distance(m_textures.begin(), it);
        }
    }

    return 0;
}


/**
 * Retourne le nom de la texture associée à un identifiant.
 *
 * \param id Identifiant de la texture.
 * \return Nom de la texture ou chaine vide si la texture n'existe pas.
 ******************************/

CString CTextureManager::getTextureName(TTextureId id) const
{
    sf::Lock lock(m_mutex);

    if (id < m_textures.size())
    {
        return m_textures[id].name;
    }

    // La texture n'a pas été trouvée
    return CString();
}


/**
 * Donne la taille totale occupée par les textures.
 * Cela concerne uniquement la mémoire vive, et pas la mémoire vidéo.
 *
 * \return Taille totale utilisée pour les textures.
 ******************************/

unsigned int CTextureManager::getMemorySize() const
{
    unsigned int s = 0;

    for (TTextureInfoVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        s += it->width * it->height * 4;
        s += sizeof(TTextureInfo);
    }

    return s;
}


unsigned int CTextureManager::getNumElements() const
{
    return m_textures.size();
}


/**
 * Initialise le gestionnaire de texture et crée la texture par défaut (échiquier).
 * Cette méthode doit être appelée dans le thread principal.
 ******************************/

void CTextureManager::init()
{
    sf::Lock lock(m_mutex);

    CApplication::getApp()->log("Initialisation du gestionnaire de textures");
    m_textures.reserve(10);

    float maxanis = 0;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxanis);
    CApplication::getApp()->log(CString("Filtrage anisotropique maximal : %1").arg(maxanis));

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        m_texture[i] = 0;
    }


    // Création et initialisation de la texture par défaut
    GLuint idgl1;
    glGenTextures(1, &idgl1);

    glBindTexture(GL_TEXTURE_2D, idgl1);
    m_texture[0] = 1;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    GLubyte * checker = new GLubyte[4 * 64 * 64];

    // Construction de l'échiquier
    for (unsigned int i = 0; i < 64; ++i)
    {
        for (unsigned int j = 0; j < 64; ++j)
        {
            int c = (!(i & 8) ^ !(j & 8)) * 0xFF;

//#ifdef T_BIG_ENDIAN
            checker[(i * 256) + (j * 4) + 0] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 1] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 2] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 3] = static_cast<unsigned char>(0xFF);
/*
#else
            checker[(i * 256) + (j * 4) + 0] = static_cast<unsigned char>(0xFF);
            checker[(i * 256) + (j * 4) + 1] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 2] = static_cast<unsigned char>(c);
            checker[(i * 256) + (j * 4) + 3] = static_cast<unsigned char>(c);
#endif
*/
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, checker);
    delete[] checker;


    // Ajout à la base de données
    TTextureInfo texture_info0(0, CString(), FilterNone);
    m_textures.push_back(texture_info0);

    TTextureInfo texture_info1(idgl1, "default",  FilterNone);
    texture_info1.width  = 64;
    texture_info1.height = 64;
    m_textures.push_back(texture_info1);

    CApplication::getApp()->log(CString::fromUTF8("Création de la texture par défaut (1)"));
}


/**
 * Charge une texture à partir d'un fichier image.
 *
 * Vérifie si la texture n'est pas déjà stockée en mémoire. Si c'est
 * le cas, la fonction retourne l'identifiant de la texture déjà stockée.
 * Sinon, elle retourne l'identifiant de la nouvelle texture.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param fileName Adresse du fichier contenant la texture.
 * \param filter   Filtrage à utiliser.
 * \param repeat   Indique si la texture doit se répéter sur les bords (true par défaut).
 * \return Identifiant de la texture.
 *
 * \todo Utiliser la liste des répertoires.
 ******************************/

TTextureId CTextureManager::loadTexture(const CString& fileName, TFilter filter, bool repeat)
{
    sf::Lock lock(m_mutex);

    TTextureId identifiant = DefaultTexture;
    bool exist = false;

    // On cherche si la texture existe déjà
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture existe
        if (it->name == fileName)
        {
            identifiant = std::distance(m_textures.begin(), it);

            // La texture n'est pas chargée
            if (it->id == 0)
            {
                exist = true;
                break;
            }

            return identifiant;
        }
    }

    bool loaded = false;

    // Recherche de l'image dans chaque répertoire du gestionnaire
    for (std::list<CString>::const_iterator it = m_paths.begin(); it != m_paths.end(); ++it)
    {
        CImage image;

        // On tente de charger la texture
        if (image.loadFromFile(*it + fileName))
        {
            // Création d'une nouvelle texture et ajout à la liste
            if (exist)
            {
                m_textures[identifiant].filter = filter;
                m_textures[identifiant].repeat = repeat;
                m_textures[identifiant].action = TextureActionLoad;
                m_textures[identifiant].image  = image;
                m_textures[identifiant].width  = image.getWidth();
                m_textures[identifiant].height = image.getHeight();
            }
            else
            {
                identifiant = m_textures.size();

                TTextureInfo texture_info;

                texture_info.id     = 0;
                texture_info.name   = fileName;
                texture_info.filter = filter;
                texture_info.repeat = repeat;
                texture_info.action = TextureActionLoad;
                texture_info.image  = image;
                texture_info.width  = image.getWidth();
                texture_info.height = image.getHeight();

                m_textures.push_back(texture_info);
            }

            loaded = true;
            break;
        }
    }

    // Impossible de charger la texture, texture par défault utilisée
    if (!loaded)
    {
        CApplication::getApp()->log(CString::fromUTF8("Impossible de créer la texture %1").arg(fileName), ILogger::Error);
    }

    return identifiant;
}


/**
 * Crée une texture depuis une image déjà chargée.
 *
 * Vérifie si la texture n'est pas déjà stockée en mémoire. Si c'est le cas, la
 * fonction retourne l'id de la texture déjà stockée. Sinon, elle retourne
 * l'identifiant de la nouvelle texture.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param name   Nom de la texture.
 * \param image  Image à utiliser pour créer la texture.
 * \param filter Filtrage à utiliser.
 * \param repeat Indique si la texture doit se répéter sur les bords (true par défaut).
 * \return Identifiant de la texture.
 ******************************/

TTextureId CTextureManager::loadTexture(const CString& name, const CImage& image, TFilter filter, bool repeat)
{
    sf::Lock lock(m_mutex);

    TTextureId identifiant = DefaultTexture;
    bool exist = false;

    // On cherche si la texture existe déjà
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture existe
        if (it->name == name)
        {
            identifiant = std::distance(m_textures.begin(), it);

            // La texture n'est pas chargée
            if (it->id == 0)
            {
                exist = true;
                break;
            }

            return identifiant;
        }
    }

    if (identifiant < DefaultTexture)
        return identifiant;

    // Création d'une nouvelle texture et ajout à la liste
    if (exist)
    {
        m_textures[identifiant].filter = filter;
        m_textures[identifiant].repeat = repeat;
        m_textures[identifiant].action = TextureActionLoad;
    }
    else
    {
        identifiant = m_textures.size();

        TTextureInfo texture_info;

        texture_info.id     = 0;
        texture_info.name   = name;
        texture_info.filter = filter;
        texture_info.repeat = repeat;
        texture_info.action = TextureActionLoad;

        m_textures.push_back(texture_info);
    }

    m_textures[identifiant].image  = image;
    m_textures[identifiant].width  = image.getWidth();
    m_textures[identifiant].height = image.getHeight();

    return identifiant;
}


/**
 * Met-à-jour une texture à partir d'une image déjà chargée.
 *
 * Cette fonction peut être appelée dans n'importe quel thread, le chargement
 * effectif de la texture dans la carte graphique se faisant dans le thread
 * principal de l'application.
 *
 * \param id    Identifiant de la texture.
 * \param image Image à utiliser pour créer la texture.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CTextureManager::reloadTexture(TTextureId id, const CImage& image)
{
    sf::Lock lock(m_mutex);

    if (id <= DefaultTexture || id >= m_textures.size())
        return false;

    m_textures[id].action = TextureActionLoad;
    m_textures[id].image  = image;
    m_textures[id].width  = image.getWidth();
    m_textures[id].height = image.getHeight();

    return true;
}


/**
 * Supprime toutes les textures.
 ******************************/

void CTextureManager::unloadTextures()
{
    sf::Lock lock(m_mutex);

    CApplication::getApp()->log(CString::fromUTF8("Suppression des textures : %1 textures supprimées").arg(m_textures.size()));

    // Suppression des textures OpenGL
    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        it->action = TextureActionDelete;
        //glDeleteTextures(1, &(it->id));
        //it->id = 0;
    }

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        m_texture[i] = NoTexture;
    }
}


/**
 * Supprime une texture à partir de son nom.
 *
 * \param name Nom de la texture.
 ******************************/

void CTextureManager::deleteTexture(const CString& name)
{
    sf::Lock lock(m_mutex);

    TTextureId id = NoTexture;

    for (TTextureInfoVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it, ++id)
    {
        if (it->name == name && id > DefaultTexture)
        {
            it->action = TextureActionDelete;
        }
    }
}


/**
 * Supprime une texture à partir de son identifiant.
 *
 * \param id Identifiant de la texture.
 ******************************/

void CTextureManager::deleteTexture(TTextureId id)
{
    if (id > DefaultTexture)
    {
        sf::Lock lock(m_mutex);

        TTextureInfoVector::iterator it = m_textures.begin();
        std::advance(it, id);

        if (it != m_textures.end())
        {
            it->action = TextureActionDelete;
        }
    }
}


/**
 * Change la texture active sur une unité.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \param texture Identifiant de la texture.
 * \param unit    Unité de texture à utiliser (0 par défaut).
 ******************************/

void CTextureManager::bindTexture(TTextureId texture, unsigned int unit) const
{
    T_ASSERT(unit < NumUnit);

    sf::Lock lock(m_mutex);

    if (m_texture[unit] != texture)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB + unit);

        if (texture < m_textures.size())
        {
            glBindTexture(GL_TEXTURE_2D, m_textures[texture].id);
#ifdef T_TEXTURES_KEEP_TIME
            m_textures[texture].time = getElapsedTime();
#endif
            m_texture[unit] = texture;
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, m_textures[DefaultTexture].id);
#ifdef T_TEXTURES_KEEP_TIME
            m_textures[DefaultTexture].time = getElapsedTime();
#endif
            m_texture[unit] = DefaultTexture;
        }
    }
}


/**
 * Inscrit la liste des textures dans le log.
 ******************************/

void CTextureManager::logTexturesList() const
{
    sf::Lock lock(m_mutex);

    CApplication::getApp()->log("\nListe des textures :");

    // On parcourt le tableau des textures
    for (unsigned int i = 0; i < m_textures.size(); ++i)
    {
        CApplication::getApp()->log(CString(" - %1 : %2 (%3)").arg(i).arg(m_textures[i].name).arg(m_textures[i].id));
    }

    CApplication::getApp()->log("\n");
}


/**
 * Effectue les tâches en attente (chargement ou déchargement d'une texture).
 * Cette méthode doit être appelée régulièrement dans le thread principal.
 *
 * \todo Limiter le nombre de chargements à chaque appel.
 ******************************/

void CTextureManager::doPendingTasks()
{
    sf::Lock lock(m_mutex);

    for (TTextureId id = 0; id < m_textures.size(); ++id)
    {
        switch (m_textures[id].action)
        {
            default: continue;

            case TextureActionLoad:
                PrivLoadTexture(id, m_textures[id]);
                break;

            case TextureActionDelete:
                PrivDeleteTexture(id, m_textures[id]);
                break;
        }
    }
}


/**
 * Charge une texture dans la carte graphique.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \todo Supprimer les données de l'image une fois celle-ci chargée en mémoire.
 *
 * \param id      Identifiant de la texture.
 * \param texture Structure contenant les informations de la texture.
 ******************************/

void CTextureManager::PrivLoadTexture(TTextureId id, TTextureInfo& texture)
{
    T_ASSERT(id < m_textures.size());

    GLenum format = GL_RGBA;

    GLuint idgl = texture.id;
    int img_w = texture.image.getWidth();
    int img_h = texture.image.getHeight();

    // La texture a été supprimée de la carte graphique, on recrée un identifiant OpenGL
    if (texture.id == 0)
    {
        glGenTextures(1, &idgl);
    }

    // Initialisation de la texture OpenGL
    glBindTexture(GL_TEXTURE_2D, idgl);
    m_texture[0] = id;

    // Répétition de la texture sur les bords
    if (texture.repeat)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    // Filtrage
    switch (texture.filter)
    {
        case FilterNone:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            break;

        case FilterBilinear:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            break;

        case FilterBilinearMipmap:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            break;

        case FilterTrilinear:
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            break;
    }

    // Mip-mapping
    if (texture.filter ==  FilterBilinearMipmap|| texture.filter == FilterTrilinear)
    {
#if defined(GL_ARB_framebuffer_object) // OpenGL > 3.0 : glGenerateMipmap
//#   if defined(GL_ARB_texture_storage) //glTexStorage2D
//        glTexStorage2D(GL_TEXTURE_2D, 2, GL_RGBA8UI, img_w, img_h);
//        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, img_w, img_h, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
//#   else
        glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
//#   endif
        glGenerateMipmap(GL_TEXTURE_2D);
#elif defined(GL_VERSION_1_4) // OpenGL > 1.4 : GL_GENERATE_MIPMAP
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
        glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
#else
        // On utilise gluBuild2DMipmaps si on n'a vraiment pas le choix...
        if (int error = gluBuild2DMipmaps(GL_TEXTURE_2D, format, img_w, img_h, format, GL_UNSIGNED_BYTE, texture.image.getPixels()))
        {
            CApplication::getApp()->log(CString::fromUTF8("Erreur lors de la création des niveaux de détail de la texture : %1").arg(gluErrorString(error)), ILogger::Error);
        }
#endif
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, format, img_w, img_h, 0, format, GL_UNSIGNED_BYTE, texture.image.getPixels());
    }

    // Filtrage anisotropique
    float anis = Game::renderer->getAnisotropic();

    if (anis > 1.0f)
    {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anis);
    }

    if (texture.id == 0)
    {
        CApplication::getApp()->log(CString::fromUTF8("Création de la texture %1 (%2) : %3 x %4").arg(texture.name).arg(id).arg(img_w).arg(img_h));
    }

    texture.id     = idgl;
    texture.action = TextureActionNone;
  //texture.image  = CImage();
  //texture.width  = img_w;
  //texture.height = img_h;
}


/**
 * Efface les données d'une texture dans la carte graphique.
 * Cette méthode doit être appelée dans le thread principal.
 *
 * \param id      Identifiant de la texture.
 * \param texture Structure contenant les informations de la texture.
 ******************************/

void CTextureManager::PrivDeleteTexture(TTextureId id, TTextureInfo& texture)
{
    T_ASSERT(id < m_textures.size());
    T_ASSERT(id != NoTexture);
    T_ASSERT(id != DefaultTexture);

    for (unsigned int i = 0; i < NumUnit; ++i)
    {
        if (m_texture[i] == texture.id)
        {
            glBindTexture(GL_TEXTURE_2D, m_textures[NoTexture].id);
            m_texture[i] = NoTexture;
        }
    }

    glDeleteTextures(1, &(texture.id));

    texture.id     = 0;
    texture.action = TextureActionNone;
  //texture.width  = 0;
  //texture.height = 0;

    CApplication::getApp()->log(CString("Suppression de la texture %1 (%2)").arg(texture.name).arg(id));
}

} // Namespace Ted

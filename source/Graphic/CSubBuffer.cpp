/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CSubBuffer.cpp
 * \date 09/02/2010 Création de la classe CSubBuffer.
 * \date 07/12/2010 Généralisation des méthodes pour modifier les coordonnées de texture.
 * \date 09/12/2010 Suppression des méthodes pour ajouter une seule donnée aux tableaux.
 * \date 15/12/2010 Suppression des méthodes pour modifier ou accéder aux tableaux.
 * \date 15/12/2010 Création des méthodes qui retournent une référence sur un tableau.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CSubBuffer.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CSubBuffer::CSubBuffer()
{ }


/**
 * Constructeur de copie.
 *
 * \param buffer Buffer à copier.
 ******************************/

CSubBuffer::CSubBuffer(const CSubBuffer& buffer)
{
    // Copie des données
    m_indices = buffer.getIndices();
    m_vertices = buffer.getVertices();
    m_normales = buffer.getNormales();
    m_colors = buffer.getColors();
    m_textures = buffer.getTextures();

    for (unsigned short i = 0; i < NumUTUsed; ++i)
    {
        m_coords[i] = buffer.getTextCoords(i);
    }
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence sur le tableau des indices.
 ******************************/

std::vector<unsigned int>& CSubBuffer::getIndices()
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence sur le tableau des sommets.
 ******************************/

std::vector<TVector3F>& CSubBuffer::getVertices()
{
    return m_vertices;
}


/**
 * Retourne le tableau des normales.
 *
 * \return Référence sur le tableau des normales.
 ******************************/

std::vector<TVector3F>& CSubBuffer::getNormales()
{
    return m_normales;
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \param unit Unité de texture à utiliser.
 * \return Référence sur le tableau des coordonnées de texture.
 ******************************/

std::vector<TVector2F>& CSubBuffer::getTextCoords(unsigned short unit)
{
    T_ASSERT(unit < NumUTUsed);

    return m_coords[unit];
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence sur le tableau des couleurs.
 ******************************/

std::vector<float>& CSubBuffer::getColors()
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence sur le tableau des textures.
 ******************************/

TBufferTextureVector& CSubBuffer::getTextures()
{
    return m_textures;
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence constante sur le tableau des indices.
 ******************************/

const std::vector<unsigned int>& CSubBuffer::getIndices() const
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence constante sur le tableau des sommets.
 ******************************/

const std::vector<TVector3F>& CSubBuffer::getVertices() const
{
    return m_vertices;
}


/**
 * Retourne le tableau des normales.
 *
 * \return Référence constante sur le tableau des normales.
 ******************************/

const std::vector<TVector3F>& CSubBuffer::getNormales() const
{
    return m_normales;
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \param unit Unité de texture à utiliser.
 * \return Référence constante sur le tableau des coordonnées de texture.
 ******************************/

const std::vector<TVector2F>& CSubBuffer::getTextCoords(unsigned short unit) const
{
    T_ASSERT(unit < NumUTUsed);

    return m_coords[unit];
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence constante sur le tableau des couleurs.
 ******************************/

const std::vector<float>& CSubBuffer::getColors() const
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence constante sur le tableau des textures.
 ******************************/

const TBufferTextureVector& CSubBuffer::getTextures() const
{
    return m_textures;
}


/**
 * Translate tous les vertices
 *
 * \param vector Vecteur de translation
 ******************************/

void CSubBuffer::translate(const TVector3F& vector)
{
    // On parcourt le tableau des vertices
    for (std::vector<TVector3F>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        (*it) += vector;
    }
}


/**
 * Redimensionne l'ensemble des vertices.
 *
 * \param s Facteur de redimensionnement.
 ******************************/

void CSubBuffer::scale(float s)
{
    // On parcourt le tableau des vertices
    for (std::vector<TVector3F>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        (*it) *= s; // On multiplie chaque composante par le facteur
    }
}


/**
 * Supprime toutes les données.
 ******************************/

void CSubBuffer::clear()
{
    m_indices.clear();
    m_vertices.clear();
    m_normales.clear();
    m_colors.clear();
    m_textures.clear();

    for (unsigned short i = 0; i < NumUTUsed; ++i)
    {
        m_coords[i].clear();
    }
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CSkyBox.cpp
 * \date       2008 Création de la classe CSkyBox.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 08/12/2011 Les paramètres des textures sont gérés uniquement par le gestionnaire de texture.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/CSkyBox.hpp"
#include "Graphic/ICamera.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param size Dimensions de la skybox.
 ******************************/

CSkyBox::CSkyBox(float size) :
m_size     (size),
m_rotation (Origin3F)
{
    CApplication::getApp()->log(CString::fromUTF8("Création de la skybox"));

    for (int i = 0 ; i < 6 ; ++i)
    {
        m_textures[i] = 0;
    }

    // Création des buffers
    glGenBuffersARB(1, &m_vertexBuffer);
    glGenBuffersARB(1, &m_indexBuffer);

    // Coordonnées des vectices
    float vertices[20*3] = {
        -m_size,-m_size,-m_size,
        -m_size, m_size,-m_size,
         m_size, m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size,-m_size, m_size,
        -m_size,-m_size, m_size,
        -m_size,-m_size,-m_size,
         m_size, m_size,-m_size,
         m_size, m_size, m_size,
         m_size,-m_size, m_size,
        -m_size, m_size,-m_size,
        -m_size, m_size, m_size,
         m_size, m_size, m_size,
         m_size, m_size,-m_size,
        -m_size,-m_size, m_size,
        -m_size, m_size, m_size,
        -m_size, m_size,-m_size,
        -m_size, m_size, m_size,
         m_size, m_size, m_size };

    // Coordonnées des textures
    float text_coords[20*2] = {
            1,1, 1,0, 0,0, 0,1,
            1,1, 1,0, 0,0, 0,1,
            1,1, 1,0, 0,0, 1,1,
            1,0, 0,0, 0,1, 1,0,
            0,0, 0,1, 1,1, 0,1 };

    // Indices des vertices
    unsigned int indices[36] = {
             0, 1, 2, 2, 3, 0,
             4, 5, 6, 6, 7, 4,
             8, 9,10,10, 3, 8,
            11,12,13,13,14,11,
             0,15,16,16,17, 0,
            15,18,19,19,10,15 };

    // Remplissage des buffers
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER, 20*3 * sizeof(float) + 20*2 * sizeof(float), 0, GL_STATIC_DRAW);
    glBufferSubDataARB(GL_ARRAY_BUFFER, 0, 20*3 * sizeof(float), &vertices[0]);
    glBufferSubDataARB(GL_ARRAY_BUFFER, 20*3 * sizeof(float), 20*2 * sizeof(float), &text_coords[0]);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
}


/**
 * Destructeur.
 ******************************/

CSkyBox::~CSkyBox()
{
    // Suppression des textures
    for (int i = 0; i < 6; ++i)
    {
        if (m_textures[i] != 0)
        {
            Game::textureManager->deleteTexture(m_textures[i]);
        }
    }

    // Suppression des buffers
    glDeleteBuffersARB(1, &m_vertexBuffer);
    glDeleteBuffersARB(1, &m_indexBuffer);

    CApplication::getApp()->log("Suppression de la skybox");
}


/**
 * Modifie les dimensions du cube de la skybox.
 *
 * \param size Largeur de la skybox.
 *
 * \sa CSkyBox::getSize
 ******************************/

void CSkyBox::setSize(float size)
{
    m_size = size;

    // Coordonnées des vectices
    float vertices[20*3] = {
        -m_size,-m_size,-m_size,
        -m_size, m_size,-m_size,
         m_size, m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size,-m_size,-m_size,
         m_size,-m_size, m_size,
        -m_size,-m_size, m_size,
        -m_size,-m_size,-m_size,
         m_size, m_size,-m_size,
         m_size, m_size, m_size,
         m_size,-m_size, m_size,
        -m_size, m_size,-m_size,
        -m_size, m_size, m_size,
         m_size, m_size, m_size,
         m_size, m_size,-m_size,
        -m_size,-m_size, m_size,
        -m_size, m_size, m_size,
        -m_size, m_size,-m_size,
        -m_size, m_size, m_size,
         m_size, m_size, m_size };

    // Modification du buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferSubDataARB (GL_ARRAY_BUFFER, 0, 20*3 * sizeof(float), &vertices[0]);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
}


/**
 * Modifie la première texture (down).
 *
 * \param fileName Fichier de la première texture (down).
 ******************************/

void CSkyBox::setTextureDown(const CString& fileName)
{
    setTexture(0, fileName);
}


/**
 * Modifie la deuxième texture (north)
 *
 * \param fileName Fichier de la deuxième texture (north).
 ******************************/

void CSkyBox::setTextureNorth(const CString& fileName)
{
    setTexture(1, fileName);
}


/**
 * Modifie la troisième texture (east).
 *
 * \param fileName Fichier de la troisième texture (east).
 ******************************/

void CSkyBox::setTextureEast(const CString& fileName)
{
    setTexture(2, fileName);
}


/**
 * Modifie la quatrième texture (south).
 *
 * \param fileName Fichier de la quatrième texture (south).
 ******************************/

void CSkyBox::setTextureSouth(const CString& fileName)
{
    setTexture(3, fileName);
}


/**
 * Modifie la cinquième texture (west).
 *
 * \param fileName Fichier de la cinquième texture (west).
 ******************************/

void CSkyBox::setTextureWest(const CString& fileName)
{
    setTexture(4, fileName);
}


/**
 * Modifie la sixième texture (up).
 *
 * \param fileName Fichier de la sixième texture (up).
 ******************************/

void CSkyBox::setTextureUp(const CString& fileName)
{
    setTexture(5, fileName);
}


/**
 * Donne la largeur de la skybox.
 *
 * \return Largeur de la skybox.
 *
 * \sa CSkyBox::setSize
 ******************************/

float CSkyBox::getSize() const
{
    return m_size;
}


/**
 * Dessine la skybox.
 *
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

void CSkyBox::draw(TDisplayMode mode) const
{
    // En mode lightmaps, on n'affiche pas la skybox
    if (mode == Lightmaps)
        return;

    // Désactivation du test de profondeur
    glDepthMask(GL_FALSE);

    // On déplacer la caméra
    Game::renderer->pushMatrix(MatrixModelView);
    glLoadIdentity();

    // Rotation du cube
    glRotatef(m_rotation.Z, 0.0f, 0.0f, 1.0f);
    glRotatef(m_rotation.Y, 0.0f, 1.0f, 0.0f);
    glRotatef(m_rotation.X, 1.0f, 0.0f, 0.0f);

    ICamera * camera = Game::renderer->getCamera();
    TVector3F direction = camera->getDirection() - camera->getPosition();

    //gluLookAt(0.0f, 0.0f, 0.0f, direction.X, direction.Y, direction.Z, 0.0f, 0.0f, 1.0f);
    CRenderer::_gluLookAt(Origin3F, TVector3F(direction.X, direction.Y, direction.Z));

    // Buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);

    // Activation des tableaux
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, T_BUFFER_OFFSET(0));
    glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(20 * 3 * sizeof(float)));

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    switch (mode)
    {
        default:
        case Normal:
        case Textured:

            // Pour chaque texture différente
            for (int i = 0; i < 6; ++i)
            {
                Game::textureManager->bindTexture(m_textures[i], 0);
                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, T_BUFFER_OFFSET(sizeof(unsigned int)*6*i));
            }

            break;

        case Wireframe:
        case WireframeCulling:

            Game::textureManager->bindTexture(0, 0);
            glColor4ub(0, 0, 255, 255); // Bleu
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            break;

        case Grey:
        case Colored:
            break;
    }

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Désactivation des tableaux
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    Game::renderer->popMatrix(MatrixModelView);
    glDepthMask(GL_TRUE);
}


/**
 * Modifie une des textures de la SkyBox.
 *
 * \param id       Numéro de la texture à modifier.
 * \param fileName Fichier de la texture.
 ******************************/

void CSkyBox::setTexture(unsigned int id, const CString& fileName)
{
    T_ASSERT(id <= 5);

    // Suppression de l'ancienne texture
    if (m_textures[id] != 0 && m_textures[id] != Game::textureManager->getTextureId("default"))
    {
        Game::textureManager->deleteTexture(m_textures[id]);
    }

    // Chargement de la texture
    m_textures[id] = Game::textureManager->loadTexture(fileName, CTextureManager::FilterTrilinear, false); /// \todo Voir si le filtre a un sens ici
}

} // Namespace Ted

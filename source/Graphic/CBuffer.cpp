/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CBuffer.cpp
 * \date 17/05/2009 Création de la classe CBuffer.
 * \date 23/11/2010 On peut utiliser plus de deux unités de texture.
 * \date 07/12/2010 Suppression des méthodes pour ajouter une seule donnée à un tableau.
 * \date 07/12/2010 Généralisation des méthodes pour modifier les coordonnées de texture.
 * \date 14/12/2010 Suppression des méthodes permettant de modifier ou d'obtenir
 *                  un des tableaux. À la place, on doit utiliser une méthode qui
 *                  retourne une référence sur le tableau.
 * \date 15/12/2010 Ajout des méthodes qui retournent une référence constante sur un tableau.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 01/05/2012 Correction d'un problème d'affichage pour les primitives lignes en mode fil de fer.
 * \date 15/06/2014 Les coordonnées de textures sont stockées dans des TVector2F.
 * \date 06/09/2015 Ajout du mode d'affichage des couleurs uniquement.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/CBuffer.hpp"
#include "Graphic/CColor.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"

// DEBUG
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CBuffer::CBuffer() :
IBuffer       (),
m_numIndices  (0),
m_numVertices (0),
m_numNormales (0),
m_numColors   (0),
m_numTextures (0),
m_primitive   (PrimTriangle)
{
    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        m_numCoords[i] = 0;
    }
}


/**
 * Donne le type de primitive à afficher.
 *
 * \return Type de primitive à afficher.
 *
 * \sa CBuffer::setPrimitiveType
 ******************************/

TPrimitiveType CBuffer::getPrimitiveType() const
{
    return m_primitive;
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence sur le tableau des indices.
 ******************************/

std::vector<unsigned int>& CBuffer::getIndices()
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence sur le tableau des sommets.
 ******************************/

std::vector<TVector3F>& CBuffer::getVertices()
{
    return m_vertices;
}


/**
 * Retourne le tableau des normales.
 *
 * \return Référence sur le tableau des normales.
 ******************************/

std::vector<TVector3F>& CBuffer::getNormales()
{
    return m_normales;
}


/**
 * Retourne le tableau des coordonnées de texture d'une unité de texture.
 *
 * \param unit Unité de texture à utiliser.
 * \return Référence sur le tableau des coordonnées de texture.
 ******************************/

std::vector<TVector2F>& CBuffer::getTextCoords(unsigned short unit)
{
    T_ASSERT(unit < NumUTUsed);
    return m_coords[unit];
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence sur le tableau des couleurs.
 ******************************/

std::vector<float>& CBuffer::getColors()
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence sur le tableau des textures.
 ******************************/

TBufferTextureVector& CBuffer::getTextures()
{
    return m_textures;
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence constante sur le tableau des indices.
 ******************************/

const std::vector<unsigned int>& CBuffer::getIndices() const
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence constante sur le tableau des sommets.
 ******************************/

const std::vector<TVector3F>& CBuffer::getVertices() const
{
    return m_vertices;
}


/**
 * Retourne le tableau des normales.
 *
 * \return Référence constante sur le tableau des normales.
 ******************************/

const std::vector<TVector3F>& CBuffer::getNormales() const
{
    return m_normales;
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \param unit Unité de texture à utiliser.
 * \return Référence constante sur le tableau des coordonnées de texture.
 ******************************/

const std::vector<TVector2F>& CBuffer::getTextCoords(unsigned short unit) const
{
    T_ASSERT(unit < NumUTUsed);

    return m_coords[unit];
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence constante sur le tableau des couleurs.
 ******************************/

const std::vector<float>& CBuffer::getColors() const
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence constante sur le tableau des textures.
 ******************************/

const TBufferTextureVector& CBuffer::getTextures() const
{
    return m_textures;
}


/**
 * Modifie les primitive.
 *
 * \param primitive Type de primitive à afficher.
 *
 * \sa CBuffer::getPrimitiveType
 ******************************/

void CBuffer::setPrimitiveType(TPrimitiveType primitive)
{
    m_primitive = primitive;
}


/**
 * Translate tous les vertices.
 *
 * \param vector Vecteur de translation.
 ******************************/

void CBuffer::translate(const TVector3F& vector)
{
    // On parcourt le tableau des vertices
    for (std::vector<TVector3F>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        (*it) += vector;
    }
}


/**
 * Redimensionne l'ensemble des vertices.
 *
 * \param s Facteur de redimensionnement.
 ******************************/

void CBuffer::scale(float s)
{
    // On parcourt le tableau des vertices
    for (std::vector<TVector3F>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
    {
        (*it) *= s; // On multiplie chaque composante par le facteur
    }
}


/**
 * Supprime toutes les données.
 ******************************/

void CBuffer::clear()
{
    m_indices.clear();
    m_vertices.clear();
    m_normales.clear();
    m_colors.clear();
    m_textures.clear();

    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        m_coords[i].clear();
    }
}


/**
 * Dessine le contenu du buffer.
 * Doit être appelée depuis le thread principal.
 *
 * \return Nombre de triangles dessinés.
 ******************************/

unsigned int CBuffer::draw() const
{
    if (m_vertexBuffer == 0 || m_indexBuffer == 0)
    {
        return 0;
    }

    unsigned int nbr = 0; // Nombre de triangles dessinés
    unsigned int offset = 0;

    TDisplayMode mode = Game::renderer->getMode();


    // Buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);


    // Activation des tableaux de vertices
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
    offset += m_numVertices * sizeof(TVector3F);


    // Activation des tableaux de coordonnées de textures si nécessaire
    if (mode != Wireframe && mode != WireframeCulling && mode != Grey && mode != Colored &&
        m_primitive != PrimLine && m_primitive != PrimPoint &&
        m_numTextures > 0 && m_numCoords[0] > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);

        glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
        offset += m_numCoords[0] * sizeof(TVector2F);

        // Multitexturing
        if (mode == Normal || mode == Lightmaps)
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                if (m_numCoords[i] > 0)
                {
                    glActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glClientActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                    glEnable(GL_TEXTURE_2D);

                    glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
                    offset += m_numCoords[i] * sizeof(TVector2F);
                }
            }
        }
        else
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                offset += m_numCoords[i] * sizeof(TVector2F);
            }
        }
    }
    else
    {
        for (unsigned int i = 0; i < NumUTUsed; ++i)
        {
            offset += m_numCoords[i] * sizeof(TVector2F);
        }
    }

    // Activation des tableaux de couleurs
    if (m_numColors > 0)
    {
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
        offset += m_numColors * sizeof(float);
    }


    // Activation des tableaux de normales
    if (m_numNormales > 0)
    {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
        //offset += m_numNormales * sizeof(float);
    }


    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    switch (mode)
    {
        default:
        case Normal:
        {
            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                // Pour chaque texture différente
                for (TBufferTextureVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
                {
                    Game::textureManager->bindTexture(it->texture[0], 0);

                    // Chargement des textures
                    for (unsigned int i = 1; i < NumUTUsed; ++i)
                    {
                        if (m_numCoords[i] > 0)
                        {
                            Game::textureManager->bindTexture(it->texture[i], i);
                        }
                    }

                    // Affichage des primitives
                    glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));

                    nbr += it->nbr;
                }
            }
            else
            {
                // On n'utilise pas les textures
                Game::textureManager->bindTexture(0, 0);

                // Affichage des primitives
                glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

                nbr += m_numIndices;
            }

            glDisable(GL_CULL_FACE);

            break;
        }

        // Affichage des textures sans les lightmaps
        case Textured:
        {
            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                Game::textureManager->bindTexture(0, 1);

                // Pour chaque texture différente
                for (TBufferTextureVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
                {
                    // Chargement des textures
                    Game::textureManager->bindTexture(it->texture[0], 0);

                    // Affichage des primitives
                    glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));

                    nbr += it->nbr;
                }
            }
            else
            {
                Game::textureManager->bindTexture(0, 0);

                // Affichage des primitives
                glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

                nbr += m_numIndices;
            }

            glDisable(GL_CULL_FACE);

            break;
        }

        // Affichage des lightmaps
        case Lightmaps:
        {
            glEnable(GL_CULL_FACE);

            if (m_numTextures > 0)
            {
                Game::textureManager->bindTexture(0, 0);

                // Pour chaque texture différente
                for (TBufferTextureVector::const_iterator it = m_textures.begin(); it != m_textures.end(); ++it)
                {
                    // Chargement des textures
                    if (m_numCoords[1] > 0)
                    {
                        Game::textureManager->bindTexture(it->texture[1], 1);
                    }
                    else
                    {
                        Game::textureManager->bindTexture(0, 0);
                    }

                    // Affichage des primitives
                    glDrawElements(m_primitive, it->nbr, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));

                    nbr += it->nbr;
                }
            }
            else
            {
                // On n'utilise pas les textures
                Game::textureManager->bindTexture(0, 0);

                // Affichage des primitives
                glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

                nbr += m_numIndices;
            }

            glDisable(GL_CULL_FACE);

            break;
        }

        // Affichage des couleurs
        case Colored:
        {
            glEnable(GL_CULL_FACE);

            // On n'utilise pas les textures
            Game::textureManager->bindTexture(0, 0);

            // Affichage des primitives
            glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

            nbr += m_numIndices;

            glDisable(GL_CULL_FACE);

            break;
        }

        // Affichage des surfaces en gris transparent
        case Grey:
        {
            glEnable(GL_CULL_FACE);

            // On utilise la texture grise
            Game::textureManager->bindTexture(2, 0);

            // Affichage des primitives
            glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

            // Affichage des bordures
            Game::textureManager->bindTexture(0, 0);
            glColor4ub(200, 200, 200, 255);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDrawElements(GL_TRIANGLES, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            glDisable(GL_CULL_FACE);

            nbr += m_numIndices;

            break;
        }

        // Mode fil de fer
        case Wireframe:
        {
            Game::textureManager->bindTexture(0, 0);
            glColor4ub(255, 255, 255, 255);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            // Affichage des triangles
            glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            nbr += m_numIndices;

            break;
        }

        // Mode fil de fer avec culling
        case WireframeCulling:
        {
            glEnable(GL_CULL_FACE);

            Game::textureManager->bindTexture(0, 0);
            glColor4ub(255, 255, 255, 255);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            // Affichage des triangles
            glDrawElements(m_primitive, m_numIndices, GL_UNSIGNED_INT, T_BUFFER_OFFSET(0));

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glDisable(GL_CULL_FACE);

            nbr += m_numIndices;

            break;
        }
    }

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);


    // Désactivation des tableaux de normales
    if (m_numNormales > 0)
    {
        glDisableClientState(GL_NORMAL_ARRAY);
    }

    // Désactivation des tableaux de couleurs
    if (m_numColors > 0)
    {
        glDisableClientState(GL_COLOR_ARRAY);
    }

    // Désactivation des tableaux de coordonnées de textures
    if (mode != Wireframe && mode != WireframeCulling && mode != Grey && mode != Colored &&
        m_primitive != PrimLine && m_primitive != PrimPoint &&
        m_numTextures > 0 && m_numCoords[0] > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        // Multitexturing
        if (mode == Normal || mode == Lightmaps)
        {
            for (unsigned int i = 1; i < NumUTUsed; ++i)
            {
                if (m_numCoords[i] > 0)
                {
                    glActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glClientActiveTextureARB(GL_TEXTURE0_ARB + i);
                    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
                    glDisable(GL_TEXTURE_2D);
                }
            }

            glActiveTextureARB(GL_TEXTURE0_ARB);
            glClientActiveTextureARB(GL_TEXTURE0_ARB);
        }
    }

    // Désactivation des tableaux de vertices
    glDisableClientState(GL_VERTEX_ARRAY);

    if (m_primitive == PrimLine || m_primitive == PrimPoint)
    {
        nbr = 0;
    }

    if (nbr > 0)
    {
        nbr /= 3;

        if (Game::renderer != nullptr)
        {
            Game::renderer->m_numSurfaces += nbr;
        }
    }

    return nbr;
}


/**
 * Met à jour les buffers.
 * Doit être appelée depuis le thread principal.
 ******************************/

void CBuffer::update()
{
    IBuffer::update();

    // Mise-à-jour des dimensions des tableaux
    m_numIndices  = m_indices.size();
    m_numVertices = m_vertices.size();
    m_numNormales = m_normales.size();
    m_numColors   = m_colors.size();
    m_numTextures = m_textures.size();

    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        m_numCoords[i] = m_coords[i].size();
    }


    // Optimisation du tableau de textures
    unsigned int prevTexture = 0; // Identifiant de la dernière texture du tableau
    bool first = false;

    // On parcourt le tableau de textures
    for (TBufferTextureVector::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
    {
        // La texture de cette case est la même que celle de la case précédente, donc on les fusionne
        if (first && it->texture[0] == prevTexture)
        {
            unsigned int nbr = it->nbr; // On conserve le nombre de faces à afficher pour cette texture
            it = m_textures.erase(it); // On supprime la case actuelle du tableau

            // On modifie la case précédente du tableau
            --it;
            it->nbr += nbr;
        }
        else
        {
            prevTexture = it->texture[0];
            first = true;
        }
    }

    // Modification de la taille du tableau des textures
    m_numTextures = m_textures.size();

    // Tailles des tableaux
    const unsigned int sz_vert = m_numVertices * sizeof(TVector3F);
    const unsigned int sz_colo = m_numColors * sizeof(float);
    const unsigned int sz_norm = m_numNormales * sizeof(TVector3F);
    unsigned int sz_total = sz_vert + sz_colo + sz_norm;
    unsigned int sz_txt[NumUTUsed];

    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        sz_txt[i] = m_numCoords[i] * sizeof(TVector2F);
        sz_total += sz_txt[i];
    }


    // Modification du VBO
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER, sz_total, 0, GL_STATIC_DRAW);

    unsigned int sz = 0;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_vert, &m_vertices[0]);
    sz += sz_vert;

    for (unsigned int i = 0; i < NumUTUsed; ++i)
    {
        glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_txt[i], &m_coords[i][0]);
        sz += sz_txt[i];
    }

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_colo, &m_colors[0]);
    sz += sz_colo;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_norm, &m_normales[0]);
    //sz += sz_norm;

    // Modification du IBO
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, m_numIndices * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
}

} // Namespace Ted

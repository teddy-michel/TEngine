/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/ICamera.cpp
 * \date       2008 Création de la classe ICamera.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/ICamera.hpp"
#include "Graphic/CRenderer.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param position  Position de départ.
 * \param direction Direction de départ.
 ******************************/

ICamera::ICamera(const TVector3F& position, const TVector3F& direction) :
m_position  (position),
m_direction (direction),
m_zoom      (ZOOM_TIME),
m_angle     (VIEW_ANGLE)
{ }


/**
 * Destructeur.
 ******************************/

ICamera::~ICamera()
{ }


/**
 * Donne la position de la caméra.
 *
 * \return Position de la caméra.
 *
 * \sa ICamera::setPosition
 ******************************/

TVector3F ICamera::getPosition() const
{
    return m_position;
}


/**
 * Donne la direction de la caméra.
 *
 * \return Direction de la caméra.
 ******************************/

TVector3F ICamera::getDirection() const
{
    return m_direction;
}


/**
 * Niveau de zoom (entre 0 et 1).
 *
 * \return Niveau de zoom.
 ******************************/

float ICamera::getZoom() const
{
    return (static_cast<float>(ZOOM_TIME - m_zoom) / ZOOM_TIME);
}


/**
 * Accesseur pour angle.
 *
 * \return Angle de vue réel (avec zoom).
 ******************************/

unsigned char ICamera::getAngle() const
{
    return (m_angle - (ZOOM_TIME - m_zoom) * (m_angle - ZOOM_ANGLE) / ZOOM_TIME);
}


/**
 * Accesseur pour angle.
 *
 * \return Angle de vue par défaut (sans le zoom).
 ******************************/

unsigned char ICamera::getDefaultAngle() const
{
    return m_angle;
}


/**
 * Modifie la position de la caméra.
 *
 * \param position Nouvelle position.
 *
 * \sa ICamera::getPosition
 ******************************/

void ICamera::setPosition(const TVector3F& position)
{
    m_position = position;
}


/**
 * Modifie la direction de la caméra.
 *
 * \param direction Nouvelle direction.
 ******************************/

void ICamera::setDirection(const TVector3F& direction)
{
    m_direction = direction;
}


/**
 * Modifie le pourcentage de zoom.
 *
 * \param pourcentage Pourcentage de zoom.
 ******************************/

void ICamera::setZoom(float pourcentage)
{
    if (pourcentage < 0.0f) pourcentage = 0.0f;
    if (pourcentage > 1.0f) pourcentage = 1.0f;

    m_zoom = static_cast<unsigned int>((1.0f - pourcentage) * ZOOM_TIME);
}


/**
 * Mutateur pour angle.
 *
 * \param angle Angle de vue.
 ******************************/

void ICamera::setAngle(unsigned char angle)
{
    // L'angle de vue doit être compris entre ces valeurs
    if (angle >= 50 && angle <= 90)
    {
        m_angle = angle;
    }
}


/**
 * Déplace la caméra.
 *
 * \param frameTime Durée écoulée depuis la dernière frame.
 ******************************/

void ICamera::animate(unsigned int frameTime)
{
    T_UNUSED(frameTime);
}


/**
 * Positionne la caméra.
 ******************************/

void ICamera::look() const
{
    CRenderer::_gluLookAt(m_position, m_direction);
}


/**
 * Initialise les paramètres de la caméra.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

void ICamera::initialize(const TVector3F& position, const TVector3F& direction)
{
    m_position = position;
    m_direction = direction;
}

} // Namespace Ted

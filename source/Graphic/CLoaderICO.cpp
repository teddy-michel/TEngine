/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderICO.cpp
 * \date 16/01/2010 Création de la classe CLoaderICO.
 * \date 08/07/2010 Le message d'erreur est envoyé à la console.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Graphic/CLoaderICO.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Utils.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderICO::CLoaderICO() :
ILoaderImage()
{ }


/**
 * Chargement du fichier.
 *
 * \todo Implémentation.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderICO::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        CApplication::getApp()->log(CString("CLoaderICO::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // Lecture de l'en-tête
    TIconDir header;
    file.read(reinterpret_cast<char *>(&header), sizeof(TIconDir));

    // Vérification des données
    if (header.reserved != 0 || header.type != 1)
    {
        CApplication::getApp()->log(CString("CLoaderICO::loadFromFile : l'identifiant du fichier ICO est incorrect (%1)").arg(header.reserved), ILogger::Error);
        return false;
    }

    // Lectures des entrées
    for (unsigned int i = 0; i < header.count; ++i)
    {
        TIconDirEntry entry;
        file.read(reinterpret_cast<char *>(&entry), sizeof(TIconDirEntry));
    }

    //...

    return true;
}

} // Namespace Ted

/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CLoaderWAL.cpp
 * \date       2008 Création de la classe CLoaderWAL.
 * \date 10/07/2010 Le chargement se fait avec la méthode LoadFromFile.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <fstream>

#include "Graphic/CLoaderWAL.hpp"
#include "Core/CApplication.hpp"
#include "Core/ILogger.hpp"
#include "Core/Allocation.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CLoaderWAL::CLoaderWAL() :
ILoaderImage()
{ }


/**
 * Chargement du fichier.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CLoaderWAL::loadFromFile(const CString& fileName)
{
    std::ifstream file(fileName.toCharArray(), std::ios::in | std::ios::binary);

    if (!file)
    {
        CApplication::getApp()->log(CString("CLoaderWAL::loadFromFile : impossible de lire le fichier %1").arg(fileName), ILogger::Error);
        return false;
    }

    m_fileName = fileName;

    // Lecture de l'en-tête
    THeader header;
    file.read((char *)&header, sizeof(THeader));

    // Dimensions de l'image
    m_width = header.width;
    m_height = header.height;

    m_pixels.reserve(4 * m_width * m_height);
    uint8_t * tmp = new uint8_t[m_width * m_height];

    // Lecture des pixels
    for (unsigned int i = 0; i < m_width * m_height; ++i)
    {
        uint8_t pixel = 0;

        file.read(reinterpret_cast<char *>(&pixel), sizeof(uint8_t));
        tmp[i] = pixel;
    }

    // On convertit les pixels
    for (unsigned int i = 0; i < m_width * m_height; ++i)
    {
        m_pixels[4*i+0] = *tmp;
        m_pixels[4*i+1] = *tmp;
        m_pixels[4*i+2] = *tmp;
        m_pixels[4*i+3] = 0x00;

        ++tmp;
    }

    delete[] tmp;

    return true;
}

} // Namespace Ted

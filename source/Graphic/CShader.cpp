/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CShader.cpp
 * \date       2008 Création de la classe CShader.
 * \date 08/07/2010 La méthode LoadFromFile retourne un booléen.
 * \date 10/07/2010 Le chargement des fichiers se fait avec une méthode LoadFromFile.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <Cg/cgGL.h>

#include "Graphic/CShader.hpp"
#include "Graphic/CShaderLoader.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 *
 * \param program Programme du shader.
 * \param type    Type de shader.
 ******************************/

CShader::CShader(CGprogram program, TShaderType type) :
m_program (program),
m_type    (type)
{ }


/**
 * Destructeur.
 ******************************/

CShader::~CShader()
{
    // Destruction du programme
    if (m_program)
    {
        cgDestroyProgram(m_program);
    }
}


/**
 * Charge le shader à partir d'un fichier.
 *
 * \param fileName Nom du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CShader::loadFromFile(const CString& fileName)
{
    CShaderLoader file(m_type);

    if (file.loadFromFile(fileName))
    {
        m_program = file.getShader()->getProgram();
        return true;
    }

    return false;
}


/**
 * Change un paramètre du shader.
 *
 * \param name  Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, float value)
{
    float data[] = { value, 0.0f, 0.0f, 0.0f };
    cgGLSetParameter4fv(getParameter(name), data);
}


/**
 * Change un paramètre du shader.
 *
 * \param name  Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, const TVector2F& value)
{
    float data[] = { value.X, value.Y, 0.0f, 0.0f };
    cgGLSetParameter4fv(getParameter(name), data);
}


/**
 * Change un paramètre du shader.
 *
 * \param name  Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, const TVector3F& value)
{
    float data[] = { value.X, value.Y, value.Z, 0.0f };
    cgGLSetParameter4fv(getParameter(name), data);
}


/**
 * Change un paramètre du shader.
 *
 * \param name  Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, const TVector4F& value)
{
    float data[] = { value.X , value.Y , value.Z , value.W };
    cgGLSetParameter4fv(getParameter(name), data);
}


/**
 * Change un paramètre du shader.
 *
 * \param name  Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, const TMatrix4F& value)
{
    cgGLSetMatrixParameterfc(getParameter(name), value);
}


/**
 * Change un paramètre du shader.
 *
 * \param name Nom du paramètre.
 * \param value Valeur du paramètre.
 ******************************/

void CShader::setParameter(const CString& name, const CColor& value)
{
    float data[4];
    value.toFloat(data);
    cgGLSetParameter4fv(getParameter(name), data);
}


/**
 * Récupère la valeur d'un paramètre.
 *
 * \return Paramètre.
 ******************************/

CGparameter CShader::getParameter(const CString& name) const
{
    return cgGetNamedParameter(m_program, name.toCharArray());
}

} // Namespace Ted

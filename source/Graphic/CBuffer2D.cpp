/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CBuffer2D.cpp
 * \date 05/02/2010 Création de la classe CBuffer2D.
 * \date 12/11/2010 La méthode AddText prend un TTextParams en argument.
 * \date 23/01/2011 Les buffers 2D ont un fonctionnement similaire aux buffers 3D.
 * \date 27/02/2011 Création de méthodes pour ajouter des formes simples.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 15/06/2014 Utilisation de flottants pour les coordonnées.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CFontManager.hpp"
#include "Core/Maths/CRectangle.hpp"


namespace Ted
{

/**
 * Constructeur par défaut.
 ******************************/

CBuffer2D::CBuffer2D() :
IBuffer ()
{ }


/**
 * Retourne le tableau des éléments à afficher.
 *
 * \return Référence sur le tableau des éléments à afficher.
 ******************************/

std::vector<TPrimitiveType>& CBuffer2D::getElements()
{
    return m_elements;
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence sur le tableau des indices.
 ******************************/

std::vector<unsigned int>& CBuffer2D::getIndices()
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence sur le tableau des sommets.
 ******************************/

std::vector<TVector2F>& CBuffer2D::getVertices()
{
    return m_vertices;
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \return Référence sur le tableau des coordonnées de texture.
 ******************************/

std::vector<float>& CBuffer2D::getTextCoords()
{
    return m_coords;
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence sur le tableau des couleurs.
 ******************************/

std::vector<float>& CBuffer2D::getColors()
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence sur le tableau des textures.
 ******************************/

std::vector<TTextureId>& CBuffer2D::getTextures()
{
    return m_textures;
}


/**
 * Retourne le tableau des éléments à afficher.
 *
 * \return Référence constante sur le tableau des éléments à afficher.
 ******************************/

const std::vector<TPrimitiveType>& CBuffer2D::getElements() const
{
    return m_elements;
}


/**
 * Retourne le tableau des indices.
 *
 * \return Référence constante sur le tableau des indices.
 ******************************/

const std::vector<unsigned int>& CBuffer2D::getIndices() const
{
    return m_indices;
}


/**
 * Retourne le tableau des sommets.
 *
 * \return Référence constante sur le tableau des sommets.
 ******************************/

const std::vector<TVector2F>& CBuffer2D::getVertices() const
{
    return m_vertices;
}


/**
 * Retourne le tableau des coordonnées de texture.
 *
 * \return Référence constante sur le tableau des coordonnées de texture.
 ******************************/

const std::vector<float>& CBuffer2D::getTextCoords() const
{
    return m_coords;
}


/**
 * Retourne le tableau des couleurs.
 *
 * \return Référence constante sur le tableau des couleurs.
 ******************************/

const std::vector<float>& CBuffer2D::getColors() const
{
    return m_colors;
}


/**
 * Retourne le tableau des textures.
 *
 * \return Référence constante sur le tableau des textures.
 ******************************/

const std::vector<TTextureId>& CBuffer2D::getTextures() const
{
    return m_textures;
}


/**
 * Supprime toutes les données.
 ******************************/

void CBuffer2D::clear()
{
    m_elements.clear();
    m_indices.clear();
    m_vertices.clear();
    m_coords.clear();
    m_colors.clear();
    m_textures.clear();
}


/**
 * Dessine le contenu du buffer.
 * Doit être appelée depuis le thread principal.
 *
 * \return Nombre de surfaces dessinées.
 ******************************/

unsigned int CBuffer2D::draw() const
{
    if (m_vertexBuffer == 0 || m_indexBuffer == 0)
    {
        return 0;
    }

    unsigned int offset = 0;

/*
    glDisable(GL_DEPTH_TEST);

    // Sauvegarde de la matrice
    glPushMatrix();

    // On met en projection orthogonale
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight(), 0, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
*/

    // Buffer
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);

    // Activation des tableaux de vertices
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(0));
    offset += m_vertices.size() * sizeof(TVector2F);

    if (m_coords.size() > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);

        glTexCoordPointer(2, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
        offset += m_coords.size() * sizeof(float);

        Game::textureManager->bindTexture(0);
    }

    // Activation des tableaux de couleurs
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, T_BUFFER_OFFSET(offset));
    offset += m_colors.size() * sizeof(float);


    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);


    unsigned int nbr = 0; // Nombre d'indices utilisés
    unsigned int texture = 0; // Nombre de triangles affichés

    // Affichage des primitives
    for (std::vector<TPrimitiveType>::const_iterator it = m_elements.begin(); it != m_elements.end(); ++it)
    {
        switch (*it)
        {
            default:
            case PrimPoint:
            {
                // Affichage des primitives
                glDrawElements(GL_POINTS, 1, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                nbr += 1;

                break;
            }

            case PrimLine:
            {
                // Affichage des primitives
                glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                nbr += 2;

                break;
            }

            case PrimTriangle:
            case PrimTriangleFan:
            case PrimTriangleStrip:
            {
                Game::textureManager->bindTexture(m_textures[texture]);
                ++texture;

                // Affichage des primitives
                glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, T_BUFFER_OFFSET(nbr * sizeof(unsigned int)));
                nbr += 3;

                Game::textureManager->bindTexture(0);

                break;
            }
        }
    }


    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Désactivation des tableaux de couleurs
    glDisableClientState(GL_COLOR_ARRAY);

    // Désactivation des tableaux de coordonnées de textures
    if (m_coords.size() > 0)
    {
        glActiveTextureARB(GL_TEXTURE0_ARB);
        glClientActiveTextureARB(GL_TEXTURE0_ARB);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

    // Désactivation des tableaux de vertices
    glDisableClientState(GL_VERTEX_ARRAY);

/*
    // On revient à une projection en perspective
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    CRenderer::Instance().Perpective();

    // Restauration de la matrice précedemment enregistrée
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable(GL_DEPTH_TEST);
*/
    return 0;
}


/**
 * Met à jour les buffers.
 * Doit être appelée depuis le thread principal.
 ******************************/

void CBuffer2D::update()
{
    IBuffer::update();

    const unsigned int sz_vert = m_vertices.size() * sizeof(TVector2F);
    const unsigned int sz_txtc = m_coords.size() * sizeof(float);
    const unsigned int sz_colo = m_colors.size() * sizeof(float);
    unsigned int sz_total = sz_vert + sz_txtc + sz_colo;

    // Modification du VBO
    glBindBufferARB(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferDataARB(GL_ARRAY_BUFFER, sz_total, 0, GL_STATIC_DRAW);

    unsigned int sz = 0;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_vert, &m_vertices[0]);
    sz += sz_vert;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_txtc, &m_coords[0]);
    sz += sz_txtc;

    glBufferSubDataARB(GL_ARRAY_BUFFER, sz, sz_colo, &m_colors[0]);
    sz += sz_colo;

    // Modification du IBO
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);


    // Le tableau de textures doit contenir autant d'éléments que de triangles à afficher
    unsigned int nbr_tri = 0;

    for (std::vector<TPrimitiveType>::iterator it = m_elements.begin(); it != m_elements.end(); ++it)
    {
        if (*it == PrimTriangle || *it == PrimTriangleFan || *it == PrimTriangleStrip)
        {
            ++nbr_tri;
        }
    }

    m_textures.resize(nbr_tri, 0);
}


/**
 * Dessine un rectangle coloré aligné sur les axes.
 *
 * \param rec   Dimensions du rectangle à afficher.
 * \param color Couleur du rectangle.
 ******************************/

void CBuffer2D::addRectangle(const CRectangle& rec, const CColor& color)
{
    // Rien à afficher
    if (rec.isEmpty())
        return;

    if (rec.getHeight() == 1)
    {
        // Dessin d'un point
        if (rec.getWidth() == 1)
        {
            addPoint(TVector2F(rec.getX(), rec.getY()), color);
        }
        // Dessin d'une ligne
        else
        {
            addLine(TVector2F(rec.getX(), rec.getY()), TVector2F(rec.getX() + rec.getWidth(), rec.getY()), color);
        }
    }
    // Dessin d'une ligne
    else if (rec.getWidth() == 1)
    {
        addLine(TVector2F(rec.getX(), rec.getY()), TVector2F(rec.getX(), rec.getY() + rec.getHeight()), color);
    }
    else
    {
        float tmpColor[4];
        color.toFloat(tmpColor);

        m_elements.push_back(PrimTriangle);
        m_textures.push_back(0);
        m_elements.push_back(PrimTriangle);
        m_textures.push_back(0);

        unsigned int indice = m_vertices.size();

        m_indices.push_back(indice);
        m_indices.push_back(indice + 1);
        m_indices.push_back(indice + 2);
        m_indices.push_back(indice);
        m_indices.push_back(indice + 2);
        m_indices.push_back(indice + 3);

        m_vertices.push_back(TVector2F(rec.getX(), rec.getY()));
        m_vertices.push_back(TVector2F(rec.getX() + rec.getWidth(), rec.getY()));
        m_vertices.push_back(TVector2F(rec.getX() + rec.getWidth(), rec.getY() + rec.getHeight()));
        m_vertices.push_back(TVector2F(rec.getX(), rec.getY() + rec.getHeight()));

        for (int i = 0; i < 4; ++i)
        {
            m_coords.push_back(0.0f);
            m_coords.push_back(0.0f);

            m_colors.push_back(tmpColor[0]);
            m_colors.push_back(tmpColor[1]);
            m_colors.push_back(tmpColor[2]);
            m_colors.push_back(tmpColor[3]);
        }
    }
}


/**
 * Dessine un rectangle aux coins arrondis (4px).
 *
 * \todo Pouvoir dessiner des rectangles arrondis de petites dimensions.
 *
 * \param rec   Dimensions du rectangle.
 * \param color Couleur du rectangle.
 ******************************/

void CBuffer2D::addRectangleRounded(const CRectangle& rec, const CColor& color)
{
    // Rien à afficher
    if (rec.isEmpty())
        return;

    if (rec.getWidth() == 1)
    {
        if (rec.getHeight() == 1)
        {
            addPoint(TVector2F(rec.getX(), rec.getY()), color);
        }
        else
        {
            addLine(TVector2F(rec.getX(), rec.getY()), TVector2F(rec.getX(), rec.getY() + rec.getHeight()), color);
        }
    }
    else if (rec.getHeight() == 1)
    {
        addLine(TVector2F(rec.getX(), rec.getY()), TVector2F(rec.getX() + rec.getWidth(), rec.getY()), color);
    }
    else if (rec.getHeight() < 11 || rec.getWidth() < 11)
    {
        // On dessine un rectangle normal
        addRectangle(rec, color);
    }
    else
    {
        // On dessine troix rectangles et quatres quarts de cercle
        addRectangle(CRectangle(rec.getX(), rec.getY() + 4, rec.getWidth(), rec.getHeight() - 8), color);
        addRectangle(CRectangle(rec.getX() + 4, rec.getY(), rec.getWidth() - 8, 4), color);
        addRectangle(CRectangle(rec.getX() + 4, rec.getY() + rec.getHeight() - 4, rec.getWidth() - 8, 4), color);

        // Coin haut gauche
        addLine(TVector2F(rec.getX() + 2, rec.getY() + 2), TVector2F(rec.getX() + 2, rec.getY() + 4), color);
        addRectangle(CRectangle(rec.getX() + 2, rec.getY() + 1, 2, 3), color);

        // Coin haut droite
        addLine(TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + 2), TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + 4), color);
        addRectangle(CRectangle(rec.getX() + rec.getWidth() - 4, rec.getY() + 1, 2, 3), color);

        // Coin bas gauche
        addLine(TVector2F(rec.getX() + 2, rec.getY() + rec.getHeight() - 4), TVector2F(rec.getX() + 2, rec.getY() + rec.getHeight() - 2), color);
        addRectangle(CRectangle(rec.getX() + 2, rec.getY() + rec.getHeight() - 4, 2, 3), color);

        // Coin bas droite
        addLine(TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + rec.getHeight() - 4), TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + rec.getHeight() - 2), color);
        addRectangle(CRectangle(rec.getX() + rec.getWidth() - 4, rec.getY() + rec.getHeight() - 4, 2, 3), color);
    }
}


/**
 * Dessine la bordure d'un rectangle.
 *
 * \todo Supprimer le paramètre epaisseur.
 * \todo Ajouter un paramètre corner pour donner le rayon des angles (fusion avec DrawBorderRounded).
 *
 * \param rec       Dimensions du rectangle.
 * \param color     Couleur de la bordure.
 * \param epaisseur Épaisseur de la bordure.
 ******************************/

void CBuffer2D::addBorder(const CRectangle& rec, const CColor& color, int epaisseur)
{
    if (epaisseur <= 1)
    {
        addLine(TVector2F(rec.getX(), rec.getY()), TVector2F(rec.getX() + rec.getWidth(), rec.getY()), color);
        addLine(TVector2F(rec.getX() + rec.getWidth(), rec.getY()), TVector2F(rec.getX() + rec.getWidth(), rec.getY() + rec.getHeight()), color);
        addLine(TVector2F(rec.getX() + rec.getWidth(), rec.getY() + rec.getHeight()), TVector2F(rec.getX(), rec.getY() + rec.getHeight()), color);
        addLine(TVector2F(rec.getX() + 1, rec.getY()), TVector2F(rec.getX() + 1, rec.getY() + rec.getHeight()), color);
    }
    else if (epaisseur >= 2 * rec.getHeight() || epaisseur >= 2 * rec.getWidth())
    {
        addRectangle(rec, color);
    }
    else
    {
        addRectangle(CRectangle(rec.getX(), rec.getY(), rec.getWidth(), epaisseur), color);
        addRectangle(CRectangle(rec.getX(), rec.getY() + rec.getHeight() - epaisseur, rec.getWidth(), epaisseur), color);
        addRectangle(CRectangle(rec.getX(), rec.getY() + epaisseur, epaisseur, rec.getHeight() - 2 * epaisseur), color);
        addRectangle(CRectangle(rec.getX() + rec.getWidth() - epaisseur, rec.getY() + epaisseur, epaisseur, rec.getHeight() - 2 * epaisseur), color);
    }
}


/**
 * Dessine la bordure d'un rectangle arrondi.
 *
 * \todo Dessin d'une bordure épaisse.
 * \todo Pouvoir préciser le rayon des angles.
 *
 * \param rec       Dimensions du rectangle.
 * \param color     Couleur de la bordure.
 * \param epaisseur Épaisseur de la bordure.
 ******************************/

void CBuffer2D::addBorderRounded(const CRectangle& rec, const CColor& color, int epaisseur)
{
    if (rec.isEmpty())
        return;

    if (rec.getHeight() < 11 || rec.getWidth() < 11)
    {
        // On dessine une bordure normale
        addBorder(rec, color, epaisseur);
    }
    else
    {
        if (epaisseur <= 1)
        {
            addLine(TVector2F(rec.getX() + 4, rec.getY()), TVector2F(rec.getX() + rec.getWidth() - 4, rec.getY()), color);
            addLine(TVector2F(rec.getX() + rec.getWidth(), rec.getY() + 4), TVector2F(rec.getX() + rec.getWidth(), rec.getY() + rec.getHeight() - 4), color);
            addLine(TVector2F(rec.getX() + 4, rec.getY() + rec.getHeight() + 1), TVector2F(rec.getX() + rec.getWidth() - 4, rec.getY() + rec.getHeight() + 1), color);
            addLine(TVector2F(rec.getX() - 1, rec.getY() + 4), TVector2F(rec.getX() - 1, rec.getY() + rec.getHeight() - 4), color);
            addLine(TVector2F(rec.getX() + 2, rec.getY() + 1), TVector2F(rec.getX() + 4, rec.getY() + 1), color);
            addLine(TVector2F(rec.getX() + rec.getWidth() - 4, rec.getY() + 1), TVector2F(rec.getX() + rec.getWidth() - 2, rec.getY() + 1), color);
            addLine(TVector2F(rec.getX() + 2, rec.getY() + rec.getHeight()), TVector2F(rec.getX() + 4, rec.getY() + rec.getHeight()), color);
            addLine(TVector2F(rec.getX() + rec.getWidth() - 4, rec.getY() + rec.getHeight()), TVector2F(rec.getX() + rec.getWidth() - 2, rec.getY() + rec.getHeight()), color);
            addLine(TVector2F(rec.getX(), rec.getY() + rec.getHeight() - 4), TVector2F(rec.getX(), rec.getY() + rec.getHeight() - 2), color);
            addLine(TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + rec.getHeight() - 4), TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + rec.getHeight() - 2), color);
            addLine(TVector2F(rec.getX(), rec.getY() + 2), TVector2F(rec.getX(), rec.getY() + 4), color);
            addLine(TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + 2), TVector2F(rec.getX() + rec.getWidth() - 1, rec.getY() + 4), color);

            addPoint(TVector2F(rec.getX() + 1, rec.getY() + 2), color);
            addPoint(TVector2F(rec.getX() + rec.getWidth() - 2, rec.getY() + 2), color);
            addPoint(TVector2F(rec.getX() + 1, rec.getY() + rec.getHeight() - 1), color);
            addPoint(TVector2F(rec.getX() + rec.getWidth() - 2, rec.getY() + rec.getHeight() - 1), color);
        }
        else
        {
            addBorder(rec, color, epaisseur);
        }
    }
}


/**
 * Dessine un disque.
 *
 * \todo Améliorer cette méthode pour que chaque pixel ne soit dessiné qu'une fois.
 * \todo Dessiner des ellipses.
 *
 * \param x      Position horizontale du centre du disque.
 * \param y      Position verticale du centre du disque.
 * \param radius Rayon du disque.
 * \param color  Couleur du disque.
 ******************************/

void CBuffer2D::addDisc(int x, int y, int radius, const CColor& color)
{
    if (radius <= 0)
    {
        addPoint(TVector2F(x, y), color);
    }
    else
    {
        int d = 3 - radius;
        int nx = 0;
        int ny = radius;

        while (ny >= nx)
        {
            addLine(TVector2F(x - nx - 1, y - ny), TVector2F(x + nx, y - ny), color);
            addLine(TVector2F(x - nx - 1, y + ny), TVector2F(x + nx, y + ny), color);
            addLine(TVector2F(x - ny - 1, y - nx), TVector2F(x + ny, y - nx), color);
            addLine(TVector2F(x - ny - 1, y + nx), TVector2F(x + ny, y + nx), color);

            if (d < 0)
            {
                d += (4 * nx) + 6;
            }
            else
            {
                d += 4 * (nx - ny) + 10;
                --ny;
            }

            ++nx;
        }
    }
}


/**
 * Dessine un cercle.
 *
 * \todo Améliorer cette méthode pour que chaque pixel ne soit dessiné qu'une fois.
 * \todo Dessiner des ellipses.
 *
 * \param x      Position horizontale du centre du cercle.
 * \param y      Position verticale du centre du cercle.
 * \param radius Rayon du cercle.
 * \param color  Couleur du cercle.
 ******************************/

void CBuffer2D::addCircle(int x, int y, int radius, const CColor& color)
{
    if (radius <= 0)
    {
        addPoint(TVector2F(x, y), color);
    }
    else
    {
        int d = 3 - radius;
        int nx = 0;
        int ny = radius;

        while (ny >= nx)
        {
            addPoint(TVector2F(x - nx - 1, y - ny), color);
            addPoint(TVector2F(x + nx, y - ny), color);

            addPoint(TVector2F(x - nx - 1, y + ny), color);
            addPoint(TVector2F(x + nx, y + ny), color);

            addPoint(TVector2F(x - ny - 1, y - nx), color);
            addPoint(TVector2F(x + ny, y - nx), color);

            addPoint(TVector2F(x - ny - 1, y + nx), color);
            addPoint(TVector2F(x + ny, y + nx), color);

            if (d < 0)
            {
                d += (4 * nx) + 6;
            }
            else
            {
                d += 4 * (nx - ny) + 10;
                --ny;
            }

            ++nx;
        }
    }
}


/**
 * Affiche du texte dans une certaine zone.
 *
 * \param params Paramètres du texte à afficher.
 ******************************/

void CBuffer2D::addText(const TTextParams& params)
{
    Game::fontManager->drawText(this, params);
}


/**
 * Affiche une image à l'écran.
 *
 * \param rec     Dimensions du rectangle.
 * \param texture Identifiant de la texture à utiliser.
 ******************************/

void CBuffer2D::addImage(const CRectangle& rec, const TTextureId texture)
{
    m_elements.push_back(PrimTriangle);
    m_textures.push_back(texture);
    m_elements.push_back(PrimTriangle);
    m_textures.push_back(texture);

    unsigned int indice = m_vertices.size();

    m_indices.push_back(indice);
    m_indices.push_back(indice + 1);
    m_indices.push_back(indice + 2);
    m_indices.push_back(indice);
    m_indices.push_back(indice + 2);
    m_indices.push_back(indice + 3);

    for (int i = 0; i < 4 * 4; ++i)
    {
        m_colors.push_back(1.0f);
    }

    // Premier point
    m_vertices.push_back(TVector2F(rec.getX(), rec.getY()));

    m_coords.push_back(0.0f);
    m_coords.push_back(0.0f);

    // Deuxième point
    m_vertices.push_back(TVector2F(rec.getX() + rec.getWidth(), rec.getY()));

    m_coords.push_back(1.0f);
    m_coords.push_back(0.0f);

    // Troisième point
    m_vertices.push_back(TVector2F(rec.getX() + rec.getWidth(), rec.getY() + rec.getHeight()));

    m_coords.push_back(1.0f);
    m_coords.push_back(1.0f);

    // Quatrième point
    m_vertices.push_back(TVector2F(rec.getX(), rec.getY() + rec.getHeight()));

    m_coords.push_back(0.0f);
    m_coords.push_back(1.0f);
}


/**
 * Dessine une ligne.
 *
 * \param from  Position départ.
 * \param to    Position d'arrivée.
 * \param color Couleur de la ligne.
 ******************************/

void CBuffer2D::addLine(const TVector2F& from, const TVector2F& to, const CColor& color)
{
    // Point
    if (from == to)
    {
        addPoint(from, color);
    }
    else
    {
        float tmp_color[4];
        color.toFloat(tmp_color);

        m_elements.push_back(PrimLine);

        unsigned int indice = m_vertices.size();

        m_indices.push_back(indice);
        m_indices.push_back(indice + 1);

        m_vertices.push_back(from);
        m_vertices.push_back(to);

        for (int i = 0; i < 2; ++i)
        {
            m_coords.push_back(0.0f);
            m_coords.push_back(0.0f);

            m_colors.push_back(tmp_color[0]);
            m_colors.push_back(tmp_color[1]);
            m_colors.push_back(tmp_color[2]);
            m_colors.push_back(tmp_color[3]);
        }
    }
}


/**
 * Dessine un point.
 *
 * \param pos   Position du point.
 * \param color Couleur du point.
 ******************************/

void CBuffer2D::addPoint(const TVector2F& pos, const CColor& color)
{
    m_elements.push_back(PrimPoint);

    m_indices.push_back(m_vertices.size());

    m_vertices.push_back(pos);

    m_coords.push_back(0.0f);
    m_coords.push_back(0.0f);

    float tmp_color[4];
    color.toFloat(tmp_color);

    m_colors.push_back(tmp_color[0]);
    m_colors.push_back(tmp_color[1]);
    m_colors.push_back(tmp_color[2]);
    m_colors.push_back(tmp_color[3]);
}

} // Namespace Ted

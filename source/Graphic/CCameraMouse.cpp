/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CCameraMouse.cpp
 * \date 21/02/2010 Création de la classe CCameraMouse.
 * \date 19/10/2013 Ajout de la méthode anglesFromVectors.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CCameraMouse.hpp"
#include "Core/Events.hpp"


namespace Ted
{

const float CameraMouseVelocity = 0.5f; ///< Vitesse de déplacement de la caméra.
const float PiOver180 = 0.0174532925f;  ///< Pi sur 180.


/**
 * Constructeur par défaut.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

CCameraMouse::CCameraMouse(const TVector3F& position, const TVector3F& direction) :
ICamera        (position, direction),
m_forward      (Origin3F),
m_theta        (0.0f),
m_phi          (0.0f),
m_button_left  (false),
m_button_right (false)
{
    anglesFromVectors();
}


/**
 * Destructeur.
 ******************************/

CCameraMouse::~CCameraMouse()
{ }


/**
 * Accesseur pour theta.
 *
 * \return Angle Theta.
 ******************************/

float CCameraMouse::getTheta() const
{
    return m_theta;
}


/**
 * Accesseur pour phi.
 *
 * \return Angle Phi.
 ******************************/

float CCameraMouse::getPhi() const
{
    return m_phi;
}


/**
 * Modifie la direction de la caméra.
 *
 * \param direction Nouvelle direction.
 ******************************/

void CCameraMouse::setDirection(const TVector3F& direction)
{
    m_direction = direction;
    anglesFromVectors();
}


/**
 * Gestion des évènements de la souris.
 *
 * \param event Évènement.
 ******************************/

void CCameraMouse::onEvent(const CMouseEvent& event)
{
    if (m_button_left)
    {
        //... event.yrel ...
        vectorsFromAngles();
    }
    else if (m_button_right)
    {
        m_theta -= event.xrel * MOUSE_MOTION_SPEED;
        m_phi   -= event.yrel * MOUSE_MOTION_SPEED;
        vectorsFromAngles();
    }

    if (event.button == MouseButtonLeft)
    {
        if (event.type == ButtonPressed)
            m_button_left = true;
        else if (event.type == ButtonReleased)
            m_button_left = false;
    }
    else if (event.button == MouseButtonRight)
    {
        if (event.type == ButtonPressed)
            m_button_right = true;
        else if (event.type == ButtonReleased)
            m_button_right = false;
    }
}


/**
 * Déplace la caméra.
 *
 * \todo Implémentation.
 *
 * \param frameTime Durée de la frame en millisecondes.
 ******************************/

void CCameraMouse::animate(unsigned int frameTime)
{
    T_UNUSED(frameTime);
/*
    // Déplacement vers l'avant
    if (CApplication::Instance().getKeyState(m_keyconf["forward"]))
    {
        m_position += m_forward * CameraMouseVelocity * frameTime;
    }

    // Déplacement vers l'arrière
    if (CApplication::Instance().getKeyState(m_keyconf["backward"]))
    {
        m_position -= m_forward * CameraMouseVelocity * frameTime;
    }
*/
}


/**
 * Convertit les angles en direction.
 ******************************/

void CCameraMouse::vectorsFromAngles()
{
    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    double r_temp = cos(m_phi * PiOver180);
    m_forward.Z = sin(m_phi * PiOver180);
    m_forward.X = r_temp * cos(m_theta * PiOver180);
    m_forward.Y = r_temp * sin(m_theta * PiOver180);

    m_direction = m_position + m_forward;
}


/**
 * Convertit la position et la direction en angles.
 ******************************/

void CCameraMouse::anglesFromVectors()
{
    m_forward = (m_direction - m_position).getNormalized();
    m_phi = asin(m_forward.Z) / PiOver180;

    if (std::abs(m_forward.X) < std::numeric_limits<float>::epsilon())
    {
        m_theta = 90;
    }
    else
    {
        m_theta = atan(m_forward.Y / m_forward.X) / PiOver180;
    }

    if (m_phi > 89.0f)
    {
        m_phi = 89.0f;
    }
    else if (m_phi < -89.0f)
    {
        m_phi = -89.0f;
    }

    // Pour éviter un dépassement de valeur ou une perte de précision
    if (m_theta < -180.0f)
    {
        m_theta += 360.0f;
    }
    else if (m_theta > 180.0f)
    {
        m_theta -= 360.0f;
    }

    m_direction = m_position + m_forward;
}


/**
 * Initialise les paramètres de la caméra.
 *
 * \param position  Position de départ.
 * \param direction Direction d'observation.
 ******************************/

void CCameraMouse::initialize(const TVector3F& position, const TVector3F& direction)
{
    m_position = position;
    m_direction = direction;
    m_forward = Origin3F;
    m_theta = 0.0f;
    m_phi = 0.0f;

    anglesFromVectors();
}

} // Namespace Ted

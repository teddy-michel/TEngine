/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CShaderLoader.hpp
 * \date 10/05/2009 Création de la classe CShaderLoader.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CShaderLoader.hpp"
#include "Graphic/CRenderer.hpp"
#include "Core/Exceptions.hpp"


namespace Ted
{

/**
 * Constructeur.
 *
 * \param type Type de shaders.
 ******************************/

CShaderLoader::CShaderLoader(TShaderType type) :
ILoader  (),
m_type   (type),
m_shader (nullptr)
{
    // Création du context
    m_context = cgCreateContext();
}


/**
 * Destructeur.
 ******************************/

CShaderLoader::~CShaderLoader()
{
    // Destruction du context
    if (m_context)
    {
        cgDestroyContext(m_context);
    }
}


/**
 * Chargement du shader.
 *
 * \param fileName Adresse du fichier.
 * \return Booléen indiquant le succès de l'opération.
 ******************************/

bool CShaderLoader::loadFromFile(const CString& fileName)
{
    m_fileName = fileName;

    // Chargement et compilation du shader
    CGprogram program;

    try
    {
        program = cgCreateProgramFromFile(
            m_context, CG_SOURCE, m_fileName.toCharArray(),
            Game::renderer->getShaderProfile(m_type), "main",
            const_cast<const char **>(Game::renderer->getShaderOptions(m_type)));
    }
    catch (IException& e)
    {
        throw CLoadingFailed(m_fileName.toCharArray(), e.what() + std::string("\n") + cgGetLastListing(m_context));
    }

    m_shader = Game::renderer->createShader(program, m_type);
    return true;
}

} // Namespace Ted

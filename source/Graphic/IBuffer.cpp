/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/IBuffer.cpp
 * \date 11/02/2010 Création de la classe IBuffer.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 07/09/2015 Gestion asynchrone des buffers dans un contexte multithread.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <GL/glew.h>

#include "Graphic/IBuffer.hpp"


namespace Ted
{

/**
 * Crée les buffers nécessaires en mémoire graphique.
 ******************************/

IBuffer::IBuffer() :
m_vertexBuffer (0),
m_indexBuffer  (0)
{

}


/**
 * Détruit les buffers en mémoire graphique.
 ******************************/

IBuffer::~IBuffer()
{
    if (m_vertexBuffer != 0 && m_indexBuffer != 0)
    {
        Game::renderer->requestBufferDelete(this);
    }
}


/**
 * Donne l'identifiant du Vertex Buffer Object.
 *
 * \return Identifiant du Vertex Buffer Object.
 ******************************/

unsigned int IBuffer::getVertexBuffer() const
{
    return m_vertexBuffer;
}


/**
 * Donne l'identifiant du Index Buffer Object.
 *
 * \return Identifiant du Index Buffer Object.
 ******************************/

unsigned int IBuffer::getIndexBuffer() const
{
    return m_indexBuffer;
}


/**
 * Demande une mise à jour du buffer.
 * Cette méthode peut être appelée depuis n'importe quel thread, la mise à jour
 * sera faite depuis le thread principal avec l'appel de CRenderer::doPendingTasks.
 ******************************/

void IBuffer::requestUpdate()
{
    Game::renderer->requestBufferUpdate(this);
}


/**
 * Met à jour les buffers.
 * Cette méthode doit être appelée au début de la fonction update de chaque classe fille.
 ******************************/

void IBuffer::update()
{
    // Création des buffers
    if (m_vertexBuffer == 0)
    {
        glGenBuffersARB(1, &m_vertexBuffer);
    }

    if (m_indexBuffer == 0)
    {
        glGenBuffersARB(1, &m_indexBuffer);
    }
}

} // Namespace Ted

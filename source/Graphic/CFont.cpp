/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CFont.cpp
 * \date 13/04/2013 Création de la classe CFont.
 * \date 19/04/2013 Implémentation des méthodes getTextLinesSize et getTextLineSize.
 * \date 14/06/2014 La hauteur des lignes est définie manuellement.
 * \date 15/06/2014 Les caractères partiellement visibles sont affichés.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include "Graphic/CFont.hpp"
#include "Graphic/CRenderer.hpp"
#include "Graphic/CBuffer2D.hpp"
#include "Graphic/CFontManager.hpp"

#include "Core/CApplication.hpp"


namespace Ted
{

CFont::CFont() :
#ifdef T_USE_FREETYPE
m_face        (nullptr),
#endif
m_hasKerning  (false),
m_currentSize (0)
{

}


CFont::~CFont()
{
#ifdef T_USE_FREETYPE
    if (m_face)
    {
        FT_Error error = FT_Done_Face(m_face);

        if (error != 0)
        {
            //...
        }
    }
#endif
}


bool CFont::loadFromFile(const CString& fileName)
{
CApplication::getApp()->log(CString("loadFont(%1)").arg(fileName));
#ifdef T_USE_FREETYPE
    if (m_face)
    {
        // Déjà chargé
        return false;
    }

    FT_Error error = FT_New_Face(Game::fontManager->m_library, fileName.toCharArray(), 0, &m_face);

    if (error == FT_Err_Unknown_File_Format)
    {
        //...
        return false;
    }
    if (error != 0)
    {
        //...
        return false;
    }

    m_hasKerning = FT_HAS_KERNING(m_face);

    error = FT_Set_Char_Size(m_face, 0, 16 * 64, 300, 300);
    setFontSize(16);

    if (error != 0)
    {
        //...
    }
#endif

    return true;
}


CString CFont::getFamilyName() const
{
#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
        return CString();

    if (m_face->family_name)
        return CString(m_face->family_name);
#endif

    return CString();
}


CString CFont::getFontName() const
{
#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
        return CString();

    if (m_face->family_name && m_face->style_name)
        return CString(m_face->family_name) + " " + CString(m_face->style_name);
#endif

    return CString();
}


// Affichage direct
void CFont::drawText(CBuffer2D * buffer, const TTextParams& params)
{
#ifdef T_USE_FREETYPE
    if (m_face == nullptr)
        return;

    if (params.size < CFontManager::minFontSize || params.size > CFontManager::maxFontSize)
        return;

    setFontSize(params.size);

    // Variables
    float posX = static_cast<float>(params.rec.getX()) - static_cast<float>(params.left);
    float posY = static_cast<float>(params.rec.getY()) - static_cast<float>(params.top);

    bool highlight = false;
    bool drawChar = true;
    bool useKerning = (params.kerning && m_hasKerning);
    CColor colorActual = params.color;
    FT_UInt previous = 0;


    // On récupère les tableaux du buffer graphique
    std::vector<TPrimitiveType>& elements = buffer->getElements();
    std::vector<unsigned int>& indices = buffer->getIndices();
    std::vector<TVector2F>& vertices = buffer->getVertices();
    std::vector<float>& colors = buffer->getColors();
    std::vector<float>& coords = buffer->getTextCoords();
    std::vector<unsigned int>& textures = buffer->getTextures();


    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        // Gestion des caractères spéciaux
        switch (c->unicode())
        {
            // Couleur normale
            case 1:
                colorActual = params.color;
                continue;

            // Blanc
            case 2:
                colorActual = CColor::White;
                continue;

            // Noir
            case 3:
                colorActual = CColor::Black;
                continue;

            // Rouge
            case 4:
                colorActual = CColor::Red;
                continue;

            // Bleu
            case 5:
                colorActual = CColor::Blue;
                continue;

            // Vert
            case 6:
                colorActual = CColor::Green;
                continue;

            // Jaune
            case 7:
                colorActual = CColor::Yellow;
                continue;

            // Orange
            case 8:
                colorActual = CColor::Orange;
                continue;

            // Surlignement
            case 12:
                highlight = !highlight;
                continue;

            default:
                break;
        }

        TCharParams charParams = getCharParams(params.size, *c);

        FT_Error error;

        drawChar = ((params.rec.getWidth()  == 0 || (posX < params.rec.getX() + params.rec.getWidth()  &&
                                                     posX + charParams.rect.getWidth() > static_cast<float>(params.rec.getX()))) &&
                    (params.rec.getHeight() == 0 || (posY < params.rec.getY() + params.rec.getHeight() &&
                                                     posY + charParams.rect.getHeight() > static_cast<float>(params.rec.getY()))));

        if (useKerning && previous && charParams.glyphIndex)
        {
            FT_Vector delta;
            error = FT_Get_Kerning(m_face, previous, charParams.glyphIndex, FT_KERNING_DEFAULT, &delta);

            if (error == 0)
            {
                posX += delta.x >> 6;
                posY += delta.y >> 6;
            }

            previous = charParams.glyphIndex;
        }

        // Pourcentages d'affichage du caractère
        float perX1 = 0.0f;
        float perX2 = 1.0f;
        float perY1 = 0.0f;
        float perY2 = 1.0f;

        if (params.rec.getWidth() > 0)
        {
            // Caractère découpé à gauche
            if (posX < params.rec.getX() &&
                posX + charParams.rect.getWidth() > params.rec.getX())
            {
                perX1 = (params.rec.getX() - posX) / charParams.rect.getWidth();
            }

            // Caractère découpé à droite
            if (posX < params.rec.getX() + params.rec.getWidth() &&
                posX + charParams.rect.getWidth() > params.rec.getX() + params.rec.getWidth())
            {
                perX2 = (params.rec.getX() + params.rec.getWidth() - posX) / charParams.rect.getWidth();
            }
        }

        if (params.rec.getHeight() > 0)
        {
            // Caractère découpé en bas
            if (posY < params.rec.getY() &&
                posY + charParams.rect.getHeight() > params.rec.getY())
            {
                perY1 = (params.rec.getY() - posY) / charParams.rect.getHeight();
            }

            // Caractère découpé en bas
            if (posY < params.rec.getY() + params.rec.getHeight() &&
                posY + charParams.rect.getHeight() > params.rec.getY() + params.rec.getHeight())
            {
                perY2 = (params.rec.getY() + params.rec.getHeight() - posY) / charParams.rect.getHeight();
            }
        }

        // Surlignement du texte
        if (highlight && drawChar)
        {
            // Couleur de surlignement
            float tmpColor[4] = { 1.0f, 0.5f, 0.0f, 0.9f };

            elements.push_back(PrimTriangle);
            textures.push_back(0);
            elements.push_back(PrimTriangle);
            textures.push_back(0);

            unsigned int indice = vertices.size();

            indices.push_back(indice);
            indices.push_back(indice + 1);
            indices.push_back(indice + 2);
            indices.push_back(indice);
            indices.push_back(indice + 2);
            indices.push_back(indice + 3);

            vertices.push_back(TVector2F(posX + charParams.rect.getWidth() * perX1, posY + params.size * perY1));
            vertices.push_back(TVector2F(posX + charParams.rect.getWidth() * perX2, posY + params.size * perY1));
            vertices.push_back(TVector2F(posX + charParams.rect.getWidth() * perX2, posY + params.size * perY2));
            vertices.push_back(TVector2F(posX + charParams.rect.getWidth() * perX1, posY + params.size * perY2));

            for (int i = 0; i < 4; ++i)
            {
                coords.push_back(0.0f);
                coords.push_back(0.0f);

                colors.push_back(tmpColor[0]);
                colors.push_back(tmpColor[1]);
                colors.push_back(tmpColor[2]);
                colors.push_back(tmpColor[3]);
            }
        }

        switch (c->unicode())
        {
            // Saut de ligne
            case '\n':
            case '\r':
                posX = static_cast<float>(params.rec.getX()) - static_cast<float>(params.left);
                posY += charParams.rect.getHeight();
                continue;

            // Espace
            case ' ':
                posX += charParams.rect.getWidth();
                continue;

            // Tabulation horizontale
            case '\t':
                posX += 4.0f * charParams.rect.getWidth(); // TODO: gérer ça un peu mieux (alignement...)
                continue;

            default:

                if (drawChar)
                {
                    float tmp_color[4] = { colorActual.getRed()   / 255.0f ,
                                           colorActual.getGreen() / 255.0f ,
                                           colorActual.getBlue()  / 255.0f ,
                                           colorActual.getAlpha() / 255.0f };

                    elements.push_back(PrimTriangle);
                    textures.push_back(charParams.texture);
                    elements.push_back(PrimTriangle);
                    textures.push_back(charParams.texture);

                    unsigned int indice = vertices.size();

                    indices.push_back(indice);
                    indices.push_back(indice + 1);
                    indices.push_back(indice + 2);
                    indices.push_back(indice);
                    indices.push_back(indice + 2);
                    indices.push_back(indice + 3);


    if (perX1 > 0.0f)
    {
        CApplication::getApp()->log("----------");
        //CApplication::getApp()->log(CString(" char = %1").arg(c->unicode()));
        //CApplication::getApp()->log(CString("    X = %1").arg(params.rec.getX()));
        //CApplication::getApp()->log(CString(" posX = %1").arg(posX));
        //CApplication::getApp()->log(CString(" perX = %1").arg(perX1));
        //CApplication::getApp()->log(CString("width = %1").arg(charParams.rect.getWidth()));

        CApplication::getApp()->log(CString("   X = %1").arg(params.rec.getX()));
        CApplication::getApp()->log(CString("posX = %1").arg((float)posX + (float)charParams.rect.getX() + (float)charParams.pixmap.getWidth() * perX1));
        CApplication::getApp()->log(CString("posX = %1").arg(static_cast<short>(posX + charParams.rect.getX() + charParams.pixmap.getWidth() * perX1)));
    }


                    // Premier point
                    vertices.push_back(TVector2F(posX + charParams.rect.getX() + charParams.pixmap.getWidth() * perX1,
                                                 posY + params.size - charParams.rect.getY() + charParams.pixmap.getHeight() * perY1));

                    coords.push_back(perX1);
                    coords.push_back(perY1);

                    // Deuxième point
                    vertices.push_back(TVector2F(posX + charParams.rect.getX() + charParams.pixmap.getWidth() * perX2,
                                                 posY + params.size - charParams.rect.getY() + charParams.pixmap.getHeight() * perY1));

                    coords.push_back(perX2);
                    coords.push_back(perY1);

                    // Troisième point
                    vertices.push_back(TVector2F(posX + charParams.rect.getX() + charParams.pixmap.getWidth() * perX2,
                                                 posY + params.size - charParams.rect.getY() + charParams.pixmap.getHeight() * perY2));

                    coords.push_back(perX2);
                    coords.push_back(perY2);

                    // Quatrième point
                    vertices.push_back(TVector2F(posX + charParams.rect.getX() + charParams.pixmap.getWidth() * perX1,
                                                 posY + params.size - charParams.rect.getY() + charParams.pixmap.getHeight() * perY2));

                    coords.push_back(perX1);
                    coords.push_back(perY2);

                    for (int i = 0; i < 4; ++i)
                    {
                        colors.push_back(tmp_color[0]);
                        colors.push_back(tmp_color[1]);
                        colors.push_back(tmp_color[2]);
                        colors.push_back(tmp_color[3]);
                    }
                }

                break;
        }

        // Incrémentation de la position
        posX += charParams.rect.getWidth();
    }

#endif
}


TVector2UI CFont::getTextSize(const TTextParams& params)
{
    std::vector<float> lengths;
    TVector2F sz(0.0f, static_cast<float>(params.size * 1.4f));

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lengths.push_back(sz.X);
                sz.Y += charParams.rect.getHeight();
                sz.X = 0.0f;
                break;

            // Tabulation horizontale
            case '\t':
                sz.X += 4 * charParams.rect.getWidth();
                break;

            // Défaut
            default:
                sz.X += charParams.rect.getWidth();
                break;
        }
    }

    // On prend la longueur de la ligne la plus longue
    lengths.push_back(sz.X);
    sz.X = *std::max_element(lengths.begin(), lengths.end());

    return TVector2UI(static_cast<unsigned int>(sz.X), static_cast<unsigned int>(sz.Y));
}


void CFont::getTextLinesSize(const TTextParams& params, std::vector<TTextLine>& lines)
{
    TTextLine currentLine;

    unsigned int offset = 0;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\n':
            case '\r':
                lines.push_back(currentLine);
                currentLine.len.clear();
                currentLine.offset = ++offset;
                break;

            // Tabulation horizontale
            case '\t':
                ++offset;
                currentLine.len.push_back(4.0f * charParams.rect.getWidth());
                break;

            // Défaut
            default:
                ++offset;
                currentLine.len.push_back(charParams.rect.getWidth());
                break;
        }
    }

    lines.push_back(currentLine);
}


void CFont::getTextLineSize(const TTextParams& params, TTextLine& line)
{
    std::vector<float> lengths;

    // Parcours de la chaîne de caractères
    for (CString::const_iterator c = params.text.cbegin(); c != params.text.cend(); ++c)
    {
        TCharParams charParams = getCharParams(params.size, *c);

        switch (c->unicode())
        {
            // Couleurs
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                break;

            // Surlignement
            case 12:
                break;

            // Retour à la ligne
            case '\r':
            case '\n':
                return;

            // Tabulation horizontale
            case '\t':
                line.len.push_back(4.0f * charParams.rect.getWidth());
                break;

            // Défaut
            default:
                line.len.push_back(charParams.rect.getWidth());
                break;
        }
    }
}


CFont::TCharParams CFont::getCharParams(int size, const CChar& character)
{
    if (size < CFontManager::minFontSize || size > CFontManager::maxFontSize)
        return TCharParams();

    if (m_chars.count(size) == 0)
    {
        setFontSize(size);

        // Chargement des caractères ASCII
        for (uint32_t charCode = 0x20; charCode < 0x80; ++charCode)
        {
            m_chars[size][CChar(charCode)] = loadChar(charCode);
        }
    }

    if (m_chars[size].count(character) == 0)
    {
        setFontSize(size);

        // Chargement du caractère
        m_chars[size][character] = loadChar(character.unicode());
    }

    return m_chars[size][character];
}


void CFont::setFontSize(int size) const
{
    T_ASSERT(size >= CFontManager::minFontSize && size <= CFontManager::maxFontSize);

    if (m_currentSize == size)
        return;

#ifdef T_USE_FREETYPE

    T_ASSERT(m_face != nullptr);

    FT_Error error = FT_Set_Pixel_Sizes(m_face, 0, size);

    if (error == 0)
    {
        m_currentSize = size;
    }

#endif
}


CFont::TCharParams CFont::loadChar(uint32_t charCode)
{
//CApplication::getApp()->log(CString("loadChar(%1, %2, %3)").arg(charCode).arg(m_currentSize).arg(getFontName()));
    TCharParams charParams;

#ifdef T_USE_FREETYPE
    T_ASSERT(m_face != nullptr);

    charParams.glyphIndex = FT_Get_Char_Index(m_face, charCode);
    FT_Error error = FT_Load_Glyph(m_face, charParams.glyphIndex, FT_LOAD_RENDER);

    //FT_Error error = FT_Load_Char(m_face, charCode, FT_LOAD_RENDER);

    if (error != 0)
    {
        //...
        return charParams;
    }

    charParams.rect.setX(m_face->glyph->bitmap_left);
    charParams.rect.setY(m_face->glyph->bitmap_top);
    charParams.rect.setWidth(m_face->glyph->advance.x >> 6);
    //charParams.rect.setHeight(m_face->height >> 6);
    charParams.rect.setHeight(m_currentSize * 1.4f);
/*
CApplication::getApp()->log(CString("    X = %1, Y = %2, advance = %3, height = %4")
                            .arg(m_face->glyph->bitmap_left)
                            .arg(m_face->glyph->bitmap_top)
                            .arg((int)m_face->glyph->advance.x >> 6)
                            .arg(m_face->height >> 6)
                           );
CApplication::getApp()->log(CString("    Metrics.width = %1, Metrics.height = %2, horiBearingX = %3, horiBearingY = %4 horiAdvance = %5")
                            .arg((int)m_face->glyph->metrics.width >> 6)
                            .arg((int)m_face->glyph->metrics.height >> 6)
                            .arg((int)m_face->glyph->metrics.horiBearingX >> 6)
                            .arg((int)m_face->glyph->metrics.horiBearingY >> 6)
                            .arg((int)m_face->glyph->metrics.horiAdvance >> 6)
                           );
*/
    if (m_face->glyph->bitmap.pixel_mode == FT_PIXEL_MODE_GRAY)
    {
        const int numRows = m_face->glyph->bitmap.rows;
        const int numCols = m_face->glyph->bitmap.width;
        CColor * data = new CColor[numRows * numCols];
        uint8_t * bufferPtr = m_face->glyph->bitmap.buffer;
        int offset = 0;

//CApplication::getApp()->log(CString("    img.width = %1, img.height = %2, pitch = %3").arg(numCols).arg(numRows).arg(m_face->glyph->bitmap.pitch));

        for (int row = 0; row < numRows; ++row)
        {
            for (int col = 0; col < numCols; ++col)
            {
                //data[offset++] = CColor(0xFF - bufferPtr[col], 0xFF - bufferPtr[col], 0xFF - bufferPtr[col], bufferPtr[col]);
                data[offset++] = CColor(bufferPtr[col], bufferPtr[col], bufferPtr[col], bufferPtr[col]);
            }

            bufferPtr += m_face->glyph->bitmap.pitch;
        }

        charParams.pixmap = CImage(numCols, numRows, data); // Copie inutile de data

//charParams.pixmap.saveToFile(CString("font_%1_%2_%3.png").arg(getFontName()).arg(m_currentSize).arg(charCode));

        charParams.texture = Game::textureManager->loadTexture(CString(":font:%1:%2:%3").arg(getFontName()).arg(m_currentSize).arg(charCode), charParams.pixmap);

        delete[] data;
    }

#endif

    return charParams;
}

} // Namespace Ted

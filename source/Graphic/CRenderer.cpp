/*
Copyright (C) 2008-2015 Teddy Michel

This file is part of TEngine.

TEngine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TEngine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TEngine. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file Graphic/CRenderer.cpp
 * \date       2008 Création de la classe GraphicEngine.
 * \date 11/07/2010 On peut supprimer un buffer de la mémoire ou uniquement du gestionnaire.
 * \date 15/07/2010 Création de la méthode ScreenShot_Thread.
 * \date 09/12/2010 Utilisation de l'accumulation buffer.
 * \date 26/12/2010 Création de la méthode ChangeFogParams.
 * \date 14/03/2011 Inclusion de GL/glx.h sous Linux.
 * \date 05/12/2011 Utilisation de la classe Thread de la SFML.
 * \date 07/12/2011 Utilisation de Glew pour gérer les extensions OpenGL.
 * \date 11/04/2013 Recherche de la version du langage de shading.
 */


/*-------------------------------*
 *   Includes                    *
 *-------------------------------*/

#include <iomanip>
#include <cstring>
#include <GL/glew.h>
#include <SFML/System.hpp>
#include <il.h>

#include "Graphic/CRenderer.hpp"
#include "Graphic/CTextureManager.hpp"
#include "Graphic/CImage.hpp"
#include "Graphic/ICamera.hpp"
#include "Graphic/CSkyBox.hpp"
#include "Graphic/CBuffer.hpp"
#include "Graphic/CFontManager.hpp"

#include "Core/ILogger.hpp"
#include "Core/CDateTime.hpp" // Pour nommer les captures d'écran
#include "Core/CApplication.hpp"
#include "Core/Exceptions.hpp"
#include "Core/Maths/CVector3.hpp"
#include "Core/Maths/CMatrix4.hpp"

// DEBUG
#include "Core/Allocation.hpp"

#include "os.h"


namespace Ted
{

const float ZFAR  = 10000.0f;                ///< Distance far.
const float ZNEAR = 0.1f;                    ///< Distance near.
const double PiOver360 = 0.008726646259971f; ///< Pi sur 360.


namespace Game
{
    CRenderer * renderer = nullptr;
}


/**
 * Constructeur par défaut.
 ******************************/

CRenderer::CRenderer() :
m_ratio          (1.333333f),
m_camera         (nullptr),
m_skybox         (nullptr),
m_near           (ZNEAR),
m_far            (ZFAR),
m_mode           (Normal),
m_numSurfaces    (0),
m_numSurfacesOld (0),
m_anisotropic    (1.0f),
m_shaderVersion  (GLSLVersionUnknown)
{
    if (Game::renderer)
        exit(-1);

    CApplication::getApp()->log("Initialisation du moteur graphique");

    new CTextureManager();
    new CFontManager();
    Game::renderer = this;

    // Initialisation de DevIL
    ilInit();
    ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
    ilEnable(IL_ORIGIN_SET);
    ilEnable(IL_FILE_OVERWRITE);
    ilSetInteger(IL_FORMAT_MODE, IL_RGBA);
    ilEnable(IL_FORMAT_SET);
}


/**
 * Destructeur.
 ******************************/

CRenderer::~CRenderer()
{
    ilShutDown();

    Game::renderer = nullptr;
    CApplication::getApp()->log("Fermeture du moteur graphique");

    delete Game::fontManager;
    delete Game::textureManager;
    //clearBuffers();
}


/**
 * Modifie la distance minimale visible.
 *
 * \param znear Distance minimale visible.
 *
 * \sa CRenderer::getNear
 ******************************/

void CRenderer::setNear(float znear)
{
    if (znear > 0.01f)
    {
        m_near = znear;
    }
}


/**
 * Modifie la distance maximale visible.
 *
 * \param zfar Distance maximale visible.
 *
 * \sa CRenderer::getFar
 ******************************/

void CRenderer::setFar(float zfar)
{
    if (zfar > m_near)
    {
        m_far = zfar;
    }
}


/**
 * Modifie le mode d'affichage
 *
 * \param mode Mode d'affichage.
 *
 * \sa CRenderer::getMode
 ******************************/

void CRenderer::setMode(TDisplayMode mode)
{
    m_mode = mode;
}


/**
 * Modifie la caméra à utiliser.
 * Attention, cette méthode ne supprime pas l'ancienne caméra.
 *
 * \param camera Pointeur sur la caméra à utiliser par le moteur graphique.
 *
 * \sa CRenderer::getCamera
 ******************************/

void CRenderer::setCamera(ICamera * camera)
{
    m_camera = camera;
}


/**
 * Modifie la skybox à utiliser.
 * Attention, cette méthode ne supprime pas l'ancinne skybox.
 *
 * \param skybox Pointeur sur la skybox à utiliser par le moteur graphique.
 *
 * \sa CRenderer::getSkybox
 ******************************/

void CRenderer::setSkybox(CSkyBox * skybox)
{
    m_skybox = skybox;
}


/**
 * Modifie la valeur du filtrage anisotropique.
 * Cette valeur vaut 1 par défaut, et doit être comprise entre 1 et une valeur
 * maximale qui dépend de la carte graphique (souvent 16).
 * Si une valeur inférieure à 1 ou supérieure à la valeur maximale est demandée,
 * la valeur correcte la plus proche est utilisée.
 *
 * \param anisotropic Valeur du filtrage anisotropique.
 * \return Nouvelle valeur du filtrage anisotropique.
 *
 * \sa CRenderer::getAnisotropic
 ******************************/

float CRenderer::setAnisotropic(float anisotropic)
{
    static bool ext = isOGLExtension("texture_filter_anisotropic");

    if (ext)
    {
        float maxanis = 1.0f;
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxanis);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxanis);

             if (anisotropic < 1.0f)    m_anisotropic = 1.0f;
        else if (anisotropic > maxanis) m_anisotropic = maxanis;
        else                            m_anisotropic = anisotropic;
    }

    return m_anisotropic;
}


/**
 * Donne le nombre de surfaces affichées par le moteur graphique durant la dernière frame.
 *
 * \return Nombre de surfaces affichées.
 ******************************/

unsigned int CRenderer::getNumSurfaces() const
{
    return m_numSurfacesOld;
}


/**
 * Initialise OpenGL.
 ******************************/

void CRenderer::initOpenGL()
{
    CApplication::getApp()->log("Initialisation de OpenGL");

    if (glewInit() != GLEW_OK)
    {
        CApplication::getApp()->log("Erreur lors de l'initialisation de Glew", ILogger::Error);
    }

    // Test de profondeur
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Couleurs
    glEnable(GL_COLOR_MATERIAL);

    // Textures
    glEnable(GL_TEXTURE_2D);
    glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Shaders
    const unsigned char * shaderVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    CApplication::getApp()->log(CString("Version du language de shading d'OpenGL : ") + reinterpret_cast<const char *>(shaderVersion));

    if (shaderVersion[0] == '1')
    {
        if (shaderVersion[2] == '1')
            m_shaderVersion = GLSLVersion_1_10;
        else if (shaderVersion[2] == '2')
            m_shaderVersion = GLSLVersion_1_20;
        else if (shaderVersion[2] == '3')
            m_shaderVersion = GLSLVersion_1_30;
        else if (shaderVersion[2] == '4')
            m_shaderVersion = GLSLVersion_1_40;
        else if (shaderVersion[2] == '5')
            m_shaderVersion = GLSLVersion_1_50;
    }
    else if (shaderVersion[0] == '3')
    {
        if (shaderVersion[2] == '3')
            m_shaderVersion = GLSLVersion_3_30;
    }
    else if (shaderVersion[0] == '4')
    {
        if (shaderVersion[2] == '0')
            m_shaderVersion = GLSLVersion_4_00;
        else if (shaderVersion[2] == '1')
            m_shaderVersion = GLSLVersion_4_10;
        else if (shaderVersion[2] == '2')
            m_shaderVersion = GLSLVersion_4_20;
        else if (shaderVersion[2] == '3')
            m_shaderVersion = GLSLVersion_4_30;
        else if (shaderVersion[2] == '4')
            m_shaderVersion = GLSLVersion_4_40;
        else if (shaderVersion[2] == '5')
            m_shaderVersion = GLSLVersion_4_50;
    }

/*
    glEnable(GL_TEXTURE_2D);

    glActiveTextureARB(GL_TEXTURE0_ARB);
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glActiveTextureARB(GL_TEXTURE1_ARB);
    glClientActiveTextureARB(GL_TEXTURE1_ARB);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_INTERPOLATE);

    glActiveTextureARB(GL_TEXTURE0_ARB);
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glDisable(GL_TEXTURE_2D);
*/

    // Transparence
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.0f);

/*
    // Lumière
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    ILogger::log() << "Activation des lumières.\n";
*/

    // Autres paramètres
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_TEXTURE_COMPRESSION_HINT, GL_FASTEST);

    glCullFace(GL_FRONT);

    setRatio(CApplication::getApp()->getRatio());
}


/**
 * Redimensionne la fenêtre.
 *
 * \param width  Nouvelle largeur en pixels.
 * \param height Nouvelle hauteur en pixels.
 ******************************/

void CRenderer::resize(unsigned int width, unsigned int height)
{
    glViewport(0, 0, width, height);
    setRatio(static_cast<float>(width) / static_cast<float>(height));
}


/**
 * Enregistre une capture d'écran sur le disque.
 ******************************/

void CRenderer::screenShot()
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    unsigned char * buffer = new unsigned char[4 * CApplication::getApp()->getWidth() * CApplication::getApp()->getHeight()];

    // Les fonctions d'OpenGL doivent être appelées dans le thread principal
    glReadPixels(viewport[0], viewport[1], viewport[2], viewport[3], GL_RGBA, GL_UNSIGNED_BYTE, buffer);

    // Création d'un thread pour gérer l'image et l'enregistrer
    sf::Thread th(screenShot_Thread, buffer);
    th.launch(); // th est local à la méthode screenShot, donc on attend que le thread se termine pour quitter la fonction, ce qui est absurde...
}


/**
 * Enregistre une capture d'écran sur le disque (méthode appellée dans un thread).
 *
 * \todo Choix du format de l'image.
 *
 * \param buffer Pointeur sur le tableau des pixels de l'image.
 * \return L'entier 0.
 ******************************/

void CRenderer::screenShot_Thread(void * buffer)
{
    CImage image(CApplication::getApp()->getWidth(), CApplication::getApp()->getHeight(), reinterpret_cast<CColor *>(buffer));
    image.flip();

    delete[] reinterpret_cast<unsigned char *>(buffer);

    CDateTime date = CDateTime::getCurrentDateTime();

    image.saveToFile("screenshot_" + date.toString("%Y%m%d_%H%i%s" ) + ".jpg");
}


/**
 * Vérifie l'existence d'une extension OpenGL.
 *
 * \param extension Extension dont on souhaite vérifier l'existence.
 * \return Booléen.
 ******************************/

bool CRenderer::isOGLExtension(const char * extension)
{
    const unsigned char * exts = glGetString(GL_EXTENSIONS);

    return (strstr((const char *) exts, extension) != nullptr);
}


/**
 * Méthode exécutée avant le rendu de la scène.
 ******************************/

void CRenderer::beginScene()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    perpective();
    //CRenderer::_gluPerspective(m_angle - m_camera->getZoom() * (m_angle - ZOOM_ANGLE), Application::Instance().getRatio(), m_near, m_far);

    // On efface les buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (m_camera)
    {
        m_camera->look();
    }

    m_numSurfacesOld = m_numSurfaces;
    m_numSurfaces = 0;
}


/**
 * Dessine la scène.
 * Cette méthode doit être appelée entre BeginScene et EndScene.
 *
 * \todo Renommer en DrawSkybox et ne plus afficher de buffers.
 ******************************/

void CRenderer::displayScene()
{
    // Affichage de la skybox
    if (m_skybox)
    {
        m_skybox->draw(m_mode);
        m_numSurfaces += 12;
    }
/*
    // Affichage de chaque buffer
    for (std::list<CBuffer *>::const_iterator it = m_buffers.begin(); it != m_buffers.end(); ++it)
    {
        m_numSurfaces += (*it)->Draw();
    }
*/
}


/**
 * Méthode exécutée après le rendu de la scène.
 ******************************/

void CRenderer::endScene() const
{
    // Inversion des buffers
    glFlush(); // Apparemment ça ne sert à rien (sous Windows en tout cas)
}


/**
 * Effectue les tâches en attente (mise à jour ou suppression d'un buffer graphique).
 * Cette méthode doit être appelée régulièrement dans le thread principal.
 *
 * \todo Limiter le nombre d'actions à chaque appel.
 ******************************/

void CRenderer::doPendingTasks()
{
    sf::Lock lock(m_mutex);

    for (std::list<TBufferTask>::iterator it = m_buffersToUpdate.begin(); it != m_buffersToUpdate.end(); )
    {
        switch (it->action)
        {
            case BufferActionUpdate:
                it->buffer->update();
                break;

            // Suppression des buffers OpenGL
            case BufferActionDelete:
                glDeleteBuffersARB(1, &it->vertexBuffer);
                glDeleteBuffersARB(1, &it->indexBuffer);
                break;
        }

        it = m_buffersToUpdate.erase(it);
    }
}


void CRenderer::requestBufferUpdate(IBuffer * buffer)
{
    if (buffer == nullptr)
    {
        return;
    }

    for (std::list<TBufferTask>::const_iterator it = m_buffersToUpdate.begin(); it != m_buffersToUpdate.end(); ++it)
    {
        if (it->buffer == buffer)
        {
            return; // Soit on a déjà demandé une mise à jour du buffer, soit il doit être supprimé
        }
    }

    TBufferTask task;
    task.buffer = buffer;
    task.vertexBuffer = 0;
    task.indexBuffer = 0;
    task.action = BufferActionUpdate;
    m_buffersToUpdate.push_back(task);
}


void CRenderer::requestBufferDelete(IBuffer * buffer)
{
    if (buffer == nullptr)
    {
        return;
    }

    for (std::list<TBufferTask>::iterator it = m_buffersToUpdate.begin(); it != m_buffersToUpdate.end(); ++it)
    {
        if (it->buffer == buffer)
        {
            it->action = BufferActionDelete;
            it->vertexBuffer = buffer->getVertexBuffer();
            it->indexBuffer = buffer->getIndexBuffer();
            return;
        }
    }

    TBufferTask task;
    task.buffer = buffer;
    task.vertexBuffer = buffer->getVertexBuffer();
    task.indexBuffer = buffer->getIndexBuffer();
    task.action = BufferActionDelete;
    m_buffersToUpdate.push_back(task);
}


/**
 * Renvoie le profil de shader à utiliser.
 *
 * \param type Type de shader.
 * \return Profil correspondant au type de shader.
 ******************************/

CGprofile CRenderer::getShaderProfile(TShaderType type) const
{
    return (type == SHADER_VERTEX ? m_VSProfile : m_PSProfile);
}


/**
 * Renvoie les options à utiliser pour la création de shaders.
 *
 * \param type Type de shader.
 * \return Options correspondant au type de shader.
 ******************************/

const char * const * CRenderer::getShaderOptions(TShaderType type) const
{
    return (type == SHADER_VERTEX ? m_VSOptions : m_PSOptions);
}


/**
 * Crée un shader à partir d'un programme Cg.
 *
 * \param program Programme Cg.
 * \param type    Type du shader.
 * \return Pointeur sur le shader créé et chargé.
 ******************************/

CShader * CRenderer::createShader(CGprogram program, TShaderType type) const
{
    return new CShader(program, type);
}


/**
 * Change le vertex shader courant.
 *
 * \param shader Nouveau shader.
 ******************************/

void CRenderer::setVertexShader(const CShader * shader)
{
    if (shader)
    {
        if (shader->getType() != SHADER_VERTEX)
        {
            throw IException("Type de shader incorrect : pixel shader reçu dans setVertexShader");
        }

        cgGLBindProgram(shader->getProgram());
        cgGLEnableProfile(m_VSProfile);
    }
    else
    {
        cgGLDisableProfile(m_VSProfile);
        cgGLUnbindProgram(m_VSProfile);
    }
}


/**
 * Change le pixel shader courant.
 *
 * \param shader Nouveau shader.
 ******************************/

void CRenderer::setPixelShader(const CShader * shader)
{
    if (shader)
    {
        if (shader->getType() != SHADER_PIXEL)
        {
            throw IException("Type de shader incorrect : vertex shader reçu dans setPixelShader");
        }

        cgGLBindProgram(shader->getProgram());
        cgGLEnableProfile(m_PSProfile);
    }
    else
    {
        cgGLDisableProfile(m_PSProfile);
        cgGLUnbindProgram(m_PSProfile);
    }
}


/**
 * Initialise la perpective.
 ******************************/

void CRenderer::perpective() const
{
    if (m_camera)
        CRenderer::_gluPerspective(m_camera->getAngle(), m_ratio, m_near, m_far);
    else
        CRenderer::_gluPerspective(VIEW_ANGLE, m_ratio, m_near, m_far);
}


/**
 * Utilise le buffer d'accumulation. Cette méthode doit être appellée après avoir
 * dessiné la scène, et avant de dessiner les objets 2D (de préférence).
 * Ce buffer permet de réaliser des effets de type blur motion, antialiasing, ou
 * profondeur de champ.
 *
 * \param mult  Facteur de multiplication de la frame précédente.
 * \param accum Facteur de multiplication de la frame actuelle.
 ******************************/

void CRenderer::useAccumBuffer(float mult, float accum)
{
    glAccum(GL_MULT, mult);
    glAccum(GL_ACCUM, accum);
    glAccum(GL_RETURN, 1.0f);
}


/**
 * Modifie les paramètres du brouillard.
 *
 * \todo Pouvoir modifier la couleur du brouillard.
 *
 * \param start   Distance à partir de laquelle le brouillard est visible.
 * \param end     Distance au-delà de laquelle plus rien n'est visible.
 * \param density Densité du brouillard (entre 0 et 1).
 ******************************/

void CRenderer::changeFogParams(float start, float end, float density) const
{
    // Désactivation du brouillard
    if (end <= 0.0f || density <= 0.0f)
    {
        glDisable(GL_FOG);
    }
    // Activation du brouillard
    else
    {
        glEnable(GL_FOG);

        glFogf(GL_FOG_START, start);
        glFogf(GL_FOG_END, end);
        glFogf(GL_FOG_DENSITY, density);
        glFogi(GL_FOG_MODE, GL_LINEAR);
        glHint(GL_FOG_HINT, GL_NICEST);
    }
}


/**
 * Dépile la matrice.
 *
 * \param t Type de la matrice.
 ******************************/

void CRenderer::popMatrix(TMatrixType t)
{
    glMatrixMode(t);
    glPopMatrix();
}


/**
 * Empile la matrice courante.
 *
 * \param t Type de la matrice.
 ******************************/

void CRenderer::pushMatrix(TMatrixType t)
{
    glMatrixMode(t);
    glPushMatrix();
}


/**
 * Charge une matrice.
 *
 * \param t Type de la matrice.
 * \param m Matrice à charger.
 ******************************/

void CRenderer::loadMatrix(TMatrixType t, const TMatrix4F& m)
{
    glMatrixMode(t);
    glLoadMatrixf(m);
}


/**
 * Multiplie la matrice courante par une matrice.
 *
 * \param t Type de la matrice.
 * \param m Matrice.
 ******************************/

void CRenderer::multMatrix(TMatrixType t, const TMatrix4F& m)
{
    glMatrixMode(t);

    TMatrix4F m2 = m;
    m2.transpose();
    glMultMatrixf(m2);

    //glMultTransposeMatrixf(m);
}


/**
 * Récupère une matrice.
 *
 * \param t Type de la matrice.
 * \param m Matrice à récupérer.
 ******************************/

void CRenderer::getMatrix(TMatrixType t, TMatrix4F& m)
{
    glGetFloatv(t, m);
}


/**
 * Déplace la caméra. Cette fonction est équivalente à gluLookAt.
 *
 * \todo Pouvoir préciser l'axe vertical.
 *
 * \param position Position de la caméra.
 * \param view     Direction d'observation.
 ******************************/

void CRenderer::_gluLookAt(const TVector3F& position, const TVector3F& view)
{
    TVector3F f = view - position;
    f.normalize();

    TVector3F s = VectorCross(f, TVector3F(0.0f, 0.0f, 1.0f));
    s.normalize();

    TVector3F u = VectorCross(s, f);
    u.normalize();

    TMatrix4F M(s.X ,  u.X, -f.X, 0.0f,
                s.Y ,  u.Y, -f.Y, 0.0f,
                s.Z ,  u.Z, -f.Z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);

    glMultMatrixf(&M[0]);
    glTranslatef(-position.X, -position.Y, -position.Z);
}


/**
 * Modifie les options de perspective. Cette fonction est équivalente à gluPerpective.
 *
 * \param fovy   Angle de vue.
 * \param aspect Ratio.
 * \param znear  Distance minimale visible.
 * \param zfar   Distance maximale visible.
 ******************************/

void CRenderer::_gluPerspective(double fovy, double aspect, double znear, double zfar)
{
    if (std::abs(aspect) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double deltaZ = zfar - znear;

    if (std::abs(deltaZ) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double radians = fovy * PiOver360;
    double sine = ::sin(radians);

    if (std::abs(sine) < std::numeric_limits<float>::epsilon())
    {
        return;
    }

    double cotangent = ::cos(radians) / sine;

    // Make m an identity matrix
    double m[4][4] = { { 0.0f } };

    m[0][0] = cotangent / aspect;
    m[1][1] = cotangent;
    m[2][2] = -(zfar + znear) / deltaZ;
    m[2][3] = -1.0f;
    m[3][2] = -2.0f * znear * zfar / deltaZ;

    glMultMatrixd(&m[0][0]);
}


/**
 * Modifie la valeur du ratio (hauteur de la fenêtre divisée par sa largeur).
 *
 * \param ratio Valeur du ratio.
 ******************************/

void CRenderer::setRatio(double ratio)
{
    m_ratio = ratio;
}

} // Namespace Ted

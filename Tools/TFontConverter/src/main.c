
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <stdbool.h>

#include "structures.h"


bool ConvertFont ( const char * name , unsigned char quality , const char * filename );

int main ( int argc , char * argv[] )
{
    if ( argc != 3 )
    {
        fprintf ( stderr , "Usage: %s font quality\n" , argv[0] );
        return EXIT_FAILURE;
    }

    const size_t size = strlen ( argv[1] ) + strlen ( argv[2] ) + 6;
    char * filename = (char *) malloc ( size );
    memset ( filename , 0 , size );
    strcat ( filename , argv[1] );
    strcat ( filename , "_" );
    strcat ( filename , argv[2] );
    strcat ( filename , ".tef" );

    unsigned int q = atoi ( argv[2] );
    unsigned char quality = 16;
    if ( q >= 8 && q <= 255 ) quality = (unsigned char) q;

    if ( ConvertFont ( argv[1] , quality , filename ) )
    {
        fprintf ( stdout , "Font %s converted in file %s\n" , argv[1] , filename );
    }

    free ( filename );

    return EXIT_SUCCESS;
}


/// Charge une police de caractère.
bool ConvertFont ( const char * name , unsigned char quality , const char * filename )
{
    if ( quality < 8 )
    {
        fprintf ( stderr , "Quality must be greater than 8\n" );
        return false;
    }

    // Création de l'en-tête du fichier
    THeader header;

    header.ident = FileIdent;
    header.version = Version_1_0;
    header.name[30] = 0;
    header.size = quality;
    header.offset = sizeof(THeader);

    strncpy ( header.name , name , 30 );


    const unsigned int texsize = header.size * 16;
    const unsigned int texsize2 = texsize * texsize;


    // Création d'un HDC
    HDC hdc = CreateCompatibleDC ( NULL );

    if ( hdc == NULL )
    {
        fprintf ( stderr , "Error in function CreateCompatibleDC\n" );
        return false;
    }

    // Création d'une image contenant les pixels de la texture finale
    BITMAPINFO bitmap_info;
    memset ( &bitmap_info , 0 , sizeof(BITMAPINFO) );
    bitmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bitmap_info.bmiHeader.biWidth = texsize;
    bitmap_info.bmiHeader.biHeight = texsize;
    bitmap_info.bmiHeader.biBitCount = 24;
    bitmap_info.bmiHeader.biPlanes = 1;

    // Création de la DIB section
    uint8_t * data = NULL;
    HBITMAP BitmapHandle = CreateDIBSection ( hdc , &bitmap_info , DIB_RGB_COLORS , (void**) &data , NULL , 0 );

    if ( BitmapHandle == NULL )
    {
        fprintf ( stderr , "Error in function CreateDIBSection\n" );

        // Destruction des ressources
        DeleteDC ( hdc );
        return false;
    }

    // Chargement de la police
    HFONT FontHandle = CreateFont ( quality , 0 , 0 , 0 ,
                                    FW_NORMAL , FALSE , FALSE , FALSE ,
                                    DEFAULT_CHARSET , OUT_DEFAULT_PRECIS ,
                                    CLIP_DEFAULT_PRECIS , ANTIALIASED_QUALITY ,
                                    DEFAULT_PITCH , name );

    if ( FontHandle == NULL )
    {
        fprintf ( stderr , "Error in function CreateFont\n" );

        // Destruction des ressources
        DeleteObject ( BitmapHandle );
        DeleteDC ( hdc );

        return false;
    }

    // Initialisation des couleurs
    SelectObject ( hdc , BitmapHandle );
    SelectObject ( hdc , FontHandle );
    SetBkColor ( hdc , RGB ( 0 , 0 , 0 ) );
    SetTextColor ( hdc , RGB ( 255 , 255 , 255 ) );

    // Affichage des caractères
    char character = 0;

    for ( unsigned int j = 0 ; j < 16 ; ++j )
    {
        for ( unsigned int i = 0 ; i < 16 ; ++i , ++character )
        {
            // Stockage de la taille du caractère
            SIZE size;
            GetTextExtentPoint32 ( hdc, &character , 1 , &size );
            header.char_size[ i + j * 16 ].width  = size.cx;
            header.char_size[ i + j * 16 ].height = size.cy;

            // Affichage du caractère
            RECT rect = { i * quality , j * quality , ( i + 1 ) * quality , ( j + 1 ) * quality };

            if ( DrawText ( hdc , &character , 1 , &rect , DT_LEFT ) == 0 )
            {
                fprintf ( stderr , "Error with function DrawText\n" );

                // Destruction des ressources
                DeleteObject ( FontHandle );
                DeleteObject ( BitmapHandle );
                DeleteDC ( hdc );

                return false;
            }
        }
    }


    // Création de l'image
    uint8_t * pixels = (uint8_t *) malloc ( texsize2 );

    for ( unsigned int i = 0 ; i < texsize ; ++i )
    {
        for ( unsigned int j = 0 ; j < texsize ; ++j )
        {
            //pixels[i] = data[3*i];
            pixels[ i * texsize + j ] = data[ 3 * ( ( texsize - i - 1 ) * texsize + j ) ];
            //pixels[i] = ( data[3*i+0] + data[3*i+1] + data[3*i+2] ) / 3;
        }
    }


    // Destruction des ressources
    DeleteObject ( FontHandle );
    DeleteObject ( BitmapHandle );
    DeleteDC ( hdc );


    // Enregistrement du fichier
    FILE * file = fopen ( filename , "wb" );

    if ( file == NULL )
    {
        fprintf ( stderr , "Error when opening the file %s\n" , filename );
        return false;
    }

    fwrite ( &header , 1 , sizeof(header) , file );
    fwrite ( pixels , 1 , texsize2 , file );
    fclose ( file );


    free ( pixels );

    return true;
}

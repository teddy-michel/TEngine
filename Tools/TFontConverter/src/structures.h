
#ifndef T_FILE_STRUCTURES
#define T_FILE_STRUCTURES

#include <stdint.h>

static const uint16_t FileIdent = ( 'T' + ('F'<<8) ); ///< Identifiant du format de fichier.

/// Liste des versions du format de fichier.
static const uint16_t Version_1_0 = ( '1' + ('0' << 8) ); ///< Version 1.0 (18/02/2011).


#ifndef RC_INVOKED
#   pragma pack(push,1)
#endif

/// Dimensions d'un caractère.
typedef struct TCharSize_t
{
    uint8_t width;            ///< Largeur du caractère.
    uint8_t height;           ///< Hauteur du caractère.
} TCharSize;

/// En-tête des fichiers.
typedef struct THeader_t
{
    uint16_t ident;           ///< Identifiant du fichier.
    uint16_t version;         ///< Version du format.
    char name[31];            ///< Nom de la police.
    uint8_t size;             ///< Taille d'un caractère dans l'image.
    TCharSize char_size[256]; ///< Dimensions de chaque caractère.
    uint32_t offset;          ///< Offset de l'image dans le fichier.
} THeader;

#ifndef RC_INVOKED
#   pragma pack(pop)
#endif

#endif // T_FILE_STRUCTURES

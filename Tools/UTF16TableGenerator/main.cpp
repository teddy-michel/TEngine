
#include <stdint.h>
#include <fstream>
#include <iostream>
#include <sstream>


struct utf16_char
{
    uint8_t category;   ///< Catégorie du caractère.
    int8_t numeric;     ///< Chiffre correspondant au caractère.
    uint16_t lowercase; ///< Code du caractère minuscule correspondant.
    uint16_t uppercase; ///< Code du caractère majuscule correspondant.
    uint16_t titlecase; ///< Code du caractère de titre correspondant.
};

utf16_char utf16_table[256*256] = { { 0 , 0 , 0 , 0 , 0 } };


int8_t HexaToDecimal(char c);
uint16_t ConvertHexa(const std::string& str);
uint8_t Category(const std::string& str);


int8_t HexaToDecimal(char c)
{
    if (c >= '0' && c <= '9') return (c - '0');
    if (c >= 'a' && c <= 'f') return (c - 'a' + 10);
    if (c >= 'A' && c <= 'F') return (c - 'A' + 10);

    std::cerr << "Erreur : caractere non hexadecimal (" << c << ")\n";
    return -1;
}


uint16_t ConvertHexa(const std::string& str)
{
    return HexaToDecimal(str[0]) * 16 * 16 * 16 +
           HexaToDecimal(str[1]) * 16 * 16 +
           HexaToDecimal(str[2]) * 16 +
           HexaToDecimal(str[3]);
}


uint8_t Category(const std::string& str)
{
    if (str.size() != 2)
    {
        std::cerr << "Erreur : nom de categorie incorrect\n";
        return 0;
    }

    switch (str[0])
    {
        case 'M':

            switch (str[1])
            {
                case 'n': return 1;
                case 'c': return 2;
                case 'e': return 3;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'N':

            switch (str[1])
            {
                case 'd': return 4;
                case 'l': return 5;
                case 'o': return 6;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'Z':

            switch (str[1])
            {
                case 's': return 7;
                case 'l': return 8;
                case 'p': return 9;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'C':

            switch (str[1])
            {
                case 'c': return 10;
                case 'f': return 11;
                case 's': return 12;
                case 'o': return 13;
                case 'n': return 14;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'L':

            switch (str[1])
            {
                case 'u': return 15;
                case 'l': return 16;
                case 't': return 17;
                case 'm': return 18;
                case 'o': return 19;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'P':

            switch (str[1])
            {
                case 'c': return 20;
                case 'd': return 21;
                case 's': return 22;
                case 'e': return 23;
                case 'i': return 24;
                case 'f': return 25;
                case 'o': return 26;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        case 'S':

            switch (str[1])
            {
                case 'm': return 27;
                case 'c': return 28;
                case 'k': return 29;
                case 'o': return 30;

                default:
                    std::cerr << "Erreur : nom de categorie incorrect\n";
                    return 0;
            }

            break;

        default:
            std::cerr << "Erreur : nom de categorie incorrect\n";
            return 0;
    }

    return 12;
}

int main()
{
    std::ifstream fichier("UnicodeData.txt");

    if (!fichier)
    {
        std::cerr << "Erreur lors de l'ouverture du fichier.\n";
        return -1;
    }

    std::string ligne;

    while (std::getline(fichier, ligne))
    {
        utf16_char ch = { 0 , 0 , 0 , 0 , 0 };

        std::stringstream ligne_stream(ligne);
        std::string data;

        // Code
        std::getline(ligne_stream, data, ';');
        if (data.size() != 4) continue;
        uint16_t code = ConvertHexa(data);

        // Nom
        std::getline ( ligne_stream , data , ';' );

        // Catégorie
        std::getline ( ligne_stream , data , ';' );
        ch.category = Category ( data );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // Chiffre
        std::getline ( ligne_stream , data , ';' );
        if (data.size() == 1 && data[0] >= '0' && data[0] <= '9')
            ch.numeric = (data[0] - '0');
        else ch.numeric = -1;

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // RAF
        std::getline ( ligne_stream , data , ';' );

        // Majuscule
        std::getline ( ligne_stream , data , ';' );
        if ( data.size() == 4 ) ch.uppercase = ConvertHexa ( data );

        // Minuscule
        std::getline ( ligne_stream , data , ';' );
        if ( data.size() == 4 ) ch.lowercase = ConvertHexa ( data );

        // Titre
        std::getline ( ligne_stream , data , ';' );
        if ( data.size() == 4 ) ch.titlecase = ConvertHexa ( data );

        utf16_table[code] = ch;
    }

    fichier.close();

    // Sauvegarde de la table
    std::ofstream result("out.txt");

    if (!result)
    {
        std::cerr << "Impossible d'ouvrir le fichier out.txt\n";
    }

    for (std::size_t i = 0; i < 256*256; ++i)
    {
        utf16_char ch = utf16_table[i];

        if (i % 4 == 0) result << "\n";
        result << "{" << (int)ch.category << "," << (int)ch.numeric << "," << (int)ch.lowercase << "," << (int)ch.uppercase << "," << (int)ch.titlecase << "}";
        if (i < 256*256 - 1) result << ",";
    }

    result.close();

    return 0;
}

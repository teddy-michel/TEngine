
cmake_minimum_required(VERSION 2.8)

# Project name
project(TEngine)
set(LIBRARY_OUTPUT_PATH bin)

# Build type
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Choose the type of build (Debug or Release)" FORCE)
endif()

# Bullet directories
if(NOT BULLET_INCLUDE_DIR)
    set(BULLET_INCLUDE_DIR "" CACHE PATH "Bullet Dynamic include directory" FORCE)
endif()

if(NOT BULLET_LIBRARY_DIR)
    set(BULLET_LIBRARY_DIR "" CACHE PATH "Bullet Dynamic library directory" FORCE)
endif()

# SFML directories
if(NOT SFML_INCLUDE_DIR)
    set(SFML_INCLUDE_DIR "" CACHE PATH "SFML include directory" FORCE)
endif()

if(NOT SFML_LIBRARY_DIR)
    set(SFML_LIBRARY_DIR "" CACHE PATH "SFML library directory" FORCE)
endif()

# SIGC++ directories
if(NOT SIGCPP_INCLUDE_DIR)
    set(SIGCPP_INCLUDE_DIR "" CACHE PATH "sigc++ include directory" FORCE)
endif()

if(NOT SIGCPP_LIBRARY_DIR)
    set(SIGCPP_LIBRARY_DIR "" CACHE PATH "sigc++ library directory" FORCE)
endif()

# OpenAL directories
if(NOT OPENAL_INCLUDE_DIR)
    set(OPENAL_INCLUDE_DIR "" CACHE PATH "OpenAL include directory" FORCE)
endif()

# DevIL directories
if(NOT DEVIL_INCLUDE_DIR)
    set(DEVIL_INCLUDE_DIR "" CACHE PATH "DevIL include directory" FORCE)
endif()

# Cg directories
if(NOT CG_INCLUDE_DIR)
    set(CG_INCLUDE_DIR "" CACHE PATH "Cg include directory" FORCE)
endif()

# TinyXML directories
if(NOT TINYXML_INCLUDE_DIR)
    set(TINYXML_INCLUDE_DIR "" CACHE PATH "TinyXML include directory" FORCE)
endif()

if(NOT TINYXML_LIBRARY_DIR)
    set(TINYXML_LIBRARY_DIR "" CACHE PATH "TinyXML library directory" FORCE)
endif()

# Options
OPTION(USE_COVARIANT_RETURN "Use covariant return"  OFF)
OPTION(USE_MEMORY_MANAGER   "Use memory manager"    OFF)
OPTION(USE_PERFORMANCE_TEST "Use performance tests" OFF)

# Include directories
include_directories(include)
include_directories(${BULLET_INCLUDE_DIR})
include_directories(${SFML_INCLUDE_DIR})
include_directories(${SIGCPP_INCLUDE_DIR})
include_directories(${OPENAL_INCLUDE_DIR})
include_directories(${DEVIL_INCLUDE_DIR})
include_directories(${CG_INCLUDE_DIR})
include_directories(${TINYXML_INCLUDE_DIR})

# Library directories
link_directories(${BULLET_LIBRARY_DIR})
link_directories(${SFML_LIBRARY_DIR})
link_directories(${SIGCPP_LIBRARY_DIR})
link_directories(${SFML_LIBRARY_DIR})
link_directories(${TINYXML_LIBRARY_DIR})

# Definitions
add_definitions(-DTED_EXPORT_DLL)
add_definitions(-DTED_USING_PHYSIC_BULLET)
add_definitions(-DGLEW_STATIC)
#add_definitions(-DGLEW_BUILD)
add_definitions(-DSFML_DYNAMIC)

if(WIN32)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_definitions(-DTED_DEBUG -DDEBUG -D_DEBUG)

    if(CMAKE_COMPILER_IS_GNUCXX)
        add_definitions(-pg -lgmon)
    endif(CMAKE_COMPILER_IS_GNUCXX)
else()
    add_definitions(-DNDEBUG)

    if(CMAKE_COMPILER_IS_GNUCXX)
        add_definitions(-O -O1 -O2 -O3 -Os -fexpensive-optimizations)
    endif(CMAKE_COMPILER_IS_GNUCXX)
endif()

if(USE_CPP11)

    if(CMAKE_COMPILER_IS_GNUCXX)
        add_definitions(-DTED_USING_CPP11)
        add_definitions(-std=c++0x)
    endif(CMAKE_COMPILER_IS_GNUCXX)

endif()

if(NOT USE_COVARIANT_RETURN)
    add_definitions(-DTED_NO_COVARIANT_RETURN)
endif()

if(USE_MEMORY_MANAGER)
    add_definitions(-DTED_USING_MEMORY_MANAGER)
endif()

if(USE_PERFORMANCE_TEST)
    add_definitions(-DTED_PERFORMANCE)
endif()

# Files list
file(GLOB_RECURSE filesCore source/Core/* include/Core/* source/Loaders/* include/Loaders/*)
file(GLOB_RECURSE filesGame source/Game/* include/Game/* source/Entities/* include/Entities/*)
file(GLOB_RECURSE filesGraphic source/Graphic/* include/Graphic/*)
file(GLOB_RECURSE filesGUI source/GUI/* include/GUI/*)
file(GLOB_RECURSE filesNetwork source/Network/* include/Network/*)
file(GLOB_RECURSE filesPhysic source/Physic/* include/Physic/*)
file(GLOB_RECURSE filesSound source/Sound/* include/Sound/*)

add_library(
    TEngine
    SHARED
    ${filesCore}
    ${filesGame}
    ${filesGraphic}
    ${filesGUI}
    ${filesNetwork}
    ${filesPhysic}
    ${filesSound}
)

#add_library(TEngineCore SHARED ${filesCore})
#add_library(TEngineGame SHARED ${filesGame})
#add_library(TEngineGraphic SHARED ${filesGraphic})
#add_library(TEngineGUI SHARED ${filesGUI})
#add_library(TEngineNetwork SHARED ${filesNetwork})
#add_library(TEnginePhysic SHARED ${filesPhysic})
#add_library(TEngineSound SHARED ${filesSound})

# Libraries
target_link_libraries(TEngine
    sigc
    libsndfile-1
    OpenAL32
    DevIL
    cgGL
    cg
)

if(WIN32)
    target_link_libraries(TEngine
        glew32s
        #glew32
        glu32
        opengl32
        gdi32
        wsock32
        vfw32
    )
else()
    target_link_libraries(TEngine
        GLEW
        GLU
        GL
    )
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_link_libraries(TEngine
        libBulletCollision_Debug
        libBulletDynamics_Debug
        libLinearMath_Debug
        sfml-system-d
        sfml-window-d
        ticppd
    )
else()
    target_link_libraries(TEngine
        libBulletCollision
        libBulletDynamics
        libLinearMath
        sfml-system
        sfml-window
        ticpp
    )
endif()

Instructions pour compiler le moteur sur Linux.

Packages à installer :
* codeblocks
* gcc
* openal
* libsigc++
* nvidia-cg-toolkit
* devil
* libsndfile
* bullet
* sfml-git (SFML version 2)
* sfml-git-debug (SFML version 2 en mode DEBUG)
* TinyXML

Il se peut que libsigc++ ne soit pas installé dans les bons répertoires. En ce
qui me concerne, j'ai dû créer deux liens symboliques :

ln -s /usr/include/sigc++-2.0/sigc++/ /usr/include/sigc++
ln -s /usr/lib/sigc++-2.0/include/sigc++config.h /usr/include/sigc++config.h

Vous pouvez ensuite ouvrir le fichier TEngine-Linux.cbp avec CodeBlocks et le compiler.
